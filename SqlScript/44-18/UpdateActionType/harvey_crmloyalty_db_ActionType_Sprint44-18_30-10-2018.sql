INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (20, now(), now(), null, null, 'VoidPoint', null, null);
	
INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (21, now(), now(), null, null, 'VoidWallet', null, null);
	
INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (22, now(), now(), null, null, 'VoidMembership', null, null);
	
INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (23, now(), now(), null, null, 'ChangePassword', null, null);

INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (24, now(), now(), null, null, 'UpgradeMembership', null, null);
	
INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (25, now(), now(), null, null, 'RenewMembership', null, null);
	
INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (26, now(), now(), null, null, 'ExtendMembership', null, null);
	
INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (27, now(), now(), null, null, 'ChangeExpiryMembership', null, null);
	
INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (28, now(), now(), null, null, 'EditCommentMembership', null, null);