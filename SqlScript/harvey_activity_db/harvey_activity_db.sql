/*Action Typies Table*/
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (0, now(), now(), null, null, 'AddPoint', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (1, now(), now(), null, null, 'RedeemPoint', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (2, now(), now(), null, null, 'TopUp', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (3, now(), now(), null, null, 'Spending', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (4, now(), now(), null, null, 'Void', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (5, now(), now(), null, null, 'ExpiryPoint', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (6, now(), now(), null, null, 'UpdateAppSetting', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (7, now(), now(), null, null, 'InitCustomer', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (8, now(), now(), null, null, 'LogInStoreApp', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (9, now(), now(), null, null, 'LogInAdminApp', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (10, now(), now(), null, null, 'LogInMemberApp', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (11, now(), now(), null, null, 'ActiveCustomer', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (12, now(), now(), null, null, 'DeActiveCustomer', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (13, now(), now(), null, null, 'LoginServingCustomer', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (14, now(), now(), null, null, 'UpdateCustomerInfomation', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (15, now(), now(), null, null, 'UpdateCustomerProfile', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (16, now(), now(), null, null, 'ChangeMobilePhoneNumber', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (17, now(), now(), null, null, 'DeleteAppSetting', null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (18, now(), now(), null, null, 'AddAppSetting', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId")
	VALUES (19, now(), now(), null, null, 'UpdateOutlet', null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (20, now(), now(), null, null, 'VoidPoint', null, null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (21, now(), now(), null, null, 'VoidWallet', null, null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (22, now(), now(), null, null, 'VoidMembership', null, null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (23, now(), now(), null, null, 'ChangePassword', null, null);

	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (24, now(), now(), null, null, 'UpgradeMembership', null, null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (25, now(), now(), null, null, 'RenewMembership', null, null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (26, now(), now(), null, null, 'ExtendMembership', null, null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (27, now(), now(), null, null, 'ChangeExpiryMembership', null, null);
	
	INSERT INTO public."ActionTypies"(
	"Id", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Name", "ActionActivityId", "CreatedByName")
	VALUES (28, now(), now(), null, null, 'EditCommentMembership', null, null);
	 
/*Activities table*/	
	UPDATE public."Activities"
    SET "CreatedDate" = "UpdatedDate", "CreatedBy" = "UpdatedBy";
	
	UPDATE public."Activities"
    SET "UpdatedDate" = null, "UpdatedBy" = null
	