﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetBalanceCustomer.Model
{
    public class GetBalanceCustomerResponse
    {
        public decimal PointBalance { get; set; }
        public decimal WalletBalance { get; set; }
    }
}
