﻿using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetBalanceCustomer.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetBalanceCustomer
{
    public interface IGetBalanceCustomerQuery
    {
        GetBalanceCustomerResponse Execute(GetBalanceCustomerRequest request);
    }
}
