﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harvey.CRMLoyalty.Api;
using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetBalanceCustomer.Model;
using Microsoft.EntityFrameworkCore;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetBalanceCustomer
{
    public class GetBalanceCustomerQuery : IGetBalanceCustomerQuery
    {
        private readonly HarveyCRMLoyaltyDbContext _dbContext;

        public GetBalanceCustomerQuery(HarveyCRMLoyaltyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public GetBalanceCustomerResponse Execute(GetBalanceCustomerRequest request)
        {
            var queryCustomer =  _dbContext.Customers.AsNoTracking().Where(x => x.Id == request.CustomerId);

            if (queryCustomer != null)
            {
                var pointQuery = _dbContext.PointTransactions.AsNoTracking().Where(p => p.CustomerId == request.CustomerId).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
                var walleQuery = _dbContext.WalletTransactions.AsNoTracking().Where(w => w.CustomerId == request.CustomerId).OrderByDescending(w => w.CreatedDate).FirstOrDefault();
                var result = new GetBalanceCustomerResponse
                {
                    PointBalance = pointQuery != null ? pointQuery.BalanceTotal : 0,
                    WalletBalance = walleQuery != null ? walleQuery.BalanceTotal : 0
                };
                return result;
            }

            return null;
        }
    }
}
