﻿using Harvey.CRMLoyalty.Application.Models;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.CheckCustomerMembership.Model
{
    public class CheckCustomerMembershipModel
    {
        public string CustomerId { get; set; }
        public MembershipTypeModel MembershipType { get; set; }
    }
}
