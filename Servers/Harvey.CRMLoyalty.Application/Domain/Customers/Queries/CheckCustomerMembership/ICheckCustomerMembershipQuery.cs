﻿using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.CheckCustomerMembership.Model;
using System.Threading.Tasks;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.CheckCustomerMembership
{
    public interface ICheckCustomerMembershipQuery
    {
        Task<CheckCustomerMembershipModel> Execute(string customerId);
    }
}
