﻿using Harvey.CRMLoyalty.Api;
using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.CheckCustomerMembership.Model;
using Harvey.CRMLoyalty.Application.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.CheckCustomerMembership
{
    public class CheckCustomerMembershipQuery : ICheckCustomerMembershipQuery
    {
        private readonly HarveyCRMLoyaltyDbContext _dbContext;
        public CheckCustomerMembershipQuery(HarveyCRMLoyaltyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<CheckCustomerMembershipModel> Execute(string customerId)
        {
            var membership = await _dbContext.MembershipTransactions
                .Include(x => x.MembershipType)
                .Where(x => x.CustomerId == customerId)
                .OrderByDescending(x => x.CreatedDate)
                .FirstOrDefaultAsync();

            if (membership == null)
            {
                return null;
            }
            var membershipType = membership.MembershipType;
            var membershipTypeModel = new MembershipTypeModel
            {
                Id = membershipType.Id,
                TypeName = membershipType.TypeName
            };

            return new CheckCustomerMembershipModel
            {
                CustomerId = customerId,
                MembershipType = membershipTypeModel
            };
        }
    }
}
