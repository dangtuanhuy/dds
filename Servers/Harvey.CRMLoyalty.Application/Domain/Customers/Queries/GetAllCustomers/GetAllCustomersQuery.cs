﻿using Harvey.CRMLoyalty.Api;
using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetAllCustomers.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetAllCustomers
{
    public class GetAllCustomersQuery : IGetAllCustomersQuery
    {
        private readonly HarveyCRMLoyaltyDbContext _dbContext;
        public GetAllCustomersQuery(HarveyCRMLoyaltyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<GetAllCustomerInfoModel>> Execute(DateTime? lastSync)
        {
            var query = _dbContext.Customers.AsNoTracking();
            if (lastSync != null)
            {
                var lastSyncValue = lastSync.Value;
                query = query.Where(x => x.UpdatedDate >= lastSyncValue);
            }

            var result = await query.Select(x => new GetAllCustomerInfoModel
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                CustomerCode = x.CustomerCode,
                Email = x.Email,
                DateOfBirth = x.DateOfBirth,
                Gender = x.Gender,
                PhoneNumber = x.Phone,
                IsActive = (x.Status == Entities.Status.Active),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate
            }).ToListAsync();

            return result;
        }
    }
}
