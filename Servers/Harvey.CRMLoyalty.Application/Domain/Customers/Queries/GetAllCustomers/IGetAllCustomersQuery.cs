﻿using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetAllCustomers.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetAllCustomers
{
    public interface IGetAllCustomersQuery
    {
        Task<List<GetAllCustomerInfoModel>> Execute(DateTime? lastSync);
    }
}
