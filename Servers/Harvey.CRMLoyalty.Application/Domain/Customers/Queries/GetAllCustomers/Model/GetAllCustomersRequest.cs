﻿using System;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetAllCustomers.Model
{
    public class GetAllCustomersRequest
    {
        public DateTime? LastSync { get; set; }
    }
}
