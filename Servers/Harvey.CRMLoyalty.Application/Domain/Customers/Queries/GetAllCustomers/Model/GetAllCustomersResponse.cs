﻿using Harvey.CRMLoyalty.Application.Models;
using System.Collections.Generic;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetAllCustomers.Model
{
    public class GetAllCustomersResponse
    {
        public List<GetAllCustomerInfoModel> CustomerListResponse { get; set; }
    }
}
