﻿using Harvey.CRMLoyalty.Application.Domain.WalletTransactions.Queries.GetWalletSummary.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.CRMLoyalty.Application.Domain.WalletTransactions.Queries.GetWalletSummary
{
    public interface IGetWalletSummaryQuery
    {
        GetWalletSummaryResponse Execute(GetWalletSummaryRequest request);
    }
}
