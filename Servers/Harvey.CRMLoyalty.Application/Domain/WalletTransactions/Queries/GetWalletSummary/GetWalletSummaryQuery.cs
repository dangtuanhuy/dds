﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harvey.CRMLoyalty.Api;
using Harvey.CRMLoyalty.Application.Domain.WalletTransactions.Queries.GetWalletSummary.Model;
using Harvey.CRMLoyalty.Application.Models;
using Microsoft.EntityFrameworkCore;

namespace Harvey.CRMLoyalty.Application.Domain.WalletTransactions.Queries.GetWalletSummary
{
    public class GetWalletSummaryQuery : IGetWalletSummaryQuery
    {
        private readonly HarveyCRMLoyaltyDbContext _dbContext;
        public GetWalletSummaryQuery(HarveyCRMLoyaltyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public GetWalletSummaryResponse Execute(GetWalletSummaryRequest request)
        {
            var result = new GetWalletSummaryResponse();

            var outletsWalletSummary = new List<ItemWalletSummary>();

            var walletTransactions = _dbContext.WalletTransactions.AsNoTracking().Where(x => x.CreatedDate.HasValue
            && x.CreatedDate.Value >= request.FromDate
            && x.CreatedDate.Value <= request.ToDate);

            var outlets = _dbContext.Outlets.AsNoTracking().Select(o => new
            {
                Id = o.Id,
                Name = o.Name
            }).ToList();

            foreach (var outlet in outlets)
            {
                var outletWalletTransactions = walletTransactions.Where(x => x.OutletId == outlet.Id);

                var creditTransactions = outletWalletTransactions.Where(x => x.Debit == 0
                && (x.WalletTransactionReference == null
                        || (x.WalletTransactionReference != null && x.WalletTransactionReference.WalletTransactionReference != null)));

                var debitTransactions = outletWalletTransactions.Where(x => x.Credit == 0
                && (x.WalletTransactionReference == null
                        || (x.WalletTransactionReference != null && x.WalletTransactionReference.WalletTransactionReference != null)));

                var voidOfCreditTransactions = outletWalletTransactions.Where(x => x.Credit == 0
                && x.WalletTransactionReference != null
                && x.WalletTransactionReference.WalletTransactionReference == null);

                var voidOfDebitTransactions = outletWalletTransactions.Where(x => x.Debit == 0
                && x.WalletTransactionReference != null
                && x.WalletTransactionReference.WalletTransactionReference == null);

                var item = new ItemWalletSummary
                {
                    OutletName = outlet.Name,
                    TotalTopup = creditTransactions.Sum(x => x.Credit),
                    TotalSpend = debitTransactions.Sum(x => x.Debit),
                    TotalVoidOfTopup = voidOfCreditTransactions.Sum(x => x.Debit),
                    TotalVoidOfSpend = voidOfDebitTransactions.Sum(x => x.Credit)
                };

                outletsWalletSummary.Add(item);
            }

            result.walletSummaryItems = outletsWalletSummary;
            return result;
        }
    }
}
