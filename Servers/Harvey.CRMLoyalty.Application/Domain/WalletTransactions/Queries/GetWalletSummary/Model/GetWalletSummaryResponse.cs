﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.CRMLoyalty.Application.Domain.WalletTransactions.Queries.GetWalletSummary.Model
{
    public class GetWalletSummaryResponse
    {
        public List<ItemWalletSummary> walletSummaryItems { get; set; }
    }

    public class ItemWalletSummary
    {
        public string OutletName { get; set; }
        public decimal TotalTopup { get; set; }
        public decimal TotalSpend { get; set; }
        public decimal TotalVoidOfTopup { get; set; }
        public decimal TotalVoidOfSpend { get; set; }
    }
}
