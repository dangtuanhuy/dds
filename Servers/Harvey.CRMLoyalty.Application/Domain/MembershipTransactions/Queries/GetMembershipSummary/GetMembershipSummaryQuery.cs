﻿
using System.Collections.Generic;
using System.Linq;
using Harvey.CRMLoyalty.Api;
using Harvey.CRMLoyalty.Application.Domain.MembershipTransactions.Queries.GetMembershipSummary.Model;
using Harvey.CRMLoyalty.Application.Models;
using Microsoft.EntityFrameworkCore;

namespace Harvey.CRMLoyalty.Application.Domain.MembershipTransactions.Queries.GetMembershipSummary
{
    public class GetMembershipSummaryQuery : IGetMembershipSummaryQuery
    {
        private readonly HarveyCRMLoyaltyDbContext _dbContext;

        public GetMembershipSummaryQuery(HarveyCRMLoyaltyDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public GetMembershipSummaryResponse Execute(GetMembershipSummaryRequest request)
        {
            var result = new GetMembershipSummaryResponse();

            var membershipSummaryItems = new List<ItemMembershipSummary>();

            var membershipTransactions = _dbContext.MembershipTransactions.AsNoTracking().Where(x => x.CreatedDate.HasValue 
            && x.CreatedDate.Value >= request.FromDate 
            && x.CreatedDate.Value <= request.ToDate);

            var outlets = _dbContext.Outlets.AsNoTracking().Select(o => new
            {
                Id = o.Id,
                Name = o.Name
            }).ToList();

            var newBasicMembers = membershipTransactions
                .Where(x => (Data.MembershipActionType)x.MembershipActionTypeId == Data.MembershipActionType.New);

            var upgradedMembers = membershipTransactions
                .Where(x => (Data.MembershipActionType)x.MembershipActionTypeId == Data.MembershipActionType.Upgrade)
                .GroupBy(x => x.CustomerId)
                .Select(x => x.OrderByDescending(y => y.CreatedDate).FirstOrDefault());

            var renewedMembers = membershipTransactions
                .Where(x => (Data.MembershipActionType)x.MembershipActionTypeId == Data.MembershipActionType.Renew)
                .GroupBy(x => x.CustomerId)
                .Select(x => x.OrderByDescending(y => y.CreatedDate).FirstOrDefault());

            var extendedMembers = membershipTransactions
                .Where(x => (Data.MembershipActionType)x.MembershipActionTypeId == Data.MembershipActionType.Extend)
                .GroupBy(x => x.CustomerId)
                .Select(x => x.OrderByDescending(y => y.CreatedDate).FirstOrDefault());

            var voidedMembers = membershipTransactions
                .Where(x => (Data.MembershipActionType)x.MembershipActionTypeId == Data.MembershipActionType.Void)
                .GroupBy(x => x.CustomerId)
                .Select(x => x.OrderByDescending(y => y.CreatedDate).FirstOrDefault());

            var premiumMembers = _dbContext.MembershipTransactions.AsNoTracking()
                                    .GroupBy(x => x.CustomerId)
                                    .Select(x => x.OrderByDescending(y => y.CreatedDate).FirstOrDefault())
                                    .Where(x => x != null && x.MembershipTypeId == (int)MembershipTranactionEnum.PremiumPlus);

            foreach (var outlet in outlets)
            {
                var outletNewBasicMembers = newBasicMembers.Count(x => x.OutletId == outlet.Id);

                var outletUpgradedMembers = upgradedMembers.Count(x => x.OutletId == outlet.Id);

                var outletExtendedMembers = extendedMembers.Count(x => x.OutletId == outlet.Id);

                var outletRenewedMembers = renewedMembers.Count(x => x.OutletId == outlet.Id);

                var outletVoidedMembers = voidedMembers.Count(x => x.OutletId == outlet.Id);

                var outletExpiredMembers = premiumMembers.Count(x => x.ExpiredDate.HasValue
                                && x.ExpiredDate.Value >= request.FromDate
                                && x.ExpiredDate.Value <= request.ToDate
                                && x.OutletId == outlet.Id);

                var item = new ItemMembershipSummary
                {
                    OutletName = outlet.Name,
                    TotalNewBasicMember = outletNewBasicMembers,
                    TotalUpgradedMember = outletUpgradedMembers,
                    TotalExtendedMember = outletExtendedMembers,
                    TotalRenewedMember = outletRenewedMembers,
                    TotalExpiredMember = outletExpiredMembers,
                    TotalVoidedMember = outletVoidedMembers
                };

                membershipSummaryItems.Add(item);
            }

            result.membershipSummaryItems = membershipSummaryItems;
            return result;
        }
    }
}
