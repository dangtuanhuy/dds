﻿using Harvey.CRMLoyalty.Application.Domain.MembershipTransactions.Queries.GetMembershipSummary.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.CRMLoyalty.Application.Domain.MembershipTransactions.Queries.GetMembershipSummary
{
    public interface IGetMembershipSummaryQuery
    {
        GetMembershipSummaryResponse Execute(GetMembershipSummaryRequest request);
    }
}
