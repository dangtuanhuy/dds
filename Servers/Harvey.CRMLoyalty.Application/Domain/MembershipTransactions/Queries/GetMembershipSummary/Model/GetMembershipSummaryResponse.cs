﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.CRMLoyalty.Application.Domain.MembershipTransactions.Queries.GetMembershipSummary.Model
{
    public class GetMembershipSummaryResponse
    {
        public List<ItemMembershipSummary> membershipSummaryItems { get; set; }
    }

    public class ItemMembershipSummary
    {
        public string OutletName { get; set; }
        public decimal TotalNewBasicMember { get; set; }
        public decimal TotalUpgradedMember { get; set; }
        public decimal TotalRenewedMember { get; set; }
        public decimal TotalExtendedMember { get; set; }
        public decimal TotalExpiredMember { get; set; }
        public decimal TotalVoidedMember { get; set; }
    }
}
