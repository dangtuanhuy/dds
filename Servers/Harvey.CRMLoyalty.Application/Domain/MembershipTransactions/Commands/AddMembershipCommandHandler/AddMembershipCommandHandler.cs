﻿using Harvey.CRMLoyalty.Api;
using Harvey.CRMLoyalty.Application.Configuration;
using Harvey.CRMLoyalty.Application.Constants;
using Harvey.CRMLoyalty.Application.Services.Activity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.CRMLoyalty.Application.Domain.Customers.Commands.AddMembershipCommandHandler
{
    public class AddMembershipCommandHandler : IAddMembershipCommandHandler
    {
        private readonly HarveyCRMLoyaltyDbContext _dbContext;
        private readonly ILoggingActivityService _activityService;
        private IOptions<ConfigurationRabbitMq> _configurationRabbitMq;

        public AddMembershipCommandHandler(HarveyCRMLoyaltyDbContext dbContext,
            ILoggingActivityService activityService,
            IOptions<ConfigurationRabbitMq> configurationRabbitMq)
        {
            _dbContext = dbContext;
            _activityService = activityService;
            _configurationRabbitMq = configurationRabbitMq;
        }

        public async Task<string> ExecuteAsync(AddMembershipCommand addMembershipCommand)
        {
            var checkExistCustomer = _dbContext.Customers.Any(x => x.Id == addMembershipCommand.CustomerId && x.Status == Entities.Status.Active);
            if(checkExistCustomer)
            {
                var membershipTransaction = new Entities.MembershipTransaction();

                membershipTransaction.Id = Guid.NewGuid().ToString();
                membershipTransaction.CustomerId = addMembershipCommand.CustomerId;
                membershipTransaction.StaffId = addMembershipCommand.StaffId;
                membershipTransaction.OutletId = addMembershipCommand.OutletId;
                membershipTransaction.Comment = addMembershipCommand.Comment;
                membershipTransaction.MembershipTypeId = addMembershipCommand.MembershipTypeId;
                membershipTransaction.ExpiredDate = addMembershipCommand.ExpiredDate;
                membershipTransaction.UpdatedDate = DateTime.UtcNow;
                membershipTransaction.CreatedBy = addMembershipCommand.UserId;
                membershipTransaction.CreatedDate = DateTime.UtcNow;
                membershipTransaction.IPAddress = addMembershipCommand.IpAddress;
                membershipTransaction.MembershipActionTypeId = addMembershipCommand.MembershipActionType;

                _dbContext.MembershipTransactions.Add(membershipTransaction);

                var membershipActionType = await _dbContext.MembershipActionTypes.FindAsync(addMembershipCommand.MembershipActionType);
                
                await _dbContext.SaveChangesAsync();
               
                if (membershipActionType.TypeName == "Upgraded"     || 
                    membershipActionType.TypeName == "Renew"        ||
                    membershipActionType.TypeName == "Extend"       ||
                    membershipActionType.TypeName == "ChangeExpiry" ||
                    membershipActionType.TypeName == "Comment")
                {
                    var user = _dbContext.Staffs.Where(a => a.Id == addMembershipCommand.UserId).FirstOrDefault();
                    var userName = addMembershipCommand.UserId == LogInformation.AdministratorId ? LogInformation.AdministratorName : (user != null ? $"{user.FirstName} {user.LastName}" : "");
                    var outletCode = _dbContext.Outlets.Find(addMembershipCommand.OutletId).Code;
                    var customer = await _dbContext.Customers.FindAsync(addMembershipCommand.CustomerId);
                    var customerCode = "";
                    var phoneNumber = "";
                    if (customer != null)
                    {
                        customer.LastUsed = DateTime.UtcNow;
                        customerCode = customer.CustomerCode;
                        phoneNumber = customer.PhoneCountryCode + customer.Phone;
                    }
                    var expiryDate = addMembershipCommand.ExpiredDate.Value.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                    await LogAction(addMembershipCommand.UserId, _configurationRabbitMq.Value.RabbitMqUrl, customerCode, outletCode, phoneNumber, userName, membershipActionType.TypeName, addMembershipCommand.OutletId, expiryDate);
                }

                return membershipTransaction.Id;
            }
            return null;      
        }
        private async Task LogAction(string userId, string rabbitMqUrl, string customerCode, string outletCode, string phoneNumber, string userName, string membershipActionTypeName, string outletId, string expiryDate)
        {
            var request = new LoggingActivityRequest();

            if (membershipActionTypeName == "ChangeExpiry")
            {
                request.Description = $"{customerCode}.{phoneNumber}";
                request.Comment = expiryDate;
            } else
            {
                request.Description = customerCode;
                request.Comment = phoneNumber;
            }

            request.UserId = userId;
            request.ActionAreaPath = ActionArea.StoreApp;
            request.CreatedByName = userName;
            request.OutletCode = outletCode;
            request.Value = outletId;
            request.ActionType = (membershipActionTypeName == "Upgraded" ? ActionType.UpgradeMembership :
                                 (membershipActionTypeName == "Renew" ? ActionType.RenewMembership :
                                 (membershipActionTypeName == "Extend" ? ActionType.ExtendMembership :
                                 (membershipActionTypeName == "ChangeExpiry" ? ActionType.ChangeExpiryMembership : ActionType.EditCommentMembership))));

            await _activityService.ExecuteAsync(request, rabbitMqUrl);
        }
    }
}
