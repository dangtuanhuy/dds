﻿using Harvey.CRMLoyalty.Application.Domain.PointTransactions.Queries.GetPointSummary.Model;

namespace Harvey.CRMLoyalty.Application.Domain.PointTransactions.Queries.GetPointSummary
{
    public interface IGetPointSummaryQuery
    {
        GetPointSummaryResponse Execute(GetPointSummaryRequest request);
    }
}
