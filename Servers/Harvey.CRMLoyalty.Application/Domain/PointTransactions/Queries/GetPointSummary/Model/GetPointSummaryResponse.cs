﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.CRMLoyalty.Application.Domain.PointTransactions.Queries.GetPointSummary.Model
{
    public class GetPointSummaryResponse
    {
        public List<ItemPointSummary> pointSummaryItems { get; set; }
    }

    public class ItemPointSummary
    {
        public string OutletName { get; set; }
        public decimal TotalAdd { get; set; }
        public decimal TotalRedeem { get; set; }
        public decimal TotalVoidOfAdd { get; set; }
        public decimal TotalVoidOfRedeem { get; set; }
    }
}
