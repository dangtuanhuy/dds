﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harvey.CRMLoyalty.Api;
using Harvey.CRMLoyalty.Application.Domain.PointTransactions.Queries.GetPointSummary.Model;
using Microsoft.EntityFrameworkCore;

namespace Harvey.CRMLoyalty.Application.Domain.PointTransactions.Queries.GetPointSummary
{
    public class GetPointSummaryQuery : IGetPointSummaryQuery
    {
        private readonly HarveyCRMLoyaltyDbContext _dbContext;
        public GetPointSummaryQuery(HarveyCRMLoyaltyDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public GetPointSummaryResponse Execute(GetPointSummaryRequest request)
        {
            var result = new GetPointSummaryResponse();

            var pointSummaryItems = new List<ItemPointSummary>();

            var pointTransactions = _dbContext.PointTransactions.AsNoTracking().Where(x => x.CreatedDate.HasValue
            && x.CreatedDate.Value >= request.FromDate
            && x.CreatedDate.Value <= request.ToDate);

            var outlets = _dbContext.Outlets.AsNoTracking().Select(o => new
            {
                Id = o.Id,
                Name = o.Name
            }).ToList();

            foreach (var outlet in outlets)
            {
                var outletTransactions = pointTransactions.Where(x => x.OutletId == outlet.Id);

                var creditTransactions = outletTransactions.Where(x => x.Debit == 0
                && (x.PointTransactionReference == null
                        || (x.PointTransactionReference != null && x.PointTransactionReference.PointTransactionReference != null)));

                var debitTransactions = outletTransactions.Where(x => x.Credit == 0
                && (x.PointTransactionReference == null
                        || (x.PointTransactionReference != null && x.PointTransactionReference.PointTransactionReference != null)));

                var voidOfCreditTransactions = outletTransactions.Where(x => x.Credit == 0
                && x.PointTransactionReference != null
                && x.PointTransactionReference.PointTransactionReference == null);

                var voidOfDebitTransactions = outletTransactions.Where(x => x.Debit == 0
                && x.PointTransactionReference != null
                && x.PointTransactionReference.PointTransactionReference == null);

                var item = new ItemPointSummary
                {
                    OutletName = outlet.Name,
                    TotalAdd = creditTransactions.Sum(x => x.Credit),
                    TotalRedeem = debitTransactions.Sum(x => x.Debit),
                    TotalVoidOfAdd = voidOfCreditTransactions.Sum(x => x.Debit),
                    TotalVoidOfRedeem = voidOfDebitTransactions.Sum(x => x.Credit)
                };

                pointSummaryItems.Add(item);
            }

            result.pointSummaryItems = pointSummaryItems;
            return result;
        }
    }
}
