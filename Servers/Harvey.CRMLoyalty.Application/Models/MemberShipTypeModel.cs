﻿namespace Harvey.CRMLoyalty.Application.Models
{
    public class MembershipTypeModel
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}
