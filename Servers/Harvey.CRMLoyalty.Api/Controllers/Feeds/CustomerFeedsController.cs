﻿using Harvey.CRMLoyalty.Api.Extensions;
using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.CheckCustomerMembership;
using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.CheckCustomerMembership.Model;
using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetAllCustomers;
using Harvey.CRMLoyalty.Application.Domain.Customers.Queries.GetAllCustomers.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.CRMLoyalty.Api.Controllers.Feeds
{
    [Route("api/feed/customers")]
    public class CustomerFeedsController : Controller
    {
        private readonly IGetAllCustomersQuery _getAllCustomersQuery;
        private readonly ICheckCustomerMembershipQuery _checkCustomerMembershipQuery;

        public CustomerFeedsController(IGetAllCustomersQuery getAllCustomersQuery,
            ICheckCustomerMembershipQuery checkCustomerMembershipQuery)
        {
            _getAllCustomersQuery = getAllCustomersQuery;
            _checkCustomerMembershipQuery = checkCustomerMembershipQuery;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<List<GetAllCustomerInfoModel>> GetCustomers(DateTime? lastSync, string secretKey)
        {
            if (secretKey != SecretKeyExtension.SystemSecretKey)
            {
                return null;
            }

            return await _getAllCustomersQuery.Execute(lastSync);
        }

        [HttpGet("membership-inspection")]
        [AllowAnonymous]
        public async Task<CheckCustomerMembershipModel> CheckCustomerMembership(string customerId, string secretKey)
        {
            if (secretKey != SecretKeyExtension.SystemSecretKey)
            {
                return null;
            }

            return await _checkCustomerMembershipQuery.Execute(customerId);
        }
    }
}
