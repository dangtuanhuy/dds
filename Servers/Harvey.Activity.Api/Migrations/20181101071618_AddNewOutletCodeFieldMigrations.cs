﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Activity.Api.Migrations
{
    public partial class AddNewOutletCodeFieldMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OutletCode",
                table: "Activities",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OutletCode",
                table: "Activities");
        }
    }
}
