﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harvey.Activity.Api;
using Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActivitiesByActionType.Model;
using Harvey.Activity.Application.Extensions.PagingExtensions;
using Harvey.Activity.Application.Model;
using Microsoft.EntityFrameworkCore;

namespace Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActivitiesByActionType
{
    class GetActivitiesByActionTypeQuery : IGetActivitiesByActionTypeQuery
    {
        private readonly HarveyActivityDbContext _dbContext;
        public GetActivitiesByActionTypeQuery(HarveyActivityDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public GetActivitiesByActionTypeResponse Execute(GetActivitiesByActionTypeRequest request)
        {
            var query = _dbContext.Activities
                .Include(x => x.ActionType)
                .Include(x => x.AreaActivity)
                .OrderByDescending(x => x.CreatedDate).
                Where(x => x.ActionTypeId == request.ActionTypeId)
                .Select(x => new ActionActivityModel
                {
                    Id = x.Id,
                    Description = x.Description,
                    UpdatedBy = x.UpdatedBy,
                    UpdatedDate = x.UpdatedDate.Value,
                    CreatedBy = x.CreatedBy,
                    CreatedDate = x.CreatedDate.Value,
                    Comment = x.Comment,
                    ActionArea = x.ActionAreaId,
                    ActionType = x.ActionTypeId,
                    CreatedByName = x.CreatedByName,
                    OutletCode = x.OutletCode
                })
               .OrderByDescending(x => x.CreatedDate.Value)
               .AsQueryable();

            var result = PagingExtensions.GetPaged<ActionActivityModel>(query, request.PageNumber, request.PageSize);

            var response = new GetActivitiesByActionTypeResponse();
            response.TotalItem = result.TotalItem;
            response.PageSize = result.PageSize;
            response.PageNumber = result.PageNumber;
            response.ActionActivityModels = result.Results.ToList();
            return response;
        }
    }
}
