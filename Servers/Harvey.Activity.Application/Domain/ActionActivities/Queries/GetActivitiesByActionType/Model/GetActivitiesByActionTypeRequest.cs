﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActivitiesByActionType.Model
{
    public class GetActivitiesByActionTypeRequest
    {
        public string ActionTypeId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
