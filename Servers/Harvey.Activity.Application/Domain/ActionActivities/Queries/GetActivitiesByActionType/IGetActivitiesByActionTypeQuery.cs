﻿using Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActivitiesByActionType.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActivitiesByActionType
{
    public interface IGetActivitiesByActionTypeQuery
    {
        GetActivitiesByActionTypeResponse Execute(GetActivitiesByActionTypeRequest request);
    }
}
