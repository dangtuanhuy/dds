﻿using System.Linq;
using Harvey.Activity.Api;
using Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActionTypes.Model;
using Microsoft.EntityFrameworkCore;

namespace Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActionTypes
{
    public class GetActionTypesQuery : IGetActionTypesQuery
    {
        private readonly HarveyActivityDbContext _dbContext;

        public GetActionTypesQuery(HarveyActivityDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public GetActionTypesResponse Execute()
        {

            var response = new GetActionTypesResponse
            {
                ActionTypes = _dbContext.ActionTypies.AsNoTracking().Select(x => new ActionType { Id = x.Id, Name = x.Name }).ToList()
            };

            return response;
        }
    }
}
