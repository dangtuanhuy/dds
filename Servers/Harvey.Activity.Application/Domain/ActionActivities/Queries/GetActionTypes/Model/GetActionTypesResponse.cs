﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActionTypes.Model
{
    public class GetActionTypesResponse
    {
        public List<ActionType> ActionTypes { get; set; }
    }

    public class ActionType
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
