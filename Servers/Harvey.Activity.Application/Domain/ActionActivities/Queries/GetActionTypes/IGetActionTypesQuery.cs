﻿using Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActionTypes.Model;

namespace Harvey.Activity.Application.Domain.ActionActivities.Queries.GetActionTypes
{
    public interface IGetActionTypesQuery
    {
        GetActionTypesResponse Execute();
    }
}
