﻿using Harvey.Activity.Api;
using Harvey.Activity.Application.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Activity.Application.Data
{
    public class DataSeeder
    {
        public static void Seed(IServiceProvider serviceProvider)
        {
            try
            {
                Policy.Handle<Exception>()
               .WaitAndRetry(new[] {
                    TimeSpan.FromSeconds(10),
                    TimeSpan.FromSeconds(10),
                    TimeSpan.FromSeconds(10)
               }, (exception, timeSpan, retryCount, context) =>
               {

               }).Execute(() =>
               {
                   var harveyActivityDbContext = serviceProvider.GetRequiredService<HarveyActivityDbContext>();
                   harveyActivityDbContext.Database.Migrate();
                   SeedAreaActivityAsync(harveyActivityDbContext).Wait();
                   SeedActionTypeAsync(harveyActivityDbContext).Wait();
                   SeedErrorSourceAsync(harveyActivityDbContext).Wait();
               });
            }
            catch (Exception ex)
            {
                var logger = serviceProvider.GetRequiredService<ILogger<DataSeeder>>();
                logger.LogError(ex, "An error occurred seeding the DB.");
            }
        }

        #region App Setting Type
        private static async Task SeedAreaActivityAsync(HarveyActivityDbContext context)
        {
            if (context.AreaActivities.Any())
                return;

            var areaActivities = new List<AreaActivity>()
            {
                new AreaActivity
                {
                    Id = "0",
                    AreaPath = "StoreApp"
                },
                new AreaActivity
                {
                    Id = "1",
                    AreaPath = "MemberApp"
                },
                new AreaActivity
                {
                    Id = "2",
                    AreaPath = "AdminApp"
                },
                new AreaActivity
                {
                    Id = "3",
                    AreaPath = "Job"
                },

            };
            context.AddRange(areaActivities);
            await context.SaveChangesAsync();
        }
        #endregion

        #region App Action Type
        private static async Task SeedActionTypeAsync(HarveyActivityDbContext context)
        {
            if (context.ActionTypies.Any())
                return;

            var actionTypes = new List<Entities.ActionType>()
            {
                new Entities.ActionType
                {
                    Id = "0",
                    Name = "AddPoint",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "1",
                    Name = "RedeemPoint",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "2",
                    Name = "TopUp",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "3",
                    Name = "Spending",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "4",
                    Name = "Void",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "5",
                    Name = "ExpiryPoint",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "6",
                    Name = "UpdateAppSetting",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "7",
                    Name = "InitCustomer",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "8",
                    Name = "LogInStoreApp",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "9",
                    Name = "LogInAdminApp",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "10",
                    Name = "LogInMemberApp",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "11",
                    Name = "ActiveCustomer",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "12",
                    Name = "DeActiveCustomer",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="13",
                    Name = "LoginServingCustomer",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="14",
                    Name = "UpdateCustomerInfomation",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="15",
                    Name = "UpdateCustomerProfile",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="16",
                    Name =  "ChangeMobilePhoneNumber",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="17",
                    Name = "DeleteAppSetting",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "18",
                    Name = "AddAppSetting",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id="19",
                    Name ="UpdateOutlet",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="20",
                    Name = "VoidPoint",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "21",
                    Name = "VoidWallet",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="22",
                    Name = "VoidMembership",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="23",
                    Name = "ChangePassword",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "24",
                    Name = "UpgradeMembership",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="25",
                    Name = "RenewMembership",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "26",
                    Name = "ExtendMembership",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id ="27",
                    Name = "ChangeExpiryMembership",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                },
                new Entities.ActionType
                {
                    Id = "28",
                    Name = "EditCommentMembership",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                }
            };
            context.AddRange(actionTypes);
            await context.SaveChangesAsync();
        }
        #endregion

        #region Log Error Source
        private static async Task SeedErrorSourceAsync(HarveyActivityDbContext context)
        {
            if (context.ErrorLogSources.Any())
                return;

            var errorLogSources = new List<ErrorLogSource>()
            {
                new ErrorLogSource
                {
                    Id = 1,
                    SourceName = "AdminApp"
                },
                new ErrorLogSource
                {
                    Id =2,
                    SourceName = "MemberApp"
                },
                new ErrorLogSource
                {
                    Id =3,
                    SourceName = "StoreApp"
                },
                new ErrorLogSource
                {
                    Id = 4,
                    SourceName = "BackEnd"
                },
                new ErrorLogSource
                {
                    Id = 5,
                    SourceName = "FrontEnd"
                }
            };
            context.AddRange(errorLogSources);
            await context.SaveChangesAsync();
        }
        #endregion
    }
}
