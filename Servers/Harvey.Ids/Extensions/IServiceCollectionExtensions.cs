﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.EventStore.Marten;
using Harvey.EventBus.RabbitMQ;
using Harvey.EventBus.RabbitMQ.Policies;
using Harvey.Persitance.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Harvey.Ids.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IEventStore>(sp =>
            {
                return new MartenEventStore(configuration["ConnectionStrings:DefaultConnection"]);
            });

            services.AddSingleton(sp =>
            {
                var logger = sp.GetRequiredService<ILogger<MasstransitPersistanceConnection>>();
                var endPoint = configuration["RabbitMqConfig:RabbitMqUrl"];
                var id = configuration["RabbitMqConfig:Username"];
                var pass = configuration["RabbitMqConfig:Password"];
                return new MasstransitPersistanceConnection(new BusCreationRetrivalPolicy(), logger, endPoint,id, pass);
            });
            services.AddSingleton<IEventBus>(sp =>
            {
                return new MasstransitEventBus("Harvey_Ids", sp.GetRequiredService<MasstransitPersistanceConnection>(), sp);
            });

            return services;
        }
    }
}
