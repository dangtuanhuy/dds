﻿using System;

namespace Harvey.Ids.Utils
{
    public class DataInvalidException : System.Exception
    {
        public DataInvalidException(string message) : base(message)
        {
        }
    }
}
