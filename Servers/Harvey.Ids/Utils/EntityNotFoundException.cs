﻿using System;

namespace Harvey.Ids.Utils
{
    public class EntityNotFoundException : System.Exception
    {
        public EntityNotFoundException(string message) : base(message)
        {
        }
    }
}
