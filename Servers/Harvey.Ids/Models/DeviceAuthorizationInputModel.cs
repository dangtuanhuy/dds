using Harvey.Ids.Models;

namespace Harvey.Ids.Models
{
    public class DeviceAuthorizationInputModel : ConsentInputModel
    {
        public string UserCode { get; set; }
    }
}