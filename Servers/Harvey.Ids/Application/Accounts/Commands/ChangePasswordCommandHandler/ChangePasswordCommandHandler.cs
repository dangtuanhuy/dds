﻿using Harvey.Ids.Configs;
using Harvey.Ids.Domains;
using Harvey.Ids.Services.Activity;
using Harvey.Ids.Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Ids.Application.Accounts.Commands.ChangePasswordCommandHandler
{
    internal class ChangePasswordCommandHandler : IChangePasswordCommandHandler
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILoggingActivityService _activityService;
        private IOptions<ConfigurationRabbitMq> _configurationRabbitMq;
        private const string ChangePassWord = "Password has changed";

        public ChangePasswordCommandHandler(UserManager<ApplicationUser> userManager,
            ILoggingActivityService activityService,
            IOptions<ConfigurationRabbitMq> configurationRabbitMq)
        {
            _userManager = userManager;
            _activityService = activityService;
            _configurationRabbitMq = configurationRabbitMq;
        }

        public async Task<IdentityResult> ExecuteAsync(string userId, ChangePasswordCommand changePasswordCommand)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(f => f.Id== userId && f.IsActive);
            if (user == null)
            {
                return null;
            }
            var result = await _userManager.ChangePasswordAsync(user, changePasswordCommand.CurrentPassword, changePasswordCommand.NewPassword);

            return result;
        }
    }
}
