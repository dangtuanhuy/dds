﻿using Harvey.Ids.Data;

namespace Harvey.Ids.Application.User.Queries.GetAllStaff
{
    public class GetAllStaffModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Pin { get; set; }
        public bool IsActive { get; set; }
        public UserType UserType { get; set; }
        public string PhoneNumber { get; set; }
    }
}
