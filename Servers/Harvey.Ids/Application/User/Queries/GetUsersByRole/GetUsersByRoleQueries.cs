﻿using AutoMapper;
using Harvey.Ids.Application.User.Queries.GetAllUser;
using Harvey.Ids.Domains;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Ids.Application.User.Queries.GetUsersByRole
{
    public class GetUsersByRoleQueries: IGetUsersByRoleQueries
    {
        private readonly HarveyIdsDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        public GetUsersByRoleQueries(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, HarveyIdsDbContext dbContext)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _dbContext = dbContext;
        }
        public List<GetAllUserModel> Excecute(string role)
        {
            var roleId = _roleManager.Roles.Where(x => x.Name == role).FirstOrDefault().Id;
            var userIds = _dbContext.UserRoles.Where(x => x.RoleId == roleId).Select(x => x.UserId).ToList();
            var users = _userManager.Users.Where(x => userIds.Contains(x.Id)).ToList();

            return Mapper.Map<List<GetAllUserModel>>(users);
        }
    }
}
