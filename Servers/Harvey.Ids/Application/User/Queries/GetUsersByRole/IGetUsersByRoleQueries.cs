﻿using Harvey.Ids.Application.User.Queries.GetAllUser;
using System.Collections.Generic;

namespace Harvey.Ids.Application.User.Queries.GetUsersByRole
{
    public interface IGetUsersByRoleQueries
    {
        List<GetAllUserModel> Excecute(string role);
    }
}
