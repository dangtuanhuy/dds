﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Harvey.Ids.Migrations.IdentitfiedEventMigrations
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<IdentifiedEventDbContext>
    {
        public IdentifiedEventDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<IdentifiedEventDbContext>();
            var connectionString = "Server=localhost;port=5432;Database=harvey_identified_event;UserId=postgres;Password=123456";
            builder.UseNpgsql(connectionString);
            return new IdentifiedEventDbContext(builder.Options);
        }
    }
}
