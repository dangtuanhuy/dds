﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Ids.ViewModels.Accounts
{
    public class ChangePinViewModel
    {
        [Required]
        public string CurrentPin { get; set; }
        [Required]
        public string NewPin { get; set; }
        [Required]
        [Compare("NewPin", ErrorMessage = "The new Pin and confirmation Pin do not match.")]
        public string ConfirmPin { get; set; }
    }
}
