﻿using AutoMapper;
using Harvey.Ids.Application.User.Queries.GetAllStaff;
using Harvey.Ids.Application.User.Queries.GetAllUser;
using Harvey.Ids.Application.User.Queries.GetUsersByRole;
using Harvey.Ids.Data;
using Harvey.Ids.Domains;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.Ids.Api.Controllers
{
    [Route("api/users")]
    //[Authorize]
    public class UsersController : Controller
    {
        private readonly IGetAllUserQueries _getAllUser;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGetUsersByRoleQueries _getUsersByRole;
        public UsersController(
            IGetAllUserQueries getAllUser,
            UserManager<ApplicationUser> userManager,
            IGetUsersByRoleQueries getUsersByRole
            )
        {
            _getAllUser = getAllUser;
            _userManager = userManager;
            _getUsersByRole = getUsersByRole;
        }

        [HttpGet]
        [AllowAnonymous]
        public List<GetAllUserModel> GetAll([FromQuery]string role)
        {
            if (role == null)
            {
                var users = _getAllUser.Excecute();
                return users;
            }
            else
            {
                var users = _getUsersByRole.Excecute(role);
                return users;
            }
        }

        [Route("customers")]
        public List<GetAllUserModel> GetAllCustomer()
        {
            var result = _userManager.Users.Where(x => x.UserType == UserType.Member);
            return Mapper.Map<List<GetAllUserModel>>(result);
        }

        [Route("staffs")]
        public List<GetAllStaffModel> GetAllStaff(DateTime? lastSync)
        {
            IQueryable<ApplicationUser> query = null;
            if (lastSync == null)
            {
                query = _userManager.Users.Where(x => x.UserType == UserType.Staff || x.UserType == UserType.AdminStaff);
            }
            else
            {
                DateTime date = lastSync.Value;
                query = _userManager.Users.Where(x => (x.UserType == UserType.Staff || x.UserType == UserType.AdminStaff)
                                                            && (x.CreatedDate > date || x.UpdatedDate > date));
            }

            return Mapper.Map<List<GetAllStaffModel>>(query);
        }
    }
}
