$(function () {
    $(document).ready(function () {
        let editRoleAssignment = $('#edit-user-role-assignment');
        if (editRoleAssignment) {
            editRoleAssignment.select2();
            let selectRolesTag = $('#select-role');
            let selectRolesVal = selectRolesTag.val();
            if (selectRolesVal) {
                let userRoleIds = JSON.parse(selectRolesVal);
                editRoleAssignment.val(userRoleIds).trigger('change');
            }
        }
    });
});

