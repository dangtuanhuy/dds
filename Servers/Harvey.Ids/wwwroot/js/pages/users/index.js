$(function () {
    $(document).ready(function () {
        $(".anchorDetail").click(function () {
            var TeamDetailPostBackURL = '/User/Delete';
            var $buttonClicked = $(this);
            var id = $buttonClicked.attr('data-id');
            var options = { "backdrop": "static", keyboard: true };
            $.ajax({
                type: "GET",
                url: TeamDetailPostBackURL,
                contentType: "application/json; charset=utf-8",
                data: { "Id": id },
                datatype: "json",
                success: function (data) {
                    $('#myModalContent').html(data);
                    $('#myModal').css("display", "block");
                    $('#myModal').ready(function () {
                        $('#user-delete-cancel').click(function () {
                            $('#myModal').css("display", "none");
                            $('#myModalContent').empty();
                        });
                    });
                },
                error: function () {
                    alert("Dynamic content load failed.");
                }
            });
        });
    });
});

