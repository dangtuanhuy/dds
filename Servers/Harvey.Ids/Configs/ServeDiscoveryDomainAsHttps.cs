﻿using IdentityServer4.Extensions;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Harvey.Ids.Configs
{

    public class ServeDiscoveryDomainAsHttps
    {
        private readonly RequestDelegate _next;

        public ServeDiscoveryDomainAsHttps(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var request = context.Request;
            if (!ShouldHandle(Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(request)))
            {
                return;
            }
            var requested = request.Scheme + Uri.SchemeDelimiter + request.Host + ":" + request.Host.Port;
            requested = requested.Replace("http", "https");
            context.SetIdentityServerOrigin(requested);
            await _next(context);
        }


        private bool ShouldHandle(string url)
        {
            return url.EndsWith("/.well-known/openid-configuration/jwks")
                || url.EndsWith("/connect/authorize")
                || url.EndsWith("/connect/token")
                || url.EndsWith("/connect/userinfo")
                || url.EndsWith("/connect/endsession")
                || url.EndsWith("/connect/checksession")
                || url.EndsWith("/connect/revocation")
                || url.EndsWith("/connect/introspect")
                || url.EndsWith("/connect/deviceauthorization");
        }
    }
}
