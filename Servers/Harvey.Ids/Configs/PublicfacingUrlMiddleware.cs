﻿using IdentityServer4.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Ids.Configs
{
    public class PublicfacingUrlMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly string _publicFacingUri;
        private readonly ILogger<PublicfacingUrlMiddleware> _logger;

        public PublicfacingUrlMiddleware(RequestDelegate next, string publicFacingUri, ILogger<PublicfacingUrlMiddleware> logger)
        {
            _next = next;
            _publicFacingUri = publicFacingUri;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var request = context.Request;
            if (ShouldIgnore(Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(request)))
            {
                _logger.LogWarning($"a-a-a: {Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(request)} - {Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(request)}");
                context.SetIdentityServerOrigin(_publicFacingUri);
                context.SetIdentityServerBasePath(request.PathBase.Value.TrimEnd('/'));
                await _next(context);
            }
            else
            {

                context.Request.Scheme = "https";
                context.SetIdentityServerBasePath(request.PathBase.Value.TrimEnd('/'));
                await _next.Invoke(context);
                //await _next.Invoke(context);
            }
        }

        private bool ShouldIgnore(string url)
        {
            return url.ToUpper().Contains("/ACCOUNT/LOGIN")
                || url.ToLower().Contains("/api/account/get-user-profile")
                || url.Contains("/api/account/GetRolesOfCurrentUser")
                || url.ToLower().Contains("/api/get-roles-current-user");
        }
    }
}
