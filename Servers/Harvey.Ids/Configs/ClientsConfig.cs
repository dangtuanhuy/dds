﻿using IdentityServer4.Models;
using System.Collections.Generic;
using static IdentityServer4.IdentityServerConstants;

namespace Harvey.Ids.Configs
{
    public static class ClientsConfig
    {
        public static readonly string HarveyMemberPage = "Harvey-member-page";
        public static readonly string HarveyAdministratorPage = "Harvey-administrator-page";
        public static readonly string HarveyStaffPage = "Harvey-staff-page";
        public const string HarveyRimsPage = "harvey-rims-page";

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    Enabled = true,
                    ClientId = HarveyMemberPage,
                    ClientName = HarveyMemberPage,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.Email,
                        StandardScopes.Phone,
                        "Harvey.CRMLoyalty.Api",
                        "Harvey.Activity.Api",
                        "Harvey.UserManagement.Api",
                        "roles"
                    },
                    RedirectUris = HarveyMemberPageUrls,
                    RequireConsent = false,
                    AccessTokenLifetime = 1300
                },
                new Client
                {
                    Enabled = true,
                    ClientId = HarveyAdministratorPage,
                    ClientName = HarveyAdministratorPage,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.Email,
                        StandardScopes.Phone,
                        "Harvey.CRMLoyalty.Api",
                        "Harvey.Activity.Api",
                        "Harvey.UserManagement.Api",
                        "Harvey.Notification.Api",
                        "harvey.rims.api",
                        "roles"
                    },
                    RequireConsent = false,
                    RedirectUris = HarveyAdministratorPageUrls,
                    AccessTokenLifetime = 1300
                },
                new Client
                {
                    Enabled = true,
                    ClientId = HarveyStaffPage,
                    ClientName = HarveyStaffPage,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.Email,
                        StandardScopes.Phone,
                        "Harvey.CRMLoyalty.Api",
                        "Harvey.Activity.Api",
                        "Harvey.UserManagement.Api",
                        "roles"
                    },
                    RequireConsent = false,
                    RedirectUris = HarveyStorePageUrls,
                    AccessTokenLifetime = 36000
                },
                new Client
                {
                    Enabled = true,
                    ClientId = HarveyRimsPage,
                    ClientName = HarveyRimsPage,
                    AccessTokenType = AccessTokenType.Reference,
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    AllowedScopes = {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.Email,
                        StandardScopes.Phone,
                        "harvey.rims.api",
                        "roles"
                    },
                    RequireConsent = false,
                    RedirectUris = HarveyRimsRedirectUris,
                    PostLogoutRedirectUris = HarveyRimsPostLogoutRedirectUris,
                    AllowedCorsOrigins = HarveyRimsRedirectUris,
                    AccessTokenLifetime = 36000
                },
                new Client
                {
                    ClientId = "device",
                    ClientName = "Device Flow Client",
                    AllowedGrantTypes = GrantTypes.DeviceFlow,
                    AccessTokenType = AccessTokenType.Reference,
                    AllowOfflineAccess = true,
                    AllowedScopes = {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.Email,
                        StandardScopes.Phone,
                        "harvey.rims.api",
                        "roles"
                    },
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                }
            };
        }

        public static List<string> HarveyMemberPageUrls => new List<string> {
            "http://localhost:4600",
            "http://localhost:50222",
            "http://178.128.212.67:50222",
            "https://crm.dev.retaildds.net:50222",
            "https://member.app.harvey.demo.retaildds.net",
            "https://member.app.dev.toyorgame.com.sg",
            "https://member.app.toyorgame.com.sg"
        };

        public static List<string> HarveyAdministratorPageUrls => new List<string> {
            "http://localhost:4100",
            "http://localhost:50221",
            "http://178.128.212.67:50221",
            "https://crm.dev.retaildds.net:50221",
            "https://admin.app.harvey.demo.retaildds.net",
            "https://admin.app.dev.toyorgame.com.sg",
            "https://admin.app.toyorgame.com.sg"

        };

        public static List<string> HarveyStorePageUrls => new List<string> {
            "http://localhost:4500",
            "http://localhost:50223",
            "http://178.128.212.67:50223",
            "https://crm.dev.retaildds.net:50223",
            "https://store.app.harvey.demo.retaildds.net",
            "https://store.app.dev.toyorgame.com.sg",
            "https://store.app.toyorgame.com.sg"
        };

        public static List<string> HarveyRimsRedirectUris => new List<string> {

            "http://localhost:4100/index.html",
            "http://localhost:4200/index.html",
            "http://localhost:4300/index.html",
            "http://localhost:4400/index.html",
            "http://192.168.70.170:4100/index.html",
            "http://192.168.70.170:4200/index.html",
            "http://192.168.70.170:4300/index.html",
            "http://192.168.70.170:4400/index.html",
            "http://192.168.70.171:4100/index.html",
            "http://192.168.70.171:4200/index.html",
            "http://192.168.70.171:4300/index.html",
            "http://192.168.70.171:4400/index.html",
            "http://178.128.212.67:4100/index.html",
            "http://178.128.212.67:4200/index.html",
            "http://178.128.212.67:4300/index.html",
            "http://178.128.212.67:4400/index.html",
            "https://pim.app.dev.retaildds.net/index.html",
            "https://po.app.dev.retaildds.net/index.html",
            "https://retail.app.dev.retaildds.net/index.html",
            "https://promotion.app.dev.retaildds.net/index.html",
            "https://pim.app.harvey.demo.retaildds.net/index.html",
            "https://po.app.harvey.demo.retaildds.net/index.html",
            "https://retail.app.harvey.demo.retaildds.net/index.html",
            "https://promotion.app.harvey.demo.retaildds.net/index.html",
            "https://pim.app.dev.toyorgame.com.sg/index.html",
            "https://po.app.dev.toyorgame.com.sg/index.html",
            "https://retail.app.dev.toyorgame.com.sg/index.html",
            "https://promotion.app.dev.toyorgame.com.sg/index.html",
            "https://pim.app.toyorgame.com.sg/index.html",
            "https://po.app.toyorgame.com.sg/index.html",
            "https://retail.app.toyorgame.com.sg/index.html",
            "https://promotion.app.toyorgame.com.sg/index.html"

        };

        public static List<string> HarveyRimsPostLogoutRedirectUris => new List<string> {
            "http://localhost:4100",
            "http://localhost:4200",
            "http://localhost:4300",
            "http://localhost:4400",
            "http://192.168.70.170:4100",
            "http://192.168.70.170:4200",
            "http://192.168.70.170:4300",
            "http://192.168.70.170:4400",
            "http://192.168.70.171:4100",
            "http://192.168.70.171:4200",
            "http://192.168.70.171:4300",
            "http://192.168.70.171:4400",
            "http://178.128.212.67:4100",
            "http://178.128.212.67:4200",
            "http://178.128.212.67:4300",
            "http://178.128.212.67:4400",
            "https://pim.app.dev.retaildds.net",
            "https://po.app.dev.retaildds.net",
            "https://retail.app.dev.retaildds.net",
            "https://promotion.app.dev.retaildds.net",
            "https://pim.app.harvey.demo.retaildds.net",
            "https://po.app.harvey.demo.retaildds.net",
            "https://retail.app.harvey.demo.retaildds.net",
            "https://promotion.app.harvey.demo.retaildds.net",
            "https://pim.app.dev.toyorgame.com.sg",
            "https://po.app.dev.toyorgame.com.sg",
            "https://retail.app.dev.toyorgame.com.sg",
            "https://promotion.app.dev.toyorgame.com.sg",
            "https://pim.app.toyorgame.com.sg",
            "https://po.app.toyorgame.com.sg",
            "https://retail.app.toyorgame.com.sg",
            "https://promotion.app.toyorgame.com.sg"
        };

        private static string[] GetRedirectUris(IEnumerable<string> clientUrls, string login, string silentRenew)
        {
            var ret = new List<string>();
            foreach (var clientUrl in clientUrls)
            {
                ret.Add(clientUrl + login);
                ret.Add(clientUrl + silentRenew);
            }
            return ret.ToArray();
        }
    }
}
