import * as fromRoot from 'src/app/shared/state/app.state';
import { TransferOutActions, TransferOutActionTypes } from './transfer-out.action';
import { AllocationTransactionModel } from '../../allocation-transaction/allocation-transaction.model';
import { InventoryTransactionTransferOutViewModel } from '../transfer-out.model';
import { SortDirection } from 'src/app/shared/base-model/paging-filter-criteria';

export interface State extends fromRoot.State {
  allocationTransactions: TransferOutState;
}

export interface TransferOutState {
  allocationTransactions: AllocationTransactionModel[];
  selectedAllocationTransactions: AllocationTransactionModel[];
  inventoryTransactionTransferOuts: InventoryTransactionTransferOutViewModel[];
  inventoryTransactionTransferOut: InventoryTransactionTransferOutViewModel;
  sortDirection: SortDirection;
  sortColumnName: String;
}

const initialState: TransferOutState = {
  allocationTransactions: [],
  selectedAllocationTransactions: [],
  inventoryTransactionTransferOuts: [],
  inventoryTransactionTransferOut: null,
  sortColumnName: '',
  sortDirection: SortDirection.Ascending
};

export const key = 'transfer_out_reducer';

export function reducer(
  state = initialState,
  action: TransferOutActions
): TransferOutState {
  switch (action.type) {
    case TransferOutActionTypes.GetAllocationTransactionsSuccess:
      return {
        ...state,
        allocationTransactions: [...action.payload.data]
      };
    case TransferOutActionTypes.GetTransferOutsSuccess:
      return {
        ...state,
        inventoryTransactionTransferOuts: action.payload.data
      };
    case TransferOutActionTypes.GetTransferOutSuccess:
      return {
        ...state,
        inventoryTransactionTransferOut: action.payload
      };
    case TransferOutActionTypes.GetAllocationTransactionByListIdsSuccess:
      return {
        ...state,
        selectedAllocationTransactions: action.payload
      };
    case TransferOutActionTypes.AddTransferOutSuccess:
      return {
        ...state
      };
    case TransferOutActionTypes.GetTransferOuts:
      return {
        ...state,
        sortColumnName: action.payload.sortColumn,
        sortDirection: action.payload.sortDirection
      };
    default:
      return state;
  }
}
