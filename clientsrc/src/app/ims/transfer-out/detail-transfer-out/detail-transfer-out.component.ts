import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductService } from 'src/app/shared/services/product.service';
import { StockTypeService } from 'src/app/shared/services/stock-type.service';
import * as transferOutActions from '../../transfer-out/state/transfer-out.action';
import * as transferOutSelector from '../../transfer-out/state/index';
import { Store, select } from '@ngrx/store';
import { takeWhile, take, first } from 'rxjs/operators';
import { InventoryTransactionTransferOutViewModel, InventoryTransactionTransferOutProductModel } from '../transfer-out.model';
import * as moment from 'moment';
import { ProductListModel } from '../../products/product';
import { StockTypeModel } from '../../allocation-transaction/allocation-transaction.model';
import { Guid } from 'src/app/shared/utils/guid.util';
import { ReportService } from 'src/app/shared/services/report.service';
import { environment } from 'src/environments/environment';

interface TransferOutDetails {
  fromWareHouse: string,
  toOutlet: string,
  transferOutNumber: string,
  createdDate: string,
}

interface TooltipValues {
  products: {
    name: string,
    variantName: string
  }[]
}

@Component({
  selector: 'app-detail-transfer-out',
  templateUrl: './detail-transfer-out.component.html',
  styleUrls: ['./detail-transfer-out.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetailTransferOutComponent extends ComponentBase {
  constructor(
    private store: Store<any>,
    private activeModal: NgbActiveModal,
    private productService: ProductService,
    private stockTypeService: StockTypeService,
    private reportService: ReportService,
    public injector: Injector
  ) {
    super(injector);
  }
  public componentActive = true;
  public isProductsLoading = true;
  public isStockTypesLoading = true;
  public products: Array<ProductListModel> = [];
  public stockTypes: Array<StockTypeModel> = [];
  public inventoryTransactionTransferOut: InventoryTransactionTransferOutViewModel;
  public inventoryTransactionTransferOuts: Array<InventoryTransactionTransferOutViewModel> = [];
  public inventoryTransactionTransferOutProducts: Array<InventoryTransactionTransferOutProductModel> = [];
  public tooltipValues: TooltipValues = {
    products: []
  };
  public transferOutDetails = {} as TransferOutDetails;

  onInit() {
    this.getStockTypes();
    this.handleSubscription(this.store.pipe(select(transferOutSelector.getSelectedItem), takeWhile(() => this.componentActive))
      .subscribe(
        (id: string) => {
          if (id == null) {
            return;
          }
          this.tooltipValues.products = [];
          this.getInventoryTransaction(id);
        }
      ));
  }

  getInventoryTransaction(id: string) {
    this.store.dispatch(new transferOutActions.GetTransferOut(id));
    this.store.pipe(select(transferOutSelector.GetTransferOut), takeWhile(() => this.componentActive))
      .subscribe((inventoryTransactionTransferOut: InventoryTransactionTransferOutViewModel) => {
        if (inventoryTransactionTransferOut == null) {
          return;
        }

        inventoryTransactionTransferOut.createdDate =
          moment.utc(inventoryTransactionTransferOut.createdDate).format('YYYY-MM-DD');

        this.transferOutDetails.fromWareHouse = inventoryTransactionTransferOut.fromLocation;
        this.transferOutDetails.toOutlet = inventoryTransactionTransferOut.toLocation;
        this.transferOutDetails.transferOutNumber = inventoryTransactionTransferOut.transferNumber;
        this.transferOutDetails.createdDate = inventoryTransactionTransferOut.createdDate;
        this.inventoryTransactionTransferOut = inventoryTransactionTransferOut;

        this.inventoryTransactionTransferOutProducts = [];

        inventoryTransactionTransferOut.inventoryTransactionTransferProducts.forEach(item => {
          const inventoryTransactionTransferOutProduct: InventoryTransactionTransferOutProductModel = {} as InventoryTransactionTransferOutProductModel;
          if (item.productId && item.productId !== Guid.empty()) {
            inventoryTransactionTransferOutProduct.id = item.id;
            inventoryTransactionTransferOutProduct.productId = item.productId !== Guid.empty() ? item.productId : null;
            inventoryTransactionTransferOutProduct.productName = item.productName !== null ? item.productName : null;
            inventoryTransactionTransferOutProduct.variantId = item.variantId !== Guid.empty() ? item.variantId : null;
            inventoryTransactionTransferOutProduct.stockTypeId = Guid.empty();
            inventoryTransactionTransferOutProduct.quantity = item.quantity;
            inventoryTransactionTransferOutProduct.stockTransactionRefId = item.stockTransactionRefId;
            inventoryTransactionTransferOutProduct.transactionRefId = item.transactionRefId;
            inventoryTransactionTransferOutProduct.variants = item.variants;            

            this.inventoryTransactionTransferOutProducts.push(inventoryTransactionTransferOutProduct);
            inventoryTransactionTransferOutProduct.isVariantsLoading = true;
          }
          else {
            inventoryTransactionTransferOutProduct.isVariantsLoading = false;
          }
        })

        this.tooltipValues.products = this.inventoryTransactionTransferOutProducts.map(x => {
          return {
            name: x.productName,
            variantName: x.variants[0].name
          }
        });

      })
  }

  onDestroy() {
    this.tooltipValues.products = [];
  }

  onPrintTransfer() {
    const transferStatementRequest: InventoryTransactionTransferOutViewModel = {
      id: this.inventoryTransactionTransferOut.id,
      fromLocationId: this.inventoryTransactionTransferOut.fromLocationId,
      toLocationId: this.inventoryTransactionTransferOut.toLocationId,
      transferNumber: this.inventoryTransactionTransferOut.transferNumber,
      createdDate: this.inventoryTransactionTransferOut.createdDate,
      fromLocation: this.inventoryTransactionTransferOut.fromLocation,
      toLocation: this.inventoryTransactionTransferOut.toLocation,
      inventoryTransactionRefId: Guid.empty(),
      inventoryTransactionTransferProducts: this.inventoryTransactionTransferOutProducts
    };

    const redirectWindow = window.open(environment.app.ims.url + '/report-loading', '_blank');
    this.reportService.printTransferOutStatement(transferStatementRequest).subscribe(
      data => {
        if (data.status === 1) {
          const reportRedirect: any = ((environment.app.report.apiUrl + '/reports?' + data.endPoint).replace(/["]/g, ''));
          redirectWindow.location = reportRedirect;
        } else {
          const reportErrorRedirect: any = ((environment.app.ims.url + '/report-error').replace(/["]/g, ''));
          redirectWindow.location = reportErrorRedirect;
        }
      }
    );
  }
  private getStockTypes() {
    this.isStockTypesLoading = true;
    this.stockTypeService.getAllWithoutPaging().subscribe(res => {
      this.stockTypes = res;
      this.isStockTypesLoading = false;
    });
  }

  onClose(): void {    
    this.activeModal.close('closed');
  }

  onDismiss(reason: String): void {
    this.activeModal.dismiss(reason);
  }

}
