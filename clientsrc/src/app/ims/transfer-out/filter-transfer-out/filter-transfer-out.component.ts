import { Component, ViewEncapsulation, Injector, Output, EventEmitter } from '@angular/core';
import { ActionType } from 'src/app/shared/constant/action-type.constant';
import { Store, select } from '@ngrx/store';
import { takeWhile } from 'rxjs/operators';
import { Button } from 'src/app/shared/base-model/button.model';
import * as transferOutActions from '../../transfer-out/state/transfer-out.action';
import * as transferOutSelector from '../../transfer-out/state/index';
import * as fromAuths from '../../../shared/components/auth/state/index';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { UserDefinedColumnSetting } from 'src/app/shared/base-model/user-defined-column-setting.model';
import { PagingFilterCriteria } from 'src/app/shared/base-model/paging-filter-criteria';
import { environment } from 'src/environments/environment';
import {
  AllocationTransactionStatus,
  AllocationTransactionViewModel
} from '../../allocation-transaction/allocation-transaction.model';
import { LocationType } from '../../locations/location.model';
import { LocationService } from 'src/app/shared/services/location.service';
import { AllocationTransactionByListIdModel, TransferOutByAllocationRequestModel } from '../transfer-out.model';
import * as moment from 'moment';
import * as listViewManagementActions from 'src/app/shared/components/list-view-management/state/list-view-management.actions';
import { NotificationService } from 'src/app/shared/services/notification.service';

interface DropDownItemModel {
  id: string;
  itemName: string;
}

@Component({
  selector: 'app-filter-transfer-out',
  templateUrl: './filter-transfer-out.component.html',
  styleUrls: ['./filter-transfer-out.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FilterTransferOutComponent extends ComponentBase {
  @Output() onclickBackToListingButton: EventEmitter<any> = new EventEmitter();
  constructor(
    private store: Store<any>,
    private locationService: LocationService,
    public injector: Injector,
    public notificationService: NotificationService
  ) {
    super(injector);
  }

  public pageSize = 10;
  public addSuccessMessage = '';
  public updateSuccessMessage = '';
  public deleteSuccessMessage = '';

  public componentActive = true;
  public datasource: Array<AllocationTransactionViewModel> = [];
  public selectItems: Array<string> = [];
  public listButton: Button[] = [];
  public title = '';
  public actionType = ActionType.dialog;
  public selectedId = null;
  public userDefinedColumnSetting: UserDefinedColumnSetting;
  public isHiddenSearchBox = true;
  public isMultiSelect = true;
  public isShowTransferOut = false;
  public isAddTransferOut = false;

  public isLocationsLoading = true;
  public isShowNextButton = false;
  public isDisableNextbutton = true;
  public fromLocationId = null;
  public toLocationId = null;
  public fromLocationName = '';
  public toLocationName = '';
  public filteredFromDate = { year: new Date().getUTCFullYear(), month: new Date().getUTCMonth() + 1, day: new Date().getUTCDate() };
  public filteredToDate = { year: new Date().getUTCFullYear(), month: new Date().getUTCMonth() + 1, day: new Date().getUTCDate() };
  public wareHousesPlaceholder = 'Select WareHouse';
  public outletsPlaceholder = 'Select Outlet';
  public dropDownListWareHouse: DropDownItemModel[] = [];
  public dropDownListOutlet: DropDownItemModel[] = [];
  public selectedWareHouses = [];
  public selectedOutlets = [];
  public isDisableFilterButton: boolean = false;
  public isDisableDropdown: boolean;

  onInit() {
    this.store.dispatch(new listViewManagementActions.InitEmptyPage());

    this.handleSubscription(this.store.pipe(
      select(fromAuths.getUserId), takeWhile(() => this.componentActive))
      .subscribe(
        (id: string) => {
          if (id == null) {
            return;
          }
          this.userDefinedColumnSetting = new UserDefinedColumnSetting(
            `${id}_UserDefinedColumnsFilterAllocationTransactionTransferOut`,
            'name,description',
            environment.app.ims.apiUrl
          );
          this.datasource = [];
          this.getLocations();
        }
      ));
    this.handleSubscription(this.store.pipe(
      select(transferOutSelector.getSelectedItems), takeWhile(() => this.componentActive))
      .subscribe(selectedItems => {
        this.selectItems = selectedItems;
        this.isDisableNextbutton = !(this.selectItems.length > 0);
      }));
  }

  onDestroy() {
    this.store.dispatch(new listViewManagementActions.InitEmptyPage());
  }

  public onClickFilter() {
    this.selectItems = [];
    this.store.dispatch(new listViewManagementActions.InitEmptyPage());
    this.datasource = [];
    const page = 1;
    this.getAllocationTransactions(page);
  }

  changeSelectedPage(page: number) {
    this.datasource = [];
    const nextPage = page + 1;
    this.getAllocationTransactions(nextPage);
  }

  getTransferOutByAllocationRequestModel() {
    const filteredFromDate = this.filteredFromDate !== null && this.filteredFromDate !== undefined
      ? this.filteredFromDate.year + '-' + this.filteredFromDate.month + '-' + this.filteredFromDate.day
      : Date.now();
    const filteredToDate = this.filteredToDate !== null && this.filteredToDate !== undefined
      ? this.filteredToDate.year + '-' + this.filteredToDate.month + '-' + this.filteredToDate.day
      : Date.now();
    const transferOutByAllocationRequestModel: TransferOutByAllocationRequestModel = {
      fromLocationIds: this.selectedWareHouses,
      toLocationIds: this.selectedOutlets,
      fromDate: moment.utc(filteredFromDate).format('YYYY-MM-DD'),
      toDate: moment.utc(filteredToDate).format('YYYY-MM-DD')
    };
    return transferOutByAllocationRequestModel;
  }

  getAllocationTransactions(page: number) {
    const transferOutByAllocationRequestModel = this.getTransferOutByAllocationRequestModel();
    this.store.dispatch(new transferOutActions.GetAllocationTransactions(new PagingFilterCriteria(page, this.pageSize),
      transferOutByAllocationRequestModel));
    this.store.pipe(select(transferOutSelector.getAllocationTransactions), takeWhile(() => this.componentActive))
      .subscribe((allocationTransactions: Array<AllocationTransactionViewModel>) => {
        this.datasource = allocationTransactions.map(item => {
          item.status = this.getAllocationTransactionStatus(item.status);
          item.fromLocation = this.getWareHouseName(item.fromLocationId);
          item.toLocation = this.getOutletName(item.toLocationId);
          item.deliveryDate = moment.utc(item.deliveryDate).format('YYYY-MM-DD');
          const allocationTransaction = new AllocationTransactionViewModel(item);
          return allocationTransaction;
        });
      });
  }

  getWareHouseName(fromLocationId: string) {
    const wareHouse = this.dropDownListWareHouse.find(x => x.id === fromLocationId);
    if (wareHouse) {
      return wareHouse.itemName;
    } else {
      return null;
    }
  }
  getOutletName(toLocationId: string) {
    const outLet = this.dropDownListOutlet.find(x => x.id === toLocationId);
    if (outLet) {
      return outLet.itemName;
    } else {
      return null;
    }
  }

  onclickSubtmitButton(event) {
    this.isShowTransferOut = event;
    this.datasource = null;
    this.selectedOutlets = [];
    this.selectedWareHouses = [];
    this.filteredFromDate = { year: new Date().getUTCFullYear(), month: new Date().getUTCMonth() + 1, day: new Date().getUTCDate() };
    this.filteredToDate = { year: new Date().getUTCFullYear(), month: new Date().getUTCMonth() + 1, day: new Date().getUTCDate() };
    this.isAddTransferOut = false;
    this.store.dispatch(new listViewManagementActions.InitEmptyPage());
    this.store.dispatch(new listViewManagementActions.ChangeFilter([]));
  }

  onclickBackButton(event) {
    this.isShowTransferOut = event;
    this.isAddTransferOut = false;
  }

  onClickNextButton() {
    let selectedData = this.datasource.filter(x => this.selectItems.find(s => s === x.id));
    let isValid = this.validateOnNextButton(selectedData);

    if (isValid) {
      const allocationTransactionByListIdModel: AllocationTransactionByListIdModel = {
        allocationTransactionListId: this.selectItems,
        allocationTransactions: []
      };
      let data = selectedData[0];
      this.fromLocationId = data.fromLocationId;
      this.toLocationId = data.toLocationId;
      this.fromLocationName = data.fromLocation;
      this.toLocationName = data.toLocation;
      this.isDisableDropdown = true;
      this.isShowTransferOut = true;
      this.isAddTransferOut = true;
      this.store.dispatch(new transferOutActions.GetAllocationTransactionByListIds(allocationTransactionByListIdModel));
    }
    else {
      this.notificationService.warning('Select single location only');
    }
  }

  public onClickBackToListingButton() {
    this.onclickBackToListingButton.emit(false);
  }

  private getLocations() {
    this.isLocationsLoading = true;
    this.locationService.getByType(LocationType.wareHouse).subscribe(res => {
      this.dropDownListWareHouse = this.setDropdownDataSource(res);
    });
    this.locationService.getByType(LocationType.store).subscribe(res => {
      this.dropDownListOutlet = this.setDropdownDataSource(res);
      this.isLocationsLoading = false;
    });
  }

  getAllocationTransactionStatus(status: any) {
    return Object.values(AllocationTransactionStatus).includes(+status)
      ? Object.keys(AllocationTransactionStatus).find(function (item, key) { return key === (+status - 1); })
      : status;
  }

  handleSelectedDropDownListWareHouse(selectedItems: DropDownItemModel[]) {
    this.selectedWareHouses = selectedItems.map(x => x.id);;
    this.isDisableFilterButton = !this.selectedWareHouses.length && !this.selectedOutlets.length;
  }

  handleSelectedDropDownOutlet(selectedItems: DropDownItemModel[]) {
    this.selectedOutlets = selectedItems.map(x => x.id);
    this.isDisableFilterButton = !this.selectedWareHouses.length && !this.selectedOutlets.length;
  }

  private validateOnNextButton(data) {
    let isMultipleFromLocation = data.find(x => x.fromLocationId !== data[0].fromLocationId);
    let isMultipleToOutLet = data.find(x => x.toLocationId !== data[0].toLocationId);
    return !isMultipleFromLocation && !isMultipleToOutLet;
  }

  private setDropdownDataSource(res: any) {
    return res.map(item => ({
      id: item.id,
      itemName: item.name
    }));
  }
}
