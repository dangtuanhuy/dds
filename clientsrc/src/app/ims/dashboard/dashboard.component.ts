import { Component, ViewEncapsulation, Injector } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent extends ComponentBase {

  constructor(public injector: Injector) {
    super(injector);
  }

  onInit() {
  }

  onDestroy() {
  }
}
