import { Component, ViewEncapsulation, Injector } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { PurchaseOrderSummaryViewModel } from './purchase-order.model';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { ApprovalStatusEnum } from 'src/app/shared/constant/approval.constant';
import { environment } from 'src/environments/environment';
import { Store, select } from '@ngrx/store';
import * as fromAuths from '../../../shared/components/auth/state/index';
import { takeWhile } from 'rxjs/operators';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PurchaseOrderComponent extends ComponentBase {

  public title = 'Purchase Order';
  public poBlockItems: PurchaseOrderSummaryViewModel[] = [];
  public purchaseUrl = `${environment.app.purchaseOrder.url}/purchase-orders`;
  public isLoading: Boolean = true;
  public userRole: any;
  public componentActive = true;
  public rolePurchase = ['Purchaser', 'PurchaseManager', 'InventoryManager', 'Administrator'];
  constructor(private dashboardService: DashboardService,
    private notificationService: NotificationService,
    private store: Store<any>,
    public injector: Injector) {
    super(injector);
  }

  onInit() {
    this.handleSubscription(this.store.pipe(
      select(fromAuths.getUserRole), takeWhile(() => this.componentActive))
      .subscribe((role: any) => {
        this.userRole = role ? role : '';
      }));
    this.getPOSummary();
  }

  onDestroy() {
  }

  private getPOSummary() {
    this.isLoading = true;
    this.dashboardService.getPurchaseOrderSummary().subscribe(res => {
      if (res) {
        res.forEach(x => x.icon = this.getIcon(x.status));
        this.poBlockItems = res;
      }
      this.isLoading = false;
    });
  }

  private getIcon(itemStatus: ApprovalStatusEnum) {
    let icon = '';
    switch (itemStatus) {
      case ApprovalStatusEnum.approved:
        icon = '<i class="fas fa-check fa-5x text-primary"></i>';
        break;
      case ApprovalStatusEnum.confirmed:
        icon = '<i class="fas fa-check-double fa-5x text-success"></i>';
        break;
      case ApprovalStatusEnum.pending:
        icon = '<i class="far fa-clock fa-5x text-warning"></i>';
        break;
      case ApprovalStatusEnum.rejected:
        icon = '<i class="fas fa-times fa-5x text-danger"></i>';
        break;
      default:
        icon = '<i class="far fa-clipboard fa-5x"></i>';
        break;
    }
    return icon;
  }

  public onClickPurchaseOrderSummaryItem(item?: PurchaseOrderSummaryViewModel) {
    if (!this.checkRolePurchase(this.userRole)) {
      this.notificationService.error('Access Denied');
      return;
    }
    if (item) {
      window.open(`${this.purchaseUrl}?purchaseOrderApprovalStatus=${item.status}`);
    } else {
      window.open(this.purchaseUrl);
    }
  }

  private checkRolePurchase(role: string) {
    return this.rolePurchase.some(x => this.userRole.includes(x));
  }
}
