import { ApprovalStatusEnum } from 'src/app/shared/constant/approval.constant';

export class PurchaseOrderSummaryViewModel {
    public statusTitle: string;
    public status: ApprovalStatusEnum;
    public countNumber: number;
    public icon: string;
}
