import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { NewProductItemDashboardViewModel } from './new-products.model';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import * as moment from 'moment';

@Component({
  selector: 'app-new-products',
  templateUrl: './new-products.component.html',
  styleUrls: ['./new-products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewProductsComponent extends ComponentBase {

  public title = 'New Products';
  public newProducts: NewProductItemDashboardViewModel[] = [];
  public isLoading = true;
  public pageIndex = 0;
  public numberItemsPerPage = 10;
  public totalItems = 0;
  constructor(private dashboardService: DashboardService,
    public injector: Injector) {
    super(injector);
  }

  onInit() {
    this.getProductsByPagination(this.pageIndex, this.numberItemsPerPage);
  }

  onDestroy() {
  }

  private getProductsByPagination(pageNumber: number, pageSize: number) {
    this.isLoading = true;
    this.dashboardService.getNewProducts(pageNumber + 1, pageSize).subscribe(res => {
      if (res) {
        res.data.forEach(item => {
          if (item.createdDate && !moment.utc(item.createdDate, 'DD/MM/YYYY HH:mm A', true).local().isValid()) {
            item.createdDate = moment.utc(item.createdDate).local().format('DD/MM/YYYY HH:mm A');
          }
        });
        this.newProducts = res.data;
        this.totalItems = res.totalItems;
      }
      this.isLoading = false;
    });
  }

  public setPage(pageInfo: { offset: number; }) {
    this.pageIndex = pageInfo.offset;
    this.getProductsByPagination(this.pageIndex, this.numberItemsPerPage);
  }
}
