export class NewProductItemDashboardViewModel {
    public productId: string;
    public productName: string;
    public productDescription: string;
    public categoryName: string;
    public createdDate: string;
}
