export class TopProductBySalesViewModel {
    public productName: string;
    public variantName: string;
    public skuCode: string;
    public totalQuantity: number;
    public totalPrice: string;
}
