import { Component, OnInit, Injector } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { TopProductBySalesViewModel } from './top-sale-product.model';
import * as moment from 'moment';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-top-sale-product',
  templateUrl: './top-sale-product.component.html',
  styleUrls: ['./top-sale-product.component.scss']
})
export class TopSaleProductComponent extends ComponentBase {

  public title = 'Top Products By Sales';
  public fromDate = new Date;
  public toDate = new Date;
  public isLoading: Boolean = true;
  public topProductBySales: TopProductBySalesViewModel[] = [];
  get maxDate() {
    const today = new Date;
    return { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() };
  }

  constructor(private dashboardService: DashboardService,
    private notificationService: NotificationService,
    public injector: Injector) {
    super(injector);
  }

  onInit() {
  }

  onDestroy() {
  }

  public onClickFilter() {
    if ((!this.fromDate && !this.toDate) || this.fromDate > this.toDate) {
      this.notificationService.error('Please enter correct from date, to date!');
      return;
    }

    this.isLoading = true;
    const fromDate = moment.utc(this.fromDate).startOf('day').format('YYYY-MM-DD HH:mm:ss');
    const toDate = moment.utc(this.toDate).endOf('day').format('YYYY-MM-DD HH:mm:ss');

    this.dashboardService.getTopProductBySales(fromDate, toDate).subscribe(res => {
      if (res) {
        this.topProductBySales = res;
      }
      this.isLoading = false;
    });
  }

}
