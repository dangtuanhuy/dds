import * as fromRoot from 'src/app/shared/state/app.state';
import { ActivityActions, ActivityActionTypes } from './activity.action';
import { ActivityModel } from '../activity.model';
import { User } from 'src/app/shared/base-model/user.model';
import { SortDirection } from 'src/app/shared/base-model/paging-filter-criteria';

export interface State extends fromRoot.State {
  activities: ActivityState;
}

export interface ActivityState {
  activities: ActivityModel[];
  activity: ActivityModel;
  users: User[];
  sortDirection: SortDirection;
  sortColumnName: String;
}

const initialState: ActivityState = {
  activities: [],
  activity: null,
  users: [],
  sortColumnName: '',
  sortDirection: SortDirection.Ascending
};

export const key = 'activities_reducer';

export function reducer(
  state = initialState,
  action: ActivityActions
): ActivityState {
  switch (action.type) {
    case ActivityActionTypes.GetActivitiesSuccess:
      return {
        ...state,
        activities: action.payload.data
      };
    case ActivityActionTypes.GetUsersSuccess:
      return {
        ...state,
        users: action.payload
      };
    case ActivityActionTypes.GetActivities:
      return {
        ...state,
        sortColumnName: action.payload.paging.sortColumn,
        sortDirection: action.payload.paging.sortDirection
      };
    default:
      return state;
  }
}
