import * as fromRoot from 'src/app/shared/state/app.state';
import { CategoryActions, CategoryActionTypes } from './category.action';
import { CategoryModel } from '../category.model';
import { SortDirection } from 'src/app/shared/base-model/paging-filter-criteria';

export interface State extends fromRoot.State {
    categories: CategoryState;
}

export interface CategoryState {
    categories: CategoryModel[];
    category: CategoryModel;
    sortDirection: SortDirection;
    sortColumnName: String;
}

const initialState: CategoryState = {
    categories: [],
    category: null,
    sortColumnName: '',
    sortDirection: SortDirection.Ascending
};

export const key = 'categories_reducer';

export function reducer(
    state = initialState,
    action: CategoryActions
): CategoryState {
    switch (action.type) {
        case CategoryActionTypes.GetCategoriesSuccess:
            return {
                ...state,
                categories: [...action.payload.data]
            };
        case CategoryActionTypes.GetCategorySuccess:
            return {
                ...state,
                category: action.payload
            };
        case CategoryActionTypes.AddCategorySuccess:
            return {
                ...state,
                categories: [...state.categories, action.payload]
            };
        case CategoryActionTypes.AddCategoryFail: {
            return {
                ...state
            };
        }
        case CategoryActionTypes.UpdateCategorySuccess:
            const updatedCategories = state.categories.map(
                item => action.payload.id === item.id ? action.payload : item);
            return {
                ...state,
                categories: updatedCategories
            };
        case CategoryActionTypes.UpdateCategoryFail: {
            return {
                ...state
            };
        }
        case CategoryActionTypes.DeleteCategorySuccess:
            return {
                ...state,
                categories: state.categories.filter(
                    category => category.id !== action.payload
                )
            };
        case CategoryActionTypes.DeleteCategoryFail:
            return {
                ...state
            };
        case CategoryActionTypes.GetCategories:
            return {
                ...state,
                sortColumnName: action.payload.sortColumn,
                sortDirection: action.payload.sortDirection
            };
        default:
            return state;
    }
}
