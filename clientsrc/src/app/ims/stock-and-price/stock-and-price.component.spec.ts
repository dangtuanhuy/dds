import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAndPriceComponent } from './stock-and-price.component';

describe('StockAndPriceComponent', () => {
  let component: StockAndPriceComponent;
  let fixture: ComponentFixture<StockAndPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockAndPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockAndPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
