import { LocationType } from '../locations/location.model';

export class StockAndPriceViewModel {
    public totalItems: number;
    public items: StockAndPriceItemViewModel[];
}

export class StockAndPriceItemViewModel {
    public productId: string;
    public productName: string;
    public storeItems: StockAndPriceStoreItemViewModel[];
    public warehouseItems: StockAndPricesWarehouseItemViewModel[];
    public isLoadingData: boolean;
    public isExpand: boolean;
}

export class StockAndPriceStoreItemViewModel {
    public referenceId: string;
    public variantName: string;
    public skuCode: string;
    public locationName: string;
    public categoryName: string;
    public listPrice: number;
    public memberPrice: number;
    public staffPrice: number;
    public quantity: number;
}

export class StockAndPricesWarehouseItemViewModel {
    public referenceId: string;
    public variantName: string;
    public skuCode: string;
    public locationName: string;
    public categoryName: string;
    public quantity: number;
}

export class StockAndPriceProductViewModel {
    public productId: string;
    public productName: string;
}

export class StockAndPriceLevels {
    public locationType: LocationType;
    public locationId: string;
    public locationName: string;
    public productId: string;
    public productName: string;
    public variantId: string;
    public variantName: string;
    public skuCode: string;
    public quantity: number;
    public categoryName: string;
    public listPrice: number;
    public memberPrice: number;
    public staffPrice: number;
}
