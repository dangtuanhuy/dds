import {
    reducer as listViewManagementReducer,
    ListViewManagementState
} from 'src/app/shared/components/list-view-management/state/list-view-management.reducer';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StockAndPriceComponent } from './stock-and-price.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BootstrapModule } from 'src/app/shared/bootstrap.module';

const stockAndPriceRoutes: Routes = [{ path: '', component: StockAndPriceComponent }];

export interface IDashboardState {
    listviewmanagement_reducer: ListViewManagementState;
}

export const reducers: ActionReducerMap<IDashboardState> = {
    listviewmanagement_reducer: listViewManagementReducer
};

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(stockAndPriceRoutes),
        StoreModule.forFeature(`stock-and-price`, reducers),
        ReactiveFormsModule,
        NgSelectModule,
        FormsModule,
        NgxDatatableModule,
        BootstrapModule
    ],
    declarations: [StockAndPriceComponent],
    entryComponents: []
})
export class StockAndPriceModule { }
