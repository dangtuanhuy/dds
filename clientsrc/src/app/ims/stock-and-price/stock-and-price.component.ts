import { Component, Injector, ViewChild, ViewEncapsulation, ElementRef } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import {
  StockAndPriceViewModel,
  StockAndPriceItemViewModel,
  StockAndPriceProductViewModel,
  StockAndPriceLevels,
  StockAndPriceStoreItemViewModel,
  StockAndPricesWarehouseItemViewModel
} from './stock-and-price.model';
import { HubConnection } from '@aspnet/signalr';
import { StockAndPriceService } from 'src/app/shared/services/stock-and-price.service';
import { fromEvent } from 'rxjs';
import * as signalR from '@aspnet/signalr';
import { environment } from 'src/environments/environment';
import { LocationType } from '../locations/location.model';

@Component({
  selector: 'app-stock-and-price',
  templateUrl: './stock-and-price.component.html',
  styleUrls: ['./stock-and-price.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StockAndPriceComponent extends ComponentBase {

  public title = 'Stock And Price';
  private hubConnection: HubConnection;
  @ViewChild('stocksTable') stockTable: any;
  @ViewChild('variantStoreTable') variantStoreTable: any;
  @ViewChild('variantWarehouseTable') variantWarehouseTable: any;
  public pageSize = 10;
  public pageNumber = 0;
  public stockAndPricesViewModel: StockAndPriceViewModel = {
    totalItems: 0,
    items: []
  };
  public isLoadingPage = true;
  public searchText = '';
  @ViewChild('searchInput') searchInput!: ElementRef;
  constructor(private stockAndPriceService: StockAndPriceService,
    public injector: Injector) {
    super(injector);
  }

  onInit() {
    this.addKeyUpEventToSearchText();
    this.getPage(this.pageNumber, this.pageSize, this.searchText);
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.app.ims.apiUrl}/stock-and-price-level`)
      .configureLogging(signalR.LogLevel.Information)
      .build();
    this.hubConnection.start().catch(err => console.log(err));
    this.hubConnection.on('BroadcastMessage', (stockAndPriceLevels: StockAndPriceLevels[]) => {
      if (stockAndPriceLevels) {
        this.stockAndPricesViewModel.items.forEach(currentItem => {
          const storeItemsUpdated = stockAndPriceLevels.filter(x => x.productId === currentItem.productId
            && x.locationType === LocationType.store);
          const storeNewItems = [...storeItemsUpdated];
          const warehouseItemsUpdated = stockAndPriceLevels.filter(x => x.productId === currentItem.productId
            && x.locationType === LocationType.wareHouse);
          const warehouseNewItems = [...warehouseItemsUpdated];

          // Update existed items
          currentItem.storeItems.forEach(currentStoreItem => {
            const indexItemUpdated = storeItemsUpdated.findIndex(x => `${x.variantId}${x.locationId}` === currentStoreItem.referenceId);
            if (indexItemUpdated !== -1) {
              const itemUpdated = storeItemsUpdated[indexItemUpdated];
              currentStoreItem.quantity = itemUpdated.quantity;
              currentStoreItem.listPrice = itemUpdated.listPrice;
              currentStoreItem.memberPrice = itemUpdated.memberPrice;
              currentStoreItem.staffPrice = itemUpdated.staffPrice;
              storeNewItems.splice(indexItemUpdated, 1);
            }
          });

          currentItem.warehouseItems.forEach(currentWahouseItem => {
            const indexItemUpdated = warehouseItemsUpdated
              .findIndex(x => `${x.variantId}${x.locationId}` === currentWahouseItem.referenceId);
            if (indexItemUpdated !== -1) {
              const itemUpdated = warehouseItemsUpdated[indexItemUpdated];
              currentWahouseItem.quantity = itemUpdated.quantity;
              warehouseNewItems.splice(indexItemUpdated, 1);
            }
          });

          // Updated new items
          storeNewItems.forEach(item => {
            const newItem: StockAndPriceStoreItemViewModel = {
              categoryName: item.categoryName,
              locationName: item.locationName,
              variantName: item.variantName,
              skuCode: item.skuCode,
              quantity: item.quantity,
              referenceId: `${item.variantId}${item.locationId}`,
              listPrice: item.listPrice,
              memberPrice: item.memberPrice,
              staffPrice: item.staffPrice
            };
            const tempArray = [...currentItem.storeItems];
            currentItem.storeItems = [...tempArray, newItem];
          });

          warehouseNewItems.forEach(item => {
            const newItem: StockAndPricesWarehouseItemViewModel = {
              categoryName: item.categoryName,
              locationName: item.locationName,
              variantName: item.variantName,
              skuCode: item.skuCode,
              referenceId: `${item.variantId}${item.locationId}`,
              quantity: item.quantity,
            };
            const tempArray = [...currentItem.warehouseItems];
            currentItem.warehouseItems = [...tempArray, newItem];
          });
        });
      }
    });
  }

  onDestroy() {

  }

  private getPage(pageNumber: number, pageSize: number, queryText: string) {
    this.isLoadingPage = true;
    this.stockAndPriceService.getPage(pageNumber + 1, pageSize, queryText).subscribe(res => {
      if (res) {
        this.stockAndPricesViewModel.totalItems = res.totalItems;
        if (res.data) {
          const items: StockAndPriceItemViewModel[] = [];
          res.data.forEach((x: StockAndPriceProductViewModel) => {
            const newItems: StockAndPriceItemViewModel = {
              productId: x.productId,
              productName: x.productName,
              storeItems: [],
              warehouseItems: [],
              isLoadingData: false,
              isExpand: false
            };
            items.push(newItems);
          });
          this.stockAndPricesViewModel.items = items;
        }
      }
      this.isLoadingPage = false;
    });
  }

  public setPage(pageInfo: { offset: number; }) {
    this.pageNumber = pageInfo.offset;
    this.getPage(this.pageNumber, this.pageSize, this.searchText);
  }

  public toggleExpandStocksRow(row: any) {
    const index = this.stockAndPricesViewModel.items.findIndex(x => x.productId === row.productId);
    if (!this.stockAndPricesViewModel.items[index].isExpand) {
      this.getStockAndPriceByProduct(row.productId);
    }
    this.stockAndPricesViewModel.items[index].isExpand = !this.stockAndPricesViewModel.items[index].isExpand;
    this.stockTable.rowDetail.toggleExpandRow(row);
  }

  private getStockAndPriceByProduct(productId: string) {
    const index = this.stockAndPricesViewModel.items.findIndex(x => x.productId === productId);
    this.stockAndPricesViewModel.items[index].isLoadingData = true;
    this.stockAndPriceService.getByProduct(productId, this.searchText).subscribe(res => {
      if (res) {
        if (index !== -1) {
          this.stockAndPricesViewModel.items[index].storeItems = res.storeItems;
          this.stockAndPricesViewModel.items[index].warehouseItems = res.warehouseItems;
        }
      }
      this.stockAndPricesViewModel.items[index].isLoadingData = false;
    });
  }

  public expandAllStocksRows() {
    this.stockAndPricesViewModel.items.forEach(x => {
      if (!x.isExpand) {
        this.getStockAndPriceByProduct(x.productId);
        x.isExpand = true;
      }
    });
    this.stockTable.rowDetail.expandAllRows();
  }

  public collapseAllStocksRows() {
    this.stockAndPricesViewModel.items.forEach(x => {
      x.isExpand = false;
    });
    this.stockTable.rowDetail.collapseAllRows();
  }

  public getDetailStoreVariants(productId: string) {
    const correspondingStockVariant = this.stockAndPricesViewModel.items.find(x => x.productId === productId);
    if (correspondingStockVariant) {
      return correspondingStockVariant.storeItems;
    }

    return 0;
  }

  public getDetailWarehouseVariants(productId: string) {
    const correspondingStockVariant = this.stockAndPricesViewModel.items.find(x => x.productId === productId);
    if (correspondingStockVariant) {
      return correspondingStockVariant.warehouseItems;
    }

    return 0;
  }

  public formatNumberDecimal(value: number) {
    return value.toFixed(2);
  }

  public addKeyUpEventToSearchText() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .subscribe(() => {
        this.getPage(this.pageNumber, this.pageSize, this.searchText);
      });
  }
}
