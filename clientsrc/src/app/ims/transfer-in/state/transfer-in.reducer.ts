import * as fromRoot from 'src/app/shared/state/app.state';
import { TransferInActions, TransferInActionTypes } from './transfer-in.action';
import { InventoryTransactionTransferInModel, InventoryTransactionTransferInViewModel } from '../transfer-in.model';
import { InventoryTransactionTransferOutViewModel } from '../../transfer-out/transfer-out.model';
import { SortDirection } from 'src/app/shared/base-model/paging-filter-criteria';

export interface State extends fromRoot.State {
  inventoryTransactionTransferIns: TransferInState;
}

export interface TransferInState {
  inventoryTransactionTransferIns: InventoryTransactionTransferInViewModel[];
  inventoryTransactionTransferIn: InventoryTransactionTransferInViewModel;
  sortDirection: SortDirection;
  sortColumnName: String;
}

const initialState: TransferInState = {
  inventoryTransactionTransferIns: [],
  inventoryTransactionTransferIn: null,
  sortColumnName: '',
  sortDirection: SortDirection.Ascending
};

export const key = 'transfer_in_reducer';

export function reducer(
  state = initialState,
  action: TransferInActions
): TransferInState {
  switch (action.type) {
    case TransferInActionTypes.GetInventoryTransactionTransferInsByLocationSuccess:
      return {
        ...state,
        inventoryTransactionTransferIns: [...action.payload.data]
      };
    case TransferInActionTypes.GetTransferInsSuccess:
      return {
        ...state,
        inventoryTransactionTransferIns: [...action.payload.data]
      };
    case TransferInActionTypes.GetTransferInSuccess:
      return {
        ...state,
        inventoryTransactionTransferIn: action.payload
      };
    case TransferInActionTypes.GetTransferIns:
      return {
        ...state,
        sortColumnName: action.payload.sortColumn,
        sortDirection: action.payload.sortDirection
      };
    default:
      return state;
  }
}
