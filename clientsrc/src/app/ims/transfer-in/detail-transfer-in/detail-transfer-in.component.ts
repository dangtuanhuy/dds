import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductService } from 'src/app/shared/services/product.service';
import { StockTypeService } from 'src/app/shared/services/stock-type.service';
import * as transferInActions from '../../transfer-in/state/transfer-in.action';
import * as transferInSelector from '../../transfer-in/state/index';
import { Store, select } from '@ngrx/store';
import { takeWhile } from 'rxjs/operators';
import {
  InventoryTransactionTransferInViewModel,
  InventoryTransactionTransferInProductModel,
  InventoryTransactionTransferInProductItem
} from '../transfer-in.model';
import * as moment from 'moment';
import { ProductListModel } from '../../products/product';
import { StockTypeModel } from '../../allocation-transaction/allocation-transaction.model';
import { Guid } from 'src/app/shared/utils/guid.util';
import { ReportService } from 'src/app/shared/services/report.service';
import { environment } from 'src/environments/environment';

interface TransferInDetails {
  fromWareHouse: string,
  toOutlet: string,
  transferInNumber: string,
  createdDate: string,
}

interface TooltipValues {
  products: {
    name: string,
    variantName: string
  }[]
}

@Component({
  selector: 'app-detail-transfer-in',
  templateUrl: './detail-transfer-in.component.html',
  styleUrls: ['./detail-transfer-in.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetailTransferInComponent extends ComponentBase {
  constructor(
    private store: Store<any>,
    private activeModal: NgbActiveModal,
    private reportService: ReportService,
    public injector: Injector
  ) {
    super(injector);
  }
  public componentActive = true;
  public isProductsLoading = true;
  public isStockTypesLoading = true;
  public products: Array<ProductListModel> = [];
  public stockTypes: Array<StockTypeModel> = [];
  public inventoryTransactionTransferIn: InventoryTransactionTransferInViewModel;
  public inventoryTransactionTransferIns: Array<InventoryTransactionTransferInViewModel> = [];
  public inventoryTransactionTransferInProducts: Array<InventoryTransactionTransferInProductModel> = [];
  public tooltipValues: TooltipValues = {
    products: []
  };
  public transferInDetails = {} as TransferInDetails;

  onInit() {

    this.handleSubscription(this.store.pipe(
      select(transferInSelector.getSelectedItem), takeWhile(() => this.componentActive))
      .subscribe(
        (id: string) => {
          if (id == null) {
            return;
          }          
          this.getInventoryTransaction(id);
        }
      ));
  }

  getInventoryTransaction(id: string) {
    this.store.dispatch(new transferInActions.GetTransferIn(id));
    this.store.pipe(select(transferInSelector.getInventoryTransactionTransferIn), takeWhile(() => this.componentActive))
      .subscribe((inventoryTransactionTransferIn: InventoryTransactionTransferInViewModel) => {
        if (inventoryTransactionTransferIn == null) {
          return;
        }

        inventoryTransactionTransferIn.createdDate =
          moment.utc(inventoryTransactionTransferIn.createdDate).format('YYYY-MM-DD');

        this.transferInDetails.fromWareHouse = inventoryTransactionTransferIn.fromLocation;
        this.transferInDetails.toOutlet = inventoryTransactionTransferIn.toLocation;
        this.transferInDetails.transferInNumber = inventoryTransactionTransferIn.transferNumber;
        this.transferInDetails.createdDate = inventoryTransactionTransferIn.createdDate;

        this.inventoryTransactionTransferIn = inventoryTransactionTransferIn;

        this.inventoryTransactionTransferInProducts = [];        

        inventoryTransactionTransferIn.inventoryTransactionTransferProducts.forEach(item => {
          const inventoryTransactionTransferInProduct: InventoryTransactionTransferInProductModel = {} as InventoryTransactionTransferInProductModel;
          if (item.productId && item.productId !== Guid.empty()) {
            inventoryTransactionTransferInProduct.id = item.id;
            inventoryTransactionTransferInProduct.productId = item.productId !== Guid.empty() ? item.productId : null;
            inventoryTransactionTransferInProduct.productName = item.productName !== null ? item.productName : null;
            inventoryTransactionTransferInProduct.variantId = item.variantId !== Guid.empty() ? item.variantId : null;
            inventoryTransactionTransferInProduct.stockTypeId = Guid.empty();
            inventoryTransactionTransferInProduct.quantity = item.quantity;
            inventoryTransactionTransferInProduct.stockTransactionRefId = item.stockTransactionRefId;
            inventoryTransactionTransferInProduct.transactionRefId = item.transactionRefId;
            inventoryTransactionTransferInProduct.variants = item.variants;
            inventoryTransactionTransferInProduct.isVariantsLoading = true;
          }
          else {
            inventoryTransactionTransferInProduct.isVariantsLoading = false;
          }
          this.inventoryTransactionTransferInProducts.push(inventoryTransactionTransferInProduct);

          this.tooltipValues.products = this.inventoryTransactionTransferInProducts.map(x => {
            return {
              name: x.productName,
              variantName: x.variants[0].name
            }
          });

        })
      })
  }

  onDestroy() {
    this.tooltipValues.products = [];
  }

  onPrintTransfer() {
    const transferStatementRequest: InventoryTransactionTransferInViewModel = {
      id: this.inventoryTransactionTransferIn.id,
      inventoryTransactionRefId: this.inventoryTransactionTransferIn.inventoryTransactionRefId,
      fromLocationId: this.inventoryTransactionTransferIn.fromLocationId,
      toLocationId: this.inventoryTransactionTransferIn.toLocationId,
      transferNumber: this.inventoryTransactionTransferIn.transferNumber,
      createdDate: this.inventoryTransactionTransferIn.createdDate,
      fromLocation: this.inventoryTransactionTransferIn.fromLocation,
      toLocation: this.inventoryTransactionTransferIn.toLocation,
      inventoryTransactionTransferProducts: this.inventoryTransactionTransferInProducts
    };

    const redirectWindow = window.open(environment.app.ims.url + '/report-loading', '_blank');
    this.reportService.printTransferInReport(transferStatementRequest).subscribe(
      data => {
        if (data.status === 1) {
          const reportRedirect: any = ((environment.app.report.apiUrl + '/reports?' + data.endPoint).replace(/["]/g, ''));
          redirectWindow.location = reportRedirect;
        } else {
          const reportErrorRedirect: any = ((environment.app.ims.url + '/report-error').replace(/["]/g, ''));
          redirectWindow.location = reportErrorRedirect;
        }
      }
    );
  }

  onClose(): void {    
    this.activeModal.close('closed');
  }

  onDismiss(reason: String): void {
    this.activeModal.dismiss(reason);
  }

}
