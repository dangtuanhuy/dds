export const posEnvironment = {
    production: false,
    app:
    {
        deviceHub: {
            apiUrl: 'http://localhost:1234/api/devices'
        }
    },
    dbFolderName: 'POSQC',
    isDevelopment: false
};
