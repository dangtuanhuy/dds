import { OrderModel, Order } from '../models/order';
import { PriceExtension } from './price.extension';
import { OrderItemModel } from '../models/order-item';
import {
    SaleReceipt, SaleReceiptItem, SalePromotion, SaleReceiptItemPromotion, SalePayment, SaleReceiptExchange, SaleReceiptRefund
} from '../models/device-hub/sale-receipt';
import { DiscountType } from '../enums/discount-type.enum';
import { PaymentModeModel } from '../models/payment-mode.model';
import { OrderItemStatus } from '../enums/order-item-status.enum';

export class ReceiptExtension {
    public static buildReceiptFromOrderModel(order: OrderModel, cashierName: string, storeName: string, storeAddress: string
        , paymentModes: PaymentModeModel[],
        previousOrderBillNo = '', nextOrderBillNo = '') {
        const netQuantityTotal = this.calculateNetQuantity(order.orderItems);

        const receipt = new SaleReceipt();
        receipt.createdDate = order.createdDate;
        receipt.netTotal = order.amount;
        receipt.cashierName = cashierName;
        receipt.counterName = ''; // PHI TODO
        receipt.gst = order.gst;
        receipt.gstInclusive = order.gstInclusive;
        receipt.netQuantity = netQuantityTotal;
        receipt.exchangeDayQuantity = 7; // PHI TODO: get from app setting.
        receipt.customerName = order.customerName;
        receipt.customerCode = order.customerCode;
        receipt.customerPhoneNumber = order.customerPhoneNumber;
        receipt.billNo = order.billNo;
        receipt.previousBillNo = previousOrderBillNo;
        receipt.nextBillNo = nextOrderBillNo;
        receipt.change = order.change;
        receipt.storeName = storeName;
        receipt.storeAddress = storeAddress;

        let totalPriceOfReceiptItems = 0;
        let countItem = 1;
        order.orderItems.forEach(orderItem => {
            const saleReceiptItem = new SaleReceiptItem();
            saleReceiptItem.no = countItem;
            saleReceiptItem.name = orderItem.variant;
            saleReceiptItem.quantity = orderItem.quantity;
            saleReceiptItem.price = orderItem.price;
            saleReceiptItem.amount = orderItem.price * orderItem.quantity;
            saleReceiptItem.skuCode = orderItem.skuCode;
            receipt.items.push(saleReceiptItem);
            totalPriceOfReceiptItems += saleReceiptItem.amount;
            countItem++;

            saleReceiptItem.receiptItemPromotions = [];
            const orderItemPromotions = orderItem.orderItemPromotions;
            if (Array.isArray(orderItemPromotions)) {
                orderItemPromotions.forEach(orderItemPromotion => {
                    const receiptItemPromotion = new SaleReceiptItemPromotion();
                    receiptItemPromotion.description = 'Item Discount';
                    receiptItemPromotion.discountType = orderItemPromotion.discountType;
                    receiptItemPromotion.value = orderItemPromotion.value;
                    if (orderItemPromotion.discountType === DiscountType.Money) {
                        receiptItemPromotion.amount = -1 * orderItemPromotion.value;
                    }

                    if (orderItemPromotion.discountType === DiscountType.Percent) {
                        receiptItemPromotion.amount = -1 * orderItemPromotion.value * saleReceiptItem.amount / 100;
                    }

                    saleReceiptItem.receiptItemPromotions.push(receiptItemPromotion);
                    totalPriceOfReceiptItems += receiptItemPromotion.amount;
                });
            }
        });
        receipt.totalAmount = totalPriceOfReceiptItems;
        if (receipt.gstInclusive) {
            receipt.gstInclusiveAmount = this.calculateGSTInclusive(totalPriceOfReceiptItems, receipt.gst);
        } else {
            receipt.gstNoInclusiveAmount = this.calculateGSTNotInclusive(receipt.totalAmount, receipt.gst);
        }

        const discountPrices: number[] = [];
        order.orderPromotions.forEach(orderPromotion => {
            const salePromotion = new SalePromotion();
            salePromotion.description = orderPromotion.reason;
            salePromotion.discountType = orderPromotion.discountType;
            salePromotion.value = orderPromotion.value;

            if (orderPromotion.discountType === DiscountType.Money) {
                const amount = -1 * salePromotion.value;
                discountPrices.push(amount);
                salePromotion.amount = amount;
            }
            if (orderPromotion.discountType === DiscountType.Percent) {
                const amount = -1 * salePromotion.value * receipt.totalAmount / 100;
                discountPrices.push(amount);
                salePromotion.amount = amount;
            }

            receipt.salePromotions.push(salePromotion);
        });

        receipt.salePayments = [];
        order.orderPayments.forEach(orderPayment => {
            const salePayment = new SalePayment();
            const correspondingPaymentMode = paymentModes.find(x => x.code === orderPayment.paymentCode);
            salePayment.code = orderPayment.paymentCode;
            salePayment.name = correspondingPaymentMode ? correspondingPaymentMode.paymode : orderPayment.paymentCode;
            if (salePayment.code === 'CASH') {
                salePayment.amount = PriceExtension.round(orderPayment.amount + receipt.change, 2);
            } else {
                salePayment.amount = PriceExtension.round(orderPayment.amount, 2);
            }

            salePayment.decimalAmount = salePayment.amount.toFixed(2);

            receipt.salePayments.push(salePayment);
        });

        return receipt;
    }

    public static buildReceiptExchangeFromOrderModel(order: OrderModel, cashierName: string, storeName: string, storeAddress: string
        , paymentModes: PaymentModeModel[],
        previousOrderBillNo = '', nextOrderBillNo = '') {
        const netQuantityTotal = this.calculateNetQuantity(order.orderItems);

        const receipt = new SaleReceiptExchange();
        receipt.createdDate = order.createdDate;
        receipt.netTotal = order.amount;
        receipt.cashierName = cashierName;
        receipt.counterName = ''; // PHI TODO
        receipt.gst = order.gst;
        receipt.gstInclusive = order.gstInclusive;
        receipt.netQuantity = netQuantityTotal;
        receipt.exchangeDayQuantity = 7; // PHI TODO: get from app setting.
        receipt.customerName = order.customerName;
        receipt.customerCode = order.customerCode;
        receipt.customerPhoneNumber = order.customerPhoneNumber;
        receipt.billNo = order.billNo;
        receipt.previousBillNo = previousOrderBillNo;
        receipt.nextBillNo = nextOrderBillNo;
        receipt.change = order.change;
        receipt.storeName = storeName;
        receipt.storeAddress = storeAddress;
        receipt.collect = order.collect;
        receipt.reason = order.reason;

        let totalPriceOfReceiptItems = 0;
        const purchasedOrderItems = order.orderItems.filter(x => x.status === OrderItemStatus.Old);
        const exchangedOrderItems = order.orderItems.filter(x => x.status === OrderItemStatus.Exchange);
        const newOrderItems = order.orderItems.filter(x => x.status === OrderItemStatus.Normal);

        const newData = this.mapToSaleReceiptItem(newOrderItems);
        receipt.newItems = newData.receiptItems;
        totalPriceOfReceiptItems = newData.totalPriceOfReceiptItems;

        const purchasedData = this.mapToSaleReceiptItem(purchasedOrderItems);
        receipt.purchasedItems = purchasedData.receiptItems;
        totalPriceOfReceiptItems += purchasedData.totalPriceOfReceiptItems;

        const exchangedData = this.mapToSaleReceiptItem(exchangedOrderItems);
        receipt.exchangedItems = exchangedData.receiptItems;

        receipt.totalAmount = totalPriceOfReceiptItems;
        if (receipt.gstInclusive) {
            receipt.gstInclusiveAmount = this.calculateGSTInclusive(totalPriceOfReceiptItems, receipt.gst);
        } else {
            receipt.gstNoInclusiveAmount = this.calculateGSTNotInclusive(receipt.totalAmount, receipt.gst);
        }

        const discountPrices: number[] = [];
        order.orderPromotions.forEach(orderPromotion => {
            const salePromotion = new SalePromotion();
            salePromotion.description = orderPromotion.reason;
            salePromotion.discountType = orderPromotion.discountType;
            salePromotion.value = orderPromotion.value;

            if (orderPromotion.discountType === DiscountType.Money) {
                const amount = -1 * salePromotion.value;
                discountPrices.push(amount);
                salePromotion.amount = amount;
            }
            if (orderPromotion.discountType === DiscountType.Percent) {
                const amount = -1 * salePromotion.value * receipt.totalAmount / 100;
                discountPrices.push(amount);
                salePromotion.amount = amount;
            }

            receipt.salePromotions.push(salePromotion);
        });

        receipt.salePayments = [];
        order.orderPayments.forEach(orderPayment => {
            const salePayment = new SalePayment();
            const correspondingPaymentMode = paymentModes.find(x => x.code === orderPayment.paymentCode);
            salePayment.code = orderPayment.paymentCode;
            salePayment.name = correspondingPaymentMode ? correspondingPaymentMode.paymode : orderPayment.paymentCode;
            if (salePayment.code === 'CASH') {
                salePayment.amount = PriceExtension.round(orderPayment.amount + receipt.change, 2);
            } else {
                salePayment.amount = PriceExtension.round(orderPayment.amount, 2);
            }

            salePayment.decimalAmount = salePayment.amount.toFixed(2);

            receipt.salePayments.push(salePayment);
        });

        return receipt;
    }

    public static buildReceiptRefundFromOrderModel(order: OrderModel, cashierName: string, storeName: string, storeAddress: string,
        paymentModes: PaymentModeModel[],
        previousOrderBillNo: string, nextOrderBillNo: string
    ) {
        const netQuantityTotal = this.calculateNetQuantity(order.orderItems);

        const receipt = new SaleReceiptRefund();
        receipt.createdDate = order.createdDate;
        receipt.netTotal = order.amount;
        receipt.cashierName = cashierName;
        receipt.counterName = ''; // PHI TODO
        receipt.gst = order.gst;
        receipt.gstInclusive = order.gstInclusive;
        receipt.netQuantity = netQuantityTotal;
        receipt.exchangeDayQuantity = 7; // PHI TODO: get from app setting.
        receipt.customerName = order.customerName;
        receipt.customerCode = order.customerCode;
        receipt.customerPhoneNumber = order.customerPhoneNumber;
        receipt.billNo = order.billNo;
        receipt.previousBillNo = previousOrderBillNo;
        receipt.nextBillNo = nextOrderBillNo;
        receipt.change = order.change;
        receipt.storeName = storeName;
        receipt.storeAddress = storeAddress;
        receipt.refund = -1 * order.collect;
        receipt.reason = order.reason;

        const purchasedOrderItems = order.orderItems.filter(x => x.status === OrderItemStatus.Old);
        const refundedOrderItems = order.orderItems.filter(x => x.status === OrderItemStatus.Refund);

        let totalPriceOfReceiptItems = 0;
        const purchasedData = this.mapToSaleReceiptItem(purchasedOrderItems);
        receipt.purchasedItems = purchasedData.receiptItems;
        totalPriceOfReceiptItems += purchasedData.totalPriceOfReceiptItems;

        const refundedData = this.mapToSaleReceiptItem(refundedOrderItems);
        receipt.refundedItems = refundedData.receiptItems;

        receipt.totalAmount = totalPriceOfReceiptItems;
        if (receipt.gstInclusive) {
            receipt.gstInclusiveAmount = this.calculateGSTInclusive(totalPriceOfReceiptItems, receipt.gst);
        } else {
            receipt.gstNoInclusiveAmount = this.calculateGSTNotInclusive(receipt.totalAmount, receipt.gst);
        }

        const discountPrices: number[] = [];
        order.orderPromotions.forEach(orderPromotion => {
            const salePromotion = new SalePromotion();
            salePromotion.description = orderPromotion.reason;
            salePromotion.discountType = orderPromotion.discountType;
            salePromotion.value = orderPromotion.value;

            if (orderPromotion.discountType === DiscountType.Money) {
                const amount = -1 * salePromotion.value;
                discountPrices.push(amount);
                salePromotion.amount = amount;
            }
            if (orderPromotion.discountType === DiscountType.Percent) {
                const amount = -1 * salePromotion.value * receipt.totalAmount / 100;
                discountPrices.push(amount);
                salePromotion.amount = amount;
            }

            receipt.salePromotions.push(salePromotion);
        });

        receipt.salePayments = [];
        order.orderPayments.forEach(orderPayment => {
            const salePayment = new SalePayment();
            const correspondingPaymentMode = paymentModes.find(x => x.code === orderPayment.paymentCode);
            salePayment.code = orderPayment.paymentCode;
            salePayment.name = correspondingPaymentMode ? correspondingPaymentMode.paymode : orderPayment.paymentCode;
            if (salePayment.code === 'CASH') {
                salePayment.amount = PriceExtension.round(orderPayment.amount + receipt.change, 2);
            } else {
                salePayment.amount = PriceExtension.round(orderPayment.amount, 2);
            }

            salePayment.decimalAmount = salePayment.amount.toFixed(2);

            receipt.salePayments.push(salePayment);
        });

        return receipt;
    }


    public static calculateNetQuantity(orderItems: OrderItemModel[]): any {
        let result = 0;
        orderItems.forEach(x => {
            result += x.quantity;
        });

        return result;
    }

    public static calculateOrderItemsNetQuantityTotal(orderItems: OrderItemModel[]) {
        let total = 0;
        if (orderItems && orderItems.length > 0) {
            orderItems.forEach(x => total += x.quantity);
        }
        return total;
    }

    public static formatReceiptDateString(localTime: Date): string {
        if (!localTime) {
            return '';
        }
        let result = `${localTime.getDate()}/${localTime.getMonth() + 1}/${localTime.getFullYear()}`;
        result += ` ${localTime.getHours()}:${localTime.getMinutes()}`;
        return result;
    }

    private static calculateGSTInclusive(totalNet: number, gst: number) {
        return PriceExtension.round((totalNet * gst / (gst + 100)), 2);
    }

    private static calculateGSTNotInclusive(total: number, gst: number) {
        return PriceExtension.round(total * gst / 100, 2);
    }

    private static mapToSaleReceiptItem(orderItems: OrderItemModel[]) {
        let totalPriceOfReceiptItems = 0;
        const receiptItems: SaleReceiptItem[] = [];
        let countItem = 1;
        orderItems.forEach(orderItem => {
            const saleReceiptItem = new SaleReceiptItem();
            saleReceiptItem.no = countItem;
            saleReceiptItem.name = orderItem.variant;
            saleReceiptItem.quantity = orderItem.quantity;
            saleReceiptItem.price = orderItem.price;
            saleReceiptItem.amount = orderItem.price * orderItem.quantity;
            saleReceiptItem.skuCode = orderItem.skuCode;
            receiptItems.push(saleReceiptItem);
            totalPriceOfReceiptItems += saleReceiptItem.amount;
            countItem++;

            saleReceiptItem.receiptItemPromotions = [];
            const orderItemPromotions = orderItem.orderItemPromotions;
            if (Array.isArray(orderItemPromotions)) {
                orderItemPromotions.forEach(orderItemPromotion => {
                    const receiptItemPromotion = new SaleReceiptItemPromotion();
                    receiptItemPromotion.description = 'Item Discount';
                    receiptItemPromotion.discountType = orderItemPromotion.discountType;
                    receiptItemPromotion.value = orderItemPromotion.value;
                    if (orderItemPromotion.discountType === DiscountType.Money) {
                        receiptItemPromotion.amount = -1 * orderItemPromotion.value;
                    }

                    if (orderItemPromotion.discountType === DiscountType.Percent) {
                        receiptItemPromotion.amount = -1 * orderItemPromotion.value * saleReceiptItem.amount / 100;
                    }

                    saleReceiptItem.receiptItemPromotions.push(receiptItemPromotion);
                    totalPriceOfReceiptItems += receiptItemPromotion.amount;
                });
            }
        });

        return {
            totalPriceOfReceiptItems,
            receiptItems
        };
    }
}
