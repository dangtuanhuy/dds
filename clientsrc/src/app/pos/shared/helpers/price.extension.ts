export class PriceExtension {
    public static calculateNetPrices(totalPrice: number, discountPrices: number[], tax: number) {
        const totalGST = this.round(totalPrice * tax / 100, 2);
        let totalDiscount = 0;
        discountPrices.forEach(price => {
            totalDiscount += Math.abs(price);
        });
        let net = totalPrice - totalDiscount;

        net = net > 0 ? net : 0;
        if (totalGST !== 0) {
            net += totalGST;
        }

        return net;
    }

    public static roundDown(number, decimals) {
        decimals = decimals || 0;
        if (decimals === 2) {
            const thounds = number * 1000;
            return (Math.floor(thounds / 10) / Math.pow(10, 2));
        } else {
            return (Math.floor(number * Math.pow(10, decimals)) / Math.pow(10, decimals));
        }
    }

    public static roundUp(num, precision) {
        precision = Math.pow(10, precision);
        return Math.ceil(num * precision) / precision;
    }

    public static round(number, decimals) {
        const value = Math.abs(number);
        const numberRoundDown = Math.floor(value * Math.pow(10, decimals)) / Math.pow(10, decimals);

        let precision = Math.pow(10, decimals + 1);
        const numberAfterDecimal = value * precision - numberRoundDown * precision;

        precision = Math.pow(10, decimals);
        let result = numberAfterDecimal >= 5
            ? Math.ceil(value * precision) / precision
            : numberRoundDown;

        if (number < 0) {
            result = -1 * result;
        }
        return result;
    }
}
