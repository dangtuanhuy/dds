import { OrderItemModel } from '../models/order-item';
import { PriceExtension } from './price.extension';
import { DiscountType } from '../enums/discount-type.enum';

export class OrderPriceExtension {
    public static calculateOrderItemsAmount(orderItems: OrderItemModel[]) {
        let result = 0;

        orderItems.forEach(item => {
            const amountItem = item.price * item.quantity;
            
            const discountPrices = item.orderItemPromotions.map(promotion => {
                if (promotion.discountType === DiscountType.Money) {
                    return PriceExtension.round(-1 * promotion.value, 2);
                }

                if (promotion.discountType === DiscountType.Percent) {
                    const value = -1 * amountItem * promotion.value / 100;
                    return PriceExtension.round(value, 2);
                }
            });

            item.amount = PriceExtension.round(PriceExtension.calculateNetPrices(amountItem, discountPrices, 0), 2);
            result += item.amount;
        });

        return result;
    }
}
