export class NumberExtension {

    //
    // Use to SUM and ROUND the decimal numbers.
    //
    public static calculateTotal(data: number[]) {
        let result = 0;
        data.forEach(x => {
            result += x * 1000;
        });

        result = result / 1000;
        return result;
    }
}
