import { MigrationSchema } from '../migration.schema';
import { AppSettingSchema } from '../appSetting.schema';
import { CategorySchema } from '../category.schema';
import { VariantSchema } from '../variant.schema';
import { PendingSaleItemSchema } from '../pending-sale-item.schema';
import { CashingUpSchema } from '../cashing-up.schema';
import { UserSchema } from '../user.schema';
import { PendingSaleSchema } from '../pending-sale.schema';
import { CustomerSchema } from '../customer.schema';
import { TenderSchema } from '../tender.schema';
import { PromotionSchema } from '../promotion.schema';
import { OrderPromotionSchema } from '../order-promotion.schema';
import { OrderItemPromotionSchema } from '../order-item-promotion.schema';
import { BarCodeSchema } from '../barcode.schema';
import { SaleTargetSchema } from '../sale-target.schema';
import { SalesAchievedSchema } from '../sales-achieved.schema';
import { OrderPaymentSchema } from '../order-payment.schema';
import { PaymentMethodSchema } from '../migration-1/payment-method.schema';
import { DefaultPaymentMethodSchema } from '../migration-1/default-payment-method.schema';
import { Product4Schema } from '../migration-4/product-4.schema';
import { Location5Schema } from '../migration-5/location-5.schema';
import { OrderItem7Schema } from '../migration-7/order-item-7.schema';
import { Order8Schema } from './order-8.schema';

export const Migration8 = [
    MigrationSchema,
    AppSettingSchema,
    CategorySchema,
    Product4Schema,
    VariantSchema,
    CustomerSchema,
    PendingSaleSchema,
    PendingSaleItemSchema,
    UserSchema,
    CashingUpSchema,
    TenderSchema,
    PromotionSchema,
    OrderPromotionSchema,
    OrderItemPromotionSchema,
    BarCodeSchema,
    SaleTargetSchema,
    SalesAchievedSchema,
    OrderPaymentSchema,
    PaymentMethodSchema,
    DefaultPaymentMethodSchema,
    Location5Schema,
    OrderItem7Schema,
    Order8Schema
];
