import { SchemaNames } from '../../constants/schema-name.constant';

export const Order7Schema = {
    name: SchemaNames.order,
    primaryKey: 'id',
    properties: {
        id: 'string',
        customerId: { type: 'string', optional: true },
        amount: 'double',
        gst: 'double',
        gstInclusive: 'bool',
        isDelete: 'bool',
        synced: 'bool',
        billNo: 'string',
        change: 'double',
        createdDate: 'date',
        cashierId: 'string',
        orderTransactionType: 'int',
        previousOrderId: { type: 'string', optional: true },
        originalOrderId: 'string',
        reason: 'string'
    }
};
