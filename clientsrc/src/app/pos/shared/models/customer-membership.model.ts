import { MembershipTypeId } from '../enums/membership-type.enum';

export class CustomerMembershipModel {
    customerId: string;
    membershipType: CustomerMembershipTypeModel;
}

export class CustomerMembershipTypeModel {
    id: MembershipTypeId;
    typeName: string;

    constructor(id: MembershipTypeId, typeName: string) {
        this.id = id;
        this.typeName = typeName;
    }
}
