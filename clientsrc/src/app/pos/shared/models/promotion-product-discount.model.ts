import { OrderItemPromotion } from './order-item-promotion';

export class PromotionProductDiscountModel {
    key: string;
    value: string;
    promotions: OrderItemPromotion[] = [];
}
