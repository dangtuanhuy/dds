import { CashingUp } from '../cashing-up';

export class CloseDayReport {
    location: string;
    terminal: string;
    cashier: string;
    shift: number;
    settlementDate: string;

    posCashSales: number;
    totalCurrencies: number;
    tillAmount: number;
    totalCollection: number;
    grandTotal: number;
    systemTotal: number;
    shortageExcess: number;
    paymentModeDetailstTotal: number;
    totalCashAndPaymentModeDetails: number;

    openDay: CashingUp;
    currencies: CloseDayReportCurrency[] = [];
    paymentModeDetails: CloseDayReportPaymentModeDetail[] = [];
}

export class CloseDayReportCurrency {
    value: number;
    quantity: number;
    amount: number;
}

export class CloseDayReportPaymentModeDetail {
    name: string;
    amount: number;
    paymentMethodId: string;
}

export class EndDayModel {
  storeId: string;
  cashierName: string;
  cashierId: string;
  currencies: CloseDayReportCurrency[] = [];
  paymentModeDetails: CloseDayReportPaymentModeDetail[] = [];
  deviceCode: string;
  note: string;
}

export class OpenDayModel {
  storeId: string;
  cashierName: string;
  cashierId: string;
  currencies: OpenDayCurrency[] = [];  
  deviceCode: string;
}

export class OpenDayCurrency {
  value: number;
  quantity: number;
  amount: number;
}
