import { DiscountType } from '../../enums/discount-type.enum';

class ReceiptInfo {
    previousBillNo: string;
    nextBillNo: string;
    billNo: string;
    createdDate: Date;
    cashierName: string;
    counterName: string;
    storeName: string;
    storeAddress: string;
    totalAmount: number;
    gst: number;
    gstInclusiveAmount: number;
    gstNoInclusiveAmount: number;
    gstInclusive: boolean;
    netQuantity: number;
    netTotal: number;
    exchangeDayQuantity: number;
    customerName: string;
    customerCode: string;
    customerPhoneNumber: string;
    change: number;
    salePromotions: SalePromotion[] = [];
    salePayments: SalePayment[] = [];
    reason: string;
}

export class SaleReceipt extends ReceiptInfo {
    items: SaleReceiptItem[] = [];
}

export class SaleReceiptExchange extends ReceiptInfo {
    collect: number;
    purchasedItems: SaleReceiptItem[] = [];
    exchangedItems: SaleReceiptItem[] = [];
    newItems: SaleReceiptItem[] = [];
}

export class SaleReceiptItem {
    no: number;
    name: string;
    skuCode: string;
    quantity: number;
    price: number;
    amount: number;
    receiptItemPromotions: SaleReceiptItemPromotion[] = [];
}

export class SaleReceiptItemPromotion {
    description: string;
    discountType: DiscountType;
    value: number;
    amount: number;
}

export class SaleReceiptRefund extends ReceiptInfo {
    refund: number;
    purchasedItems: SaleReceiptRefundItem[] = [];
    refundedItems: SaleReceiptRefundItem[] = [];
}

export class SaleReceiptRefundItem {
    no: number;
    name: string;
    skuCode: string;
    quantity: number;
    price: number;
    amount: number;
    receiptItemPromotions: SaleReceiptRefundItemPromotion[] = [];
}

export class SaleReceiptRefundItemPromotion {
    description: string;
    discountType: DiscountType;
    value: number;
    amount: number;
}

export class SalePromotion {
    description: string;
    discountType: DiscountType;
    value: number;
    amount: number;
}

export class SalePayment {
    name: string;
    code: string;
    amount: number;
    decimalAmount: string;
}
