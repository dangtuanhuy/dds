export enum ExchangeViewStatus {
    NewItem = 0,
    PromotionNewItem = 1,
    CurrentPromotion = 2,
    NewPromotion = 3,
    ItemProduct = 4,
}
