export enum SaleItemType {
    Default = 0,
    Product = 1,
    Promotion = 2,
    ItemPromotion = 3,
    RefundItem = 4,
    RefundItemPromotion = 5,
    ExchangeItem = 6,
    ExchangeItemPromotion = 7
}
