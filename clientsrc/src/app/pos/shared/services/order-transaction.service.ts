import { Injectable } from '@angular/core';
import { OrderItemModel } from '../models/order-item';
import { OrderPayment } from '../models/order-payment';
import { OrderTransactionType } from '../enums/order-transaction-type.enum';
import { UnitOfWork } from '../data/unit-of-work';
import { Guid } from 'src/app/shared/utils/guid.util';
import { SchemaNames } from '../constants/schema-name.constant';
import { Order, OrderModel } from '../models/order';
import { OrderItemStatus } from '../enums/order-item-status.enum';
import { Observable } from 'rxjs';
import { AppContextManager } from '../app-context-manager';
import { OrderPromotion } from '../models/order-promotion';
import { OrderService } from './order.service';

@Injectable({ providedIn: 'root' })
export class OrderTransactionService {
    constructor(
        private unitOfWork: UnitOfWork,
        private orderService: OrderService,
        private appContextManager: AppContextManager) {
    }

    public exchangeOrder(exchangeOrder: Order, currentOrder: OrderModel, purchasedItems: OrderItemModel[], refundedItems: OrderItemModel[],
        newItems: OrderItemModel[], promotions: OrderPromotion[], newPayments: OrderPayment[])
        : Observable<Order> {
        return Observable.create(observer => {
            this.unitOfWork.beginTransaction();

            if (!currentOrder.previousOrderId) {
                currentOrder.originalOrderId = currentOrder.id;
                this.unitOfWork.update(SchemaNames.order, currentOrder);
            }

            this.unitOfWork.add(SchemaNames.order, exchangeOrder);

            this.addOrderItemAndItemPromotions(this.unitOfWork, exchangeOrder.id, refundedItems, OrderItemStatus.Exchange);
            this.addOrderItemAndItemPromotions(this.unitOfWork, exchangeOrder.id, purchasedItems, OrderItemStatus.Old);
            this.addOrderItemAndItemPromotions(this.unitOfWork, exchangeOrder.id, newItems, OrderItemStatus.Normal);

            promotions.forEach(newPromotion => {
                newPromotion.id = Guid.newGuid();
                newPromotion.orderId = exchangeOrder.id;
                this.unitOfWork.add(SchemaNames.orderPromotion, newPromotion);
            });

            newPayments.forEach(newPayment => {
                newPayment.id = Guid.newGuid();
                newPayment.orderId = exchangeOrder.id;
                newPayment.amount = Number(newPayment.amount);
                this.unitOfWork.add(SchemaNames.orderPayment, newPayment);
            });

            try {
                this.unitOfWork.saveChanges();
            } catch (e) {
                observer.next(null);
                observer.complete();
            }

            observer.next(exchangeOrder);
            observer.complete();
        });
    }

    public refundOrder(reason: string, currentAmout: number, exchangeAmount: number,
        currentOrder: OrderModel, purchasedItems: OrderItemModel[],
        refundedItems: OrderItemModel[], currentPromotions: OrderPromotion[])
        : Observable<Order> {
        return Observable.create(observer => {
            this.unitOfWork.beginTransaction();

            if (!currentOrder.previousOrderId) {
                currentOrder.originalOrderId = currentOrder.id;
                this.unitOfWork.update(SchemaNames.order, currentOrder);
            }

            this.orderService.generateOrderBillNo().subscribe(billNo => {
                const refundOrder: Order = {
                    id: Guid.newGuid(),
                    customerId: currentOrder.customerId,
                    amount: currentAmout,
                    gst: currentOrder.gst,
                    gstInclusive: currentOrder.gstInclusive,
                    billNo: billNo,
                    change: 0,
                    createdDate: new Date(),
                    cashierId: this.appContextManager.currentUser.id,
                    isDelete: false,
                    synced: false,
                    orderTransactionType: OrderTransactionType.Refund,
                    previousOrderId: currentOrder.id,
                    originalOrderId: currentOrder.originalOrderId,
                    reason: reason,
                    collect: -1 * exchangeAmount
                };

                this.unitOfWork.add(SchemaNames.order, refundOrder);

                this.addOrderItemAndItemPromotions(this.unitOfWork, refundOrder.id, refundedItems, OrderItemStatus.Refund);
                this.addOrderItemAndItemPromotions(this.unitOfWork, refundOrder.id, purchasedItems, OrderItemStatus.Old);


                currentPromotions.forEach(newPromotion => {
                    newPromotion.id = Guid.newGuid();
                    newPromotion.orderId = refundOrder.id;
                    this.unitOfWork.add(SchemaNames.orderPromotion, newPromotion);
                });

                try {
                    this.unitOfWork.saveChanges();
                } catch (e) {
                    observer.next(null);
                    observer.complete();
                }

                observer.next(refundOrder);
                observer.complete();
            });
        });
    }


    private addOrderItemAndItemPromotions(unitOfWork: UnitOfWork, exchangeOrderId: string, orderItems: OrderItemModel[],
        orderItemStatus: OrderItemStatus) {
        orderItems.forEach(orderItem => {
            orderItem.status = orderItemStatus;
            orderItem.orderId = exchangeOrderId;
            unitOfWork.add(SchemaNames.orderItem, orderItem);

            orderItem.orderItemPromotions.forEach(itemPromotion => {
                unitOfWork.add(SchemaNames.orderItemPromotion, itemPromotion);
            });
        });
    }
}
