import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PosCustomerServerService {
    constructor(
        private httpClient: HttpClient
    ) {
    }

    getCustomerMemberShip(deviceCode: string, customerId: string): Observable<any> {
        return this.httpClient.get(
            `${environment.app.retail.apiUrl}/api/syncs/customer/membership-inspection?customerId=${customerId}`,
            {
                headers: new HttpHeaders({
                    'x-device-code': deviceCode
                })
            }
        );
    }
}
