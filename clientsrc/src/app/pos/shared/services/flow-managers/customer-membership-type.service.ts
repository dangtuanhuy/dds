import { Injectable } from '@angular/core';
import { CustomerMembershipTypeModel } from '../../models/customer-membership.model';
import { MembershipTypeId } from '../../enums/membership-type.enum';

@Injectable({ providedIn: 'root' })
export class CustomerMembershipTypeService {
    private customerMembershipTypeModels: CustomerMembershipTypeModel[] = [
        new CustomerMembershipTypeModel(MembershipTypeId.Basic, 'Basic'),
        new CustomerMembershipTypeModel(MembershipTypeId.Premium, 'Premium+')
    ];

    getCustomerMembershipTypeModels() {
        return this.customerMembershipTypeModels;
    }
}
