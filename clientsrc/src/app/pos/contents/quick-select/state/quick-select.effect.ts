import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import * as quickSelectsActions from './quick-select.action';
import { map, switchMap } from 'rxjs/operators';
import { CategoryService } from 'src/app/pos/shared/services/category.service';
import { VariantService } from 'src/app/pos/shared/services/variant.service';
import { CategoryPagingModel } from 'src/app/pos/shared/view-models/category-paging.model';
import { VariantPagingModel } from 'src/app/pos/shared/view-models/variant-paging.model';

@Injectable()
export class QuickSelectEffects {
    constructor(
        private action$: Actions,
        private categoryService: CategoryService,
        private variantService: VariantService,
    ) { }

    @Effect()
    getCategoriesPaging$: Observable<Action> = this.action$.pipe(
        ofType(quickSelectsActions.QuickSelectActionTypes.GetCategoriesPaging),
        switchMap((action: any) => this.categoryService.getPaging(action.payload.pageIndex, action.payload.pageSize).pipe(
            map((categoryPagingModel: CategoryPagingModel) => (new quickSelectsActions.GetCategoriesPagingSuccess(categoryPagingModel)))
        ))
    );

    @Effect()
    getVariantsByCategoryPaging$: Observable<Action> = this.action$.pipe(
        ofType(quickSelectsActions.QuickSelectActionTypes.GetVariantsByCategoryPaging),
        switchMap((action: any) =>
            this.variantService.getByCategoryPaging(action.payload.categoryId, action.payload.locationId
                , action.payload.pageIndex, action.payload.pageSize)
                .pipe(
                    map((variantPagingModel: VariantPagingModel) =>
                        (new quickSelectsActions.GetVariantsByCategoryPagingSuccess(variantPagingModel)))
                ))
    );
}
