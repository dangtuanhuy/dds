import { Action } from '@ngrx/store';
import { CategoryPagingModel } from 'src/app/pos/shared/view-models/category-paging.model';
import { VariantPagingModel } from 'src/app/pos/shared/view-models/variant-paging.model';

export enum QuickSelectActionTypes {
    GetCategoriesPaging = '[QuickSelect] Get Categories Paging.',
    GetCategoriesPagingSuccess = '[QuickSelect] Get Categories Paging Success.',

    GetVariantsByCategoryPaging = '[QuickSelect] Get Variants By Paging Category.',
    GetVariantsByCategoryPagingSuccess = '[QuickSelect] Get Variants By Category Paging Success.'
}

export class GetCategoriesPaging implements Action {
    readonly type = QuickSelectActionTypes.GetCategoriesPaging;
    constructor(public payload: any) { }
}

export class GetCategoriesPagingSuccess implements Action {
    readonly type = QuickSelectActionTypes.GetCategoriesPagingSuccess;
    constructor(public payload: CategoryPagingModel) { }
}

export class GetVariantsByCategoryPaging implements Action {
    readonly type = QuickSelectActionTypes.GetVariantsByCategoryPaging;
    constructor(public payload: any) { }
}

export class GetVariantsByCategoryPagingSuccess implements Action {
    readonly type = QuickSelectActionTypes.GetVariantsByCategoryPagingSuccess;
    constructor(public payload: VariantPagingModel) { }
}

export type QuickSelectActions =
    GetCategoriesPaging
    | GetCategoriesPagingSuccess
    | GetVariantsByCategoryPaging
    | GetVariantsByCategoryPagingSuccess;
