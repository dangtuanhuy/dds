import { NgModule } from '@angular/core';
import { ReceiptComponent } from './receipt.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedPosModule } from 'src/app/shared/shared-pos.module';
import { PosCommonModule } from '../../shared/pos-common.module';
import { ReceiptRefundComponent } from './receipt-refund/receipt-refund.component';
import { ReceiptExchangeComponent } from './receipt-exchange/receipt-exchange.component';

const routes: Routes = [
    {
        path: ':orderId',
        component: ReceiptComponent
    },
    {
        path: ':orderId/refund',
        component: ReceiptRefundComponent
    },
    {
        path: ':orderId/exchange',
        component: ReceiptExchangeComponent
    }
];

@NgModule({
    declarations: [
        ReceiptComponent,
        ReceiptRefundComponent,
        ReceiptExchangeComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        SharedPosModule,
        PosCommonModule
    ]
})
export class ReceiptModule { }
