import { Component, Injector } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { OrderModel } from 'src/app/pos/shared/models/order';
import { PageConstants, CommonConstants } from 'src/app/pos/shared/constants/common.constant';
import { SaleReceiptRefund } from 'src/app/pos/shared/models/device-hub/sale-receipt';
import { DiscountType } from 'src/app/promotion/promotion.model';
import { DeviceHubService } from 'src/app/pos/shared/services/device-hub.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { AppContextManager } from 'src/app/pos/shared/app-context-manager';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { PaymentMethodService } from 'src/app/pos/shared/services/payment-method.service';
import { SystemAppSettingKeys } from 'src/app/pos/shared/constants/appSetting-key.constant';
import { DeviceHubRequest } from 'src/app/pos/shared/models/device-hub/device-hub-request';
import { DeviceCommand } from 'src/app/pos/shared/enums/device-hub/device-command.enum';
import { PriceExtension } from 'src/app/pos/shared/helpers/price.extension';
import { AppSettingService } from 'src/app/pos/shared/services/appSetting.service';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { OrderRefundService } from '../../order-transaction/refund/service/order-refund.service';

@Component({
  selector: 'app-receipt-refund',
  templateUrl: './receipt-refund.component.html',
  styleUrls: ['./receipt-refund.component.scss']
})
export class ReceiptRefundComponent extends ComponentBase {
  public orderId = '';
  public order: OrderModel = null;
  public title = 'Receipt (Refund)';
  public defaultPage = `/${PageConstants.defaultPage}`;
  public pageWidth = `${CommonConstants.contentPageWidth}px`;

  public street = '';
  public createdDate = '';

  public gstInclusive = false;
  public totalAmount = 0;

  public netQuantityTotal = 2;
  public netTotal = 130;

  public exchangeDayAmount = 7;

  public saleReceipt: SaleReceiptRefund;
  public discountType = DiscountType;

  public purchasedItems: OrderItemModel[] = [];
  public refundedItems: OrderItemModel[] = [];
  public totalRefund = 0;
  constructor(
    private deviceHubService: DeviceHubService,
    private _activatedRoute: ActivatedRoute,
    private orderService: OrderService,
    private appContextManager: AppContextManager,
    private notification: NotificationService,
    private appSettingService: AppSettingService,
    private paymentMethodService: PaymentMethodService,
    private orderRefundService: OrderRefundService,
    private router: Router,
    public injector: Injector
  ) {
    super(injector);
    this.orderId = this._activatedRoute.snapshot.paramMap.get('orderId');
  }

  onInit() {
    this.getOrder();
  }
  onDestroy() {

  }

  public onPrint() {
    this.appSettingService.getByKey(SystemAppSettingKeys.deviceHubName).subscribe((appSetting) => {
      if (appSetting) {
        const request = new DeviceHubRequest();
        request.command = DeviceCommand.PrintReceiptRefund;
        request.deviceHubName = appSetting.value;
        request.data = JSON.stringify(this.saleReceipt);

        this.deviceHubService.printEpson(request).subscribe(result => {
          if (result) {
            this.notification.success('Success');
            this.router.navigate([PageConstants.quickSelect]);
          } else {
            this.notification.error('Something went wrong!');
          }
        });
      }
    });
  }

  public onEsc() {
    this.router.navigate([PageConstants.quickSelect]);
  }

  public getOrder() {
    this.orderRefundService.getReceiptRefundByOrderId(this.orderId).subscribe(data => {
      if (!data) {
        return;
      }
      this.purchasedItems = data.purchasedItems;
      this.refundedItems = data.refundedItems;
      this.totalRefund = data.totalRefund;
      this.gstInclusive = data.saleReceiptRefund.gstInclusive;
      this.createdDate = data.createdDateString;
      this.saleReceipt = data.saleReceiptRefund;
      this.netQuantityTotal = data.netQuantityTotal;
      this.totalAmount = data.saleReceiptRefund.totalAmount;
      this.netTotal = data.saleReceiptRefund.netTotal;
    });
  }

  public formatNumberDecimal(value: number) {
    if (isNaN(value)) {
      return '';
    }
    return PriceExtension.round(value, 2).toFixed(2);
  }
}
