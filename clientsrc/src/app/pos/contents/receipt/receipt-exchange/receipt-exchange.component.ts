import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DiscountType } from 'src/app/pos/shared/enums/discount-type.enum';
import { SaleReceiptExchange } from 'src/app/pos/shared/models/device-hub/sale-receipt';
import { PageConstants, CommonConstants } from 'src/app/pos/shared/constants/common.constant';
import { OrderModel } from 'src/app/pos/shared/models/order';
import { DeviceHubService } from 'src/app/pos/shared/services/device-hub.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { SystemAppSettingKeys } from 'src/app/pos/shared/constants/appSetting-key.constant';
import { AppSettingService } from 'src/app/pos/shared/services/appSetting.service';
import { DeviceHubRequest } from 'src/app/pos/shared/models/device-hub/device-hub-request';
import { DeviceCommand } from 'src/app/pos/shared/enums/device-hub/device-command.enum';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { PriceExtension } from 'src/app/pos/shared/helpers/price.extension';
import { OrderExchangeService } from '../../order-transaction/exchange/services/order-exchange.service';

@Component({
  selector: 'app-receipt-exchange',
  templateUrl: './receipt-exchange.component.html',
  styleUrls: ['./receipt-exchange.component.scss']
})
export class ReceiptExchangeComponent implements OnInit {
  public orderId = '';
  public order: OrderModel = null;
  public title = 'Receipt (Exchange)';
  public defaultPage = `/${PageConstants.defaultPage}`;
  public pageWidth = `${CommonConstants.contentPageWidth}px`;

  public street = '';
  public createdDate = '';
  public country = '';
  public phone = '';
  public email = '';
  public GSTNo = '';

  public billno = '';
  public cashierName = '';
  public counter = '';

  public gstInclusive = false;
  public totalAmount = 0;
  public gstInclusiveAmount = 0;
  public gstAmount = 0;

  public netQuantityTotal = 2;
  public netTotal = 130;

  public exchangeDayAmount = 7;

  public saleReceipt: SaleReceiptExchange;
  public discountType = DiscountType;

  public purchasedItems: OrderItemModel[];
  public exchangedItems: OrderItemModel[];
  public newItems: OrderItemModel[];

  constructor(
    private deviceHubService: DeviceHubService,
    private _activatedRoute: ActivatedRoute,
    private notification: NotificationService,
    private appSettingService: AppSettingService,
    private orderExchangeService: OrderExchangeService,
    private router: Router,
  ) {
    this.orderId = this._activatedRoute.snapshot.paramMap.get('orderId');
  }

  ngOnInit() {
    this.getOrder();
  }

  public onPrint() {
    let deviceHubName = '';
    this.appSettingService.getByKey(SystemAppSettingKeys.deviceHubName).subscribe((appSetting) => {
      if (appSetting) {
        deviceHubName = appSetting.value;
      }
    });

    const request = new DeviceHubRequest();
    request.command = DeviceCommand.PrintReceiptExchange;
    request.deviceHubName = deviceHubName;
    request.data = JSON.stringify(this.saleReceipt);

    this.deviceHubService.printEpson(request).subscribe(result => {
      if (result) {
        this.notification.success('Success');
        this.router.navigate([PageConstants.quickSelect]);
      } else {
        this.notification.error('Something went wrong!');
      }
    });
  }

  public onEsc() {
    this.router.navigate([PageConstants.quickSelect]);
  }

  public getOrder() {
    this.orderExchangeService.getReceiptExchangeByOrderId(this.orderId).subscribe(data => {
      if (!data) {
        return;
      }

      this.purchasedItems = data.purchasedItems;
      this.exchangedItems = data.exchangedItems;
      this.newItems = data.newItems;

      this.createdDate = data.createdDateString;

      this.netQuantityTotal = data.netQuantityTotal;

      this.saleReceipt = data.saleReceiptExchange;
      this.totalAmount = this.saleReceipt.totalAmount;
      this.netTotal = this.saleReceipt.netTotal;
      this.gstInclusive = this.saleReceipt.gstInclusive;
      this.gstInclusiveAmount = this.saleReceipt.gstInclusiveAmount;
      this.gstAmount = this.saleReceipt.gstNoInclusiveAmount;
      this.billno = this.saleReceipt.billNo;
      this.cashierName = this.saleReceipt.cashierName;
      this.counter = this.saleReceipt.counterName;
    });
  }

  private calculateNetQuantityTotal() {
    let total = 0;
    if (this.purchasedItems && this.purchasedItems.length > 0) {
      this.purchasedItems.forEach(x => total += x.quantity);
    }

    if (this.newItems && this.newItems.length > 0) {
      this.newItems.forEach(x => total += x.quantity);
    }
    return total;
  }

  public formatNumberDecimal(value: number) {
    return PriceExtension.round(value, 2).toFixed(2);
  }

  private mapToDraftReceipt() {
    this.createdDate = this.formatReceiptDateString(this.order.createdDate);
  }

  private formatReceiptDateString(localTime: Date): string {
    if (!localTime) {
      return '';
    }
    let result = `${localTime.getDate()}/${localTime.getMonth() + 1}/${localTime.getFullYear()}`;
    result += ` ${localTime.getHours()}:${localTime.getMinutes()}`;
    return result;
  }

}
