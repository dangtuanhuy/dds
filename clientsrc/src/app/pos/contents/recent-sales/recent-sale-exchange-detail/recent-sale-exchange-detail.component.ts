import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromRecentSaleState from '../state/recent-sales.reducer';
import * as fromRecentSaleActions from '../state/recent-sales.action';
import * as recentSaleSelector from '../state/index';
import { OrderModel, Order } from 'src/app/pos/shared/models/order';
import { SaleItemType } from 'src/app/pos/shared/enums/sale-item-type.enum';
import { DiscountType } from 'src/app/pos/shared/enums/discount-type.enum';
import { PriceExtension } from 'src/app/pos/shared/helpers/price.extension';
import { OrderItemStatus } from 'src/app/pos/shared/enums/order-item-status.enum';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { OrderPromotion } from 'src/app/pos/shared/models/order-promotion';
import { PageConstants, CommonConstants } from 'src/app/pos/shared/constants/common.constant';
import { OrderService } from 'src/app/pos/shared/services/order.service';

@Component({
  selector: 'app-recent-sale-exchange-detail',
  templateUrl: './recent-sale-exchange-detail.component.html',
  styleUrls: ['./recent-sale-exchange-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RecentSaleExchangeDetailComponent implements OnInit, OnDestroy {
  public title = 'Recent Sale Detail (Exchange)';
  public defaultPage = `/${PageConstants.defaultPage}`;
  public pageWidth = `${CommonConstants.contentPageWidth}px`;
  public sale = new OrderModel();
  public previousSale: Order;
  public nextSale: Order;
  public dataSource = [];

  private saleId = '';
  private subscriptions: Subscription[] = [];

  constructor(
    private _activatedRoute: ActivatedRoute,
    private recentSaleStore: Store<fromRecentSaleState.RecentSalesState>,
    private orderService: OrderService,
    private router: Router
  ) {
    this.saleId = this._activatedRoute.snapshot.paramMap.get('saleId');
  }

  ngOnInit() {
    this.initGetSelectors();
    this.getRelatedSale();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  public formatNumberDecimal(value: number) {
    return value.toFixed(2);
  }

  public onClickExchange() {

  }

  public onClickRefund() {

  }

  public formatDate(date: Date) {
    return [date.getMonth() + 1, date.getDate(), date.getFullYear()].join('/')
      + ' ' + [date.getHours(), date.getMinutes(), date.getSeconds()].join(':');
  }

  public onClickReprint() {
    this.router.navigate([`${PageConstants.receipt}/${this.saleId}/exchange`]);
  }

  public onChangeSale(event) {
    this.saleId = event.saleId;
    this.recentSaleStore.dispatch(new fromRecentSaleActions.GetRecentSale(this.saleId));
    this.getRelatedSale();
  }

  private initGetSelectors() {
    this.subscriptions.push(
      this.recentSaleStore.pipe(select(recentSaleSelector.getRecentSale)).subscribe(sale => {
        if (sale) {
          this.sale = sale;

          this.buildDataSourceForProductsAndPromotions();
        }
      })
    );

    this.recentSaleStore.dispatch(new fromRecentSaleActions.GetRecentSale(this.saleId));
  }

  private buildDataSourceForProductsAndPromotions() {
    const newDataSource = [];
    const orderItems = this.sale.orderItems;
    const purchasedItems = orderItems.filter(x => x.status === OrderItemStatus.Old);
    const exchangeItems = orderItems.filter(x => x.status === OrderItemStatus.Exchange);
    const newItems = orderItems.filter(x => x.status === OrderItemStatus.Normal);

    this.mapOrderItemsToDataSource('New Items', newDataSource, newItems);
    this.mapOrderItemsToDataSource('Purchased Items', newDataSource, purchasedItems);
    this.mapOrderItemsToDataSource('Exchange Items', newDataSource, exchangeItems, SaleItemType.ExchangeItem, SaleItemType.ExchangeItemPromotion);

    const orderTotalPrice = this.calculateDatasourceTotalPrice(newDataSource);
    this.mapPromotionsToDataSource(newDataSource, orderTotalPrice, this.sale.orderPromotions);

    this.dataSource = newDataSource;
  }

  private calculateDatasourceTotalPrice(datasource: any[]) {
    const filteredSource = datasource.filter(x => x.type === SaleItemType.Product || x.type === SaleItemType.ItemPromotion);
    let totalPrice = 0;
    filteredSource.forEach(element => {
      totalPrice += element.amount;
    });

    return totalPrice;
  }

  private mapOrderItemsToDataSource(title: string, dataSource: any[], orderItems: OrderItemModel[],
    itemType = SaleItemType.Product, itemPromotionType = SaleItemType.ItemPromotion, proportion = 1) {
    if (dataSource.length > 0) {
      dataSource.push({
        description: '',
        quantity: '',
        price: '',
        decimalAmount: ''
      });
    }

    dataSource.push({
      description: title,
      quantity: '',
      price: '',
      decimalAmount: ''
    });

    orderItems.forEach(orderItem => {
      const amount = orderItem.quantity * orderItem.price;
      dataSource.push({
        description: orderItem.variant,
        quantity: orderItem.quantity * proportion,
        price: this.formatNumberDecimal(orderItem.price * proportion),
        amount: amount * proportion,
        decimalAmount: this.formatNumberDecimal(amount),
        type: itemType
      });

      const itemPromotions = orderItem.orderItemPromotions;
      itemPromotions.forEach(itemPromotion => {
        const data = {
          description: 'Item discount',
          quantity: '',
          price: '',
          amount: 0,
          decimalAmount: '',
          type: itemPromotionType
        };
        let itemPromotionAmount = 0;
        if (itemPromotion.discountType === DiscountType.Money) {
          itemPromotionAmount = -1 * itemPromotion.value;
        }
        if (itemPromotion.discountType === DiscountType.Percent) {
          data.description += ` (${itemPromotion.value}%)`;
          itemPromotionAmount = -1 * PriceExtension.round(itemPromotion.value * orderItem.price * orderItem.quantity / 100, 2);
        }
        data.amount = itemPromotionAmount;
        data.decimalAmount = this.formatNumberDecimal(itemPromotionAmount);

        dataSource.push(data);
      });
    });
  }

  private mapPromotionsToDataSource(dataSource: any[], orderTotalPrice: number, orderPromotions: OrderPromotion[]) {
    if (orderPromotions.length === 0) {
      return;
    }

    dataSource.push({
      description: '',
      quantity: '',
      price: '',
      amount: ''
    });

    dataSource.push({
      description: 'Promotions',
      quantity: '',
      price: '',
      amount: ''
    });

    orderPromotions.forEach(orderPromotion => {
      const data = {
        description: orderPromotion.reason,
        quantity: '',
        price: '',
        amount: '',
        decimalAmount: '',
        type: SaleItemType.Promotion
      };

      let price = -1 * orderPromotion.value;

      if (orderPromotion.discountType === DiscountType.Percent) {
        data.description += ` (${orderPromotion.value}%)`;
        price = -1 * PriceExtension.round(orderTotalPrice * orderPromotion.value / 100, 2);
      }

      data.decimalAmount = this.formatNumberDecimal(price);

      dataSource.push(data);
    });
  }

  private getRelatedSale() {
    this.orderService.getPreviousAndNextOrders(this.sale.id, this.sale.previousOrderId, '').subscribe(x => {
      this.previousSale = x.previousOrder;
      this.nextSale = x.nextOrder;
    });
  }
}
