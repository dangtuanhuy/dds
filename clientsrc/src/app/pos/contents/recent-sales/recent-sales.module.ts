import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedPosModule } from 'src/app/shared/shared-pos.module';
import { RecentSalesComponent } from './recent-sales.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer } from './state/recent-sales.reducer';
import { RecentSalesEffects } from './state/recent-sales.effect';
import { recentSalesFeatureName } from './state';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RecentSaleDetailComponent } from './recent-sale-detail/recent-sale-detail.component';
import { PosCommonModule } from '../../shared/pos-common.module';
import { RecentSaleRefundDetailComponent } from './recent-sale-refund-detail/recent-sale-refund-detail.component';
import { RecentSaleExchangeDetailComponent } from './recent-sale-exchange-detail/recent-sale-exchange-detail.component';
import { RecentSaleDetailActionsComponent } from './recent-sale-detail-actions/recent-sale-detail-actions.component';
import { RecentSaleDetailHeaderComponent } from './recent-sale-detail-header/recent-sale-detail-header.component';

const routes: Routes = [
    {
        path: '',
        component: RecentSalesComponent
    },
    {
        path: ':saleId/detail',
        component: RecentSaleDetailComponent
    },
    {
        path: ':saleId/detail/exchange',
        component: RecentSaleExchangeDetailComponent
    },
    {
        path: ':saleId/detail/refund',
        component: RecentSaleRefundDetailComponent
    }
];

@NgModule({
    declarations: [
        RecentSalesComponent,
        RecentSaleDetailComponent,
        RecentSaleRefundDetailComponent,
        RecentSaleExchangeDetailComponent,
        RecentSaleDetailActionsComponent,
        RecentSaleDetailHeaderComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        SharedPosModule,
        StoreModule.forFeature(recentSalesFeatureName, reducer),
        EffectsModule.forFeature([RecentSalesEffects]),
        NgxDatatableModule,
        PosCommonModule
    ],
    providers: [

    ],
    exports: [
    ]
})
export class RecentSalesModule { }
