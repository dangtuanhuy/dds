import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PageConstants } from 'src/app/pos/shared/constants/common.constant';
import { OrderRefundService } from '../../order-transaction/refund/service/order-refund.service';
import { OrderExchangeService } from '../../order-transaction/exchange/services/order-exchange.service';
import { OrderItemStatus } from 'src/app/pos/shared/enums/order-item-status.enum';
import { OrderModel } from 'src/app/pos/shared/models/order';
import { OrderTransactionType } from 'src/app/pos/shared/enums/order-transaction-type.enum';
import { OrderService } from 'src/app/pos/shared/services/order.service';

@Component({
  selector: 'app-recent-sale-detail-actions',
  templateUrl: './recent-sale-detail-actions.component.html',
  styleUrls: ['./recent-sale-detail-actions.component.scss']
})
export class RecentSaleDetailActionsComponent implements OnInit {
  @Input() sale = new OrderModel();

  public enableExchangeRefund = false;

  constructor(
    private router: Router,
    private orderRefundService: OrderRefundService,
    private orderExchangeService: OrderExchangeService,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.checkEnableExchangeRefund();
  }

  public onClickReprint() {
    switch (this.sale.orderTransactionType) {
      case OrderTransactionType.Normal: {
        this.router.navigate([`${PageConstants.receipt}/${this.sale.id}`]);
        break;
      }
      case OrderTransactionType.Refund: {
        this.router.navigate([`${PageConstants.receipt}/${this.sale.id}/refund`]);
        break;
      }
      case OrderTransactionType.Exchange: {
        this.router.navigate([`${PageConstants.receipt}/${this.sale.id}/exchange`]);
        break;
      }
      default: {
        break;
      }
    }
  }

  public onClickRefund() {
    this.orderRefundService.resetState();
    this.orderRefundService.addCurrentOrder(this.sale);
    this.orderRefundService.setCurrentItems(
      this.sale.orderItems.filter(x => x.status === OrderItemStatus.Old || x.status === OrderItemStatus.Normal)
    );
    this.orderRefundService.setCurrentPromotions(this.sale.orderPromotions);

    this.router.navigate([`${PageConstants.orderRefund}/sale/${this.sale.id}`]);
  }

  public onClickExchange() {
    this.orderExchangeService.addCurrentOrder(this.sale);
    this.orderExchangeService.setCurrentItems(
      this.sale.orderItems.filter(x => x.status === OrderItemStatus.Old || x.status === OrderItemStatus.Normal)
    );
    this.orderExchangeService.setCurrentPromotions(this.sale.orderPromotions);

    this.router.navigate([`${PageConstants.orderExchange}/sale/${this.sale.id}`]);
  }

  private checkEnableExchangeRefund() {
    this.orderService.getLatestOrderByOriginalOrderId(this.sale.originalOrderId).subscribe(order => {
      if (order.id === this.sale.id) {
        this.enableExchangeRefund = true;
      }
    });
  }
}
