import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { OrderExchangeService } from '../../order-transaction/exchange/services/order-exchange.service';
import { PageConstants, CommonConstants } from 'src/app/pos/shared/constants/common.constant';
import { OrderModel, Order } from 'src/app/pos/shared/models/order';
import * as fromRecentSaleState from '../state/recent-sales.reducer';
import * as fromRecentSaleActions from '../state/recent-sales.action';
import * as recentSaleSelector from '../state/index';
import { SaleItemType } from 'src/app/pos/shared/enums/sale-item-type.enum';
import { DiscountType } from 'src/app/pos/shared/enums/discount-type.enum';
import { PriceExtension } from 'src/app/pos/shared/helpers/price.extension';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { OrderItemStatus } from 'src/app/pos/shared/enums/order-item-status.enum';
import { OrderRefundService } from '../../order-transaction/refund/service/order-refund.service';
import { OrderService } from 'src/app/pos/shared/services/order.service';

@Component({
    selector: 'app-recent-sale-refund-detail',
    templateUrl: './recent-sale-refund-detail.component.html',
    styleUrls: ['./recent-sale-refund-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RecentSaleRefundDetailComponent extends ComponentBase {
    public title = 'Recent Sale Detail (Refund)';
    public defaultPage = `/${PageConstants.defaultPage}`;
    public pageWidth = `${CommonConstants.contentPageWidth}px`;
    public sale = new OrderModel();
    public previousSale: Order;
    public nextSale: Order;
    public refund = 0;

    public dataSource = [];

    private saleId = '';

    public purchaseOrderItems: OrderItemModel[] = [];
    public refundOrderItems: OrderItemModel[] = [];
    constructor(
        private router: Router,
        private _activatedRoute: ActivatedRoute,
        private recentSaleStore: Store<fromRecentSaleState.RecentSalesState>,
        private orderExchangeService: OrderExchangeService,
        private orderRefundService: OrderRefundService,
        private orderService: OrderService,
        public injector: Injector
    ) {
        super(injector);
        this.saleId = this._activatedRoute.snapshot.paramMap.get('saleId');
    }
    onInit() {
        this.initGetSelectors();
        this.getRelatedSale();
    }
    onDestroy() {
    }
    public onClickReprint() {
        this.router.navigate([`${PageConstants.receipt}/${this.saleId}/refund`]);
    }

    public onClickRefund() {
        this.orderRefundService.addCurrentOrder(this.sale);
        this.orderRefundService.addCurrentItem(this.sale.orderItems);
        this.orderRefundService.addCurrentPromotion(this.sale.orderPromotions);
        this.router.navigate([`${PageConstants.orderRefund}/${this.saleId}`]);
    }

    public onClickExchange() {
        this.orderExchangeService.addCurrentOrder(this.sale);
        this.orderExchangeService.addCurrentItem(this.sale.orderItems);
        this.orderExchangeService.addCurrentPromotion(this.sale.orderPromotions);
        this.router.navigate([`${PageConstants.orderExchange}/${this.saleId}`]);
    }

    public formatNumberDecimal(value: number) {
        if (isNaN(value) || !value) {
            return '';
        }
        return value.toFixed(2);
    }

    public formatDate(date: Date) {
        if (!date) {
            return '';
        }
        return [date.getMonth() + 1, date.getDate(), date.getFullYear()].join('/')
            + ' ' + [date.getHours(), date.getMinutes(), date.getSeconds()].join(':');
    }

    public onChangeSale(event) {
        this.saleId = event.saleId;
        this.recentSaleStore.dispatch(new fromRecentSaleActions.GetRecentSale(this.saleId));
        this.getRelatedSale();
    }

    private initGetSelectors() {
        this.handleSubscription(
            this.recentSaleStore.pipe(select(recentSaleSelector.getRecentSale)).subscribe(sale => {
                if (sale) {
                    this.sale = sale;
                    this.refund = -1 * sale.collect;

                    this.buildDataSourceForProductsAndPromotions();
                }
            })
        );

        this.recentSaleStore.dispatch(new fromRecentSaleActions.GetRecentSale(this.saleId));
    }

    private buildDataSourceForProductsAndPromotions() {
        let newDataSource = [];
        const purchaseItems = this.sale.orderItems.filter(x => x.status === OrderItemStatus.Old);
        const refundItems = this.sale.orderItems.filter(x => x.status === OrderItemStatus.Refund);
        const titlePurchaseItems = {
            description: 'PURCHASED ITEMS',
            quantity: null,
            price: null,
            amount: null,
            type: null
        };
        const titleRefundItems = {
            description: 'REFUNDED ITEMS',
            quantity: null,
            price: null,
            amount: null,
            type: null
        };
        const blankItem = {
            description: '',
            quantity: null,
            price: null,
            amount: null,
            type: null
        };
        newDataSource = [...this.buildDataSource(titlePurchaseItems, purchaseItems), blankItem,
        ...this.buildDataSource(titleRefundItems, refundItems, SaleItemType.RefundItem, SaleItemType.RefundItemPromotion)];

        const orderTotalPrice = this.calculateDatasourceTotalPrice(newDataSource);

        if (this.sale.orderPromotions.length > 0) {
            newDataSource.push(blankItem);
            newDataSource.push({
                description: 'PROMOTIONS',
                quantity: null,
                price: null,
                amount: null,
                type: null
            });

            this.sale.orderPromotions.forEach(orderPromotion => {
                const data = {
                    description: orderPromotion.reason,
                    quantity: '',
                    price: -1 * orderPromotion.value,
                    type: SaleItemType.Promotion
                };

                if (orderPromotion.discountType === DiscountType.Percent) {
                    data.description += ` (${orderPromotion.value}%)`;
                    data.price = -1 * PriceExtension.round(orderTotalPrice * orderPromotion.value / 100, 2);
                }

                newDataSource.push(data);
            });
        }

        this.dataSource = newDataSource;
    }

    private buildDataSource(title: any, items: any, itemType = SaleItemType.Product, itemTypePromotion = SaleItemType.ItemPromotion) {
        const newDataSource = [];
        newDataSource.push(title);
        items.forEach(item => {
            newDataSource.push({
                description: item.variant,
                quantity: item.quantity,
                price: item.price,
                amount: item.quantity * item.price,
                type: itemType
            });

            const orderItemPromotions = item.orderItemPromotions;
            if (Array.isArray(orderItemPromotions)) {
                orderItemPromotions.forEach(itemPromotion => {
                    const data = {
                        description: 'Item discount',
                        quantity: '',
                        price: 0,
                        amount: 0,
                        type: itemTypePromotion
                    };

                    if (itemPromotion.discountType === DiscountType.Money) {
                        data.price = -1 * itemPromotion.value;
                    }
                    if (itemPromotion.discountType === DiscountType.Percent) {
                        data.description += ` (${itemPromotion.value}%)`;
                        data.price = -1 * PriceExtension.round(itemPromotion.value * item.price * item.quantity / 100, 2);
                    }
                    data.amount = data.price;

                    newDataSource.push(data);
                });
            }
        });
        return newDataSource;
    }

    private calculateDatasourceTotalPrice(datasource: any[]) {
        const filteredSource = datasource.filter(x => x.type === SaleItemType.Product || x.type === SaleItemType.ItemPromotion);
        let totalPrice = 0;
        filteredSource.forEach(element => {
            totalPrice += element.amount;
        });

        return totalPrice;
    }

    private getRelatedSale() {
        this.orderService.getPreviousAndNextOrders(this.sale.id, this.sale.previousOrderId, '').subscribe(x => {
            this.previousSale = x.previousOrder;
            this.nextSale = x.nextOrder;
        });
    }
}
