import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { OrderModel, Order } from 'src/app/pos/shared/models/order';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PageConstants } from 'src/app/pos/shared/constants/common.constant';
import { OrderTransactionType } from 'src/app/pos/shared/enums/order-transaction-type.enum';

@Component({
  selector: 'app-recent-sale-detail-header',
  templateUrl: './recent-sale-detail-header.component.html',
  styleUrls: ['./recent-sale-detail-header.component.scss']
})
export class RecentSaleDetailHeaderComponent implements OnInit, OnDestroy {
  @Input() sale = new OrderModel();
  @Input() previousSale: Order;
  @Input() nextSale: Order;
  @Output() changeSale = new EventEmitter<any>();

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.changeSale.complete();
  }

  public formatNumberDecimal(value: number) {
    if (isNaN(value) || !value) {
      return '';
    }
    return value.toFixed(2);
  }

  public formatDate(date: Date) {
    if (!date) {
      return '';
    }
    return [date.getMonth() + 1, date.getDate(), date.getFullYear()].join('/')
      + ' ' + [date.getHours(), date.getMinutes(), date.getSeconds()].join(':');
  }

  public goToRecentSaleDetail(saleId: string, orderTransactionType: OrderTransactionType) {
    if (orderTransactionType === this.sale.orderTransactionType) {
      this.changeSale.emit({ saleId, orderTransactionType });
      return;
    }

    switch (orderTransactionType) {
      case OrderTransactionType.Refund:
        this.router.navigateByUrl(`${PageConstants.recentSales}/${saleId}/detail/refund`);
        return;
      case OrderTransactionType.Exchange:
        this.router.navigate([`${PageConstants.recentSales}/${saleId}/detail/exchange`]);
        return;
      default:
        this.router.navigate([`${PageConstants.recentSales}/${saleId}/detail`]);
        return;
    }
  }
}
