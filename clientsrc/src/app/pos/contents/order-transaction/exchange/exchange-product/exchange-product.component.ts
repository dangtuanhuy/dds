import { ComponentBase } from 'src/app/shared/components/component-base';
import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { OrderExchangeService } from '../services/order-exchange.service';
import { PageInputId, CommonConstants, PageConstants } from 'src/app/pos/shared/constants/common.constant';
import { Variant } from 'src/app/pos/shared/models/variant';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { VariantService } from 'src/app/pos/shared/services/variant.service';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { OrderItemStatus } from 'src/app/pos/shared/enums/order-item-status.enum';
import { DiscountType } from 'src/app/pos/shared/enums/discount-type.enum';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Guid } from 'src/app/shared/utils/guid.util';
import { OrderTransactionService } from 'src/app/pos/shared/services/order-transaction.service';
import { Order, OrderModel } from 'src/app/pos/shared/models/order';
import { OrderTransactionType } from 'src/app/pos/shared/enums/order-transaction-type.enum';
import { AppContextManager } from 'src/app/pos/shared/app-context-manager';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { PriceExtension } from 'src/app/pos/shared/helpers/price.extension';
import { OrderPromotion } from 'src/app/pos/shared/models/order-promotion';
import { ExchangeViewStatus } from 'src/app/pos/shared/enums/exchange-view-status';
import { DeviceHubService } from 'src/app/pos/shared/services/device-hub.service';
import { DeviceCommand } from 'src/app/pos/shared/enums/device-hub/device-command.enum';
import { PosCustomerServerService } from 'src/app/pos/shared/services/server/pos-customer-server.service';
import { CustomerMembershipModel } from 'src/app/pos/shared/models/customer-membership.model';
import { MembershipTypeId } from 'src/app/pos/shared/enums/membership-type.enum';
import { ProductPriceType } from 'src/app/pos/shared/enums/product-price-type.enum';
import { AppSettingService } from 'src/app/pos/shared/services/appSetting.service';
import { SystemAppSettingKeys } from 'src/app/pos/shared/constants/appSetting-key.constant';
import { DeviceHubRequest } from 'src/app/pos/shared/models/device-hub/device-hub-request';


@Component({
    selector: 'app-exchange-product',
    templateUrl: './exchange-product.component.html',
    styleUrls: ['./exchange-product.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ExchangeProductComponent extends ComponentBase {
    public title = 'Exchange';
    public defaultPage = '';
    public pageWidth = `${CommonConstants.contentPageWidth}px`;

    public productSearchId = '';
    public productExchangeQuantityId = '';
    public dataSource = [];
    public filteredProducts: Variant[] = [];
    public selectProducts: Variant[] = [];
    public searchProductText = '';

    public newItems: OrderItemModel[] = [];
    public purchaseItems: OrderItemModel[] = [];
    public refundItems: OrderItemModel[] = [];
    public currentPromotions: OrderPromotion[] = [];
    public newPromotions: OrderPromotion[] = [];
    public discountType = DiscountType;

    public totalAmount = 0;
    public totalNet = 0;
    public isOfflineMode = false;

    public currentProductPriceType = ProductPriceType.ListPrice;

    public newProductPriceInformations = [];

    public inputQuantityStyle = {
        'height': '30px',
        'min-width': '20px',
        'max-width': '50px',
        'padding-left': '2px',
        'border': 'none',
        'border-color': 'transparent',
        'text-align': 'center',
        'margin': '0 2px'
    };

    public collect = 0;
    public exchangeViewStatus = ExchangeViewStatus;

    private customerMembership: CustomerMembershipModel;

    private currentOrder: OrderModel;

    private searchProductTextSubject = new Subject<string>();

    constructor(private orderExchangeService: OrderExchangeService,
        private router: Router,
        private variantService: VariantService,
        private notificationService: NotificationService,
        private orderTransactionService: OrderTransactionService,
        private orderService: OrderService,
        private appContextManager: AppContextManager,
        private deviceHubService: DeviceHubService,
        private posCustomerServerService: PosCustomerServerService,
        private appSettingService: AppSettingService,
        public injector: Injector) {
        super(injector);
    }
    onInit() {
        this.initData();
        this.initCustomerMembership();
        this.variantService.searchVariants(this.searchProductTextSubject).subscribe((result: any) => {
            if (result) {
                const fromBarCode = result.fromBarCode;
                const data = result.data;
                if (fromBarCode && data.length === 1) {
                    this.searchProductText = '';
                    this.searchProductTextSubject.next('');
                    return;
                }
                this.filteredProducts = data;
            }
        });
        this.buildDataSourceForProductsAndPromotions();
    }

    onDestroy() {
    }

    getRowClass(row) {
        return row.type === null ? { 'header-title-back-ground-color': true }
            : { 'header-product-back-ground-color': row.type === ExchangeViewStatus.ItemProduct ? true : false };
    }

    public onSearchProduct(value: string) {
        if (!value) {
            this.filteredProducts = [];
        }

        this.searchProductText = value;
        this.searchProductTextSubject.next(this.searchProductText);
    }

    public onSelectProduct(variant: Variant) {
        const product = this.newItems.find(x => x.variantId === variant.variantId);
        if (product) {
            this.notificationService.warning('Duplicate product');
        } else {
            const price = this.currentProductPriceType === ProductPriceType.MemberPrice
                ? variant.memberPrice
                : variant.listPrice;
            const newItem: OrderItemModel = {
                id: '',
                isDelete: variant.isDelete,
                variant: variant.description,
                variantName: variant.description,
                description: variant.description,
                orderId: '',
                variantId: variant.variantId,
                priceId: variant.priceId,
                stockTypeId: '',
                quantity: 1,
                price: price,
                amount: 0,
                status: OrderItemStatus.Exchange,
                previousOrderItemId: null,
                skuCode: '',
                orderItemPromotions: []
            };

            this.updateProductPriceInformation(variant.variantId, variant.listPrice, variant.memberPrice);

            this.orderExchangeService.addNewItem(newItem);
            this.orderExchangeService.addSelectProduct(variant);
            this.buildDataSourceForProductsAndPromotions();
            this.recalculateTotalAndCollect();
        }
    }

    public onSelectPromotion() {
        this.router.navigate(['order-exchange/promotions']);
    }

    public onSelectCollect() {
        this.orderExchangeService.collect = this.collect;
        this.orderExchangeService.amount = this.totalNet;
        this.newItems = this.newItems.map(newItem => {
            newItem.id = Guid.newGuid();
            newItem.status = OrderItemStatus.Exchange;
            newItem.amount = this.calculateAmount([newItem]);
            newItem.stockTypeId = Guid.empty();
            return newItem;
        });
        this.calculateAmount(this.refundItems);
        if (this.collect > 0) {
            this.router.navigate(['order-exchange/payment']);
        } else {
            const currentOrder = this.orderExchangeService.getCurrentOrder();
            const exchangeOrder: Order = {
                id: Guid.newGuid(),
                customerId: currentOrder.customerId,
                amount: this.orderExchangeService.amount,
                gst: currentOrder.gst,
                gstInclusive: currentOrder.gstInclusive,
                billNo: '',
                change: currentOrder.change,
                createdDate: new Date(),
                cashierId: this.appContextManager.currentUser.id,
                isDelete: false,
                synced: false,
                orderTransactionType: OrderTransactionType.Exchange,
                previousOrderId: currentOrder.id,
                originalOrderId: currentOrder.originalOrderId,
                reason: this.orderExchangeService.reason,
                collect: this.collect
            };
            this.orderService.generateOrderBillNo().subscribe(billNo => {
                exchangeOrder.billNo = billNo;
            });
            this.newItems.forEach(newItem => {
                newItem.orderItemPromotions.forEach(x => {
                    x.orderItemId = newItem.id;
                });
            });
            const exchangeOrderPromotions = this.currentPromotions.concat(this.newPromotions);

            this.orderTransactionService.exchangeOrder(exchangeOrder, currentOrder,
                this.purchaseItems, this.refundItems,
                this.newItems, exchangeOrderPromotions, []).subscribe(
                    result => {
                        if (result) {
                            this.notificationService.success('Exchange Success');
                            this.printReceiptsForExchange(result.id);
                            this.openCashDrawer();
                            this.router.navigate([PageConstants.quickSelect]);
                        } else {
                            this.notificationService.error('Something went wrong!');
                        }
                    }
                );
        }
    }

    public onDelete(row) {
        let index = null;
        switch (row.status) {
            case ExchangeViewStatus.NewItem:
                this.onDeleteProduct(row.variantId);
                break;
            case ExchangeViewStatus.CurrentPromotion:
                index = this.currentPromotions.findIndex(x => x.discountType === row.type
                    && x.value === row.value
                    && row.description.includes(x.reason));
                this.onDeleteCurrentPromotion(index);
                break;
            case ExchangeViewStatus.NewPromotion:
                index = this.newPromotions.findIndex(x => x.discountType === row.type
                    && x.value === row.value
                    && row.description.includes(x.reason));
                this.onDeleteNewPromotion(index);
                break;
            case ExchangeViewStatus.PromotionNewItem:
                const newProductIndex = this.newItems.findIndex(x => x.variantId === row.variantId);
                const promotionItemIndex = this.newItems[newProductIndex].orderItemPromotions.findIndex(x => x.discountType === row.type
                    && x.value === row.value);
                this.onDeleteItemPromotion(newProductIndex, promotionItemIndex);
                break;
        }
        this.buildDataSourceForProductsAndPromotions();
    }

    private updateProductPriceInformation(variantId: string, listPrice: number, memberPrice: number) {
        const existedProductPriceInformation = this.newProductPriceInformations.find(x => x.variantId === variantId);
        if (existedProductPriceInformation) {
            existedProductPriceInformation.listPrice = listPrice;
            existedProductPriceInformation.memberPrice = memberPrice;
        } else {
            this.newProductPriceInformations.push({
                variantId: variantId,
                listPrice: listPrice,
                memberPrice: memberPrice
            });
        }
    }

    private openCashDrawer() {
        this.deviceHubService.openCashDrawer(this.appContextManager.getPrinterShareName()).subscribe();
    }

    private printReceiptsForExchange(orderId: string) {
        this.orderExchangeService.getReceiptExchangeByOrderId(orderId).subscribe(data => {
            if (!data) {
                return;
            }

            this.appSettingService.getByKey(SystemAppSettingKeys.deviceHubName).subscribe((appSetting) => {
                if (appSetting) {
                    const request = new DeviceHubRequest();
                    request.command = DeviceCommand.PrintReceiptExchange;
                    request.deviceHubName = appSetting.value;
                    request.data = JSON.stringify(data.saleReceiptExchange);

                    this.deviceHubService.printEpson(request).subscribe(x => {
                        this.deviceHubService.printEpson(request).subscribe();
                    });
                }
            });
        });
    }

    private onDeleteProduct(variantId: string) {
        const newItemindex = this.newItems.findIndex(x => x.variantId === variantId);
        this.orderExchangeService.deleteNewItem(newItemindex);
        this.orderExchangeService.deleteSelectProduct(variantId);
        this.recalculateTotalAndCollect();
    }

    private onDeleteCurrentPromotion(index: number) {
        this.orderExchangeService.deleteCurrentPromotion(index);
        this.recalculateTotalAndCollect();
    }

    private onDeleteNewPromotion(index: number) {
        this.orderExchangeService.deleteNewPromotion(index);
        this.recalculateTotalAndCollect();
    }

    private initCustomerMembership() {
        if (!this.currentOrder.customerId) {
            return;
        }
        
        this.appContextManager.detectServerAvailable().subscribe(serverIsAvailable => {
            if (serverIsAvailable) {
                this.posCustomerServerService.getCustomerMemberShip(this.appContextManager.getDeviceCode(), this.currentOrder.customerId)
                    .subscribe((customerMembership: CustomerMembershipModel) => {
                        if (!customerMembership) {
                            this.applyCustomerMembersip(true);
                            return;
                        }

                        this.customerMembership = customerMembership;
                        this.currentProductPriceType = this.customerMembership.membershipType.id === MembershipTypeId.Basic
                            ? ProductPriceType.ListPrice
                            : ProductPriceType.MemberPrice;
                    });

                this.applyCustomerMembersip(false);
                return;
            }

            this.applyCustomerMembersip(true);
        }, error => {
            this.applyCustomerMembersip(true);
        });
    }
    public applyCustomerMembersip(isOfflineMode: boolean) {
        this.isOfflineMode = isOfflineMode;
    }

    private onDeleteItemPromotion(itemIndex: number, itemPromotionIndex: number) {
        this.newItems[itemIndex].orderItemPromotions.splice(itemPromotionIndex, 1);
        this.recalculateTotalAndCollect();
    }

    public onInPutNumberPlus(row: OrderItemModel) {
        this.onChangeQuantity(row.quantity + 1, row);
    }

    public onInPutNumberMinus(row: OrderItemModel) {
        if (row.quantity - 1 >= 0) {
            this.onChangeQuantity(row.quantity - 1, row);
        }
    }

    public onChangeQuantity(event, row: OrderItemModel) {
        if (!event) {
            return;
        }
        const inputQuantity = Number(event);
        const productQuantity = this.selectProducts.find(x => x.variantId === row.variantId).quantity;
        if (inputQuantity - productQuantity > 0) {
            this.notificationService.warning(`The quantity is not enough!`);
            row.quantity = productQuantity;
        } else {
            row.quantity = inputQuantity;
        }
        this.newItems.find(x => x.variantId === row.variantId).quantity = row.quantity;
        this.recalculateTotalAndCollect();
    }

    public calculateAmount(items: OrderItemModel[]): number {
        let amount = 0;
        items.forEach(item => {
            const amountItem = item.price * item.quantity;
            const discountPrices = item.orderItemPromotions.map(promotion => {
                if (promotion.discountType === DiscountType.Money) {
                    return PriceExtension.round(-1 * promotion.value, 2);
                }

                if (promotion.discountType === DiscountType.Percent) {
                    const value = -1 * amountItem * promotion.value / 100;
                    return PriceExtension.round(value, 2);
                }
            });

            item.amount = PriceExtension.round(PriceExtension.calculateNetPrices(amountItem, discountPrices, 0), 2);
            amount += item.amount;
        });
        return amount;
    }

    public calculateTotalAmount(purchaseItems: OrderItemModel[], currentItems: OrderItemModel[],
        currentPromotions: OrderPromotion[], newPromotions: OrderPromotion[]): number {
        this.totalAmount = this.calculateAmount(currentItems) + this.calculateAmount(purchaseItems);
        const promotions = currentPromotions.concat(newPromotions);
        const discountPrices = promotions.map(promotion => {
            if (promotion.discountType === DiscountType.Money) {
                return PriceExtension.round(-1 * promotion.value, 2);
            }

            if (promotion.discountType === DiscountType.Percent) {
                const value = -1 * this.totalAmount * promotion.value / 100;
                return PriceExtension.round(value, 2);
            }
        });
        const tax = this.currentOrder.gstInclusive ? 0 : this.currentOrder.gst;
        this.totalNet = PriceExtension.round(PriceExtension.calculateNetPrices(this.totalAmount, discountPrices, tax), 2);
        return this.totalNet;
    }


    public formatNumberDecimal(value) {
        if (value) {
            return Number(value).toFixed(2);
        }
        return value;
    }

    public roundPrice(value: number) {
        return PriceExtension.round(value, 2);
    }

    public applyMemberPrice() {
        this.currentProductPriceType = ProductPriceType.MemberPrice;
        this.updatePriceForNewItems();
    }

    public applyListPrice() {
        this.currentProductPriceType = ProductPriceType.ListPrice;
        this.updatePriceForNewItems();
    }

    private updatePriceForNewItems() {
        this.newItems.forEach(x => {
            const productPriceInformation = this.newProductPriceInformations.find(y => y.variantId === x.variantId);
            x.price = this.currentProductPriceType === ProductPriceType.MemberPrice
                ? productPriceInformation.memberPrice
                : productPriceInformation.listPrice;
        });

        this.buildDataSourceForProductsAndPromotions();
        this.recalculateTotalAndCollect();
    }

    private initData() {
        this.currentOrder = this.orderExchangeService.getCurrentOrder();
        this.productSearchId = PageInputId.sales.inputIds.productExchangeSearch;
        this.productExchangeQuantityId = PageInputId.orderTransaction.exchange.productExchange.inputIds.productExchangeQuantity;
        this.newItems = this.orderExchangeService.getNewItems();
        this.selectProducts = this.orderExchangeService.selectProducts;
        this.purchaseItems = this.orderExchangeService.getPurchaseItems();
        this.refundItems = this.orderExchangeService.getRefundItems();
        this.currentPromotions = this.orderExchangeService.getCurrentPromotions();
        this.newPromotions = this.orderExchangeService.getNewPromotions();
        this.recalculateTotalAndCollect();
    }

    private recalculateTotalAndCollect() {
        this.calculateTotalAmount(this.purchaseItems, this.newItems, this.currentPromotions, this.newPromotions);
        this.collect = PriceExtension.round(this.totalNet - this.currentOrder.amount, 2);
    }

    private buildDataSourceForProductsAndPromotions() {
        let newDataSource = [];
        if (this.newItems.length > 0) {
            newDataSource.push({
                description: 'NEW ITEMS',
                quantity: null,
                price: null,
                amount: null,
                type: null,
                isDelete: false
            });

            const items = this.addProductItemAndPromotion(this.newItems, ExchangeViewStatus.NewItem);
            newDataSource = newDataSource.concat(items);
        }
        if (this.purchaseItems.length > 0) {
            newDataSource.push({
                description: 'PURCHASED ITEMS',
                quantity: null,
                price: null,
                amount: null,
                type: null,
                isDelete: false
            });
            const purchaseItems = this.addProductItemAndPromotion(this.purchaseItems, null);
            newDataSource = newDataSource.concat(purchaseItems);
        }

        if (this.refundItems.length > 0) {
            newDataSource.push({
                description: 'REFUNDED ITEMS',
                quantity: null,
                price: null,
                amount: null,
                type: null,
                isDelete: false
            });
            const newItems = this.addProductItemAndPromotion(this.refundItems, null);
            newDataSource = newDataSource.concat(newItems);
        }

        if (this.currentPromotions.length > 0 || this.newPromotions.length > 0) {
            const orderTotalPrice = this.calculateAmount(this.newItems) + this.calculateAmount(this.purchaseItems);
            newDataSource.push({
                description: 'PROMOTIONS',
                quantity: null,
                price: null,
                amount: null,
                type: null,
                status: null,
                isDelete: false
            });
            this.currentPromotions.forEach(orderPromotion => {
                const data = {
                    id: orderPromotion.id,
                    description: orderPromotion.reason,
                    quantity: '',
                    price: -1 * orderPromotion.value,
                    value: orderPromotion.value,
                    type: orderPromotion.discountType,
                    status: ExchangeViewStatus.CurrentPromotion,
                    isDelete: true
                };

                if (orderPromotion.discountType === DiscountType.Percent) {
                    data.description = (data.description ? data.description : '') + ` (${orderPromotion.value}%)`;
                    data.price = -1 * PriceExtension.round(orderTotalPrice * orderPromotion.value / 100, 2);
                }
                newDataSource.push(data);
            });
            this.newPromotions.forEach(orderPromotion => {
                const data = {
                    id: orderPromotion.id,
                    description: orderPromotion.reason,
                    quantity: '',
                    price: -1 * orderPromotion.value,
                    type: orderPromotion.discountType,
                    value: orderPromotion.value,
                    status: ExchangeViewStatus.NewPromotion,
                    isDelete: true
                };

                if (orderPromotion.discountType === DiscountType.Percent) {
                    data.description += ` (${orderPromotion.value}%)`;
                    data.price = -1 * PriceExtension.round(orderTotalPrice * orderPromotion.value / 100, 2);
                }
                newDataSource.push(data);
            });
        }

        this.dataSource = newDataSource;
    }

    private addProductItemAndPromotion(orderItems: OrderItemModel[], status: number) {
        const newDataSource = [];
        if (orderItems !== []) {
            orderItems.forEach(orderItem => {
                newDataSource.push({
                    description: orderItem.variant,
                    variantId: orderItem.variantId,
                    quantity: orderItem.quantity,
                    price: orderItem.price,
                    amount: orderItem.quantity * orderItem.price,
                    type: ExchangeViewStatus.ItemProduct,
                    status: status,
                    isDelete: status === ExchangeViewStatus.NewItem ? true : false
                });

                const orderItemPromotions = orderItem.orderItemPromotions;
                if (Array.isArray(orderItemPromotions)) {
                    orderItemPromotions.forEach(itemPromotion => {
                        const data = {
                            description: 'Item discount',
                            variantId: orderItem.variantId,
                            quantity: '',
                            price: 0,
                            amount: 0,
                            type: itemPromotion.discountType,
                            value: itemPromotion.value,
                            status: status === ExchangeViewStatus.NewItem ? ExchangeViewStatus.PromotionNewItem : null,
                            isDelete: status === ExchangeViewStatus.NewItem ? true : false
                        };

                        if (itemPromotion.discountType === DiscountType.Money) {
                            data.price = -1 * itemPromotion.value;
                        }
                        if (itemPromotion.discountType === DiscountType.Percent) {
                            data.description += ` (${itemPromotion.value}%)`;
                            data.price = -1 * PriceExtension.round(itemPromotion.value * orderItem.price * orderItem.quantity / 100, 2);
                        }
                        data.amount = data.price;

                        newDataSource.push(data);
                    });
                }
            });
        }
        return newDataSource;
    }
}
