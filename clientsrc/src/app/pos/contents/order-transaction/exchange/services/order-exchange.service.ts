import { Injectable } from '@angular/core';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { OrderPromotion } from 'src/app/pos/shared/models/order-promotion';
import { OrderPayment } from 'src/app/pos/shared/models/order-payment';
import { OrderModel } from 'src/app/pos/shared/models/order';
import { Observable } from 'rxjs';
import { Variant } from 'src/app/pos/shared/models/variant';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { PaymentMethodService } from 'src/app/pos/shared/services/payment-method.service';
import { SaleReceiptExchange } from 'src/app/pos/shared/models/device-hub/sale-receipt';
import { OrderItemStatus } from 'src/app/pos/shared/enums/order-item-status.enum';
import { SystemAppSettingKeys } from 'src/app/pos/shared/constants/appSetting-key.constant';
import { PaymentModeModel } from 'src/app/pos/shared/models/payment-mode.model';
import { ReceiptExtension } from 'src/app/pos/shared/helpers/receipt.extension';
import { AppContextManager } from 'src/app/pos/shared/app-context-manager';
import { AppSettingService } from 'src/app/pos/shared/services/appSetting.service';

@Injectable({ providedIn: 'root' })
export class OrderExchangeService {
    reason: string;
    public currentOrder: OrderModel;
    currentItems: OrderItemModel[] = [];
    public newItems: OrderItemModel[] = [];
    currentPromotions: OrderPromotion[] = [];
    public newPromotions: OrderPromotion[] = [];
    refundedItems: OrderItemModel[] = [];
    purchasedItems: OrderItemModel[] = [];
    newPayments: OrderPayment[] = [];
    collect: number;
    amount: number;
    selectProducts: Variant[] = [];

    constructor(
        private orderService: OrderService,
        private appSettingService: AppSettingService,
        private paymentMethodService: PaymentMethodService,
        private appContextManager: AppContextManager
    ) { }

    addPurchaseItem(purchasedItem: any) {
        this.purchasedItems.push(purchasedItem);
    }

    addRefundItem(refundItem: any) {
        this.refundedItems.push(refundItem);
    }

    addNewItem(newItem: any) {
        this.newItems.push(newItem);
    }

    addCurrentItem(newItem: any) {
        this.currentItems.push(newItem);
    }

    addCurrentOrder(order: any) {
        this.currentOrder = order;
    }

    addCurrentPromotion(promotion: any) {
        this.currentPromotions.push(promotion);
    }

    addNewPromotion(promotion: any) {
        this.newPromotions.push(promotion);
    }

    addReason(reason: any) {
        this.reason = reason;
    }

    setNewItems(items: OrderItemModel[]) {
        this.newItems = items;
    }

    getNewItems() {
        return this.newItems;
    }

    setNewPromotions(promotions: OrderPromotion[]) {
        this.newPromotions = promotions;
    }

    getCurrentItems() {
        return this.currentItems;
    }

    getNewPromotions() {
        return this.newPromotions;
    }

    getPurchaseItems() {
        return this.purchasedItems;
    }

    getRefundItems() {
        return this.refundedItems;
    }

    getCurrentPromotions() {
        return this.currentPromotions;
    }

    getCollect(): number {
        return this.collect;
    }

    getCurrentOrder() {
        return this.currentOrder;
    }

    addSelectProduct(item: Variant) {
        this.selectProducts.push(item);
    }

    addCurrentItems(currentItem: OrderItemModel) {
        this.currentItems.push(currentItem);
    }

    deleteCurrentItem(index: number) {
        this.currentItems.splice(index, 1);
    }

    deleteNewItem(index: number) {
        this.newItems.splice(index, 1);
    }

    deleteCurrentPromotion(index: number) {
        this.currentPromotions.splice(index, 1);
    }

    deleteNewPromotion(index: number) {
        this.newPromotions.splice(index, 1);
    }

    deleteSelectProduct(variantId: string) {
        const selectedProductIndex = this.selectProducts.findIndex(x => x.variantId === variantId);
        this.selectProducts.splice(selectedProductIndex, 1);
    }

    dataExchangeStep() {
        this.reason = '';
        this.currentItems = [];
        this.newItems = [];
        this.newPromotions = [];
        this.refundedItems = [];
        this.purchasedItems = [];
        this.newPayments = [];
    }

    resetState() {
        this.reason = '';
        this.currentOrder = null;
        this.currentItems = [];
        this.newItems = [];
        this.currentPromotions = [];
        this.newPromotions = [];
        this.refundedItems = [];
        this.purchasedItems = [];
        this.newPayments = [];
    }

    setCurrentItems(items: OrderItemModel[]) {
        this.currentItems = items;
    }

    setCurrentPromotions(promotions: OrderPromotion[]) {
        this.currentPromotions = promotions;
    }

    getReceiptExchangeByOrderId(orderId: string): Observable<any> {
        return Observable.create(observer => {
            this.orderService.getOrderIncludeOrderItems(orderId).subscribe(order => {
                const result = {
                    newItems: [],
                    purchasedItems: [],
                    exchangedItems: [],
                    createdDateString: '',
                    saleReceiptExchange: new SaleReceiptExchange,
                    netQuantityTotal: 0
                };

                if (order) {
                    const orderItems = order.orderItems;
                    result.purchasedItems = orderItems.filter(x => x.status === OrderItemStatus.Old);
                    result.exchangedItems = orderItems.filter(x => x.status === OrderItemStatus.Exchange);
                    result.newItems = orderItems.filter(x => x.status === OrderItemStatus.Normal);

                    result.netQuantityTotal = ReceiptExtension.calculateOrderItemsNetQuantityTotal(result.purchasedItems);
                    result.netQuantityTotal += ReceiptExtension.calculateOrderItemsNetQuantityTotal(result.newItems);

                    result.createdDateString = ReceiptExtension.formatReceiptDateString(order.createdDate);
                    this.appSettingService.getByKeys([SystemAppSettingKeys.storeName, SystemAppSettingKeys.storeAddress]).subscribe(appSettings => {
                        const storeNameAppSetting = appSettings.find(x => x.key === SystemAppSettingKeys.storeName);
                        const storeAddressAppSetting = appSettings.find(x => x.key === SystemAppSettingKeys.storeAddress);

                        let cashierName = '';
                        if (this.appContextManager.currentUser) {
                            cashierName = this.appContextManager.currentUser.firstName ? this.appContextManager.currentUser.firstName : '';
                            cashierName += this.appContextManager.currentUser.lastName
                                ? ' ' + this.appContextManager.currentUser.lastName
                                : '';
                        }
                        this.paymentMethodService.get().subscribe((paymentModes: PaymentModeModel[]) => {
                            this.orderService.getPreviousAndNextOrders(order.id, order.previousOrderId, '').subscribe(x => {
                                const previousOrder = x.previousOrder;
                                const nextOrder = x.nextOrder;

                                result.saleReceiptExchange = ReceiptExtension.buildReceiptExchangeFromOrderModel(
                                    order,
                                    cashierName,
                                    storeNameAppSetting ? storeNameAppSetting.value : '',
                                    storeAddressAppSetting ? storeAddressAppSetting.value : '',
                                    paymentModes,
                                    previousOrder ? previousOrder.billNo : '',
                                    nextOrder ? nextOrder.billNo : '');

                                observer.next(result);
                                observer.complete();
                            });
                        });
                    });
                }
            });
        });
    }
}
