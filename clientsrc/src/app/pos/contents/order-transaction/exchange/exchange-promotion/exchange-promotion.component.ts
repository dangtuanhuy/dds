import { Component, OnInit } from '@angular/core';
import { PageConstants, CommonConstants } from 'src/app/pos/shared/constants/common.constant';
import { Guid } from 'src/app/shared/utils/guid.util';
import { PromotionProductDiscountModel } from 'src/app/pos/shared/models/promotion-product-discount.model';
import { OrderPromotion } from 'src/app/pos/shared/models/order-promotion';
import { OrderExchangeService } from '../services/order-exchange.service';
import { OrderItemPromotion } from 'src/app/pos/shared/models/order-item-promotion';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exchange-promotion',
  templateUrl: './exchange-promotion.component.html',
  styleUrls: ['./exchange-promotion.component.scss']
})
export class ExchangePromotionComponent implements OnInit {
  public title = 'Promotions';
  public defaultPage = `/${PageConstants.defaultPage}`;
  public pageWidth = `${CommonConstants.contentPageWidth}px`;

  public itemDatasource: PromotionProductDiscountModel[] = [];
  public orderPromotions: OrderPromotion[] = [];

  constructor(
    private orderExchangeService: OrderExchangeService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initDataSource();
  }

  public addManualDiscount(event) {
    const orderPromotion = new OrderPromotion();
    orderPromotion.id = Guid.newGuid();
    orderPromotion.promotionId = Guid.empty();
    orderPromotion.reason = event.reason;
    orderPromotion.discountType = event.discountType;
    orderPromotion.value = +event.value;

    this.orderPromotions.push(orderPromotion);
  }

  public addProductDiscount(event) {
    const correspondingProduct = this.itemDatasource.find(x => x.key === event.key);
    const orderItemPromotion: OrderItemPromotion = {
      discountType: event.discountType,
      value: +event.value,
      orderItemId: Guid.empty(),
      id: Guid.newGuid()
    };

    correspondingProduct.promotions.push(orderItemPromotion);
  }

  public onOk() {
    this.orderExchangeService.setNewPromotions(this.orderPromotions);
    const newItems = this.orderExchangeService.getNewItems();

    this.itemDatasource.forEach(item => {
      const correspondingItem = newItems.find(x => x.variantId === item.key);
      correspondingItem.orderItemPromotions = correspondingItem.orderItemPromotions.concat(item.promotions);
    });

    this.router.navigateByUrl(`${PageConstants.orderExchange}/product`);
  }

  private initDataSource() {
    this.orderPromotions = this.orderExchangeService.getNewPromotions();
    const newItems = this.orderExchangeService.getNewItems();
    this.itemDatasource = newItems.map(x => {
      const promotionProductDiscountModel = new PromotionProductDiscountModel();
      promotionProductDiscountModel.key = x.variantId;
      promotionProductDiscountModel.value = x.description;
      promotionProductDiscountModel.promotions = [];

      return promotionProductDiscountModel;
    });
  }
}
