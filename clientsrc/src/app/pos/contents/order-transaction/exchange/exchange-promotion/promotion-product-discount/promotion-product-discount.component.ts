import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, ViewEncapsulation } from '@angular/core';
import { PageInputId } from 'src/app/pos/shared/constants/common.constant';
import { DiscountType } from 'src/app/pos/shared/enums/discount-type.enum';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { PromotionProductDiscountModel } from 'src/app/pos/shared/models/promotion-product-discount.model';

@Component({
  selector: 'app-promotion-product-discount',
  templateUrl: './promotion-product-discount.component.html',
  styleUrls: ['./promotion-product-discount.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PromotionProductDiscountComponent implements OnInit, OnDestroy {
  @Input() products: PromotionProductDiscountModel[] = [];
  @Output() addDiscount = new EventEmitter<any>();
  public variantDiscountValueId = '';
  public variantDiscountValue = '0';

  public selectedDiscountType = DiscountType.Default;
  public discountTypes = [];
  public discountUnit = '$';

  public discountReasonTypes = [];
  public selectedKey = '';

  constructor(
    private notificationService: NotificationService
  ) {

  }

  ngOnInit() {
    this.initData();
  }
  ngOnDestroy() {
    this.addDiscount.complete();
  }

  public onChangeVariantDiscountValue(event) {
    this.variantDiscountValue = event;
  }

  public onChangeDiscountType(event) {
    if (this.selectedDiscountType === DiscountType.Money) {
      this.discountUnit = '$';
    }

    if (this.selectedDiscountType === DiscountType.Percent) {
      this.discountUnit = '%';
    }
  }

  public onAddVariantDiscount(event) {
    if (!this.selectedKey) {
      this.notificationService.warning('Please Select Product!');
      return;
    }

    const value = Number(this.variantDiscountValue);
    if (value <= 0) {
      this.notificationService.warning('Value equals 0!');
      return;
    }

    this.addDiscount.emit({
      key: this.selectedKey,
      discountType: this.selectedDiscountType,
      value: value
    });

    this.selectedKey = '';
    this.variantDiscountValue = '0';
  }


  private initData() {
    this.variantDiscountValueId = PageInputId.orderTransaction.exchange.productDiscount.inputIds.productDiscountValue;
    this.selectedDiscountType = DiscountType.Money;
    this.discountTypes = [
      {
        id: DiscountType.Money,
        value: DiscountType[DiscountType.Money]
      },
      {
        id: DiscountType.Percent,
        value: DiscountType[DiscountType.Percent]
      }
    ];
  }

}
