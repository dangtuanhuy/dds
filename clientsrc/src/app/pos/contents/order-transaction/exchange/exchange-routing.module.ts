import { NgModule } from '@angular/core';
import { ExchangeComponent } from './exchange.component';
import { Routes, RouterModule } from '@angular/router';
import { ExchangePromotionComponent } from './exchange-promotion/exchange-promotion.component';
import { PromotionManualDiscountComponent } from './exchange-promotion/promotion-manual-discount/promotion-manual-discount.component';
import { PromotionProductDiscountComponent } from './exchange-promotion/promotion-product-discount/promotion-product-discount.component';
import { ExchangeProductComponent } from './exchange-product/exchange-product.component';
import { ExchangePaymentComponent } from './exchange-payment/exchange-payment.component';
const routes: Routes = [
  {
    path: 'sale/:id',
    component: ExchangeComponent
  },
  {
    path: 'product',
    component: ExchangeProductComponent
  },
  {
    path: 'promotions',
    component: ExchangePromotionComponent
  },
  {
    path: 'payment',
    component: ExchangePaymentComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    [RouterModule.forChild(routes)]
  ],
  exports: [RouterModule]
})
export class ExchangeRoutingModule {
  static components = [
    ExchangeComponent,
    ExchangePromotionComponent,
    PromotionManualDiscountComponent,
    PromotionProductDiscountComponent,
    ExchangeProductComponent,
    ExchangePaymentComponent
  ];
}
