import { Component, ViewEncapsulation, Injector } from '@angular/core';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { PaymentModeModel, PaymentModeViewModel, calculatePaymentPaid } from 'src/app/pos/shared/models/payment-mode.model';
import { PaymentMethodService } from 'src/app/pos/shared/services/payment-method.service';
import { Router } from '@angular/router';
import { ListViewColumn } from 'src/app/shared/components/inline-edit-list-view/model';
import { PageInputId, CommonConstants, PageConstants } from 'src/app/pos/shared/constants/common.constant';
import { OrderExchangeService } from '../services/order-exchange.service';
import { PriceExtension } from 'src/app/pos/shared/helpers/price.extension';
import { OrderModel, Order } from 'src/app/pos/shared/models/order';
import { OrderTransactionService } from 'src/app/pos/shared/services/order-transaction.service';
import { OrderPayment } from 'src/app/pos/shared/models/order-payment';
import { Guid } from 'src/app/shared/utils/guid.util';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { OrderPromotion } from 'src/app/pos/shared/models/order-promotion';
import { OrderTransactionType } from 'src/app/pos/shared/enums/order-transaction-type.enum';
import { AppContextManager } from 'src/app/pos/shared/app-context-manager';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { DeviceCommand } from 'src/app/pos/shared/enums/device-hub/device-command.enum';
import { DeviceHubService } from 'src/app/pos/shared/services/device-hub.service';
import { AppSettingService } from 'src/app/pos/shared/services/appSetting.service';
import { DeviceHubRequest } from 'src/app/pos/shared/models/device-hub/device-hub-request';
import { SystemAppSettingKeys } from 'src/app/pos/shared/constants/appSetting-key.constant';

@Component({
    selector: 'app-exchange-payment',
    templateUrl: './exchange-payment.component.html',
    styleUrls: ['./exchange-payment.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ExchangePaymentComponent extends ComponentBase {

    public title = 'Payment';
    public defaultPage = '';
    public paymentModeSearchId = '';
    public pageWidth = `${CommonConstants.contentPageWidth}px`;

    public reason: string;
    public currentOrder: OrderModel;
    public purchasedItems: OrderItemModel[];
    public refundedItems: OrderItemModel[];
    public newItems: OrderItemModel[];
    public newPromotions: OrderPromotion[];

    public decimalCollect = '0.00';
    public decimalTotalNetPrice = '0.00';
    public decimalTotalPaid = '0.00';
    public decimalBalance = '0.00';
    public decimalDeposit = '0.00';

    public collect = 0;
    public balance = 0;
    public deposit = 0;
    private totalPaid = 0;

    public paymentModes: PaymentModeViewModel[] = [];
    public filteredPaymenModes: PaymentModeModel[] = [];
    public paymentModeViewColumns: ListViewColumn[] = [
        new ListViewColumn({ name: 'icon', isIcon: true, }),
        new ListViewColumn({ name: 'paymode' }),
        new ListViewColumn({ name: 'amount', isEdit: true, isNumber: true, })
    ];

    constructor(private notificationService: NotificationService,
        private orderExchangeService: OrderExchangeService,
        private paymentMethodService: PaymentMethodService,
        private orderTransactionService: OrderTransactionService,
        private orderService: OrderService,
        private appContextManager: AppContextManager,
        private deviceHubService: DeviceHubService,
        private appSettingService: AppSettingService,
        private router: Router,
        public injector: Injector) {
        super(injector);
    }

    onInit() {
        this.initData();
        this.initLoadStores();
    }
    onDestroy() {
    }

    public onSearchPaymentMode(value: string) {
        if (!value) {
            this.filteredPaymenModes = [];
        }
        this.paymentMethodService.searchPaymentModes(value).subscribe(filteredPaymenModes => {
            this.filteredPaymenModes = filteredPaymenModes.map(x => {
                x.description = x.paymode;
                return x;
            });
        });
    }

    public onSelectPaymentMode(payment) {
        const selectPayment: PaymentModeViewModel = {
            id: payment.id,
            code: payment.code,
            paymode: payment.paymode,
            icon: payment.icon,
            slipno: payment.slipno,
            isDelete: payment.isDelete,
            description: payment.description,
            amount: 0,
        };
        this.paymentModes.push(selectPayment);
        this.paymentModes = JSON.parse(JSON.stringify(this.paymentModes));
    }

    public onEsc() {
        this.router.navigate(['/quick-select']);
    }

    public changePaymentAmount(values) {
        const updatedpaymentMode = this.paymentModes.find(x => x.code === values.code);
        if (updatedpaymentMode) {
            updatedpaymentMode.amount = values.amount;
        }
        this.totalPaid = calculatePaymentPaid(this.paymentModes);
        this.decimalTotalPaid = this.totalPaid.toFixed(2);
        this.calculateBalanceAndDeposit();
    }

    public onClickIcon(event) {
        if (event.colName === 'icon') {
            const item = event.item;

            if (item) {
                let itemAmount = Number(item.amount);
                itemAmount = itemAmount ? itemAmount : 0;
                this.changePaymentAmount({
                    id: item.id,
                    code: item.code,
                    amount: PriceExtension.round(Number(this.balance) + itemAmount, 2)
                });
            }
        }
    }

    public onDeletePaymentMode(id) {
        this.paymentModes = this.paymentModes.filter(x => x.id !== id);
    }

    public onSave() {
        if (Math.abs(this.balance) > 0) {
            this.notificationService.error('Please check balance total!');
            return;
        }

        if (this.deposit) {
            const cashPayment = this.paymentModes.find(x => x.code === 'CASH');
            if (cashPayment) {
                if (cashPayment.amount < this.deposit) {
                    this.notificationService.error('The cash must be larger then the change!');
                    return;
                }
            }
        }

        const newPayments = this.paymentModes.filter(x => x.amount > 0).map(paymentMode => {
            const orderPayment: OrderPayment = {
                id: Guid.empty(),
                orderId: '',
                paymentCode: paymentMode.code,
                amount: paymentMode.amount
            };
            return orderPayment;
        });

        const collect = this.orderExchangeService.getCollect();

        const exchangeOrder: Order = {
            id: Guid.newGuid(),
            customerId: this.currentOrder.customerId,
            amount: PriceExtension.round(this.orderExchangeService.amount, 2),
            gst: this.currentOrder.gst,
            gstInclusive: this.currentOrder.gstInclusive,
            billNo: '',
            change: PriceExtension.round(this.deposit, 2),
            createdDate: new Date(),
            cashierId: this.appContextManager.currentUser.id,
            isDelete: false,
            synced: false,
            orderTransactionType: OrderTransactionType.Exchange,
            previousOrderId: this.currentOrder.id,
            originalOrderId: this.currentOrder.originalOrderId,
            reason: this.orderExchangeService.reason,
            collect: collect
        };

        if (exchangeOrder.change) {
            const cashPayment = newPayments.find(x => x.paymentCode === 'CASH');
            if (cashPayment) {
                cashPayment.amount = PriceExtension.round(cashPayment.amount - exchangeOrder.change, 2);
            }
        }

        this.newItems.forEach(newItem => {
            newItem.orderItemPromotions.forEach(x => {
                x.orderItemId = newItem.id;
            });
        });

        this.orderService.generateOrderBillNo().subscribe(billNo => {
            exchangeOrder.billNo = billNo;
            const promotions = this.orderExchangeService.getCurrentPromotions().concat(this.newPromotions);
            this.orderTransactionService.exchangeOrder(exchangeOrder, this.currentOrder, this.purchasedItems,
                this.refundedItems, this.newItems, promotions, newPayments).subscribe(result => {
                    if (result) {
                        this.notificationService.success('Exchange Success');
                        this.printReceiptsForExchange(result.id);
                        this.openCashDrawer();
                        this.router.navigate([PageConstants.quickSelect]);
                    } else {
                        this.notificationService.error('Something went wrong!');
                    }
                });
        });
    }

    private openCashDrawer() {
        this.deviceHubService.openCashDrawer(this.appContextManager.getPrinterShareName()).subscribe();
    }

    printReceiptsForExchange(orderId: string) {
        this.orderExchangeService.getReceiptExchangeByOrderId(orderId).subscribe(data => {
            if (!data) {
                return;
            }

            this.appSettingService.getByKey(SystemAppSettingKeys.deviceHubName).subscribe((appSetting) => {
                if (appSetting) {
                    const request = new DeviceHubRequest();
                    request.command = DeviceCommand.PrintReceiptExchange;
                    request.deviceHubName = appSetting.value;
                    request.data = JSON.stringify(data.saleReceiptExchange);

                    this.deviceHubService.printEpson(request).subscribe(x => {
                        this.deviceHubService.printEpson(request).subscribe();
                    });
                }
            });
        });
    }

    private initData() {
        this.defaultPage = '';
        this.paymentModeSearchId = PageInputId.payment.inputIds.exchangePaymentModeSearchId;
        this.currentOrder = this.orderExchangeService.getCurrentOrder();
        this.purchasedItems = this.orderExchangeService.getPurchaseItems();
        this.refundedItems = this.orderExchangeService.getRefundItems();
        this.newItems = this.orderExchangeService.getNewItems();
        this.newPromotions = this.orderExchangeService.getNewPromotions();
    }

    private initLoadStores() {
        this.paymentMethodService.getDefaultPaymentModes().subscribe(paymentModes => {
            const paymentModeViewModels = paymentModes.map(paymentMode => {
                const model = new PaymentModeViewModel(paymentMode);
                model.amount = 0;
                return model;
            });
            this.paymentModes = paymentModeViewModels;
        });

        this.collect = this.orderExchangeService.getCollect();
        this.decimalCollect = this.collect.toFixed(2);
        this.decimalBalance = this.decimalCollect;
        this.calculateBalanceAndDeposit();
    }

    private calculateBalanceAndDeposit() {
        const balance = PriceExtension.round(this.totalPaid - this.collect, 2);
        this.balance = balance > 0 ? 0 : -1 * balance;
        this.deposit = balance > 0 ? balance : 0;

        this.decimalBalance = this.balance.toFixed(2);
        this.decimalDeposit = this.deposit.toFixed(2);
    }
}
