import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { CommonConstants, PageInputId, PageConstants } from 'src/app/pos/shared/constants/common.constant';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { OrderModel } from 'src/app/pos/shared/models/order';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { SaleItemType } from 'src/app/pos/shared/enums/sale-item-type.enum';
import { OrderExchangeService } from './services/order-exchange.service';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { Guid } from 'src/app/shared/utils/guid.util';
import { OrderItemStatus } from 'src/app/pos/shared/enums/order-item-status.enum';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExchangeComponent extends ComponentBase {
  public title = 'Exchange';
  public defaultPage = '';
  public pageWidth = `${CommonConstants.contentPageWidth}px`;
  public sale = new OrderModel();
  public productExchangeQuantityId = '';
  public productExchangeReasonId = '';
  public isTextarea = true;

  public dataSource = [];
  public orderItems: OrderItemModel[] = [];

  public isDisableAddProductForExchangeButton = true;

  private saleId = '';
  public quantity = 0;
  public reasonExchange = '';
  public inputQuantityStyle = {
    'height': '30px',
    'min-width': '20px',
    'max-width': '80px',
    'padding-left': '2px',
    'border': 'none',
    'border-color': 'transparent',
    'text-align': 'center'
  };
  public inputReasonStyle = {
    'height': '200px',
    'width': '100%',
    'padding': '10px',
    'margin': '30px 0px 10px 0px'
  };

  constructor(
    private router: Router,
    private _activatedRoute: ActivatedRoute,
    private orderService: OrderService,
    private orderExchangeService: OrderExchangeService,
    private notificationService: NotificationService,
    public injector: Injector
  ) {
    super(injector);
    this.saleId = this._activatedRoute.snapshot.paramMap.get('id');
  }
  onInit() {
    this.orderExchangeService.dataExchangeStep();
    this.productExchangeQuantityId = PageInputId.orderTransaction.exchange.productExchange.inputIds.productExchangeQuantity;
    this.productExchangeReasonId = PageInputId.orderTransaction.exchange.productExchange.inputIds.productExchangeReason;
    this.orderService.getOrderIncludeOrderItems(this.saleId).subscribe(item => {
      this.sale = item;
      this.buildDataSourceForProductsAndPromotions(this.sale);
    });
  }
  onDestroy() {
  }

  public onChangeReason(event) {
    this.reasonExchange = event;
  }

  public onClickAddProductForExchange() {
    this.orderExchangeService.addReason(this.reasonExchange);
    this.dataSource.forEach(item => {
      let itemIsRefunded = false;
      if (item.exchangeQuantity > 0) {
        const exchangeItem = JSON.parse(JSON.stringify(this.orderItems.find(x => x.id === item.id)));
        exchangeItem.quantity = item.exchangeQuantity;
        exchangeItem.previousOrderItemId = exchangeItem.id;
        exchangeItem.id = Guid.newGuid();
        exchangeItem.orderItemPromotions.forEach(itemPromotion => {
          itemPromotion.id = Guid.newGuid();
          itemPromotion.orderItemId = exchangeItem.id;
        });
        this.orderExchangeService.addRefundItem(exchangeItem);
        itemIsRefunded = true;
      }
      if (item.remainingQuantity > 0) {
        const purchasedItem = JSON.parse(JSON.stringify(this.orderItems.find(x => x.id === item.id)));
        purchasedItem.quantity = item.remainingQuantity;
        purchasedItem.previousOrderItemId = purchasedItem.id;
        purchasedItem.id = Guid.newGuid();
        if (itemIsRefunded) {
          purchasedItem.orderItemPromotions = [];
        } else {
          purchasedItem.orderItemPromotions.forEach(itemPromotion => {
            itemPromotion.id = Guid.newGuid();
            itemPromotion.orderItemId = purchasedItem.id;
          });
        }

        this.orderExchangeService.addPurchaseItem(purchasedItem);
      }
    });
    this.router.navigate([`${PageConstants.orderExchange}/product`]);
  }

  public onBack() {
    this.orderExchangeService.resetState();
  }

  public onInPutNumberPlus(row: any) {
    if (row.remainingQuantity > 0) {
      row.exchangeQuantity += 1;
      row.remainingQuantity -= 1;
    }

    this.checkisDisableAddProductForExchangeButton();
  }

  public onInPutNumberMinus(row: any) {
    if (row.exchangeQuantity > 0) {
      row.exchangeQuantity -= 1;
      row.remainingQuantity += 1;
    }

    this.checkisDisableAddProductForExchangeButton();
  }

  public onChangeQuantity(event, row: any) {
    const inputQuantity = Number(event);
    row.exchangeQuantity = inputQuantity;
    if (row.quantity < inputQuantity) {
      this.notificationService.warning(`${row.description} has refund quantity greater than purchased quantity!`);
    }
    row.remainingQuantity = row.quantity - inputQuantity;
    this.checkisDisableAddProductForExchangeButton();
  }

  public formatNumberDecimal(value: number) {
    return value.toFixed(2);
  }

  private buildDataSourceForProductsAndPromotions(sale: any) {
    if (sale) {
      const newDataSource = [];
      sale.orderItems.filter(x => x.status === OrderItemStatus.Old || x.status === OrderItemStatus.Normal).forEach(orderItem => {
        this.orderItems.push(orderItem);
        newDataSource.push({
          id: orderItem.id,
          description: orderItem.variant,
          quantity: orderItem.quantity,
          exchangeQuantity: 0,
          remainingQuantity: orderItem.quantity,
          price: orderItem.price,
          amount: orderItem.quantity * orderItem.price,
          type: SaleItemType.Product
        });
      });
      this.dataSource = newDataSource;
    }
  }

  private checkisDisableAddProductForExchangeButton() {
    if (this.dataSource.find(x => x.exchangeQuantity > 0) && !this.dataSource.find(x => x.exchangeQuantity > x.quantity)) {
      this.isDisableAddProductForExchangeButton = false;
    } else {
      this.isDisableAddProductForExchangeButton = true;
    }
  }
}
