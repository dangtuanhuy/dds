import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedPosModule } from 'src/app/shared/shared-pos.module';
import { PosCommonModule } from 'src/app/pos/shared/pos-common.module';
import { ExchangeRoutingModule } from './exchange-routing.module';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
    declarations: [
        ExchangeRoutingModule.components
    ],
    imports: [
        ExchangeRoutingModule,
        CommonModule,
        SharedPosModule,
        PosCommonModule,
        CommonModule,
        NgSelectModule,
        NgxDatatableModule,
        FormsModule
    ],
    providers: [

    ],
    exports: [
    ]
})
export class ExchangeModule { }
