import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { CommonConstants, PageInputId, PageConstants } from 'src/app/pos/shared/constants/common.constant';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { OrderModel } from 'src/app/pos/shared/models/order';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { OrderRefundService } from './service/order-refund.service';
import { SaleItemType } from 'src/app/pos/shared/enums/sale-item-type.enum';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { Guid } from 'src/app/shared/utils/guid.util';
import { OrderItemStatus } from 'src/app/pos/shared/enums/order-item-status.enum';
import { DiscountType } from 'src/app/pos/shared/enums/discount-type.enum';
import { PriceExtension } from 'src/app/pos/shared/helpers/price.extension';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-refund',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RefundComponent extends ComponentBase {
  public title = 'Refund';
  public defaultPage = '';
  public pageWidth = `${CommonConstants.contentPageWidth}px`;
  public sale = new OrderModel();
  public isTextarea = true;
  public dataSource = [];
  public orderItems: OrderItemModel[] = [];
  public productRefundQuantityId = '';
  public productRefundReasonId = '';
  private saleId = '';
  public quantity = 0;
  public reasonRefund = '';

  public isDisableNextStepButton = true;

  public inputQuantityStyle = {
    'height': '30px',
    'min-width': '20px',
    'max-width': '80px',
    'padding-left': '2px',
    'border': 'none',
    'border-color': 'transparent',
    'text-align': 'center'
  };
  public inputReasonStyle = {
    'height': '200px',
    'width': '100%',
    'padding': '10px',
    'margin': '30px 0px 10px 0px'
  };


  constructor(
    private router: Router,
    private _activatedRoute: ActivatedRoute,
    private orderService: OrderService,
    private orderRefundService: OrderRefundService,
    private notificationService: NotificationService,
    public injector: Injector
  ) {
    super(injector);
    this.saleId = this._activatedRoute.snapshot.paramMap.get('id');
  }
  onInit() {
    this.productRefundQuantityId = PageInputId.orderTransaction.refund.productRefund.inputIds.productRefundQuantity;
    this.productRefundReasonId = PageInputId.orderTransaction.refund.productRefund.inputIds.productRefundReason;
    this.orderService.getOrderIncludeOrderItems(this.saleId).subscribe(item => {
      this.sale = item;
      this.buildDataSourceForProductsAndPromotions(this.sale);
    });
  }
  onDestroy() {
  }

  public onBack() {
    this.orderRefundService.resetState();
  }

  public onChangeReason(event) {
    this.reasonRefund = event;
  }

  public onClickNext() {
    this.orderRefundService.addReason(this.reasonRefund);
    this.dataSource.forEach(item => {
      if (item.refundQuantity > 0) {
        const refundItem = JSON.parse(JSON.stringify(this.orderItems.find(x => x.id === item.id)));
        refundItem.quantity = item.refundQuantity;
        refundItem.previousOrderItemId = refundItem.id;
        refundItem.amount = Number(refundItem.quantity) * Number(refundItem.price);
        refundItem.id = Guid.newGuid();
        const refundItemAmout = refundItem.amount;
        refundItem.orderItemPromotions.forEach(itemPromotion => {
          itemPromotion.id = Guid.newGuid();
          itemPromotion.orderItemId = refundItem.id;

          if (itemPromotion.discountType === DiscountType.Money) {
            refundItem.amount += (-1) * Number(itemPromotion.value);
          }
          if (itemPromotion.discountType === DiscountType.Percent) {
            refundItem.amount += (-1) * PriceExtension.round(refundItemAmout * itemPromotion.value / 100, 2);
          }
        });
        this.orderRefundService.addRefundItem(refundItem);
      }

      if (item.remainingQuantity > 0) {
        const purchasedItem = JSON.parse(JSON.stringify(this.orderItems.find(x => x.id === item.id)));
        purchasedItem.quantity = item.remainingQuantity;
        purchasedItem.previousOrderItemId = purchasedItem.id;
        purchasedItem.amount = Number(purchasedItem.quantity) * Number(purchasedItem.price);
        purchasedItem.id = Guid.newGuid();
        if (item.refundQuantity > 0) {
          purchasedItem.orderItemPromotions = [];
        } else {
          const purchasedItemAmout = purchasedItem.amount;
          purchasedItem.orderItemPromotions.forEach(itemPromotion => {
            itemPromotion.id = Guid.newGuid();
            itemPromotion.orderItemId = purchasedItem.id;
            if (itemPromotion.discountType === DiscountType.Money) {
              purchasedItem.amount += (-1) * Number(itemPromotion.value);
            }
            if (itemPromotion.discountType === DiscountType.Percent) {
              purchasedItem.amount += (-1) * PriceExtension.round(purchasedItemAmout * itemPromotion.value / 100, 2);
            }
          });
        }
        this.orderRefundService.addPurchaseItem(purchasedItem);
      }
    });
    this.router.navigate([`${PageConstants.orderRefund}/refund-detail`]);
  }

  public onInPutNumberPlus(row: any) {
    if (row.remainingQuantity > 0) {
      row.refundQuantity += 1;
      row.remainingQuantity -= 1;
    }
    this.checkisDisableNextStepButton();
  }

  public onInPutNumberMinus(row: any) {
    if (row.refundQuantity > 0) {
      row.refundQuantity -= 1;
      row.remainingQuantity += 1;
    }
    this.checkisDisableNextStepButton();
  }

  public onChangeQuantity(event, row: any) {
    const inputQuantity = Number(event);
    row.refundQuantity = inputQuantity;
    if (row.quantity < inputQuantity) {
      this.notificationService.warning(`${row.description} has refund quantity greater than purchased quantity!`);
    }
    row.remainingQuantity = row.quantity - inputQuantity;
    this.checkisDisableNextStepButton();
  }

  public formatNumberDecimal(value: number) {
    return value.toFixed(2);
  }

  private buildDataSourceForProductsAndPromotions(sale: any) {
    if (sale) {
      const newDataSource = [];
      sale.orderItems.filter(x => x.status === OrderItemStatus.Old || x.status === OrderItemStatus.Normal).forEach(orderItem => {
        this.orderItems.push(orderItem);
        newDataSource.push({
          id: orderItem.id,
          description: orderItem.variant,
          quantity: orderItem.quantity,
          refundQuantity: 0,
          remainingQuantity: orderItem.quantity,
          price: orderItem.price,
          amount: orderItem.quantity * orderItem.price,
          type: SaleItemType.Product
        });
      });
      this.dataSource = newDataSource;
    }
  }

  private checkisDisableNextStepButton() {
    if (this.dataSource.find(x => x.refundQuantity > 0) && !this.dataSource.find(x => x.refundQuantity > x.quantity)) {
      this.isDisableNextStepButton = false;
    } else {
      this.isDisableNextStepButton = true;
    }
  }
}
