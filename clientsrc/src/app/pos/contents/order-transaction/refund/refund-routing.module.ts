import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RefundComponent } from './refund.component';
import { RefundDetailComponent } from './refund-detail/refund-detail.component';

const routes: Routes = [
  {
    path: 'sale/:id',
    component: RefundComponent
  },
  {
    path: 'refund-detail',
    component: RefundDetailComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    [RouterModule.forChild(routes)]
  ],
  exports: [RouterModule]
})
export class RefundRoutingModule {
  static components = [
    RefundComponent,
    RefundDetailComponent
  ];
}
