import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedPosModule } from 'src/app/shared/shared-pos.module';
import { PosCommonModule } from 'src/app/pos/shared/pos-common.module';
import { RefundRoutingModule } from './refund-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        RefundRoutingModule.components
    ],
    imports: [
        RefundRoutingModule,
        CommonModule,
        SharedPosModule,
        PosCommonModule,
        NgxDatatableModule,
        FormsModule
    ],
    providers: [

    ],
    exports: [
    ]
})
export class RefundModule { }
