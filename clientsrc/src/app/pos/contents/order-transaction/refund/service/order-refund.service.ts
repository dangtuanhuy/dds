import { Injectable } from '@angular/core';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { OrderPromotion } from 'src/app/pos/shared/models/order-promotion';
import { OrderPayment } from 'src/app/pos/shared/models/order-payment';
import { OrderModel } from 'src/app/pos/shared/models/order';
import { Observable } from 'rxjs';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { OrderItemStatus } from 'src/app/pos/shared/enums/order-item-status.enum';
import { OrderPriceExtension } from 'src/app/pos/shared/helpers/order-price.extention';
import { SystemAppSettingKeys } from 'src/app/pos/shared/constants/appSetting-key.constant';
import { PaymentModeModel } from 'src/app/pos/shared/models/payment-mode.model';
import { ReceiptExtension } from 'src/app/pos/shared/helpers/receipt.extension';
import { AppSettingService } from 'src/app/pos/shared/services/appSetting.service';
import { AppContextManager } from 'src/app/pos/shared/app-context-manager';
import { PaymentMethodService } from 'src/app/pos/shared/services/payment-method.service';
import { SaleReceiptRefund } from 'src/app/pos/shared/models/device-hub/sale-receipt';

@Injectable({ providedIn: 'root' })
export class OrderRefundService {
    reason: string;
    public currentOrder: OrderModel;
    currentItems: OrderItemModel[] = [];
    currentPromotions: OrderPromotion[] = [];
    refundedItems: OrderItemModel[] = [];
    purchasedItems: OrderItemModel[] = [];
    newPayments: OrderPayment[] = [];

    constructor(
        private orderService: OrderService,
        private appSettingService: AppSettingService,
        private appContextManager: AppContextManager,
        private paymentMethodService: PaymentMethodService
    ) { }

    addCurrentOrder(order: any) {
        this.currentOrder = order;
    }

    addReason(reason: any) {
        this.reason = reason;
    }

    addCurrentItem(newItem: any) {
        this.currentItems.push(newItem);
    }

    addCurrentPromotion(promotion: any) {
        this.currentPromotions.push(promotion);
    }

    addRefundItem(refundItem: any) {
        this.refundedItems.push(refundItem);
    }

    addPurchaseItem(purchaseItem: any) {
        this.purchasedItems.push(purchaseItem);
    }
    getCurrentOrder() {
        return this.currentOrder;
    }
    getAllPurchaseItems() {
        return this.purchasedItems;
    }
    getAllRefundItems() {
        return this.refundedItems;
    }
    resetPurchaseItems() {
        this.purchasedItems = [];
    }
    resetRefundItems() {
        this.refundedItems = [];
    }
    getCurrentPromotions() {
        return this.currentPromotions;
    }
    getReason() {
        return this.reason;
    }

    resetState() {
        this.reason = '';
        this.currentOrder = null;
        this.currentItems = [];
        this.currentPromotions = [];
        this.refundedItems = [];
        this.purchasedItems = [];
        this.newPayments = [];
    }

    setCurrentItems(items: OrderItemModel[]) {
        this.currentItems = items;
    }

    setCurrentPromotions(promotions: OrderPromotion[]) {
        this.currentPromotions = promotions;
    }

    getReceiptRefundByOrderId(orderId: string): Observable<any> {
        return Observable.create(observer => {
            this.orderService.getOrderIncludeOrderItems(orderId).subscribe(order => {
                const result = {
                    purchasedItems: [],
                    refundedItems: [],
                    totalRefund: 0,
                    createdDateString: '',
                    saleReceiptRefund: new SaleReceiptRefund,
                    netQuantityTotal: 0
                };
                if (order) {
                    result.purchasedItems = order.orderItems.filter(x => x.status === OrderItemStatus.Old);
                    result.refundedItems = order.orderItems.filter(x => x.status === OrderItemStatus.Refund);
                    result.totalRefund = OrderPriceExtension.calculateOrderItemsAmount(result.refundedItems);
                    result.netQuantityTotal = ReceiptExtension.calculateOrderItemsNetQuantityTotal(result.purchasedItems);

                    result.createdDateString = ReceiptExtension.formatReceiptDateString(order.createdDate);
                    this.appSettingService.getByKeys([SystemAppSettingKeys.storeName, SystemAppSettingKeys.storeAddress]).subscribe(appSettings => {
                        const storeNameAppSetting = appSettings.find(x => x.key === SystemAppSettingKeys.storeName);
                        const storeAddressAppSetting = appSettings.find(x => x.key === SystemAppSettingKeys.storeAddress);

                        let cashierName = '';
                        if (this.appContextManager.currentUser) {
                            cashierName = this.appContextManager.currentUser.firstName ? this.appContextManager.currentUser.firstName : '';
                            cashierName += this.appContextManager.currentUser.lastName
                                ? ' ' + this.appContextManager.currentUser.lastName
                                : '';
                        }
                        this.paymentMethodService.get().subscribe((paymentModes: PaymentModeModel[]) => {
                            this.orderService.getPreviousAndNextOrders(order.id, order.previousOrderId, '').subscribe(x => {
                                const previousOrder = x.previousOrder;
                                const nextOrder = x.nextOrder;

                                result.saleReceiptRefund = ReceiptExtension.buildReceiptRefundFromOrderModel(
                                    order,
                                    cashierName,
                                    storeNameAppSetting ? storeNameAppSetting.value : '',
                                    storeAddressAppSetting ? storeAddressAppSetting.value : '',
                                    paymentModes,
                                    previousOrder ? previousOrder.billNo : '',
                                    nextOrder ? nextOrder.billNo : '');

                                observer.next(result);
                                observer.complete();
                            });
                        });
                    });
                }
            });
        });
    }
}
