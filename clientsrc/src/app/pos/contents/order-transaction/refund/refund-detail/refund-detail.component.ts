import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { CommonConstants, PageConstants } from 'src/app/pos/shared/constants/common.constant';
import { ComponentBase } from 'src/app/shared/components/component-base';
import { SaleItemType } from 'src/app/pos/shared/enums/sale-item-type.enum';
import { DiscountType } from 'src/app/pos/shared/enums/discount-type.enum';
import { PriceExtension } from 'src/app/pos/shared/helpers/price.extension';
import { OrderModel, Order } from 'src/app/pos/shared/models/order';
import { OrderItemModel } from 'src/app/pos/shared/models/order-item';
import { OrderPromotion } from 'src/app/pos/shared/models/order-promotion';
import { OrderRefundService } from '../service/order-refund.service';
import { OrderTransactionService } from 'src/app/pos/shared/services/order-transaction.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Router } from '@angular/router';
import { OrderService } from 'src/app/pos/shared/services/order.service';
import { AppSettingService } from 'src/app/pos/shared/services/appSetting.service';
import { SystemAppSettingKeys } from 'src/app/pos/shared/constants/appSetting-key.constant';
import { DeviceHubRequest } from 'src/app/pos/shared/models/device-hub/device-hub-request';
import { DeviceCommand } from 'src/app/pos/shared/enums/device-hub/device-command.enum';
import { DeviceHubService } from 'src/app/pos/shared/services/device-hub.service';
import { AppContextManager } from 'src/app/pos/shared/app-context-manager';

@Component({
  selector: 'app-refund-detail',
  templateUrl: './refund-detail.component.html',
  styleUrls: ['./refund-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RefundDetailComponent extends ComponentBase {
  public title = 'Refund';
  public defaultPage = '';
  public pageWidth = `${CommonConstants.contentPageWidth}px`;
  public dataSource = [];
  public reason: string;
  public currentOrder: OrderModel;
  public currentItems: OrderItemModel[] = [];
  public currentPromotions: OrderPromotion[] = [];
  public refundedItems: OrderItemModel[] = [];
  public purchasedItems: OrderItemModel[] = [];
  public discountPrices: number[] = [];
  public exchangeMoney: number;
  public currentAmount: number;
  public disableRefundButton = false;

  constructor(
    private orderRefundService: OrderRefundService,
    private orderTransactionService: OrderTransactionService,
    private notificationService: NotificationService,
    private appSettingService: AppSettingService,
    private deviceHubService: DeviceHubService,
    private appContextManager: AppContextManager,
    private router: Router,
    public injector: Injector
  ) {
    super(injector);
  }

  onInit() {
    this.refundedItems = this.orderRefundService.getAllRefundItems();
    this.purchasedItems = this.orderRefundService.getAllPurchaseItems();
    this.currentPromotions = JSON.parse(JSON.stringify(this.orderRefundService.getCurrentPromotions()));
    this.buildDataSourceForProductsAndPromotions();
  }

  onDestroy() {
  }

  public onClickRefund() {
    if (this.exchangeMoney <= 0) {
      this.notificationService.warning(`The refund payable must be larger than 0!`);
      return;
    }

    this.disableRefundButton = true;
    const reason = this.orderRefundService.getReason();
    const currentOrder = this.orderRefundService.getCurrentOrder();
    const purchasedItems = this.orderRefundService.getAllPurchaseItems();
    const refundedItems = this.orderRefundService.getAllRefundItems();
    const currentPromotions = JSON.parse(JSON.stringify(this.orderRefundService.getCurrentPromotions()));
    this.orderTransactionService.refundOrder(reason, this.currentAmount, this.exchangeMoney, currentOrder, purchasedItems,
      refundedItems, currentPromotions).subscribe((result: Order) => {
        if (result) {
          this.notificationService.success('Success');
          this.printReceiptsForRefund(result.id);
          this.openCashDrawer();
          this.router.navigateByUrl(`${PageConstants.quickSelect}`);
        } else {
          this.notificationService.error('Something went wrong!');
          this.disableRefundButton = false;
        }
      });
  }
  private openCashDrawer() {
    this.deviceHubService.openCashDrawer(this.appContextManager.getPrinterShareName()).subscribe();
  }

  private printReceiptsForRefund(orderId: string) {
    this.orderRefundService.getReceiptRefundByOrderId(orderId).subscribe(data => {
      if (!data) {
        return;
      }

      this.appSettingService.getByKey(SystemAppSettingKeys.deviceHubName).subscribe((appSetting) => {
        if (appSetting) {
          const request = new DeviceHubRequest();
          request.command = DeviceCommand.PrintReceiptRefund;
          request.deviceHubName = appSetting.value;
          request.data = JSON.stringify(data.saleReceiptRefund);

          this.deviceHubService.printEpson(request).subscribe(x => {
            this.deviceHubService.printEpson(request).subscribe();
          });
        }
      });
    });
  }

  public onBack() {
    this.orderRefundService.resetPurchaseItems();
    this.orderRefundService.resetRefundItems();
    this.currentPromotions = this.orderRefundService.getCurrentPromotions();
  }

  public onDelete(row: any) {
    const promotionIndex = this.currentPromotions.findIndex(x => x.id === row.id);
    this.currentPromotions.splice(promotionIndex, 1);
    this.currentPromotions = JSON.parse(JSON.stringify(this.currentPromotions));
    this.discountPrices = [];
    this.buildDataSourceForProductsAndPromotions();
  }

  public formatNumberDecimal(value: any) {
    if (value) {
      const num = Number(value);
      return num.toFixed(2);
    }
    return value;
  }

  getRowClass(row) {
    return row.type === null ? { 'header-title-back-ground-color': true }
      : { 'header-product-back-ground-color': row.type === SaleItemType.Product ? true : false };
  }

  private buildDataSourceForProductsAndPromotions() {
    let newDataSource = [];
    let purchasedItems = [];
    let refundedItems = [];
    if (this.purchasedItems.length > 0) {
      purchasedItems.push({
        description: 'PURCHASED ITEMS',
        quantity: null,
        price: null,
        amount: null,
        type: null,
        isPromotion: false
      });
      const newItems = this.addProductItemAndPromotion(this.purchasedItems);
      purchasedItems = purchasedItems.concat(newItems);
    }

    if (this.currentPromotions.length > 0) {
      const orderTotalPrice = this.calculateDatasourceTotalPrice(purchasedItems);
      purchasedItems.push({
        description: 'PROMOTIONS',
        quantity: null,
        price: null,
        amount: null,
        type: null,
        isPromotion: false
      });
      this.currentPromotions.forEach(orderPromotion => {
        const data = {
          id: orderPromotion.id,
          description: orderPromotion.reason,
          quantity: '',
          price: -1 * orderPromotion.value,
          type: SaleItemType.Promotion,
          isPromotion: true
        };

        if (orderPromotion.discountType === DiscountType.Percent) {
          data.description = (data.description ? data.description : '') + ` (${orderPromotion.value}%)`;
          data.price = -1 * PriceExtension.round(orderTotalPrice * orderPromotion.value / 100, 2);
        }
        this.discountPrices.push(Number(data.price));
        purchasedItems.push(data);
      });
    }

    this.currentAmount = PriceExtension.round(PriceExtension.calculateNetPrices(this.calculateDatasourceTotalPrice(purchasedItems),
      this.discountPrices, 0), 2);
    this.exchangeMoney = PriceExtension.round(this.orderRefundService.getCurrentOrder().amount - this.currentAmount, 2);

    if (this.refundedItems.length > 0) {
      refundedItems.push({
        description: 'REFUNDED ITEMS',
        quantity: null,
        price: null,
        amount: null,
        type: null,
        isPromotion: false
      });
      const newItems = this.addProductItemAndPromotion(this.refundedItems);
      refundedItems = refundedItems.concat(newItems);
    }

    newDataSource = purchasedItems.concat(refundedItems);

    this.dataSource = newDataSource;
  }

  private calculateDatasourceTotalPrice(datasource: any[]) {
    const filteredSource = datasource.filter(x => x.type === SaleItemType.Product || x.type === SaleItemType.ItemPromotion);
    let totalPrice = 0;
    filteredSource.forEach(element => {
      totalPrice += element.amount;
    });

    return totalPrice;
  }

  private addProductItemAndPromotion(orderItems: OrderItemModel[]) {
    const newDataSource = [];
    if (orderItems !== []) {
      orderItems.forEach(orderItem => {
        newDataSource.push({
          description: orderItem.variant,
          quantity: orderItem.quantity,
          price: orderItem.price,
          amount: orderItem.quantity * orderItem.price,
          type: SaleItemType.Product,
          isPromotion: false
        });

        const orderItemPromotions = orderItem.orderItemPromotions;
        if (Array.isArray(orderItemPromotions)) {
          orderItemPromotions.forEach(itemPromotion => {
            const data = {
              description: 'Item discount',
              quantity: '',
              price: 0,
              amount: 0,
              type: SaleItemType.ItemPromotion,
              isPromotion: false
            };

            if (itemPromotion.discountType === DiscountType.Money) {
              data.price = -1 * itemPromotion.value;
            }
            if (itemPromotion.discountType === DiscountType.Percent) {
              data.description += ` (${itemPromotion.value}%)`;
              data.price = -1 * PriceExtension.round(itemPromotion.value * orderItem.price * orderItem.quantity / 100, 2);
            }
            data.amount = data.price;

            newDataSource.push(data);
          });
        }
      });
    }
    return newDataSource;
  }
}
