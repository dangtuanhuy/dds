import * as fromRoot from 'src/app/shared/state/app.state';
import { StoreModel } from '../stores.component';
import { StoreActions, StoreActionTypes } from './store.action';
import { SortDirection } from 'src/app/shared/base-model/paging-filter-criteria';

export interface State extends fromRoot.State {
    stores: StoreState;
}

export interface StoreState {
    stores: StoreModel[];
    store: StoreModel;
    sortDirection: SortDirection;
    sortColumnName: String;
}

const initialState: StoreState = {
    stores: [],
    store: null,
    sortColumnName: '',
    sortDirection: SortDirection.Ascending
};

export const key = 'stores_reducer';

export function reducer(state = initialState, action: StoreActions): StoreState {
    switch (action.type) {
        case StoreActionTypes.GetStoresSuccess:
            return {
                ...state,
                stores: action.payload.data
            };
        case StoreActionTypes.GetStoreSuccess:
            return {
                ...state,
                store: action.payload
            };
        case StoreActionTypes.UpdateStoreSuccess:
            const updatedStores = state.stores.map(
                item => action.payload.id === item.id ? action.payload : item);
            return {
                ...state,
                stores: updatedStores
            };
        case StoreActionTypes.GetStores:
            return {
                ...state,
                sortColumnName: action.payload.sortColumn,
                sortDirection: action.payload.sortDirection
            };
        default:
            return state;
    }
}
