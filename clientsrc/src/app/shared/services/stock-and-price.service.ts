import { ServiceBase } from './service-base';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PagingFilterCriteria } from '../base-model/paging-filter-criteria';
import { StockAndPriceItemViewModel } from 'src/app/ims/stock-and-price/stock-and-price.model';
@Injectable({ providedIn: 'root' })

export class StockAndPriceService extends ServiceBase {
    getPage(page: number = 1, numberItemsPerPage: number = 10, queryText: string) {
        return this.pageWithSearchText(`${environment.app.ims.apiUrl}/api/stock-and-price`,
            new PagingFilterCriteria(page, numberItemsPerPage), queryText);
    }

    getByProduct(productId: string, queryText: string) {
        return this.get<StockAndPriceItemViewModel>
            (`${environment.app.ims.apiUrl}/api/stock-and-price/getByProduct?productId=${productId}&queryText=${queryText}`);
    }
}

