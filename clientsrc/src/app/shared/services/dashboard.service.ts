import { ServiceBase } from './service-base';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DashboardModel, DashboardRequestModel } from 'src/app/ims/dashboard/dashboard.model';
import { PurchaseOrderSummaryViewModel } from 'src/app/ims/dashboard/purchase-order/purchase-order.model';
import { TopProductBySalesViewModel } from 'src/app/ims/dashboard/top-sale-product/top-sale-product.model';
import { NewProductItemDashboardViewModel } from 'src/app/ims/dashboard/new-products/new-products.model';
import { PagingFilterCriteria } from '../base-model/paging-filter-criteria';

@Injectable({
    providedIn: 'root'
})
export class DashboardService extends ServiceBase {

    getByLocation(request: DashboardRequestModel) {
        return this.get<DashboardModel>
            (`${environment.app.ims.apiUrl}/api/dashboard?locationId=${request.locationId}&queryText=${request.queryText}`);
    }

    getPurchaseOrderSummary() {
        return this.get<PurchaseOrderSummaryViewModel[]>(`${environment.app.ims.apiUrl}/api/dashboard/purchaseOrderSummary`);
    }

    getTopProductBySales(fromDate: string, toDate: string) {
        return this.get<TopProductBySalesViewModel[]>
                    (`${environment.app.ims.apiUrl}/api/dashboard/top-product-sale/?fromDate=${fromDate}&toDate=${toDate}`);
    }

    getNewProducts(pageIndex: number = 1, numberItemsPerPage: number = 10) {
      const requestModel = new PagingFilterCriteria(pageIndex, numberItemsPerPage);
        return this.page<NewProductItemDashboardViewModel>(`${environment.app.ims.apiUrl}/api/dashboard/newProducts`, requestModel);
    }
}
