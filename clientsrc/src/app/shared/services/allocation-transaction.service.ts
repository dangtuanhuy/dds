import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ServiceBase } from './service-base';
import { PagingFilterCriteria, SortDirection } from '../base-model/paging-filter-criteria';
import {
  AllocationTransactionModel,
  UpdateStatusAllocationTransactionModel
} from 'src/app/ims/allocation-transaction/allocation-transaction.model';
import { QueryString } from '../base-model/query-string';
import { AllocationTransactionByListIdModel, TransferOutByAllocationRequestModel } from 'src/app/ims/transfer-out/transfer-out.model';
import { FilterRequestModel } from 'src/app/ims/goods-inwards/goods-inward.model';


@Injectable({ providedIn: 'root' })
export class AllocationTransactionService extends ServiceBase {
  // getAll(page: number = 1, numberItemsPerPage: number = 10, queryText: string = '') {
  //   return this.pageWithSearchText(`${environment.app.ims.apiUrl}/api/allocation/transaction`,
  //                                          new PagingFilterCriteria(page, numberItemsPerPage), queryText);
  // }

  getAll(page: number = 1, numberItemsPerPage: number = 10,  sortColumnName: string,
    sortDirection: SortDirection, stockAllcationFilterRequestModel: FilterRequestModel) {

    const queryStrings = [];
    let statusIdString = '';
    let toLocationIdString = '';
    let fromLocationIdString = '';

    statusIdString = this.parseArrayStringtoString(stockAllcationFilterRequestModel.statusIds);
    fromLocationIdString = this.parseArrayStringtoString(stockAllcationFilterRequestModel.fromLocationIds);
    toLocationIdString = this.parseArrayStringtoString(stockAllcationFilterRequestModel.toLocationIds);

    queryStrings.push(new QueryString('statusIdString', statusIdString));
    queryStrings.push(new QueryString('toLocationIdString', toLocationIdString));
    queryStrings.push(new QueryString('fromLocationIdString', fromLocationIdString));
    queryStrings.push(new QueryString('queryString', stockAllcationFilterRequestModel.queryString));
    return this.pageWithQueryString(`${environment.app.ims.apiUrl}/api/allocation/transaction`,
    queryStrings,
    new PagingFilterCriteria(page, numberItemsPerPage, sortColumnName, sortDirection));
  }


  getAllocationTransactionsByLocation(page: number = 1, numberItemsPerPage: number = 10, sortColumnName: string,
    sortDirection: SortDirection, transferOutByAllocationRequestModel: TransferOutByAllocationRequestModel) {
    const queryStrings = [];
    let toLocationIdString = '';
    let fromLocationIdString = '';

    fromLocationIdString = this.parseArrayStringtoString(transferOutByAllocationRequestModel.fromLocationIds);
    toLocationIdString = this.parseArrayStringtoString(transferOutByAllocationRequestModel.toLocationIds);
    queryStrings.push(new QueryString('fromLocationIds', fromLocationIdString));
    queryStrings.push(new QueryString('toLocationIds', toLocationIdString));
    queryStrings.push(new QueryString('fromDate', transferOutByAllocationRequestModel.fromDate));
    queryStrings.push(new QueryString('toDate', transferOutByAllocationRequestModel.toDate));
    return this.pageWithQueryString(`${environment.app.ims.apiUrl}/api/allocation/transaction/location`,
      queryStrings,
      new PagingFilterCriteria(page, numberItemsPerPage, sortColumnName, sortDirection));
  }

  getAllocationTransactionsByListId(allocationTransactionByListIdModel: AllocationTransactionByListIdModel)
    : Observable<AllocationTransactionByListIdModel> {
    return this.post(`${environment.app.ims.apiUrl}/api/allocation/transaction/bylistid`, allocationTransactionByListIdModel);
  }

  getBy(id: string): Observable<AllocationTransactionModel> {
    return this.get(`${environment.app.ims.apiUrl}/api/allocation/transaction/${id}`);
  }

  getMassAllocation(inventoryTransactionId: string) {
    return this.get(`${environment.app.ims.apiUrl}/api/allocation/transaction/mass?inventoryTransactionId=${inventoryTransactionId}`);
  }

  getMassAllocationByDate(fromDate: string, toDate: string, fromLocationId: string) {
    // tslint:disable-next-line:max-line-length
    return this.list(`${environment.app.ims.apiUrl}/api/allocation/transaction/outlet?FromDate=${fromDate}&ToDate=${toDate}&FromLocationId=${fromLocationId}`);
  }

  add(allocationTransactionModel: AllocationTransactionModel): Observable<AllocationTransactionModel> {
    return this.post(`${environment.app.ims.apiUrl}/api/allocation/transaction`, allocationTransactionModel);
  }

  addMulti(allocationTransactionModel: Array<AllocationTransactionModel>, fromDate: string, toDate: string)
    : Observable<Array<AllocationTransactionModel>> {
    return this.post(`${environment.app.ims.apiUrl}/api/allocation/transaction/multi?FromDate=${fromDate}&ToDate=${toDate}`,
      allocationTransactionModel);
  }

  update(allocationTransactionModel: AllocationTransactionModel): Observable<AllocationTransactionModel> {
    return this.put(`${environment.app.ims.apiUrl}/api/allocation/transaction/${allocationTransactionModel.id}`,
      allocationTransactionModel);
  }

  remove(id: string) {
    return this.delete(`${environment.app.ims.apiUrl}/api/allocation/transaction/${id}`);
  }

  updateStatus(updateStatusAllocationTransactionModel: UpdateStatusAllocationTransactionModel) {
    return this.put(`${environment.app.ims.apiUrl}/api/allocation/transaction/updateStatus/${updateStatusAllocationTransactionModel.id}`,
      updateStatusAllocationTransactionModel);
  }

  parseArrayStringtoString(arrayString: String[]) {
    if (!arrayString.length) { return ''; }

    let result = '';
    arrayString.map(string => {
      result += string + '|';
    });
    return result;
  }
}
