export class PagingFilterCriteria {
  page: number;
  numberItemsPerPage: number;
  sortColumn: string;
  sortDirection: SortDirection;

  constructor(
    page: number,
    numberItemsPerPage: number,
    sortColumn = '',
    sortDirection = SortDirection.Descending
  ) {
    this.page = page;
    this.numberItemsPerPage = numberItemsPerPage;
    this.sortColumn = sortColumn;
    this.sortDirection = sortDirection;
  }
}

export enum SortDirection {
  Ascending = 0,
  Descending = 1
}
