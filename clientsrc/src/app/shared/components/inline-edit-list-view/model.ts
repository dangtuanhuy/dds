export class ListViewColumn {
    name = '';
    width = '';
    isEdit = false;
    isNumber = false;
    isDecimalNumber = false;
    isIcon = false;
    displayedName = '';

    constructor(values: Object = {}) {
        if (values) {
            Object.assign(this, values);
        }
    }
}
