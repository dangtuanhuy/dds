import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { ActivityService } from './activity.service';
import { ActivitiesRequest, Activity, TypeAction, GetActivitiesByActionTypeRequest } from './activity.model';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ScreenSize } from 'src/app/shared/constants/common.constant';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ActivityComponent implements OnInit {

  public pageNumber: number;
  public pageSize: number;
  public totalItem: number;
  public dataList: Array<Activity> = [];
  public loadingIndicator = true;
  public searchText = '';
  @ViewChild('searchInput')
  searchInput!: ElementRef;
  @ViewChild('activityTable') table: any;
  public screenSizeDisplaySubscription: Subscription;
  public isSmallDevice = false;
  public isExtraSmallDevice = false;
  public isLargeDevice = false;

  public actionTypes: Array<TypeAction> = [];
  public selectedActionType: string | undefined = undefined;

  constructor(
    private activityService: ActivityService,
    private sharedService: SharedService) {
    this.pageNumber = 0;
    this.pageSize = 10;
    this.totalItem = 0;
    this.screenSizeDisplaySubscription = this.sharedService.screenSizeDisplay.subscribe(res => {
      if (res <= ScreenSize.largeDevices) {
        this.isLargeDevice = false;
      } else {
        this.isLargeDevice = true;
      }
      if (res <= ScreenSize.smallDevices) {
        this.isSmallDevice = false;
      } else {
        this.isSmallDevice = true;
      }
      if (res <= ScreenSize.extraSmallDevices) {
        this.isExtraSmallDevice = false;
      } else {
        this.isExtraSmallDevice = true;
      }
    });
  }

  ngOnInit() {
    this.onSearch();
    this.addKeyUpEventToSearchText();
    this.getActionTypes();
  }

  public loadData(request: ActivitiesRequest) {
    this.activityService.getActivities(request).subscribe(res => {
      if (res) {
        const temp = res;
        this.pageNumber = temp.pageNumber;
        this.pageSize = temp.pageSize;
        this.totalItem = temp.totalItem;
        this.loadingIndicator = false;
        this.dataList = temp.actionActivityModels.map(result => {
          const model = Activity.buildActivity(result);
          return model;
        });
      }
    });
  }

  setPage(pageInfo: { offset: number; }) {
    const request: ActivitiesRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      searchText: this.searchText,
      dateFilter: ''
    };
    this.loadData(request);
  }

  onSearch() {
    this.setPage({ offset: 0 });
  }

  addKeyUpEventToSearchText() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .subscribe(() => {
        this.pageNumber = 0;
        this.selectedActionType = undefined;
        this.onSearch();
      });
  }

  toggleExpandRow(row: any) {
    this.table.rowDetail.collapseAllRows();
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle() {
  }

  getActionTypes() {
    this.activityService.getActionTypes().subscribe(res => {
      this.actionTypes = res.actionTypes;
    });
  }

  onChange(event: any) {
    this.pageNumber = 0;
    this.searchText = '';
    if (this.selectedActionType) {
      this.getActivitiesByActionType(this.selectedActionType);
    }
  }

  getActivitiesByActionType(actionTypeId: string) {
    const request: GetActivitiesByActionTypeRequest = {
      actionTypeId: actionTypeId,
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };

    this.activityService.getActivitiesByActionType(request).subscribe(res => {
      if (res) {
        const temp = res;
        this.pageNumber = temp.pageNumber;
        this.pageSize = temp.pageSize;
        this.totalItem = temp.totalItem;
        this.loadingIndicator = false;
        this.dataList = temp.actionActivityModels.map(result => {
          const model = Activity.buildActivity(result);
          return model;
        });
      }
    });
  }
}
