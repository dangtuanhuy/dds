import { Component, OnInit, Input } from '@angular/core';
import { PointTransaction } from 'src/app/containers/customer/customer-detail/customer-detail.model';
import { HomepageService } from 'src/app/containers/homepage/homepage.service';
import {
  DebitPointTransactionRequest,
  CreditPointTransactionRequest,
  VoidOfDebitPointTransactionRequest,
  VoidOfCreditPointTransactionRequest,
  ItemPointSummaryTable,
  GetOutletsPointSummaryRequest
} from 'src/app/containers/homepage/homepage.model';
import { Subject } from 'rxjs';
import { OutletModel } from '../../outlet/outlet.model';

@Component({
  selector: 'app-point-summary',
  templateUrl: './point-summary.component.html',
  styleUrls: ['./point-summary.component.scss']
})
export class PointSummaryComponent implements OnInit {

  @Input() outletRequest: OutletModel | undefined = undefined;
  @Input() fromDateRequest: any;
  @Input() toDateRequest: any;
  @Input() appSummaryLabelTitle = '';
  @Input() eventUpdatePointData: Subject<any> = new Subject();

  public debitValuePointTransactionList: Array<PointTransaction> = [];
  public totalItemDebitValuePointTransaction = 0;
  public isCollapseDebitValuePointTransaction = false;
  public pageNumberDebitValuePointTransaction = 0;
  public loadingIndicatorDebitValuePointTransaction = true;
  public isLoadingDebitValuePointTransaction = false;

  public creditPointTransactionList: Array<PointTransaction> = [];
  public totalItemCreditPointTransaction = 0;
  public isCollapsedCreditPointTransaction = false;
  public pageNumberCreditPointTransaction = 0;
  public loadingIndicatorCreditPointTransaction = true;
  public isLoadingCreditPointTransaction = false;

  public voidOfDebitValuePointTransactionList: Array<PointTransaction> = [];
  public totalItemVoidOfDebitValuePointTransaction = 0;
  public isCollapseVoidOfDebitValuePointTransaction = false;
  public pageNumberVoidOfDebitValuePointTransaction = 0;
  public loadingIndicatorVoidOfDebitValuePointTransaction = true;
  public isLoadingVoidOfDebitValuePointTransaction = false;

  public voidOfCreditPointTransactionList: Array<PointTransaction> = [];
  public totalItemVoidOfCreditPointTransaction = 0;
  public isCollapsedVoidOfCreditPointTransaction = false;
  public pageNumberVoidOfCreditPointTransaction = 0;
  public loadingIndicatorVoidOfCreditPointTransaction = true;
  public isLoadingVoidOfCreditPointTransaction = false;

  public pageSize = 10;
  public loadingTotalBalancePointTransaction = true;

  public totalBalancePointTrans = 0;
  public totalCreditPointTrans = 0;
  public totalDebitPointTrans = 0;
  public totalVoidOfCreditPointTrans = 0;
  public totalVoidOfDebitPointTrans = 0;

  public outletPointSummaryList: Array<ItemPointSummaryTable> = [];
  public totalItemOutletPointSummaryList = 0;
  public pageNumberOutletPointSummaryList = 0;
  public loadingIndicatorOutletPointSummaryList = true;

  constructor(
    private homepageService: HomepageService
  ) { }

  ngOnInit() {
    this.setOutletPointSummaryList();
    this.setDataSummary();
    this.eventUpdatePointData.subscribe(event => {
      this.setDataSummary();
      if (!this.outletRequest) {
        this.setOutletPointSummaryList();
      }
    });
  }

  setIsLoadingData() {
    this.isLoadingCreditPointTransaction = true;
    this.isLoadingDebitValuePointTransaction = true;
    this.isLoadingVoidOfCreditPointTransaction = true;
    this.isLoadingVoidOfDebitValuePointTransaction = true;
  }

  public loadDebitValuePointTransaction(request: DebitPointTransactionRequest) {
    this.loadingIndicatorDebitValuePointTransaction = true;
    this.homepageService.GetDebitValuePointTransaction(request).subscribe(res => {
      const temp = res;
      this.pageNumberDebitValuePointTransaction = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItemDebitValuePointTransaction = temp.totalItem;
      this.loadingIndicatorDebitValuePointTransaction = false;
      this.isLoadingDebitValuePointTransaction = false;
      this.totalDebitPointTrans = temp.totalDebitValue;
      if (temp.listPointTransaction != null) {
        this.debitValuePointTransactionList = temp.listPointTransaction.map((result: any) => {
          const pointTransactionModel = PointTransaction.buildPointTransaction(result);
          return pointTransactionModel;
        });
      }
      this.loadTotalBalancePointTransaction();
      if (this.outletRequest) {
        this.updateOutletPointSummaryList();
      }
    });
  }

  public loadVoidOfCreditValuePointTransaction(request: VoidOfCreditPointTransactionRequest) {
    this.loadingIndicatorVoidOfCreditPointTransaction = true;
    this.homepageService.GetVoidOfCreditValuePointTransaction(request).subscribe(res => {
      const temp = res;
      this.pageNumberVoidOfCreditPointTransaction = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItemVoidOfCreditPointTransaction = temp.totalItem;
      this.totalVoidOfCreditPointTrans = temp.totalVoidOfCreditValue;
      this.loadingIndicatorVoidOfCreditPointTransaction = false;
      this.isLoadingVoidOfCreditPointTransaction = false;
      if (temp.listPointTransaction != null) {
        this.voidOfCreditPointTransactionList = temp.listPointTransaction.map((result: any) => {
          const pointTransactionModel = PointTransaction.buildPointTransaction(result);
          return pointTransactionModel;
        });
      }
      this.loadTotalBalancePointTransaction();
      if (this.outletRequest) {
        this.updateOutletPointSummaryList();
      }
    });
  }

  public loadVoidOfDebitValuePointTransaction(request: VoidOfDebitPointTransactionRequest) {
    this.loadingIndicatorVoidOfDebitValuePointTransaction = true;
    this.homepageService.GetVoidOfDebitValuePointTransaction(request).subscribe(res => {
      const temp = res;
      this.pageNumberVoidOfDebitValuePointTransaction = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItemVoidOfDebitValuePointTransaction = temp.totalItem;
      this.loadingIndicatorVoidOfDebitValuePointTransaction = false;
      this.isLoadingVoidOfDebitValuePointTransaction = false;
      this.totalVoidOfDebitPointTrans = temp.totalVoidOfDebitValue;
      if (temp.listPointTransaction != null) {
        this.voidOfDebitValuePointTransactionList = temp.listPointTransaction.map((result: any) => {
          const pointTransactionModel = PointTransaction.buildPointTransaction(result);
          return pointTransactionModel;
        });
      }
      this.loadTotalBalancePointTransaction();
      if (this.outletRequest) {
        this.updateOutletPointSummaryList();
      }
    });
  }

  public loadCreditValuePointTransaction(request: CreditPointTransactionRequest) {
    this.loadingIndicatorCreditPointTransaction = true;
    this.homepageService.GetCreditValuePointTransaction(request).subscribe(res => {
      const temp = res;
      this.pageNumberCreditPointTransaction = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItemCreditPointTransaction = temp.totalItem;
      this.totalCreditPointTrans = temp.totalCreditValue;
      this.loadingIndicatorCreditPointTransaction = false;
      this.isLoadingCreditPointTransaction = false;
      if (temp.listPointTransaction != null) {
        this.creditPointTransactionList = temp.listPointTransaction.map((result: any) => {
          const pointTransactionModel = PointTransaction.buildPointTransaction(result);
          return pointTransactionModel;
        });
      }
      this.loadTotalBalancePointTransaction();
      if (this.outletRequest) {
        this.updateOutletPointSummaryList();
      }
    });
  }

  setDebitValuePointTransactionPage(pageInfo: { offset: number; }) {
    const request: DebitPointTransactionRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      fromDateFilter: this.fromDateRequest,
      toDateFilter: this.toDateRequest,
      outletId: this.outletRequest ? this.outletRequest.id : ''
    };
    this.loadDebitValuePointTransaction(request);
  }

  setCreditValuePointTransactionPage(pageInfo: { offset: number; }) {
    const request: CreditPointTransactionRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      fromDateFilter: this.fromDateRequest,
      toDateFilter: this.toDateRequest,
      outletId: this.outletRequest ? this.outletRequest.id : ''
    };
    this.loadCreditValuePointTransaction(request);
  }

  setVoidOfCreditValuePointTransactionPage(pageInfo: { offset: number; }) {
    const request: VoidOfCreditPointTransactionRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      fromDateFilter: this.fromDateRequest,
      toDateFilter: this.toDateRequest,
      outletId: this.outletRequest ? this.outletRequest.id : ''
    };
    this.loadVoidOfCreditValuePointTransaction(request);
  }

  setVoidOfDebitValuePointTransactionPage(pageInfo: { offset: number; }) {
    const request: VoidOfDebitPointTransactionRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      fromDateFilter: this.fromDateRequest,
      toDateFilter: this.toDateRequest,
      outletId: this.outletRequest ? this.outletRequest.id : ''
    };
    this.loadVoidOfDebitValuePointTransaction(request);
  }

  public loadTotalBalancePointTransaction() {
    this.totalBalancePointTrans =
      this.totalCreditPointTrans + this.totalVoidOfDebitPointTrans - this.totalDebitPointTrans - this.totalVoidOfCreditPointTrans;
  }

  setDataSummary() {
    this.setIsLoadingData();
    this.setDebitValuePointTransactionPage({
      offset: 0
    });
    this.setCreditValuePointTransactionPage({
      offset: 0
    });
    this.setVoidOfCreditValuePointTransactionPage({
      offset: 0
    });
    this.setVoidOfDebitValuePointTransactionPage({
      offset: 0
    });
    this.loadTotalBalancePointTransaction();
    if (this.outletRequest) {
      this.updateOutletPointSummaryList();
    }
  }

  isDoneLoadData() {
    return (!this.isLoadingCreditPointTransaction
      && !this.isLoadingDebitValuePointTransaction
      && !this.isLoadingVoidOfCreditPointTransaction
      && !this.isLoadingVoidOfDebitValuePointTransaction) ? true : false;
  }

  setOutletPointSummaryList() {
    this.loadingIndicatorOutletPointSummaryList = true;
    const request: GetOutletsPointSummaryRequest = {
      fromDate: this.fromDateRequest,
      toDate: this.toDateRequest
    };

    this.homepageService.GetOutletsPointSummary(request).subscribe(res => {
      this.outletPointSummaryList = res.pointSummaryItems;
      this.totalItemOutletPointSummaryList = this.outletPointSummaryList.length;
      this.loadingIndicatorOutletPointSummaryList = false;
    });
  }

  updateOutletPointSummaryList() {
    this.loadingIndicatorOutletPointSummaryList = true;
    const outletName = this.outletRequest ? this.outletRequest.name : '';
    const item = this.outletPointSummaryList.find(x => x.outletName === outletName);
    if (item && item !== undefined && this.isDoneLoadData()) {
      const indexItem = this.outletPointSummaryList.indexOf(item);
      this.loadingIndicatorOutletPointSummaryList = true;

      const itemUpdate: ItemPointSummaryTable = {
        outletName: outletName,
        totalAdd: this.totalCreditPointTrans,
        totalRedeem: this.totalDebitPointTrans,
        totalVoidOfAdd: this.totalVoidOfCreditPointTrans,
        totalVoidOfRedeem: this.totalVoidOfDebitPointTrans
      };

      this.outletPointSummaryList[indexItem] = itemUpdate;
      this.outletPointSummaryList = [...this.outletPointSummaryList];
      this.loadingIndicatorOutletPointSummaryList = false;
    }
  }
}

