import { Component, OnInit, Input } from '@angular/core';
import { WalletTransaction } from 'src/app/containers/customer/customer-detail/customer-detail.model';
import { HomepageService } from 'src/app/containers/homepage/homepage.service';
import {
  DebitWalletTransactionRequest,
  VoidOfDebitWalletTransactionRequest,
  VoidOfCreditWalletTransactionRequest,
  CreditPointTransactionRequest,
  CreditWalletTransactionRequest,
  ItemWalletSummaryTable,
  GetOutletsWalletSummaryRequest
} from 'src/app/containers/homepage/homepage.model';
import { Subject } from 'rxjs';
import { OutletModel } from '../../outlet/outlet.model';

@Component({
  selector: 'app-wallet-summary',
  templateUrl: './wallet-summary.component.html',
  styleUrls: ['./wallet-summary.component.scss']
})
export class WalletSummaryComponent implements OnInit {

  @Input() outletRequest: OutletModel | undefined = undefined;
  @Input() fromDateRequest: any;
  @Input() toDateRequest: any;
  @Input() appSummaryLabelTitle = '';
  @Input() eventUpdateWalletData: Subject<any> = new Subject();

  public debitWalletTransactionList: Array<WalletTransaction> = [];
  public totalItemDebitWalletTransaction = 0;
  public isCollapsedDebitWalletTrans = false;
  public pageNumberDebitWallet = 0;
  public loadingIndicatorDebitWalletTransaction = true;
  public isLoadingTotalDebitWallet = false;

  public creditWalletTransactionList: Array<WalletTransaction> = [];
  public totalItemCreditWalletTransaction = 0;
  public isCollapsedCreditWalletTrans = false;
  public pageNumberCreditWallet = 0;
  public loadingIndicatorCreditWalletTransaction = true;
  public isLoadingTotalCreditWallet = false;

  public voidOfDebitWalletTransactionList: Array<WalletTransaction> = [];
  public totalItemVoidOfDebitWalletTransaction = 0;
  public isCollapsedVoidOfDebitWalletTrans = false;
  public pageNumberVoidOfDebitWallet = 0;
  public loadingIndicatorVoidOfDebitWalletTransaction = true;
  public isLoadingTotalVoidOfDebitWallet = false;

  public voidOfCreditWalletTransactionList: Array<WalletTransaction> = [];
  public totalItemVoidOfCreditWalletTransaction = 0;
  public isCollapsedVoidOfCreditWalletTrans = false;
  public pageNumberVoidOfCreditWallet = 0;
  public loadingIndicatorVoidOfCreditWalletTransaction = true;
  public isLoadingTotalVoidOfCreditWallet = false;

  public totalBalanceWallet = 0;
  public totalCreditWallet = 0;
  public totalDebitWallet = 0;
  public totalVoidOfCreditWallet = 0;
  public totalVoidOfDebitWallet = 0;

  public pageSize = 10;
  public loadingTotalBalancePointTransaction = true;

  public outletWalletSummaryList: Array<ItemWalletSummaryTable> = [];
  public totalItemOutletWalletSummaryList = 0;
  public pageNumberOutletWalletSummaryList = 0;
  public loadingIndicatorOutletWalletSummaryList = true;

  constructor(
    private homepageService: HomepageService
  ) { }

  ngOnInit() {
    this.setOutletWalletSummaryList();
    this.setDataSummary();
    this.eventUpdateWalletData.subscribe(event => {
      this.setDataSummary();
      if (!this.outletRequest) {
        this.setOutletWalletSummaryList();
      }
    });
  }

  setIsLoadingData() {
    this.isLoadingTotalCreditWallet = true;
    this.isLoadingTotalDebitWallet = true;
    this.isLoadingTotalVoidOfCreditWallet = true;
    this.isLoadingTotalVoidOfDebitWallet = true;
  }

  public loadDebitValueWalletTransaction(request: DebitWalletTransactionRequest) {
    this.loadingIndicatorDebitWalletTransaction = true;
    this.homepageService.GetDebitValueWalletTransaction(request).subscribe(res => {
      const temp = res;
      this.pageNumberDebitWallet = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItemDebitWalletTransaction = temp.totalItem;
      this.loadingIndicatorDebitWalletTransaction = false;
      this.isLoadingTotalDebitWallet = false;
      this.totalDebitWallet = temp.totalDebitValue;
      if (temp.listWalletTransaction != null) {
        this.debitWalletTransactionList = temp.listWalletTransaction.map((result: any) => {
          const pointTransactionModel = WalletTransaction.buildWalletTransaction(result);
          return pointTransactionModel;
        });
      }
      this.loadTotalBalanceWalletTransaction();
      if (this.outletRequest) {
        this.updateOutletWalletSummaryList();
      }
    });
  }

  public loadCreditValueWalletTransaction(request: CreditPointTransactionRequest) {
    this.loadingIndicatorCreditWalletTransaction = true;
    this.homepageService.GetCreditValueWalletTransaction(request).subscribe(res => {
      const temp = res;
      this.pageNumberCreditWallet = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItemCreditWalletTransaction = temp.totalItem;
      this.totalCreditWallet = temp.totalCreditValue;
      this.loadingIndicatorCreditWalletTransaction = false;
      this.isLoadingTotalCreditWallet = false;
      if (temp.listWalletTransaction != null) {
        this.creditWalletTransactionList = temp.listWalletTransaction.map((result: any) => {
          const walletTransactionModel = WalletTransaction.buildWalletTransaction(result);
          return walletTransactionModel;
        });
      }
      this.loadTotalBalanceWalletTransaction();
      if (this.outletRequest) {
        this.updateOutletWalletSummaryList();
      }
    });
  }

  public loadVoidOfCreditValueWalletTransaction(request: VoidOfCreditWalletTransactionRequest) {
    this.loadingIndicatorVoidOfCreditWalletTransaction = true;
    this.homepageService.GetVoidOfCreditValueWalletTransaction(request).subscribe(res => {
      const temp = res;
      this.pageNumberVoidOfCreditWallet = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItemVoidOfCreditWalletTransaction = temp.totalItem;
      this.totalVoidOfCreditWallet = temp.totalVoidOfCreditValue;
      this.loadingIndicatorVoidOfCreditWalletTransaction = false;
      this.isLoadingTotalVoidOfCreditWallet = false;
      if (temp.listWalletTransaction != null) {
        this.voidOfCreditWalletTransactionList = temp.listWalletTransaction.map((result: any) => {
          const walletTransactionModel = WalletTransaction.buildWalletTransaction(result);
          return walletTransactionModel;
        });
      }
      this.loadTotalBalanceWalletTransaction();
      if (this.outletRequest) {
        this.updateOutletWalletSummaryList();
      }
    });
  }

  public loadVoidOfDebitValueWalletTransaction(request: VoidOfDebitWalletTransactionRequest) {
    this.loadingIndicatorVoidOfDebitWalletTransaction = true;
    this.homepageService.GetVoidOfDebitValueWalletTransaction(request).subscribe(res => {
      const temp = res;
      this.pageNumberVoidOfDebitWallet = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItemVoidOfDebitWalletTransaction = temp.totalItem;
      this.loadingIndicatorVoidOfDebitWalletTransaction = false;
      this.isLoadingTotalVoidOfDebitWallet = false;
      this.totalVoidOfDebitWallet = temp.totalVoidOfDebitValue;
      if (temp.listWalletTransaction != null) {
        this.voidOfDebitWalletTransactionList = temp.listWalletTransaction.map((result: any) => {
          const walletTransactionModel = WalletTransaction.buildWalletTransaction(result);
          return walletTransactionModel;
        });
      }
      this.loadTotalBalanceWalletTransaction();
      if (this.outletRequest) {
        this.updateOutletWalletSummaryList();
      }
    });
  }

  setDebitValueWalletTransactionPage(pageInfo: { offset: number; }) {
    const outletId = this.outletRequest ? this.outletRequest.id : '';
    const request: DebitWalletTransactionRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      fromDateFilter: this.fromDateRequest,
      toDateFilter: this.toDateRequest,
      outletId: outletId
    };
    this.loadDebitValueWalletTransaction(request);
  }

  setCreditValueWalletTransactionPage(pageInfo: { offset: number; }) {
    const outletId = this.outletRequest ? this.outletRequest.id : '';
    const request: CreditWalletTransactionRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      fromDateFilter: this.fromDateRequest,
      toDateFilter: this.toDateRequest,
      outletId: outletId
    };
    this.loadCreditValueWalletTransaction(request);
  }

  setVoidOfCreditValueWalletTransactionPage(pageInfo: { offset: number; }) {
    const outletId = this.outletRequest ? this.outletRequest.id : '';
    const request: VoidOfCreditWalletTransactionRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      fromDateFilter: this.fromDateRequest,
      toDateFilter: this.toDateRequest,
      outletId: outletId
    };
    this.loadVoidOfCreditValueWalletTransaction(request);
  }

  setVoidOfDebitValueWalletTransactionPage(pageInfo: { offset: number; }) {
    const outletId = this.outletRequest ? this.outletRequest.id : '';
    const request: VoidOfDebitWalletTransactionRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      fromDateFilter: this.fromDateRequest,
      toDateFilter: this.toDateRequest,
      outletId: outletId
    };
    this.loadVoidOfDebitValueWalletTransaction(request);
  }

  public loadTotalBalanceWalletTransaction() {
    const floatNumber =
      this.calculateFloatNumber([
        this.totalCreditWallet,
        this.totalVoidOfDebitWallet,
        this.totalDebitWallet,
        this.totalVoidOfCreditWallet]);
    this.totalBalanceWallet =
      (this.totalCreditWallet * floatNumber
        + this.totalVoidOfDebitWallet * floatNumber
        - this.totalDebitWallet * floatNumber
        - this.totalVoidOfCreditWallet * floatNumber) / floatNumber;
  }

  private calculateFloatNumber(arrNumber: number[]) {
    if (arrNumber.length === 0) {
      return 0;
    }
    let maxDecimalPlace = this.getDecimalPlace(arrNumber[0]);
    for (let i = 0; i < arrNumber.length; i++) {
      if (this.getDecimalPlace(arrNumber[i]) > maxDecimalPlace) {
        maxDecimalPlace = this.getDecimalPlace(arrNumber[i]);
      }
    }
    return Math.pow(10, maxDecimalPlace);
  }

  private getDecimalPlace(number: number) {
    const valueNumbers = number.toString().split('.');
    return valueNumbers.length > 1 ? valueNumbers[1].length : 0;
  }

  setDataSummary() {
    this.setIsLoadingData();
    this.setCreditValueWalletTransactionPage({
      offset: 0
    });
    this.setDebitValueWalletTransactionPage({
      offset: 0
    });
    this.setVoidOfCreditValueWalletTransactionPage({
      offset: 0
    });
    this.setVoidOfDebitValueWalletTransactionPage({
      offset: 0
    });
    this.loadTotalBalanceWalletTransaction();
    if (this.outletRequest) {
      this.updateOutletWalletSummaryList();
    }
  }

  setOutletWalletSummaryList() {
    this.loadingIndicatorOutletWalletSummaryList = true;
    const request: GetOutletsWalletSummaryRequest = {
      fromDate: this.fromDateRequest,
      toDate: this.toDateRequest
    };

    this.homepageService.GetOutletsWalletSummary(request).subscribe(res => {
      this.outletWalletSummaryList = res.walletSummaryItems;
      this.totalItemOutletWalletSummaryList = this.outletWalletSummaryList.length;
      this.loadingIndicatorOutletWalletSummaryList = false;
    });
  }

  isDoneLoadData() {
    return (!this.isLoadingTotalCreditWallet
      && !this.isLoadingTotalDebitWallet
      && !this.isLoadingTotalVoidOfCreditWallet
      && !this.isLoadingTotalVoidOfDebitWallet) ? true : false;
  }

  updateOutletWalletSummaryList() {
    this.loadingIndicatorOutletWalletSummaryList = true;
    const outletName = this.outletRequest ? this.outletRequest.name : '';
    const item = this.outletWalletSummaryList.find(x => x.outletName === outletName);
    if (item && item !== undefined && this.isDoneLoadData()) {
      const indexItem = this.outletWalletSummaryList.indexOf(item);
      this.loadingIndicatorOutletWalletSummaryList = true;

      const itemUpdate: ItemWalletSummaryTable = {
        outletName: outletName,
        totalSpend: this.totalDebitWallet,
        totalTopup: this.totalCreditWallet,
        totalVoidOfSpend: this.totalVoidOfDebitWallet,
        totalVoidOfTopup: this.totalVoidOfCreditWallet
      };

      this.outletWalletSummaryList[indexItem] = itemUpdate;
      this.outletWalletSummaryList = [...this.outletWalletSummaryList];
      this.loadingIndicatorOutletWalletSummaryList = false;
    }
  }
}

