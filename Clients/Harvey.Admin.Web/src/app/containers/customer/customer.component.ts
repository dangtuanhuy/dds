import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/containers/customer/customer.service';
import { CustomersRequest, CustomerInfo, GetBalanceCustomerRequest } from 'src/app/containers/customer/customer.model';
import { ViewEncapsulation } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadFileComponent } from 'src/app/shared/components/upload-file/upload-file.component';
import { ExportCSVService } from 'src/app/shared/services/export-csv.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../shared/services/shared.service';
import { ScreenSize } from '../../shared/constants/common.constant';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerComponent implements OnInit {

  constructor(
    private cutomerService: CustomerService,
    private modalService: NgbModal,
    private exportCSVService: ExportCSVService,
    private toast: ToastrService,
    private translate: TranslateService,
    private sharedService: SharedService) {
    this.pageNumber = 0;
    this.pageSize = 10;
    this.totalItem = 0;
    this.screenSizeDisplaySubscription = this.sharedService.screenSizeDisplay.subscribe(res => {
      if (res > ScreenSize.smallDevices) {
        this.isSmallDevice = false;
        this.isExtraSmallDevice = false;
      } else {
        if (res > ScreenSize.extraSmallDevices && res <= ScreenSize.smallDevices) {
          this.isSmallDevice = true;
          this.isExtraSmallDevice = false;
        }
        if (res <= ScreenSize.extraSmallDevices) {
          this.isSmallDevice = false;
          this.isExtraSmallDevice = true;
        }
      }
    });
  }

  public pageNumber: number;
  public pageSize: number;
  public totalItem: number;
  public customerList: Array<CustomerInfo> = [];
  public loadingIndicator = true;
  public searchText = '';
  public isLoading = false;
  public screenSizeDisplaySubscription: Subscription;
  public isSmallDevice = false;
  public isExtraSmallDevice = false;
  rows: any[] = [];
  expanded: any = {};
  @ViewChild('searchInput')
  searchInput!: ElementRef;

  @ViewChild('tableCustomer') tableCustomer: any;
  public isLoadMembership = false;
  public isLoadBalance = false;
  ngOnInit() {
    window.scrollTo(0, 0);
    this.onSearch();
    this.addKeyUpEventToSearchText();
  }

  public loadCustomers(request: CustomersRequest) {
    this.cutomerService.GetCustomers(request).subscribe(res => {
      const temp = res;
      this.pageNumber = temp.pageNumber;
      this.pageSize = temp.pageSize;
      this.totalItem = temp.totalItem;
      this.loadingIndicator = false;
      this.customerList = temp.customerListResponse.map(result => {
        const customerModel = CustomerInfo.buildCustomerInfo(result);
        return customerModel;
      });
    });
  }

  setPage(pageInfo: { offset: number; }) {
    const request: CustomersRequest = {
      pageNumber: pageInfo.offset,
      pageSize: this.pageSize,
      searchText: this.searchText,
      outletId: '',
      dateFilter: ''
    };
    this.loadCustomers(request);
  }

  onSearch() {
    this.setPage({ offset: 0 });
  }

  addKeyUpEventToSearchText() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .subscribe(() => {
        this.onSearch();
      });
  }

  onClickUploadFile() {
    const dialogRef = this.modalService.open(UploadFileComponent, { size: 'lg', centered: true, backdrop: 'static' });
  }

  onClickExportCSV() {
    this.isLoading = true;
    this.exportCSVService.ExportCustomerCSV().subscribe((data) => {
      const blob = new Blob([data], { type: 'text/csv' });
      const url = window.URL.createObjectURL(blob);

      if (navigator.msSaveOrOpenBlob) {
        navigator.msSaveBlob(blob, 'Customers.csv');
      } else {
        const a = document.createElement('a');
        a.href = url;
        a.download = 'Customers.csv';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      }
      window.URL.revokeObjectURL(url);
      this.isLoading = false;
    });
  }

  onClickUpdateGender() {
    this.isLoading = true;
    this.cutomerService.UpdateGender(null).subscribe((data) => {
      if (data === 1) {
        this.translate.get('APP.CUSTOMER_COMPONENT.UPDATE_SUCCESSFULLY').subscribe(message => {
          this.toast.success(message);
        });
      } else {
        this.translate.get('APP.ERROR.UPDATE_FAILED').subscribe(message => {
          this.toast.success(message);
        });
      }
      this.isLoading = false;
    });
  }

  toggleExpandRow(row: any) {
    this.tableCustomer.rowDetail.collapseAllRows();
    this.tableCustomer.rowDetail.toggleExpandRow(row);
    this.isLoadBalance = true;

    const requestBalance: GetBalanceCustomerRequest = {
      customerId: row.id
    };
    this.cutomerService.GetBalanceCustomer(requestBalance).subscribe(res => {
      this.isLoadBalance = false;
      if (res) {
        row.pointBalance = res.pointBalance;
        row.walletBalance = res.walletBalance;
      } else {
        row.pointBalance = 0;
        row.walletBalance = 0;
      }
    });
  }
}
