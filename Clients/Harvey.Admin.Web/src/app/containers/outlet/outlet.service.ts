import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../../shared/services/http.service';
import { environment } from '../../../environments/environment';
import HttpParamsHelper from '../../shared/helper/http-params.helper';
import { GetOutletsRequest, GetOutletsReponse, OutletModel } from './outlet.model';

const OutletResourceUrl = 'api/outlets/getOutletsWithStoreAccount';
const UpdateOutletResourceUrl = 'api/outlets/updateOutlet';
const getOutletsWithoutPaging = 'api/outlets/getsAllWithoutPaging';

@Injectable()
export class OutletService {

    constructor(private httpService: HttpService) { }

    getOutlets(request: GetOutletsRequest): Observable<GetOutletsReponse> {
        const apiUrl = `${environment.apiGatewayUri}/${OutletResourceUrl}`;
        return this.httpService.get(apiUrl, { params: HttpParamsHelper.parseObjectToHttpParams(request) });
    }

    getOutletsWithoutPaging(): Observable<GetOutletsReponse> {
        const apiUrl = `${environment.apiGatewayUri}/${getOutletsWithoutPaging}`;
        return this.httpService.get(apiUrl, { params: HttpParamsHelper.parseObjectToHttpParams() });
    }
    updateOutlets(request: OutletModel): Observable<string> {
        const apiUrl = `${environment.apiGatewayUri}/${UpdateOutletResourceUrl}`;
        return this.httpService.post(apiUrl, request);
    }
}
