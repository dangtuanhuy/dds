export const AppSettingLabelConstants = {
    AdminAppLoginLogoImageText: 'AdminAppLoginLogoImage',
    AdminAppHeaderLogoImageText: 'AdminAppHeaderLogoImage',
    AdminAppSummaryLabelTitleText: 'AdminAppSummaryLabelTitle'
};

export const AppSettingLabelByContentTypeConstants = {
    BrandTitleValue: 'BrandTitleValue',
    AcronymBrandTitleValue: 'AcronymBrandTitleValue'
};

export const CommonConstants = {
    storeMembershipBasicButtonColor: 'MembershipBasicButtonColor',
    storeMembershipPremiumButtonColor: 'MembershipPremiumButtonColor'
};

export const ScreenSize = {
    extraSmallDevices: 576,
    smallDevices: 768,
    mediumDevices: 992,
    largeDevices: 1200
};
