import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public screenSizeDisplay = new BehaviorSubject<any>(0);

  constructor() { }
}
