export const environment = {
  production: true,
  authorityUri: 'https://ids.harvey.demo.retaildds.net',
  apiGatewayUri: 'https://apigateway.harvey.demo.retaildds.net',
  memberPageUrl: 'https://member.app.harvey.demo.retaildds.net'
};

