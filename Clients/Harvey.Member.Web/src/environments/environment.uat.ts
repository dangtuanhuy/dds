export const environment = {
  production: true,
  authorityUri: 'https://ids.crm.dev.retaildds.net',
  apiGatewayUri: 'https://api.crm.dev.retaildds.net'
};
