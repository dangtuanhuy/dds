export const CommonConstants = {
  currentOutlet: 'currentOutlet',
  currentCustomer: 'currentCustomer',
  currentUser: 'currentUser',
  userTokenKey: 'access_token',
  redirectUrlKey: 'redirectUrl',
  appSettings: 'appSettings',
  tokenExpired: 'expires_at',
  expired_token: 'expires_token',
  storeTypeAppsetting: 2,
  storeAppSummaryLabelTitle: 'StoreAppSummaryLabelTitle',
  storeHomeAppTitle: 'StoreAppHomeTitle',
  storeAppOutletLogoImage: 'StoreAppOutletLogoImage',
  storeAppHeaderLogoImage: 'StoreAppHeaderLogoImage',
  storeAppLoginLogoImage: 'StoreAppLoginLogoImage',
  storeContentTermOfUse: 'StoreContentTermOfUse',
  storeHeaderTermOfUse: 'StoreHeaderTermOfUse',
  storeContentPrivacyPolicy: 'StoreContentPrivacyPolicy',
  storeMembershipBasicButtonColor: 'MembershipBasicButtonColor',
  storeMembershipPremiumButtonColor: 'MembershipPremiumButtonColor',
  storePanelColor: 'PanelColor',
  storeWallPaper: 'StoreAppWallpaper'
};

export const AppSettingLabelByContentTypeConstants = {
  BrandTitleValue: 'BrandTitleValue',
  AcronymBrandTitleValue: 'AcronymBrandTitleValue'
};
