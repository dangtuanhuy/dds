import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { OAuthService, OAuthStorage } from 'angular-oauth2-oidc';
import { CommonConstants } from '../constants/common.constant';
import { ValidatePhoneModel } from '../models/validate-phone.model';
import { AnnounceNewMembership, AnnounceEditComment } from '../models/membershipTransaction.model';
import { AppSettingModel } from '../models/app-setting.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public isValidRedeemSubcription = new Subject<any>();
  public redeemBalanceSubscription = new Subject<any>();
  public addBalanceSubscription = new Subject<any>();
  public refreshCustomerInfoSubscription = new Subject<any>();

  public topUpWalletBalanceSubcription = new Subject<any>();
  public spendWalletBalanceSubcription = new Subject<any>();
  public membershipEditSubcription = new Subject<AnnounceNewMembership>();
  public editCommentSubcription = new Subject<AnnounceEditComment>();

  constructor(private router: Router,
    private oAuthStorage: OAuthStorage,
    private oAuthService: OAuthService) {
  }

  public routingToPage(page: string) {
    this.router.navigateByUrl('/' + page);
  }

  public routingToPageWithParam(page: string, param: any) {
    this.router.navigate([`/${page}`], { queryParams: param });
  }

  public setLocalStorage(localStorageName: string, data: any) {
    this.oAuthStorage.setItem(localStorageName, JSON.stringify(data));
  }

  public getLocalStorage(localStorageName: string) {
    return this.oAuthStorage.getItem(localStorageName) ?
      JSON.parse(this.oAuthStorage.getItem(localStorageName)) : null;
  }

  public checkExpToken() {
    return this.oAuthService.hasValidAccessToken();
  }

  public getValidatePhone(phoneValidates: any) {
    const listValidate: Array<ValidatePhoneModel> = [];
    phoneValidates.appSettingModels
      .forEach(item => {
        if (item.appSettingTypeId === 4) {
          const validate: ValidatePhoneModel = {
            countryCode: item.name,
            name: item.value.substr(0, 2),
            regex: item.value.substr(3)
          };
          listValidate.push(validate);
        }
      });
    return listValidate;
  }

  public removeLocalStorage(item: string) {
    this.oAuthStorage.removeItem(item);
  }

  public getAppsettingByName(appSettingName: string) {
    const appSettings = this.getLocalStorage(CommonConstants.appSettings);

    if (!appSettings) {
      return null;
    }

    const appsetting = appSettings.appSettingModels.find(x => x.name === appSettingName);
    return appsetting;
  }

  public parseJsonValueAppsetting(appsetting: AppSettingModel | null | undefined) {
    if (appsetting.value !== '') {
      return JSON.parse(appsetting.value);
    }
    return null;
  }

  public getJsonValueAppsetting(appsettingName: string) {
    const appsetting = this.getAppsettingByName(appsettingName);
    return this.parseJsonValueAppsetting(appsetting);
  }
}
