﻿namespace Harvey.LabelPrinter.PrintProductLabel
{
    partial class FrmPrintProductLabel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dataGridViewSearchProducts = new System.Windows.Forms.DataGridView();
            this.dataGridViewSelectedProducts = new System.Windows.Forms.DataGridView();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.btnMinusProduct = new System.Windows.Forms.Button();
            this.lblSearching = new System.Windows.Forms.Label();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pageSetupDialog2 = new System.Windows.Forms.PageSetupDialog();
            this.btnPrint2 = new System.Windows.Forms.Button();
            this.txtBlockHeihgt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGap = new System.Windows.Forms.TextBox();
            this.txtGapOffset = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textfit = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNameFont = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBlockProperties = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTextProperties = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnPrevious = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSearchProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSelectedProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(530, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search";
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(595, 37);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(217, 26);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(818, 37);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 27);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dataGridViewSearchProducts
            // 
            this.dataGridViewSearchProducts.AllowUserToAddRows = false;
            this.dataGridViewSearchProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSearchProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSearchProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSearchProducts.Location = new System.Drawing.Point(31, 97);
            this.dataGridViewSearchProducts.MultiSelect = false;
            this.dataGridViewSearchProducts.Name = "dataGridViewSearchProducts";
            this.dataGridViewSearchProducts.ReadOnly = true;
            this.dataGridViewSearchProducts.RowHeadersVisible = false;
            this.dataGridViewSearchProducts.Size = new System.Drawing.Size(620, 430);
            this.dataGridViewSearchProducts.TabIndex = 3;
            // 
            // dataGridViewSelectedProducts
            // 
            this.dataGridViewSelectedProducts.AllowUserToAddRows = false;
            this.dataGridViewSelectedProducts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSelectedProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSelectedProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSelectedProducts.Location = new System.Drawing.Point(734, 97);
            this.dataGridViewSelectedProducts.MultiSelect = false;
            this.dataGridViewSelectedProducts.Name = "dataGridViewSelectedProducts";
            this.dataGridViewSelectedProducts.RowHeadersVisible = false;
            this.dataGridViewSelectedProducts.Size = new System.Drawing.Size(665, 430);
            this.dataGridViewSelectedProducts.TabIndex = 4;
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnAddProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProduct.Location = new System.Drawing.Point(656, 168);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(74, 68);
            this.btnAddProduct.TabIndex = 5;
            this.btnAddProduct.Text = "button1";
            this.btnAddProduct.UseVisualStyleBackColor = true;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // btnMinusProduct
            // 
            this.btnMinusProduct.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnMinusProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinusProduct.Location = new System.Drawing.Point(656, 253);
            this.btnMinusProduct.Name = "btnMinusProduct";
            this.btnMinusProduct.Size = new System.Drawing.Size(74, 68);
            this.btnMinusProduct.TabIndex = 6;
            this.btnMinusProduct.Text = "button2";
            this.btnMinusProduct.UseVisualStyleBackColor = true;
            this.btnMinusProduct.Click += new System.EventHandler(this.btnMinusProduct_Click);
            // 
            // lblSearching
            // 
            this.lblSearching.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSearching.AutoSize = true;
            this.lblSearching.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearching.ForeColor = System.Drawing.Color.Red;
            this.lblSearching.Location = new System.Drawing.Point(920, 41);
            this.lblSearching.Name = "lblSearching";
            this.lblSearching.Size = new System.Drawing.Size(81, 20);
            this.lblSearching.TabIndex = 8;
            this.lblSearching.Text = "Searching";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(55, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 60);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // btnPrint2
            // 
            this.btnPrint2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint2.Location = new System.Drawing.Point(1221, 41);
            this.btnPrint2.Name = "btnPrint2";
            this.btnPrint2.Size = new System.Drawing.Size(107, 41);
            this.btnPrint2.TabIndex = 10;
            this.btnPrint2.Text = "Print";
            this.btnPrint2.UseVisualStyleBackColor = true;
            this.btnPrint2.Click += new System.EventHandler(this.btnPrint2_Click);
            // 
            // txtBlockHeihgt
            // 
            this.txtBlockHeihgt.Location = new System.Drawing.Point(286, 62);
            this.txtBlockHeihgt.Name = "txtBlockHeihgt";
            this.txtBlockHeihgt.Size = new System.Drawing.Size(100, 20);
            this.txtBlockHeihgt.TabIndex = 11;
            this.txtBlockHeihgt.Text = "60";
            this.txtBlockHeihgt.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(173, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "block height";
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(173, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "GAP";
            this.label3.Visible = false;
            // 
            // txtGap
            // 
            this.txtGap.Location = new System.Drawing.Point(286, 37);
            this.txtGap.Name = "txtGap";
            this.txtGap.Size = new System.Drawing.Size(100, 20);
            this.txtGap.TabIndex = 13;
            this.txtGap.Text = "2.5";
            this.txtGap.Visible = false;
            // 
            // txtGapOffset
            // 
            this.txtGapOffset.Location = new System.Drawing.Point(405, 37);
            this.txtGapOffset.Name = "txtGapOffset";
            this.txtGapOffset.Size = new System.Drawing.Size(100, 20);
            this.txtGapOffset.TabIndex = 15;
            this.txtGapOffset.Text = "2";
            this.txtGapOffset.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(173, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "fit";
            this.label4.Visible = false;
            // 
            // textfit
            // 
            this.textfit.Location = new System.Drawing.Point(286, 12);
            this.textfit.Name = "textfit";
            this.textfit.Size = new System.Drawing.Size(100, 20);
            this.textfit.TabIndex = 16;
            this.textfit.Text = "0";
            this.textfit.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(420, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "name font";
            this.label5.Visible = false;
            // 
            // txtNameFont
            // 
            this.txtNameFont.Location = new System.Drawing.Point(533, 9);
            this.txtNameFont.Name = "txtNameFont";
            this.txtNameFont.Size = new System.Drawing.Size(100, 20);
            this.txtNameFont.TabIndex = 18;
            this.txtNameFont.Text = "0";
            this.txtNameFont.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(649, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 20);
            this.label6.TabIndex = 21;
            this.label6.Text = "block properties";
            this.label6.Visible = false;
            // 
            // txtBlockProperties
            // 
            this.txtBlockProperties.Location = new System.Drawing.Point(786, 12);
            this.txtBlockProperties.Name = "txtBlockProperties";
            this.txtBlockProperties.Size = new System.Drawing.Size(100, 20);
            this.txtBlockProperties.TabIndex = 20;
            this.txtBlockProperties.Text = "0,4,4, 0, 2";
            this.txtBlockProperties.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(904, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 20);
            this.label7.TabIndex = 23;
            this.label7.Text = "text properties";
            this.label7.Visible = false;
            // 
            // txtTextProperties
            // 
            this.txtTextProperties.Location = new System.Drawing.Point(1041, 10);
            this.txtTextProperties.Name = "txtTextProperties";
            this.txtTextProperties.Size = new System.Drawing.Size(100, 20);
            this.txtTextProperties.TabIndex = 22;
            this.txtTextProperties.Text = "0,2, 2, 2";
            this.txtTextProperties.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevious.Location = new System.Drawing.Point(436, 530);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 24;
            this.btnPrevious.Text = "<";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // lblPage
            // 
            this.lblPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPage.AutoSize = true;
            this.lblPage.Location = new System.Drawing.Point(516, 535);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(52, 13);
            this.lblPage.TabIndex = 25;
            this.lblPage.Text = "Page 0/0";
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(575, 530);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 26;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // FrmPrintProductLabel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1411, 565);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtTextProperties);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtBlockProperties);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNameFont);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textfit);
            this.Controls.Add(this.txtGapOffset);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtGap);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBlockHeihgt);
            this.Controls.Add(this.btnPrint2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblSearching);
            this.Controls.Add(this.btnMinusProduct);
            this.Controls.Add(this.btnAddProduct);
            this.Controls.Add(this.dataGridViewSelectedProducts);
            this.Controls.Add(this.dataGridViewSearchProducts);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(1120, 604);
            this.Name = "FrmPrintProductLabel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintProductLabel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPrintProductLabel_Load);
            this.Enter += new System.EventHandler(this.FrmPrintProductLabel_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSearchProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSelectedProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dataGridViewSearchProducts;
        private System.Windows.Forms.DataGridView dataGridViewSelectedProducts;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.Button btnMinusProduct;
        private System.Windows.Forms.Label lblSearching;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog2;
        private System.Windows.Forms.Button btnPrint2;
        private System.Windows.Forms.TextBox txtBlockHeihgt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGap;
        private System.Windows.Forms.TextBox txtGapOffset;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textfit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNameFont;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBlockProperties;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTextProperties;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.Button btnNext;
    }
}