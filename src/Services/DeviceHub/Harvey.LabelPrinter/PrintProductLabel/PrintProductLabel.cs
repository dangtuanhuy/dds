﻿using Harvey.LabelPrinter.Domain;
using Harvey.LabelPrinter.Extensions;
using Harvey.LabelPrinter.Implementations.TSC;
using Harvey.LabelPrinter.Log;
using Harvey.LabelPrinter.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Windows.Forms;

namespace Harvey.LabelPrinter.PrintProductLabel
{
    public partial class FrmPrintProductLabel : Form
    {
        string DeviceName = "";
        string IdsUrl = "";
        string ImsUrlApi = "";
        string Email = "";
        string Password = "";
        private DataSet searchDs = new DataSet();
        private DataTable searchDt = new DataTable();

        private DataSet assignedDs = new DataSet();
        private DataTable assignedDt = new DataTable();

        string PreviousSearchText = "";

        int PageIndex = 0;
        int PageSize = 10;
        int TotalItems = 0;
        int TotalPages = 0;

        List<string> searchColumnNames = new List<string>
        {
            "No.",
            "Name",
            "BarCode",
            "SKUCode",
            "List Price",
            "Member Price",
            "Created Date"

        };

        List<string> assignedColumnNames = new List<string>
        {
            "No.",
            "Name",
            "BarCode",
            "SKUCode",
            "List Price",
            "Member Price",
            "Quantity"
        };

        public FrmPrintProductLabel()
        {
            InitializeComponent();
            timer1.Interval = 2000;
            timer1.Start();
        }

        public FrmPrintProductLabel(string deviceName, string idsUrl, string imsApiUrl, string email, string password)
        {
            InitializeComponent();
            timer1.Interval = 2000;
            timer1.Start();
            DeviceName = deviceName;
            IdsUrl = idsUrl;
            ImsUrlApi = imsApiUrl;
            Email = email;
            Password = password;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            OnClickSearchProducts(fromClickSearch: true);
        }

        private void OnClickSearchProducts(bool fromClickSearch = false)
        {
            if (PreviousSearchText == txtSearch.Text && !fromClickSearch)
            {
                return;
            }
            if (txtSearch.Text.Length < 3)
            {
                return;
            }

            if (PreviousSearchText != txtSearch.Text)
            {
                ResetPaging();
                PreviousSearchText = txtSearch.Text;
            }

            lblSearching.Text = "Searching...";

            try
            {
                var data = SearchProducts(txtSearch.Text);
                if (!data.Item1)
                {
                    return;
                }

                var productForPrintLabelResponse = data.Item2;
                if (productForPrintLabelResponse == null)
                {
                    lblSearching.Text = "";
                    MessageBox.Show("Not found!");
                    return;
                }

                PageIndex = productForPrintLabelResponse.PageIndex;
                TotalItems = productForPrintLabelResponse.TotalItems;
                FormatPaging();

                var dataFromBarCodeOnly = productForPrintLabelResponse.BarCodeDataOnly;
                var products = productForPrintLabelResponse.Products;
                if (products.Count == 1 && dataFromBarCodeOnly)
                {
                    var theFirstProduct = products[0];

                    var existedRows = assignedDt.Select($"BarCode = '{theFirstProduct.BarCode}'").ToList();
                    if (existedRows.Count > 0)
                    {
                        var exsitedRow = existedRows[0];
                        exsitedRow["Quantity"] = int.Parse(exsitedRow["Quantity"].ToString()) + 1;
                    }
                    else
                    {
                        var dataRow = assignedDt.NewRow();
                        dataRow["No."] = assignedDt.Rows.Count + 1;
                        dataRow["Name"] = theFirstProduct.VariantName;
                        dataRow["BarCode"] = theFirstProduct.BarCode;
                        dataRow["SKUCode"] = theFirstProduct.SKUCode;
                        dataRow["List Price"] = string.Format("{0:0.00}", theFirstProduct.ListPrice);
                        dataRow["Member Price"] = string.Format("{0:0.00}", theFirstProduct.MemberPrice);
                        dataRow["Quantity"] = 1;

                        assignedDt.Rows.Add(dataRow);
                    }

                    txtSearch.Text = "";
                    PreviousSearchText = "";
                    txtSearch.Focus();
                }

                if (products == null || !products.Any())
                {
                    lblSearching.Text = "";

                    if (!dataFromBarCodeOnly && fromClickSearch)
                    {
                        MessageBox.Show("Not found!");
                    }

                    if (dataFromBarCodeOnly)
                    {
                        txtSearch.Text = "";
                        txtSearch.Focus();
                    }

                    return;
                }

                searchDs.Reset();
                searchDt = new DataTable();

                foreach (var item in searchColumnNames)
                {
                    searchDt.Columns.Add(item);
                }

                for (int i = 0; i < products.Count; i++)
                {
                    var product = products[i];
                    var dataRow = searchDt.NewRow();
                    dataRow["No."] = i + 1;
                    dataRow["Name"] = product.VariantName;
                    dataRow["BarCode"] = product.BarCode;
                    dataRow["SKUCode"] = product.SKUCode;
                    dataRow["List Price"] = string.Format("{0:0.00}", product.ListPrice);
                    dataRow["Member Price"] = string.Format("{0:0.00}", product.MemberPrice);
                    dataRow["Created Date"] = product.CreatedDateString;

                    searchDt.Rows.Add(dataRow);
                }

                dataGridViewSearchProducts.DataSource = searchDt;
                foreach (var item in searchColumnNames)
                {
                    dataGridViewSearchProducts.Columns[item].ReadOnly = true;
                }

                dataGridViewSearchProducts.Columns["No."].Width = 25;
                dataGridViewSearchProducts.Columns["BarCode"].Width = 100;
                dataGridViewSearchProducts.Columns["SKUCode"].Width = 150;
                dataGridViewSearchProducts.Columns["List Price"].Width = 55;
                dataGridViewSearchProducts.Columns["Member Price"].Width = 55;
                dataGridViewSearchProducts.Columns["Created Date"].Width = 120;

                lblSearching.Text = "";
            }
            catch (Exception)
            {
                lblSearching.Text = "";
            }

        }

        private Tuple<bool, ProductForPrintLabelResponse> SearchProducts(string searchText)
        {
            using (var client = new HttpClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var content = new FormUrlEncodedContent(new[] {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username", Email),
                        new KeyValuePair<string, string>("password", Password),
                        new KeyValuePair<string, string>("client_id", "Harvey-administrator-page"),
                        new KeyValuePair<string, string>("client_secret", "secret")
                });

                var getAccessTokenUrl = $"{IdsUrl}/connect/token";
                var result = client.PostAsync(getAccessTokenUrl, content).Result;
                var contentString = result.Content.ReadAsStringAsync().Result;
                var r = JToken.Parse(contentString);
                var token = r["access_token"].Value<string>();

                if (IdsExtention.CheckRole(token))
                {
                    HttpClient _bicDataClient = new HttpClient();
                    _bicDataClient.DefaultRequestHeaders.Add("Authorization", ("Bearer " + token));
                    var searchUrl = $"{ImsUrlApi}/api/products/print-label-variants?querySearch={txtSearch.Text}&pageIndex={PageIndex}&pageSize={PageSize}";
                    var bicData = _bicDataClient.GetAsync(searchUrl).Result;
                    var containerDataString = bicData.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<ProductForPrintLabelResponse>(containerDataString);
                    return new Tuple<bool, ProductForPrintLabelResponse>(true, data);
                }

                MessageBox.Show("Please login again!");
                this.Close();
                return new Tuple<bool, ProductForPrintLabelResponse>(false, null);
            }
        }

        private List<Product> SearchProductsByConnectrion(string searchText, ref bool dataFromBarCodeOnly)
        {
            List<Product> products = new List<Product>();
            string constring = "";
            NpgsqlConnection conn = new NpgsqlConnection(constring);
            try
            {
                string barCodeQuery = " SELECT t1.\"VariantId\", t1.\"CreatedDate\", t1.\"Code\" ";
                barCodeQuery += " FROM \"BarCode\" AS t1";
                barCodeQuery += " INNER JOIN (SELECT \"VariantId\", MAX(\"CreatedDate\") AS \"MaxDate\" FROM \"BarCode\" WHERE \"Code\" IS NOT NULL GROUP BY \"VariantId\") AS t2";
                barCodeQuery += " ON (t1.\"VariantId\" = t2.\"VariantId\" AND t1.\"CreatedDate\" = t2.\"MaxDate\")";

                string variantInCludePriceQuery = "SELECT v.\"Id\" AS \"VariantId\", v.\"SKUCode\", v.\"ProductId\", p.\"ListPrice\", p.\"MemberPrice\", p.\"StaffPrice\" ";
                variantInCludePriceQuery += " FROM \"Variants\" v";
                variantInCludePriceQuery += " JOIN \"Price\" p ON v.\"PriceId\" = p.\"Id\"";

                conn.Open();
                var productLabelSqls = SearchVariantByBarCode(conn, searchText, variantInCludePriceQuery);
                conn.Close();
                products = GetProductsFromProductLabelSqls(productLabelSqls);
                if (products.Count == 1)
                {
                    dataFromBarCodeOnly = true;
                    var theFirstProduct = products[0];

                    var existedRows = assignedDt.Select($"BarCode = '{theFirstProduct.BarCode}'").ToList();
                    if (existedRows.Count > 0)
                    {
                        var exsitedRow = existedRows[0];
                        exsitedRow["Quantity"] = int.Parse(exsitedRow["Quantity"].ToString()) + 1;
                    }
                    else
                    {
                        var dataRow = assignedDt.NewRow();
                        dataRow["No."] = assignedDt.Rows.Count + 1;
                        dataRow["Name"] = theFirstProduct.VariantName;
                        dataRow["BarCode"] = theFirstProduct.BarCode;
                        dataRow["SKUCode"] = theFirstProduct.SKUCode;
                        dataRow["List Price"] = string.Format("{0:0.00}", theFirstProduct.ListPrice);
                        dataRow["Member Price"] = string.Format("{0:0.00}", theFirstProduct.MemberPrice);
                        dataRow["Quantity"] = 1;

                        assignedDt.Rows.Add(dataRow);
                    }

                    return new List<Product>();
                }

                var queriedVariantIds = products.Select(x => x.VariantId).ToList();

                conn.Open();
                productLabelSqls = SearchVariantBySkuCode(conn, searchText, barCodeQuery);
                productLabelSqls = productLabelSqls.Where(x => !queriedVariantIds.Contains(x.VariantId)).ToList();
                conn.Close();
                products.AddRange(GetProductsFromProductLabelSqls(productLabelSqls));
                queriedVariantIds = products.Select(x => x.VariantId).ToList();

                conn.Open();
                productLabelSqls = SearchVariantByProductName(conn, searchText, barCodeQuery, variantInCludePriceQuery);
                productLabelSqls = productLabelSqls.Where(x => !queriedVariantIds.Contains(x.VariantId)).ToList();
                products.AddRange(GetProductsFromProductLabelSqls(productLabelSqls));
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot connect to Database. Please enter correct details");
            }

            return products;
        }

        private List<Product> GetProductsFromProductLabelSqls(List<ProductLabelSql> data)
        {
            return data.GroupBy(x => x.VariantId)
                       .Select(x => new Product
                       {
                           VariantId = x.First().VariantId,
                           BarCode = x.First().BarCode,
                           CreatedDateString = x.First().CreatedDateString,
                           ListPrice = x.First().ListPrice,
                           MemberPrice = x.First().MemberPrice,
                           StaffPrice = x.First().StaffPrice,
                           SKUCode = x.First().SKUCode,
                           VariantName = x.First().Name + "-" + string.Join("-", x.Select(y => y.PredefinedListValue)),
                           Quantity = 1
                       }).ToList();
        }

        private void FrmPrintProductLabel_Load(object sender, EventArgs e)
        {
            lblSearching.Text = "";
            btnAddProduct.Text = char.ConvertFromUtf32(0x2192);
            btnMinusProduct.Text = char.ConvertFromUtf32(0x2190);

            foreach (var item in assignedColumnNames)
            {
                assignedDt.Columns.Add(item);
            }
            dataGridViewSelectedProducts.DataSource = assignedDt;
            foreach (var item in assignedColumnNames)
            {
                dataGridViewSelectedProducts.Columns[item].ReadOnly = true;
            }

            dataGridViewSelectedProducts.Columns["No."].Width = 25;
            dataGridViewSelectedProducts.Columns["BarCode"].Width = 100;
            dataGridViewSelectedProducts.Columns["SKUCode"].Width = 150;
            dataGridViewSelectedProducts.Columns["List Price"].Width = 55;
            dataGridViewSelectedProducts.Columns["Member Price"].Width = 55;
            dataGridViewSelectedProducts.Columns["Quantity"].Width = 60;
            dataGridViewSelectedProducts.Columns["Quantity"].ReadOnly = false;

            //txtSearch.KeyUp += OnSearchProductKeyUp;
        }

        private void OnSearchProductKeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    OnClickSearchProducts();
            //}
        }

        private List<ProductLabelSql> SearchVariantByBarCode(NpgsqlConnection conn, string searchText, string variantInCludePriceQuery)
        {
            var searchBarCodeQuery = " SELECT t1.\"VariantId\", t1.\"CreatedDate\", t1.\"Code\" ";
            searchBarCodeQuery += " FROM (SELECT \"VariantId\", \"CreatedDate\", \"Code\" FROM \"BarCode\" where \"Code\" IS NOT NULL AND upper(\"Code\") = upper('" + searchText + "')) AS t1";
            searchBarCodeQuery += " INNER JOIN (SELECT \"VariantId\", MAX(\"CreatedDate\") AS \"MaxDate\" FROM \"BarCode\" GROUP BY \"VariantId\") AS t2";
            searchBarCodeQuery += " ON (t1.\"VariantId\" = t2.\"VariantId\" AND t1.\"CreatedDate\" = t2.\"MaxDate\")";

            string injectVariantQuery = " SELECT a.\"VariantId\", a.\"Code\", a.\"CreatedDate\", b.\"ProductId\" ";
            injectVariantQuery += " ,b.\"ListPrice\", b.\"MemberPrice\", b.\"StaffPrice\", b.\"SKUCode\" ";
            injectVariantQuery += " FROM (" + searchBarCodeQuery + ") AS a";
            injectVariantQuery += " JOIN (" + variantInCludePriceQuery + ") AS b";
            injectVariantQuery += " ON a.\"VariantId\" = b.\"VariantId\" ";

            string injectProductQuery = "SELECT distinct on (c.\"VariantId\") c.\"VariantId\", c.\"Code\", c.\"CreatedDate\", c.\"ProductId\", d.\"Name\" ";
            injectProductQuery += " , c.\"ListPrice\", c.\"MemberPrice\", c.\"StaffPrice\", c.\"SKUCode\" ";
            injectProductQuery += " FROM (" + injectVariantQuery + ") AS c";
            injectProductQuery += " JOIN \"Products\" AS d";
            injectProductQuery += " ON c.\"ProductId\" = d.\"Id\" ";

            string injectFieldValues = "SELECT data.*, fv.\"PredefinedListValue\" FROM (" + injectProductQuery + ") AS data";
            injectFieldValues += " JOIN \"FieldValues\" AS fv";
            injectFieldValues += " ON data.\"VariantId\" = fv.\"EntityId\"";

            NpgsqlCommand command = new NpgsqlCommand(injectFieldValues, conn);
            NpgsqlDataReader data = command.ExecuteReader();

            var rawResults = new List<ProductLabelSql>();
            rawResults = ReadData(data);
            return rawResults;
        }

        private List<ProductLabelSql> SearchVariantBySkuCode(NpgsqlConnection conn, string searchText, string barCodeSql)
        {
            string searchSKuCodeQuery = "SELECT \"Id\" AS \"VariantId\", \"ProductId\",  \"SKUCode\", \"PriceId\" FROM \"Variants\" ";
            searchSKuCodeQuery += " WHERE upper(\"SKUCode\") = upper('" + searchText + "')";

            string injectPriceQuery = "SELECT k.\"VariantId\", k.\"SKUCode\", k.\"ProductId\", k.\"PriceId\", p.\"ListPrice\", p.\"MemberPrice\", p.\"StaffPrice\" ";
            injectPriceQuery += " FROM (" + searchSKuCodeQuery + ") AS k";
            injectPriceQuery += " JOIN \"Price\" p ON k.\"PriceId\" = p.\"Id\"";

            string injectProductsQuery = " SELECT a.\"VariantId\", a.\"ProductId\", b.\"Name\" ";
            injectProductsQuery += " , a.\"ListPrice\", a.\"MemberPrice\", a.\"StaffPrice\", a.\"SKUCode\" ";
            injectProductsQuery += " FROM (" + injectPriceQuery + ") AS a";
            injectProductsQuery += " JOIN \"Products\" AS b";
            injectProductsQuery += " ON a.\"ProductId\" = b.\"Id\" ";

            string injectBarCodeQuery = "SELECT distinct on (c.\"VariantId\") c.\"VariantId\", d.\"Code\", d.\"CreatedDate\", c.\"ProductId\", c.\"Name\" ";
            injectBarCodeQuery += " , c.\"ListPrice\", c.\"MemberPrice\", c.\"StaffPrice\", c.\"SKUCode\" ";
            injectBarCodeQuery += " FROM (" + injectProductsQuery + ") AS c";
            injectBarCodeQuery += " JOIN (" + barCodeSql + ") AS d";
            injectBarCodeQuery += " ON c.\"VariantId\" = d.\"VariantId\" ";

            string injectFieldValues = "SELECT data.*, fv.\"PredefinedListValue\" FROM (" + injectBarCodeQuery + ") AS data";
            injectFieldValues += " JOIN \"FieldValues\" AS fv";
            injectFieldValues += " ON data.\"VariantId\" = fv.\"EntityId\"";

            NpgsqlCommand command = new NpgsqlCommand(injectFieldValues, conn);
            NpgsqlDataReader data = command.ExecuteReader();

            var rawResults = new List<ProductLabelSql>();
            rawResults = ReadData(data);
            return rawResults;
        }

        private List<ProductLabelSql> SearchVariantByProductName(NpgsqlConnection conn, string searchText, string barCodeQuery, string variantInCludePriceQuery)
        {
            string searchProductNameQuery = "SELECT \"Id\" AS \"ProductId\", \"Name\" FROM \"Products\" WHERE upper(\"Name\") LIKE upper('%" + searchText + "%')";

            string injectVariantsQuery = " SELECT a.\"ProductId\", a.\"Name\", b.\"VariantId\" ";
            injectVariantsQuery += " , b.\"ListPrice\", b.\"MemberPrice\", b.\"StaffPrice\", b.\"SKUCode\" ";
            injectVariantsQuery += " FROM (" + searchProductNameQuery + ") AS a";
            injectVariantsQuery += " JOIN (" + variantInCludePriceQuery + ") AS b";
            injectVariantsQuery += " ON a.\"ProductId\" = b.\"ProductId\" ";

            string injectBarCodeQuery = "SELECT distinct on (c.\"VariantId\") c.\"VariantId\", d.\"Code\", d.\"CreatedDate\", c.\"ProductId\", c.\"Name\" ";
            injectBarCodeQuery += " , c.\"ListPrice\", c.\"MemberPrice\", c.\"StaffPrice\", c.\"SKUCode\" ";
            injectBarCodeQuery += " FROM (" + injectVariantsQuery + ") AS c";
            injectBarCodeQuery += " JOIN (" + barCodeQuery + ") AS d";
            injectBarCodeQuery += " ON c.\"VariantId\" = d.\"VariantId\" ";

            string injectFieldValues = "SELECT data.*, fv.\"PredefinedListValue\" FROM (" + injectBarCodeQuery + ") AS data";
            injectFieldValues += " JOIN \"FieldValues\" AS fv";
            injectFieldValues += " ON data.\"VariantId\" = fv.\"EntityId\"";

            NpgsqlCommand command = new NpgsqlCommand(injectFieldValues, conn);
            NpgsqlDataReader data = command.ExecuteReader();

            var rawResults = new List<ProductLabelSql>();
            rawResults = ReadData(data);
            return rawResults;
        }

        private List<ProductLabelSql> ReadData(NpgsqlDataReader data)
        {
            var result = new List<ProductLabelSql>();
            while (data.Read())
            {
                var product = new ProductLabelSql();
                product.VariantId = new Guid(data["VariantId"].ToString());
                product.Name = (string)data["Name"] ?? string.Empty;
                product.BarCode = (string)data["Code"] ?? string.Empty;
                product.SKUCode = (string)data["SKUCode"] ?? string.Empty;
                product.CreatedDateString = ((DateTime)data["CreatedDate"]).ToString("yyyy-MM-dd h:mm tt");

                product.ListPrice = double.Parse(data["ListPrice"].ToString());
                product.MemberPrice = double.Parse(data["MemberPrice"].ToString());
                product.StaffPrice = double.Parse(data["StaffPrice"].ToString());

                product.PredefinedListValue = data["PredefinedListValue"].ToString();

                result.Add(product);
            }

            return result;
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            var currentCell = dataGridViewSearchProducts.CurrentCell;
            if (currentCell == null)
            {
                return;
            }

            var rowIndex = currentCell.RowIndex;
            DataGridViewRow row = dataGridViewSearchProducts.Rows[rowIndex];

            var name = row.Cells["Name"].Value.ToString();
            var barCode = row.Cells["BarCode"].Value.ToString();
            var skuCode = row.Cells["SKUCode"].Value.ToString();
            var listPrice = double.Parse(row.Cells["List Price"].Value.ToString());
            var memberPrice = double.Parse(row.Cells["Member Price"].Value.ToString());

            object[] findTheseVals = new object[2];

            findTheseVals[0] = name;
            findTheseVals[1] = barCode;

            var existedRows = assignedDt.Select($"BarCode = '{barCode}'").ToList();
            if (existedRows.Count > 0)
            {
                var exsitedRow = existedRows[0];
                exsitedRow["Quantity"] = int.Parse(exsitedRow["Quantity"].ToString()) + 1;
            }
            else
            {
                var dataRow = assignedDt.NewRow();
                dataRow["No."] = assignedDt.Rows.Count + 1;
                dataRow["Name"] = name;
                dataRow["BarCode"] = barCode;
                dataRow["SKUCode"] = skuCode;
                dataRow["List Price"] = string.Format("{0:0.00}", listPrice);
                dataRow["Member Price"] = string.Format("{0:0.00}", memberPrice);
                dataRow["Quantity"] = 1;

                assignedDt.Rows.Add(dataRow);
            }
        }

        private void btnMinusProduct_Click(object sender, EventArgs e)
        {
            var currentCell = dataGridViewSelectedProducts.CurrentCell;
            if (currentCell == null)
            {
                return;
            }

            var rowIndex = currentCell.RowIndex;
            assignedDt.Rows.RemoveAt(rowIndex);
            for (int i = 0; i < assignedDt.Rows.Count; i++)
            {
                assignedDt.Rows[i]["No."] = i + 1;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            var tSCPrintProductLabelPrinter = new TSCPrintProductLabelPrinter();
            tSCPrintProductLabelPrinter.PrintTest(DeviceName);
        }

        private void FrmPrintProductLabel_Enter(object sender, EventArgs e)
        {
            if (assignedDt.Rows.Count <= 0)
            {
                MessageBox.Show("Not variants to print!");
                return;
            }

            var data = new List<Product>();
            foreach (DataRow item in assignedDt.Rows)
            {
                data.Add(new Product
                {
                    VariantName = item["Name"].ToString(),
                    BarCode = item["BarCode"].ToString(),
                    SKUCode = item["SKUCode"].ToString(),
                    ListPrice = double.Parse(item["List Price"].ToString()),
                    MemberPrice = double.Parse(item["Member Price"].ToString()),
                    Quantity = int.Parse(item["Quantity"].ToString())
                });
            }

            var productLabel = new ProductLabel();

            var request = new DeviceRequest()
            {
                Command = Enums.DeviceCommand.PrintProductLabel,
                DeviceHubName = DeviceName,
                Data = JsonConvert.SerializeObject(productLabel)
            };

            var deviceExecutor = new DeviceExecutor();
            var tSCPrintProductLabelPrinter = new TSCPrintProductLabelPrinter();
            List<ProductLabelPrinterModel> productLabelPrinterModels = new List<ProductLabelPrinterModel>();
            foreach (var item in data)
            {
                ProductLabelPrinterModel productLabelPrinter = new ProductLabelPrinterModel
                {
                    BarCode = item.BarCode,
                    ListPrice = item.ListPrice,
                    MemberPrice = item.MemberPrice,
                    Variant = item.VariantName,
                    SKUCode = item.SKUCode,
                    quantity = item.Quantity
                };
                productLabelPrinterModels.Add(productLabelPrinter);
                //tSCPrintProductLabelPrinter.PrintByCommand(DeviceName, productLabelPrinter);
            }
            if (tSCPrintProductLabelPrinter.PrintByCommands(DeviceName, productLabelPrinterModels, txtBlockHeihgt.Text,
                txtGap.Text, txtGapOffset.Text, textfit.Text))
            {
                MessageBox.Show("Success");
            }
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            //OnClickSearchProducts();
        }

        private void btnPrint2_Click(object sender, EventArgs e)
        {
            if (assignedDt.Rows.Count <= 0)
            {
                MessageBox.Show("Not variants to print!");
                return;
            }

            var data = new List<Product>();
            var parseDataSuccess = true;
            var messageError = "";
            for (int i = 0; i < assignedDt.Rows.Count; i++)
            {
                DataRow item = assignedDt.Rows[i];
                var product = new Product
                {
                    VariantName = item["Name"].ToString(),
                    BarCode = item["BarCode"].ToString(),
                    SKUCode = item["SKUCode"].ToString(),
                    ListPrice = double.Parse(item["List Price"].ToString()),
                    MemberPrice = double.Parse(item["Member Price"].ToString()),
                    Quantity = 0
                };
                var quantity = 0;
                if (int.TryParse(item["Quantity"].ToString(), out quantity))
                {
                    product.Quantity = quantity;
                }
                else
                {
                    parseDataSuccess = false;
                    if (string.IsNullOrEmpty(messageError))
                    {
                        messageError = $"{product.VariantName}'s quantity must be number!";
                    }
                    else
                    {
                        messageError += $"\n{product.VariantName}'s quantity must be number!";
                    }
                }

                data.Add(product);
            }
            if (!parseDataSuccess)
            {
                MessageBox.Show(messageError);
                return;
            }

            var productLabel = new ProductLabel();

            var request = new DeviceRequest()
            {
                Command = Enums.DeviceCommand.PrintProductLabel,
                DeviceHubName = DeviceName,
                Data = JsonConvert.SerializeObject(productLabel)
            };

            var deviceExecutor = new DeviceExecutor();
            var tSCPrintProductLabelPrinter = new TSCPrintProductLabelPrinter();
            List<ProductLabelPrinterModel> productLabelPrinterModels = new List<ProductLabelPrinterModel>();
            foreach (var item in data)
            {
                ProductLabelPrinterModel productLabelPrinter = new ProductLabelPrinterModel
                {
                    BarCode = item.BarCode,
                    ListPrice = item.ListPrice,
                    MemberPrice = item.MemberPrice,
                    Variant = item.VariantName,
                    SKUCode = item.SKUCode,
                    quantity = item.Quantity
                };
                productLabelPrinterModels.Add(productLabelPrinter);
                //tSCPrintProductLabelPrinter.PrintByCommand(DeviceName, productLabelPrinter);
            }
            if (tSCPrintProductLabelPrinter.PrintByCommands(DeviceName, productLabelPrinterModels, txtBlockHeihgt.Text,
                txtGap.Text, txtGapOffset.Text, textfit.Text, txtNameFont.Text, txtBlockProperties.Text, txtTextProperties.Text))
            {
                MessageBox.Show("Success");
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            OnClickSearchProducts();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            PageIndex++;
            if (PageIndex > TotalPages - 1)
            {
                PageIndex = TotalPages - 1;
                return;
            }

            OnClickSearchProducts(true);
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            PageIndex--;
            if (PageIndex < 0)
            {
                PageIndex = 0;
                return;
            }
            OnClickSearchProducts(true);
        }

        private void FormatPaging()
        {
            TotalPages = TotalItems / PageSize;
            if (TotalItems % PageSize != 0)
            {
                TotalPages++;
            }
            lblPage.Text = $"Page {PageIndex + 1}/{TotalPages}";
        }

        private void ResetPaging()
        {
            PageIndex = 0;
            TotalPages = 0;
            TotalItems = 0;
        }
    }
}
