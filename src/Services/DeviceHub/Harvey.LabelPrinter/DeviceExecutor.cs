﻿using Harvey.LabelPrinter.Factories;
using Harvey.LabelPrinter.Models;
using System.Threading.Tasks;

namespace Harvey.LabelPrinter
{
    public class DeviceExecutor
    {
        public async Task<bool> ExecuteAsync(DeviceRequest request)
        {
            var serializer = SerializerFactory.CreateSerializerFromType(request.Command);
            var device = DeviceFactory.CreateDeviceFromType(request.Command);
            var data = serializer.Serialize(request.Data);
            var result = await device.ExecuteAsync(request.DeviceHubName, data);
            return result;
        }
    }
}
