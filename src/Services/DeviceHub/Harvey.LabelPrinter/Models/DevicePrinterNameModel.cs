﻿namespace Harvey.LabelPrinter.Models
{
    public class DevicePrinterNameModel
    {
        public string PrinterLabel { get; set; }
        public string DeviceHubName { get; set; }
    }
}
