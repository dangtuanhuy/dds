﻿using Harvey.LabelPrinter.Enums;

namespace Harvey.LabelPrinter.Models
{
    public class DeviceRequest
    {
        public DeviceCommand Command { get; set; }
        public string DeviceHubName { get; set; }
        public string Data { get; set; }
    }
}
