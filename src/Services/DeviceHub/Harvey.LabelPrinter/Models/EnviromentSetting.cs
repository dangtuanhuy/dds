﻿namespace Harvey.LabelPrinter.Models
{
    public class EnviromentSetting
    {
        public ConnectionString ConnectionString { get; set; }

        public EnviromentSetting()
        {
            ConnectionString = new ConnectionString();
        }
    }

    public class ConnectionString
    {
        public string IdsApiUrl { get; set; }
        public string ImsApiUrl { get; set; }
    }

}
