﻿using System;

namespace Harvey.LabelPrinter.Models
{
    public class ProductLabel
    {
        public string Variant { get; set; }
        public string BarCode { get; set; }
        public double ListPrice { get; set; }
        public double MemberPrice { get; set; }
        public string SKUCode { get; set; }
    }

    public class ProductLabelSql
    {
        public Guid VariantId { get; set; }
        public string BarCode { get; set; }
        public string SKUCode { get; set; }
        public string Name { get; set; }
        public string CreatedDateString { get; set; }
        public double ListPrice { get; set; }
        public double MemberPrice { get; set; }
        public double StaffPrice { get; set; }
        public string PredefinedListValue { get; set; }
    }

    public class ProductLabelPrinterModel :ProductLabel
    {
        public int quantity { get; set; }
    }
}
