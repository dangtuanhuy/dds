﻿using Harvey.LabelPrinter.Extensions;
using Harvey.LabelPrinter.Forms;
using Harvey.LabelPrinter.Log;
using Harvey.LabelPrinter.Models;
using Harvey.LabelPrinter.PrintProductLabel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Harvey.LabelPrinter
{
    public partial class FrmMain : Form
    {
        LoginModel LoginModel;
        DevicePrinterNameModel DevicePrinterNameModel;
        EnviromentSetting Enviroment;

        public FrmMain()
        {
            InitializeComponent();
            DevicePrinterNameModel = new DevicePrinterNameModel();
            Enviroment = new EnviromentSetting();
            LoginModel = new LoginModel()
            {
                LoggedIn = false
            };
        }

        private void btnPrintLabel_Click(object sender, EventArgs e)
        {
            var loggedIn = IdsExtention.CheckLogin(Enviroment.ConnectionString.IdsApiUrl, LoginModel.Email, LoginModel.Password);
            if (!loggedIn)
            {
                FrmLogin frmLogin = new FrmLogin(LoginModel, Enviroment);
                frmLogin.FormClosed += new FormClosedEventHandler(FrmLogin_Closed);
                frmLogin.ShowDialog();
            }
            else
            {
                FrmLogin_Closed(null, null);
            }
        }

        private void FrmLogin_Closed(object sender, FormClosedEventArgs e)
        {
            if (LoginModel.LoggedIn)
            {
                var connectrionStringData = Enviroment.ConnectionString;
                FrmPrintProductLabel frmPrintProductLabel = new FrmPrintProductLabel(DevicePrinterNameModel.PrinterLabel,
                                                                                    connectrionStringData.IdsApiUrl,
                                                                                    connectrionStringData.ImsApiUrl,
                                                                                    LoginModel.Email,
                                                                                    LoginModel.Password);
                frmPrintProductLabel.ShowDialog();
            }
        }

        private void devicePrinterNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDevicePrinterName frmDevicePrinterName = new FrmDevicePrinterName(DevicePrinterNameModel);
            frmDevicePrinterName.ShowDialog();
        }

        private void appSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            DevicePrinterNameModel.PrinterLabel = LogFile.ReadFile(FileNames.AppSettingDevicePrintLabelName)
                                                .ToString()
                                                .Replace("\n", string.Empty)
                                                .Replace("\r", string.Empty);
            lbltest.Text = System.AppDomain.CurrentDomain.BaseDirectory;

            //var data = LogFile.ReadFileByteArray(FileNames.AppSettingConnectiontring);
            //var contents = new List<string>();
            //if (data.Length >= 4)
            //{
            //    var server = EncryptDecryptExtension.Decrypt(data[0]);
            //    var database = EncryptDecryptExtension.Decrypt(data[1]);
            //    var userId = EncryptDecryptExtension.Decrypt(data[2]);
            //    var password = EncryptDecryptExtension.Decrypt(data[3]);

            //    if ((!string.IsNullOrEmpty(server) && !string.IsNullOrEmpty(database)
            //        && !string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(password)))
            //    {
            //        contents = new List<string>
            //        {
            //            server, database, userId, password
            //        };
            //    }
            //}

            //if (contents.Count < 4)
            //{
            //    contents = new List<string>
            //    {
            //         "178.128.212.67",
            //        "harveypim",
            //        "postgres",
            //        "123456"
            //    };
            //}

            //Enviroment.ConnectionString.Server = contents[0];
            //Enviroment.ConnectionString.Database = contents[1];
            //Enviroment.ConnectionString.UserId = contents[2];
            //Enviroment.ConnectionString.Password = contents[3];

            LoadAppSettingUrl();
        }

        private void LoadAppSettingUrl()
        {
            var data = LogFile.ReadFileByteArray(FileNames.AppSettingApiUrl);
            var contents = new List<string>();
            if (data.Length >= 2)
            {
                var idsUrl = data[0].TrimEnd('/');
                var imsApiUrl = data[1].TrimEnd('/');

                if (!string.IsNullOrEmpty(idsUrl) && !string.IsNullOrEmpty(imsApiUrl))
                {
                    contents = new List<string>
                    {
                        idsUrl, imsApiUrl
                    };
                }
            }

            if (contents.Count < 2)
            {
                contents = new List<string>
                {
                    "http://178.128.212.67:61555",
                    "http://178.128.212.67:5001"
                };
            }

            Enviroment.ConnectionString.IdsApiUrl = contents[0];
            Enviroment.ConnectionString.ImsApiUrl = contents[1];
        }

        private void lbltest_Click(object sender, EventArgs e)
        {
            lbltest.Visible = !lbltest.Visible;
        }

        private void enviromentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSettingEnviroment frmSettingEnviroment = new FrmSettingEnviroment(Enviroment);
            frmSettingEnviroment.ShowDialog();
        }
    }
}
