﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Harvey.LabelPrinter.Extensions
{
    public static class IdsExtention
    {
        public static List<string> RolesCanAccess = new List<string>
        {
            "Administrator", "WarehouseStaff", "InventoryManager"
        };

        public static bool CheckLogin(string idsUrl, string email, string password)
        {
            using (var client = new HttpClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var content = new FormUrlEncodedContent(new[]
                {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", email),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("client_id", "Harvey-administrator-page"),
                new KeyValuePair<string, string>("client_secret", "secret")
            });

                var getAccessTokenUrl = $"{idsUrl}/connect/token";
                try
                {
                    var result = client.PostAsync(getAccessTokenUrl, content).Result;
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var contentString = result.Content.ReadAsStringAsync().Result;
                        var r = JToken.Parse(contentString);
                        var token = r["access_token"].Value<string>();

                        return CheckRole(token);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public static bool CheckRole(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var tokenS = handler.ReadToken(token) as JwtSecurityToken;
            var roles = tokenS.Claims.Where(claim => claim.Type == "role").Select(x => x.Value).ToList();

            var allowedRoles = RolesCanAccess.Where(x => roles.Contains(x)).ToList();
            if (allowedRoles != null && allowedRoles.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }

}
