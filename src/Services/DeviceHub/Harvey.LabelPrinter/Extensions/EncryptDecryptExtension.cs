﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Harvey.LabelPrinter.Extensions
{
    public static class EncryptDecryptExtension
    {
        private static string Hash = "f0xle@rn";

        public static string Encrypt(string plainText, string hash = "")
        {
            if (string.IsNullOrEmpty(hash))
            {
                hash = Hash;
            }

            var result = "";
            try
            {
                byte[] data = UTF8Encoding.UTF8.GetBytes(plainText);
                using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
                {
                    byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                    using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider()
                    {
                        Key = keys,
                        Mode = CipherMode.ECB,
                        Padding = PaddingMode.PKCS7
                    })
                    {
                        ICryptoTransform transform = tripDes.CreateEncryptor();
                        byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                        result = Convert.ToBase64String(results, 0, results.Length);
                    }
                }
            }
            catch (Exception)
            {

            }
            
            return result;
        }

        public static string Decrypt(string encryptText, string hash = "")
        {
            if (string.IsNullOrEmpty(hash))
            {
                hash = Hash;
            }

            var result = "";
            try
            {
                byte[] data = Convert.FromBase64String(encryptText);
                using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
                {
                    byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                    using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider()
                    {
                        Key = keys,
                        Mode = CipherMode.ECB,
                        Padding = PaddingMode.PKCS7
                    })
                    {
                        ICryptoTransform transform = tripDes.CreateDecryptor();
                        byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                        result = UTF8Encoding.UTF8.GetString(results, 0, results.Length);
                    }
                }
            }
            catch (Exception)
            {

            }

            return result;
        }
    }
}
