﻿using System;
using System.Linq.Expressions;

namespace Harvey.LabelPrinter.Extensions
{
    public static class ClassExtension
    {
        public static string GetMemberName<T, TValue>(Expression<Func<T, TValue>> memberAccess)
        {
            return ((MemberExpression)memberAccess.Body).Member.Name;
        }
    }
}
