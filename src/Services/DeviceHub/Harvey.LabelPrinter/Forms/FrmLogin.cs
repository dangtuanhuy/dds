﻿using Harvey.LabelPrinter.Extensions;
using Harvey.LabelPrinter.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Harvey.LabelPrinter.Forms
{
    public partial class FrmLogin : Form
    {
        LoginModel LoginModel;
        EnviromentSetting Enviroment;

        public FrmLogin()
        {
            InitializeComponent();
            LoginModel = new LoginModel();
            Enviroment = new EnviromentSetting();
        }

        public FrmLogin(LoginModel loginModel, EnviromentSetting enviroment)
        {
            InitializeComponent();
            LoginModel = loginModel;
            Enviroment = enviroment;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var loggedIn = IdsExtention.CheckLogin(Enviroment.ConnectionString.IdsApiUrl, txtEmail.Text, txtPassword.Text);
            if (loggedIn)
            {
                LoginModel.Email = txtEmail.Text;
                LoginModel.Password = txtPassword.Text;
                LoginModel.LoggedIn = true;
                this.Close();
            }
            else
            {
                LoginModel.LoggedIn = false;
                MessageBox.Show("Invalid email or password!");
            }
        }

        private void FrmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(null, null);
            }
        }
    }
}
