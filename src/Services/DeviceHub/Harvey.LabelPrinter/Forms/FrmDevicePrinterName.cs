﻿using Harvey.LabelPrinter.Log;
using Harvey.LabelPrinter.Models;
using System;
using System.Windows.Forms;

namespace Harvey.LabelPrinter.Forms
{
    public partial class FrmDevicePrinterName : Form
    {
        DevicePrinterNameModel DevicePrinterNameModel = new DevicePrinterNameModel();

        public FrmDevicePrinterName()
        {
            InitializeComponent();
        }

        public FrmDevicePrinterName(DevicePrinterNameModel devicePrinterName)
        {
            InitializeComponent();
            DevicePrinterNameModel = devicePrinterName;
            txtLabelPrinterName.Text = DevicePrinterNameModel.PrinterLabel;
        }

        private void btnSaveLabelPrinter_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLabelPrinterName.Text))
            {
                MessageBox.Show("Name is empty!");
                return;
            }

            LogFile.WriteFileContent(FileNames.AppSettingDevicePrintLabelName, txtLabelPrinterName.Text);
            DevicePrinterNameModel.PrinterLabel = txtLabelPrinterName.Text;
            MessageBox.Show("Success");
        }

        private void FrmDevicePrinterName_Load(object sender, EventArgs e)
        {

        }

        private void FrmDevicePrinterName_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
