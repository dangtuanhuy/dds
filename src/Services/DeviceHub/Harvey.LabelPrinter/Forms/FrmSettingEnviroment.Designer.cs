﻿namespace Harvey.LabelPrinter.Forms
{
    partial class FrmSettingEnviroment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaveConnectionString = new System.Windows.Forms.Button();
            this.txtIdsApiUrl = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtImsApiUrl = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSaveConnectionString
            // 
            this.btnSaveConnectionString.Location = new System.Drawing.Point(461, 165);
            this.btnSaveConnectionString.Name = "btnSaveConnectionString";
            this.btnSaveConnectionString.Size = new System.Drawing.Size(75, 23);
            this.btnSaveConnectionString.TabIndex = 8;
            this.btnSaveConnectionString.Text = "Save";
            this.btnSaveConnectionString.UseVisualStyleBackColor = true;
            this.btnSaveConnectionString.Click += new System.EventHandler(this.btnSaveConnectionString_Click);
            // 
            // txtIdsApiUrl
            // 
            this.txtIdsApiUrl.Location = new System.Drawing.Point(218, 73);
            this.txtIdsApiUrl.Name = "txtIdsApiUrl";
            this.txtIdsApiUrl.Size = new System.Drawing.Size(328, 20);
            this.txtIdsApiUrl.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(154, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Ids Url";
            // 
            // txtImsApiUrl
            // 
            this.txtImsApiUrl.Location = new System.Drawing.Point(218, 112);
            this.txtImsApiUrl.Name = "txtImsApiUrl";
            this.txtImsApiUrl.Size = new System.Drawing.Size(328, 20);
            this.txtImsApiUrl.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(154, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Ims Api Url";
            // 
            // FrmSettingEnviroment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtImsApiUrl);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtIdsApiUrl);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSaveConnectionString);
            this.Name = "FrmSettingEnviroment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Setting Enviroment";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSettingEnviroment_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSaveConnectionString;
        private System.Windows.Forms.TextBox txtIdsApiUrl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtImsApiUrl;
        private System.Windows.Forms.Label label6;
    }
}