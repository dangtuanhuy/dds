﻿using Harvey.LabelPrinter.Extensions;
using Harvey.LabelPrinter.Log;
using Harvey.LabelPrinter.Models;
using System.Windows.Forms;

namespace Harvey.LabelPrinter.Forms
{
    public partial class FrmSettingEnviroment : Form
    {
        EnviromentSetting EnviromentSetting;

        public FrmSettingEnviroment()
        {
            InitializeComponent();
            EnviromentSetting = new EnviromentSetting();
        }

        public FrmSettingEnviroment(EnviromentSetting enviromentSetting)
        {
            InitializeComponent();
            EnviromentSetting = enviromentSetting;
            var connectionString = EnviromentSetting.ConnectionString;

            txtIdsApiUrl.Text = connectionString.IdsApiUrl;
            txtImsApiUrl.Text = connectionString.ImsApiUrl;
        }

        private void btnSaveConnectionString_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIdsApiUrl.Text))
            {
                MessageBox.Show("Ids Url is empty!");
                return;
            }

            if (string.IsNullOrEmpty(txtImsApiUrl.Text))
            {
                MessageBox.Show("Ims Url is empty!");
                return;
            }
            
            string[] contents = new string[]
            {
                txtIdsApiUrl.Text,
                txtImsApiUrl.Text
            };

            LogFile.WriteFileContentOverwrite(FileNames.AppSettingApiUrl, contents);
            EnviromentSetting.ConnectionString.IdsApiUrl = txtIdsApiUrl.Text.TrimEnd('/');
            EnviromentSetting.ConnectionString.ImsApiUrl = txtImsApiUrl.Text.TrimEnd('/');
            MessageBox.Show("Success");
        }

        private void FrmSettingEnviroment_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
