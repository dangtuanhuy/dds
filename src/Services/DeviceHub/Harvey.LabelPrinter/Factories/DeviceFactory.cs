﻿using Harvey.LabelPrinter.Enums;
using Harvey.LabelPrinter.Implementations.Espson;

namespace Harvey.LabelPrinter.Factories
{
    public class DeviceFactory
    {
        public static dynamic CreateDeviceFromType(DeviceCommand device)
        {
            switch (device)
            {
                case DeviceCommand.PrintProductLabel:
                    return new EpsonPrinterDevice();
                default:
                    throw new System.NotSupportedException($"Device hub did not support device for command {device.ToString()}");
            }
        }
    }
}
