﻿using Harvey.LabelPrinter.Enums;
using Harvey.LabelPrinter.Implementations.Espson;

namespace Harvey.LabelPrinter.Factories
{
    public class SerializerFactory
    {
        public static dynamic CreateSerializerFromType(DeviceCommand device)
        {
            switch (device)
            {
                case DeviceCommand.PrintProductLabel:
                    return new EsponPrintProductLabelSerializer();
                default:
                    throw new System.NotSupportedException($"Device hub didnot support serilizer for command {device.ToString()}");
            }
        }
    }
}
