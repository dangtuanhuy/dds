﻿using System.Threading.Tasks;

namespace Harvey.LabelPrinter
{
    public interface IDevice<TInput>
    {
        Task<bool> ExecuteAsync(string deviceHubName, TInput model);
    }
}
