﻿using System;
using System.IO;
//using System.IO;

namespace Harvey.LabelPrinter.Log
{
    public static class LogFile
    {
        public static string ReadFile(string fileName)
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files\\" + fileName + ".txt";
            try
            {
                return File.ReadAllText(path);
            }
            catch (System.Exception e)
            {
                return "";
            }
        }

        public static string[] ReadFileByteArray(string fileName)
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files\\" + fileName + ".txt";
            try
            {
                return File.ReadAllLines(path);
            }
            catch (System.Exception e)
            {
                return new string[] { };
            }
        }

        public static void WriteFile(string fileName, string caption, string text)
        {
            EnsureFolderExist("Files");
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files";
            string path = folderPath + "\\" + fileName + ".txt";
            var fileContent = ReadFile(fileName);
            var logText = $"{DateTime.UtcNow} {caption}: {text}";

            string[] contents = new string[]
            {
                fileContent,
                logText
            };
            File.WriteAllLines(path, contents);
        }

        public static void WriteFileContent(string fileName, string text)
        {
            EnsureFolderExist("Files");
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files";
            string path = folderPath + "\\" + fileName + ".txt";

            string[] contents = new string[]
            {
                text
            };
            File.WriteAllLines(path, contents);
        }

        public static void WriteFileContentOverwrite(string fileName, string[] contents)
        {
            EnsureFolderExist("Files");
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files";
            string path = folderPath + "\\" + fileName + ".txt";
            File.WriteAllLines(path, contents);
        }

        public static void WriteAllBytes(string fileName, byte[] data)
        {
            EnsureFolderExist("Files");
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files";
            string path = folderPath + $"\\{fileName}.txt";
            File.WriteAllBytes(path, data);
        }

        public static void WriteAllBytesToPdfFile(string fileName, byte[] data)
        {
            EnsureFolderExist("Files");
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files";
            string path = folderPath + $"\\{fileName}.pdf";

            FileStream fs = File.Create(path);
            foreach (var item in data)
            {
                fs.WriteByte(item);
            }

            BinaryWriter bw = new BinaryWriter(fs);


            //File.WriteAllBytes(path, data);
        }

        private static void EnsureFolderExist(string folderName)
        {
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + $"\\{folderName}";
            bool exists = System.IO.Directory.Exists(folderPath);

            if (!exists)
                System.IO.Directory.CreateDirectory(folderPath);
        }

        public static void EnsureExactlyFolderExist(string pathFolder)
        {
            bool exists = System.IO.Directory.Exists(pathFolder);

            if (!exists)
                System.IO.Directory.CreateDirectory(pathFolder);
        }
    }
}