﻿using System;

namespace Harvey.LabelPrinter.Log
{
    public static class FileNames
    {
        public static string PrintProductLabelLog = "PrintProductLabelLog";
        public static string PrintroductLabelReview = "PrintroductLabelReview";
        public static string RawPrinterHelperLog = "RawPrinterHelperLog";

        public static string AppSettingDevicePrintLabelName = "AppSettingDevicePrintLabelName";
        public static string AppSettingConnectiontring = "AppSettingConnectiontring";

        public static string AppSettingApiUrl = "AppSettingApiUrl";

        public static string GetFileNameByData(string fileName)
        {
            var now = DateTime.UtcNow;
            return $"{now.Year}_{now.Month}_{now.Day}_{fileName}";
        }
    }
}