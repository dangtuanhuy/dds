﻿namespace Harvey.LabelPrinter
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrintLabel = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.appSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devicePrinterNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviromentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbltest = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrintLabel
            // 
            this.btnPrintLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintLabel.Location = new System.Drawing.Point(42, 67);
            this.btnPrintLabel.Name = "btnPrintLabel";
            this.btnPrintLabel.Size = new System.Drawing.Size(158, 54);
            this.btnPrintLabel.TabIndex = 0;
            this.btnPrintLabel.Text = "Print label";
            this.btnPrintLabel.UseVisualStyleBackColor = true;
            this.btnPrintLabel.Click += new System.EventHandler(this.btnPrintLabel_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.appSettingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // appSettingToolStripMenuItem
            // 
            this.appSettingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.devicePrinterNameToolStripMenuItem,
            this.enviromentToolStripMenuItem});
            this.appSettingToolStripMenuItem.Name = "appSettingToolStripMenuItem";
            this.appSettingToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.appSettingToolStripMenuItem.Text = "App Setting";
            this.appSettingToolStripMenuItem.Click += new System.EventHandler(this.appSettingToolStripMenuItem_Click);
            // 
            // devicePrinterNameToolStripMenuItem
            // 
            this.devicePrinterNameToolStripMenuItem.Name = "devicePrinterNameToolStripMenuItem";
            this.devicePrinterNameToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.devicePrinterNameToolStripMenuItem.Text = "Device Printer Name";
            this.devicePrinterNameToolStripMenuItem.Click += new System.EventHandler(this.devicePrinterNameToolStripMenuItem_Click);
            // 
            // enviromentToolStripMenuItem
            // 
            this.enviromentToolStripMenuItem.Name = "enviromentToolStripMenuItem";
            this.enviromentToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.enviromentToolStripMenuItem.Text = "Enviroment";
            this.enviromentToolStripMenuItem.Click += new System.EventHandler(this.enviromentToolStripMenuItem_Click);
            // 
            // lbltest
            // 
            this.lbltest.AutoSize = true;
            this.lbltest.Location = new System.Drawing.Point(12, 428);
            this.lbltest.Name = "lbltest";
            this.lbltest.Size = new System.Drawing.Size(35, 13);
            this.lbltest.TabIndex = 2;
            this.lbltest.Text = "label1";
            this.lbltest.Visible = false;
            this.lbltest.Click += new System.EventHandler(this.lbltest_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbltest);
            this.Controls.Add(this.btnPrintLabel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrintLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem appSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem devicePrinterNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviromentToolStripMenuItem;
        private System.Windows.Forms.Label lbltest;
    }
}

