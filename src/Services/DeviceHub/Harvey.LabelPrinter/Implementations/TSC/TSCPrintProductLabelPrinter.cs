﻿using Harvey.LabelPrinter.Log;
using Harvey.LabelPrinter.Models;
using System;
using System.Collections.Generic;

namespace Harvey.LabelPrinter.Implementations.TSC
{
    public class TSCPrintProductLabelPrinter
    {
        public void Print(string deviceHubName, ProductLabelPrinterModel productLabelPrinterModel)
        {
            try
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Start Print label");

                TSCLIB_DLL.openport(deviceHubName);
                TSCLIB_DLL.setup("35", "25", "4", "8", "GAP", "2.5", "2");
                TSCLIB_DLL.clearbuffer();
                var variantDescriptions = SplitVariantName(productLabelPrinterModel.Variant);

                var paperYPos = 12;
                foreach (var item in variantDescriptions)
                {
                    TSCLIB_DLL.printerfont("24", paperYPos.ToString(), "1", "0", "1", "1", item);
                    paperYPos += 21;
                }

                TSCLIB_DLL.barcode("24", paperYPos.ToString(), "128", "44", "0", "0", "1", "1", productLabelPrinterModel.BarCode);
                paperYPos += 46;

                TSCLIB_DLL.printerfont("24", paperYPos.ToString(), "2", "0", "1", "1", productLabelPrinterModel.SKUCode);
                paperYPos += 21;

                if (productLabelPrinterModel.ListPrice == productLabelPrinterModel.MemberPrice)
                {
                    var priceText = $"Price $ {string.Format("{0:0.00}", productLabelPrinterModel.ListPrice)}";
                    TSCLIB_DLL.printerfont("24", paperYPos.ToString(), "2", "0", "1", "1", priceText);
                }
                else
                {
                    var listPriceText = $"U. P   $ {string.Format("{0:0.00}", productLabelPrinterModel.ListPrice)}";
                    var memberPriceText = $"Member $ {string.Format("{0:0.00}", productLabelPrinterModel.MemberPrice)}";
                    TSCLIB_DLL.printerfont("24", paperYPos.ToString(), "2", "0", "1", "1", listPriceText);
                    paperYPos += 21;
                    TSCLIB_DLL.printerfont("24", paperYPos.ToString(), "2", "0", "1", "1", memberPriceText);
                }

                TSCLIB_DLL.printlabel("1", productLabelPrinterModel.quantity.ToString());
                TSCLIB_DLL.closeport();

                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Print Label Success");
            }
            catch (Exception ex)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Print Label fail");
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label - Error: ", ex.Message);
                throw;
            }
        }

        private List<string> SplitVariantName(string variantName)
        {
            if (variantName.Length <= 15)
            {
                return new List<string>() { variantName };
            }

            var result = new List<string>();
            result.Add(variantName.Substring(0, 15));
            var remainning = variantName.Substring(15, variantName.Length - 15);
            result.Add(remainning.Length <= 15 ? remainning : remainning.Substring(0, 15));
            return result;
        }

        public void PrintByCommand(string deviceHubName, ProductLabelPrinterModel productLabelPrinterModel)
        {
            //try
            //{
            //    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Start Print label");

            //    TSCLIB_DLL.openport(deviceHubName);
            //    TSCLIB_DLL.sendcommand("SIZE 35 mm,25 mm");
            //    TSCLIB_DLL.sendcommand("GAP 0,0");
            //    TSCLIB_DLL.sendcommand("DIRECTION 1");
            //    TSCLIB_DLL.sendcommand("CLS");
            //    var paperYPos = 12;
            //    TSCLIB_DLL.sendcommand($"TEXT 24,{paperYPos.ToString()},\"3.EFT\",0,1,1,2,\"{productLabelPrinterModel.Variant}\"");
            //    paperYPos += 50;
            //    TSCLIB_DLL.sendcommand($"BARCODE 24,{paperYPos},\"128\",100,2,0,1,1,\"{productLabelPrinterModel.BarCode}\"");
            //    paperYPos += 46;

            //    TSCLIB_DLL.sendcommand($"TEXT 24,{paperYPos},\"2.EFT\",0,1,1,2,\"{productLabelPrinterModel.SKUCode}\"");
            //    paperYPos += 21;
            //    var listPriceText = $"U. P   $ {string.Format("{0:0.00}", productLabelPrinterModel.ListPrice)}";
            //    var memberPriceText = $"Member $ {string.Format("{0:0.00}", productLabelPrinterModel.MemberPrice)}";

            //    TSCLIB_DLL.sendcommand($"TEXT 24,{paperYPos},\"2.EFT\",0,1,1,2,\"{listPriceText}\"");
            //    paperYPos += 21;
            //    TSCLIB_DLL.sendcommand($"TEXT 24,{paperYPos},\"2.EFT\",0,1,1,2,\"{memberPriceText}\"");
            //    TSCLIB_DLL.sendcommand($"PRINT 1,{productLabelPrinterModel.quantity}");
            //    TSCLIB_DLL.closeport();

            //    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Print Label Success");
            //}
            //catch (Exception ex)
            //{
            //    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Print Label fail");
            //    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label - Error: ", ex.Message);
            //    throw;
            //}
        }

        public bool PrintByCommands(string deviceHubName,
            List<ProductLabelPrinterModel> productLabelPrinterModels,
            string blockHeight,
            string GAP = "2.5",
            string GAPOffset = "2",
            string fit = "0",
            string manualBlockFont = "0",
            string manualBlockProperties = "0",
            string manualTextProperties = "0"
            )

        {
            try
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Start Print label");
                TSCLIB_DLL.openport(deviceHubName);

                //TSCLIB_DLL.setup("35","25", "4", "8", "0", "2.5", "2");

                var width = 35 * 8;
                foreach (var productLabelPrinterModel in productLabelPrinterModels)
                {

                    //TSCLIB_DLL.setup("35", "25", "4", "8", "1", "2.5", GAPOffset);
                    TSCLIB_DLL.sendcommand("SIZE 35 mm,25 mm");
                    TSCLIB_DLL.sendcommand("DENSITY 8");
                    TSCLIB_DLL.sendcommand("SET PRINTQUALITY OPTIMUM");
                    TSCLIB_DLL.sendcommand("SPEED 2");
                    TSCLIB_DLL.sendcommand($"GAP \"2.5\" mm,2 mm");
                    TSCLIB_DLL.sendcommand("DIRECTION 1");
                    TSCLIB_DLL.sendcommand("CLS");
                    var paperYPos = 18;
                    //TSCLIB_DLL.sendcommand($"TEXT 140,{paperYPos.ToString()},\"1.EFT\",0,1,1,2,\"{productLabelPrinterModel.Variant}\"");
                    //fit = string.IsNullOrEmpty(fit) ? fit : ",fit";
                    string nameFont = "";
                    string blockProperties = "";
                    CalculateBlockFont(productLabelPrinterModel.Variant, ref nameFont, ref blockProperties);
                    TSCLIB_DLL.sendcommand($"BLOCK 16,{paperYPos.ToString()},{width - 32},{blockHeight},\"{nameFont}\",{blockProperties},\"{productLabelPrinterModel.Variant}\"");

                    paperYPos += 60;
                    TSCLIB_DLL.sendcommand($"BARCODE 125,{paperYPos},\"128\",45,0,0,1,1,2,\"{productLabelPrinterModel.BarCode}\"");
                    paperYPos += 55;

                    TSCLIB_DLL.sendcommand($"TEXT 125,{paperYPos},\"A.FNT\",{manualTextProperties},\"{productLabelPrinterModel.SKUCode}\"");
                    paperYPos += 21;
                    var listPriceText = $"U. P   $ {string.Format("{0:0.00}", productLabelPrinterModel.ListPrice)}";
                    var memberPriceText = $"Member $ {string.Format("{0:0.00}", productLabelPrinterModel.MemberPrice)}";

                    TSCLIB_DLL.sendcommand($"TEXT 125,{paperYPos},\"A.FNT\",{manualTextProperties},\"{listPriceText}\"");
                    paperYPos += 21;
                    TSCLIB_DLL.sendcommand($"TEXT 125,{paperYPos},\"A.FNT\",{manualTextProperties},\"{memberPriceText}\"");

                    if (productLabelPrinterModel.quantity > 1)
                    {
                        TSCLIB_DLL.sendcommand($"PRINT 1,{productLabelPrinterModel.quantity - 1}");
                    }
                    else
                    {
                        TSCLIB_DLL.sendcommand($"PRINT 1");
                    }
                }

                TSCLIB_DLL.closeport();

                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Print Label Success");
            }
            catch (Exception ex)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label", "Print Label fail");
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print Product Label - Error: ", ex.Message);
                return false;
            }

            return true;
        }

        public void PrintTest(string deviceHubName)
        {
            TSCLIB_DLL.openport(deviceHubName);

            TSCLIB_DLL.sendcommand("SIZE 35 mm,25 mm");
            TSCLIB_DLL.sendcommand("DENSITY 8");
            TSCLIB_DLL.sendcommand("SET PRINTQUALITY OPTIMUM");
            TSCLIB_DLL.sendcommand("SPEED 2");
            TSCLIB_DLL.sendcommand($"GAP \"2.5\" mm,2 mm");
            TSCLIB_DLL.sendcommand("DIRECTION 1");
            TSCLIB_DLL.sendcommand("CLS");
            var a = @"By more than a 2-1 ratio, lawmakers in West Virginia's House of
                    Delegates have approved a bill that would
                    allow gun owners to carry concealed";
            TSCLIB_DLL.sendcommand($"BLOCK 20,20,500,170,\"0\",0,10,10,0,0,1,\"{a}\"");

            TSCLIB_DLL.sendcommand($"PRINT 1");
            TSCLIB_DLL.closeport();
        }

        private void CalculateBlockFont(string name, ref string blockFont, ref string blockProperties)
        {
            if (string.IsNullOrEmpty(name) || name.Length <= 100)
            {
                blockFont = "A.FNT";
                blockProperties = "0,1,2, 0, 2";
            }
            else
            {
                blockFont = "A.FNT";
                blockProperties = "0,1.5,1.5, 0, 2";
            }

        }
    }
}
