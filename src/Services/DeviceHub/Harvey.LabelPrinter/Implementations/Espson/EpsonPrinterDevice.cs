﻿using Harvey.LabelPrinter.Log;
using System.Threading.Tasks;

namespace Harvey.LabelPrinter.Implementations.Espson
{
    public class EpsonPrinterDevice : IDevice<byte[]>
    {
        public Task<bool> ExecuteAsync(string deviceHubName, byte[] model)
        {
            var result = false;
            LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.RawPrinterHelperLog), "RawPrinterHelperLog", $"Start Sending model to printer \"{deviceHubName}\"!");
            try
            {
                result = RawPrinterHelper.SendValueToPrinter(deviceHubName, model, FileNames.RawPrinterHelperLog);
                if (result)
                {
                    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.RawPrinterHelperLog), "RawPrinterHelperLog", "Print success!");
                }
                else
                {
                    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.RawPrinterHelperLog), "RawPrinterHelperLog", "Print fail!");
                }
            }
            catch (System.Exception)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.RawPrinterHelperLog), "RawPrinterHelperLog", "Print got error!");
                result = false;
            }
            return Task.FromResult(result);
        }
    }
}
