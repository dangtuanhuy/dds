﻿using Harvey.LabelPrinter.Extensions;
using Harvey.LabelPrinter.Log;
using Harvey.LabelPrinter.Models;
using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;


namespace Harvey.LabelPrinter.Implementations.Espson
{
    public class EsponPrintProductLabelSerializer : ISerializer<byte[]>
    {
        public byte[] Serialize(string data)
        {
            var productLabel = JsonConvert.DeserializeObject<ProductLabel>(data);
            LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print product label", "Start printing product label!");

            byte[] result = null;
            try
            {
                result = GenerateLabel(productLabel);
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print product label", "Serialize data to byte array success!");
            }
            catch (Exception e)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print product label", "Serialize data to byte array fail!");
                throw;
            }

            return result;
        }

        public byte[] GenerateLabel(ProductLabel productLabel)
        {
            //PrinterUtility.EscPosEpsonCommands.EscPosEpson obj = new PrinterUtility.EscPosEpsonCommands.EscPosEpson(26);

            //var BytesValue = Encoding.ASCII.GetBytes(string.Empty);
            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.CharSize.Nomarl());
            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.FontSelect.FontA());

            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Lf());
            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            //BytesValue = PrintExtensions.AddBytes(BytesValue, $"{productLabel.Variant}\n\n");

            //LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print product label", "Create Barcode!");
            //try
            //{
            //    Bitmap bitmap = new Bitmap(productLabel.BarCode.Length * 40, 150);
            //    using (Graphics graphics = Graphics.FromImage(bitmap))
            //    {
            //        var oFont = new System.Drawing.Font("Code 128", 20);
            //        PointF point = new PointF(2f, 2f);
            //        SolidBrush black = new SolidBrush(Color.Black);
            //        SolidBrush white = new SolidBrush(Color.White);
            //        graphics.FillRectangle(white, 0, 0, bitmap.Width, bitmap.Height);
            //        graphics.DrawString("*" + productLabel.BarCode + "*", oFont, black, point);
            //    }

            //    using (MemoryStream ms = new MemoryStream())
            //    {
            //        bitmap.Save(ms, ImageFormat.Png);
            //        BytesValue = PrintExtensions.AddBytes(BytesValue, ms.ToArray());
            //    }

            //    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print product label", "Get BarCode.png!");
            //}
            //catch (Exception e)
            //{
            //    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Print product label", "Create Barcode fail!");
            //    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Message", e.Message);
            //    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintProductLabelLog), "Stack Trace", e.StackTrace);
            //}

            //BytesValue = PrintExtensions.AddBytes(BytesValue, "\n\n");
            //BytesValue = PrintExtensions.AddBytes(BytesValue, $"{productLabel.SKUCode}\n");
            //BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  List Price   $ {0}\n", string.Format("{0:0.00}", productLabel.ListPrice)));
            //BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  Member Price $ {0}\n", string.Format("{0:0.00}", productLabel.MemberPrice)));

            //BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.CutPage());

            //LogFile.WriteAllBytes(FileNames.PrintroductLabelReview, BytesValue);

            //return BytesValue;

            return new byte[] { };
        }
    }
}
