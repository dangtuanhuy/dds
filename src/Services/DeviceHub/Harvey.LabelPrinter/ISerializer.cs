﻿namespace Harvey.LabelPrinter
{
    public interface ISerializer<TOutput>
    {
        TOutput Serialize(string data);
    }
}
