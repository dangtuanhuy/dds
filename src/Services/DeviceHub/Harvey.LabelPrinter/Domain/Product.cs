﻿using System;

namespace Harvey.LabelPrinter.Domain
{
    public class Product
    {
        public Guid VariantId { get; set; }
        public string BarCode { get; set; }
        public string SKUCode { get; set; }
        public string VariantName { get; set; }
        public string CreatedDateString { get; set; }
        public int Quantity { get; set; }
        public double ListPrice { get; set; }
        public double MemberPrice { get; set; }
        public double StaffPrice { get; set; }
    }
}
