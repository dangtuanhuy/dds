﻿using System.Collections.Generic;

namespace Harvey.LabelPrinter.Domain
{
    public class ProductForPrintLabelResponse
    {
        public bool BarCodeDataOnly { get; set; }
        public List<Product> Products { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
    }
}
