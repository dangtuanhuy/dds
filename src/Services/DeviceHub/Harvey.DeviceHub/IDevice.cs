﻿using System.Threading.Tasks;

namespace Harvey.DeviceHub
{
    public interface IDevice<TInput>
    {
        Task<bool> ExecuteAsync(string deviceHubName, TInput model);
    }
}