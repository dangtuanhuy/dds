﻿namespace Harvey.DeviceHub.Models.CloseDayReports
{
    public class CloseDayReportPaymentModeDetail
    {
        public string Name { get; set; }
        public double Amount { get; set; }
    }
}