﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Harvey.DeviceHub.Models.CloseDayReports
{
    public class CloseDayReportCurrency
    {
        public double Value { get; set; }
        public int Quantity { get; set; }
        public double Amount { get; set; }
    }
}