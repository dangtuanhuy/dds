﻿using System;
using System.Collections.Generic;

namespace Harvey.DeviceHub.Models.CloseDayReports
{
    public class CloseDayReport
    {
        public string Location { get; set; }
        public string Terminal { get; set; }
        public string Cashier { get; set; }
        public int Shift { get; set; }
        public DateTime SettlementDate { get; set; }

        public double PosCashSales { get; set; }
        public double TotalCurrencies { get; set; }
        public double TillAmount { get; set; }
        public double TotalCollection { get; set; }
        public double GrandTotal { get; set; }
        public double SystemTotal { get; set; }
        public double ShortageExcess { get; set; }
        public double PaymentModeDetailstTotal { get; set; }
        public double TotalCashAndPaymentModeDetails { get; set; }

        public CashingUp OpenDay { get; set; }
        public List<CloseDayReportCurrency> Currencies { get; set; }
        public List<CloseDayReportPaymentModeDetail> PaymentModeDetails { get; set; }
    }
}