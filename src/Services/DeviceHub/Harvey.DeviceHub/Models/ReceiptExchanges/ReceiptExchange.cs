﻿using Harvey.DeviceHub.Models.Receipts;
using System.Collections.Generic;

namespace Harvey.DeviceHub.Models.ReceiptExchanges
{
    public class ReceiptExchange : ReceiptInfo
    {
        public double Collect { get; set; }
        public IList<ReceiptItem> PurchasedItems { get; set; }
        public IList<ReceiptItem> ExchangedItems { get; set; }
        public IList<ReceiptItem> NewItems { get; set; }
        public IList<SalePromotion> SalePromotions { get; set; }
        public IList<ReceiptPayment> SalePayments { get; set; }
    }
}