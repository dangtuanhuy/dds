﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Harvey.DeviceHub.Models
{
    public class CashingUp
    {
        public Guid UserId { get; set; }
        public double Amount { get; set; }
    }
}