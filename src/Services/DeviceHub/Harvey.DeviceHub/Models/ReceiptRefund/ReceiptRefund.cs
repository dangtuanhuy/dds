﻿using Harvey.DeviceHub.Models.Receipts;
using System.Collections.Generic;

namespace Harvey.DeviceHub.Models.ReceiptRefund
{
    public class ReceiptRefund: ReceiptInfo
    {
        public double Refund { get; set; }
        public IList<ReceiptItem> PurchasedItems { get; set; }
        public IList<ReceiptItem> RefundedItems { get; set; }
        public IList<SalePromotion> SalePromotions { get; set; }
        public IList<ReceiptPayment> SalePayments { get; set; }
    }
}