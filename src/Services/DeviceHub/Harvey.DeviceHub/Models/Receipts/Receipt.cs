﻿using System.Collections.Generic;

namespace Harvey.DeviceHub.Models.Receipts
{
    public class Receipt: ReceiptInfo
    {
        public IList<ReceiptItem> Items { get; set; }
        public IList<SalePromotion> SalePromotions { get; set; }
        public IList<ReceiptPayment> SalePayments { get; set; }
    }
}