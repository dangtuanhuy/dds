﻿namespace Harvey.DeviceHub.Models.Receipts
{
    public class ReceiptPayment
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public double Amount { get; set; }
    }
}