﻿using System;

namespace Harvey.DeviceHub.Models.Receipts
{
    public class ReceiptInfo
    {
        public string BillNo { get; set; }
        public string PreviousBillNo { get; set; }
        public string NextBillNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CashierName { get; set; }
        public string CounterName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public double TotalAmount { get; set; }
        public double Change { get; set; }
        public double GstInclusiveAmount { get; set; }
        public double GstAmount { get; set; }
        public bool GstInclusive { get; set; }
        public int NetQuantity { get; set; }
        public double NetTotal { get; set; }
        public int ExchangeDayQuantity { get; set; }
        public string Reason { get; set; }
    }
}