﻿using Harvey.DeviceHub.Enums;
using System.Collections.Generic;

namespace Harvey.DeviceHub.Models.Receipts
{
    public class ReceiptItem
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string SKUCode { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double Amount { get; set; }
        public List<ReceiptItemPromotion> ReceiptItemPromotions { get; set; }
    }

    public class ReceiptItemPromotion
    {
        public string Description { get; set; }
        public DiscountType DiscountType { get; set; }
        public double Value { get; set; }
        public double Amount { get; set; }
    }
}