﻿using Harvey.DeviceHub.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Harvey.DeviceHub.Models.Receipts
{
    public class SalePromotion
    {
        public string Description { get; set; }
        public DiscountType DiscountType { get; set; }
        public double Value { get; set; }
        public double Amount { get; set; }
    }
}