﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Harvey.DeviceHub.Extensions
{
    public static class BarCodeExtension
    {
        public static byte[] GenerateBarCodeImage(string barCode)
        {
            Bitmap bitmap = new Bitmap(barCode.Length * 40, 150);
            
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                var oFont = new System.Drawing.Font("Code 128", 20);
                PointF point = new PointF(2f, 2f);
                SolidBrush black = new SolidBrush(Color.Black);
                SolidBrush white = new SolidBrush(Color.White);
                graphics.FillRectangle(white, 0, 0, bitmap.Width, bitmap.Height);
                graphics.DrawString("*" + barCode + "*", oFont, black, point);

                bitmap.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\barCode.png");
                using (MemoryStream ms = new MemoryStream())
                {
                    bitmap.Save(ms, ImageFormat.Png);
                    return ms.ToArray();
                }
            }
        }
    }
}