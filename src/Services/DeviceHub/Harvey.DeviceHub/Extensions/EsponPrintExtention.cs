﻿using Harvey.DeviceHub.Models.Receipts;
using PrinterUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Harvey.DeviceHub.Extensions
{
    public static class EsponPrintExtention
    {
        public static byte[] GetLogo(string LogoPath, double multiplier = 570)
        {
            var fullMultiplier = 570;
            List<byte> byteList = new List<byte>();
            if (!File.Exists(LogoPath))
                return null;
            BitmapData data = GetBitmapData(LogoPath, multiplier, fullMultiplier);
            BitArray dots = data.Dots;
            byte[] width = BitConverter.GetBytes(data.Width);

            int offset = 0;
            MemoryStream stream = new MemoryStream();
            // BinaryWriter bw = new BinaryWriter(stream);
            byteList.Add(Convert.ToByte(Convert.ToChar(0x1B)));
            //bw.Write((char));
            byteList.Add(Convert.ToByte('@'));
            //bw.Write('@');
            byteList.Add(Convert.ToByte(Convert.ToChar(0x1B)));
            // bw.Write((char)0x1B);
            byteList.Add(Convert.ToByte('3'));
            //bw.Write('3');
            //bw.Write((byte)24);
            byteList.Add((byte)24);
            while (offset < data.Height)
            {
                byteList.Add(Convert.ToByte(Convert.ToChar(0x1B)));
                byteList.Add(Convert.ToByte('*'));
                //bw.Write((char)0x1B);
                //bw.Write('*');         // bit-image mode
                byteList.Add((byte)33);
                //bw.Write((byte)33);    // 24-dot double-density
                byteList.Add(width[0]);
                byteList.Add(width[1]);
                //bw.Write(width[0]);  // width low byte
                //bw.Write(width[1]);  // width high byte

                for (int x = 0; x < data.Width; ++x)
                {
                    for (int k = 0; k < 3; ++k)
                    {
                        byte slice = 0;
                        for (int b = 0; b < 8; ++b)
                        {
                            int y = (((offset / 8) + k) * 8) + b;
                            // Calculate the location of the pixel we want in the bit array.
                            // It'll be at (y * width) + x.
                            int i = (y * data.Width) + x;

                            // If the image is shorter than 24 dots, pad with zero.
                            bool v = false;
                            if (i < dots.Length)
                            {
                                v = dots[i];
                            }
                            slice |= (byte)((v ? 1 : 0) << (7 - b));
                        }
                        byteList.Add(slice);
                        //bw.Write(slice);
                    }
                }
                offset += 24;
                byteList.Add(Convert.ToByte(0x0A));
                //bw.Write((char));
            }
            // Restore the line spacing to the default of 30 dots.
            byteList.Add(Convert.ToByte(0x1B));
            byteList.Add(Convert.ToByte('3'));
            //bw.Write('3');
            byteList.Add((byte)30);
            return byteList.ToArray();
            //bw.Flush();
            //byte[] bytes = stream.ToArray();
            //return logo + Encoding.Default.GetString(bytes);
        }

        // multiplier this depends on your printer model. for Beiyang you should use 1000
        private static BitmapData GetBitmapData(string bmpFileName, double multiplier, double fullMultiplier)
        {
            using (var bitmap = (Bitmap)Bitmap.FromFile(bmpFileName))
            {
                var threshold = 127;
                var index = 0;

                double scale = (double)(multiplier / (double)bitmap.Width);
                int xheight = (int)(bitmap.Height * scale);
                int xwidth = (int)(bitmap.Width * scale);


                double fullScale = (double)(fullMultiplier / (double)bitmap.Width);
                int fullXheight = (int)(bitmap.Height * fullScale);
                int fullXwidth = (int)(bitmap.Width * fullScale);
                var fullDimensions = fullXwidth * fullXheight;
                var fullDots = new BitArray(fullDimensions);

                var dimensions = fullXwidth * xheight;
                var dots = new BitArray(dimensions);

                var whiteCharactersWidth = fullXwidth - xwidth;
                var whiteCharactersHeight = fullXheight - xheight;
                var whiteCharactersHalfWidth = whiteCharactersWidth / 2;
                var whiteCharactersHalfHeight = whiteCharactersHeight / 2;

                //List<List<int>> test = new List<List<int>>();

                for (var y = 0; y < xheight; y++)
                {
                    //var test2 = new List<int>();

                    var currentWidth = 0;
                    for (int i = 0; i < whiteCharactersHalfWidth; i++)
                    {
                        var luminance = 255;
                        dots[index] = (luminance < threshold);
                        //test2.Add(luminance);
                        index++;
                    }
                    currentWidth = whiteCharactersHalfWidth;

                    for (var x = 0; x < xwidth; x++)
                    {
                        var _x = (int)(x / scale);
                        var _y = (int)(y / scale);
                        var color = bitmap.GetPixel(_x, _y);
                        var luminance = (int)(color.R * 0.3 + color.G * 0.59 + color.B * 0.11);
                        dots[index] = (luminance < threshold);
                        //test2.Add(luminance);
                        index++;
                    }

                    currentWidth += xwidth;
                    var remainingWidth = fullXwidth - currentWidth;

                    for (int i = 0; i < remainingWidth; i++)
                    {
                        var luminance = 255;
                        dots[index] = (luminance < threshold);
                        //test2.Add(luminance);
                        index++;
                    }

                    //test.Add(test2);
                }
                //var count = 0;
                //var indexs = new List<int>();
                //for (int i = 0; i < test.Count; i++)
                //{
                //    var item = test[i];
                //    var a = item.Where(x => x == 255).ToList();
                //    if (a.Count == 437)
                //    {
                //        count++;
                //    }
                //    if (a.Count == item.Count)
                //    {
                //        indexs.Add(i);
                //    }
                //}

                return new BitmapData()
                {
                    Dots = dots,
                    Height = (int)(bitmap.Height * scale),
                    Width = fullXwidth
                };
            }
        }

        public static byte[] CutPage()
        {
            List<byte> oby = new List<byte>();
            oby.Add(Convert.ToByte(Convert.ToChar(0x1D)));
            oby.Add(Convert.ToByte('V'));
            oby.Add((byte)66);
            oby.Add((byte)3);
            return oby.ToArray();
        }

        public static List<string> SpitVariantName(string name, int wordQuantityOnRow)
        {
            List<string> result = new List<string>();
            if (string.IsNullOrEmpty(name))
            {
                return result;
            }

            var namesWithSpace = name.Split(' ');
            var currentName = "";
            foreach (var nameBetweenSpace in namesWithSpace)
            {
                if (currentName.Length + nameBetweenSpace.Length + 1 < wordQuantityOnRow)
                {
                    currentName += " " + nameBetweenSpace;
                }
                else
                {
                    var nameWithHorizontalLine = nameBetweenSpace.Split('-');
                    var isSpaceCharacter = true;
                    for (int i = 0; i < nameWithHorizontalLine.Length; i++)
                    {
                        if (currentName.Length + nameWithHorizontalLine[i].Length + 1 >= wordQuantityOnRow)
                        {
                            result.Add(currentName);
                            currentName = "";
                        }

                        if (isSpaceCharacter)
                        {
                            currentName += " " + nameWithHorizontalLine[i];
                            isSpaceCharacter = false;
                        }
                        else
                        {
                            currentName += "-" + nameWithHorizontalLine[i];
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(currentName))
            {
                result.Add(currentName);
            }

            return result;
        }

        public static string GetSeparator()
        {
            return "------------------------------------------\n";
        }

        public static byte[] PrintPromotions(IList<SalePromotion> salePromotions)
        {
            var BytesValue = Encoding.ASCII.GetBytes(string.Empty);
            if (salePromotions != null && salePromotions.Any())
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($" Manual Discount:\n"));

                foreach (var salePromotion in salePromotions)
                {
                    BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"   {salePromotion.Description}\n"));
                    if (salePromotion.DiscountType == Enums.DiscountType.Money)
                    {
                        BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0,39}($)\n", string.Format("{0:0.00}", salePromotion.Amount)));
                    }

                    if (salePromotion.DiscountType == Enums.DiscountType.Percent)
                    {
                        BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0,25}(%){1,11}($)\n", string.Format("{0:0.00}", salePromotion.Value),
                                                                                                string.Format("{0:0.00}", salePromotion.Amount)));
                    }
                }
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            }

            return BytesValue;
        }

        public static byte[] GetConfiguration(PrinterUtility.EscPosEpsonCommands.EscPosEpson obj, byte[] BytesValue)
        {
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.CharSize.Nomarl());
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.FontSelect.FontA());
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Left());

            return BytesValue;
        }

    }
}