﻿using Harvey.DeviceHub.Log;
using Harvey.DeviceHub.Models;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;

namespace Harvey.DeviceHub.Controllers
{
    [RoutePrefix("api/devices")]
    public class DevicesController : ApiController
    {
        [HttpPost]
        public async Task<bool> PostReceiptPrinter([FromBody] DeviceRequest request)
        {
            var deviceExecutor = new DeviceExecutor();
            return await deviceExecutor.ExecuteAsync(request);
        }

        [HttpGet]
        [Route("cash-drawer")]
        public void GetCashDrawer(string printerShareName)
        {
            string fileName = "open.bat";
            string cashDrawerPath = $"{System.AppDomain.CurrentDomain.BaseDirectory}App_Data\\Open_Cash_Drawer";
            string filePath = $"{cashDrawerPath}\\{fileName}";
            var newOpenBat = "Copy /b open.txt" + " " + printerShareName;

            var cdCashDrawer = "cd " + cashDrawerPath;
            var data = new string[]{
                cdCashDrawer,
                newOpenBat
            };
            LogFile.WriteContentArray(filePath, data);

            LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.CashDrawerOpen), "Open Cash Drawer", "Open open.bat file");
            try
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.CashDrawerOpen),
                    "Open Cash Drawer file path", filePath);
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.CashDrawerOpen), "Open Cash Drawer test", LogFile.ReadContent(filePath));

                Process process = new Process();
                process.StartInfo.FileName = filePath;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();

                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.CashDrawerOpen), "Open Cash Drawer", "Open Cash Drawer successfully!");
            }
            catch (System.Exception e)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.CashDrawerOpen), "Open Cash Drawer", "Open Cash Drawer failure!");
            }

        }
    }
}