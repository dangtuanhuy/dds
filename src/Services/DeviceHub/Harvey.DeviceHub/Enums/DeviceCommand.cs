﻿namespace Harvey.DeviceHub.Enums
{
    public enum DeviceCommand
    {
        PrintReceipt = 1,
        PrintEndDayReport = 2,
        PrintReceiptRefund = 3,
        PrintReceiptExchange = 4
    }
}