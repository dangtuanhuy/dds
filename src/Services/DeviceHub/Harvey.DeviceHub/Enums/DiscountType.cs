﻿namespace Harvey.DeviceHub.Enums
{
    public enum DiscountType
    {
        Default = 0,
        Money = 1,
        Percent = 2
    }
}