﻿using System;

namespace Harvey.DeviceHub.Log
{
    public static class FileNames
    {
        public static string PrintReceiptLog = "PrintReceiptLog";
        public static string PrintReceiptReview = "PrintReceiptReview";

        public static string PrintReceiptExchangeLog = "PrintReceiptExchangeLog";
        public static string PrintReceiptExchangeReview = "PrintReceiptExchangeReview";

        public static string PrintCloseDayReportLog = "PrintCloseDayReportLog";
        public static string PrintCloseDayReportReview = "PrintCloseDayReportReview";

        public static string PrintReceiptRefundLog = "PrintReceiptRefundLog";
        public static string PrintReceiptRefundReview = "PrintReceiptRefundReview";

        public static string EpsonPrinterLog = "EpsonPrinterLog";

        public static string CashDrawerOpen = "CashDrawerOpen";

        public static string GetFileNameByData(string fileName)
        {
            var now = DateTime.UtcNow;
            return $"{now.Year}_{now.Month}_{now.Day}_{fileName}";
        }
    }
}