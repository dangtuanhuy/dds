﻿using System;
using System.IO;

namespace Harvey.DeviceHub.Log
{
    public static class LogFile
    {
        public static string ReadFile(string fileName)
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files\\" + fileName + ".txt";
            try
            {
                return File.ReadAllText(path);
            }
            catch (System.Exception e)
            {
                return "";
            }
        }

        public static void WriteFile(string fileName, string caption, string text)
        {
            EnsureFolderExist("Files");
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files";
            string path = folderPath + "\\" + fileName + ".txt";
            var fileContent = ReadFile(fileName);
            var logText = $"{DateTime.UtcNow} {caption}: {text}";

            string[] contents = new string[]
            {
                fileContent,
                logText
            };
            File.WriteAllLines(path, contents);
        }

        public static void WriteContent(string filePath, string data)
        {
            File.WriteAllText(filePath, data);
        }

        public static void WriteContentArray(string filePath, string[] data)
        {
            File.WriteAllLines(filePath, data);
        }

        public static string ReadContent(string filePath)
        {
            return File.ReadAllText(filePath);
        }

        public static void WriteAllBytes(string fileName, byte[] data)
        {
            EnsureFolderExist("Files");
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Files";
            string path = folderPath + $"\\{fileName}.txt";
            File.WriteAllBytes(path, data);
        }

        private static void EnsureFolderExist(string folderName)
        {
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + $"\\{folderName}";
            bool exists = System.IO.Directory.Exists(folderPath);

            if (!exists)
                System.IO.Directory.CreateDirectory(folderPath);
        }
    }
}