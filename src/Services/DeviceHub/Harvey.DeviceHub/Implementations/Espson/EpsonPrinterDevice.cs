﻿using Harvey.DeviceHub.Log;
using System.Threading.Tasks;

namespace Harvey.DeviceHub.Implementations.Espson
{
    public class EpsonPrinterDevice : IDevice<byte[]>
    {

        public Task<bool> ExecuteAsync(string deviceHubName, byte[] model)
        {
            var result = false;
            LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.EpsonPrinterLog), "Epso nPrinter Log", $"Start Sending receipt to printer \"{deviceHubName}\"!");
            try
            {
                result = RawPrinterHelper.SendValueToPrinter(deviceHubName, model, FileNames.EpsonPrinterLog);
                if (result)
                {
                    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.EpsonPrinterLog), "EpsonPrinterLog", "EpsonPrinterLog success!");
                }
                else
                {
                    LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.EpsonPrinterLog), "EpsonPrinterLog", "EpsonPrinterLog fail!");
                }
            }
            catch (System.Exception)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.EpsonPrinterLog), "EpsonPrinterLog", "EpsonPrinterLog got error!");
                result = false;
            }
            return Task.FromResult(result);
        }
    }
}