﻿using Harvey.DeviceHub.Extensions;
using Harvey.DeviceHub.Log;
using Harvey.DeviceHub.Models.ReceiptRefund;
using Harvey.DeviceHub.Models.Receipts;
using Newtonsoft.Json;
using PrinterUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.DeviceHub.Implementations.Espson
{
    public class EsponPrintReceiptRefundSerializer : ISerializer<byte[]>
    {
        public byte[] Serialize(string data)
        {
            LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptRefundLog), "Print receipt refund", "Start printing receipt!");

            var receiptRefund = JsonConvert.DeserializeObject<ReceiptRefund>(data);
            byte[] result = null;
            try
            {
                result = GenerateReceiptRefund(receiptRefund);
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptRefundLog), "Print receipt refund", "Serialize data to byte array success!");
            }
            catch (Exception e)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptRefundLog), "Print receipt refund", "Serialize data to byte array fail!");
                throw;
            }

            return result;
        }

        private byte[] GenerateReceiptRefund(ReceiptRefund receiptRefund)
        {
            var logoPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\logo.png";

            PrinterUtility.EscPosEpsonCommands.EscPosEpson obj = new PrinterUtility.EscPosEpsonCommands.EscPosEpson(35);

            var BytesValue = Encoding.ASCII.GetBytes(string.Empty);
            BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(logoPath, 350));
            BytesValue = EsponPrintExtention.GetConfiguration(obj, BytesValue);
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("TOG Connection Pte Ltd\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"{receiptRefund.StoreName}\n"));
            if (!string.IsNullOrEmpty(receiptRefund.StoreAddress))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"{receiptRefund.StoreAddress}\n\n"));
            }
            else
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"\n"));
            }

            if (!string.IsNullOrEmpty(receiptRefund.CustomerName))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"Name:  {receiptRefund.CustomerName}\n"));
            }
            if (!string.IsNullOrEmpty(receiptRefund.CustomerPhoneNumber))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"Phone: {receiptRefund.CustomerPhoneNumber}\n"));
            }
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));

            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes(string.Format("BillNo: {0}\n", receiptRefund.BillNo)));

            if (!string.IsNullOrEmpty(receiptRefund.PreviousBillNo))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("BillNo: {0} (Previous)\n", receiptRefund.PreviousBillNo)));
            }

            if (!string.IsNullOrEmpty(receiptRefund.NextBillNo))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("BillNo: {0} (Next)\n", receiptRefund.NextBillNo)));
            }

            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes(string.Format("Date: {0}\n", receiptRefund.CreatedDate.ToLocalTime().ToString("dd/MM/yyyy HH:mm"))));

            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes(string.Format("Cashier: {0}\n", receiptRefund.CashierName)));

            if (!string.IsNullOrEmpty(receiptRefund.CounterName))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes(string.Format("Counter: {0}\n", receiptRefund.CounterName)));
            }

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("No  Item        Qty    Net($)     Total($)\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(" Purchased Items\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, printItems(receiptRefund.PurchasedItems));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(" Refunded Items\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, printItems(receiptRefund.RefundedItems));

            BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.PrintPromotions(receiptRefund.SalePromotions));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Total Amount:{0,25}($)\n", string.Format("{0:0.00}", receiptRefund.TotalAmount))));
            if (receiptRefund.GstInclusive)
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($" (GST S$ {string.Format("{0:0.00}", receiptRefund.GstInclusiveAmount)} Inclusive)\n"));
            }
            else
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" GST {0,21} ($)\n", string.Format("{0:0.00}", receiptRefund.GstAmount))));
            }


            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Net Qty: {0,3}      Net Total: {1,9}($)\n", receiptRefund.NetQuantity,
                                                                                                                            string.Format("{0:0.00}", receiptRefund.NetTotal))));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Refund:{0,31}($)\n", string.Format("{0:0.00}", receiptRefund.Refund))));

            if (receiptRefund.SalePayments.Any())
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
                int amountPosition = 38;
                foreach (var receiptPayment in receiptRefund.SalePayments)
                {
                    if (receiptPayment.Amount != 0)
                    {
                        var dataString = " {0}" + "{1," + (amountPosition - receiptPayment.Name.Length).ToString() + "}($)\n";
                        BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(dataString, receiptPayment.Name, string.Format("{0:0.00}", receiptPayment.Amount))));
                    }
                }
            }

            if (receiptRefund.Change != 0)
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Change:{0,31}($)\n", string.Format("{0:0.00}", receiptRefund.Change))));
            }

            if (!string.IsNullOrEmpty(receiptRefund.Reason))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($" Reason: {receiptRefund.Reason}\n"));
            }

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"Retain Your Receipt for Exchange\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"within {receiptRefund.ExchangeDayQuantity} Days For Selected Items Only!\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"Thanks! Please Come again\n"));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Lf());
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            BytesValue = PrintExtensions.AddBytes(BytesValue, "\n\n");

            if (!string.IsNullOrEmpty(receiptRefund.BillNo))
            {
                Zen.Barcode.Code128BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                var barcodeImage = barcode.Draw(receiptRefund.BillNo, 50);
                barcodeImage.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\barCode.png");

                BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\barCode.png", 350));
                BytesValue = PrintExtensions.AddBytes(BytesValue, "\n\n");
            }

            if (!string.IsNullOrEmpty(receiptRefund.CustomerCode))
            {
                Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                var image = qrCode.Draw(receiptRefund.CustomerCode, 50);
                image.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\qrCode.png");

                BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\qrCode.png", 150));
                BytesValue = PrintExtensions.AddBytes(BytesValue, "\n");
            }

            BytesValue = EsponPrintExtention.GetConfiguration(obj, BytesValue);
            BytesValue = PrintExtensions.AddBytes(BytesValue, "------------Thank you for coming----------\n");
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Left());
            BytesValue = PrintExtensions.AddBytes(BytesValue, CutPage());

            LogFile.WriteAllBytes(FileNames.PrintReceiptRefundReview, BytesValue);

            return BytesValue;
        }
        public byte[] CutPage()
        {
            List<byte> oby = new List<byte>();
            oby.Add(Convert.ToByte(Convert.ToChar(0x1D)));
            oby.Add(Convert.ToByte('V'));
            oby.Add((byte)66);
            oby.Add((byte)3);
            return oby.ToArray();
        }

        private byte[] printItems(IList<ReceiptItem> items)
        {
            var BytesValue = Encoding.ASCII.GetBytes(string.Empty);
            if (items.Any())
            {
                foreach (var item in items)
                {
                    var splitNames = SpitName(item.Name, 38);
                    if (splitNames.Count == 0)
                    {
                        BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0} {1,7}\n", item.No, ""));
                    }
                    else
                    {
                        BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0}{1,7}\n", item.No, splitNames[0]));
                        for (int i = 1; i < splitNames.Count; i++)
                        {
                            BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {1,7}\n", item.No, splitNames[i]));
                        }
                    }

                    BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,-16}\n", item.SKUCode));
                    BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0,19}{1,9}{2,14}\n",
                                                                                    item.Quantity, string.Format("{0:0.00}", item.Price),
                                                                                    string.Format("{0:0.00}", item.Amount)));

                    var receiptItemPromotions = item.ReceiptItemPromotions;
                    if (receiptItemPromotions != null && receiptItemPromotions.Any())
                    {
                        foreach (var receiptItemPromotion in receiptItemPromotions)
                        {
                            if (receiptItemPromotion.DiscountType == Enums.DiscountType.Money)
                            {
                                BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,18}{1,22}\n",
                                    receiptItemPromotion.Description, string.Format("{0:0.00}", receiptItemPromotion.Amount)));
                            }

                            if (receiptItemPromotion.DiscountType == Enums.DiscountType.Percent)
                            {
                                BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,18}{1,11}(%){2,8}\n",
                                    receiptItemPromotion.Description, string.Format("{0:0.00}", receiptItemPromotion.Value), string.Format("{0:0.00}", receiptItemPromotion.Amount)));
                            }
                        }
                    }
                }
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            }
            return BytesValue;
        }

        private List<string> SpitName(string name, int wordQuantityOnRow)
        {
            List<string> result = new List<string>();
            if (string.IsNullOrEmpty(name))
            {
                return result;
            }

            var namesWithSpace = name.Split(' ');
            var currentName = "";
            foreach (var nameBetweenSpace in namesWithSpace)
            {
                if (currentName.Length + nameBetweenSpace.Length + 1 < wordQuantityOnRow)
                {
                    currentName += " " + nameBetweenSpace;
                }
                else
                {
                    var nameWithHorizontalLine = nameBetweenSpace.Split('-');
                    var isSpaceCharacter = true;
                    for (int i = 0; i < nameWithHorizontalLine.Length; i++)
                    {
                        if (currentName.Length + nameWithHorizontalLine[i].Length + 1 >= wordQuantityOnRow)
                        {
                            result.Add(currentName);
                            currentName = "";
                        }

                        if (isSpaceCharacter)
                        {
                            currentName += " " + nameWithHorizontalLine[i];
                            isSpaceCharacter = false;
                        }
                        else
                        {
                            currentName += "-" + nameWithHorizontalLine[i];
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(currentName))
            {
                result.Add(currentName);
            }

            return result;
        }
    }
}