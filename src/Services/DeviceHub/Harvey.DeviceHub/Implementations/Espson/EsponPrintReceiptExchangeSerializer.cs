﻿using Harvey.DeviceHub.Extensions;
using Harvey.DeviceHub.Log;
using Harvey.DeviceHub.Models.ReceiptExchanges;
using Harvey.DeviceHub.Models.Receipts;
using Newtonsoft.Json;
using PrinterUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.DeviceHub.Implementations.Espson
{
    public class EsponPrintReceiptExchangeSerializer : ISerializer<byte[]>
    {
        public byte[] Serialize(string data)
        {
            LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptExchangeLog), "Print receipt exchange", "Start printing receipt exchange!");

            var receipt = JsonConvert.DeserializeObject<ReceiptExchange>(data);
            byte[] result = null;
            try
            {
                result = GenerateReceipt(receipt);
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptExchangeLog), "Print receipt exchange", "Serialize data to byte array success!");
            }
            catch (Exception e)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptExchangeLog), "Print receipt exchange", "Serialize data to byte array fail!");
                throw;
            }

            return result;
        }

        private byte[] GenerateReceipt(ReceiptExchange receipt)
        {
            var togLogoPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\logo.png";

            PrinterUtility.EscPosEpsonCommands.EscPosEpson obj = new PrinterUtility.EscPosEpsonCommands.EscPosEpson(35);

            var BytesValue = Encoding.ASCII.GetBytes(string.Empty);
            BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(togLogoPath, 350));
            BytesValue = EsponPrintExtention.GetConfiguration(obj, BytesValue);
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));

            BytesValue = AddReceiptInfo(BytesValue, receipt);

            BytesValue = AddOrderItems(BytesValue, receipt);
            BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.PrintPromotions(receipt.SalePromotions));
          
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Total Amount:{0,25}($)\n", string.Format("{0:0.00}", receipt.TotalAmount))));
            if (receipt.GstInclusive)
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($" (GST S$ {string.Format("{0:0.00}", receipt.GstInclusiveAmount)} Inclusive)\n"));
            }
            else
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" GST {0,21} ($)\n", string.Format("{0:0.00}", receipt.GstAmount))));
            }


            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Net Qty: {0,3}      Net Total: {1,9}($)\n", receipt.NetQuantity,
                                                                                                                            string.Format("{0:0.00}", receipt.NetTotal))));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            if (receipt.Collect > 0)
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Collect:{0,30}($)\n", string.Format("{0:0.00}", receipt.Collect))));
            }
            else
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Refund:{0,31}($)\n", string.Format("{0:0.00}", -1 * receipt.Collect))));
            }


            BytesValue = AddPayments(BytesValue, receipt.SalePayments, receipt.Change);

            if (!string.IsNullOrEmpty(receipt.Reason))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($" Reason: {receipt.Reason}\n"));
            }

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"Retain Your Receipt for Exchange\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"within {receipt.ExchangeDayQuantity} Days For Selected Items Only!\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"Thanks! Please Come again\n"));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Lf());
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.CharSize.DoubleHeight6());
            BytesValue = PrintExtensions.AddBytes(BytesValue, "\n\n");

            if (!string.IsNullOrEmpty(receipt.BillNo))
            {
                Zen.Barcode.Code128BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                var barcodeImage = barcode.Draw(receipt.BillNo, 50);
                barcodeImage.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\barCode.png");

                BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\barCode.png", 350));
                BytesValue = PrintExtensions.AddBytes(BytesValue, "\n\n");
            }

            if (!string.IsNullOrEmpty(receipt.CustomerCode))
            {
                Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                var image = qrCode.Draw(receipt.CustomerCode, 50);
                image.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\qrCode.png");

                BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\qrCode.png", 150));
                //BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(receipt.CustomerCode));
                //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.QrCode.Print(receipt.CustomerCode, PrinterUtility.Enums.QrCodeSize.Grande));
                BytesValue = PrintExtensions.AddBytes(BytesValue, "\n");
            }

            BytesValue = EsponPrintExtention.GetConfiguration(obj, BytesValue);
            BytesValue = PrintExtensions.AddBytes(BytesValue, "------------Thank you for coming----------\n");
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Left());
            BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.CutPage());

            LogFile.WriteAllBytes(FileNames.PrintReceiptExchangeReview, BytesValue);

            return BytesValue;
        }

        private byte[] AddPayments(byte[] bytesValue, IList<ReceiptPayment> salePayments, double exchange)
        {
            if (salePayments == null || salePayments.Count == 0)
            {
                return bytesValue;
            }

            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            int amountPosition = 38;
            foreach (var receiptPayment in salePayments)
            {
                if (receiptPayment.Amount != 0)
                {
                    var dataString = " {0}" + "{1," + (amountPosition - receiptPayment.Name.Length).ToString() + "}($)\n";
                    bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(string.Format(dataString, receiptPayment.Name, string.Format("{0:0.00}", receiptPayment.Amount))));
                }
            }

            if (exchange != 0)
            {
                bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
                bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(string.Format(" Change:{0,31}($)\n", string.Format("{0:0.00}", exchange))));
            }

            return bytesValue;
        }

        private byte[] AddReceiptInfo(byte[] bytesValue, ReceiptExchange receipt)
        {
            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes("TOG Connection Pte Ltd\n"));
            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes($"{receipt.StoreName}\n"));
            if (!string.IsNullOrEmpty(receipt.StoreAddress))
            {
                bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes($"{receipt.StoreAddress}\n\n"));
            }
            else
            {
                bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes($"\n"));
            }

            if (!string.IsNullOrEmpty(receipt.CustomerName))
            {
                bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes($"Name:  {receipt.CustomerName}\n"));
            }
            if (!string.IsNullOrEmpty(receipt.CustomerPhoneNumber))
            {
                bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes($"Phone: {receipt.CustomerPhoneNumber}\n"));
            }
            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));

            bytesValue = PrintExtensions.AddBytes(bytesValue,
                Encoding.ASCII.GetBytes(string.Format("BillNo: {0}\n", receipt.BillNo)));

            if (!string.IsNullOrEmpty(receipt.PreviousBillNo))
            {
                bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(string.Format("BillNo: {0} (Previous)\n", receipt.PreviousBillNo)));
            }

            if (!string.IsNullOrEmpty(receipt.NextBillNo))
            {
                bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(string.Format("BillNo: {0} (Next)\n", receipt.NextBillNo)));
            }

            bytesValue = PrintExtensions.AddBytes(bytesValue,
                Encoding.ASCII.GetBytes(string.Format("Date: {0}\n", receipt.CreatedDate.ToLocalTime().ToString("dd/MM/yyyy HH:mm"))));

            bytesValue = PrintExtensions.AddBytes(bytesValue,
                Encoding.ASCII.GetBytes(string.Format("Cashier: {0}\n", receipt.CashierName)));

            if (!string.IsNullOrEmpty(receipt.CounterName))
            {
                bytesValue = PrintExtensions.AddBytes(bytesValue,
                Encoding.ASCII.GetBytes(string.Format("Counter: {0}\n", receipt.CounterName)));
            }

            return bytesValue;
        }

        private byte[] AddOrderItems(byte[] bytesValue, ReceiptExchange receipt)
        {
            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes("No  Item        Qty    Net($)     Total($)\n"));
            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));

            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(" New Items($)\n"));
            bytesValue = getOrderItemsByteArray(bytesValue, receipt.NewItems);

            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(" Purchased Items($)\n"));
            bytesValue = getOrderItemsByteArray(bytesValue, receipt.PurchasedItems);

            bytesValue = PrintExtensions.AddBytes(bytesValue, Encoding.ASCII.GetBytes(" Exchanged Items($)\n"));
            bytesValue = getOrderItemsByteArray(bytesValue, receipt.ExchangedItems);

            return bytesValue;
        }

        private byte[] getOrderItemsByteArray(byte[] BytesValue, IList<ReceiptItem> receiptItems, int proportion = 1)
        {
            if (receiptItems.Any())
            {
                foreach (var item in receiptItems)
                {
                    var splitNames = EsponPrintExtention.SpitVariantName(item.Name, 38);
                    if (splitNames.Count == 0)
                    {
                        BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0} {1,7}\n", item.No, ""));
                    }
                    else
                    {
                        BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0}{1,7}\n", item.No, splitNames[0]));
                        for (int i = 1; i < splitNames.Count; i++)
                        {
                            BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {1,7}\n", item.No, splitNames[i]));
                        }
                    }

                    BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,-16}\n", item.SKUCode));
                    BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0,19}{1,9}{2,14}\n",
                                                                                    item.Quantity, string.Format("{0:0.00}", item.Price),
                                                                                    string.Format("{0:0.00}", item.Amount)));

                    var receiptItemPromotions = item.ReceiptItemPromotions;
                    if (receiptItemPromotions != null && receiptItemPromotions.Any())
                    {
                        foreach (var receiptItemPromotion in receiptItemPromotions)
                        {
                            if (receiptItemPromotion.DiscountType == Enums.DiscountType.Money)
                            {
                                BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,18}{1,22}\n",
                                    receiptItemPromotion.Description, string.Format("{0:0.00}", receiptItemPromotion.Amount)));
                            }

                            if (receiptItemPromotion.DiscountType == Enums.DiscountType.Percent)
                            {
                                BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,18}{1,11}(%){2,8}\n",
                                    receiptItemPromotion.Description, string.Format("{0:0.00}", receiptItemPromotion.Value), string.Format("{0:0.00}", receiptItemPromotion.Amount)));
                            }
                        }
                    }
                }
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            }

            return BytesValue;
        }

    }

}