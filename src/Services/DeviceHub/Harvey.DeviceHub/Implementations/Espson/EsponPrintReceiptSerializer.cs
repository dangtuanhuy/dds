﻿using Harvey.DeviceHub.Extensions;
using Harvey.DeviceHub.Log;
using Harvey.DeviceHub.Models.Receipts;
using Newtonsoft.Json;
using PrinterUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.DeviceHub.Implementations.Espson
{
    public class EsponPrintReceiptSerializer : ISerializer<byte[]>
    {
        public byte[] Serialize(string data)
        {
            LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptLog), "Print receipt", "Start printing receipt!");

            var receipt = JsonConvert.DeserializeObject<Receipt>(data);
            byte[] result = null;
            try
            {
                result = GenerateReceipt(receipt);
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptLog), "Print receipt", "Serialize data to byte array success!");
            }
            catch (Exception e)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintReceiptLog), "Print receipt", "Serialize data to byte array fail!");
                throw;
            }

            return result;
        }

        private byte[] GenerateReceipt(Receipt receipt)
        {
            var togLogoPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\logo.png";

            PrinterUtility.EscPosEpsonCommands.EscPosEpson obj = new PrinterUtility.EscPosEpsonCommands.EscPosEpson(35);

            var BytesValue = Encoding.ASCII.GetBytes(string.Empty);
            BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(togLogoPath, 350));
            BytesValue = EsponPrintExtention.GetConfiguration(obj, BytesValue);
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("TOG Connection Pte Ltd\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"{receipt.StoreName}\n"));
            if (!string.IsNullOrEmpty(receipt.StoreAddress))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"{receipt.StoreAddress}\n\n"));
            }
            else
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"\n"));
            }

            if (!string.IsNullOrEmpty(receipt.CustomerName))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"Name:  {receipt.CustomerName}\n"));
            }
            if (!string.IsNullOrEmpty(receipt.CustomerPhoneNumber))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"Phone: {receipt.CustomerPhoneNumber}\n"));
            }
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));

            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes(string.Format("BillNo: {0}\n", receipt.BillNo)));

            if (!string.IsNullOrEmpty(receipt.PreviousBillNo))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("BillNo: {0} (Previous)\n", receipt.PreviousBillNo)));
            }

            if (!string.IsNullOrEmpty(receipt.NextBillNo))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("BillNo: {0} (Next)\n", receipt.NextBillNo)));
            }

            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes(string.Format("Date: {0}\n", receipt.CreatedDate.ToLocalTime().ToString("dd/MM/yyyy HH:mm"))));

            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes(string.Format("Cashier: {0}\n", receipt.CashierName)));

            if (!string.IsNullOrEmpty(receipt.CounterName))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes(string.Format("Counter: {0}\n", receipt.CounterName)));
            }

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("No  Item        Qty    Net($)     Total($)\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));

            BytesValue = PrintExtensions.AddBytes(BytesValue, PrintItems(receipt.Items));

            BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.PrintPromotions(receipt.SalePromotions));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Total Amount:{0,25}($)\n", string.Format("{0:0.00}", receipt.TotalAmount))));
            if (receipt.GstInclusive)
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($" (GST S$ {string.Format("{0:0.00}", receipt.GstInclusiveAmount)} Inclusive)\n"));
            }
            else
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" GST {0,21} ($)\n", string.Format("{0:0.00}", receipt.GstAmount))));
            }


            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Net Qty: {0,3}      Net Total: {1,9}($)\n", receipt.NetQuantity,
                                                                                                                            string.Format("{0:0.00}", receipt.NetTotal))));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));

            int amountPosition = 38;
            foreach (var receiptPayment in receipt.SalePayments)
            {
                if (receiptPayment.Amount != 0)
                {
                    var dataString = " {0}" + "{1," + (amountPosition - receiptPayment.Name.Length).ToString() + "}($)\n";
                    BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(dataString, receiptPayment.Name, string.Format("{0:0.00}", receiptPayment.Amount))));
                }
            }

            if (receipt.Change != 0)
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" Change:{0,31}($)\n", string.Format("{0:0.00}", receipt.Change))));
            }

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"Retain Your Receipt for Exchange\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"within {receipt.ExchangeDayQuantity} Days For Selected Items Only!\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue,
                Encoding.ASCII.GetBytes($"Thanks! Please Come again\n"));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Lf());
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.CharSize.DoubleHeight6());
            BytesValue = PrintExtensions.AddBytes(BytesValue, "\n\n");

            if (!string.IsNullOrEmpty(receipt.BillNo))
            {
                Zen.Barcode.Code128BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                var barcodeImage = barcode.Draw(receipt.BillNo, 50);
                barcodeImage.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\barCode.png");

                BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\barCode.png", 350));
                BytesValue = PrintExtensions.AddBytes(BytesValue, "\n\n");
            }

            if (!string.IsNullOrEmpty(receipt.CustomerCode))
            {
                Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                var image = qrCode.Draw(receipt.CustomerCode, 50);
                image.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\qrCode.png");

                BytesValue = PrintExtensions.AddBytes(BytesValue, EsponPrintExtention.GetLogo(System.AppDomain.CurrentDomain.BaseDirectory + "\\Images\\qrCode.png", 150));
                //BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(receipt.CustomerCode));
                //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.QrCode.Print(receipt.CustomerCode, PrinterUtility.Enums.QrCodeSize.Grande));
                BytesValue = PrintExtensions.AddBytes(BytesValue, "\n");
            }

            BytesValue = EsponPrintExtention.GetConfiguration(obj, BytesValue);
            BytesValue = PrintExtensions.AddBytes(BytesValue, "------------Thank you for coming----------\n");
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Left());
            BytesValue = PrintExtensions.AddBytes(BytesValue, CutPage());

            LogFile.WriteAllBytes(FileNames.PrintReceiptReview, BytesValue);

            return BytesValue;
        }

        public byte[] CutPage()
        {
            List<byte> oby = new List<byte>();
            oby.Add(Convert.ToByte(Convert.ToChar(0x1D)));
            oby.Add(Convert.ToByte('V'));
            oby.Add((byte)66);
            oby.Add((byte)3);
            return oby.ToArray();
        }

        private List<string> SpitName(string name, int wordQuantityOnRow)
        {
            List<string> result = new List<string>();
            if (string.IsNullOrEmpty(name))
            {
                return result;
            }

            var namesWithSpace = name.Split(' ');
            var currentName = "";
            foreach (var nameBetweenSpace in namesWithSpace)
            {
                if (currentName.Length + nameBetweenSpace.Length + 1 < wordQuantityOnRow)
                {
                    currentName += " " + nameBetweenSpace;
                }
                else
                {
                    var nameWithHorizontalLine = nameBetweenSpace.Split('-');
                    var isSpaceCharacter = true;
                    for (int i = 0; i < nameWithHorizontalLine.Length; i++)
                    {
                        if (currentName.Length + nameWithHorizontalLine[i].Length + 1 >= wordQuantityOnRow)
                        {
                            result.Add(currentName);
                            currentName = ""; 
                        }

                        if (isSpaceCharacter)
                        {
                            currentName += " " + nameWithHorizontalLine[i];
                            isSpaceCharacter = false;
                        }
                        else
                        {
                            currentName += "-" + nameWithHorizontalLine[i];
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(currentName))
            {
                result.Add(currentName);
            }

            return result;
        }

        private byte[] PrintItems(IList<ReceiptItem> items)
        {
            var BytesValue = Encoding.ASCII.GetBytes(string.Empty);
            if (items.Any())
            {
                foreach (var item in items)
                {
                    //BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0} {1,7}\n", item.No, item.Name));
                    var splitNames = SpitName(item.Name, 38);
                    if (splitNames.Count == 0)
                    {
                        BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0} {1,7}\n", item.No, ""));
                    }
                    else
                    {
                        BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0}{1,7}\n", item.No, splitNames[0]));
                        for (int i = 1; i < splitNames.Count; i++)
                        {
                            BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {1,7}\n", item.No, splitNames[i]));
                        }
                    }

                    BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,-16}\n", item.SKUCode));
                    BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("{0,19}{1,9}{2,14}\n",
                                                                                    item.Quantity, string.Format("{0:0.00}", item.Price),
                                                                                    string.Format("{0:0.00}", item.Amount)));

                    var receiptItemPromotions = item.ReceiptItemPromotions;
                    if (receiptItemPromotions != null && receiptItemPromotions.Any())
                    {
                        foreach (var receiptItemPromotion in receiptItemPromotions)
                        {
                            if (receiptItemPromotion.DiscountType == Enums.DiscountType.Money)
                            {
                                BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,18}{1,22}\n",
                                    receiptItemPromotion.Description, string.Format("{0:0.00}", receiptItemPromotion.Amount)));
                            }

                            if (receiptItemPromotion.DiscountType == Enums.DiscountType.Percent)
                            {
                                BytesValue = PrintExtensions.AddBytes(BytesValue, string.Format("  {0,18}{1,11}(%){2,8}\n",
                                    receiptItemPromotion.Description, string.Format("{0:0.00}", receiptItemPromotion.Value), string.Format("{0:0.00}", receiptItemPromotion.Amount)));
                            }
                        }
                    }
                }
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(EsponPrintExtention.GetSeparator()));
            }

            return BytesValue;
        }

    }
}