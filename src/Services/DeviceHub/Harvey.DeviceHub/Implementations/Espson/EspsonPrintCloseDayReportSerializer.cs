﻿using Harvey.DeviceHub.Log;
using Harvey.DeviceHub.Models.CloseDayReports;
using Newtonsoft.Json;
using PrinterUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Harvey.DeviceHub.Implementations.Espson
{
    public class EspsonPrintCloseDayReportSerializer : ISerializer<byte[]>
    {
        public byte[] Serialize(string data)
        {
            LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintCloseDayReportLog), "Print close day report", "Start printing close day report!");

            var closeDayReport = JsonConvert.DeserializeObject<CloseDayReport>(data);
            byte[] result = null;
            try
            {
                result = GenerateReceipt(closeDayReport);
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintCloseDayReportLog), "Print close day report", "Serialize data to byte array success!");
            }
            catch (Exception e)
            {
                LogFile.WriteFile(FileNames.GetFileNameByData(FileNames.PrintCloseDayReportLog), "Print close day report", "Serialize data to byte array fail!");
                throw;
            }

            return result;
        }

        private byte[] GenerateReceipt(CloseDayReport closeDayReport)
        {
            PrinterUtility.EscPosEpsonCommands.EscPosEpson obj = new PrinterUtility.EscPosEpsonCommands.EscPosEpson(35);

            var BytesValue = Encoding.ASCII.GetBytes(string.Empty);
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.CharSize.Nomarl());
            //BytesValue = PrintExtensions.AddBytes(BytesValue, obj.CharSize.DoubleWidth6());
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.FontSelect.FontA());
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("TOG Connection Pte Ltd\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("Cashier settlement\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));

            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Left());
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"Location          {closeDayReport.Location}\n"));
            //BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"Terminal        {closeDayReport.Terminal}\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"Cashier           {closeDayReport.Cashier}\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes($"Settlement Date   {closeDayReport.SettlementDate.ToString("dd/MM/yyyy HH:mm")}\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("Currency     |    Qty    |        Amount  \n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));

            foreach (var item in closeDayReport.Currencies.OrderByDescending(x => x.Value))
            {
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(" {0,-10}{1,3}{2,7}{3,5}{4,14} \n"
                                                                                        , string.Format("{0:0.00}", item.Value), "|",
                                                                                        item.Quantity, "|",
                                                                                        string.Format("{0:0.00}", item.Amount))));
            }
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("Total Cash{0,29}\n", string.Format("{0:0.00}", closeDayReport.TotalCurrencies))));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("POS CASH SALES{0,25}\n", string.Format("{0:0.00}", closeDayReport.PosCashSales))));

            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Center());
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("Paymode Details\n"));

            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Left());
            int amountPosition = 39;
            var enteredCashString = "{0}" + "{1," + (amountPosition - ("CASH").Length).ToString() + "}\n";
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(enteredCashString, "CASH", string.Format("{0:0.00}", closeDayReport.TotalCurrencies))));
            foreach (var item in closeDayReport.PaymentModeDetails)
            {
                var dataString = "{0}" + "{1," + (amountPosition - item.Name.Length).ToString() + "}\n";
                BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format(dataString, item.Name, string.Format("{0:0.00}", item.Amount))));
            }
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("Total {0, 33}\n", string.Format("{0:0.00}", closeDayReport.TotalCashAndPaymentModeDetails))));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("Till Amount {0, 27}\n", string.Format("{0:0.00}", closeDayReport.TillAmount))));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("Total Collection {0, 22}\n", string.Format("{0:0.00}", closeDayReport.TotalCollection))));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("Grand Total {0, 27}\n", string.Format("{0:0.00}", closeDayReport.GrandTotal))));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("\n")));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("System Total {0, 26}\n", string.Format("{0:0.00}", closeDayReport.SystemTotal))));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("\n")));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes(string.Format("Shortage / Excess {0, 21}\n", string.Format("{0:0.00}", closeDayReport.ShortageExcess))));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("Cashier Signature\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("\n"));

            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("------------------------------------------\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("Teller Signagure\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, Encoding.ASCII.GetBytes("\n"));
            BytesValue = PrintExtensions.AddBytes(BytesValue, obj.Alignment.Left());
            BytesValue = PrintExtensions.AddBytes(BytesValue, CutPage());

            LogFile.WriteAllBytes(FileNames.PrintCloseDayReportReview, BytesValue);

            return BytesValue;
        }

        public byte[] CutPage()
        {
            List<byte> oby = new List<byte>();
            oby.Add(Convert.ToByte(Convert.ToChar(0x1D)));
            oby.Add(Convert.ToByte('V'));
            oby.Add((byte)66);
            oby.Add((byte)3);
            return oby.ToArray();
        }
    }
}