﻿using Harvey.MarketingAutomation;

namespace Harvey.PurchaseControl.Application.Domain
{
    public class TransactionType : FeedItemBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
