﻿
using Harvey.MarketingAutomation;
using System;

namespace Harvey.PurchaseControl.Application.Domain
{
    public class BarCode : FeedItemBase
    {
        public Guid VariantId { get; set; }
        public string Code { get; set; }
    }
}
