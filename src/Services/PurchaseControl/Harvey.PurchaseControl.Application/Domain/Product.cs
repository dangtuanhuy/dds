﻿using Harvey.Domain;
using Harvey.MarketingAutomation;

namespace Harvey.PurchaseControl.Application.Domain
{
    public class Product : FeedItemBase
    {
        public const string IndexName = "product_index";
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }
    }
}
