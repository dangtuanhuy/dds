﻿using Harvey.Domain;
using Harvey.MarketingAutomation;
using System;

namespace Harvey.PurchaseControl.Application.Domain
{
    public class Variant : FeedItemBase
    {
        public Guid ProductId { get; set; }
        public Guid PriceId { get; set; }
        public string SKUCode { get; set; }
        public string BarCode { get; set; }
    }
}
