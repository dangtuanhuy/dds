﻿
using Harvey.Domain;
using System;

namespace Harvey.PurchaseControl.Application.Domain.PIM
{
    public class PimVariant : EntityBase
    {
        public Guid ProductId { get; set; }
        public Guid PriceId { get; set; }
        public string SKUCode { get; set; }
    }
}
