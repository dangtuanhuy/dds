﻿using Harvey.Domain;

namespace Harvey.PurchaseControl.Application.Domain.PIM
{
    public class PimCategory : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
