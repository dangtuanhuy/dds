﻿using Harvey.Domain;
using Harvey.MarketingAutomation;

namespace Harvey.PurchaseControl.Application.Domain.PIM
{
    public class PimStockType : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
