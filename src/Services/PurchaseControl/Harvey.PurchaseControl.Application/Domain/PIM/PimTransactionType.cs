﻿using Harvey.Domain;

namespace Harvey.PurchaseControl.Application.Domain.PIM
{
    public class PimTransactionType : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
