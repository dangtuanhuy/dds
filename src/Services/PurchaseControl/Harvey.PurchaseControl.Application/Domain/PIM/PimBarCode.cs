﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Domain.PIM
{
    public class PimBarCode : EntityBase
    {
        public Guid VariantId { get; set; }
        public string Code { get; set; }
    }
}
