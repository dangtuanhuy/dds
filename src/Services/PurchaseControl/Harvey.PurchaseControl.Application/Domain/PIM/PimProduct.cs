﻿using System;
using Harvey.Domain;
using Harvey.MarketingAutomation;

namespace Harvey.PurchaseControl.Application.Domain.PIM
{
    public class PimProduct : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? CategoryId { get; set; }
        public Guid FieldTemplateId { get; set; }
        public bool IsDelete { get; set; }
    }
}
