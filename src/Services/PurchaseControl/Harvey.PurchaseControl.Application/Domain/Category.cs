﻿using Harvey.Domain;
using Harvey.MarketingAutomation;

namespace Harvey.PurchaseControl.Application.Domain
{
    public class Category: FeedItemBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
