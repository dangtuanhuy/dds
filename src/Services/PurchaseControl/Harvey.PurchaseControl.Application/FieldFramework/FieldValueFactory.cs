﻿
using Harvey.Domain;
using Harvey.PurchaseControl.Application.Domain.PIM;

namespace Harvey.PurchaseControl.Application.FieldFramework
{
    public class FieldValueFactory
    {
        public static string GetFieldValueFromFieldType(FieldType fieldType, FieldValue field)
        {
            switch (fieldType)
            {
                case FieldType.Checkbox:
                    return field.BooleanValue.ToString();
                case FieldType.EntityReference:
                    return field.EntityReferenceValue?.ToString();
                case FieldType.Numeric:
                    return field.NumericValue.ToString();
                case FieldType.PredefinedList:
                    return field.PredefinedListValue?.ToString();
                case FieldType.RichText:
                    return field.RichTextValue?.ToString();
                case FieldType.Tags:
                    return field.TagsValue?.ToString();
                case FieldType.Text:
                    return field.TextValue?.ToString();
                case FieldType.DateTime:
                    return field.TextValue?.ToString();
                default:
                    return null;
            }
        }
    }
}
