﻿
namespace Harvey.PurchaseControl.Application.Infrastructure.Enums
{
    public enum ApprovalStatus
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3,
        Confirmed = 4,
        None = 5
    }
}
