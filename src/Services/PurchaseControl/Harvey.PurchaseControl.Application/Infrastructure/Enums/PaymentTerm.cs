﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Enums
{
    public enum PaymentTerm
    {
        PT1 = 1,
        PT2 = 2,
        PT3 = 3,
    }
}
