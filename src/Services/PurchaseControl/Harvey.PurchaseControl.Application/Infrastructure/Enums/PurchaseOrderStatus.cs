﻿namespace Harvey.PurchaseControl.Application.Infrastructure.Enums
{
    public enum PurchaseOrderStatus
    {
        OpenOrder = 1,
        Received = 2,
        Invoiced = 3,
        Canceled = 4,
        Draft = 5,
        Pending = 6,
    }
}
