﻿namespace Harvey.PurchaseControl.Application.Infrastructure.Enums
{
    public enum PurchaseOrderType
    {
        DraftPO = 1,
        PurchaseOrder = 2,
        ReturnedOrder = 3,
        DraftRO = 4
    }
}
