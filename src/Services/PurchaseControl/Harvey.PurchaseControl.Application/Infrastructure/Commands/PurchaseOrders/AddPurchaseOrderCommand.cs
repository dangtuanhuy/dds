﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.PurchaseOrders
{
    public class AddPurchaseOrderCommand : ICommand<PurchaseOrderViewModel>
    {
        public Guid Creator { get; set; }
        public PurchaseOrderModel PurchaseOrder { get; set; }
        public AddPurchaseOrderCommand(Guid creator, PurchaseOrderModel purchaseOrder)
        {
            Creator = creator;
            PurchaseOrder = purchaseOrder;
        }
    }

    public class AddPurchaseOrderCommandHandler : ICommandHandler<AddPurchaseOrderCommand, PurchaseOrderViewModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _repository;
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> _poItemRepository;
        private readonly IEfRepository<PurchaseControlDbContext, Vendor> _vendorRepository;
        private readonly IMapper _mapper;
        private PurchaseOrderService _purchaseOrderService;
        public AddPurchaseOrderCommandHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> repository,
                                              IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> poItemRepository,
                                              IEfRepository<PurchaseControlDbContext, Vendor> vendorRepository,
                                              IMapper mapper,
                                              PurchaseOrderService purchaseOrderService)
        {
            _repository = repository;
            _poItemRepository = poItemRepository;
            _vendorRepository = vendorRepository;
            _mapper = mapper;
            _purchaseOrderService = purchaseOrderService;
        }
        public async Task<PurchaseOrderViewModel> Handle(AddPurchaseOrderCommand command)
        {
            var entity = new PurchaseOrder();
            var key = command.PurchaseOrder.Type == PurchaseOrderType.PurchaseOrder || command.PurchaseOrder.Type == PurchaseOrderType.DraftPO ? "PO" : "RO";
            entity.Name = string.IsNullOrEmpty(command.PurchaseOrder.Name) ? await _purchaseOrderService.GeneratePurchaseOrderName(key) : command.PurchaseOrder.Name;
            entity.Description = command.PurchaseOrder.Description;
            entity.VendorId = command.PurchaseOrder.VendorId;
            entity.Type = command.PurchaseOrder.Type;
            entity.Status = PurchaseOrderStatus.Draft;
            entity.CreatedBy = command.Creator;
            entity.UpdatedBy = command.Creator;
            entity.CurrencyId = command.PurchaseOrder.CurrencyId;
            entity.Date = DateTime.Parse(command.PurchaseOrder.Date);

            var result = await _repository.AddAsync(entity);

            foreach (var purchaseOrderItem in command.PurchaseOrder.PurchaseOrderItems)
            {
                var poItem = new PurchaseOrderItem();
                poItem.VariantId = purchaseOrderItem.VariantId;
                poItem.StockTypeId = purchaseOrderItem.StockTypeId;
                poItem.PurchaseOrderId = result.Id;
                poItem.Quantity = purchaseOrderItem.Quantity;
                poItem.CostValue = purchaseOrderItem.CostValue;
                poItem.CurrencyId = purchaseOrderItem.CurrencyId;
                await _poItemRepository.AddAsync(poItem);
            }

            var purchaseViewModel = new PurchaseOrderViewModel()
            {
                Id = result.Id,
                Name = result.Name,
                Description = result.Description,
                Type = result.Type,
                Status = result.Status,
                Date = result.Date,
                UpdatedDate = result.UpdatedDate,
                CreatedDate = result.CreatedDate
            };

            var vendor = await _vendorRepository.GetByIdAsync(result.VendorId);
            if (vendor != null)
            {
                purchaseViewModel.Vendor = vendor.Name;
            }
            return purchaseViewModel;
        }
    }
}
