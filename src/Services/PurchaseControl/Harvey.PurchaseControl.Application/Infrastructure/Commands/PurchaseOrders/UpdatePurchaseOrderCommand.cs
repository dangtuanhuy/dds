﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.PurchaseOrders
{
    public class UpdatePurchaseOrderCommand: ICommand<PurchaseOrderModel>
    {
        public Guid Id { get; set; }
        public Guid Updater { get; set; }
        public PurchaseOrderModel PurchaseOrder { get; set; }
        public UpdatePurchaseOrderCommand(Guid id, Guid updater, PurchaseOrderModel purchaseOrder)
        {
            Id = id;
            Updater = updater;
            PurchaseOrder = purchaseOrder;
        }
    }

    public class UpdatePurchaseOrderCommandHandler : ICommandHandler<UpdatePurchaseOrderCommand, PurchaseOrderModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _repository;
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> _poItemRepository;
        private readonly IMapper _mapper;
        public UpdatePurchaseOrderCommandHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> repository,
                                                 IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> poItemRepository,
                                                 IMapper mapper)
        {
            _repository = repository;
            _poItemRepository = poItemRepository;
            _mapper = mapper;
        }
        public async Task<PurchaseOrderModel> Handle(UpdatePurchaseOrderCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Purchase Order {command.Id} is not presented.");
            }
            entity.UpdatedBy = command.Updater;
            entity.Name = command.PurchaseOrder.Name;
            entity.Description = command.PurchaseOrder.Description;
            entity.VendorId = command.PurchaseOrder.VendorId;
            entity.CurrencyId = command.PurchaseOrder.CurrencyId;
            entity.Date = DateTime.Parse(command.PurchaseOrder.Date);

            var purchaseOrderItems = await _poItemRepository.ListAsync(x => x.PurchaseOrderId == command.Id);

            foreach (var purchaseOrderItem in command.PurchaseOrder.PurchaseOrderItems)
            {
                var poItemEntity = await _poItemRepository.GetByIdAsync(purchaseOrderItem.Id);
                if (poItemEntity != null)
                {
                    poItemEntity.VariantId = purchaseOrderItem.VariantId;
                    poItemEntity.StockTypeId = purchaseOrderItem.StockTypeId;
                    poItemEntity.Quantity = purchaseOrderItem.Quantity;
                    poItemEntity.CostValue = purchaseOrderItem.CostValue;
                    poItemEntity.CurrencyId = purchaseOrderItem.CurrencyId;
                    await _poItemRepository.UpdateAsync(poItemEntity);
                    purchaseOrderItems =  purchaseOrderItems.Where(x => x.Id != purchaseOrderItem.Id);
                }
                else
                {
                    var poItem = new PurchaseOrderItem()
                    {
                        VariantId = purchaseOrderItem.VariantId,
                        StockTypeId = purchaseOrderItem.StockTypeId,
                        Quantity = purchaseOrderItem.Quantity,
                        CostValue = purchaseOrderItem.CostValue,
                        CurrencyId = purchaseOrderItem.CurrencyId,
                        PurchaseOrderId = command.Id
                    };

                    await _poItemRepository.AddAsync(poItem);
                }
            }
            await _poItemRepository.DeleteAsync(purchaseOrderItems.ToList());
            await _repository.UpdateAsync(entity);

            return _mapper.Map<PurchaseOrderModel>(entity);
        }
    }
}
