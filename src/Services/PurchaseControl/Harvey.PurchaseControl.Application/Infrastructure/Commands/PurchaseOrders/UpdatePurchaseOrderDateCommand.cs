﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.PurchaseOrders
{
    public class UpdatePurchaseOrderDateCommand : ICommand<PurchaseOrderModel>
    {
        public Guid Id { get; set; }
        public Guid Updater { get; set; }
        public PurchaseOrderDateModel PurchaseOrder { get; set; }
        public UpdatePurchaseOrderDateCommand(Guid id, Guid updater, PurchaseOrderDateModel purchaseOrder)
        {
            Id = id;
            Updater = updater;
            PurchaseOrder = purchaseOrder;
        }
    }

    public class UpdatePurchaseOrderDateCommandHandler : ICommandHandler<UpdatePurchaseOrderDateCommand, PurchaseOrderModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _repository;
        private readonly IMapper _mapper;
        public UpdatePurchaseOrderDateCommandHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<PurchaseOrderModel> Handle(UpdatePurchaseOrderDateCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Purchase Order {command.Id} is not presented.");
            }

            entity.Date = DateTime.Parse(command.PurchaseOrder.Date);

            await _repository.UpdateAsync(entity);
            return _mapper.Map<PurchaseOrderModel>(entity);
        }
    }
}
