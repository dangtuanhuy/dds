﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using System;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.PurchaseOrders
{
    public class SubmitPurchaseOrderCommand : ICommand<bool>
    {
        public Guid PurchaseOrderId { get; set; }
        public Guid Updater { get; set; }
        public Guid ManagerId { get; set; }
        public SubmitPurchaseOrderCommand(Guid purchaseOrderId, Guid updater, Guid managerId)
        {
            PurchaseOrderId = purchaseOrderId;
            Updater = updater;
            ManagerId = managerId;
        }
    }

    public class SubmitPurchaseOrderCommandHandler : ICommandHandler<SubmitPurchaseOrderCommand, bool>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _purchaseOrderRepository;
        private readonly IEfRepository<PurchaseControlDbContext, POApproval> _approvalRepository;
        public SubmitPurchaseOrderCommandHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> purchaseOrderRepository,
                                                IEfRepository<PurchaseControlDbContext, POApproval> approvalRepository)
        {
            _purchaseOrderRepository = purchaseOrderRepository;
            _approvalRepository = approvalRepository;
        }
        public async Task<bool> Handle(SubmitPurchaseOrderCommand command)
        {
            var purchaseOrder = await _purchaseOrderRepository.GetByIdAsync(command.PurchaseOrderId);

            if(purchaseOrder == null)
            {
                throw new ArgumentException($"Purchase Order {command.PurchaseOrderId} is not presented.");
            }

            if (purchaseOrder.Status != PurchaseOrderStatus.OpenOrder && purchaseOrder.ApprovalStatus != ApprovalStatus.Rejected)
            {
                throw new ArgumentException($"Cannot submit purchase order. Please try again.");
            }

            purchaseOrder.Status = PurchaseOrderStatus.Pending;
            purchaseOrder.ApprovalStatus = ApprovalStatus.Pending;
            purchaseOrder.UpdatedBy = command.Updater;
            await _purchaseOrderRepository.UpdateAsync(purchaseOrder);

            var approval = new POApproval()
            {
                PurchaseOrderId = command.PurchaseOrderId,
                PurchaseOrderName = purchaseOrder.Name,
                PONumber = purchaseOrder.PONumber,
                Status = ApprovalStatus.Pending,
                ManagerId = command.ManagerId,
                CreatedBy = command.Updater,
                UpdatedBy = command.Updater
            };

            await _approvalRepository.AddAsync(approval);
            return true;
        }
    }
}

