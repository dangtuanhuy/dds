﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using System;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.PurchaseOrders
{
    public class DeletePurchaseOrderCommand: ICommand<bool>
    {
        public Guid Id { get; set; }
        public DeletePurchaseOrderCommand(Guid id)
        {
            Id = id;
        }
    }

    public class DeletePurchaseOrderCommandHandler : ICommandHandler<DeletePurchaseOrderCommand, bool>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _repository;
        private readonly IEventBus _eventBus;
        public DeletePurchaseOrderCommandHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> repository,
            IEventBus eventBus)
        {
            _repository = repository;
            _eventBus = eventBus;
        }
        public async Task<bool> Handle(DeletePurchaseOrderCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"PurchaseOrder {command.Id} is not presented.");
            }
            if (entity.Status != PurchaseOrderStatus.Draft)
            {
                throw new ArgumentException($"Cannot delete purchase order. Please try again.");
            }
            await _repository.DeleteAsync(entity);

            await _eventBus.PublishAsync(new PurchaseOrderDeletedEvent()
            {
                Id = command.Id
            });
            return true;
        }
    }
}
