﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.PurchaseOrders
{
    public class ChangeStatusPurchaseOrderFromDraftCommnand : ICommand<PurchaseOrderViewModel>
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public Guid Updater { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public ChangeStatusPurchaseOrderFromDraftCommnand(Guid id, string text, Guid updater, PurchaseOrderStatus status, ApprovalStatus approvalStatus)
        {
            Id = id;
            Text = text;
            Updater = updater;
            Status = status;
            ApprovalStatus = approvalStatus;
        }
    }

    public class ChangeStatusPurchaseOrderFromDraftCommnandHandler : ICommandHandler<ChangeStatusPurchaseOrderFromDraftCommnand, PurchaseOrderViewModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _repository;
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> _poItemRepository;
        private readonly IEventBus _eventBus;
        private readonly PurchaseControlDbContext _purchaseOrderDbContext;
        private readonly INextSequenceService<TransientPurchaseControlDbContext> _nextSequenceService;
        public ChangeStatusPurchaseOrderFromDraftCommnandHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> repository,
                                                  IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> poItemRepository,
                                                  IEventBus eventBus,
                                                  PurchaseControlDbContext purchaseOrderDbContext,
                                                  INextSequenceService<TransientPurchaseControlDbContext> nextSequenceService)
        {
            _repository = repository;
            _poItemRepository = poItemRepository;
            _eventBus = eventBus;
            _purchaseOrderDbContext = purchaseOrderDbContext;
            _nextSequenceService = nextSequenceService;
        }

        public async Task<PurchaseOrderViewModel> Handle(ChangeStatusPurchaseOrderFromDraftCommnand command)
        {
            var purchaseOrder = await _repository.GetByIdAsync(command.Id);
            if (purchaseOrder.Type != PurchaseOrderType.DraftPO && purchaseOrder.Type != PurchaseOrderType.DraftRO)
            {
                throw new ArgumentException($"Cannot {command.Text} purchase order. Please try again.");
            }

            if (purchaseOrder.VendorId == Guid.Empty)
            {
                throw new ArgumentException($"Vendor is required.");
            }
            var purchaseOrderItems = await _poItemRepository.ListAsync(x => x.PurchaseOrderId == command.Id);
            if (purchaseOrderItems.ToList().Count <= 0)
            {
                throw new ArgumentException($"Purchase order item is required.");
            }

            foreach (var purchaseOrderItem in purchaseOrderItems)
            {
                if (purchaseOrderItem.VariantId == Guid.Empty)
                {
                    throw new ArgumentException($"Variant is required.");
                }
                if (purchaseOrderItem.StockTypeId == Guid.Empty)
                {
                    throw new ArgumentException($"StockType is required.");
                }
                if (purchaseOrderItem.Quantity <= 0)
                {
                    throw new ArgumentException($"Quantity must greater than 0.");
                }
                if (purchaseOrderItem.CostValue <= 0)
                {
                    throw new ArgumentException($"Cost value must greater than 0.");
                }
            }

            if (purchaseOrder.Type == PurchaseOrderType.DraftPO)
            {
                purchaseOrder.Type = PurchaseOrderType.PurchaseOrder;
            }

            if (purchaseOrder.Type == PurchaseOrderType.DraftRO)
            {
                purchaseOrder.Type = PurchaseOrderType.ReturnedOrder;
            }

            if (command.ApprovalStatus != ApprovalStatus.None)
            {
                purchaseOrder.ApprovalStatus = command.ApprovalStatus;
                if (command.ApprovalStatus == ApprovalStatus.Confirmed)
                {
                    var po = await _repository.GetByIdAsync(command.Id);
                    var currencies = _purchaseOrderDbContext.Currencies.AsNoTracking().ToList();
                    if (po != null)
                    {
                        var poItems = await _poItemRepository.GetAsync();
                        poItems = poItems.Where(x => x.PurchaseOrderId == po.Id).ToList();

                        var eventItems = poItems.Select(x => new PurchaseOrderItemEventModel()
                        {
                            Id = x.Id,
                            StockTypeId = x.StockTypeId,
                            VariantId = x.VariantId,
                            PurchaseOrderId = x.PurchaseOrderId,
                            CurrencyCode = currencies.FirstOrDefault(y => y.Id == x.CurrencyId)?.Code,
                            CostValue = x.CostValue,
                            Quantity = x.Quantity
                        }).ToList();
                        await _eventBus.PublishAsync(new PurchaseOrderConfirmStatusEvent()
                        {
                            Id = po.Id,
                            Name = po.Name,
                            Description = po.Description,
                            VendorId = po.VendorId,
                            ApprovalStatus = (int)po.ApprovalStatus,
                            PurchaseOrderItems = eventItems.Cast<dynamic>().ToList(),
                            Type = Convert.ToInt32(po.Type)
                        });
                    }
                }
                else
                {
                    var po = await _poItemRepository.GetByIdAsync(command.Id);

                    if (po != null)
                    {
                        await _eventBus.PublishAsync(new PurchaseOrderRejectStatusEvent()
                        {
                            Id = po.Id
                        });
                    }
                }
            }

            var key = purchaseOrder.Type == PurchaseOrderType.PurchaseOrder || purchaseOrder.Type == PurchaseOrderType.DraftPO ? "PO" : "RO";
            var currentTime = DateTime.UtcNow.ToString("yyMM", System.Globalization.CultureInfo.InvariantCulture);
            var keyName = $"{key}{currentTime}";
            var PONumber = $"{keyName}{_nextSequenceService.Get(keyName)}";

            purchaseOrder.PONumber = purchaseOrder.PONumber == null ? PONumber : purchaseOrder.PONumber;
            purchaseOrder.Status = command.Status;
            purchaseOrder.UpdatedBy = command.Updater;

            await _repository.UpdateAsync(purchaseOrder);
            return new PurchaseOrderViewModel()
            {
                Id = purchaseOrder.Id,
                Status = purchaseOrder.Status,
                Type = purchaseOrder.Type,
                PONumber = purchaseOrder.PONumber,
                ApprovalStatus = purchaseOrder.ApprovalStatus
            };
        }
    }
}
