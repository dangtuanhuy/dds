﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Vendors;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.Vendors
{
    public sealed class AddVendorCommand : ICommand<VendorViewModel>
    {
        public Guid Creator { get;}
        public VendorViewModel Vendor { get;}
        public AddVendorCommand(
            Guid creator,
            VendorViewModel vendor)
        {
            Creator = creator;
            Vendor = vendor;
        }
    }
    public sealed class AddVendorCommandHandler : ICommandHandler<AddVendorCommand, VendorViewModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, Vendor> _repository;
        private readonly IMapper _mapper;
        private readonly IEventBus _eventBus;
        private readonly PurchaseControlDbContext _purchaseControlDbContext;
        public AddVendorCommandHandler(IEventBus eventBus, IEfRepository<PurchaseControlDbContext, Vendor> repository, IMapper mapper, PurchaseControlDbContext purchaseControlDbContext)
        {
            _repository = repository;
            _mapper = mapper;
            _eventBus = eventBus;
            _purchaseControlDbContext = purchaseControlDbContext;
        }
        public async Task<VendorViewModel> Handle(AddVendorCommand command)
        {
            var vendor =  new Vendor
            {
                UpdatedBy = command.Creator,
                CreatedBy = command.Creator,
                VendorCode = command.Vendor.VendorCode,
                Name = command.Vendor.Name,
                Description = command.Vendor.Description,
                Address1 = command.Vendor.Address1,
                Address2 = command.Vendor.Address2,
                Email = command.Vendor.Email,
                VendorUrl = command.Vendor.VendorUrl,
                BarCode = command.Vendor.BarCode,
                Country = command.Vendor.Country,
                CityName = command.Vendor.CityName,
                CityCode = command.Vendor.CityCode,
                Phone = command.Vendor.Phone,
                PaymentTermId = command.Vendor.PaymentTermId,
                CurrencyId = command.Vendor.CurrencyId,
                TaxTypeId = command.Vendor.TaxTypeId,
                ZipCode = command.Vendor.ZipCode,
                Fax = command.Vendor.Fax,
                Attention = command.Vendor.Attention,
                Active = true
            };

            var entity = await _repository.AddAsync(vendor);

            await _eventBus.PublishAsync(new VendorCreatedEvent()
            {
                Id = entity.Id,
                Name = entity.Name,
                Active = entity.Active,
                Description = entity.Description,
                Address1 = entity.Address1,
                Address2 = entity.Address2,
                Email = entity.Email,
                VendorUrl = entity.VendorUrl,
                VendorCode = entity.VendorCode,
                Country = entity.Country,
                CityName = entity.CityName,
                CityCode = entity.CityCode,
                Phone = entity.Phone,
                PaymentTermName = _purchaseControlDbContext.PaymentTerms.FirstOrDefault(x => x.Id == entity.PaymentTermId)?.Name,
                CurrencyName = _purchaseControlDbContext.Currencies.FirstOrDefault(x => x.Id == entity.CurrencyId)?.Name,
                TaxTypeValue = _purchaseControlDbContext.TaxTypes.FirstOrDefault(x => x.Id == entity.TaxTypeId)?.Value,
                ZipCode = entity.ZipCode,
                Fax = entity.Fax
            });

            var vendorViewModel = _mapper.Map<VendorViewModel>(command.Vendor);
            vendorViewModel.Id = entity.Id;
            vendorViewModel.UpdatedDate = entity.UpdatedDate;
            vendorViewModel.CreatedDate = entity.CreatedDate;

            return _mapper.Map<VendorViewModel>(vendorViewModel);
        }
    }
}
