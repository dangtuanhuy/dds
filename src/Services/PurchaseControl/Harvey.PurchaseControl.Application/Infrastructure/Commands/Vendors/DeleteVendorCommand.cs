﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Vendors;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.Vendors
{
    public sealed class DeleteVendorCommand : ICommand<bool>
    {
        public Guid Id { get; }
        public DeleteVendorCommand(Guid id)
        {
            Id = id;
        }
    }

    public sealed class DeleteVendorCommandHandler : ICommandHandler<DeleteVendorCommand, bool>
    {
        private readonly IEfRepository<PurchaseControlDbContext, Vendor> _repository;
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _purchaseOrderRepository;
        private readonly IMapper _mapper;
        private readonly IEventBus _eventBus;
        public DeleteVendorCommandHandler(IEfRepository<PurchaseControlDbContext, Vendor> repository, IMapper mapper,
                                          IEfRepository<PurchaseControlDbContext, PurchaseOrder> purchaseOrderRepository,
                                          IEventBus eventBus)
        {
            _repository = repository;
            _purchaseOrderRepository = purchaseOrderRepository;
            _mapper = mapper;
            _eventBus = eventBus;
        }
        public async Task<bool> Handle(DeleteVendorCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Vendor {command.Id} is not presented.");
            }
            var purchaseOrders = await _purchaseOrderRepository.ListAsync(x => x.VendorId == command.Id);
            if (purchaseOrders.ToList().Count > 0)
            {
                throw new ArgumentException("Vendor is being used.");
            }
            await _repository.DeleteAsync(entity);

            await _eventBus.PublishAsync(new VendorDeletedEvent()
            {
                Id = command.Id,
                Name = entity.Name,
                Active = entity.Active
            });

            return true;
        }
    }
}
