﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Vendors;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.Vendors
{
    public sealed class UpdateVendorCommand : ICommand<VendorViewModel>
    {
        public Guid Updater { get; }
        public Guid Id { get; }
        public VendorViewModel Vendor { get; }

        public UpdateVendorCommand(
            Guid updater,
            Guid id,
            VendorViewModel vendor)
        {
            Updater = updater;
            Id = id;
            Vendor = vendor;
        }
    }
    public sealed class UpdateVendorCommandHandler : ICommandHandler<UpdateVendorCommand, VendorViewModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, Vendor> _repository;
        private readonly IMapper _mapper;
        private readonly IEventBus _eventBus;
        private readonly PurchaseControlDbContext _purchaseControlDbContext;
        public UpdateVendorCommandHandler(IEfRepository<PurchaseControlDbContext, Vendor> repository,
            IMapper mapper,
            IEventBus eventBus,
             PurchaseControlDbContext purchaseControlDbContext)
        {
            _repository = repository;
            _mapper = mapper;
            _eventBus = eventBus;
            _purchaseControlDbContext = purchaseControlDbContext;
        }
        public async Task<VendorViewModel> Handle(UpdateVendorCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Vendor {command.Id} is not presented.");
            }
            entity.UpdatedBy = command.Updater;
            entity.VendorCode = command.Vendor.VendorCode;
            entity.Name = command.Vendor.Name;
            entity.Description = command.Vendor.Description;
            entity.Address1 = command.Vendor.Address1;
            entity.Address2 = command.Vendor.Address2;
            entity.Email = command.Vendor.Email;
            entity.VendorUrl = command.Vendor.VendorUrl;
            entity.BarCode = command.Vendor.BarCode;
            entity.Country = command.Vendor.Country;
            entity.CityName = command.Vendor.CityName;
            entity.CityCode = command.Vendor.CityCode;
            entity.Phone = command.Vendor.Phone;
            entity.PaymentTermId = command.Vendor.PaymentTermId;
            entity.CurrencyId = command.Vendor.CurrencyId;
            entity.TaxTypeId = command.Vendor.TaxTypeId;
            entity.ZipCode = command.Vendor.ZipCode;
            entity.Fax = command.Vendor.Fax;
            entity.Attention = command.Vendor.Attention;
            entity.Active = command.Vendor.Active;
            await _repository.UpdateAsync(entity);

            await _eventBus.PublishAsync(new VendorUpdatedEvent()
            {
                Id = command.Id,
                Name = entity.Name,
                Active = entity.Active,
                Description = entity.Description,
                Address1 = entity.Address1,
                Address2 = entity.Address2,
                Email = entity.Email,
                VendorUrl = entity.VendorUrl,
                VendorCode = entity.VendorCode,
                Country = entity.Country,
                CityName = entity.CityName,
                CityCode = entity.CityCode,
                Phone = entity.Phone,
                PaymentTermName = _purchaseControlDbContext.PaymentTerms.FirstOrDefault(x => x.Id == entity.PaymentTermId)?.Name,
                CurrencyName = _purchaseControlDbContext.Currencies.FirstOrDefault(x => x.Id == entity.CurrencyId)?.Name,
                TaxTypeValue = _purchaseControlDbContext.TaxTypes.FirstOrDefault(x => x.Id == entity.TaxTypeId)?.Value,
                ZipCode = entity.ZipCode,
                Fax = entity.Fax
            });

            var vendorViewModel = _mapper.Map<VendorViewModel>(command.Vendor);
            vendorViewModel.Id = entity.Id;
            vendorViewModel.UpdatedDate = entity.UpdatedDate;
            vendorViewModel.CreatedDate = entity.CreatedDate;

            return _mapper.Map<VendorViewModel>(vendorViewModel);
        }
    }
}
