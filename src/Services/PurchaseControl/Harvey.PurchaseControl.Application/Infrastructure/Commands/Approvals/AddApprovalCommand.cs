﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Commands.Approvals
{
    public class AddApprovalCommand: ICommand<ApprovalModel>
    {
        public Guid Updator { get; }
        public Guid Id { get; set; }
        public Guid ManagerId { get; set; }
        public ApprovalStatus Status { get; set; }
        public string Reason { get; set; }
        public AddApprovalCommand(Guid updator, Guid id, Guid mananerId, ApprovalStatus status, string reason)
        {
            Updator = updator;
            Id = id;
            ManagerId = mananerId;
            Status = status;
            Reason = reason;
        }
    }

    public class AddApprovalCommandHandler: ICommandHandler<AddApprovalCommand, ApprovalModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, POApproval> _approvalRepository;
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _poRepository;
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> _poItemRepository;
        private readonly PurchaseControlDbContext _purchaseOrderDbContext;
        private readonly IMapper _mapper;
        private readonly IEventBus _eventBus;
        public AddApprovalCommandHandler(IEfRepository<PurchaseControlDbContext, POApproval> approvalRepository,
                                        IEfRepository<PurchaseControlDbContext, PurchaseOrder> purchaseOrderRepository,
                                        IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> poItemRepository,
                                        PurchaseControlDbContext purchaseOrderDbContext,
                                        IMapper mapper,
                                        IEventBus eventBus)
        {
            _approvalRepository = approvalRepository;
            _poRepository = purchaseOrderRepository;
            _poItemRepository = poItemRepository;
            _purchaseOrderDbContext = purchaseOrderDbContext;
            _mapper = mapper;
            _eventBus = eventBus;
        }
        public async Task<ApprovalModel> Handle(AddApprovalCommand command)
        {
            var approval = await _approvalRepository.GetByIdAsync(command.Id);

            if (approval == null)
            {
                throw new NotImplementedException();
            }

            var purchase = await _poRepository.GetByIdAsync(approval.PurchaseOrderId);
            purchase.ApprovalStatus = command.Status;

            var newApproval = new POApproval()
            {
                PurchaseOrderId = approval.PurchaseOrderId,
                PurchaseOrderName = approval.PurchaseOrderName,
                ManagerId = command.ManagerId,
                Status = command.Status,
                PONumber = approval.PONumber,
                CreatedBy = approval.CreatedBy,
                UpdatedBy = command.Updator
            };
            if(command.Reason != null)
            {
                newApproval.ReasonRecord = command.Reason;
            }
            var result = await _approvalRepository.AddAsync(newApproval);

            if(command.Status == ApprovalStatus.Confirmed)
            {
                var po = await _poRepository.GetByIdAsync(approval.PurchaseOrderId);
                var currencies = _purchaseOrderDbContext.Currencies.AsNoTracking().ToList();
                if (po!= null)
                {
                    var poItems = await _poItemRepository.GetAsync();
                    poItems = poItems.Where(x => x.PurchaseOrderId == po.Id).ToList();

                    var eventItems = poItems.Select(x => new PurchaseOrderItemEventModel()
                    {
                        Id = x.Id,
                        StockTypeId = x.StockTypeId,
                        VariantId = x.VariantId,
                        PurchaseOrderId = x.PurchaseOrderId,
                        CurrencyCode = currencies.FirstOrDefault(y => y.Id == x.CurrencyId)?.Code,
                        CostValue = x.CostValue,
                        Quantity = x.Quantity
                    }).ToList();
                    await _eventBus.PublishAsync(new PurchaseOrderConfirmStatusEvent()
                    {
                        Id = po.Id,
                        Name = po.Name,
                        Description = po.Description,
                        VendorId = po.VendorId,
                        ApprovalStatus = (int)po.ApprovalStatus,
                        PurchaseOrderItems = eventItems.Cast<dynamic>().ToList(),
                        Type = Convert.ToInt32(po.Type)
                    });
                }
            } else
            {
                var po = await _poRepository.GetByIdAsync(approval.PurchaseOrderId);

                if( po !=null)
                {
                    await _eventBus.PublishAsync(new PurchaseOrderRejectStatusEvent()
                    {
                        Id = po.Id
                    });
                }
            }

            return new ApprovalModel()
            {
                Id = result.Id,
                ManagerId = result.ManagerId,
                Status = result.Status,
                CreatedDate = result.CreatedDate,
                PurchaseOrderId = result.PurchaseOrderId,
                PurchaseOrderName = result.PurchaseOrderName,
                PONumber = result.PONumber,
                CreatedBy = result.CreatedBy,
                UpdatedBy = result.UpdatedBy,
                UpdatedDate = result.UpdatedDate
            };
        }
    }
}
