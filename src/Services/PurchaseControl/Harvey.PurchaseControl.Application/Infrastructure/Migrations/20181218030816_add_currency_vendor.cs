﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PurchaseControl.Application.Infrastructure.Migrations
{
    public partial class add_currency_vendor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CurrencyId",
                table: "Vendors",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_CurrencyId",
                table: "Vendors",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vendors_Currencies_CurrencyId",
                table: "Vendors",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vendors_Currencies_CurrencyId",
                table: "Vendors");

            migrationBuilder.DropIndex(
                name: "IX_Vendors_CurrencyId",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Vendors");
        }
    }
}
