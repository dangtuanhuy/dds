﻿// <auto-generated />
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Harvey.PurchaseControl.Application.Infrastructure.Migrations
{
    [DbContext(typeof(PurchaseControlDbContext))]
    [Migration("20190409111952_add-feed-time")]
    partial class addfeedtime
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Harvey.Domain.AppSetting", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<string>("Key");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.ToTable("AppSettings");
                });

            modelBuilder.Entity("Harvey.Domain.NextSequence", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Key");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("Key")
                        .IsUnique();

                    b.ToTable("NextSequences");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Domain.Category", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Domain.Field", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<Guid>("EntityId");

                    b.Property<Guid>("FieldId");

                    b.Property<string>("FieldName");

                    b.Property<int>("FieldType");

                    b.Property<string>("FieldValue");

                    b.Property<Guid>("FieldValueId");

                    b.Property<bool>("IsVariantField");

                    b.Property<int>("OrderSection");

                    b.Property<string>("Section");

                    b.HasKey("Id");

                    b.ToTable("FieldValues");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Domain.Product", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<string>("Description");

                    b.Property<bool>("IsDelete");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Domain.StockType", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<string>("Code");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("StockTypes");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Domain.TimeToFeed", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("FeedType");

                    b.Property<DateTime>("LastSync");

                    b.HasKey("Id");

                    b.ToTable("TimeToFeeds");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Domain.TransactionType", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<string>("Code");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("TransactionTypes");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Domain.Variant", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<string>("BarCode");

                    b.Property<Guid>("PriceId");

                    b.Property<Guid>("ProductId");

                    b.Property<string>("SKUCode");

                    b.HasKey("Id");

                    b.ToTable("Variants");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.Currency", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("Code")
                        .IsUnique();

                    b.ToTable("Currencies");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.PaymentTerm", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("Code")
                        .IsUnique();

                    b.ToTable("PaymentTerms");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.POApproval", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<Guid>("ManagerId");

                    b.Property<string>("PONumber");

                    b.Property<Guid>("PurchaseOrderId");

                    b.Property<string>("PurchaseOrderName");

                    b.Property<string>("ReasonRecord");

                    b.Property<int>("Status");

                    b.Property<Guid>("UpdatedBy");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.HasIndex("PurchaseOrderId");

                    b.ToTable("POApprovals");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.PurchaseOrder", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ApprovalStatus");

                    b.Property<Guid>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<Guid>("CurrencyId");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("PONumber");

                    b.Property<int>("Status");

                    b.Property<int>("Type");

                    b.Property<Guid>("UpdatedBy");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<Guid>("VendorId");

                    b.HasKey("Id");

                    b.HasIndex("CurrencyId");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("PurchaseOrders");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.PurchaseOrderItem", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<float>("CostValue");

                    b.Property<Guid>("CurrencyId");

                    b.Property<Guid>("PurchaseOrderId");

                    b.Property<int>("Quantity");

                    b.Property<Guid>("StockTypeId");

                    b.Property<Guid>("VariantId");

                    b.HasKey("Id");

                    b.HasIndex("CurrencyId");

                    b.HasIndex("PurchaseOrderId");

                    b.ToTable("PurchaseOrderItems");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.TaxType", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("TaxTypes");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.Vendor", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<string>("Address1");

                    b.Property<string>("Address2");

                    b.Property<string>("Attention");

                    b.Property<string>("BarCode");

                    b.Property<string>("CityCode");

                    b.Property<string>("CityName");

                    b.Property<string>("Country");

                    b.Property<Guid>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<Guid>("CurrencyId");

                    b.Property<string>("Description");

                    b.Property<string>("Email");

                    b.Property<string>("Fax");

                    b.Property<string>("Name");

                    b.Property<Guid>("PaymentTermId");

                    b.Property<string>("Phone");

                    b.Property<Guid>("TaxTypeId");

                    b.Property<Guid>("UpdatedBy");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<string>("VendorCode");

                    b.Property<string>("VendorUrl");

                    b.Property<string>("ZipCode");

                    b.HasKey("Id");

                    b.HasIndex("CurrencyId");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("PaymentTermId");

                    b.HasIndex("TaxTypeId");

                    b.HasIndex("VendorCode")
                        .IsUnique();

                    b.ToTable("Vendors");
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.POApproval", b =>
                {
                    b.HasOne("Harvey.PurchaseControl.Application.Infrastructure.Domain.PurchaseOrder", "PurchaseOrder")
                        .WithMany("Approvals")
                        .HasForeignKey("PurchaseOrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.PurchaseOrder", b =>
                {
                    b.HasOne("Harvey.PurchaseControl.Application.Infrastructure.Domain.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.PurchaseOrderItem", b =>
                {
                    b.HasOne("Harvey.PurchaseControl.Application.Infrastructure.Domain.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Harvey.PurchaseControl.Application.Infrastructure.Domain.PurchaseOrder", "PurchaseOrder")
                        .WithMany("PurchaseOrderItems")
                        .HasForeignKey("PurchaseOrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Harvey.PurchaseControl.Application.Infrastructure.Domain.Vendor", b =>
                {
                    b.HasOne("Harvey.PurchaseControl.Application.Infrastructure.Domain.Currency", "Currency")
                        .WithMany("Vendors")
                        .HasForeignKey("CurrencyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Harvey.PurchaseControl.Application.Infrastructure.Domain.PaymentTerm", "PaymentTerm")
                        .WithMany("Vendors")
                        .HasForeignKey("PaymentTermId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Harvey.PurchaseControl.Application.Infrastructure.Domain.TaxType", "TaxType")
                        .WithMany("Vendors")
                        .HasForeignKey("TaxTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
