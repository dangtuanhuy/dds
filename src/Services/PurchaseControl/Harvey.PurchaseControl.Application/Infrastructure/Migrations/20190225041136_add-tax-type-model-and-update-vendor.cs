﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PurchaseControl.Application.Infrastructure.Migrations
{
    public partial class addtaxtypemodelandupdatevendor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CityCode",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CityName",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TaxTypeId",
                table: "Vendors",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "TaxTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_TaxTypeId",
                table: "Vendors",
                column: "TaxTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vendors_TaxTypes_TaxTypeId",
                table: "Vendors",
                column: "TaxTypeId",
                principalTable: "TaxTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vendors_TaxTypes_TaxTypeId",
                table: "Vendors");

            migrationBuilder.DropTable(
                name: "TaxTypes");

            migrationBuilder.DropIndex(
                name: "IX_Vendors_TaxTypeId",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CityCode",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CityName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "TaxTypeId",
                table: "Vendors");
        }
    }
}
