﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PurchaseControl.Application.Infrastructure.Migrations
{
    public partial class addnextsequencemodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PONumber",
                table: "PurchaseOrders",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "NextSequences",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NextSequences", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NextSequences_Key",
                table: "NextSequences",
                column: "Key",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NextSequences");

            migrationBuilder.DropColumn(
                name: "PONumber",
                table: "PurchaseOrders");
        }
    }
}
