﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PurchaseControl.Application.Infrastructure.Migrations
{
    public partial class UdateBarCodeFieldMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BarCode",
                table: "Variants",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BarCode",
                table: "Variants");
        }
    }
}
