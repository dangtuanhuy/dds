﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PurchaseControl.Application.Infrastructure.Migrations
{
    public partial class AddSKUCodeForVariantMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SKUCode",
                table: "Variants",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SKUCode",
                table: "Variants");
        }
    }
}
