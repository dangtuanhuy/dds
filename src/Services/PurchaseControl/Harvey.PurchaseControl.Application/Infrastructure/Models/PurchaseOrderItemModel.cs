﻿using System;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class PurchaseOrderItemModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public ProductListModel Product { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public float CostValue { get; set; }
        public Guid CurrencyId { get; set; }
        public VariantModel Variant { get; set; }
    }

    public class PurchaseOrderItemEventModel
    {
        public Guid Id { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public float CostValue { get; set; }
        public string CurrencyCode { get; set; }
    }
}
