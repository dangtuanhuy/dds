﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class PaymentTermModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
