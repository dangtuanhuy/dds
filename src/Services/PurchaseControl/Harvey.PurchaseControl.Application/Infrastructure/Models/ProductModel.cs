﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class ProductModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<VariantModel> Variants { get; set; } = new List<VariantModel>();
    }

    public class VariantModel
    {
        public Guid Id { get; set; }
        public List<FieldValueModel> FieldValues { get; set; } = new List<FieldValueModel>();
        public string Code { get; set; }
    }

    public class FieldValueModel
    {
        public string Name { get; set; }
        public List<string> Value { get; set; }
    }

    public class ProductListModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public VariantModel Variant { get; set; }
    }
}
