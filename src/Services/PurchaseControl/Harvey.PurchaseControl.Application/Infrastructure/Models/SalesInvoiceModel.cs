﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class SalesInvoiceModel
    {
        public Guid Id { get; set; }
        public Guid VendorId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public string SoDo { get; set; }
        public List<PurchaseOrderItemModel> PurchaseOrderItems { get; set; }
    }
}
