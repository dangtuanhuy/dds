﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class TaxTypeModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string value { get; set; }
    }
}
