﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class GetPurchaseOrdersByIdsRequestModel
    {
        public List<Guid> Ids { get; set; }
    }
}
