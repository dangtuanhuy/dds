﻿using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class ReturnOrderModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid VendorId { get; set; }
        public PurchaseOrderType Type { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public string Date { get; set; }
        public List<ReturnOrderItemModel> ReturnOrderItems { get; set; }
        public Guid CurrencyId { get; set; }
        public string RONumber { get; set; }
    }

    public class ReturnOrderViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Vendor { get; set; }
        public PurchaseOrderType Type { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public DateTime Date { get; set; }
        public Guid CurrencyId { get; set; }
        public string RONumber { get; set; }
        public Guid VendorId { get; set; }
    }
}
