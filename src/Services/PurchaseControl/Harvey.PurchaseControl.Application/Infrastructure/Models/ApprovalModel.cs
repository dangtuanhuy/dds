﻿using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using System;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class ApprovalModel
    {
        public Guid Id { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public string PurchaseOrderName { get; set; }
        public string PONumber { get; set; }
        public ApprovalStatus Status { get; set; }
        public Guid ManagerId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class RejectPOModel
    {
        public Guid Id { get; set; }
        public Guid ManagerId { get; set; }
        public string Reason { get; set; }
    }
}
