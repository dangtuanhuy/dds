﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class ReturnOrderItemModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public ProductListModel Product { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public float CostValue { get; set; }
        public Guid CurrencyId { get; set; }
        public VariantModel Variant { get; set; }
    }
}
