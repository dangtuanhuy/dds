﻿using Harvey.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.Infrastructure.Models
{
    public class PurchaseOrderModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid VendorId { get; set; }
        public PurchaseOrderType Type { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public string Date { get; set; }
        public List<PurchaseOrderItemModel> PurchaseOrderItems { get; set; }
        public Guid CurrencyId { get; set; }
        public string PONumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class PurchaseOrderDateModel
    {
        public Guid Id { get; set; }
        public string Date { get; set; }
    }

    public class PurchaseOrderViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Vendor { get; set; }
        public PurchaseOrderType Type { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public DateTime Date { get; set; }
        public Guid CurrencyId { get; set; }
        public string PONumber { get; set; }
        public Guid VendorId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class GetPurchaseOrderModel : PagingFilterCriteria
    {
        public string QueryText { get; set; }
        public string Status { get; set; }
        public string Stages { get; set; }
        public string Type { get; set; }
    }

    public class PurchaseOrderCountModel
    {
        public string StatusTitle { get; set; }
        public ApprovalStatus Status { get; set; }
        public int CountNumber { get; set; }
        public string Icon { get; set; }
    }
}
