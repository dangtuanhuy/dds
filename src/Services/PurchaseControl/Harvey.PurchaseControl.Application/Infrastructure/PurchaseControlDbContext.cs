﻿using Harvey.Domain;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace Harvey.PurchaseControl.Application.Infrastructure
{
    public class PurchaseControlDbContext : DbContext
    {
        public DbSet<AppSetting> AppSettings { get; set; }
        public DbSet<Field> FieldValues { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<StockType> StockTypes { get; set; }
        public DbSet<TransactionType> TransactionTypes { get; set; }
        public DbSet<Variant> Variants { get; set; }
        public DbSet<BarCode> BarCodes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<PurchaseOrderItem> PurchaseOrderItems { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<TaxType> TaxTypes { get; set; }
        public DbSet<PaymentTerm> PaymentTerms { get; set; }
        public DbSet<POApproval> POApprovals { get; set; }
        public DbSet<Harvey.Domain.NextSequence> NextSequences { get; set; }
        public DbSet<TimeToFeed> TimeToFeeds { get; set; }
        public PurchaseControlDbContext(DbContextOptions<PurchaseControlDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<AppSetting>());
            Setup(modelBuilder.Entity<Field>());
            Setup(modelBuilder.Entity<Product>());
            Setup(modelBuilder.Entity<StockType>());
            Setup(modelBuilder.Entity<TransactionType>());
            Setup(modelBuilder.Entity<Variant>());
            Setup(modelBuilder.Entity<BarCode>());
            Setup(modelBuilder.Entity<Category>());
            Setup(modelBuilder.Entity<Vendor>());
            Setup(modelBuilder.Entity<PurchaseOrder>());
            Setup(modelBuilder.Entity<PurchaseOrderItem>());
            Setup(modelBuilder.Entity<Currency>());
            Setup(modelBuilder.Entity<PaymentTerm>());
            Setup(modelBuilder.Entity<POApproval>());
            Setup(modelBuilder.Entity<Harvey.Domain.NextSequence>());
            Setup(modelBuilder.Entity<TaxType>());
        }

        public void Setup(EntityTypeBuilder<AppSetting> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }
        public void Setup(EntityTypeBuilder<Field> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }
        public void Setup(EntityTypeBuilder<Product> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }
        public void Setup(EntityTypeBuilder<StockType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }
        public void Setup(EntityTypeBuilder<TransactionType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }
        public void Setup(EntityTypeBuilder<Variant> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }
        public void Setup(EntityTypeBuilder<BarCode> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }
        public void Setup(EntityTypeBuilder<Category> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }
        public void Setup(EntityTypeBuilder<Vendor> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig.HasIndex(x => x.VendorCode).IsUnique();
            entityConfig.HasOne(x => x.TaxType)
                .WithMany(x => x.Vendors)
                .HasForeignKey(x => x.TaxTypeId);
        }
        public void Setup(EntityTypeBuilder<PurchaseOrder> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();

            entityConfig.HasMany(x => x.PurchaseOrderItems)
                .WithOne(x => x.PurchaseOrder)
                .HasForeignKey(x => x.PurchaseOrderId);

            entityConfig.HasMany(x => x.Approvals)
                .WithOne(x => x.PurchaseOrder)
                .HasForeignKey(x => x.PurchaseOrderId);
        }
        public void Setup(EntityTypeBuilder<PurchaseOrderItem> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);

            entityConfig.HasOne(x => x.PurchaseOrder)
               .WithMany(x => x.PurchaseOrderItems)
               .HasForeignKey(x => x.PurchaseOrderId);
        }
        public void Setup(EntityTypeBuilder<Currency> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
            entityConfig.HasMany(x => x.Vendors)
                .WithOne(x => x.Currency)
                .HasForeignKey(x => x.CurrencyId);
        }
        public void Setup(EntityTypeBuilder<TaxType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig.HasMany(x => x.Vendors)
                .WithOne(x => x.TaxType)
                .HasForeignKey(x => x.TaxTypeId);
        }

        public void Setup(EntityTypeBuilder<PaymentTerm> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
            entityConfig.HasMany(x => x.Vendors)
                .WithOne(x => x.PaymentTerm)
                .HasForeignKey(x => x.PaymentTermId);
        }
        public void Setup(EntityTypeBuilder<POApproval> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasOne(x => x.PurchaseOrder)
               .WithMany(x => x.Approvals)
               .HasForeignKey(x => x.PurchaseOrderId);
        }

        public void Setup(EntityTypeBuilder<Harvey.Domain.NextSequence> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Key).IsUnique();
        }
    }

    public class TransientPurchaseControlDbContext : PurchaseControlDbContext
    {
        public TransientPurchaseControlDbContext(DbContextOptions<PurchaseControlDbContext> options) : base(options)
        {
        }
    }
}
