﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.IdentifiedDbContextMigrations
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<IdentifiedEventDbContext>
    {
        public IdentifiedEventDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<IdentifiedEventDbContext>();
            var connectionString = "Server=localhost;port=5432;Database=harveypurchaseorder;UserId=postgres;Password=123456";
            builder.UseNpgsql(connectionString);
            return new IdentifiedEventDbContext(builder.Options);
        }
    }
}
