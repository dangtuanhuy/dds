﻿using Harvey.Polly;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure
{
    public class PurchaseControlDbContextDataSeed
    {
        public async Task SeedAsync(PurchaseControlDbContext context, ILogger<PurchaseControlDbContext> logger)
        {
            var policy = new DataSeedRetrivalPolicy();
            await policy.ExecuteStrategyAsync(logger, () =>
             {
                 using (context)
                 {
                     if (!context.Currencies.Any())
                     {
                         context.Currencies.AddRange(GetPreconfiguredCurrencies());
                     }
                     if (!context.PaymentTerms.Any())
                     {
                         context.PaymentTerms.AddRange(GetPreconfiguredPaymentTerms());
                     }
                     if (!context.TaxTypes.Any())
                     {
                         context.TaxTypes.AddRange(GetPreconfiguredTaxes());
                     }
                     context.SaveChanges();
                 }
             });
        }

        private List<Currency> GetPreconfiguredCurrencies()
        {
            return new List<Currency>()
            {
                new Currency()
                {
                    Id = new Guid("7044EC79-914B-4DDB-8B89-1D90352F2F57"),
                    Code = "SGD",
                    Name  = "Singapore Dollar",
                },
                 new Currency()
                {
                    Id = new Guid("638B58EA-D534-46F6-82BE-B781FAA1586C"),
                    Code = "USD",
                    Name  = "US Dollar",
                },
                  new Currency()
                {
                    Id = new Guid("5348EE4B-7C51-4D9C-A0B6-9D477744CAF3"),
                    Code = "MYN",
                    Name  = "Malaysian Ringgit",
                }
            };
        }

        private List<PaymentTerm> GetPreconfiguredPaymentTerms()
        {
            return new List<PaymentTerm>()
            {
                new PaymentTerm()
                {
                    Id = new Guid("20C17B62-F98B-472B-A8F6-61643BDB44C2"),
                    Code = "PT1",
                    Name  = "COD",
                    Description = ""
                },
                 new PaymentTerm()
                {
                    Id = new Guid("D50422AD-8031-4DE2-BC80-2C9FAAADACA0"),
                    Code = "PT2",
                    Name  = "1 MONTH",
                    Description = ""
                },
                  new PaymentTerm()
                {
                    Id = new Guid("089E5BDD-F687-4646-B0BA-EF355B39ADFA"),
                    Code = "PT3",
                    Name  = "2 MONTH",
                    Description = ""
                }
            };
        }

        private List<TaxType> GetPreconfiguredTaxes()
        {
            return new List<TaxType>()
            {
                new TaxType()
                {
                    Id = new Guid("0A708574-A7E2-4186-B8DB-ABE330F2E77B"),
                    Name = "CAPG",
                    Value = "0"
                },
                 new TaxType()
                {
                    Id = new Guid("499E224D-E000-4145-8008-A6C4856E504C"),
                    Name = "Import",
                    Value = "0"
                },
                  new TaxType()
                {
                    Id = new Guid("056E672E-7E1A-4B83-B9E4-9DFD29D1B26D"),
                    Name = "MRVs",
                    Value = "0"
                },
                  new TaxType()
                {
                    Id = new Guid("43DBA96A-9F34-4430-9111-824994A57AED"),
                    Name = "Yes",
                    Value = "7"
                },
                  new TaxType()
                {
                    Id = new Guid("EDE71CC8-7354-4135-BDC0-EC246342E352"),
                    Name = "No",
                    Value = "0"
                }
            };
        }
    }
}
