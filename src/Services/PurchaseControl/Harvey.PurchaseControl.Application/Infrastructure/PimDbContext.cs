﻿using Harvey.PurchaseControl.Application.Domain.PIM;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace Harvey.PurchaseControl.Application.Infrastructure
{
    public class PimDbContext : DbContext
    {
        public DbSet<PimProduct> Products { get; set; }
        public DbSet<PimVariant> Variants { get; set; }
        public DbSet<PimStockType> StockTypes { get; set; }
        public DbSet<PimTransactionType> TransactionTypes { get; set; }
        public DbSet<Field_FieldTemplate> Field_FieldTemplates { get; set; }
        public DbSet<FieldValue> FieldValues { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<PimCategory> Categories { get; set; }

        public PimDbContext(DbContextOptions<PimDbContext> options) : base(options)
        {

        }
    }
}
