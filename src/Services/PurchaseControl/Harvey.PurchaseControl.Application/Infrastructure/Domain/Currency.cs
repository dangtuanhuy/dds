﻿using Harvey.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.Infrastructure.Domain
{
    public class Currency : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual ICollection<Vendor> Vendors { get; set; }
    }
}
