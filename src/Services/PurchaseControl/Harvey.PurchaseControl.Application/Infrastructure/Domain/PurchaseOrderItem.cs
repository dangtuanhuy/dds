﻿using Harvey.Domain;
using System;

namespace Harvey.PurchaseControl.Application.Infrastructure.Domain
{
    public class PurchaseOrderItem : EntityBase
    {
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public float CostValue { get; set; }
        public Guid CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
