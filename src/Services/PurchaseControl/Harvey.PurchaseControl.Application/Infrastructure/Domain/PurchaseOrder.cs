﻿using Harvey.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.Infrastructure.Domain
{
    public class PurchaseOrder : EntityBase, IAuditable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid VendorId { get; set; }
        public PurchaseOrderType Type { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime Date { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid CurrencyId { get; set; }
        public string PONumber { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual ICollection<POApproval> Approvals { get; set; }
        public virtual ICollection<PurchaseOrderItem> PurchaseOrderItems { get; set; }
    }
}
