﻿using Harvey.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using System;

namespace Harvey.PurchaseControl.Application.Infrastructure.Domain
{
    public class POApproval : EntityBase, IAuditable
    {
        public Guid PurchaseOrderId { get; set; }
        public string PurchaseOrderName { get; set; }
        public string PONumber { get; set; }
        public ApprovalStatus Status { get; set; }
        public Guid ManagerId { get; set; }
        public string ReasonRecord { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
