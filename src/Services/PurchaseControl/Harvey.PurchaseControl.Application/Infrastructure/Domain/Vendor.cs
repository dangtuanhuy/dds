﻿using Harvey.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using System;

namespace Harvey.PurchaseControl.Application.Infrastructure.Domain
{
    public class Vendor : EntityBase, IAuditable
    {
        public string VendorCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Email { get; set; }
        public string VendorUrl { get; set; }
        public string BarCode { get; set; }
        public string Country { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string Phone { get; set; }
        public Guid PaymentTermId { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }
        public Guid CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }
        public Guid TaxTypeId { get; set; }
        public virtual TaxType TaxType { get; set; }
        public string ZipCode { get; set; }
        public string Fax { get; set; }
        public string Attention { get; set; }
        public bool Active { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
