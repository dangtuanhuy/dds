﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PurchaseControl.Application.Infrastructure.Domain
{
    public class TaxType : EntityBase
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public virtual ICollection<Vendor> Vendors { get; set; }
    }
}
