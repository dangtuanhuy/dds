﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure
{
    public class IdentifiedEventDbContextDataSeed
    {
        public Task SeedAsync(IdentifiedEventDbContext context, ILogger<IdentifiedEventDbContextDataSeed> logger)
        {
            return Task.CompletedTask;
        }
    }
}
