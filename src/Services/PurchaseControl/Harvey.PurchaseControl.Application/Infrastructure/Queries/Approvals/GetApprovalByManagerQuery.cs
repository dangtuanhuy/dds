﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Approvals
{
    public class GetApprovalByManagerQuery : IQuery<PagedResult<ApprovalModel>>
    {
        public Guid ManagerId { get; set; }
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public GetApprovalByManagerQuery(Guid managerId, PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            ManagerId = managerId;
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }

    public class GetApprovalByManagerQueryHandler: IQueryHandler<GetApprovalByManagerQuery, PagedResult<ApprovalModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, POApproval> _approvalRepository;
        public GetApprovalByManagerQueryHandler(IEfRepository<PurchaseControlDbContext, POApproval> approvalRepository)
        {
            _approvalRepository = approvalRepository;
        }

        public async Task<PagedResult<ApprovalModel>> Handle(GetApprovalByManagerQuery query)
        {
            var approvalsModel = new List<ApprovalModel>();
            var approvals = new List<POApproval>();
            if (query.QueryText == "" || query.QueryText == null)
            {
                var result = await _approvalRepository.ListAsync(x => x.ManagerId == query.ManagerId, 
                    query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                    query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);
                approvals = result.ToList();
            }
            else
            {
                 var result = await _approvalRepository.ListAsync(x => x.ManagerId == query.ManagerId
                                                               && (x.PurchaseOrderName.ToUpper().Contains(query.QueryText.ToUpper()) || x.PONumber.ToUpper().Contains(query.QueryText.ToUpper())),
                                                               query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                                                               query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);
                approvals = result.ToList();
            }
           
            approvals = approvals.GroupBy(x => x.PurchaseOrderId).Select(x => x.OrderByDescending(y => y.CreatedDate).FirstOrDefault()).ToList();

            var totalPages = approvals.Count();

            if (query.PagingFilterCriteria.Page != 0 && query.PagingFilterCriteria.NumberItemsPerPage != 0)
            {
                approvals = approvals.Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
                      .Take(query.PagingFilterCriteria.NumberItemsPerPage).ToList();
            }

            foreach (var approval in approvals)
            {
                var aprrovalModel = new ApprovalModel()
                {
                   Id = approval.Id,
                   PurchaseOrderId = approval.PurchaseOrderId,
                   PurchaseOrderName = approval.PurchaseOrderName,
                   PONumber = approval.PONumber,
                   Status = approval.Status,
                   ManagerId = approval.ManagerId,
                   CreatedDate = approval.CreatedDate,
                   CreatedBy = approval.CreatedBy,
                   UpdatedBy = approval.UpdatedBy,
                   UpdatedDate = approval.UpdatedDate
                };
                approvalsModel.Add(aprrovalModel);
            }

            return new PagedResult<ApprovalModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = approvalsModel
            };
        }
    }
}
