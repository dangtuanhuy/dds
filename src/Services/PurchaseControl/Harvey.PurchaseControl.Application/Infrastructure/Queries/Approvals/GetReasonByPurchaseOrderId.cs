﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Approvals
{
    public class GetReasonByPurchaseOrderId : IQuery<RejectPOModel>
    {
        public Guid PurchaseOrderId { get; set; }
        public GetReasonByPurchaseOrderId (Guid purchaseOrderId)
        {
            PurchaseOrderId = purchaseOrderId;
        }
    }

    public class GetReasonByPurchaseOrderIdHandler : IQueryHandler<GetReasonByPurchaseOrderId, RejectPOModel>
    {

        private readonly IEfRepository<PurchaseControlDbContext, POApproval> _approvalRepository;
        public GetReasonByPurchaseOrderIdHandler(IEfRepository<PurchaseControlDbContext, POApproval> approvalRepository)
        {
            _approvalRepository = approvalRepository;
        }
        public async Task<RejectPOModel> Handle(GetReasonByPurchaseOrderId query)
        {
            var approvals = await _approvalRepository.ListAsync(x => x.PurchaseOrderId == query.PurchaseOrderId);

            string reason = approvals.ToList().OrderByDescending(x => x.CreatedDate).FirstOrDefault().ReasonRecord;

            return new RejectPOModel()
            {
                Reason = reason
            };
        }
    }
}
