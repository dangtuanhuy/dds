﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Vendors;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Vendors
{
    public sealed class SyncVendorsToPIMQuery : IQuery<bool>
    {
        public SyncVendorsToPIMQuery()
        {
        }
    }

    public sealed class SyncVendorsToPIMQueryHandler : IQueryHandler<SyncVendorsToPIMQuery, bool>
    {
        private readonly PurchaseControlDbContext _purchaseControlDbContext;
        private readonly IEventBus _eventBus;
        public SyncVendorsToPIMQueryHandler(PurchaseControlDbContext purchaseControlDbContext,
            IEventBus eventBus)
        {
            _purchaseControlDbContext = purchaseControlDbContext;
            _eventBus = eventBus;
        }
        public async Task<bool> Handle(SyncVendorsToPIMQuery query)
        {
            var vendors = await _purchaseControlDbContext.Vendors.AsNoTracking().ToListAsync();
            if(vendors.Count > 0)
            {
                foreach(var vendor in vendors)
                {
                    await _eventBus.PublishAsync(new VendorCreatedEvent()
                    {
                        Id = vendor.Id,
                        Name = vendor.Name,
                        Active = vendor.Active,
                        Description = vendor.Description,
                        Address1 = vendor.Address1,
                        Address2 = vendor.Address2,
                        Email = vendor.Email,
                        VendorUrl = vendor.VendorUrl,
                        VendorCode = vendor.VendorCode,
                        Country = vendor.Country,
                        CityName = vendor.CityName,
                        CityCode = vendor.CityCode,
                        Phone = vendor.Phone,
                        PaymentTermName = _purchaseControlDbContext.PaymentTerms.FirstOrDefault(x => x.Id == vendor.PaymentTermId)?.Name,
                        CurrencyName = _purchaseControlDbContext.Currencies.FirstOrDefault(x => x.Id == vendor.CurrencyId)?.Name,
                        TaxTypeValue = _purchaseControlDbContext.TaxTypes.FirstOrDefault(x => x.Id == vendor.TaxTypeId)?.Value,
                        ZipCode = vendor.ZipCode,
                        Fax = vendor.Fax
                    });
                }
                return true;
            }
            return false;
        }
    }
}
