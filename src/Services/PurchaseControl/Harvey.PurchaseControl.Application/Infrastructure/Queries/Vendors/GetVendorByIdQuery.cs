﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Vendors
{
    public class GetVendorByIdQuery : IQuery<VendorModel>
    {
        public Guid Id { get; }
        public GetVendorByIdQuery(Guid id)
        {
            Id = id;
        }
    }
    public sealed class GetVendorByIdQueryHandler : IQueryHandler<GetVendorByIdQuery, VendorModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, Vendor, VendorModel> _repository;
        public GetVendorByIdQueryHandler(IEfRepository<PurchaseControlDbContext, Vendor, VendorModel> repository)
        {
            _repository = repository;
        }
        public async Task<VendorModel> Handle(GetVendorByIdQuery query)
        {
            return await _repository.GetByIdAsync(query.Id);
        }
    }
}
