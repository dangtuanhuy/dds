﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Extensions;
using Harvey.Domain.Constant;
using System.Reflection;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Vendors
{
    public sealed class GetVendorsQuery : IQuery<PagedResult<VendorModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string PaymentTermIds { get; }
        public string CurrencyIds { get; }
        public string TaxTypeIds { get; }
        public string QueryString { get; }
        public GetVendorsQuery(PagingFilterCriteria pagingFilterCriteria, string paymentIds, string currencyIds, string taxtypeIds, string querystring)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            PaymentTermIds = paymentIds;
            CurrencyIds = currencyIds;
            TaxTypeIds = taxtypeIds;
            QueryString = querystring;
        }
    }
    public sealed class GetVendorsQueryHandler : IQueryHandler<GetVendorsQuery, PagedResult<VendorModel>>
    {
        private readonly PurchaseControlDbContext _purchaseControlDbContext;
        private readonly IEfRepository<PurchaseControlDbContext, Vendor> _repository;
        private readonly PurchaseControlDbContext _dbContext;

        public GetVendorsQueryHandler(IEfRepository<PurchaseControlDbContext, Vendor> repository, PurchaseControlDbContext purchaseControlDbContext, PurchaseControlDbContext dbContext)
        {
            _repository = repository;
            _purchaseControlDbContext = purchaseControlDbContext;
            _dbContext = dbContext;
        }
        public async Task<PagedResult<VendorModel>> Handle(GetVendorsQuery query)
        {
            var paymentTermIds = string.IsNullOrEmpty(query.PaymentTermIds) ? null : query.PaymentTermIds.Split("|");
            var currencyIds = string.IsNullOrEmpty(query.CurrencyIds) ? null : query.CurrencyIds.Split("|");
            var taxTypeIds = string.IsNullOrEmpty(query.TaxTypeIds) ? null : query.TaxTypeIds.Split("|");

            var predicate = PredicateBuilder.True<Vendor>();
            if (!string.IsNullOrEmpty(query.QueryString))
            {
                predicate = predicate.And(x => x.Name.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.Description.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.VendorCode.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.Address1.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.Address2.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.Email.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.VendorUrl.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.BarCode.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.Country.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.CityName.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.Phone.ToUpper().Contains(query.QueryString.ToUpper()));
            }
            if (!string.IsNullOrEmpty(query.PaymentTermIds))
            {
                predicate = predicate.And(x => paymentTermIds.Contains(x.PaymentTermId.ToString()));
            }

            if (!string.IsNullOrEmpty(query.CurrencyIds))
            {
                predicate = predicate.And(x => currencyIds.Contains(x.CurrencyId.ToString()));
            }

            if (!string.IsNullOrEmpty(query.TaxTypeIds))
            {
                predicate = predicate.And(x => taxTypeIds.Contains(x.TaxTypeId.ToString()));
            }

            var vendorQuery = _dbContext.Vendors.Where(predicate);

            var resultData = from vendor in vendorQuery
                             join pt in _dbContext.PaymentTerms on vendor.PaymentTermId equals pt.Id into ps
                             from ptJoin in ps.DefaultIfEmpty()
                             join c in _dbContext.Currencies on vendor.CurrencyId equals c.Id into cs
                             from cJoin in cs.DefaultIfEmpty()
                             join t in _dbContext.TaxTypes on vendor.TaxTypeId equals t.Id into ts
                             from tJoin in ts.DefaultIfEmpty()
                             select new VendorViewModel
                             {
                                 Id = vendor.Id,
                                 VendorCode = vendor.VendorCode,
                                 Name = vendor.Name,
                                 Description = vendor.Description,
                                 Address1 = vendor.Address1,
                                 Address2 = vendor.Address2,
                                 Email = vendor.Email,
                                 VendorUrl = vendor.VendorUrl,
                                 BarCode = vendor.BarCode,
                                 Country = vendor.Country,
                                 Phone = vendor.Phone,
                                 CityCode = vendor.CityCode,
                                 CityName = vendor.CityName,
                                 PaymentTermId = vendor.PaymentTermId,
                                 CurrencyId = vendor.CurrencyId,
                                 TaxTypeId = vendor.TaxTypeId,
                                 PaymentTermName = ptJoin.Name,
                                 CurrencyName = cJoin.Name,
                                 TaxTypeName = tJoin.Name,
                                 ZipCode = vendor.ZipCode,
                                 Fax = vendor.Fax,
                                 Attention = vendor.Attention,
                                 Active = vendor.Active,
                                 UpdatedBy = vendor.UpdatedBy,
                                 CreatedBy = vendor.CreatedBy,
                                 UpdatedDate = vendor.UpdatedDate,
                                 CreatedDate = vendor.CreatedDate
                             };

            if (!string.IsNullOrEmpty(query.PagingFilterCriteria.SortColumn))
            {
                resultData = query.PagingFilterCriteria.SortDirection == SortDirection.Ascending
                       ? resultData.OrderBy(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn))
                       : resultData.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn));
            }
            else
            {
                PropertyInfo propertyInfo =
                     resultData.GetType().GetGenericArguments()[0].GetProperty(SortColumnConstants.DefaultSortColumn);

                if (propertyInfo != null)
                {
                    resultData = resultData.OrderByDescending(a => a.UpdatedDate);
                }
                else
                {
                    resultData = resultData.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, SortColumnConstants.SecondSortColumn));
                }
            }

            var entities = resultData
                 .Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
                 .Take(query.PagingFilterCriteria.NumberItemsPerPage)
                 .ToList();

            var totalPages = resultData.Count();

            return new PagedResult<VendorModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = entities
            };
        }
    }
}
