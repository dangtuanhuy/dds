﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Vendors
{
    public class GetAllVendorsQuery: IQuery<List<VendorModel>>
    {
    }

    public class GetAllVendorsQueryHandler : IQueryHandler<GetAllVendorsQuery, List<VendorModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, Vendor> _repository;
        private readonly IMapper _mapper;
        public GetAllVendorsQueryHandler(IEfRepository<PurchaseControlDbContext, Vendor> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<List<VendorModel>> Handle(GetAllVendorsQuery query)
        {
            var result = await _repository.ListAsync(x => x.Active == true);
            return _mapper.Map<List<VendorModel>>(result);
        }
    }
}
