﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Currencies
{
    public sealed class GetTaxTypesQuery : IQuery<List<TaxTypeModel>>
    {
        
    }
    public sealed class GetTaxTypesQueryHandler : IQueryHandler<GetTaxTypesQuery, List<TaxTypeModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, TaxType, TaxTypeModel> _repository;
        public GetTaxTypesQueryHandler(IEfRepository<PurchaseControlDbContext, TaxType, TaxTypeModel> repository)
        {
            _repository = repository;
        }
        public async Task<List<TaxTypeModel>> Handle(GetTaxTypesQuery query)
        {
            var result = await _repository.GetAsync();
            return result.ToList();
        }
    }
}
