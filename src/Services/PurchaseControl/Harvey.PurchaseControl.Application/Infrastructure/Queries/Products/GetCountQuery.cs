﻿using Harvey.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Products
{
    public sealed class GetCountQuery : IQuery<CountModel>
    {
        public GetCountQuery() { }
    }

    public sealed class GetCountQueryHandler : IQueryHandler<GetCountQuery, CountModel>
    {
        private readonly PurchaseControlDbContext _poDbContext;
        public GetCountQueryHandler(PurchaseControlDbContext poDbContext)
        {
            _poDbContext = poDbContext;
        }
        public async Task<CountModel> Handle(GetCountQuery query)
        {
            var productCount = await _poDbContext.Products.AsNoTracking().CountAsync();
            var variantCount = await _poDbContext.Variants.AsNoTracking().CountAsync(); 

            return new CountModel()
            {
                Products = productCount,
                Variants = variantCount
            };
        }
    }
}
