﻿using Harvey.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System.Threading.Tasks;
using Harvey.Search.Abstractions;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using Harvey.PurchaseControl.Application.Infrastructure.Indexing;
using Microsoft.EntityFrameworkCore;
using Harvey.PurchaseControl.Application.Domain;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Products
{
    public sealed class GetProductListQuery : IQuery<List<ProductListModel>>
    {
        public string QueryText { get; }
        public GetProductListQuery(string queryText)
        {
            QueryText = queryText;
        }
    }

    public sealed class GetProductListQueryHandler : IQueryHandler<GetProductListQuery, List<ProductListModel>>
    {
        private readonly ISearchService _searchService;
        private readonly IMapper _mapper;
        private readonly PurchaseControlDbContext _poDbContext;
        public GetProductListQueryHandler(ISearchService searchService,
            IMapper mapper,
            PurchaseControlDbContext poDbContext)
        {
            _searchService = searchService;
            _mapper = mapper;
            _poDbContext = poDbContext;
        }
        public async Task<List<ProductListModel>> Handle(GetProductListQuery query)
        {
            if (string.IsNullOrEmpty(query.QueryText))
            {
                return new List<ProductListModel>();
            }

            var barCode = await _poDbContext.BarCodes.FirstOrDefaultAsync(x => x.Code == query.QueryText);

            if (barCode != null)
            {
                var variantBarcode = await _poDbContext.Variants.FirstOrDefaultAsync(x => x.Id == barCode.VariantId);

                if (variantBarcode != null)
                {
                    return await GetProductByBarcodeSKU(variantBarcode, query);
                }
            }

            var variantSKU = _poDbContext.Variants.FirstOrDefault(x => x.SKUCode == query.QueryText);


            if (variantSKU != null)
            {
                return await GetProductByBarcodeSKU(variantSKU, query);
            }

            var productSearchQuery = new ProductSearchQuery()
            {
                QueryText = query.QueryText,
                NumberItemsPerPage = 0,
                Page = 0
            };


            var result = await _searchService.SearchAsync<ProductSearchItem, ProductSearchResult>(productSearchQuery);

            return result.Results.Select(x => new ProductListModel
            {
                Name = x.Item.Name,
                Id = x.Item.Id,
                Variant = new VariantModel()
            }).ToList();
        }

        public async Task<List<ProductListModel>> GetProductByBarcodeSKU(Variant variant, GetProductListQuery query)
        {
            var data = new List<ProductListModel>();
            var variantModel = new VariantModel();
            variantModel.Id = variant.Id;
            variantModel.Code = query.QueryText;
            var fieldValues = await _poDbContext.FieldValues.Where(x => x.EntityId == variant.Id).OrderBy(x => x.FieldName).ToListAsync();
            foreach (var fieldValue in fieldValues)
            {
                var fieldValueModel = new FieldValueModel()
                {
                    Name = fieldValue.FieldName,
                    Value = fieldValue.FieldValue.Split(',').ToList(),
                };
                variantModel.FieldValues.Add(fieldValueModel);
            }
            var product = _poDbContext.Products.SingleOrDefault(x => x.Id == variant.ProductId);
            if (product != null)
            {
                var productModel = new ProductListModel
                {
                    Name = product.Name,
                    Id = variant.ProductId.ToString(),
                    Variant = variantModel
                };
                data.Add(productModel);
            }
            return data;
        }
    }
}