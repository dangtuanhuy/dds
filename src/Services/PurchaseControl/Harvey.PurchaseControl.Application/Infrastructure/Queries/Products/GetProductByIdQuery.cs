﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Products
{
    public class GetProductByIdQuery : IQuery<ProductModel>
    {
        public readonly Guid Id;
        public GetProductByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetProductByIdQueryHandler : IQueryHandler<GetProductByIdQuery, ProductModel>
    {
        private readonly PurchaseControlDbContext _poDbContext;
        private readonly IProductService _productService;
        private readonly IEfRepository<TransientPurchaseControlDbContext, Product> _repository;
        public GetProductByIdQueryHandler(PurchaseControlDbContext poDbContext,
                                          IProductService productService,
                                          IEfRepository<TransientPurchaseControlDbContext, Product> repository)
        {
            _poDbContext = poDbContext;
            _productService = productService;
            _repository = repository;
        }
        public async Task<ProductModel> Handle(GetProductByIdQuery query)
        {

            var result = await _productService.GetAsync(query.Id);

            return result;
        }
    }
}
