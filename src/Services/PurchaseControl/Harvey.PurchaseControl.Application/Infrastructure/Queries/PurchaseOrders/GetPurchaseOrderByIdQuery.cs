﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.PurchaseOrders
{
    public class GetPurchaseOrderByIdQuery : IQuery<PurchaseOrderModel>
    {
        public Guid Id { get; set; }
        public GetPurchaseOrderByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public class GetPurchaseOrderByIdQueryHandler : IQueryHandler<GetPurchaseOrderByIdQuery, PurchaseOrderModel>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder, PurchaseOrderModel> _repository;
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> _poItemRepository;
        private readonly IEfRepository<TransientPurchaseControlDbContext, Variant> _variantRepository;
        private readonly IEfRepository<TransientPurchaseControlDbContext, Product> _productRepository;
        public GetPurchaseOrderByIdQueryHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder, PurchaseOrderModel> repository,
                                           IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> poItemRepository,
                                           IEfRepository<TransientPurchaseControlDbContext, Variant> variantRepository,
                                           IEfRepository<TransientPurchaseControlDbContext, Product> productRepository)
        {
            _repository = repository;
            _poItemRepository = poItemRepository;
            _variantRepository = variantRepository;
            _productRepository = productRepository;
        }
        public async Task<PurchaseOrderModel> Handle(GetPurchaseOrderByIdQuery query)
        {
            var purchaseOrder = await _repository.GetByIdAsync(query.Id);
            var result = await _poItemRepository.ListAsync(x => x.PurchaseOrderId == purchaseOrder.Id);
            var purchaseOrderItems = new List<PurchaseOrderItemModel>();
            foreach (var element in result.ToList())
            {
                var purchaseOrderItem = new PurchaseOrderItemModel()
                {
                    Id = element.Id,
                    VariantId = element.VariantId,
                    StockTypeId = element.StockTypeId,
                    Quantity = element.Quantity,
                    PurchaseOrderId = element.PurchaseOrderId,
                    CostValue = element.CostValue,
                    CurrencyId = element.CurrencyId,
                };
                var variant = await _variantRepository.GetByIdAsync(element.VariantId);
                if(variant != null)
                {
                    var product = await _productRepository.GetByIdAsync(variant.ProductId);
                    purchaseOrderItem.Product = new ProductListModel() {Id = product.Id.ToString(), Name = product.Name };
                }
                purchaseOrderItems.Add(purchaseOrderItem);
            }
            purchaseOrder.PurchaseOrderItems = purchaseOrderItems;
            return purchaseOrder;
        }
    }

}
