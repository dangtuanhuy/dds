﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.PurchaseOrders
{
    public class GetPurchaseOrdesByIdsQuery: IQuery<List<PurchaseOrderModel>>
    {
        public List<Guid> Ids { get; set; }
        public GetPurchaseOrdesByIdsQuery(List<Guid> ids)
        {
            Ids = ids;
        }
    }

    public class GetPurchaseOrdesByIdsQueryHandler : IQueryHandler<GetPurchaseOrdesByIdsQuery, List<PurchaseOrderModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder, PurchaseOrderModel> _repository;
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> _poItemRepository;
        private readonly IEfRepository<TransientPurchaseControlDbContext, Variant> _variantRepository;
        private readonly IEfRepository<TransientPurchaseControlDbContext, Field> _fieldRepository;
        public GetPurchaseOrdesByIdsQueryHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder, PurchaseOrderModel> repository,
                                           IEfRepository<PurchaseControlDbContext, PurchaseOrderItem> poItemRepository,
                                           IEfRepository<TransientPurchaseControlDbContext, Variant> variantRepository,
                                           IEfRepository<TransientPurchaseControlDbContext, Field> fieldRepository)
        {
            _repository = repository;
            _poItemRepository = poItemRepository;
            _variantRepository = variantRepository;
            _fieldRepository = fieldRepository;
        }
        public async Task<List<PurchaseOrderModel>> Handle(GetPurchaseOrdesByIdsQuery query)
        {
            var results = new List<PurchaseOrderModel>();
            var fields = await _fieldRepository.GetAsync();
            var variants = await _variantRepository.GetAsync();
            foreach (var id in query.Ids)
            {
                var purchaseOrder = await _repository.GetByIdAsync(id);
                var result = await _poItemRepository.ListAsync(x => x.PurchaseOrderId == purchaseOrder.Id);
                var purchaseOrderItems = new List<PurchaseOrderItemModel>();
                foreach (var element in result.ToList())
                {
                    var purchaseOrderItem = new PurchaseOrderItemModel()
                    {
                        Id = element.Id,
                        VariantId = element.VariantId,
                        StockTypeId = element.StockTypeId,
                        Quantity = element.Quantity,
                        PurchaseOrderId = element.PurchaseOrderId,
                        CostValue = element.CostValue,
                        CurrencyId = element.CurrencyId,
                        ProductId = variants.FirstOrDefault(x => x.Id == element.VariantId) != null ?
                        variants.FirstOrDefault(x => x.Id == element.VariantId).ProductId : Guid.Empty,
                        Variant = null
                    };

                    var poFieldValues = new List<FieldValueModel>();
                    var fieldValues = fields.Where(x => x.EntityId == element.VariantId).ToList();
                    foreach(var field in fieldValues)
                    {
                        var value = new List<string>();
                        value.Add(field.FieldValue);
                        poFieldValues.Add(new FieldValueModel()
                        {
                            Name = field.FieldName,
                            Value = value
                        });
                    }

                    var poVariant = new VariantModel()
                    {
                        Id = element.VariantId,
                        FieldValues = poFieldValues
                    };
                    purchaseOrderItem.Variant = poVariant;

                    purchaseOrderItems.Add(purchaseOrderItem);
                }
                purchaseOrder.PurchaseOrderItems = purchaseOrderItems;
                results.Add(purchaseOrder);
            }
            return results;
        }
    }
}
