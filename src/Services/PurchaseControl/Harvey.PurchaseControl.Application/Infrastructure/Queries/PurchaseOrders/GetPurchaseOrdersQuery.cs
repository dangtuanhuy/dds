﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.PurchaseOrders
{
    public class GetPurchaseOrdersQuery : IQuery<PagedResult<PurchaseOrderViewModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public string Status { get; }
        public string Stages { get; }
        public string Type { get; }
        public GetPurchaseOrdersQuery(PagingFilterCriteria pagingFilterCriteria, string queryText, string status, string stages, string type)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
            Status = status;
            Stages = stages;
            Type = type;
        }
    }

    public class GetPurchaseOrdersQueryHandler : IQueryHandler<GetPurchaseOrdersQuery, PagedResult<PurchaseOrderViewModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _repository;
        private readonly IEfRepository<PurchaseControlDbContext, POApproval> _approvalRepository;
        private readonly IEfRepository<PurchaseControlDbContext, Vendor> _vendorRepository;

        public GetPurchaseOrdersQueryHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> repository,
                                            IEfRepository<PurchaseControlDbContext, POApproval> approvalRepository,
                                            IEfRepository<PurchaseControlDbContext, Vendor> vendorRepository)
        {
            _repository = repository;
            _approvalRepository = approvalRepository;
            _vendorRepository = vendorRepository;
        }
        public async Task<PagedResult<PurchaseOrderViewModel>> Handle(GetPurchaseOrdersQuery query)
        {
            var purchasesOrderViewModel = new List<PurchaseOrderViewModel>();
            var purchaseOrders = new List<PurchaseOrder>();
            var result = string.IsNullOrEmpty(query.QueryText) ? await _repository.GetAsync()
                                                               : await _repository.ListAsync(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper())
                                                                  || x.PONumber.ToUpper().Contains(query.QueryText.ToUpper()), query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                                                                  query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);

            if (!string.IsNullOrEmpty(query.Type))
            {
                PurchaseOrderType status = (PurchaseOrderType)Enum.Parse(typeof(PurchaseOrderType), query.Type);
                result = status == PurchaseOrderType.PurchaseOrder ? result.Where(x => x.Type == PurchaseOrderType.PurchaseOrder || x.Type == PurchaseOrderType.DraftPO)
                                                                   : result.Where(x => x.Type == PurchaseOrderType.ReturnedOrder || x.Type == PurchaseOrderType.DraftRO);
            }

            if (!string.IsNullOrEmpty(query.Status))
            {
                PurchaseOrderStatus status = (PurchaseOrderStatus)Enum.Parse(typeof(PurchaseOrderStatus), query.Status);
                result = result.Where(x => x.Status == status);
            }

            if (!string.IsNullOrEmpty(query.Stages))
            {
                ApprovalStatus stages = (ApprovalStatus)Enum.Parse(typeof(ApprovalStatus), query.Stages);
                result = result.Where(x => x.ApprovalStatus == stages);
            }

            var vendors = await _vendorRepository.GetAsync();

            var totalPages =  result.Count();

            purchaseOrders = result.Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
                                            .Take(query.PagingFilterCriteria.NumberItemsPerPage)
                                            .ToList();

            foreach (var purchaseOrder in purchaseOrders)
            {
                var purchaseOrderViewModel = new PurchaseOrderViewModel()
                {
                    Id = purchaseOrder.Id,
                    Name = purchaseOrder.Name,
                    Description = purchaseOrder.Description,
                    Vendor = vendors.Where(x => x.Id == purchaseOrder.VendorId).Select(x => x.Name).FirstOrDefault(),
                    VendorId = purchaseOrder.VendorId,
                    Type = purchaseOrder.Type,
                    Status = purchaseOrder.Status,
                    ApprovalStatus = purchaseOrder.ApprovalStatus,
                    Date = purchaseOrder.Date,
                    CurrencyId = purchaseOrder.CurrencyId,
                    PONumber = purchaseOrder.PONumber,
                    UpdatedDate = purchaseOrder.UpdatedDate,
                    CreatedDate = purchaseOrder.CreatedDate

                };
                purchasesOrderViewModel.Add(purchaseOrderViewModel);
            }

            return new PagedResult<PurchaseOrderViewModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = purchasesOrderViewModel
            };

        }
    }
}
