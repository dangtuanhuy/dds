﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.PurchaseOrders
{
    public class GetCountPurchaseOrderByApprovalStatusQuery : IQuery<List<PurchaseOrderCountModel>>
    {
        public GetCountPurchaseOrderByApprovalStatusQuery() { }
    }

    public class GetCountPurchaseOrderByApprovalStatusQueryHandler : IQueryHandler<GetCountPurchaseOrderByApprovalStatusQuery, List<PurchaseOrderCountModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _poRepository;
        public GetCountPurchaseOrderByApprovalStatusQueryHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> poRepository)
        {
            _poRepository = poRepository;
        }
        public async Task<List<PurchaseOrderCountModel>> Handle(GetCountPurchaseOrderByApprovalStatusQuery query)
        {
            var result = new List<PurchaseOrderCountModel>();
            var approvalStatus = Enum.GetValues(typeof(ApprovalStatus));
            var purchaseOrders = await _poRepository.ListAsync(x => x.Type == PurchaseOrderType.PurchaseOrder);
            foreach (ApprovalStatus status in approvalStatus)
            {
                if (status != ApprovalStatus.None)
                {
                    result.Add(new PurchaseOrderCountModel()
                    {
                        StatusTitle = status.ToString(),
                        Status = status,
                        CountNumber = purchaseOrders.Count(x => x.ApprovalStatus == status),
                        Icon = ""
                    });
                }
            }

            return result;
        }
    }
}
