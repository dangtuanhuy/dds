﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.PurchaseOrders
{
    public class GetPurchaseOrderByVendorQuery : IQuery<List<PurchaseOrderViewModel>>
    {
        public Guid VendorId { get; set; }
        public GetPurchaseOrderByVendorQuery(Guid vendorId)
        {
            VendorId = vendorId;
        }
    }

    public class GetPurchaseOrderByVendorQueryHandler : IQueryHandler<GetPurchaseOrderByVendorQuery, List<PurchaseOrderViewModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PurchaseOrder> _repository;
        private readonly IEfRepository<PurchaseControlDbContext, Vendor> _vendorRepository;
        public GetPurchaseOrderByVendorQueryHandler(IEfRepository<PurchaseControlDbContext, PurchaseOrder> repository,
            IEfRepository<PurchaseControlDbContext, Vendor> vendorRepository)
        {
            _repository = repository;
            _vendorRepository = vendorRepository;
        }

        public async Task<List<PurchaseOrderViewModel>> Handle(GetPurchaseOrderByVendorQuery query)
        {
            var results = new List<PurchaseOrderViewModel>();
            var vendors = await _vendorRepository.GetAsync();
            var purchaseOrders = await _repository.GetAsync();
            purchaseOrders = purchaseOrders.Where(x => x.VendorId == query.VendorId && x.ApprovalStatus == ApprovalStatus.Confirmed).ToList();
            
            foreach(var po in purchaseOrders)
            {
                var newPO = new PurchaseOrderViewModel()
                {
                    Id = po.Id,
                    CurrencyId = po.CurrencyId,
                    Description = po.Description,
                    Name = po.Name,
                    Vendor = vendors.Where(x => x.Id == po.VendorId).Select(x => x.Name).FirstOrDefault(),
                    Status = po.Status,
                    Type = po.Type
                };
                results.Add(newPO);
            }

            return results;
        }
    }
}
