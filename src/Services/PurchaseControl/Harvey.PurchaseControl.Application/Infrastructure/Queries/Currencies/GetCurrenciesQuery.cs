﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Currencies
{
    public sealed class GetCurrenciesQuery : IQuery<List<CurrencyModel>>
    {
        
    }
    public sealed class GetCurrenciesQueryHandler : IQueryHandler<GetCurrenciesQuery, List<CurrencyModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, Currency, CurrencyModel> _repository;
        public GetCurrenciesQueryHandler(IEfRepository<PurchaseControlDbContext, Currency, CurrencyModel> repository)
        {
            _repository = repository;
        }
        public async Task<List<CurrencyModel>> Handle(GetCurrenciesQuery query)
        {
            var result = await _repository.GetAsync();
            return result.ToList();
        }
    }
}
