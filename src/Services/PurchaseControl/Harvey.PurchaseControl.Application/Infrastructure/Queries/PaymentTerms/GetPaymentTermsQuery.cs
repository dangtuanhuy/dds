﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Infrastructure.Queries.Currencies
{
    public sealed class GetPaymentTermsQuery : IQuery<List<PaymentTermModel>>
    {
        
    }
    public sealed class GetPaymentTermsQueryHandler : IQueryHandler<GetPaymentTermsQuery, List<PaymentTermModel>>
    {
        private readonly IEfRepository<PurchaseControlDbContext, PaymentTerm, PaymentTermModel> _repository;
        public GetPaymentTermsQueryHandler(IEfRepository<PurchaseControlDbContext, PaymentTerm, PaymentTermModel> repository)
        {
            _repository = repository;
        }
        public async Task<List<PaymentTermModel>> Handle(GetPaymentTermsQuery query)
        {
            var result = await _repository.GetAsync();
            return result.ToList();
        }
    }
}
