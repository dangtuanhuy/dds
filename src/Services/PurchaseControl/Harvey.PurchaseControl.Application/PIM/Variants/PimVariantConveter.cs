﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.MarketingAutomation;

namespace Harvey.PurchaseControl.Application.PIM.Variants
{
    public class PimVariantConveter : IFeedConverter<PimVariant, Variant>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimVariant>).GetType();

        public IEnumerable<Variant> Convert(IEnumerable<PimVariant> source)
        {
            return source.Select(x => new Variant()
            {
                Id = x.Id,
                ProductId = x.ProductId,
                PriceId = x.PriceId,
                SKUCode = x.SKUCode,
                UpdatedDate = x.UpdatedDate
            });
        }
    }
}
