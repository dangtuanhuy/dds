﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using Harvey.MarketingAutomation;
using System.Linq;
using Harvey.MarketingAutomation.Enums;
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Harvey.PurchaseControl.Application.PIM.Variants
{
    public class PimVariantSerializer : IFeedSerializer<Variant>
    {
        private readonly IEfRepository<TransientPurchaseControlDbContext, Variant> _efRepository;
        private readonly TransientPurchaseControlDbContext _purchaseDbContext;
        private readonly IConfiguration _configuration;
        public PimVariantSerializer(IEfRepository<TransientPurchaseControlDbContext, Variant> efRepository,
                                    TransientPurchaseControlDbContext purchaseDbContext,
                                     IConfiguration configuration)
        {
            _efRepository = efRepository;
            _purchaseDbContext = purchaseDbContext;
            _configuration = configuration;
        }
        public async Task SerializeAsync(IEnumerable<Variant> feedItems)
        {
            var variantFeedIds = feedItems.Select(x => x.Id);
            var existedVariantIds = _purchaseDbContext.Variants.Where(x => variantFeedIds.Contains(x.Id)).Select(x => x.Id).ToList();

            var newVariants = feedItems.Where(x => !existedVariantIds.Contains(x.Id));
            var updateVariants = feedItems.Where(x => existedVariantIds.Contains(x.Id));

            var optionsBuilder = new DbContextOptionsBuilder<PurchaseControlDbContext>();
            optionsBuilder.UseNpgsql(_configuration["ConnectionString"]);

            if (newVariants != null && newVariants.Any())
            {
                AddVariants(optionsBuilder.Options, newVariants);
            }

            if (updateVariants != null && updateVariants.Any())
            {
                UpdateVariants(optionsBuilder.Options, updateVariants);
            }

            var timeFeed = _purchaseDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Variant);
            var lastSync = (feedItems != null && feedItems.Count() > 0)
                                ? feedItems.Max(x => x.UpdatedDate)
                                : DateTime.UtcNow;
            if (timeFeed != null)
            {
                timeFeed.LastSync = lastSync;
                _purchaseDbContext.Update(timeFeed);
            }
            else
            {
                _purchaseDbContext.Add(new TimeToFeed
                {
                    LastSync = lastSync,
                    FeedType = FeedType.Variant
                });
            }

            await _purchaseDbContext.SaveChangesAsync();
        }

        private void AddVariants(DbContextOptions<PurchaseControlDbContext> dbOption, IEnumerable<Variant> newVariants)
        {
            int index = 0;
            while (true)
            {
                var currentData = newVariants.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PurchaseControlDbContext(dbOption))
                {
                    dbContext.AddRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }

        private void UpdateVariants(DbContextOptions<PurchaseControlDbContext> catalogOption, IEnumerable<Variant> updateVariants)
        {
            int index = 0;
            while (true)
            {
                var currentData = updateVariants.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PurchaseControlDbContext(catalogOption))
                {
                    dbContext.UpdateRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }
    }
}
