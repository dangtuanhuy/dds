﻿using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.MarketingAutomation;
using Microsoft.Extensions.Configuration;
using Harvey.MarketingAutomation.Enums;
using Harvey.PurchaseControl.Application.Infrastructure;
using System.Linq;

namespace Harvey.PurchaseControl.Application.PIM.Variants
{
    public class PimVariantFetcher : WebApiFetcherBase<PimVariant>
    {
        public PimVariantFetcher(IConfiguration configuration, TransientPurchaseControlDbContext dbContext) 
            : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.Variant}" +
                  $"&lastSync={dbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Variant)?.LastSync}")
        {
            
        }
    }
}
