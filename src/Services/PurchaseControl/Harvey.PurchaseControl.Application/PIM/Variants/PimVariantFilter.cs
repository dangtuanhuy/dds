﻿using System.Collections.Generic;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.MarketingAutomation;
using System;

namespace Harvey.PurchaseControl.Application.PIM.Variants
{
    public class PimVariantFilter : IFeedFilter<PimVariant>
    {
        public IEnumerable<PimVariant> Filter(Guid CorrelationId, IEnumerable<PimVariant> source)
        {
            return source;
        }
    }
}
