﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.PIM.Barcodes
{
    public class PimBarCodeSerializer : IFeedSerializer<BarCode>
    {
        private readonly IEfRepository<TransientPurchaseControlDbContext, BarCode> _efRepository;
        private readonly TransientPurchaseControlDbContext _purchaseDbContext;
        public PimBarCodeSerializer(IEfRepository<TransientPurchaseControlDbContext, BarCode> efRepository, TransientPurchaseControlDbContext purchaseDbContext)
        {
            _efRepository = efRepository;
            _purchaseDbContext = purchaseDbContext;
        }
        public async Task SerializeAsync(IEnumerable<BarCode> feedItems)
        {
            foreach (var item in feedItems)
            {
                var entity = await _efRepository.GetByIdAsync(item.Id);
                if (entity == null)
                {
                    entity = new BarCode()
                    {
                        Id = item.Id,
                        VariantId = item.VariantId,
                        Code = item.Code
                    };
                    await _efRepository.AddAsync(entity);
                }
                else
                {
                    entity.VariantId = item.VariantId;
                    entity.Code = item.Code;
                }

            }

            await _efRepository.SaveChangesAsync();

            var timeFeed = _purchaseDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.BarCode);
            if (timeFeed != null)
            {
                timeFeed.LastSync = DateTime.UtcNow;
                _purchaseDbContext.TimeToFeeds.Update(timeFeed);
            }
            else
            {
                _purchaseDbContext.TimeToFeeds.Add(new TimeToFeed
                {
                    LastSync = DateTime.UtcNow,
                    FeedType = FeedType.BarCode
                });
            }
            await _purchaseDbContext.SaveChangesAsync();
        }
    }
}
