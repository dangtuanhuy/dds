﻿using Harvey.MarketingAutomation;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.PurchaseControl.Application.PIM.Barcodes
{
    public class PimBarcodeConveter : IFeedConverter<PimBarCode, BarCode>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimBarCode>).GetType();

        public IEnumerable<BarCode> Convert(IEnumerable<PimBarCode> source)
        {
            return source.Select(x => new BarCode()
            {
                Id = x.Id,
                VariantId = x.VariantId,
                Code = x.Code
            });
        }
    }
}
