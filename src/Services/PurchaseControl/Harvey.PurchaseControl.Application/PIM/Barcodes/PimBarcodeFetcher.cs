﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace Harvey.PurchaseControl.Application.PIM.Barcodes
{
    public class PimBarcodeFetcher : WebApiFetcherBase<PimBarCode>
    {
        public PimBarcodeFetcher(IConfiguration configuration, TransientPurchaseControlDbContext dbContext)
            : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.BarCode}" +
                    $"&lastSync={dbContext.TimeToFeeds.AsNoTracking().FirstOrDefault(x => x.FeedType == FeedType.BarCode)?.LastSync}")
        {
        }
    }
}
