﻿using Harvey.MarketingAutomation;
using Harvey.PurchaseControl.Application.Domain.PIM;
using System;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.PIM.Barcodes
{
    public class PimBarcodeFilter : IFeedFilter<PimBarCode>
    {
        public IEnumerable<PimBarCode> Filter(Guid CorrelationId, IEnumerable<PimBarCode> source)
        {
            return source;
        }
    }
}
