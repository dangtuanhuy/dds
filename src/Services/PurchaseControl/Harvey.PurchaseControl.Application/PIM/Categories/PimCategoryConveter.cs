﻿using Harvey.MarketingAutomation;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.PurchaseControl.Application.PIM.Categories
{
    public class PimCategoryConveter : IFeedConverter<PimCategory, Category>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimCategory>).GetType();

        public IEnumerable<Category> Convert(IEnumerable<PimCategory> source)
        {
            return source.Select(x => new Category()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description
            });
        }
    }
}
