﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.PIM.Categories
{
    public class PimCategorySerializer : IFeedSerializer<Category>
    {
        private readonly IEfRepository<TransientPurchaseControlDbContext, Category> _efRepository;
        private readonly TransientPurchaseControlDbContext _purchaseDbContext;
        public PimCategorySerializer(IEfRepository<TransientPurchaseControlDbContext, Category> efRepository, TransientPurchaseControlDbContext purchaseDbContext)
        {
            _efRepository = efRepository;
            _purchaseDbContext = purchaseDbContext;
        }
        public async Task SerializeAsync(IEnumerable<Category> feedItems)
        {
            foreach (var item in feedItems)
            {
                var entity = await _efRepository.GetByIdAsync(item.Id);
                if (entity == null)
                {
                    entity = new Category()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Description = item.Description
                    };
                    await _efRepository.AddAsync(entity);
                }
                else
                {
                    entity.Name = item.Name;
                    entity.Description = item.Description;
                }

            }

            await _efRepository.SaveChangesAsync();

            var timeFeed = _purchaseDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Category);
            if (timeFeed != null)
            {
                timeFeed.LastSync = DateTime.UtcNow;
                _purchaseDbContext.TimeToFeeds.Update(timeFeed);
            }
            else
            {
                _purchaseDbContext.TimeToFeeds.Add(new TimeToFeed
                {
                    LastSync = DateTime.UtcNow,
                    FeedType = FeedType.Category
                });
            }
            await _purchaseDbContext.SaveChangesAsync();
        }
    }
}
