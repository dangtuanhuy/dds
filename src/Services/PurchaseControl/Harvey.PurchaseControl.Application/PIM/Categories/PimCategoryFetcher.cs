﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace Harvey.PurchaseControl.Application.PIM.Categories
{
    public class PimCategoryFetcher : WebApiFetcherBase<PimCategory>
    {
        public PimCategoryFetcher(IConfiguration configuration, TransientPurchaseControlDbContext dbContext)
            : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.Category}" +
                    $"&lastSync={dbContext.TimeToFeeds.AsNoTracking().FirstOrDefault(x => x.FeedType == FeedType.Category)?.LastSync}")
        {
        }
    }
}
