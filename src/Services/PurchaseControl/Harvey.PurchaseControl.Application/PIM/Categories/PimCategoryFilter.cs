﻿using Harvey.MarketingAutomation;
using Harvey.PurchaseControl.Application.Domain.PIM;
using System;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.PIM.Categories
{
    public class PimCategoryFilter : IFeedFilter<PimCategory>
    {
        public IEnumerable<PimCategory> Filter(Guid CorrelationId, IEnumerable<PimCategory> source)
        {
            return source;
        }
    }
}
