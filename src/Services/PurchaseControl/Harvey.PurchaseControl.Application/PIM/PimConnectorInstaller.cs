﻿using Harvey.EventBus.Abstractions;
using Harvey.MarketingAutomation;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.PurchaseControl.Application.PIM.Barcodes;
using Harvey.PurchaseControl.Application.PIM.Categories;
using Harvey.PurchaseControl.Application.PIM.Products;
using Harvey.PurchaseControl.Application.PIM.StockTypes;
using Harvey.PurchaseControl.Application.PIM.TransactionTypes;
using Harvey.PurchaseControl.Application.PIM.Variants;
using System;

namespace Harvey.PurchaseControl.Application.PIM
{
    public class PimConnectorInstaller
    {
        private Guid _id = Guid.Parse("BECE7E28-FF8E-4EBD-B94F-74424D7EFA19");
        private readonly string _jobName = "_pim_to_po";
        private readonly IEventBus _eventBus;

        public PimConnectorInstaller(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public void Install(ApplicationBuilder appBuilder)
        {
            appBuilder.AddConnector(_id, _jobName, _eventBus, (connectorRegistration) =>
             {
                 connectorRegistration
                 .AddProductFeedService<PimProductFeed, ProductFeed>(productFeedServiceRegistration =>
                 {
                     productFeedServiceRegistration
                     .UseFetcher<PimProductFetcher>()
                     .UseFilter<PimProductFilter>()
                     .UseConverter<PimProductConveter>()
                     .UseSerializer<PimProductSerializer>()
                     .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 3, 0));
                 }, rebuildOnExecution: true)
                 .AddVariantFeedService<PimVariant, Variant>(variantFeedServiceRegistration =>
                 {
                     variantFeedServiceRegistration
                     .UseFetcher<PimVariantFetcher>()
                     .UseFilter<PimVariantFilter>()
                     .UseConverter<PimVariantConveter>()
                     .UseSerializer<PimVariantSerializer>()
                     .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 1, 0));
                 }, rebuildOnExecution: true)
                 .AddStockTypeFeedService<PimStockType, StockType>(stockTypeFeedServiceRegistration =>
                 {
                     stockTypeFeedServiceRegistration
                     .UseFetcher<PimStockTypeFetcher>()
                     .UseFilter<PimStockTypeFilter>()
                     .UseConverter<PimStockTypeConveter>()
                     .UseSerializer<PimStockTypeSerializer>()
                     .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 1, 0));
                 })
                 .AddTransactionTypeFeedService<PimTransactionType, TransactionType>(transactionTypeFeedServiceRegistration =>
                 {
                     transactionTypeFeedServiceRegistration
                     .UseFetcher<PimTransactionTypeFetcher>()
                     .UseFilter<PimTransactionTypeFilter>()
                     .UseConverter<PimTransactionTypeConveter>()
                     .UseSerializer<PimTransactionTypeSerializer>()
                     .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 1, 0));
                 })
                 .AddCategoryFeedService<PimCategory, Category>(categoryFeedServiceRegistration => {
                     categoryFeedServiceRegistration
                     .UseFetcher<PimCategoryFetcher>()
                     .UseFilter<PimCategoryFilter>()
                     .UseConverter<PimCategoryConveter>()
                     .UseSerializer<PimCategorySerializer>()
                     .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 1, 0));
                 }, rebuildOnExecution: true)
                 .AddBarCodeFeedService<PimBarCode, BarCode>(barcodeFeedServiceRegistration => {
                     barcodeFeedServiceRegistration
                     .UseFetcher<PimBarcodeFetcher>()
                     .UseFilter<PimBarcodeFilter>()
                     .UseConverter<PimBarcodeConveter>()
                     .UseSerializer<PimBarCodeSerializer>()
                     .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 1, 0));
                 });
                 //TODO add more feed
             });
        }
    }
}
