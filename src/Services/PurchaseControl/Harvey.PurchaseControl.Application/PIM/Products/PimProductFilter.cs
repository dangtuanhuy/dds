﻿using Harvey.MarketingAutomation;
using System;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.PIM.Products
{
    public class PimProductFilter : IFeedFilter<PimProductFeed>
    {
        public IEnumerable<PimProductFeed> Filter(Guid CorrelationId, IEnumerable<PimProductFeed> source)
        {
            return source;
        }
    }
}
