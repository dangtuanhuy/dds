﻿using Harvey.PurchaseControl.Application.Domain.PIM;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.PIM.Products
{
    public class PimProductFeed : PimProduct
    {
        public List<Domain.Field> FieldValues { get; set; } = new List<Domain.Field>();
    }
}
