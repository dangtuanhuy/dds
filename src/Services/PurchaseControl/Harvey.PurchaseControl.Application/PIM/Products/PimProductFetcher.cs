﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace Harvey.PurchaseControl.Application.PIM.Products
{
    public class PimProductFetcher : WebApiFetcherBase<PimProductFeed>
    {
        public PimProductFetcher(IConfiguration configuration, TransientPurchaseControlDbContext dbContext) 
            : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.Product}" +
                  $"&lastSync={dbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Product)?.LastSync}")
        {
            
        }
    }
}
