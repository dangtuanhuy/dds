﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Harvey.PurchaseControl.Application.PIM.Products
{
    public class PimProductSerializer : IFeedSerializer<ProductFeed>
    {
        private readonly TransientPurchaseControlDbContext _purchaseDbContext;
        private readonly IConfiguration _configuration;
        public PimProductSerializer(TransientPurchaseControlDbContext purchaseDbContext,
            IConfiguration configuration)
        {
            _purchaseDbContext = purchaseDbContext;
            _configuration = configuration;
        }
        public async Task SerializeAsync(IEnumerable<ProductFeed> feedItems)
        {
            var allFeedProductIds = feedItems.Select(x => x.Id);
            var existedProductIds = _purchaseDbContext.Products.Where(x => allFeedProductIds.Contains(x.Id)).Select(x => x.Id).ToList();

            var allFeedFieldValues = feedItems.SelectMany(x => x.FieldValues);
            var allFeedFieldValueIds = allFeedFieldValues.Select(x => x.Id);
            var existedFieldValueIds = _purchaseDbContext.FieldValues.Where(x => allFeedFieldValueIds.Contains(x.Id)).Select(x => x.Id).ToList();

            var newProducts = feedItems.Where(x => !existedProductIds.Contains(x.Id)).Select(item =>
            {
                return new Product()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    IsDelete = item.IsDelete
                };
            });
            var updateProducts = feedItems.Where(x => existedProductIds.Contains(x.Id));

            var newFieldValues = allFeedFieldValues.Where(x => !existedFieldValueIds.Contains(x.Id)).Select(fv =>
            {
                return new Field
                {
                    EntityId = fv.EntityId,
                    FieldId = fv.FieldId,
                    FieldName = fv.FieldName,
                    FieldType = fv.FieldType,
                    FieldValue = fv.FieldValue,
                    FieldValueId = fv.FieldValueId,
                    Id = fv.Id,
                    IsVariantField = fv.IsVariantField,
                    OrderSection = fv.OrderSection,
                    Section = fv.Section
                };
            });
            var updateFieldValues = allFeedFieldValues.Where(x => existedFieldValueIds.Contains(x.Id));

            var optionsBuilder = new DbContextOptionsBuilder<PurchaseControlDbContext>();
            optionsBuilder.UseNpgsql(_configuration["ConnectionString"]);
            
            if (newProducts != null && newProducts.Any())
            {
                AddProducts(optionsBuilder.Options, newProducts);
            }
            if (updateProducts != null && updateProducts.Any())
            {
                UpdateProducts(optionsBuilder.Options, updateProducts);
            }

            if (newFieldValues != null && newFieldValues.Any())
            {
                AddFieldValues(optionsBuilder.Options, newFieldValues);
            }
            if (updateFieldValues != null && updateFieldValues.Any())
            {
                UpdateFieldValues(optionsBuilder.Options, updateFieldValues);
            }
            
            var timeFeed = _purchaseDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Product);
            
            if (timeFeed != null)
            {
                var lastSync = feedItems.LastOrDefault()?.UpdatedDate ?? timeFeed.LastSync;
                timeFeed.LastSync = lastSync;
                _purchaseDbContext.TimeToFeeds.Update(timeFeed);
            }
            else
            {
                var lastSync = feedItems.LastOrDefault()?.UpdatedDate ?? DateTime.MinValue;
                _purchaseDbContext.TimeToFeeds.Add(new TimeToFeed
                {
                    LastSync = lastSync,
                    FeedType = FeedType.Product
                });
            }
            await _purchaseDbContext.SaveChangesAsync();
        }

        private void AddProducts(DbContextOptions<PurchaseControlDbContext> dbOption, IEnumerable<Product> newProducts)
        {
            int index = 0;
            while (true)
            {
                var currentData = newProducts.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PurchaseControlDbContext(dbOption))
                {
                    dbContext.AddRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }

        private void UpdateProducts(DbContextOptions<PurchaseControlDbContext> catalogOption, IEnumerable<Product> updateProducts)
        {
            int index = 0;
            while (true)
            {
                var currentData = updateProducts.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PurchaseControlDbContext(catalogOption))
                {
                    dbContext.UpdateRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }

        private void AddFieldValues(DbContextOptions<PurchaseControlDbContext> dbOption, IEnumerable<Field> newFieldValues)
        {
            int index = 0;
            while (true)
            {
                var currentData = newFieldValues.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PurchaseControlDbContext(dbOption))
                {
                    dbContext.AddRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }

        private void UpdateFieldValues(DbContextOptions<PurchaseControlDbContext> catalogOption, IEnumerable<Field> updateFieldValues)
        {
            int index = 0;
            while (true)
            {
                var currentData = updateFieldValues.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PurchaseControlDbContext(catalogOption))
                {
                    dbContext.UpdateRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }
    }
}
