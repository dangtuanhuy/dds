﻿using Harvey.PurchaseControl.Application.Domain;
using System.Collections.Generic;

namespace Harvey.PurchaseControl.Application.PIM.Products
{
    public class ProductFeed: Product
    {
        public List<Field> FieldValues { get; set; } = new List<Field>();
    }
}
