﻿using Harvey.MarketingAutomation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.PurchaseControl.Application.PIM.Products
{
    public class PimProductConveter : IFeedConverter<PimProductFeed, ProductFeed>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimProductFeed>).GetType();

        public IEnumerable<ProductFeed> Convert(IEnumerable<PimProductFeed> source)
        {
            return source.Select(x => new ProductFeed()
            {
                Id = x.Id,
                Description = x.Description,
                Name = x.Name,
                IsDelete = x.IsDelete,
                FieldValues = x.FieldValues,
                UpdatedDate = x.UpdatedDate,
            });
        }
    }
}
