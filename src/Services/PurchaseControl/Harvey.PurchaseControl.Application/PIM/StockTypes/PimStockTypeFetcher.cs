﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace Harvey.PurchaseControl.Application.PIM.StockTypes
{
    public class PimStockTypeFetcher : WebApiFetcherBase<PimStockType>
    {
        public PimStockTypeFetcher(IConfiguration configuration) : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.StockType}")
        {
            
        }
    }
}
