﻿using System.Collections.Generic;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.MarketingAutomation;
using System;

namespace Harvey.PurchaseControl.Application.PIM.StockTypes
{
    public class PimStockTypeFilter : IFeedFilter<PimStockType>
    {
        public IEnumerable<PimStockType> Filter(Guid CorrelationId, IEnumerable<PimStockType> source)
        {
            return source;
        }
    }
}
