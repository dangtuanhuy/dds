﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using Harvey.MarketingAutomation;

namespace Harvey.PurchaseControl.Application.PIM.StockTypes
{
    public class PimStockTypeSerializer : IFeedSerializer<StockType>
    {
        private readonly IEfRepository<TransientPurchaseControlDbContext, StockType> _efRepository;
        public PimStockTypeSerializer(IEfRepository<TransientPurchaseControlDbContext, StockType> efRepository)
        {
            _efRepository = efRepository;
        }
        public async Task SerializeAsync(IEnumerable<StockType> feedItems)
        {
            foreach (var item in feedItems)
            {
                var entity = await _efRepository.GetByIdAsync(item.Id);
                if (entity == null)
                {
                    entity = new StockType()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Code = item.Code,
                        Description = item.Description
                    };
                    await _efRepository.AddAsync(entity);
                }
                else
                {
                    entity.Name = item.Name;
                    entity.Code = item.Code;
                    entity.Description = item.Description;
                }

            }
            await _efRepository.SaveChangesAsync();
        }
    }
}
