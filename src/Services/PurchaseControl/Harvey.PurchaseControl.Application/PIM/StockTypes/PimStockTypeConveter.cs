﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harvey.MarketingAutomation;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Domain.PIM;

namespace Harvey.PurchaseControl.Application.PIM.StockTypes
{
    public class PimStockTypeConveter : IFeedConverter<PimStockType, StockType>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimStockType>).GetType();

        public IEnumerable<Domain.StockType> Convert(IEnumerable<PimStockType> source)
        {
            return source.Select(x => new StockType()
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
                Description = x.Description
            });
        }
    }
}
