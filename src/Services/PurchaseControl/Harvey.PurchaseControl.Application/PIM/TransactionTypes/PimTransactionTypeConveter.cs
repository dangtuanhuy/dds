﻿using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.MarketingAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.PurchaseControl.Application.PIM.TransactionTypes
{
    public class PimTransactionTypeConveter : IFeedConverter<PimTransactionType, TransactionType>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimTransactionType>).GetType();

        public IEnumerable<TransactionType> Convert(IEnumerable<PimTransactionType> source)
        {
            return source.Select(x => new TransactionType()
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
                Description = x.Description
            });
        }
    }
}
