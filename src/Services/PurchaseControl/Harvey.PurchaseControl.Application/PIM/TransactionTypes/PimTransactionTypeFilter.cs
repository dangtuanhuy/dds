﻿using System.Collections.Generic;
using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.MarketingAutomation;
using System;

namespace Harvey.PurchaseControl.Application.PIM.TransactionTypes
{
    public class PimTransactionTypeFilter : IFeedFilter<PimTransactionType>
    {
        public IEnumerable<PimTransactionType> Filter(Guid CorrelationId, IEnumerable<PimTransactionType> source)
        {
            return source;
        }
    }
}
