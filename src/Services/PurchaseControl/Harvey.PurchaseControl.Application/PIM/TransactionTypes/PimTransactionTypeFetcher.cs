﻿using Harvey.PurchaseControl.Application.Domain.PIM;
using Harvey.MarketingAutomation;
using Microsoft.Extensions.Configuration;
using Harvey.MarketingAutomation.Enums;

namespace Harvey.PurchaseControl.Application.PIM.TransactionTypes
{
    public class PimTransactionTypeFetcher : WebApiFetcherBase<PimTransactionType>
    {
        public PimTransactionTypeFetcher(IConfiguration configuration) : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.TransactionType}")
        {
        }
    }
}
