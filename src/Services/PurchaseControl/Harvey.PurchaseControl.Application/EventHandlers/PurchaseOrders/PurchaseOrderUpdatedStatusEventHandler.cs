﻿

using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Infrastructure;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.EventHandlers.PurchaseOrders
{
    public class PurchaseOrderUpdatedStatusEventHandler : EventHandlerBase<PurchaseOrderUpdatedStatusEvent>
    {
        private readonly IEfRepository<TransientPurchaseControlDbContext, PurchaseOrder> _poRepository;
        public PurchaseOrderUpdatedStatusEventHandler(IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<PurchaseOrderUpdatedStatusEvent>> logger,
            IEfRepository<TransientPurchaseControlDbContext, PurchaseOrder> poRepository) : base(repository, logger)
        {
            _poRepository = poRepository;
        }

        protected override async Task ExecuteAsync(PurchaseOrderUpdatedStatusEvent @event)
        {
            foreach(var id in @event.Ids)
            {
                var po = await _poRepository.GetByIdAsync(id);

                po.Status = PurchaseOrderStatus.Received;

                await _poRepository.UpdateAsync(po);
            }

            await _poRepository.SaveChangesAsync();
        }
    }
}
