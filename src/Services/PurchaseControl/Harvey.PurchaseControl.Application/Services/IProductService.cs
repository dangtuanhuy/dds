﻿using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Services
{
    public interface IProductService
    {
        Task<ProductModel> GetAsync(Guid Id);
    }
}
