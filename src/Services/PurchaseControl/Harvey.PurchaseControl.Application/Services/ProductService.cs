﻿using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.Application.Services
{
    public class ProductService: IProductService
    {
        private readonly TransientPurchaseControlDbContext _purchaseOrderDbContext;
        private readonly IEfRepository<TransientPurchaseControlDbContext, Product> _repository;

        public ProductService(IEfRepository<TransientPurchaseControlDbContext, Product> repository,
                              TransientPurchaseControlDbContext purchaseOrderDbContext)
        {
            _repository = repository;
            _purchaseOrderDbContext = purchaseOrderDbContext;
        }

        public async Task<ProductModel> GetAsync(Guid Id)
        {
            var product = await _repository.GetByIdAsync(Id);
            var variants = _purchaseOrderDbContext.Variants.Where(x => x.ProductId == Id).ToList();
            var result = new ProductModel
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description
            };
            foreach (var variant in variants)
            {
                var fieldValues = _purchaseOrderDbContext.FieldValues.Where(x => x.EntityId == variant.Id).OrderBy(x => x.FieldName).ToList();
                var variantModel = new VariantModel()
                {
                    Id = variant.Id
                };
                foreach (var fieldValue in fieldValues)
                {
                    var fieldValueModel = new FieldValueModel()
                    {
                        Name = fieldValue.FieldName,
                        Value = fieldValue.FieldValue.Split(',').ToList(),
                    };
                    variantModel.FieldValues.Add(fieldValueModel);
                }
                result.Variants.Add(variantModel);
            }

            return result;
        }
    }
}
