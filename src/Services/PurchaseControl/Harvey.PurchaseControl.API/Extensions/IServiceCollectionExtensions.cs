﻿using AutoMapper;
using Harvey.Domain;
using Hangfire;
using Hangfire.PostgreSql;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.EventStore.Marten;
using Harvey.EventBus.RabbitMQ;
using Harvey.EventBus.RabbitMQ.Policies;
using Harvey.Job;
using Harvey.Job.Hangfire;
using Harvey.Logging;
using Harvey.Logging.SeriLog;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.API.Filters;
using Harvey.PurchaseControl.Application.EventHandlers;
using Harvey.PurchaseControl.Application.Infrastructure;
using Harvey.PurchaseControl.Application.PIM;
using Harvey.PurchaseControl.Application.PIM.Products;
using Harvey.PurchaseControl.Application.PIM.StockTypes;
using Harvey.PurchaseControl.Application.PIM.TransactionTypes;
using Harvey.PurchaseControl.Application.PIM.Variants;
using Harvey.PurchaseControl.Application.Infrastructure.Commands.Vendors;
using Harvey.PurchaseControl.Application.Infrastructure.Domain;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.Vendors;
using Harvey.PurchaseControl.Application.Services;
using Harvey.Setting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Connectors;
using Harvey.PurchaseControl.Application.PIM.Categories;
using Harvey.PurchaseControl.Application.Infrastructure.Commands.PurchaseOrders;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.PurchaseOrders;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.Products;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.Currencies;
using Harvey.Exception.Handlers;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.Approvals;
using Harvey.PurchaseControl.Application.Infrastructure.Commands.Approvals;
using Harvey.NextSequence;
using Harvey.PurchaseControl.Application.EventHandlers.PurchaseOrders;
using Harvey.EventBus;
using Harvey.Search.Abstractions;
using Harvey.Search.NEST;
using Harvey.Reporting;
using Harvey.PurchaseControl.Application.PIM.Barcodes;

namespace Harvey.PurchaseControl.API.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IEventStore>(sp =>
            {
                return new MartenEventStore(configuration["ConnectionString"]);
            });

            services.AddTransient<GreetingEventEventHandler>();
            services.AddTransient<LoggingEventHandler>();
            services.AddTransient<PurchaseOrderUpdatedStatusEventHandler>();

            services.AddSingleton(sp =>
            {
                var logger = sp.GetRequiredService<ILogger<MasstransitPersistanceConnection>>();
                var endPoint = configuration["RabbitMqConfig:RabbitMqUrl"];
                var id = configuration["RabbitMqConfig:Username"];
                var pass = configuration["RabbitMqConfig:Password"];
                return new MasstransitPersistanceConnection(new BusCreationRetrivalPolicy(), logger, endPoint, id, pass);
            });

            services.AddSingleton<IEventBus>(sp =>
            {
                return new MasstransitEventBus("Harvey_PurchaseControl_API", sp.GetRequiredService<MasstransitPersistanceConnection>(), sp);
            });

            services.AddDbContext<IdentifiedEventDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddTransient<IRepository<IdentifiedEvent>, EfRepository<IdentifiedEventDbContext, IdentifiedEvent>>();

            return services;
        }

        public static IServiceCollection AddServiceDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<PurchaseControlDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            });
            services.AddDbContext<TransientPurchaseControlDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);
            return services;
        }

        public static IServiceCollection AddCQRS(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ICommandExecutor, CommandExecutor>();
            services.AddTransient<IQueryExecutor, QueryExecutor>();

            services.AddTransient<IQueryHandler<GetProductByIdQuery, ProductModel>, GetProductByIdQueryHandler>();

            services.AddTransient<ICommandHandler<AddVendorCommand, VendorViewModel>, AddVendorCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateVendorCommand, VendorViewModel>, UpdateVendorCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteVendorCommand, bool>, DeleteVendorCommandHandler>();
            services.AddTransient<IQueryHandler<GetVendorsQuery, PagedResult<VendorModel>>, GetVendorsQueryHandler>();
            services.AddTransient<IQueryHandler<GetVendorByIdQuery, VendorModel>, GetVendorByIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetAllVendorsQuery, List<VendorModel>>, GetAllVendorsQueryHandler>();

            services.AddTransient<ICommandHandler<AddPurchaseOrderCommand, PurchaseOrderViewModel>, AddPurchaseOrderCommandHandler>();
            services.AddTransient<ICommandHandler<UpdatePurchaseOrderCommand, PurchaseOrderModel>, UpdatePurchaseOrderCommandHandler>();
            services.AddTransient<ICommandHandler<UpdatePurchaseOrderDateCommand, PurchaseOrderModel>, UpdatePurchaseOrderDateCommandHandler>();
            services.AddTransient<ICommandHandler<DeletePurchaseOrderCommand, bool>, DeletePurchaseOrderCommandHandler>();
            services.AddTransient<ICommandHandler<ChangeStatusPurchaseOrderFromDraftCommnand, PurchaseOrderViewModel>, ChangeStatusPurchaseOrderFromDraftCommnandHandler>();
            services.AddTransient<ICommandHandler<SubmitPurchaseOrderCommand, bool>, SubmitPurchaseOrderCommandHandler>();
            services.AddTransient<IQueryHandler<GetPurchaseOrdersQuery, PagedResult<PurchaseOrderViewModel>>, GetPurchaseOrdersQueryHandler>();
            services.AddTransient<IQueryHandler<GetPurchaseOrderByIdQuery, PurchaseOrderModel>, GetPurchaseOrderByIdQueryHandler>();

            services.AddTransient<IQueryHandler<GetCurrenciesQuery, List<CurrencyModel>>, GetCurrenciesQueryHandler>();

            services.AddTransient<IQueryHandler<GetTaxTypesQuery, List<TaxTypeModel>>, GetTaxTypesQueryHandler>();

            services.AddTransient<IQueryHandler<GetPaymentTermsQuery, List<PaymentTermModel>>, GetPaymentTermsQueryHandler>();

            services.AddTransient<ICommandHandler<AddApprovalCommand, ApprovalModel>, AddApprovalCommandHandler>();
            services.AddTransient<IQueryHandler<GetApprovalByManagerQuery, PagedResult<ApprovalModel>>, GetApprovalByManagerQueryHandler>();
            services.AddTransient<IQueryHandler<GetReasonByPurchaseOrderId, RejectPOModel>, GetReasonByPurchaseOrderIdHandler>();

            services.AddTransient<IQueryHandler<GetPurchaseOrderByVendorQuery, List<PurchaseOrderViewModel>>, GetPurchaseOrderByVendorQueryHandler>();
            services.AddTransient<IQueryHandler<GetPurchaseOrdesByIdsQuery, List<PurchaseOrderModel>>, GetPurchaseOrdesByIdsQueryHandler>();
            services.AddTransient<IQueryHandler<GetApprovalByManagerQuery, PagedResult<ApprovalModel>>, GetApprovalByManagerQueryHandler>();

            services.AddTransient<IQueryHandler<GetProductListQuery, List<ProductListModel>>, GetProductListQueryHandler>();
            services.AddTransient<IQueryHandler<SyncVendorsToPIMQuery, bool>, SyncVendorsToPIMQueryHandler>();

            services.AddTransient<IQueryHandler<GetCountQuery, CountModel>, GetCountQueryHandler>();
            services.AddTransient<IQueryHandler<GetCountPurchaseOrderByApprovalStatusQuery, List<PurchaseOrderCountModel>>, GetCountPurchaseOrderByApprovalStatusQueryHandler>();
            return services;
        }

        public static IServiceCollection AddService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ProductService>();
            services.AddTransient<PurchaseOrderService>();
            return services;
        }

        public static IServiceCollection AddNextSequence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<INextSequenceService<TransientPurchaseControlDbContext>, NextSequenceService<TransientPurchaseControlDbContext>>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<EfUnitOfWork>();
            services.AddScoped<IEfRepository<PurchaseControlDbContext, Vendor, VendorModel>, EfRepository<PurchaseControlDbContext, Vendor, VendorModel>>();
            services.AddScoped<IEfRepository<PurchaseControlDbContext, Vendor>, EfRepository<PurchaseControlDbContext, Vendor>>();

            services.AddScoped<IEfRepository<PurchaseControlDbContext, Currency, CurrencyModel>, EfRepository<PurchaseControlDbContext, Currency, CurrencyModel>>();
            services.AddScoped<IEfRepository<PurchaseControlDbContext, Currency>, EfRepository<PurchaseControlDbContext, Currency>>();

            services.AddScoped<IEfRepository<PurchaseControlDbContext, TaxType, TaxTypeModel>, EfRepository<PurchaseControlDbContext, TaxType, TaxTypeModel>>();
            services.AddScoped<IEfRepository<PurchaseControlDbContext, TaxType>, EfRepository<PurchaseControlDbContext, TaxType>>();


            services.AddScoped<IEfRepository<PurchaseControlDbContext, PaymentTerm, PaymentTermModel>, EfRepository<PurchaseControlDbContext, PaymentTerm, PaymentTermModel>>();
            services.AddScoped<IEfRepository<PurchaseControlDbContext, PaymentTerm>, EfRepository<PurchaseControlDbContext, PaymentTerm>>();


            services.AddScoped<IEfRepository<PurchaseControlDbContext, PurchaseOrder, PurchaseOrderModel>, EfRepository<PurchaseControlDbContext, PurchaseOrder, PurchaseOrderModel>>();
            services.AddScoped<IEfRepository<PurchaseControlDbContext, PurchaseOrder>, EfRepository<PurchaseControlDbContext, PurchaseOrder>>();

            services.AddScoped<IEfRepository<PurchaseControlDbContext, POApproval>, EfRepository<PurchaseControlDbContext, POApproval>>();
            services.AddScoped<IEfRepository<PurchaseControlDbContext, POApproval, ApprovalModel>, EfRepository<PurchaseControlDbContext, POApproval, ApprovalModel>>();

            services.AddScoped<IEfRepository<PurchaseControlDbContext, PurchaseOrderItem>, EfRepository<PurchaseControlDbContext, PurchaseOrderItem>>();
            services.AddScoped<IEfRepository<PurchaseControlDbContext, PurchaseOrderItem, PurchaseOrderItemModel>, EfRepository<PurchaseControlDbContext, PurchaseOrderItem, PurchaseOrderItemModel>>();

            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, Product>, EfRepository<TransientPurchaseControlDbContext, Product>>();
            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, Field>, EfRepository<TransientPurchaseControlDbContext, Field>>();
            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, Variant>, EfRepository<TransientPurchaseControlDbContext, Variant>>();
            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, StockType>, EfRepository<TransientPurchaseControlDbContext, StockType>>();
            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, TransactionType>, EfRepository<TransientPurchaseControlDbContext, TransactionType>>();
            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, Category>, EfRepository<TransientPurchaseControlDbContext, Category>>();
            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, BarCode>, EfRepository<TransientPurchaseControlDbContext, BarCode>>();
            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, PurchaseOrder>, EfRepository<TransientPurchaseControlDbContext, PurchaseOrder>>();
            services.AddTransient<IEfRepository<TransientPurchaseControlDbContext, PurchaseOrderItem>, EfRepository<TransientPurchaseControlDbContext, PurchaseOrderItem>>();
            return services;
        }
        public static IServiceCollection AddAppSetting(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAppSettingService, AppSettingService>();
            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper();
            return services;
        }

        public static IServiceCollection AddExceptionHandlers(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ArgumentExceptionHandler>();
            services.AddScoped<EfUniqueConstraintExceptionHandler>();
            services.AddScoped<SqlExceptionHandler>();
            services.AddScoped<ForBiddenExceptionHandler>();
            services.AddScoped<NotFoundExceptionHandler>();
            services.AddScoped<BadModelExceptionHandler>();
            return services;
        }

        public static IServiceCollection AddLogger(this IServiceCollection services, IConfiguration configuration)
        {
            new SeriLogger().Initilize(new List<IDatabaseLoggingConfiguration>()
            {
                new DatabaseLoggingConfiguration()
                {
                    ConnectionString = configuration["Logging:DataLogger:ConnectionString"],
                    TableName = configuration["Logging:DataLogger:TableName"],
                    LogLevel = (LogLevel)Enum.Parse( typeof(LogLevel), configuration["Logging:DataLogger:LogLevel"],true)
                }
            }, new List<ICentralizeLoggingConfiguration>() {
                new CentralizeLoggingConfiguration()
                {
                    Url = configuration["Logging:CentralizeLogger:Url"],
                    LogLevel = (LogLevel)Enum.Parse( typeof(LogLevel), configuration["Logging:CentralizeLogger:LogLevel"],true)
                }
            });
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
            return services;
        }
        public static IServiceCollection AddJobManager(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IJobManager, HangfireJobManager>();
            services.AddHangfire(options => options.UseStorage(new PostgreSqlStorage(configuration["ConnectionString"]))
            .UseColouredConsoleLogProvider());
            return services;
        }
        public static IServiceCollection AddMarketingAutomation(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<ApplicationBuilder>();
            services.AddSingleton<ConnectorInfoCollection>();
            services.AddTransient<PimConnectorInstaller>();
            services.AddSingleton<FeedWorker>();

            services.AddTransient<PimProductFetcher>();
            services.AddTransient<PimProductFilter>();
            services.AddTransient<PimProductConveter>();
            services.AddTransient<PimProductSerializer>();

            services.AddTransient<PimVariantFetcher>();
            services.AddTransient<PimVariantFilter>();
            services.AddTransient<PimVariantConveter>();
            services.AddTransient<PimVariantSerializer>();

            services.AddTransient<PimStockTypeFetcher>();
            services.AddTransient<PimStockTypeFilter>();
            services.AddTransient<PimStockTypeConveter>();
            services.AddTransient<PimStockTypeSerializer>();

            services.AddTransient<PimTransactionTypeFetcher>();
            services.AddTransient<PimTransactionTypeFilter>();
            services.AddTransient<PimTransactionTypeConveter>();
            services.AddTransient<PimTransactionTypeSerializer>();

            services.AddTransient<PimCategoryFetcher>();
            services.AddTransient<PimCategoryFilter>();
            services.AddTransient<PimCategoryConveter>();
            services.AddTransient<PimCategorySerializer>();

            services.AddTransient<PimBarcodeFetcher>();
            services.AddTransient<PimBarcodeFilter>();
            services.AddTransient<PimBarcodeConveter>();
            services.AddTransient<PimBarCodeSerializer>();
            return services;
        }
        public static IServiceCollection AddSearchService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ISearchService, SearchService>();
            services.AddTransient<SearchSettings>(sp =>
            {
                return new SearchSettings(configuration["ELASTICSEARCHURL"]);
            });
            return services;
        }

        public static IServiceCollection AddReportingServer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(x => new ReportServer(configuration["ReportServer"]));
            return services;
        }
    }
}
