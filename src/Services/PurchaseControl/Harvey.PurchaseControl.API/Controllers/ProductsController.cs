﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.Products;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.PurchaseControl.API.Controllers
{
    [Route("api/products")]
    [Authorize]
    public class ProductsController : ControllerBase
    {
        private readonly IEfRepository<TransientPurchaseControlDbContext, Product> _efRepository;
        private readonly IQueryExecutor _queryExecutor;
        public ProductsController(IEfRepository<TransientPurchaseControlDbContext, Product> efRepository, IQueryExecutor queryExecutor)
        {
            _efRepository = efRepository;
            _queryExecutor = queryExecutor;
        }

        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<Product>>> GetAll()
        {
            return Ok(await _efRepository.GetAsync());
        }

        [HttpGet]
        public async Task<ActionResult<List<ProductListModel>>> GetAll(string queryText)
        {
            return await _queryExecutor.ExecuteAsync(new GetProductListQuery(queryText));
        }

        [HttpGet()]
        [Route("{id}")]
        public async Task<ActionResult<ProductModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetProductByIdQuery(id));
            return result;
        }

        [AllowAnonymous]
        [HttpGet("count")]
        public async Task<ActionResult<IEnumerable<Product>>> Count()
        {
            return Ok(await _queryExecutor.ExecuteAsync(new GetCountQuery()));
        }
    }
}