﻿using System;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.PurchaseControl.API.Extensions;
using Harvey.PurchaseControl.API.Filters;
using Harvey.PurchaseControl.Application.Infrastructure.Commands.Approvals;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.Approvals;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.PurchaseControl.API.Controllers
{
    [Route("api/approval")]
    [Authorize(Policy = "ApprovalPurchase")]
    public class ApprovalController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;

        public ApprovalController(IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet("{managerId}")]
        public async Task<ActionResult<PagedResult<ApprovalModel>>> Get(Guid managerId, PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            if (managerId == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetApprovalByManagerQuery(managerId, pagingFilterCriteria, queryText));
            return result;
        }

        [HttpGet("reason")]
        [AllowAnonymous]
        public async Task<ActionResult<RejectPOModel>> GetReason(Guid purchaseOrderId)
        {
            if (purchaseOrderId == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetReasonByPurchaseOrderId(purchaseOrderId));
            return result;
        }

        [HttpPost]
        [Route("{id}/approve")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<PurchaseOrderViewModel>> ApprovePurchaseOrder(Guid id, Guid managerId)
        {
            if (id == null)
            {
                return BadRequest("Purchase Order is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new AddApprovalCommand(User.GetUserId(), id, managerId, ApprovalStatus.Approved, null));
            if (result != null && result.Id != Guid.Empty)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot approve purchase order. Please try again.");
            }
        }

        [HttpPost]
        [Route("{id}/reject")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<PurchaseOrderViewModel>> RejectPurchaseOrder(Guid id, [FromBody] RejectPOModel command)
        {
            if (id == null)
            {
                return BadRequest("Purchase Order is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new AddApprovalCommand(User.GetUserId(), id, command.ManagerId, ApprovalStatus.Rejected, command.Reason));
            if (result != null && result.Id != Guid.Empty)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot reject purchase order. Please try again.");
            }
        }

        [HttpPost]
        [Route("{id}/confirm")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<PurchaseOrderViewModel>> ConfirmPurchaseOrder(Guid id, Guid managerId )
        {
            if (id == null)
            {
                return BadRequest("Purchase Order is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new AddApprovalCommand(User.GetUserId(), id, managerId, ApprovalStatus.Confirmed, null));
            if (result != null && result.Id != Guid.Empty)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot reject purchase order. Please try again.");
            }
        }
    }
}