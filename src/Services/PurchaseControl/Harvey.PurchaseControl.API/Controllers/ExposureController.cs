﻿using Harvey.Domain;
using Harvey.PurchaseControl.API.Extensions;
using Harvey.PurchaseControl.Application.Extensions;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.PurchaseOrders;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.API.Controllers
{
    [Route("api/purchase-order-exposure")]
    public class ExposureController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        public ExposureController(IQueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }

        [HttpGet("count")]
        public async Task<ActionResult<List<PurchaseOrderCountModel>>> CountPurchaseOrder(string secretKey)
        {
            if(secretKey != SecretKeyExtension.SystemSecretKey)
            {
                return NotFound();
            }
            var result = await _queryExecutor.ExecuteAsync(new GetCountPurchaseOrderByApprovalStatusQuery());
            return Ok(result);
        }

    }
}
