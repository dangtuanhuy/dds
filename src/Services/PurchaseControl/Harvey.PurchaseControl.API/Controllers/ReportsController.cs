﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.Reporting;
using Harvey.Reporting.ReportStatementModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.API.Controllers
{
    [Route("api/reports")]
    [Authorize]
    public class ReportsController : ControllerBase
    {
        private readonly ReportServer _reportService;
        private readonly PurchaseControlDbContext _purchaseControlDbContext;
        private readonly IEfRepository<TransientPurchaseControlDbContext, Variant> _variantRepository;
        private readonly IEfRepository<TransientPurchaseControlDbContext, Product> _productRepository;
        private readonly INextSequenceService<TransientPurchaseControlDbContext> _nextSequenceService;
        public ReportsController(
            ReportServer reportService,
            PurchaseControlDbContext purchaseControlDbContext,
            IEfRepository<TransientPurchaseControlDbContext, Variant> variantRepository,
            IEfRepository<TransientPurchaseControlDbContext, Product> productRepository,
            INextSequenceService<TransientPurchaseControlDbContext> nextSequenceService)
        {
            _reportService = reportService;
            _purchaseControlDbContext = purchaseControlDbContext;
            _productRepository = productRepository;
            _variantRepository = variantRepository;
            _nextSequenceService = nextSequenceService;
        }

        [HttpPost("sales-invoice")]
        public async Task<ActionResult<string>> SalesInvoice([FromBody] SalesInvoiceModel SalesInvoiceViewModel)
        {
            var reportId = Guid.NewGuid().ToString();
            var vendor = _purchaseControlDbContext.Vendors.Include(x => x.PaymentTerm).Include(x => x.TaxType).SingleOrDefault(x => x.Id == SalesInvoiceViewModel.VendorId);
            var purchaseOrder = _purchaseControlDbContext.PurchaseOrders.Include(x => x.PurchaseOrderItems).SingleOrDefault(x => x.Id == SalesInvoiceViewModel.PurchaseOrderId);
            if (vendor == null || purchaseOrder == null)
            {
                throw new InvalidOperationException("Cannot add purchase order. Please try again.");
            }
            var currentTime = DateTime.UtcNow.ToString("yyMM", System.Globalization.CultureInfo.InvariantCulture);
            var keyName = $"HQ{currentTime}";
            var InvoiceNo = $"{keyName}{_nextSequenceService.Get(keyName, 10)}";
            var dueDate = "";
            PaymentTerm paymentTerm = (PaymentTerm)Enum.Parse(typeof(PaymentTerm), vendor.PaymentTerm.Code);
            switch (paymentTerm)
            {
                case  PaymentTerm.PT1:
                    dueDate = purchaseOrder?.Date.ToString("dd/MM/yyyy");
                    break;
                case PaymentTerm.PT2:
                    dueDate = purchaseOrder.Date.AddMonths(1).ToString("dd/MM/yyyy");
                    break;
                case PaymentTerm.PT3:
                    dueDate = purchaseOrder.Date.AddMonths(2).ToString("dd/MM/yyyy");
                    break;
                default:
                    dueDate = DateTime.UtcNow.ToString("dd/MM/yyyy");
                    break;
            }
                
            var salesInvoice= new SalesInvoiceHeader()
            {
                ReportId = reportId,
                VendorName = vendor.Name,
                Address = vendor.Address1 != null ? vendor.Address1 : vendor.Address2,
                City = vendor.CityCode,
                Country = vendor.Country,
                ZipCode = vendor.ZipCode,
                Phone = vendor.Phone,
                Fax = vendor.Fax,
                InvoiceNo = InvoiceNo,
                InvoiceDate = DateTime.UtcNow.ToString("dd/MM/yyyy"),
                GSTRegNo = "201229862H",
                PaymentTerms = vendor.PaymentTerm.Name,
                DueDate = dueDate,
                Discount = ConvertToDecimal(0),
                SubTotal = ConvertToDecimal(0),
                Tax = ConvertToDecimal(0),
                NetTotal = ConvertToDecimal(0),
                SoDo = SalesInvoiceViewModel.SoDo

            };

            foreach (var purchaseOrderItem in purchaseOrder.PurchaseOrderItems)
            {
                var salesInvoiceDetail = new SalesInvoiceDetail();
                salesInvoiceDetail.ReportId = reportId;
                salesInvoiceDetail.Quantity = purchaseOrderItem.Quantity.ToString();
                salesInvoiceDetail.UOM = "PCS";
                salesInvoiceDetail.UnitPrice = purchaseOrderItem.CostValue.ToString();
                salesInvoiceDetail.TotalAmount = ConvertToDecimal(purchaseOrderItem.CostValue * purchaseOrderItem.Quantity);
                var variant = await _variantRepository.GetByIdAsync(purchaseOrderItem.VariantId);
                if (variant != null)
                {
                    salesInvoiceDetail.InventoryCode = variant.SKUCode;
                    
                    var product = await _productRepository.GetByIdAsync(variant.ProductId);
                    salesInvoiceDetail.Description = product?.Name;
                }
                salesInvoice.Details.Add(salesInvoiceDetail);
            }

            foreach (var salesInvoiceDetail in salesInvoice.Details)
            {
                salesInvoice.SubTotal = ConvertToDecimal((Double.Parse(salesInvoice.SubTotal) + Double.Parse(salesInvoiceDetail.TotalAmount)));
            }

            salesInvoice.Tax = ConvertToDecimal(Double.Parse(vendor.TaxType.Value) *  +Double.Parse(salesInvoice.SubTotal) / 100);
            salesInvoice.TaxRoundOff = ConvertToDecimal(0);
            salesInvoice.NetTotal = ConvertToDecimal(Double.Parse(salesInvoice.SubTotal) + Double.Parse(salesInvoice.Tax));

            var result = await _reportService.PrepareData(new ReportModel<SalesInvoiceHeader>()
            {
                Type = Reporting.Enums.ReportType.SalesInvoice,
                ReportId = reportId,
                DataSource = salesInvoice
            });
            return Ok(result);

        }

        [HttpPost("purchase-order")]
        public async Task<ActionResult<string>> PurchaseOrder([FromBody] PurchaseOrderModel purchaseOrderModel)
        {
            var reportId = Guid.NewGuid().ToString();
            var vendor = _purchaseControlDbContext.Vendors.Include(x => x.PaymentTerm).Include(x => x.TaxType).SingleOrDefault(x => x.Id == purchaseOrderModel.VendorId);
            var purchaseOrder = _purchaseControlDbContext.PurchaseOrders.Include(x => x.PurchaseOrderItems).SingleOrDefault(x => x.Id == purchaseOrderModel.Id);
            
            if (vendor == null || purchaseOrder == null)
            {
                throw new InvalidOperationException("Cannot add purchase order. Please try again.");
            }
            
            var purchaseOrderStatementHeader = new PurchaseOrderStatementHeader()
            {
                ReportId = reportId,
                VendorName = vendor.Name,
                Address = vendor.Address1 != null ? vendor.Address1 : vendor.Address2,
                City = vendor.CityCode,
                Country = vendor.Country,
                ZipCode = vendor.ZipCode,
                Phone = vendor.Phone,
                Fax = vendor.Fax,
                PONo = purchaseOrder.PONumber,
                PODate = purchaseOrder.CreatedDate.ToString("dd/MM/yyyy"),
                Delivery = purchaseOrder.CreatedDate.ToString("dd/MM/yyyy"),
                GSTRegNo = "201229862H",
                PaymentTerms = vendor.PaymentTerm.Name,
                DiscountAmount = ConvertToDecimal(0),
                SubTotal = ConvertToDecimal(0),
                Tax = ConvertToDecimal(0),
                NetTotal = ConvertToDecimal(0),
                TotalValue = ConvertToDecimal(0)
            };

            foreach (var purchaseOrderItem in purchaseOrder.PurchaseOrderItems)
            {
                var purchaseOrderDetail = new PurchaseOrderStatementDetail();

                purchaseOrderDetail.ReportId = reportId;
                purchaseOrderDetail.Quantity = ConvertToDecimal(purchaseOrderItem.Quantity);
                purchaseOrderDetail.UOM = "PCS";
                purchaseOrderDetail.Cost = ConvertToDecimal(purchaseOrderItem.CostValue);
                purchaseOrderDetail.Discount = ConvertToDecimal(0);
                purchaseOrderDetail.TotalAmount = ConvertToDecimal(purchaseOrderItem.CostValue * purchaseOrderItem.Quantity);
                
                var variant = await _variantRepository.GetByIdAsync(purchaseOrderItem.VariantId);
                var fieldValues = await _purchaseControlDbContext.FieldValues.Where(x => x.EntityId == variant.Id).ToListAsync();
                
                var variantName = "";
                foreach (var fieldValue in fieldValues)
                {
                    if (variantName == "")
                    {
                        variantName = fieldValue?.FieldValue;
                    }
                    else
                    {
                        variantName = variantName + " " + fieldValue?.FieldValue;
                    }
                }

                if (variant != null)
                {
                    purchaseOrderDetail.InventoryCode = variant.SKUCode;

                    var product = await _productRepository.GetByIdAsync(variant.ProductId);
                    purchaseOrderDetail.Description = $"{product?.Name} - {variantName}";
                }

                purchaseOrderStatementHeader.Details.Add(purchaseOrderDetail);

            }

            foreach (var purchaseOrderDetail in purchaseOrderStatementHeader.Details)
            {
                purchaseOrderStatementHeader.TotalValue = ConvertToDecimal((Double.Parse(purchaseOrderStatementHeader.TotalValue) + Double.Parse(purchaseOrderDetail.TotalAmount)));
            }
            purchaseOrderStatementHeader.SubTotal = ConvertToDecimal(Double.Parse(purchaseOrderStatementHeader.TotalValue) - Double.Parse(purchaseOrderStatementHeader.DiscountAmount));
            purchaseOrderStatementHeader.Tax = ConvertToDecimal(Double.Parse(vendor.TaxType.Value) * +Double.Parse(purchaseOrderStatementHeader.SubTotal) / 100);
            purchaseOrderStatementHeader.NetTotal = ConvertToDecimal(Double.Parse(purchaseOrderStatementHeader.SubTotal) + Double.Parse(purchaseOrderStatementHeader.Tax));

            var result = await _reportService.PrepareData(new ReportModel<PurchaseOrderStatementHeader>()
            {
                Type = Reporting.Enums.ReportType.PurchaseOrder,
                ReportId = reportId,
                DataSource = purchaseOrderStatementHeader
            });
            return Ok(result);
        }

        [HttpPost("purchase-return")]
        public async Task<ActionResult<string>> PurchaseReturn([FromBody] PurchaseOrderModel purchaseOrderModel)
        {
            var reportId = Guid.NewGuid().ToString();
            var vendor = _purchaseControlDbContext.Vendors.Include(x => x.PaymentTerm).Include(x => x.TaxType).SingleOrDefault(x => x.Id == purchaseOrderModel.VendorId);
            var purchaseOrder = _purchaseControlDbContext.PurchaseOrders.Include(x => x.PurchaseOrderItems).SingleOrDefault(x => x.Id == purchaseOrderModel.Id);

            if (vendor == null || purchaseOrder == null)
            {
                throw new InvalidOperationException("Cannot add purchase order. Please try again.");
            }

            var purchaseReturnStatementHeader = new PurchaseReturnStatementHeader()
            {
                ReportId = reportId,
                VendorName = vendor.Name,
                Address = vendor.Address1 != null ? vendor.Address1 : vendor.Address2,
                City = vendor.CityCode,
                Country = vendor.Country,
                ZipCode = vendor.ZipCode,
                Phone = vendor.Phone,
                Fax = vendor.Fax,
                PRNo = purchaseOrder.PONumber,
                PRDate = purchaseOrder.CreatedDate.ToString("dd/MM/yyyy"),
                GSTRegNo = "201229862H",
                SubTotal = ConvertToDecimal(0),
                Tax = ConvertToDecimal(0),
                NetTotal = ConvertToDecimal(0)
            };

            foreach (var purchaseOrderItem in purchaseOrder.PurchaseOrderItems)
            {
                var purchaseOrderDetail = new PurchaseReturnStatementDetail();

                purchaseOrderDetail.ReportId = reportId;
                purchaseOrderDetail.Quantity = ConvertToDecimal(purchaseOrderItem.Quantity);
                purchaseOrderDetail.UOM = "PCS";
                purchaseOrderDetail.UnitCost = ConvertToDecimal(purchaseOrderItem.CostValue);
                purchaseOrderDetail.NetCost = ConvertToDecimal(purchaseOrderItem.CostValue * purchaseOrderItem.Quantity);

                var variant = await _variantRepository.GetByIdAsync(purchaseOrderItem.VariantId);
                var fieldValues = await _purchaseControlDbContext.FieldValues.Where(x => x.EntityId == variant.Id).ToListAsync();

                var variantName = "";
                foreach (var fieldValue in fieldValues)
                {
                    if (variantName == "")
                    {
                        variantName = fieldValue?.FieldValue;
                    }
                    else
                    {
                        variantName = variantName + " " + fieldValue?.FieldValue;
                    }
                }

                if (variant != null)
                {
                    purchaseOrderDetail.InventoryCode = variant.SKUCode;

                    var product = await _productRepository.GetByIdAsync(variant.ProductId);
                    purchaseOrderDetail.Description = $"{product?.Name} - {variantName}";
                }

                purchaseReturnStatementHeader.Details.Add(purchaseOrderDetail);
            }

            foreach (var purchaseOrderDetail in purchaseReturnStatementHeader.Details)
            {
                purchaseReturnStatementHeader.SubTotal = ConvertToDecimal(Double.Parse(purchaseReturnStatementHeader.SubTotal) + Double.Parse(purchaseOrderDetail.NetCost));
            }

            purchaseReturnStatementHeader.Tax = ConvertToDecimal(Double.Parse(vendor.TaxType.Value) * Double.Parse(purchaseReturnStatementHeader.SubTotal) / 100);
            purchaseReturnStatementHeader.NetTotal = ConvertToDecimal(Double.Parse(purchaseReturnStatementHeader.SubTotal) + Double.Parse(purchaseReturnStatementHeader.Tax));

            var result = await _reportService.PrepareData(new ReportModel<PurchaseReturnStatementHeader>()
            {
                Type = Reporting.Enums.ReportType.PurchaseReturn,
                ReportId = reportId,
                DataSource = purchaseReturnStatementHeader
            });
            return Ok(result);

        }


        public string ConvertToDecimal(object value)
        {
            return String.Format("{0:0.00}", value);
        }

    }
}
