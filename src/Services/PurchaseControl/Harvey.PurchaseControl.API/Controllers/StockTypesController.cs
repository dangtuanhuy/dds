﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.PurchaseControl.API.Controllers
{
    [Route("api/stocktypes")]
    [Authorize]
    public class StockTypesController : ControllerBase
    {
        private readonly IEfRepository<TransientPurchaseControlDbContext, StockType> _efRepository;
        public StockTypesController(IEfRepository<TransientPurchaseControlDbContext, StockType> efRepository)
        {
            _efRepository = efRepository;
        }
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<StockType>>> GetAll()
        {
            var result = await _efRepository.GetAsync();
            result = result.Where(x => x.Code != "STI").ToList();
            return Ok(result);
        }
    }
}