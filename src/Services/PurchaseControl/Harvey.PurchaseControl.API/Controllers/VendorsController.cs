﻿using Harvey.Domain;
using Harvey.PurchaseControl.API.Extensions;
using Harvey.PurchaseControl.API.Filters;
using Harvey.PurchaseControl.Application.Infrastructure.Commands.Vendors;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.Vendors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.API.Controllers
{
    [Route("api/vendors")]
    [Authorize(Policy = "PurchaseOrder")]
    public class VendorsController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        public VendorsController(IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<VendorModel>>> Get(PagingFilterCriteria pagingFilterCriteria, string paymentTermIds, string currencyIds, string taxTypeIds, string queryString)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetVendorsQuery(pagingFilterCriteria, paymentTermIds, currencyIds, taxTypeIds, queryString));
            return Ok(result);
        }

        [HttpGet("all")]
        public async Task<ActionResult<PagedResult<VendorModel>>> Get()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetAllVendorsQuery());
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetVendorByIdQuery(id));
            return Ok(result);
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<VendorViewModel>> Add([FromBody] VendorViewModel vendor)
        {
            if (string.IsNullOrEmpty(vendor.Name))
            {
                return BadRequest("Name is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new AddVendorCommand(User.GetUserId(),
                                                                                    vendor));
            if (result != null && result.Id != Guid.Empty)
            {
                return CreatedAtAction("Add", result);
            }
            else
            {
                throw new InvalidOperationException("Cannot add vendor. Please try again.");
            }
        }

        [HttpPut]
        [Route("{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Update(Guid id, [FromBody] VendorViewModel vendor)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new UpdateVendorCommand(User.GetUserId(), id, vendor));

            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot update vendor. Please try again.");
            }
        }

        [HttpDelete("{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required");
            }
            await _commandExecutor.ExecuteAsync(new DeleteVendorCommand(id));
            return Ok();
        }

        [HttpGet("rebuild-vendors")]
        public async Task<ActionResult> RebuildVendor()
        {
            var result = await _queryExecutor.ExecuteAsync(new SyncVendorsToPIMQuery());
            if (result == false)
            {
                throw new InvalidOperationException("Cannot rebuild vendors. Please try again.");
            }
            return Ok(result);
        }
    }
}
