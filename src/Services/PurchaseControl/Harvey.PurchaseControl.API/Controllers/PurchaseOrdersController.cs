﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.PurchaseControl.API.Extensions;
using Harvey.PurchaseControl.API.Filters;
using Harvey.PurchaseControl.Application.Infrastructure;
using Harvey.PurchaseControl.Application.Infrastructure.Commands.PurchaseOrders;
using Harvey.PurchaseControl.Application.Infrastructure.Enums;
using Harvey.PurchaseControl.Application.Infrastructure.Models;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.Currencies;
using Harvey.PurchaseControl.Application.Infrastructure.Queries.PurchaseOrders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.PurchaseControl.API.Controllers
{
    [Route("api/purchase-order")]
    [Authorize (Policy = "PurchaseOrder")]
    public class PurchaseOrdersController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        private readonly INextSequenceService<TransientPurchaseControlDbContext> _nextSequenceService;

        public PurchaseOrdersController(IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor,
            INextSequenceService<TransientPurchaseControlDbContext> nextSequenceService)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
            _nextSequenceService = nextSequenceService;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<PurchaseOrderViewModel>>> Get(GetPurchaseOrderModel purchaseOrderRequest)
        {
            var pagingFilterCriteria = new PagingFilterCriteria
            {
                Page = purchaseOrderRequest.Page,
                NumberItemsPerPage = purchaseOrderRequest.NumberItemsPerPage
            };
            var result = await _queryExecutor.ExecuteAsync(new GetPurchaseOrdersQuery(pagingFilterCriteria, 
                                                                                      purchaseOrderRequest.QueryText, 
                                                                                      purchaseOrderRequest.Status,
                                                                                      purchaseOrderRequest.Stages, 
                                                                                      purchaseOrderRequest.Type));
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetPurchaseOrderByIdQuery(id));
            return Ok(result);
        }

        [HttpGet("currencies")]
        public async Task<ActionResult<CurrencyModel>> Get()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetCurrenciesQuery());
            return Ok(result);
        }

        [HttpGet("taxTypes")]
        public async Task<ActionResult<TaxTypeModel>> GetTaxTypes()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetTaxTypesQuery());
            return Ok(result);
        }

        [HttpGet("paymentTerms")]
        public async Task<ActionResult<PaymentTermModel>> GetPaymentTerms()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetPaymentTermsQuery());
            return Ok(result);
        }

        [HttpGet("giw")]
        public async Task<ActionResult> GetByIds([FromQuery]List<Guid> ids)
        {
            if(ids == null)
                return BadRequest();
            var result = await _queryExecutor.ExecuteAsync(new GetPurchaseOrdesByIdsQuery(ids));
            return Ok(result);
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<PurchaseOrderViewModel>> Add([FromBody] PurchaseOrderModel purchaseOrder)
        {
            var result = await _commandExecutor.ExecuteAsync(new AddPurchaseOrderCommand(User.GetUserId(), purchaseOrder));
            if (result != null && result.Id != Guid.Empty)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot add purchase order. Please try again.");
            }
        }

        [HttpPut]
        [Route("{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Update(Guid id, [FromBody] PurchaseOrderModel purchaseOrder)
        {
            if (id == Guid.Empty || id == null)
            {
                return BadRequest("id is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new UpdatePurchaseOrderCommand(id, User.GetUserId(), purchaseOrder));
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new InvalidOperationException("Cannot update vendor. Please try again.");
            }
        }

        [HttpPut]
        [Route("{id}/date")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> UpdatePODate(Guid id, [FromBody] PurchaseOrderDateModel purchaseOrder)
        {
            if (id == Guid.Empty || id == null)
            {
                return BadRequest("id is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new UpdatePurchaseOrderDateCommand(id, User.GetUserId(), purchaseOrder));
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new InvalidOperationException("Cannot update vendor. Please try again.");
            }
        }

        [HttpPut]
        [Route("{id}/convert")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<PurchaseOrderViewModel>> Convert(Guid id)
        {
            if (id == Guid.Empty || id == null)
            {
                return BadRequest("id is required.");
            }

            var result = await _commandExecutor.ExecuteAsync(new ChangeStatusPurchaseOrderFromDraftCommnand(id, 
                                                                                                            "convert",
                                                                                                            User.GetUserId(), 
                                                                                                            PurchaseOrderStatus.OpenOrder, 
                                                                                                            ApprovalStatus.None));
            return Ok(result);     
        }

        [HttpPut]
        [Route("{id}/bypass")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<PurchaseOrderViewModel>> ByPass(Guid id)
        {
            if (id == Guid.Empty || id == null)
            {
                return BadRequest("id is required.");
            }

            var result = await _commandExecutor.ExecuteAsync(new ChangeStatusPurchaseOrderFromDraftCommnand(id,
                                                                                                            "bypass",
                                                                                                            User.GetUserId(), 
                                                                                                            PurchaseOrderStatus.Pending,
                                                                                                            ApprovalStatus.Confirmed));
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}/submit")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Submit(Guid id, [FromQuery] Guid managerId)
        {
            if (id == Guid.Empty || id == null)
            {
                return BadRequest("id is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new SubmitPurchaseOrderCommand(id, User.GetUserId(), managerId));
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new InvalidOperationException("Cannot submit purchase order. Please try again.");
            }
        }

        [HttpDelete("{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required");
            }
            await _commandExecutor.ExecuteAsync(new DeletePurchaseOrderCommand(id));
            return Ok();
        }

        [HttpGet("vendor/{VendorId}")]
        public async Task<ActionResult<PaymentTermModel>> GetByVendor(Guid VendorId)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetPurchaseOrderByVendorQuery(VendorId));
            return Ok(result);
        }
    }
}