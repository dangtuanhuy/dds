﻿using System;
using System.Threading.Tasks;
using Harvey.Persitance.EF;
using Harvey.PurchaseControl.Application.Domain;
using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.PurchaseControl.API.Controllers
{
    [Route("api/variants")]
    [Authorize]
    public class VariantsController : ControllerBase
    {
        private readonly IEfRepository<TransientPurchaseControlDbContext, Variant> _efRepository;
        public VariantsController(IEfRepository<TransientPurchaseControlDbContext, Variant> efRepository)
        {
            _efRepository = efRepository;
        }
        [HttpGet()]
        public async Task<ActionResult> Get(Guid productId)
        {
            if (productId == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _efRepository.ListAsync(x => x.ProductId == productId);
            return Ok(result);
        }
    }
}