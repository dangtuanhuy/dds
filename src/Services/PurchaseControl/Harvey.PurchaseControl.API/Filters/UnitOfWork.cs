﻿using Harvey.PurchaseControl.Application.Infrastructure;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PurchaseControl.API.Filters
{
    public class EfUnitOfWork : IActionFilter
    {
        private readonly PurchaseControlDbContext _purchaseDbContext;
        public EfUnitOfWork(PurchaseControlDbContext purchaseDbContext)
        {
            _purchaseDbContext = purchaseDbContext;
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception == null)
            {
                _purchaseDbContext.SaveChangesAsync().Wait();
            }
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }
    }
}
