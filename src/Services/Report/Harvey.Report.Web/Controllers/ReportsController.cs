﻿using FastReport;
using FastReport.Data;
using FastReport.Web;
using Harvey.Report.Web.Data;
using Harvey.Report.Web.Factories;
using Harvey.Report.Web.Models;
using Harvey.Reporting;
using Harvey.Reporting.Enums;
using Harvey.Reporting.ReportStatementModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Harvey.Report.Web.Controllers
{
    [Route("reports")]
    public class ReportsController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ReportDbContext _reportDbContext;
        private readonly IConfiguration _configuration;
        public ReportsController(
            ReportDbContext reportDbContext,
            IConfiguration configuration,
            IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _reportDbContext = reportDbContext;
            FastReport.Utils.RegisteredObjects.AddConnection(typeof(PostgresDataConnection));
        }
        [HttpPost]
        public async Task<ActionResult<string>> Post([FromBody]ReportModel<string> reportModel)
        {
            switch (reportModel.Type)
            {
                case ReportType.TransferIn:

                    var transferIn = await _reportDbContext.TransferInStatementHeaders.FirstOrDefaultAsync(x => x.ReportId == reportModel.ReportId);

                    if (transferIn != null)
                    {
                        return Ok(reportModel.ReportId);
                    }
                    var data = JsonConvert.DeserializeObject<TransferInStatementHeader>(reportModel.DataSource);
                    data.ReportId = reportModel.ReportId;
                    await _reportDbContext.TransferInStatementHeaders.AddAsync(data);
                    data.Details.ForEach(x => x.ReportId = reportModel.ReportId);
                    await _reportDbContext.TransferInStatementDetails.AddRangeAsync(data.Details);
                    await _reportDbContext.SaveChangesAsync();
                    break;

                case ReportType.TransferOut:
                   
                    var transferOut = await _reportDbContext.TransferOutStatementHeaders.FirstOrDefaultAsync(x => x.ReportId == reportModel.ReportId);

                    if (transferOut != null)
                    {
                        return Ok(reportModel.ReportId);
                    }

                    var transferOutData = JsonConvert.DeserializeObject<TransferOutStatementHeader>(reportModel.DataSource);
                    transferOutData.ReportId = reportModel.ReportId;
                    await _reportDbContext.TransferOutStatementHeaders.AddAsync(transferOutData);
                    transferOutData.Details.ForEach(x => x.ReportId = reportModel.ReportId);
                    await _reportDbContext.TransferOutStatementDetails.AddRangeAsync(transferOutData.Details);
                    await _reportDbContext.SaveChangesAsync();
                    break;
                case ReportType.GoodsInWard:
                    var goodsInWardReport = await _reportDbContext.GoodsInwardStatementHeaders.FirstOrDefaultAsync(x => x.ReportId == reportModel.ReportId);
                    
                    if(goodsInWardReport != null)
                    {
                        return Ok(reportModel.ReportId);
                    }

                    var goodsInwardData = JsonConvert.DeserializeObject<GoodsInwardStatementHeader>(reportModel.DataSource);
                    goodsInwardData.ReportId = reportModel.ReportId;
                    await _reportDbContext.GoodsInwardStatementHeaders.AddAsync(goodsInwardData);
                    goodsInwardData.Details.ForEach(x => x.ReportId = reportModel.ReportId);
                    await _reportDbContext.GoodsInwardStatementDetails.AddRangeAsync(goodsInwardData.Details);
                    await _reportDbContext.SaveChangesAsync();
                    break;
                case ReportType.SalesInvoice:
                    foreach (var entity in _reportDbContext.SalesInvoiceHeaders.Where(x => x.ReportId == reportModel.ReportId))
                    {
                        _reportDbContext.SalesInvoiceHeaders.Remove(entity);
                    }

                    foreach (var entity in _reportDbContext.SalesInvoiceDetails.Where(x => x.ReportId == reportModel.ReportId))
                    {
                        _reportDbContext.SalesInvoiceDetails.Remove(entity);
                    }
                    var salesInvoiceData = JsonConvert.DeserializeObject<SalesInvoiceHeader>(reportModel.DataSource);
                    salesInvoiceData.ReportId = reportModel.ReportId;
                    await _reportDbContext.SalesInvoiceHeaders.AddAsync(salesInvoiceData);
                    salesInvoiceData.Details.ForEach(x => x.ReportId = reportModel.ReportId);
                    await _reportDbContext.SalesInvoiceDetails.AddRangeAsync(salesInvoiceData.Details);
                    await _reportDbContext.SaveChangesAsync();
                    break;
                case ReportType.PurchaseOrder:
                    
                    var purchaseOrder = await _reportDbContext.PurchaseOrderStatementHeaders.FirstOrDefaultAsync(x => x.ReportId == reportModel.ReportId);

                    if (purchaseOrder != null)
                    {
                        return Ok(reportModel.ReportId);
                    }

                    var purchaseOrderData = JsonConvert.DeserializeObject<PurchaseOrderStatementHeader>(reportModel.DataSource);
                    purchaseOrderData.ReportId = reportModel.ReportId;
                    await _reportDbContext.PurchaseOrderStatementHeaders.AddAsync(purchaseOrderData);
                    purchaseOrderData.Details.ForEach(x => x.ReportId = reportModel.ReportId);
                    await _reportDbContext.PurchaseOrderStatementDetails.AddRangeAsync(purchaseOrderData.Details);
                    await _reportDbContext.SaveChangesAsync();
                    break;
                case ReportType.PurchaseReturn:
                    
                    var purchaseReturn = await _reportDbContext.PurchaseReturnStatementHeaders.FirstOrDefaultAsync(x => x.ReportId == reportModel.ReportId);

                    if (purchaseReturn != null)
                    {
                        return Ok(reportModel.ReportId);
                    }

                    var purchaseReturnData = JsonConvert.DeserializeObject<PurchaseReturnStatementHeader>(reportModel.DataSource);
                    purchaseReturnData.ReportId = reportModel.ReportId;
                    await _reportDbContext.PurchaseReturnStatementHeaders.AddAsync(purchaseReturnData);
                    purchaseReturnData.Details.ForEach(x => x.ReportId = reportModel.ReportId);
                    await _reportDbContext.PurchaseReturnStatementDetails.AddRangeAsync(purchaseReturnData.Details);
                    await _reportDbContext.SaveChangesAsync();
                    break;
            }
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Report([FromQuery] ReportType reportType, [FromQuery]string reportId)
        {
            await Task.Yield();
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            
            var webRoot = envName == "Local" ? $"{GetApplicationRoot()}\\Templates" : "Templates";
            var reportPath = Path.Combine(webRoot, ReportFactory.GetReportNameFromType(reportType));
            if (!System.IO.File.Exists(reportPath))
            {
                throw new FileNotFoundException($"Reporting server cannnot find template for report type { reportType.ToString() }");
            }
            var model = new WebReportModel()
            {
                WebReport = new WebReport()
            };

            var conn = new PostgresDataConnection()
            {
                ConnectionString = _configuration["ConnectionString"]
            };

            model.WebReport.Report.Load(reportPath);
            foreach (DataConnectionBase item in model.WebReport.Report.Dictionary.Connections)
            {
                item.ConnectionString = _configuration["ConnectionString"];
            }
            model.WebReport.Report.SetParameterValue("ReportId", reportId);

            return View(model);
        }

        private string GetApplicationRoot()
        {
            var exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var appRoot = appPathMatcher.Match(exePath).Value;
            return appRoot;
        }
    }
}