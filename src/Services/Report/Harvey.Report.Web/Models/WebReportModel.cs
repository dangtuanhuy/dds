﻿using FastReport.Web;

namespace Harvey.Report.Web.Models
{
    public class WebReportModel
    {
        public WebReport WebReport { get; set; }
    }
}
