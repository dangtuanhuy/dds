﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class updategoodsinwardstatement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "GoodsInwardStatementHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Website",
                table: "GoodsInwardStatementHeaders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "GoodsInwardStatementHeaders");

            migrationBuilder.DropColumn(
                name: "Website",
                table: "GoodsInwardStatementHeaders");
        }
    }
}
