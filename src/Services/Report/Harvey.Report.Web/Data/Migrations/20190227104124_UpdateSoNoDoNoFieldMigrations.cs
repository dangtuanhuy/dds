﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class UpdateSoNoDoNoFieldMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SoDo",
                table: "SalesInvoiceHeaders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SoDo",
                table: "SalesInvoiceHeaders");
        }
    }
}
