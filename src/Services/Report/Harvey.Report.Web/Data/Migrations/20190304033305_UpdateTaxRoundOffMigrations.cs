﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class UpdateTaxRoundOffMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TaxRoundOff",
                table: "SalesInvoiceHeaders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TaxRoundOff",
                table: "SalesInvoiceHeaders");
        }
    }
}
