﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class UpdatePropertyFaxMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Fax",
                table: "SalesInvoiceHeaders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fax",
                table: "SalesInvoiceHeaders");
        }
    }
}
