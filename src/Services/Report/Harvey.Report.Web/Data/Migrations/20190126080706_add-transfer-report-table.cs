﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class addtransferreporttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TransferInStatementDetails",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    BarCode = table.Column<string>(nullable: true),
                    InventoryCode = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    BinNo = table.Column<string>(nullable: true),
                    UOM = table.Column<string>(nullable: true),
                    TransQuantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferInStatementDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransferInStatementHeaders",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    FromLocation = table.Column<string>(nullable: true),
                    ToLocation = table.Column<string>(nullable: true),
                    TransferNo = table.Column<string>(nullable: true),
                    TransferDate = table.Column<string>(nullable: true),
                    TotalQuantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferInStatementHeaders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransferOutStatementHeaders",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    FromLocation = table.Column<string>(nullable: true),
                    ToLocation = table.Column<string>(nullable: true),
                    TransferNo = table.Column<string>(nullable: true),
                    TransferDate = table.Column<string>(nullable: true),
                    TotalQuantity = table.Column<int>(nullable: false),
                    TotalTransferPrice = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferOutStatementHeaders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransferOutStatementDetails",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    BarCode = table.Column<string>(nullable: true),
                    InventoryCode = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    BinNo = table.Column<string>(nullable: true),
                    UOM = table.Column<string>(nullable: true),
                    Price = table.Column<string>(nullable: true),
                    TransQuantity = table.Column<int>(nullable: false),
                    TotalProductPrice = table.Column<double>(nullable: false),
                    TransferOutStatementHeaderId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferOutStatementDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransferOutStatementDetails_TransferOutStatementHeaders_Tra~",
                        column: x => x.TransferOutStatementHeaderId,
                        principalTable: "TransferOutStatementHeaders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransferOutStatementDetails_TransferOutStatementHeaderId",
                table: "TransferOutStatementDetails",
                column: "TransferOutStatementHeaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransferInStatementDetails");

            migrationBuilder.DropTable(
                name: "TransferInStatementHeaders");

            migrationBuilder.DropTable(
                name: "TransferOutStatementDetails");

            migrationBuilder.DropTable(
                name: "TransferOutStatementHeaders");
        }
    }
}
