﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class AddSalesInvoiceTableMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SalesInvoiceDetails",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    InventoryCode = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    UOM = table.Column<string>(nullable: true),
                    Quantity = table.Column<string>(nullable: true),
                    UnitPrice = table.Column<string>(nullable: true),
                    TotalAmount = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesInvoiceDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SalesInvoiceHeaders",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    VendorName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    InvoiceNo = table.Column<string>(nullable: true),
                    InvoiceDate = table.Column<string>(nullable: true),
                    GSTRegNo = table.Column<string>(nullable: true),
                    PaymentTerms = table.Column<string>(nullable: true),
                    DueDate = table.Column<string>(nullable: true),
                    Discount = table.Column<string>(nullable: true),
                    SubTotal = table.Column<string>(nullable: true),
                    Tax = table.Column<string>(nullable: true),
                    NetTotal = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesInvoiceHeaders", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SalesInvoiceDetails");

            migrationBuilder.DropTable(
                name: "SalesInvoiceHeaders");
        }
    }
}
