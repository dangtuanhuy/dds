﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class updatereportstatement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BinNo",
                table: "TransferOutStatementDetails");

            migrationBuilder.DropColumn(
                name: "BinNo",
                table: "TransferInStatementDetails");

            migrationBuilder.AlterColumn<string>(
                name: "TotalTransferPrice",
                table: "TransferOutStatementHeaders",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<string>(
                name: "TotalProductPrice",
                table: "TransferOutStatementDetails",
                nullable: true,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "TotalTransferPrice",
                table: "TransferOutStatementHeaders",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "TotalProductPrice",
                table: "TransferOutStatementDetails",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BinNo",
                table: "TransferOutStatementDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BinNo",
                table: "TransferInStatementDetails",
                nullable: true);
        }
    }
}
