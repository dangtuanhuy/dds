﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class addpurchasecontrolmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GoodsInwardStatementDetails_GoodsInwardStatementHeaders_Goo~",
                table: "GoodsInwardStatementDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferOutStatementDetails_TransferOutStatementHeaders_Tra~",
                table: "TransferOutStatementDetails");

            migrationBuilder.DropIndex(
                name: "IX_TransferOutStatementDetails_TransferOutStatementHeaderId",
                table: "TransferOutStatementDetails");

            migrationBuilder.DropIndex(
                name: "IX_GoodsInwardStatementDetails_GoodsInwardStatementHeaderId",
                table: "GoodsInwardStatementDetails");

            migrationBuilder.DropColumn(
                name: "TransferOutStatementHeaderId",
                table: "TransferOutStatementDetails");

            migrationBuilder.DropColumn(
                name: "GoodsInwardStatementHeaderId",
                table: "GoodsInwardStatementDetails");

            migrationBuilder.CreateTable(
                name: "PurchaseOrderStatementDetails",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    InventoryCode = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    UOM = table.Column<string>(nullable: true),
                    Quantity = table.Column<string>(nullable: true),
                    Cost = table.Column<string>(nullable: true),
                    Discount = table.Column<string>(nullable: true),
                    TotalAmount = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseOrderStatementDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseOrderStatementHeaders",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    VendorName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    PONo = table.Column<string>(nullable: true),
                    PODate = table.Column<string>(nullable: true),
                    Delivery = table.Column<string>(nullable: true),
                    PaymentTerms = table.Column<string>(nullable: true),
                    GSTRegNo = table.Column<string>(nullable: true),
                    DiscountAmount = table.Column<string>(nullable: true),
                    TotalValue = table.Column<string>(nullable: true),
                    SubTotal = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true),
                    Tax = table.Column<string>(nullable: true),
                    NetTotal = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseOrderStatementHeaders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseReturnStatementDetails",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    InventoryCode = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    UOM = table.Column<string>(nullable: true),
                    Quantity = table.Column<string>(nullable: true),
                    UnitCost = table.Column<string>(nullable: true),
                    NetCost = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseReturnStatementDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseReturnStatementHeaders",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    VendorName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    PRNo = table.Column<string>(nullable: true),
                    PRDate = table.Column<string>(nullable: true),
                    GSTRegNo = table.Column<string>(nullable: true),
                    SubTotal = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true),
                    Tax = table.Column<string>(nullable: true),
                    NetTotal = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseReturnStatementHeaders", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PurchaseOrderStatementDetails");

            migrationBuilder.DropTable(
                name: "PurchaseOrderStatementHeaders");

            migrationBuilder.DropTable(
                name: "PurchaseReturnStatementDetails");

            migrationBuilder.DropTable(
                name: "PurchaseReturnStatementHeaders");

            migrationBuilder.AddColumn<Guid>(
                name: "TransferOutStatementHeaderId",
                table: "TransferOutStatementDetails",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "GoodsInwardStatementHeaderId",
                table: "GoodsInwardStatementDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransferOutStatementDetails_TransferOutStatementHeaderId",
                table: "TransferOutStatementDetails",
                column: "TransferOutStatementHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_GoodsInwardStatementDetails_GoodsInwardStatementHeaderId",
                table: "GoodsInwardStatementDetails",
                column: "GoodsInwardStatementHeaderId");

            migrationBuilder.AddForeignKey(
                name: "FK_GoodsInwardStatementDetails_GoodsInwardStatementHeaders_Goo~",
                table: "GoodsInwardStatementDetails",
                column: "GoodsInwardStatementHeaderId",
                principalTable: "GoodsInwardStatementHeaders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferOutStatementDetails_TransferOutStatementHeaders_Tra~",
                table: "TransferOutStatementDetails",
                column: "TransferOutStatementHeaderId",
                principalTable: "TransferOutStatementHeaders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
