﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Report.Web.Data.Migrations
{
    public partial class addgoodsinwardstatement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GoodsInwardStatementHeaders",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    VendorName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    GIDNo = table.Column<string>(nullable: true),
                    GIDDate = table.Column<string>(nullable: true),
                    InvoiceNo = table.Column<string>(nullable: true),
                    GSTRegNo = table.Column<string>(nullable: true),
                    Discount = table.Column<string>(nullable: true),
                    SubTotal = table.Column<string>(nullable: true),
                    Tax = table.Column<string>(nullable: true),
                    NetTotal = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoodsInwardStatementHeaders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GoodsInwardStatementDetails",
                columns: table => new
                {
                    ReportId = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    InventoryCode = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    UOM = table.Column<string>(nullable: true),
                    Quantity = table.Column<string>(nullable: true),
                    Cost = table.Column<string>(nullable: true),
                    TotalValue = table.Column<string>(nullable: true),
                    GoodsInwardStatementHeaderId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoodsInwardStatementDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoodsInwardStatementDetails_GoodsInwardStatementHeaders_Goo~",
                        column: x => x.GoodsInwardStatementHeaderId,
                        principalTable: "GoodsInwardStatementHeaders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GoodsInwardStatementDetails_GoodsInwardStatementHeaderId",
                table: "GoodsInwardStatementDetails",
                column: "GoodsInwardStatementHeaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GoodsInwardStatementDetails");

            migrationBuilder.DropTable(
                name: "GoodsInwardStatementHeaders");
        }
    }
}
