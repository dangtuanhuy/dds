﻿using Harvey.Reporting;
using Harvey.Reporting.ReportStatementModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Harvey.Report.Web.Data
{
    public class ReportDbContext : DbContext
    {
        public ReportDbContext(DbContextOptions options) : base(options)
        {
        }

        protected ReportDbContext()
        {
        }

        public DbSet<TransferInStatementHeader> TransferInStatementHeaders { get; set; }
        public DbSet<TransferInStatementDetail> TransferInStatementDetails { get; set; }
        public DbSet<TransferOutStatementHeader> TransferOutStatementHeaders { get; set; }
        public DbSet<TransferOutStatementDetail> TransferOutStatementDetails { get; set; }
        public DbSet<GoodsInwardStatementHeader> GoodsInwardStatementHeaders { get; set; }
        public DbSet<GoodsInwardStatementDetail> GoodsInwardStatementDetails { get; set; }
        public DbSet<SalesInvoiceHeader> SalesInvoiceHeaders { get; set; }
        public DbSet<SalesInvoiceDetail> SalesInvoiceDetails { get; set; }
        public DbSet<PurchaseOrderStatementHeader> PurchaseOrderStatementHeaders { get; set; }
        public DbSet<PurchaseOrderStatementDetail> PurchaseOrderStatementDetails { get; set; }
        public DbSet<PurchaseReturnStatementHeader> PurchaseReturnStatementHeaders { get; set; }
        public DbSet<PurchaseReturnStatementDetail> PurchaseReturnStatementDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<TransferInStatementHeader>());
            Setup(modelBuilder.Entity<TransferInStatementDetail>());
            Setup(modelBuilder.Entity<SalesInvoiceHeader>());
            Setup(modelBuilder.Entity<SalesInvoiceDetail>());
            Setup(modelBuilder.Entity<TransferOutStatementHeader>());
            Setup(modelBuilder.Entity<TransferOutStatementDetail>());
            Setup(modelBuilder.Entity<GoodsInwardStatementHeader>());
            Setup(modelBuilder.Entity<GoodsInwardStatementDetail>());
            Setup(modelBuilder.Entity<PurchaseOrderStatementHeader>());
            Setup(modelBuilder.Entity<PurchaseOrderStatementDetail>());
            Setup(modelBuilder.Entity<PurchaseReturnStatementHeader>());
            Setup(modelBuilder.Entity<PurchaseReturnStatementDetail>());
        }

        public void Setup(EntityTypeBuilder<TransferInStatementHeader> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Ignore(x => x.Details);
        }
        public void Setup(EntityTypeBuilder<TransferInStatementDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
        public void Setup(EntityTypeBuilder<TransferOutStatementHeader> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Ignore(x => x.Details);
        }
        public void Setup(EntityTypeBuilder<TransferOutStatementDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
        public void Setup(EntityTypeBuilder<GoodsInwardStatementHeader> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Ignore(x => x.Details);
        }
        public void Setup(EntityTypeBuilder<GoodsInwardStatementDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
        public void Setup(EntityTypeBuilder<SalesInvoiceHeader> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Ignore(x => x.Details);
        }
        public void Setup(EntityTypeBuilder<SalesInvoiceDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
        public void Setup(EntityTypeBuilder<PurchaseOrderStatementHeader> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Ignore(x => x.Details);
        }
        public void Setup(EntityTypeBuilder<PurchaseOrderStatementDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
        public void Setup(EntityTypeBuilder<PurchaseReturnStatementHeader> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Ignore(x => x.Details);
        }
        public void Setup(EntityTypeBuilder<PurchaseReturnStatementDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
    }
}
