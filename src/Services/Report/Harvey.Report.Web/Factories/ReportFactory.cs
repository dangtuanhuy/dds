﻿using Harvey.Reporting.Enums;

namespace Harvey.Report.Web.Factories
{
    public class ReportFactory
    {
        public static string GetReportNameFromType(ReportType reportType)
        {
            switch (reportType)
            {
                case ReportType.TransferIn:
                    return "StockTransferInStatement.frx";
                case ReportType.TransferOut:
                    return "StockTransferOutStatement.frx";
                case ReportType.GoodsInWard:
                    return "GoodsInwardStatement.frx";
                case ReportType.PurchaseOrder:
                    return "PurchaseOrderStatement.frx";
                case ReportType.PurchaseReturn:
                    return "PurchaseReturnStatement.frx";
                case ReportType.SalesInvoice:
                    return "SalesInvoice.frx";
                default:
                    throw new System.Exception($"Reporting server did not support report type {reportType.ToString()}");
            }
        }
    }
}
