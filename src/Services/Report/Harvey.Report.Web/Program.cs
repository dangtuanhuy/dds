﻿using Harvey.Report.Web.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Harvey.Report.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .Build()
                .MigrateDbContext<ReportDbContext>((context, services) =>
                {
                })
               .Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseDefaultServiceProvider(options => options.ValidateScopes = false);
    }
}
