﻿using Harvey.MarketingAutomation;
using Harvey.Promotion.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.Promotion.Application.PIM.Locations
{
    public class PimLocationFilter : IFeedFilter<Location>
    {
        public IEnumerable<Location> Filter(Guid CorrelationId, IEnumerable<Location> source)
        {
            return source;
        }
    }
}
