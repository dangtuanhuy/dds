﻿using Harvey.MarketingAutomation;
using Harvey.Promotion.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.Promotion.Application.PIM.Locations
{
    public class PimLocationConverter : IFeedConverter<Location, Location>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<Location>).GetType();

        public IEnumerable<Location> Convert(IEnumerable<Location> source)
        {
            return source.Select(x => new Location()
            {
                Id = x.Id,
                Name = x.Name,
                Address = x.Address,
                LocationCode = x.LocationCode,
                ContactPerson = x.ContactPerson,
                Phone1 = x.Phone1,
                Phone2 = x.Phone2
            });
        }
    }
}
