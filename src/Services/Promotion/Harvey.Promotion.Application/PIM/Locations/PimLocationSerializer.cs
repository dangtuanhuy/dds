﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Persitance.EF;
using Harvey.Promotion.Application.Infrastructure;
using Harvey.Promotion.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.PIM.Locations
{
    public class PimLocationSerializer : IFeedSerializer<Location>
    {
        private readonly IEfRepository<TransientPromotionDbContext, Location> _efRepository;
        private readonly TransientPromotionDbContext _promotionDbContext;
        public PimLocationSerializer(IEfRepository<TransientPromotionDbContext, Location> efRepository,
                                     TransientPromotionDbContext promotionDbContext)
        {
            _efRepository = efRepository;
            _promotionDbContext = promotionDbContext;
        }
        public async Task SerializeAsync(IEnumerable<Location> feedItems)
        {
            foreach (var item in feedItems)
            {
                var entity = await _efRepository.GetByIdAsync(item.Id);
                if (entity == null)
                {
                    entity = new Location()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Address = item.Address,
                        LocationCode = item.LocationCode,
                        ContactPerson = item.ContactPerson,
                        Phone1 = item.Phone1,
                        Phone2 = item.Phone2
                    };
                    await _efRepository.AddAsync(entity);
                }
                else
                {
                    entity.Name = item.Name;
                    entity.Address = item.Address;
                    entity.LocationCode = item.LocationCode;
                    entity.ContactPerson = item.ContactPerson;
                    entity.Phone1 = item.Phone1;
                    entity.Phone2 = item.Phone2;
                }

            }
            await _efRepository.SaveChangesAsync();

            var timeFeed = _promotionDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Location);
            if (timeFeed != null)
            {
                timeFeed.LastSync = DateTime.UtcNow;
                _promotionDbContext.TimeToFeeds.Update(timeFeed);
            }
            else
            {
                _promotionDbContext.TimeToFeeds.Add(new TimeToFeed
                {
                    LastSync = DateTime.UtcNow,
                    FeedType = FeedType.Location
                });
            }
            await _promotionDbContext.SaveChangesAsync();
        }
    }
}
