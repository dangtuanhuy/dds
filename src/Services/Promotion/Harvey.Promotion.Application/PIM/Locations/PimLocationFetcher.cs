﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Promotion.Application.Infrastructure;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace Harvey.Promotion.Application.PIM.Locations
{
    public class PimLocationFetcher : WebApiFetcherBase<Location>
    {
        public PimLocationFetcher(IConfiguration configuration, TransientPromotionDbContext dbContext) 
            : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.Location}" +
                  $"&lastSync={dbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Location)?.LastSync}")
        {

        }
    }
}
