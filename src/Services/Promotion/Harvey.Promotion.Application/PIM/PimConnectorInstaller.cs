﻿using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Products;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Promotion.Application.EventHandlers;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Harvey.Promotion.Application.PIM.Locations;
using Harvey.Promotion.Application.PIM.Products;
using System;

namespace Harvey.Promotion.Application.PIM
{
    public class PimConnectorInstaller
    {
        private Guid _id = Guid.Parse("BECE7E28-FF8E-4EBD-B94F-74424D7EFA19");
        private readonly string _jobName = "_pim_to_promotion";
        private readonly IEventBus _eventBus;

        public PimConnectorInstaller(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public void Install(ApplicationBuilder appBuilder)
        {
            appBuilder
                .AddConnector(_id, _jobName, _eventBus, (connectorRegistration) =>
                {
                    connectorRegistration
                     .AddProductSyncService(productSyncServiceRegistration =>
                     {
                         productSyncServiceRegistration
                         .UseSyncHandler<MarketingAutomationEvent<ProductCreatedEvent>, ProductCreatedEventHandler>();
                     })
                    .AddProductFeedService<Product, Product>(productFeedServiceRegistration =>
                    {
                        productFeedServiceRegistration
                        .UseFetcher<PimProductFetcher>()
                        .UseFilter<PimProductFilter>()
                        .UseConverter<PimProductConverter>()
                        .UseSerializer<PimProductSerializer>()
                        .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 5, 0));
                    }, rebuildOnExecution: true)
                    .AddVariantFeedService<Location, Location>(variantFeedServiceRegistration =>
                    {
                        variantFeedServiceRegistration
                        .UseFetcher<PimLocationFetcher>()
                        .UseFilter<PimLocationFilter>()
                        .UseConverter<PimLocationConverter>()
                        .UseSerializer<PimLocationSerializer>()
                        .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 5, 0));
                    }, rebuildOnExecution: true, type: FeedType.Location);
                    //TODO add more feed
                }).Merge();
        }
    }
}
