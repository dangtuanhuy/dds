﻿using Harvey.MarketingAutomation;
using Harvey.Promotion.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.Promotion.Application.PIM.Products
{
    public class PimProductConverter : IFeedConverter<Product, Product>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<Product>).GetType();

        public IEnumerable<Product> Convert(IEnumerable<Product> source)
        {
            return source.Select(x => new Product()
            {
                Id = x.Id,
                Description = x.Description,
                Name = x.Name,
                IsDelete = x.IsDelete,
                UpdatedDate = x.UpdatedDate
            });
        }
    }
}
