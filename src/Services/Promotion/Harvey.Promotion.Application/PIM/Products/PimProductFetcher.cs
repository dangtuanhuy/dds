﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Promotion.Application.Infrastructure;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace Harvey.Promotion.Application.PIM.Products
{
    public class PimProductFetcher : WebApiFetcherBase<Product>
    {
        public PimProductFetcher(IConfiguration configuration, TransientPromotionDbContext dbContext) 
            : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.Product}" +
                  $"&lastSync={dbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Product)?.LastSync}")
        {

        }
    }
}
