﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Promotion.Application.Infrastructure;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.PIM.Products
{
    public class PimProductSerializer : IFeedSerializer<Product>
    {
        private readonly TransientPromotionDbContext _promotionDbContext;
        private readonly IConfiguration _configuration;
        public PimProductSerializer(TransientPromotionDbContext promotionDbContext,
             IConfiguration configuration)
        {
            _promotionDbContext = promotionDbContext;
            _configuration = configuration;
        }
        public async Task SerializeAsync(IEnumerable<Product> feedItems)
        {
            var allFeedProductIds = feedItems.Select(x => x.Id);
            var allProductIds = _promotionDbContext.Products.Where(x => allFeedProductIds.Contains(x.Id)).Select(x => x.Id).ToList();

            var newProducts = feedItems.Where(x => !allProductIds.Contains(x.Id)).Select(item =>
            {
                return new Product()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    IsDelete = item.IsDelete
                };
            });

            var updateProducts = feedItems.Where(x => allProductIds.Contains(x.Id));

            var optionsBuilder = new DbContextOptionsBuilder<PromotionDbContext>();
            optionsBuilder.UseNpgsql(_configuration["ConnectionString"]);

            if (newProducts != null && newProducts.Any())
            {
                AddProducts(optionsBuilder.Options, newProducts);
            }
            if (updateProducts != null && updateProducts.Any())
            {
                UpdateProducts(optionsBuilder.Options, updateProducts);
            }
            

            var timeFeed = _promotionDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Product);
            if (timeFeed != null)
            {
                timeFeed.LastSync = feedItems.LastOrDefault().UpdatedDate;
                _promotionDbContext.TimeToFeeds.Update(timeFeed);
            }
            else
            {
                _promotionDbContext.TimeToFeeds.Add(new TimeToFeed
                {
                    LastSync = feedItems.LastOrDefault().UpdatedDate,
                    FeedType = FeedType.Product
                });
            }
            await _promotionDbContext.SaveChangesAsync();
        }

        private void AddProducts(DbContextOptions<PromotionDbContext> dbOption, IEnumerable<Product> newProducts)
        {
            int index = 0;
            while (true)
            {
                var currentData = newProducts.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PromotionDbContext(dbOption))
                {
                    dbContext.AddRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }

        private void UpdateProducts(DbContextOptions<PromotionDbContext> catalogOption, IEnumerable<Product> updateProducts)
        {
            int index = 0;
            while (true)
            {
                var currentData = updateProducts.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PromotionDbContext(catalogOption))
                {
                    dbContext.UpdateRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }
    }
}
