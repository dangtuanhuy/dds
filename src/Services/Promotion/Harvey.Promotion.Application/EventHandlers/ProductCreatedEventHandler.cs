﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Products;
using Harvey.MarketingAutomation.Connectors;
using Harvey.Persitance.EF;
using Harvey.Promotion.Application.Infrastructure;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.EventHandlers
{
    public class ProductCreatedEventHandler : EventHandlerBase<MarketingAutomationEvent<ProductCreatedEvent>>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEfRepository<TransientPromotionDbContext, Product> _efRepository;
        private readonly IEventBus _eventBus;

        public ProductCreatedEventHandler(IRepository<IdentifiedEvent> repository,
                                        ILogger<EventHandlerBase<MarketingAutomationEvent<ProductCreatedEvent>>> logger,
                                        ConnectorInfoCollection connectorInfos,
                                        IEfRepository<TransientPromotionDbContext, Product> efRepository,
                                        IEventBus eventBus) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _efRepository = efRepository;
            _eventBus = eventBus;
        }

        protected override async Task ExecuteAsync(MarketingAutomationEvent<ProductCreatedEvent> @event)
        {
            var product = _efRepository.GetByIdAsync(@event.CorrelationId.Value);
            if (product != null)
            {
                return;
            }
            await _efRepository.AddAsync(new Product()
            {
                Id = Guid.Parse(@event.InnerEvent.AggregateId),
                Name = @event.InnerEvent.Name,
                Description = @event.InnerEvent.Description
            });
            await _efRepository.SaveChangesAsync();
        }
    }
}
