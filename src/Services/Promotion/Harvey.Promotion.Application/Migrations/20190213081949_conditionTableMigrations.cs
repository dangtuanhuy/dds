﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Promotion.Application.Migrations
{
    public partial class conditionTableMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Conditions",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Conditions");
        }
    }
}
