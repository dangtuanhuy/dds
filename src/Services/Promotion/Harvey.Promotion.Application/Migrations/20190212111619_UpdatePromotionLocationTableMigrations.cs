﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Promotion.Application.Migrations
{
    public partial class UpdatePromotionLocationTableMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                table: "PromotionLocations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "PromotionLocations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "PromotionLocations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "IdentifiedId",
                table: "PromotionLocations",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedBy",
                table: "PromotionLocations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "PromotionLocations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "PromotionLocations");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "PromotionLocations");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "PromotionLocations");

            migrationBuilder.DropColumn(
                name: "IdentifiedId",
                table: "PromotionLocations");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "PromotionLocations");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "PromotionLocations");
        }
    }
}
