﻿using Harvey.Promotion.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Migrations
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<PromotionDbContext>
    {
        public PromotionDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PromotionDbContext>();
            var connectionString = "Server=localhost;port=5432;Database=harveypromotion;UserId=postgres;Password=123456";
            builder.UseNpgsql(connectionString);
            return new PromotionDbContext(builder.Options);
        }
    }
}
