﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Promotion.Application.Migrations
{
    public partial class updateCouponcodeTableMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "CouponCodes",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "CouponCodes");
        }
    }
}
