﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Promotion.Application.Migrations
{
    public partial class addfeedtime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TimeToFeeds",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    LastSync = table.Column<DateTime>(nullable: false),
                    FeedType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeToFeeds", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TimeToFeeds");
        }
    }
}
