﻿using Harvey.Domain;
using Harvey.Promotion.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class Promotions : Identifiable, IAuditable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int PromotionTypeId { get; set; }
        public virtual PromotionType PromotionType { get; set; }
        public Guid PromotionDetailId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public PromotionStatus Status { get; set; }
        public ICollection<Location> Locations { get; set; }
        public IList<PromotionLocation> PromotionLocations { get; set; }
        public virtual PromotionDetail PromotionDetail { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
