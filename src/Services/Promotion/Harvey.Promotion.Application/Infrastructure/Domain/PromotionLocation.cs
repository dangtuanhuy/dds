﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class PromotionLocation : Identifiable, IAuditable
    {
            public Guid PromotionId { get; set; }
            public Promotions Promotion { get; set; }
            public Guid LocationId { get; set; }
            public Location Location { get; set; }
            public Guid CreatedBy { get; set; }
            public Guid UpdatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime UpdatedDate { get; set; }
    }
}
