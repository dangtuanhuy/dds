﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class ConditionType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public virtual ICollection<Condition> Conditions { get; set; }
    }
}
