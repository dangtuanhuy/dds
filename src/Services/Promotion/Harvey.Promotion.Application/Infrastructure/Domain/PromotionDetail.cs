﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class PromotionDetail : Identifiable, IAuditable
    {
        public int DiscountTypeId { get; set; }
        public virtual DiscountType DiscountType { get; set; }
        public long Value { get; set; }
        public bool IsUseCouponCodes { get; set; }
        public bool IsUseConditions { get; set; }
        public virtual Promotions Promotion{ get; set; }
        public ICollection<CouponCode> CouponCodes { get; set; }
        public Condition Condition { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
