﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class Condition : Identifiable, IAuditable
    {
        public int OperatorTypeId { get; set; }
        public long Value { get; set; }
        public int ConditionTypeId { get; set; }
        public bool IsActive { get; set; }
        public virtual ConditionType ConditionType { get; set; }
        public virtual OperatorType OperatorType { get; set; }
        public PromotionDetail PromotionDetail { get; set; }
        public Guid PromotionDetailId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
