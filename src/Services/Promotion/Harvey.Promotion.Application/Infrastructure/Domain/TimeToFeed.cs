﻿using Harvey.Domain;
using Harvey.MarketingAutomation.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class TimeToFeed: EntityBase
    {
        public DateTime LastSync { get; set; }
        public FeedType FeedType { get; set; }
    }
}
