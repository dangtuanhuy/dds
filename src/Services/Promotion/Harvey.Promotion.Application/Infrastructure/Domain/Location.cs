﻿using Harvey.Domain;
using Harvey.MarketingAutomation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class Location : FeedItemBase
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string LocationCode { get; set; }
        public string ContactPerson { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public IList<PromotionLocation> PromotionLocations { get; set; }
    }
}
