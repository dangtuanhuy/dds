﻿using Harvey.Domain;
using Harvey.MarketingAutomation;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class Product : FeedItemBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }
    }
}
