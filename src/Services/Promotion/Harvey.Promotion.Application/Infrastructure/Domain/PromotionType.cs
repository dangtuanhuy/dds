﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class PromotionType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public virtual ICollection<Promotions> Promotions { get; set; }
    }
}
