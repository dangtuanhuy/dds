﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Domain
{
    public class CouponCode : Identifiable, IAuditable
    {
        public string Code { get; set; }
        public bool IsUsed { get; set; }
        public bool IsActive { get; set; }
        public Guid PromotionDetailId { get; set; }
        public PromotionDetail PromotionDetail { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
