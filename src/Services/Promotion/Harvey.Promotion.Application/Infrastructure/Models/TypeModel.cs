﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Models
{
    public class TypeModel
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}
