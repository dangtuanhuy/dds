﻿using System;

namespace Harvey.Promotion.Application.Infrastructure.Models
{
    public class ConditionModel
    {
        public Guid Id { get; set; }
        public int OperatorTypeId { get; set; }
        public long Value { get; set; }
        public int ConditionTypeId { get; set; }
        public bool IsActive { get; set; }
        public Guid MyProperty { get; set; }
        public Guid PromotionDetailId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
