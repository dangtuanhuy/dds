﻿using System;
using System.Collections.Generic;

namespace Harvey.Promotion.Application.Infrastructure.Models
{
    public class PromotionDetailModel
    {
        public Guid Id { get; set; }
        public int DiscountTypeId { get; set; }
        public string DiscountTypeName { get; set; }
        public long Value { get; set; }
        public bool IsUseCouponCodes { get; set; }
        public List<CouponCodeModel> CouponCodes { get; set; }
        public bool IsUseConditions { get; set; }
        public ConditionModel Condition { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
