﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Promotion.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.Promotion.Application.Infrastructure.Models
{
    public class PromotionModel: EntityBase
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PromotionTypeId { get; set; }
        public Guid PromotionDetailId { get; set; }
        public PromotionDetailModel PromotionDetail { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public PromotionStatus Status { get; set; }
        public List<LocationModel> Locations { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
