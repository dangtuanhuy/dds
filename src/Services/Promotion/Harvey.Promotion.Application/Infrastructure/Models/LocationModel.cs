﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Models
{
    public class LocationModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string LocationCode { get; set; }
        public string ContactPerson { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
    }
}
