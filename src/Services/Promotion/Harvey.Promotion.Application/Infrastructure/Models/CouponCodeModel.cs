﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.Models
{
    public class CouponCodeModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public bool IsUsed { get; set; }
        public bool IsActive { get; set; }
        public Guid PromotionDetailId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
