﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure.ActivityMigrations
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ActivityLogDbContext>
    {
        public ActivityLogDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ActivityLogDbContext>();
            var connectionString = "Server=localhost;port=5432;Database=harveypromotion;UserId=postgres;Password=123456";
            builder.UseNpgsql(connectionString);
            return new ActivityLogDbContext(builder.Options);
        }
    }
}
