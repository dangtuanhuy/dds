﻿using Harvey.Polly;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure
{
    public class DataSeedRetrivalPolicy : IRetrivalPolicy
    {
        public int NumbersOfRetrival => 3;

        public RetrivalStategy RetrivalStategy => RetrivalStategy.Exponential;

        public int Delay => 2;

        public List<System.Exception> HandledExceptions => new List<System.Exception>() { new System.Exception() };
    }
}
