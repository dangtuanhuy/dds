﻿using Harvey.Polly;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.Infrastructure
{
    public class PromotionDbContextDataSeed
    {
        public async Task SeedAsync(PromotionDbContext context, ILogger<PromotionDbContextDataSeed> logger)
        {
            var policy = new DataSeedRetrivalPolicy();
            await policy.ExecuteStrategyAsync(logger, () =>
            {
                using (context)
                {
                    if (!context.PromotionTypes.Any())
                    {
                        context.PromotionTypes.AddRange(GetPreconfiguredPromotionTypes());
                    }
                    if (!context.DiscountTypes.Any())
                    {
                        context.DiscountTypes.AddRange(GetPreconfiguredDiscountTypes());
                    }
                    if (!context.ConditionTypes.Any())
                    {
                        context.ConditionTypes.AddRange(GetPreconfiguredConditionTypes());
                    }
                    if (!context.OperatorTypes.Any())
                    {
                        context.OperatorTypes.AddRange(GetPreconfiguredOperatorTypes());
                    }
                    context.SaveChanges();
                }
            });
        }

        private List<PromotionType> GetPreconfiguredPromotionTypes()
        {
            return new List<PromotionType>()
            {
                new PromotionType()
                {
                    Id = 0,
                    TypeName = "Default"
                },
                new PromotionType()
                {
                    Id = 1,
                    TypeName = "Discount"
                },
                new PromotionType()
                {
                    Id = 2,
                    TypeName = "Bundle"
                },
                new PromotionType()
                {
                    Id = 3,
                    TypeName = "Discount Manual"
                }
            };
        }

        private List<DiscountType> GetPreconfiguredDiscountTypes()
        {
            return new List<DiscountType>()
            {
                new DiscountType()
                {
                    Id = 0,
                    TypeName = "Default"
                },
                new DiscountType()
                {
                    Id = 1,
                    TypeName = "Amount"
                },
                 new DiscountType()
                {
                    Id = 2,
                    TypeName = "Percent"
                }
            };
        }

        private List<ConditionType> GetPreconfiguredConditionTypes()
        {
            return new List<ConditionType>()
            {
                new ConditionType()
                {
                    Id = 0,
                    TypeName = "Default"
                },
                new ConditionType()
                {
                    Id = 1,
                    TypeName = "TotalPrice"
                },
                new ConditionType()
                {
                    Id = 2,
                    TypeName = "TotalQuantity"
                }
            };
        }

        private List<OperatorType> GetPreconfiguredOperatorTypes()
        {
            return new List<OperatorType>()
            {
                new OperatorType()
                {
                    Id = 1,
                    TypeName = "Equal"
                },
                new OperatorType()
                {
                    Id = 2,
                    TypeName = "NotEqual"
                },
                new OperatorType()
                {
                    Id = 3,
                    TypeName = "GreaterThan"
                },
                new OperatorType()
                {
                    Id = 4,
                    TypeName = "GreaterThanOrEqualTo"
                },
                new OperatorType()
                {
                    Id = 5,
                    TypeName = "LessThan"
                },
                new OperatorType()
                {
                    Id = 6,
                    TypeName = "LessThanOrEqualTo"
                }
            };
        }
    }

}
