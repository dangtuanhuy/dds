﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Harvey.Promotion.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.Infrastructure.Queries.Promotions
{
    public sealed class GetPromotionsQuery : IQuery<PagedResult<PromotionModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public GetPromotionsQuery(PagingFilterCriteria pagingFilterCriteria)
        {
            PagingFilterCriteria = pagingFilterCriteria;
        }
    }
    public sealed class GetPromotionsQueryHandler : IQueryHandler<GetPromotionsQuery, PagedResult<PromotionModel>>
    {
        private readonly IEfRepository<PromotionDbContext, Domain.Promotions, PromotionModel> _promotionRepository;
        private readonly PromotionDbContext _promotionDbContext;
        public GetPromotionsQueryHandler(IEfRepository<PromotionDbContext, Domain.Promotions, PromotionModel> promotionRepository,
            PromotionDbContext promotionDbContext)
        {
            _promotionRepository = promotionRepository;
            _promotionDbContext = promotionDbContext;
        }

        public async Task<PagedResult<PromotionModel>> Handle(GetPromotionsQuery query)
        {
            var promotionLocations = _promotionDbContext.PromotionLocations.Include(x => x.Location)
                .Select(x => new { x.PromotionId, x.Location }).ToList();
            var promotions = _promotionDbContext.Promotions.AsNoTracking()
                .Select(x => new Domain.Promotions()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    PromotionTypeId = x.PromotionTypeId,
                    PromotionType = x.PromotionType,
                    PromotionDetailId = x.PromotionDetailId,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Status = x.Status,
                    Locations = promotionLocations.Where(y => y.PromotionId == x.Id).Select(y => y.Location).ToList(),
                    PromotionDetail = x.PromotionDetail,
                    CreatedBy = x.CreatedBy,
                    CreatedDate = x.CreatedDate,
                    UpdatedBy = x.UpdatedBy,
                    UpdatedDate = x.UpdatedDate
                });
            var result = await _promotionRepository.GetAsync(promotions, query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                query.PagingFilterCriteria.SortColumn,query.PagingFilterCriteria.SortDirection);      
            var totalPages = await _promotionRepository.Count();
            return new PagedResult<PromotionModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = result
            };
        }
    }
}
