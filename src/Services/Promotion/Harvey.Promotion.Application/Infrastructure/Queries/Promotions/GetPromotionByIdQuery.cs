﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Promotion.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.Infrastructure.Queries.Promotions
{
    public sealed class GetPromotionByIdQuery : IQuery<PromotionModel>
    {
        public readonly Guid Id;
        public GetPromotionByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetPromotionByIdQueryHandler : IQueryHandler<GetPromotionByIdQuery, PromotionModel>
    {
        private readonly PromotionDbContext _promotionDbContext;
        private readonly IEfRepository<PromotionDbContext, Domain.Promotions> _repository;
        public GetPromotionByIdQueryHandler(
            PromotionDbContext promotionDbContext,
            IEfRepository<PromotionDbContext, Domain.Promotions> repository
            )
        {
            _promotionDbContext = promotionDbContext;
            _repository = repository;
        }

        public async Task<PromotionModel> Handle(GetPromotionByIdQuery query)
        {
            var promotion = await _repository.GetByIdAsync(query.Id);
            if (promotion != null)
            {
                var promotionModel = new PromotionModel()
                {
                    Id = promotion.Id,
                    Name = promotion.Name,
                    Description = promotion.Description,
                    PromotionTypeId = promotion.PromotionTypeId,
                    PromotionDetailId = promotion.PromotionDetailId,
                    FromDate = promotion.FromDate,
                    ToDate = promotion.ToDate,
                    Status = promotion.Status
                };

                var locations = _promotionDbContext.PromotionLocations
                    .Include(x => x.Location)
                    .Where(x => x.PromotionId == promotion.Id)
                    .Select(x => x.Location).ToList();
                promotionModel.Locations = locations.Select(x => new LocationModel() {
                    Id = x.Id,
                    Name = x.Name,
                    Address = x.Address,
                    LocationCode  = x.LocationCode,
                    ContactPerson  = x.ContactPerson,
                    Phone1 = x.Phone1,
                    Phone2 = x.Phone2
                }).ToList();

                var promotionDetail = _promotionDbContext.PromotionDetails
                    .Include(x => x.Condition)
                    .Include(x => x.CouponCodes)
                    .Include(x => x.DiscountType)
                    .SingleOrDefault(x => x.Id == promotion.PromotionDetailId);
                
                if (promotionDetail != null)
                {
                    promotionModel.PromotionDetail = new PromotionDetailModel()
                    {
                        Id = promotionDetail.Id,
                        DiscountTypeId = promotionDetail.DiscountTypeId,
                        DiscountTypeName = promotionDetail.DiscountType.TypeName,
                        Value = promotionDetail.Value,
                        IsUseCouponCodes = promotionDetail.IsUseCouponCodes,
                        IsUseConditions = promotionDetail.IsUseConditions
                    };

                    if(promotionDetail.CouponCodes != null && promotionDetail.CouponCodes.Count > 0)
                    {
                        promotionModel.PromotionDetail.CouponCodes = promotionDetail.CouponCodes.Select(x => new CouponCodeModel()
                        {
                            Id = x.Id,
                            Code = x.Code,
                            IsUsed = x.IsUsed,
                            IsActive = x.IsActive
                        }).ToList();
                    }
                    
                    if(promotionDetail.Condition != null)
                    {
                        promotionModel.PromotionDetail.Condition = new ConditionModel()
                        {
                            Id = promotionDetail.Condition.Id,
                            OperatorTypeId = promotionDetail.Condition.OperatorTypeId,
                            IsActive = promotionDetail.Condition.IsActive,
                            Value = promotionDetail.Condition.Value,
                            ConditionTypeId = promotionDetail.Condition.ConditionTypeId
                        };
                    }                                     
                }
                return promotionModel;
            }
            return null;
        }
    }
}
