﻿using Harvey.Domain;
using Harvey.Promotion.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.Infrastructure.Queries.Products
{
    public sealed class GetCountQuery : IQuery<CountModel>
    {
        public GetCountQuery() { }
    }

    public sealed class GetCountQueryHandler : IQueryHandler<GetCountQuery, CountModel>
    {
        private readonly PromotionDbContext _promotionDbContext;
        public GetCountQueryHandler(PromotionDbContext promotionDbContext)
        {
            _promotionDbContext = promotionDbContext;
        }
        public async Task<CountModel> Handle(GetCountQuery query)
        {
            var productCount = await _promotionDbContext.Products.AsNoTracking().CountAsync();

            return new CountModel()
            {
                Products = productCount,
                Variants = 0
            };
        }
    }
}
