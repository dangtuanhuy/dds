﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Promotion.Application.Infrastructure.IdentitfiedEventMigrations
{
    public partial class init_db_model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IdentifiedEvents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    EventId = table.Column<Guid>(nullable: false),
                    Data = table.Column<string>(nullable: true),
                    Handler = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentifiedEvents", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IdentifiedEvents");
        }
    }
}
