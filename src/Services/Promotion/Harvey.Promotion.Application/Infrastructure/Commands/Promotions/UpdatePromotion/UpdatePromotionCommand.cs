﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Harvey.Promotion.Application.Infrastructure.Enums;
using Harvey.Promotion.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.Infrastructure.Commands.Promotions.UpdatePromotion
{
    public sealed class UpdatePromotionCommand : ICommand<PromotionModel>
    {
        public Guid Creator { get; }
        public PromotionModel PromotionModel { get; }
        public UpdatePromotionCommand(Guid creator, PromotionModel promotionModel)
        {
            Creator = creator;
            PromotionModel = promotionModel;
        }
    }

    public sealed class UpdatePromotionCommandHandler : ICommandHandler<UpdatePromotionCommand, PromotionModel>
    {
        private readonly PromotionDbContext _promotionDbContext;
        public UpdatePromotionCommandHandler(
            PromotionDbContext promotionDbContext
            )
        {
            _promotionDbContext = promotionDbContext;
        }

        public async Task<PromotionModel> Handle(UpdatePromotionCommand command)
        {
            var entity = _promotionDbContext.Promotions.SingleOrDefault(x => x.Id == command.PromotionModel.Id);
            if (entity == null)
            {
                throw new ArgumentException($"promotion {command.PromotionModel.Id} is not presented.");
            }

            var promotionModel = command.PromotionModel;
            if (promotionModel == null)
            {
                throw new InvalidOperationException("Promotion is null.");
            }

            var promotionDetailModel = promotionModel.PromotionDetail;
            if (promotionDetailModel == null)
            {
                throw new InvalidOperationException("Promotion detail is null.");
            }

            var promotionDetail = _promotionDbContext.PromotionDetails.Include(x => x.CouponCodes).SingleOrDefault(x => x.Id == promotionModel.PromotionDetailId);
            var dtNow = DateTime.UtcNow;
            var codes = _promotionDbContext.CouponCodes.Where(x => x.PromotionDetailId == promotionDetail.Id);
            if (promotionDetailModel.IsUseCouponCodes && promotionDetailModel.CouponCodes != null && promotionDetailModel.CouponCodes.Any())
            {
                var couponCodes = codes.Select(x => x.Code);
                var couponCodeModels = promotionDetailModel.CouponCodes;
                var newCouponCodes = couponCodeModels.Where(x => !couponCodes.Contains(x.Code) && !string.IsNullOrEmpty(x.Code));

                foreach (var couponCodeModel in newCouponCodes)
                {
                    var couponCode = new CouponCode();
                    couponCode.Code = couponCodeModel.Code;
                    couponCode.IsUsed = false;
                    couponCode.IsActive = true;
                    couponCode.PromotionDetailId = promotionDetail.Id;
                    couponCode.CreatedBy = command.Creator;
                    couponCode.UpdatedBy = command.Creator;
                    couponCode.CreatedDate = dtNow;
                    couponCode.UpdatedDate = dtNow;

                    await _promotionDbContext.CouponCodes.AddAsync(couponCode);
                }
            }
            else if(!promotionDetailModel.IsUseCouponCodes)
            {
                var couponCodes = codes.AsEnumerable().Select(x => { x.IsActive = false; return x; });
                _promotionDbContext.CouponCodes.UpdateRange(couponCodes);
            }

            var condition = _promotionDbContext.Conditions.SingleOrDefault(x => x.PromotionDetailId == promotionDetail.Id);
            if (promotionDetailModel.IsUseConditions && promotionDetailModel.Condition != null)
            {
                var conditionModel = promotionDetailModel.Condition;          
                if (condition == null)
                {
                    condition = new Condition();
                    condition.CreatedBy = command.Creator;
                    condition.CreatedDate = dtNow;
                }
                condition.OperatorTypeId = conditionModel.OperatorTypeId;
                condition.Value = conditionModel.Value;
                condition.ConditionTypeId = conditionModel.ConditionTypeId;
                condition.IsActive = true;
                condition.CreatedBy = command.Creator;
                condition.UpdatedBy = command.Creator;
                condition.UpdatedDate = dtNow;
                if (condition.Id == Guid.Empty)
                {
                    condition.PromotionDetailId = promotionDetail.Id;
                    await _promotionDbContext.Conditions.AddAsync(condition);
                } else
                {
                    _promotionDbContext.Conditions.Update(condition);
                }
                
            } else
            {
                if (condition != null)
                {
                    condition.IsActive = false;
                    _promotionDbContext.Conditions.Update(condition);
                }
            }

            promotionDetail.DiscountTypeId = promotionDetailModel.DiscountTypeId;
            promotionDetail.Value = promotionDetailModel.Value;
            promotionDetail.IsUseCouponCodes = promotionDetailModel.IsUseCouponCodes;
            promotionDetail.IsUseConditions = promotionDetailModel.IsUseConditions;
            promotionDetail.UpdatedBy = command.Creator;
            promotionDetail.UpdatedDate = dtNow;
            _promotionDbContext.PromotionDetails.Update(promotionDetail);

            entity.Name = promotionModel.Name;
            entity.Description = promotionModel.Description;
            entity.PromotionTypeId = promotionModel.PromotionTypeId;
            entity.FromDate = promotionModel.FromDate;
            entity.ToDate = promotionModel.ToDate;
            entity.UpdatedBy = command.Creator;
            entity.UpdatedDate = dtNow;
            entity.Status = promotionModel.Status;
            _promotionDbContext.Promotions.Update(entity);

            var promotionLocations = _promotionDbContext.PromotionLocations.Where(x => x.PromotionId == promotionModel.Id);
            var promotionLocationsLocationIds = promotionLocations.Select(x => x.LocationId);

            var locationIds = promotionModel.Locations.ToList().Select(x => x.Id).ToList();
            if (locationIds != null)
            {
                var deleteLocation = promotionLocations.Where(x => !locationIds.Contains(x.LocationId));
                _promotionDbContext.PromotionLocations.RemoveRange(deleteLocation);
                    
                var newLocation = locationIds.Where(x => !promotionLocationsLocationIds.Contains(x));
                foreach (var item in newLocation)
                {
                    var promotionLocation = new PromotionLocation();
                    promotionLocation.LocationId = item;
                    promotionLocation.PromotionId = promotionModel.Id;
                    await _promotionDbContext.PromotionLocations.AddAsync(promotionLocation);
                }
            }
            await _promotionDbContext.SaveChangesAsync();

            promotionModel.CreatedBy = entity.CreatedBy;
            promotionModel.CreatedDate = entity.CreatedDate;
            promotionModel.UpdatedBy = entity.UpdatedBy;
            promotionModel.UpdatedDate = entity.UpdatedDate;

            return promotionModel;
        }
    }
}
