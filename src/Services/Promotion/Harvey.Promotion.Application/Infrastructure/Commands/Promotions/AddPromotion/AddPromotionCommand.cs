﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Harvey.Promotion.Application.Infrastructure.Enums;
using Harvey.Promotion.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.Infrastructure.Commands.Promotions.AddPromotion
{
    public sealed class AddPromotionCommand : ICommand<PromotionModel>
    {
        public Guid Creator { get; }
        public PromotionModel PromotionModel { get; }
        public AddPromotionCommand(Guid creator, PromotionModel promotionModel)
        {
            Creator = creator;
            PromotionModel = promotionModel;
        }
    }

    public sealed class AddPromotionCommandHandler : ICommandHandler<AddPromotionCommand, PromotionModel>
    {
        private readonly IMapper _mapper;
        private readonly PromotionDbContext _promotionDbContext;
        public AddPromotionCommandHandler(
            IMapper mapper,
            PromotionDbContext promotionDbContext
            )
        {
            _mapper = mapper;
            _promotionDbContext = promotionDbContext;
        }

        public async Task<PromotionModel> Handle(AddPromotionCommand command)
        {
            var promotionModel = command.PromotionModel;
            if (promotionModel == null)
            {
                throw new InvalidOperationException("Promotion is null.");
            }

            var promotionDetailModel = promotionModel.PromotionDetail;
            if (promotionDetailModel == null)
            {
                throw new InvalidOperationException("Promotion detail is null.");
            }

            var dtNow = DateTime.UtcNow;
            var promotionDetail = new PromotionDetail();
            promotionDetail.DiscountTypeId = promotionDetailModel.DiscountTypeId;
            promotionDetail.Value = promotionDetailModel.Value;
            promotionDetail.IsUseCouponCodes = promotionDetailModel.IsUseCouponCodes;
            promotionDetail.IsUseConditions = promotionDetailModel.IsUseConditions;
            promotionDetail.CreatedBy = command.Creator;
            promotionDetail.UpdatedBy = command.Creator;
            promotionDetail.CreatedDate = dtNow;
            promotionDetail.UpdatedDate = dtNow;

            await _promotionDbContext.PromotionDetails.AddAsync(promotionDetail);

            var promotion = new Domain.Promotions();
            promotion.PromotionDetailId = promotionDetail.Id;
            promotion.Name = promotionModel.Name;
            promotion.Description = promotionModel.Description;
            promotion.PromotionTypeId = promotionModel.PromotionTypeId;
            promotion.FromDate = promotionModel.FromDate;
            promotion.ToDate = promotionModel.ToDate;
            promotion.CreatedBy = command.Creator;
            promotion.UpdatedBy = command.Creator;
            promotion.CreatedDate = dtNow;
            promotion.UpdatedDate = dtNow;
            promotion.Status = PromotionStatus.Active;

            await _promotionDbContext.Promotions.AddAsync(promotion);

            var locationIds = promotionModel.Locations.ToList().Select(x => x.Id).ToList();
            if (locationIds.Any())
            {
                foreach (var item in locationIds)
                {
                    var promotionLocation = new PromotionLocation();
                    promotionLocation.LocationId = item;
                    promotionLocation.PromotionId = promotion.Id;

                    await _promotionDbContext.PromotionLocations.AddAsync(promotionLocation);
                }
            }

            if (promotionDetailModel.IsUseCouponCodes && promotionDetailModel.CouponCodes != null && promotionDetailModel.CouponCodes.Any())
            {
                var couponCodeModels = promotionDetailModel.CouponCodes;
                foreach (var couponCodeModel in couponCodeModels)
                {
                    var couponCode = new CouponCode();
                    couponCode.Code = couponCodeModel.Code;
                    couponCode.IsUsed = false;
                    couponCode.IsActive = true;
                    couponCode.PromotionDetailId = promotionDetail.Id;
                    couponCode.CreatedBy = command.Creator;
                    couponCode.UpdatedBy = command.Creator;
                    couponCode.CreatedDate = dtNow;
                    couponCode.UpdatedDate = dtNow;

                    await _promotionDbContext.CouponCodes.AddAsync(couponCode);
                    couponCodeModel.Id = couponCode.Id;
                    couponCodeModel.CreatedBy = command.Creator;
                    couponCodeModel.UpdatedBy = command.Creator;
                    couponCodeModel.IsActive = true;
                    couponCodeModel.CreatedDate = dtNow;
                    couponCodeModel.UpdatedDate = dtNow;
                    couponCodeModel.PromotionDetailId = promotionDetail.Id;
                }
            }

            if (promotionDetailModel.IsUseConditions && promotionDetailModel.Condition != null)
            {
                var conditionModel = promotionDetailModel.Condition;
                var condition = new Condition();
                condition.OperatorTypeId = conditionModel.OperatorTypeId;
                condition.Value = conditionModel.Value;
                condition.ConditionTypeId = conditionModel.ConditionTypeId;
                condition.PromotionDetailId = promotionDetail.Id;
                condition.CreatedBy = command.Creator;
                condition.UpdatedBy = command.Creator;
                condition.IsActive = true;
                condition.CreatedDate = dtNow;
                condition.UpdatedDate = dtNow;

                await _promotionDbContext.Conditions.AddAsync(condition);
                conditionModel.Id = condition.Id;
                conditionModel.CreatedBy = command.Creator;
                conditionModel.UpdatedBy = command.Creator;
                conditionModel.CreatedDate = dtNow;
                conditionModel.UpdatedDate = dtNow;
                conditionModel.PromotionDetailId = promotionDetail.Id;
            }
            await _promotionDbContext.SaveChangesAsync();

            promotionModel.Id = promotion.Id;
            promotionModel.CreatedBy = command.Creator;
            promotionModel.UpdatedBy = command.Creator;
            promotionModel.CreatedDate = dtNow;
            promotionModel.UpdatedDate = dtNow;
            promotionModel.PromotionDetailId = promotionDetail.Id;

            promotionModel.PromotionDetail.Id = promotionDetail.Id;
            promotionModel.PromotionDetail.CreatedBy = command.Creator;
            promotionModel.PromotionDetail.UpdatedBy = command.Creator;
            promotionModel.PromotionDetail.CreatedDate = dtNow;
            promotionModel.PromotionDetail.UpdatedDate = dtNow;

            return promotionModel;
        }
    }
}
