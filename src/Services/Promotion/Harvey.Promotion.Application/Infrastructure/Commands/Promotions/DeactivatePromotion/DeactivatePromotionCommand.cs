﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.Persitance.EF;
using Harvey.Promotion.Application.Infrastructure.Enums;
using Harvey.Promotion.Application.Infrastructure.Models;
using System;
using System.Threading.Tasks;

namespace Harvey.Promotion.Application.Infrastructure.Commands.Promotions.DeactivatePromotion
{
    public class DeactivatePromotionCommand : ICommand<PromotionModel>
    {
        public Guid Creator { get; }
        public Guid Id { get; }
        public PromotionStatus PromotionStatus { get; }
        public DeactivatePromotionCommand(Guid creator, Guid id, PromotionStatus promotionStatus)
        {
            Creator = creator;
            Id = id;
            PromotionStatus = promotionStatus;
        }
    }

    public sealed class DeactivatePromotionCommandHandler : ICommandHandler<DeactivatePromotionCommand, PromotionModel>
    {
        private readonly IEventBus _eventBus;
        private IEfRepository<PromotionDbContext, Domain.Promotions> _repository;
        private readonly IMapper _mapper;

        public DeactivatePromotionCommandHandler(
            IEfRepository<PromotionDbContext, Domain.Promotions> repository,
            IEventBus eventBus,
            IMapper mapper)
        {
            _eventBus = eventBus;
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<PromotionModel> Handle(DeactivatePromotionCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Promotion {command.Id} is not presented.");
            }
            entity.Status = command.PromotionStatus;
            entity.UpdatedBy = command.Creator;
            entity.UpdatedDate = DateTime.UtcNow;
            await _repository.SaveChangesAsync();
            return _mapper.Map<PromotionModel>(entity);
        }
    }
}
