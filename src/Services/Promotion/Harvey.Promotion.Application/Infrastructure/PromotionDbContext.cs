﻿using Harvey.Domain;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Harvey.Promotion.Application.Infrastructure
{

    public class PromotionDbContext : DbContext
    {
        public DbSet<Promotions> Promotions { get; set; }
        public DbSet<PromotionDetail> PromotionDetails { get; set; }
        public DbSet<CouponCode> CouponCodes { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<PromotionLocation> PromotionLocations { get; set; }
        public DbSet<DiscountType> DiscountTypes { get; set; }
        public DbSet<PromotionType> PromotionTypes { get; set; }
        public DbSet<AppSetting> AppSettings { get; set; }
        public DbSet<Condition> Conditions { get; set; }
        public DbSet<ConditionType> ConditionTypes { get; set; }
        public DbSet<OperatorType> OperatorTypes { get; set; }
        public DbSet<TimeToFeed> TimeToFeeds { get; set; }
        public PromotionDbContext(DbContextOptions<PromotionDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<Promotions>());
            Setup(modelBuilder.Entity<PromotionDetail>());
            Setup(modelBuilder.Entity<CouponCode>());
            Setup(modelBuilder.Entity<Location>());
            Setup(modelBuilder.Entity<Product>());
            Setup(modelBuilder.Entity<DiscountType>());
            Setup(modelBuilder.Entity<PromotionType>());
            Setup(modelBuilder.Entity<AppSetting>());
            Setup(modelBuilder.Entity<Condition>());
            Setup(modelBuilder.Entity<ConditionType>());
            Setup(modelBuilder.Entity<OperatorType>());
            modelBuilder.Entity<PromotionLocation>().HasKey(pl => new { pl.PromotionId, pl.LocationId });
        }

        public void Setup(EntityTypeBuilder<Promotions> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<PromotionDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasOne(x => x.Promotion)
                .WithOne(x => x.PromotionDetail)
                .HasForeignKey<Promotions>(x => x.PromotionDetailId);

            entityConfig
            .HasOne(x => x.Condition)
            .WithOne(x => x.PromotionDetail)
            .HasForeignKey<Condition>(x => x.PromotionDetailId);
        }

        public void Setup(EntityTypeBuilder<DiscountType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever();
        }

        public void Setup(EntityTypeBuilder<PromotionType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever();
        }


        public void Setup(EntityTypeBuilder<CouponCode> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasOne(s => s.PromotionDetail)
            .WithMany(g => g.CouponCodes)
            .HasForeignKey(s => s.PromotionDetailId);
        }

        public void Setup(EntityTypeBuilder<Location> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<Product> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<AppSetting> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<Condition> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<ConditionType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever();
        }

        public void Setup(EntityTypeBuilder<OperatorType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever();
        }
    }

    public class TransientPromotionDbContext : PromotionDbContext
    {
        public TransientPromotionDbContext(DbContextOptions<PromotionDbContext> options) : base(options)
        {
        }
    }
}
