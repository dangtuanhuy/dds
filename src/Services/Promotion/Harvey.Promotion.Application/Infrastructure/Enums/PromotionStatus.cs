﻿namespace Harvey.Promotion.Application.Infrastructure.Enums
{
    public enum PromotionStatus
    {
        Default = 0,
        Active = 1,
        InActive = 2
    }
}
