﻿using Harvey.Logging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Promotion.Application.Infrastructure
{
    public class ActivityLogDbContext : DbContext
    {
        public ActivityLogDbContext(DbContextOptions<ActivityLogDbContext> options) : base(options)
        {

        }

        public DbSet<ActivityLog> ActivityLogs { get; set; }
    }
}
