﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Promotion.API.Filters;
using Harvey.Promotion.Application.Infrastructure;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Harvey.Promotion.Application.Infrastructure.Models;
using Harvey.Promotion.Application.Infrastructure.Queries.Locations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Promotion.API.Controllers
{
    [Route("api/locations")]
    [Authorize]
    [ServiceFilter(typeof(ActivityTracking))]
    public class LocationsController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        private readonly IEfRepository<PromotionDbContext, Location, LocationModel> _efRepository;
        public LocationsController(
            IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor,
            IEfRepository<PromotionDbContext, Location, LocationModel> efRepository)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
            _efRepository = efRepository;
        }

        [HttpGet]
        [Route("all")]
        public async Task<ActionResult<IEnumerable<LocationModel>>> GetAll()
        {
            return Ok(await _efRepository.GetAsync());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<LocationModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetLocationByIdQuery(id));
            return result;
        }
    }
}
