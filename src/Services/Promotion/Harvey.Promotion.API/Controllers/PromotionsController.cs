﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Promotions;
using Harvey.EventBus.Events.Promotions.PromotionConditions;
using Harvey.EventBus.Events.Promotions.PromotionCouponCodes;
using Harvey.EventBus.Events.Promotions.PromotionDetails;
using Harvey.Promotion.API.Extentions;
using Harvey.Promotion.API.Filters;
using Harvey.Promotion.Application.Infrastructure;
using Harvey.Promotion.Application.Infrastructure.Commands.Promotions.AddPromotion;
using Harvey.Promotion.Application.Infrastructure.Commands.Promotions.DeactivatePromotion;
using Harvey.Promotion.Application.Infrastructure.Commands.Promotions.UpdatePromotion;
using Harvey.Promotion.Application.Infrastructure.Models;
using Harvey.Promotion.Application.Infrastructure.Queries.Products;
using Harvey.Promotion.Application.Infrastructure.Queries.Promotions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Promotion.API.Controllers
{
    [Route("api/promotions")]
    [Authorize(Policy = "Promotion")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class PromotionsController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        private readonly PromotionDbContext _promotionDbContext;
        private readonly IEventBus _eventBus;
        public PromotionsController(
            IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor,
            PromotionDbContext promotionDbContext,
             IEventBus eventBus
            )
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
            _promotionDbContext = promotionDbContext;
            _eventBus = eventBus;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<PromotionModel>>> GetAll(PagingFilterCriteria pagingFilterCriteria)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetPromotionsQuery(pagingFilterCriteria));
            return Ok(result);
        }

        [HttpGet()]
        [Route("{id}")]
        public async Task<ActionResult<PromotionModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetPromotionByIdQuery(id));
            return result;
        }

        [HttpGet]
        [Route("promotionTypes")]
        public async Task<ActionResult<IEnumerable<TypeModel>>> GetPromotionTypes()
        {
            var result = await _promotionDbContext.PromotionTypes.Where(x => x.Id != 0).Select(x => new TypeModel()
            {
                Id = x.Id,
                TypeName = x.TypeName
            }).ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("discountTypes")]
        public async Task<ActionResult<IEnumerable<TypeModel>>> GetDiscountTypes()
        {
            var result = await _promotionDbContext.DiscountTypes.Where(x => x.Id != 0).Select(x => new TypeModel()
            {
                Id = x.Id,
                TypeName = x.TypeName
            }).ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("conditionTypes")]
        public async Task<ActionResult<IEnumerable<TypeModel>>> GetConditionTypes()
        {
            var result = await _promotionDbContext.ConditionTypes.Where(x => x.Id != 0).Select(x => new TypeModel()
            {
                Id = x.Id,
                TypeName = x.TypeName
            }).ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("operatorTypes")]
        public async Task<ActionResult<IEnumerable<TypeModel>>> GetOperatorTypes()
        {
            var result = await _promotionDbContext.OperatorTypes.Select(x => new TypeModel()
            {
                Id = x.Id,
                TypeName = x.TypeName
            }).ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("coupon-codes")]
        public async Task<ActionResult<IEnumerable<CouponCodeModel>>> GetCouponCodes()
        {
            var result = await _promotionDbContext.CouponCodes.Select(x => new CouponCodeModel()
            {
                Id = x.Id,
                Code = x.Code,
                IsUsed = x.IsUsed,
                PromotionDetailId = x.PromotionDetailId
            }).ToListAsync();
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult<PromotionModel>> Add([FromBody] PromotionModel promotion)
        {
            var result = await _commandExecutor.ExecuteAsync(new AddPromotionCommand(User.GetUserId(), promotion));
            if (result != null && result.Id != Guid.Empty)
            {
                var locationIds = result.Locations?.Select(x => x.Id).ToList() ?? new List<Guid>();
                if (!locationIds.Any())
                {
                    return CreatedAtAction("Add", result);
                }

                try
                {
                    await _eventBus.PublishAsync(new PromotionCreatedEvent()
                    {
                        Id = result.Id,
                        Name = result.Name,
                        Description = result.Description,
                        PromotionTypeId = result.PromotionTypeId,
                        PromotionDetailId = result.PromotionDetailId,
                        FromDate = result.FromDate,
                        ToDate = result.ToDate,
                        Status = (EventBus.Events.Promotions.PromotionStatus)result.Status,
                        CreatedBy = result.CreatedBy,
                        UpdatedBy = result.UpdatedBy,
                        CreatedDate = result.CreatedDate,
                        UpdatedDate = result.UpdatedDate,
                        LocationIds = locationIds
                    });

                    var promotionDetailModel = result.PromotionDetail;
                    await _eventBus.PublishAsync(new PromotionDetailCreatedEvent()
                    {
                        Id = promotionDetailModel.Id,
                        DiscountTypeId = promotionDetailModel.DiscountTypeId,
                        Value = promotionDetailModel.Value,
                        IsUseCouponCodes = promotionDetailModel.IsUseCouponCodes,
                        IsUseConditions = promotionDetailModel.IsUseConditions,
                        CreatedBy = promotionDetailModel.CreatedBy,
                        UpdatedBy = promotionDetailModel.UpdatedBy,
                        CreatedDate = promotionDetailModel.CreatedDate,
                        UpdatedDate = promotionDetailModel.UpdatedDate,
                        LocationIds = locationIds
                    });

                    var couponCodes = promotionDetailModel.CouponCodes;
                    if (couponCodes != null && couponCodes.Any())
                    {
                        foreach (var couponCode in couponCodes)
                        {
                            await _eventBus.PublishAsync(new PromotionCouponCodeCreatedEvent()
                            {
                                Id = couponCode.Id,
                                Code = couponCode.Code,
                                IsUsed = couponCode.IsUsed,
                                PromotionDetailId = couponCode.PromotionDetailId,
                                CreatedBy = couponCode.CreatedBy,
                                UpdatedBy = couponCode.UpdatedBy,
                                CreatedDate = couponCode.CreatedDate,
                                UpdatedDate = couponCode.UpdatedDate,
                                LocationIds = locationIds
                            });
                        }
                    }

                    var promotionCondition = promotionDetailModel.Condition;
                    if (promotionCondition != null)
                    {
                        await _eventBus.PublishAsync(new PromotionConditionCreatedEvent()
                        {
                            Id = promotionCondition.Id,
                            OperatorTypeId = promotionCondition.OperatorTypeId,
                            Value = promotionCondition.Value,
                            ConditionTypeId = promotionCondition.ConditionTypeId,
                            PromotionDetailId = promotionCondition.PromotionDetailId,
                            CreatedBy = promotionCondition.CreatedBy,
                            UpdatedBy = promotionCondition.UpdatedBy,
                            CreatedDate = promotionCondition.CreatedDate,
                            UpdatedDate = promotionCondition.UpdatedDate,
                            LocationIds = locationIds
                        });
                    }

                }
                catch (System.Exception e)
                {

                    throw;
                }

                return CreatedAtAction("Add", result);
            }
            else
            {
                throw new InvalidOperationException("Cannot add promotion. Please try again.");
            }
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<ActionResult<PromotionModel>> Update(Guid id, [FromBody] PromotionModel promotion)
        {
            if (id == Guid.Empty || promotion == null)
            {
                return BadRequest();
            }
            var result = await _commandExecutor.ExecuteAsync(new UpdatePromotionCommand(User.GetUserId(), promotion));
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot not update promotion. Please try again.");
            }
        }

        [HttpPut("{id}/status/update/{value}")]
        public async Task<ActionResult> Deactivate(Guid id, Harvey.Promotion.Application.Infrastructure.Enums.PromotionStatus value)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Id is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new DeactivatePromotionCommand(User.GetUserId(), id, value));
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot not update promotion status. Please try again.");
            }
        }
    }
}
