﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.Promotion.Application.Infrastructure.Models;
using Harvey.Promotion.Application.Infrastructure.Queries.Products;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.Promotion.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        public readonly IQueryExecutor _queryExecutor;
        public ValuesController(IQueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }
        [HttpGet]
        [Route("count")]
        public async Task<ActionResult<CountModel>> Count()
        {
            return Ok(await _queryExecutor.ExecuteAsync(new GetCountQuery()));
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
