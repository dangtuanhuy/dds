﻿using AutoMapper;
using Hangfire;
using Hangfire.PostgreSql;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.EventStore.Marten;
using Harvey.EventBus.RabbitMQ;
using Harvey.EventBus.RabbitMQ.Policies;
using Harvey.Exception.Handlers;
using Harvey.Job;
using Harvey.Job.Hangfire;
using Harvey.Logging;
using Harvey.Logging.SeriLog;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Connectors;
using Harvey.Persitance.EF;
using Harvey.Promotion.API.Filters;
using Harvey.Promotion.Application.EventHandlers;
using Harvey.Promotion.Application.Infrastructure;
using Harvey.Promotion.Application.Infrastructure.Commands.Promotions.AddPromotion;
using Harvey.Promotion.Application.Infrastructure.Commands.Promotions.DeactivatePromotion;
using Harvey.Promotion.Application.Infrastructure.Commands.Promotions.UpdatePromotion;
using Harvey.Promotion.Application.Infrastructure.Domain;
using Harvey.Promotion.Application.Infrastructure.Enums;
using Harvey.Promotion.Application.Infrastructure.Models;
using Harvey.Promotion.Application.Infrastructure.Queries.Locations;
using Harvey.Promotion.Application.Infrastructure.Queries.Products;
using Harvey.Promotion.Application.Infrastructure.Queries.Promotions;
using Harvey.Promotion.Application.PIM;
using Harvey.Promotion.Application.PIM.Locations;
using Harvey.Promotion.Application.PIM.Products;
using Harvey.Promotion.Application.Services;
using Harvey.Search.Abstractions;
using Harvey.Search.NEST;
using Harvey.Setting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;

namespace Harvey.Promotion.API.Extentions
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IEventStore>(sp =>
            {
                return new MartenEventStore(configuration["ConnectionString"]);
            });
            services.AddTransient<LoggingEventHandler>();

            services.AddSingleton(sp =>
            {
                var logger = sp.GetRequiredService<ILogger<MasstransitPersistanceConnection>>();
                var endPoint = configuration["RabbitMqConfig:RabbitMqUrl"];
                var id = configuration["RabbitMqConfig:Username"];
                var pass = configuration["RabbitMqConfig:Password"];
                return new MasstransitPersistanceConnection(new BusCreationRetrivalPolicy(), logger, endPoint, id, pass);
            });

            services.AddSingleton<IEventBus>(sp =>
            {
                return new MasstransitEventBus("Harvey_Promotion_API", sp.GetRequiredService<MasstransitPersistanceConnection>(), sp);
            });

            services.AddDbContext<IdentifiedEventDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddTransient<IRepository<IdentifiedEvent>, EfRepository<IdentifiedEventDbContext, IdentifiedEvent>>();

            return services;
        }

        public static IServiceCollection AddServiceDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ActivityLogDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddDbContext<PromotionDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            });

            services.AddDbContext<TransientPromotionDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            return services;
        }

        public static IServiceCollection AddCQRS(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ICommandExecutor, CommandExecutor>();
            services.AddTransient<IQueryExecutor, QueryExecutor>();
            services.AddTransient<IQueryHandler<GetLocationByIdQuery, LocationModel>, GetLocationByIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetPromotionsQuery, PagedResult<PromotionModel>>, GetPromotionsQueryHandler>();
            services.AddTransient<IQueryHandler<GetPromotionByIdQuery, PromotionModel>, GetPromotionByIdQueryHandler>();

            services.AddTransient<ICommandHandler<AddPromotionCommand, PromotionModel>, AddPromotionCommandHandler>();
            services.AddTransient<ICommandHandler<UpdatePromotionCommand, PromotionModel>, UpdatePromotionCommandHandler>();
            services.AddTransient<ICommandHandler<DeactivatePromotionCommand, PromotionModel>, DeactivatePromotionCommandHandler>();

            services.AddTransient<IQueryHandler<GetCountQuery, CountModel>, GetCountQueryHandler>();
            return services;
        }

        public static IServiceCollection AddTrackingActivity(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ActivityTracking>();
            services.AddTransient<IEfRepository<ActivityLogDbContext, ActivityLog>, EfRepository<ActivityLogDbContext, ActivityLog>>();

            return services;
        }

        public static IServiceCollection AddLogger(this IServiceCollection services, IConfiguration configuration)
        {
            new SeriLogger().Initilize(new List<IDatabaseLoggingConfiguration>()
            {
                new DatabaseLoggingConfiguration()
                {
                    ConnectionString = configuration["Logging:DataLogger:ConnectionString"],
                    TableName = configuration["Logging:DataLogger:TableName"],
                    LogLevel = (LogLevel)Enum.Parse( typeof(LogLevel), configuration["Logging:DataLogger:LogLevel"],true)
                }
            }, new List<ICentralizeLoggingConfiguration>() {
                new CentralizeLoggingConfiguration()
                {
                    Url = configuration["Logging:CentralizeLogger:Url"],
                    LogLevel = (LogLevel)Enum.Parse( typeof(LogLevel), configuration["Logging:CentralizeLogger:LogLevel"],true)
                }
            });
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IEfRepository<TransientPromotionDbContext, Product>, EfRepository<TransientPromotionDbContext, Product>>();
            services.AddScoped<IEfRepository<TransientPromotionDbContext, Location>, EfRepository<TransientPromotionDbContext, Location>>();
            services.AddScoped<IEfRepository<PromotionDbContext, Location, LocationModel>, EfRepository<PromotionDbContext, Location, LocationModel>>();
            services.AddScoped<IEfRepository<PromotionDbContext, Promotions, PromotionModel>, EfRepository<PromotionDbContext, Promotions, PromotionModel>>();
            services.AddScoped<IEfRepository<PromotionDbContext, Promotions>, EfRepository<PromotionDbContext, Promotions>>();
            services.AddScoped<IEfRepository<TransientPromotionDbContext, PromotionLocation>, EfRepository<TransientPromotionDbContext, PromotionLocation>>();
            return services;
        }

        public static IServiceCollection AddAppSetting(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAppSettingService, AppSettingService>();
            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper();
            return services;
        }

        public static IServiceCollection AddExceptionHandlers(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ArgumentExceptionHandler>();
            services.AddScoped<EfUniqueConstraintExceptionHandler>();
            services.AddScoped<SqlExceptionHandler>();
            services.AddScoped<ForBiddenExceptionHandler>();
            services.AddScoped<NotFoundExceptionHandler>();
            services.AddScoped<BadModelExceptionHandler>();
            return services;
        }

        public static IServiceCollection AddSearchService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ISearchService, SearchService>();
            services.AddTransient<SearchSettings>(sp =>
            {
                return new SearchSettings(configuration["ELASTICSEARCHURL"]);
            });
            return services;
        }

        public static IServiceCollection AddJobManager(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IJobManager, HangfireJobManager>();
            services.AddHangfire(options => options.UseStorage(new PostgreSqlStorage(configuration["ConnectionString"]))
            .UseColouredConsoleLogProvider());
            return services;
        }

        public static IServiceCollection AddMarketingAutomation(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<ApplicationBuilder>();
            services.AddSingleton<ConnectorInfoCollection>();
            services.AddTransient<PimConnectorInstaller>();
            services.AddSingleton<FeedWorker>();
            services.AddTransient<ProductCreatedEventHandler>();
            services.AddTransient<PimProductFetcher>();
            services.AddTransient<PimProductFilter>();
            services.AddTransient<PimProductConverter>();
            services.AddTransient<PimProductSerializer>();

            services.AddTransient<PimLocationFetcher>();
            services.AddTransient<PimLocationFilter>();
            services.AddTransient<PimLocationConverter>();
            services.AddTransient<PimLocationSerializer>();
            return services;
        }
    }
}
