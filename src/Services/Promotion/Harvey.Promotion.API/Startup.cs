﻿using Harvey.Promotion.API.Extentions;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Harvey.Promotion.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Promotion",
                     policy => policy.RequireRole("Administrator", "SalesManager"));
            });

            services.AddHttpContextAccessor();

            services
                .AddServiceDbContext(Configuration)
                .AddEventBus(Configuration)
                .AddCQRS(Configuration)
                .AddLogger(Configuration)
                .AddMapper(Configuration)
                .AddRepositories(Configuration)
                .AddExceptionHandlers(Configuration)
                .AddTrackingActivity(Configuration)
                .AddSearchService(Configuration)
                .AddJobManager(Configuration)
                .AddMarketingAutomation(Configuration)
                .AddAppSetting(Configuration);


            services.AddAuthorization();

            services
                .AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication("Bearer", options =>
                {
                    options.Authority = Configuration["Authority"];
                    options.SupportedTokens = SupportedTokens.Reference;
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "harvey.rims.api";
                    options.ApiSecret = "secret";
                });

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowAnyOrigin();
                builder.AllowCredentials();
            });

            app.UseAuthentication();

            app.UseStaticFiles();

            app.ConfigureExceptionHandler(env);

            app.UseMvc();

            app.ConfigureJobManager(env);

            app.ConfigureMarketingAutomation();

            app.ConfigureEventBus();

        }
    }
}
