﻿using Harvey.Promotion.Application.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;


namespace Harvey.Promotion.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .Build()
                .MigrateDbContext<PromotionDbContext>((context, services) =>
                {
                    var logger = services.GetService<ILogger<PromotionDbContextDataSeed>>();
                    new PromotionDbContextDataSeed().SeedAsync(context, logger).Wait();
                })
                 .MigrateDbContext<IdentifiedEventDbContext>((context, services) =>
                 {
                     var logger = services.GetService<ILogger<IdentifiedEventDbContextDataSeed>>();
                     new IdentifiedEventDbContextDataSeed().SeedAsync(context, logger).Wait();
                 })
                  .MigrateDbContext<ActivityLogDbContext>((context, services) =>
                  {
                  })
                .Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseDefaultServiceProvider(options => options.ValidateScopes = false);
    }
}
