﻿using Hangfire.Annotations;
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Harvey.Promotion.API.Filters
{
    public class DashboardAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            return true;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
        }
    }
}
