﻿using Harvey.Promotion.Application.Infrastructure;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Harvey.Promotion.API.Filters
{
    public class EfUnitOfWork : IActionFilter
    {
        private readonly PromotionDbContext _promotionDbContext;
        public EfUnitOfWork(PromotionDbContext promotionDbContext)
        {
            _promotionDbContext = promotionDbContext;
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception == null)
            {
                _promotionDbContext.SaveChangesAsync().Wait();
            }
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }
    }
}
