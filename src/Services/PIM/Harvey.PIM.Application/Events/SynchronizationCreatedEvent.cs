﻿using Harvey.EventBus;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;

namespace Harvey.PIM.Application.Events
{
    public class SynchronizationCreatedEvent : EventBase
    {
        public SynchAction Action { get; set; }
        public SynchEntity Entity { get; set; }
        public Guid RefId { get; set; }
        public SynchronizationCreatedEvent()
        {
        }

        public SynchronizationCreatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
