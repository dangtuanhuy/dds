﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.FieldFramework.Services.Interface;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Harvey.PIM.Application.FieldFramework.Services.Implementation
{
    public class FieldTemplateService : IFieldTemplateService
    {
        private readonly PimDbContext _pimDbContext;
        private readonly IMapper _mapper;
        private readonly IEfRepository<PimDbContext, FieldTemplate> _repository;
        public FieldTemplateService(PimDbContext pimDbContext, IMapper mapper, IEfRepository<PimDbContext, FieldTemplate> repository)
        {
            _pimDbContext = pimDbContext;
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<PagedResult<FieldTemplate>> GetAllAsync(PagingFilterCriteria query, string queryText)
        {
            var result = string.IsNullOrEmpty(queryText) ? await _repository.GetAsync(query.Page, query.NumberItemsPerPage, query.SortColumn, query.SortDirection) 
                                                         : await _repository.ListAsync(x => x.Name.ToUpper().Contains(queryText.ToUpper()), query.Page, query.NumberItemsPerPage, query.SortColumn, query.SortDirection);
            var totalPages = string.IsNullOrEmpty(queryText) ? await _repository.Count()
                                                             : await _repository.Count(x => x.Name.ToUpper().Contains(queryText.ToUpper()));
            return new PagedResult<FieldTemplate>()
            {
                CurrentPage = query.Page,
                NumberItemsPerPage = query.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = result
            };
        }

        public async Task<FieldTemplateModel> GetAsync(Guid id)
        {
            var fieldTemplate = await _pimDbContext
                .FieldTemplates
                .Include(x => x.Field_FieldTemplates)
                .ThenInclude(x => x.Field)
                .FirstOrDefaultAsync(f => f.Id == id);
            if (fieldTemplate == null)
            {
                return null;
            }
            return _mapper.Map<FieldTemplate, FieldTemplateModel>(fieldTemplate);
        }

        public async Task<FieldTemplateModel> SaveAsync(Guid creator, FieldTemplateModel fieldTemplateModel)
        {
            var fieldTemplate = _mapper.Map<FieldTemplateModel, FieldTemplate>(fieldTemplateModel);
            if (fieldTemplate.Id == Guid.Empty)
            {
                fieldTemplate.CreatedDate = DateTime.UtcNow;
                fieldTemplate.CreatedBy = creator;
                fieldTemplate.UpdatedDate = DateTime.UtcNow;
                fieldTemplate.UpdatedBy = creator;
                _pimDbContext.Add(fieldTemplate);
                fieldTemplateModel.Id = fieldTemplate.Id;
            }
            else
            {
                fieldTemplate.UpdatedDate = DateTime.UtcNow;
                fieldTemplate.UpdatedBy = creator;
                _pimDbContext.Update(fieldTemplate);
            }

            var fields = fieldTemplateModel.Sections.SelectMany(x => x.Fields);

            var fieldFieldTemplates = _mapper.Map<FieldTemplateModel, List<Field_FieldTemplate>>(fieldTemplateModel);
            fieldFieldTemplates.ForEach(x =>
            {
                if (x.Id == Guid.Empty)
                {
                    _pimDbContext.Add(x);
                }
                else
                {
                    var field = fields.SingleOrDefault(f => f.Id == x.Id);
                    if (field != null)
                    {
                        switch (field.Action)
                        {
                            case ItemActionType.Update:
                                _pimDbContext.Update(x);
                                break;
                            case ItemActionType.Delete:
                                _pimDbContext.Remove(x);
                                break;
                        }
                    }
                }
            });
            await _pimDbContext.SaveChangesAsync();
            return await GetAsync(fieldTemplate.Id);
        }

        public async Task Delete(Guid id)
        {
            var fieldTemplate = _pimDbContext.FieldTemplates.Where(x => x.Id == id).SingleOrDefault();
            if (fieldTemplate == null)
                return;
            var product = _pimDbContext.Products.FirstOrDefault(x => x.FieldTemplateId == id);
            if (product != null)
            {
                throw new ArgumentException("Can not delete this field template!");
            }
            var field_fieldTemplate = _pimDbContext.Field_FieldTemplates.Where(x => x.FieldTemplateId == id).ToList();
            _pimDbContext.Field_FieldTemplates.RemoveRange(field_fieldTemplate);
            _pimDbContext.FieldTemplates.Remove(fieldTemplate);
            await _pimDbContext.SaveChangesAsync();
        }
    }
}
