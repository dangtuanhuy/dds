﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Extensions
{
    public class SecretKeyExtension
    {
        public const string SystemSecretKey = "B39D7816-6FC4-4B08-A160-8AC6225BBFED";
        public const string PurchaseOrderSecretKey = "BCC00A77-A444-4C62-A7F8-71156E9451BC";
        public const string RetailSecretKey = "6EC481F3-2C4A-4E0A-BFAF-C909E605BD1D";
    }
}
