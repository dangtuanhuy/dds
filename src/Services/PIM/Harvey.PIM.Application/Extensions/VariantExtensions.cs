﻿using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.PIM.Application.Extensions
{
    public static class VariantExtensions
    {
        public static string GetVariantName(List<DetailFieldValueModel> detailFieldValueModels)
        {
            return string.Join("|", detailFieldValueModels.Select(t => t.FieldName + ":" + t.FieldValue.ToString()));
        }

        public static string GetVariantDescription(string productName, List<DetailFieldValueModel> detailFieldValueModels)
        {
            return $"{productName}-{string.Join("-", detailFieldValueModels.Select(x => x.FieldValue).ToArray())}";
        }

        public static string GetVariantName(IEnumerable<FieldValue> fieldValues, IList<Field> fields)
        {
            var variantName = "";
            if (fieldValues.Any())
            {
                foreach (var fieldValue in fieldValues)
                {
                    var field = fields.FirstOrDefault(x => x.Id == fieldValue.FieldId);

                    if (fieldValues.IndexOf(fieldValue) == fieldValues.Count() - 1)
                    {
                        variantName += $"{field.Name}:{FieldValueFactory.GetFieldValueFromFieldType(field.Type, fieldValue)}";
                    }
                    else
                        variantName += $"{field.Name}:{FieldValueFactory.GetFieldValueFromFieldType(field.Type, fieldValue)}|";
                }
            }

            return variantName;
        }

        public static string GetVariantDescription(string productName, IList<FieldValue> fieldValues, IList<Field> fields)
        {
            //var variantDescription = "";
            var fieldValuesData = new List<string>();
            if (fieldValues.Any())
            {
                foreach (var fieldValue in fieldValues)
                {
                    var field = fields.FirstOrDefault(x => x.Id == fieldValue.FieldId);
                    fieldValuesData.Add(FieldValueFactory.GetFieldValueFromFieldType(field.Type, fieldValue));
                }

                return $"{productName}-{string.Join("-", fieldValuesData.ToArray())}";
            }

            return null;
        }

        public static string GetVariantName(IEnumerable<FieldValueModel> fieldValues, IList<Field> fields)
        {
            var variantName = "";
            if (fieldValues.Any())
            {
                foreach (var fieldValue in fieldValues)
                {
                    var field = fields.FirstOrDefault(x => x.Id == fieldValue.FieldId);

                    if (fieldValues.IndexOf(fieldValue) == fieldValues.Count() - 1)
                    {
                        variantName += $"{field.Name}:{fieldValue.FieldValue.ToString()}";
                    }
                    else
                        variantName += $"{field.Name}:{fieldValue.FieldValue.ToString()}|";

                }
            }

            return variantName;
        }

        public static string GetVariantDescription(string productName, List<FieldValueModel> detailFieldValueModels)
        {
            return $"{productName}-{string.Join("-", detailFieldValueModels.Select(x => x.FieldValue).ToArray())}";
        }
    }
}
