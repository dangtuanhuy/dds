﻿using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.Services
{
    public interface IAssignmentService
    {
        List<Guid> GetAssignmentBy(AssortmentAssignmentType assortmentAssignmentType, Guid channelId);
        bool IsAssignment(AssortmentAssignmentType assortmentAssignmentType, Guid channelId, Guid referenceId);
        Task ApplyChannelAssignmentToSynchronization(SynchAction synchAction, Guid channelId, List<ChannelAssignment> channelAssignments);
        Task ApplyAssortmentAssignmentToSynchronization(SynchAction synchAction, Guid channelId, List<AssortmentAssignment> assortmentAssignments);
        Task<ChannelStoreAssignment> GetChannelStoreAssignment(Guid channelId, Guid storeId);
    }
}
