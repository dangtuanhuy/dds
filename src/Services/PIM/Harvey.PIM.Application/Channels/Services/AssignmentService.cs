﻿using Harvey.Extensions;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Harvey.EventBus.Abstractions;
using System.Threading.Tasks;
using Harvey.PIM.Application.Events;

namespace Harvey.PIM.Application.Channels.Services
{
    public class AssignmentService : IAssignmentService
    {
        private readonly TransientPimDbContext _pimDbContext;
        private readonly IEventBus _eventBus;
        public AssignmentService(
            TransientPimDbContext pimDbContext,
            IEventBus eventBus)
        {
            _eventBus = eventBus;
            _pimDbContext = pimDbContext;
        }

        public async Task ApplyAssortmentAssignmentToSynchronization(SynchAction synchAction, Guid channelId, List<AssortmentAssignment> assortmentAssignments)
        {
            var channel = _pimDbContext.Channels.FirstOrDefault(x => x.Id == channelId);
            if (channel == null)
            {
                throw new System.Exception("Channel is not presented.");
            }
            var categoriesIds = assortmentAssignments.Where(x => x.EntityType == AssortmentAssignmentType.Category).Select(x => x.ReferenceId).ToList();
            var productIds = assortmentAssignments.Where(x => x.EntityType == AssortmentAssignmentType.Product).Select(x => x.ReferenceId).ToList();
            var allProductIdsOfCatagories = _pimDbContext.Products.Where(x => x.CategoryId.HasValue && categoriesIds.Contains(x.CategoryId.Value)).Select(x => x.Id).ToList();
            var extraProductIds = new List<Guid>();
            foreach (var item in productIds)
            {
                if (!allProductIdsOfCatagories.Any(x => x == item))
                {
                    allProductIdsOfCatagories.Add(item);
                }
            }

            foreach (var item in categoriesIds)
            {
                await _eventBus.PublishAsync(new SynchronizationCreatedEvent(channel.Id.ToString())
                {
                    Action = synchAction,
                    Entity = SynchEntity.Category,
                    RefId = item
                });
            }

            foreach (var item in allProductIdsOfCatagories)
            {
                await _eventBus.PublishAsync(new SynchronizationCreatedEvent(channel.Id.ToString())
                {
                    Action = synchAction,
                    Entity = SynchEntity.Product,
                    RefId = item
                });
            }
        }

        public async Task ApplyChannelAssignmentToSynchronization(SynchAction synchAction, Guid channelId, List<ChannelAssignment> channelAssignments)
        {
            var channel = _pimDbContext.Channels.FirstOrDefault(x => x.Id == channelId);
            if (channel == null)
            {
                throw new System.Exception("Channel is not presented.");
            }

            foreach (var assignment in channelAssignments)
            {
                switch (assignment.EntityType)
                {
                    case ChannelAssignmentType.Assortment:
                        var assortmentAssignments = _pimDbContext.AssortmentAssignments.Where(x => x.AssortmentId == assignment.ReferenceId);
                        await ApplyAssortmentAssignmentToSynchronization(synchAction, channelId, assortmentAssignments.ToList());
                        break;
                }
            }
        }

        public List<Guid> GetAssignmentBy(AssortmentAssignmentType assortmentAssignmentType, Guid channelId)
        {
            var assormentIds = _pimDbContext
                                .ChannelAssignments
                                .AsNoTracking()
                                .Where(x => x.ChannelId == channelId && x.EntityType == ChannelAssignmentType.Assortment)
                                .Select(x => x.ReferenceId).ToArray();
            if (!assormentIds.Any())
            {
                return new List<Guid>();
            }

            var feedTypePredicate = PredicateBuilder.False<AssortmentAssignment>();
            var assortmentPredicate = PredicateBuilder.False<AssortmentAssignment>();
            foreach (var item in assormentIds)
            {
                assortmentPredicate = assortmentPredicate.Or(x => x.AssortmentId == item);
            }
            switch (assortmentAssignmentType)
            {
                case AssortmentAssignmentType.Category:
                    feedTypePredicate = feedTypePredicate.Or(x => x.EntityType == AssortmentAssignmentType.Category);
                    break;
                case AssortmentAssignmentType.Product:
                    feedTypePredicate = feedTypePredicate.Or(x => x.EntityType == AssortmentAssignmentType.Category);
                    feedTypePredicate = feedTypePredicate.Or(x => x.EntityType == AssortmentAssignmentType.Product);
                    break;
                default:
                    return new List<Guid>();
            }
            var predicate = feedTypePredicate.And(assortmentPredicate);

            var assignments = _pimDbContext
                .AssortmentAssignments
                .AsNoTracking()
                .Where(predicate)
                .ToList();

            var cateIds = assignments.Where(x => x.EntityType == AssortmentAssignmentType.Category).Select(x => x.ReferenceId).ToList();
            var productIds = _pimDbContext.Products.Where(x => x.CategoryId != null && cateIds.Contains(x.CategoryId.Value)).Select(x => x.Id).ToList();

            return assignments.Select(x => x.ReferenceId).Union(productIds).ToList();

        }

        public bool IsAssignment(AssortmentAssignmentType assortmentAssignmentType, Guid channelId, Guid referenceId)
        {
            var ids = GetAssignmentBy(assortmentAssignmentType, channelId);
            return ids.Contains(referenceId);
        }

        public Task<ChannelStoreAssignment> GetChannelStoreAssignment(Guid channelId, Guid storeId)
        {
            return _pimDbContext.ChannelStoreAssignments.FirstOrDefaultAsync(x => x.ChannelId == channelId && x.StoreId == storeId);
        }
    }
}
