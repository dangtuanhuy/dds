﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Promotions.PromotionConditions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.Promotions.PromotionConditions
{
    public class PromotionConditionCreatedEventHander : EventHandlerBase<PromotionConditionCreatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, CatalogPromotionCondition> _promotionConditionRepository;
        private readonly TransientPimDbContext _transientPimDbContext;

        public PromotionConditionCreatedEventHander(
            IRepository<IdentifiedEvent> repository,
             ILogger<EventHandlerBase<PromotionConditionCreatedEvent>> logger,
             IEfRepository<TransientPimDbContext, CatalogPromotionCondition> promotionConditionRepository,
             TransientPimDbContext transientPimDbContext
            ) : base(repository, logger)
        {
            _promotionConditionRepository = promotionConditionRepository;
            _transientPimDbContext = transientPimDbContext;
        }

        protected override async Task ExecuteAsync(PromotionConditionCreatedEvent @event)
        {
            var locationIds = @event.LocationIds;
            if (locationIds != null && locationIds.Any())
            {
                var promotionCondition = new CatalogPromotionCondition()
                {
                    Id = @event.Id,
                    OperatorTypeId = @event.OperatorTypeId,
                    Value = @event.Value,
                    ConditionTypeId = @event.ConditionTypeId,
                    PromotionDetailId = @event.PromotionDetailId,
                    CreatedBy = @event.CreatedBy,
                    UpdatedBy = @event.UpdatedBy,
                    CreatedDate = @event.CreatedDate,
                    UpdatedDate = @event.UpdatedDate
                };
                
                foreach (var locationId in locationIds)
                {
                    await AddPromotionConditionToChannel(locationId, promotionCondition);
                }
            }
        }

        private async Task AddPromotionConditionToChannel(Guid storeId, CatalogPromotionCondition data)
        {
            var channelId = (await _transientPimDbContext.ChannelStoreAssignments.FirstOrDefaultAsync(x => x.StoreId == storeId)).ChannelId;
            if (channelId == Guid.Empty)
            {
                return;
            }
            var channelServerInfomation = (await _transientPimDbContext.Channels.FirstOrDefaultAsync(x => x.Id == channelId)).ServerInformation;

            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channelServerInfomation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var entity = await dbContext.PromotionConditions.FirstOrDefaultAsync(x => x.Id == data.Id);
                if (entity == null)
                {
                    await dbContext.PromotionConditions.AddAsync(data);
                }
                else
                {
                    dbContext.Entry(entity).State = EntityState.Modified;
                    entity.OperatorTypeId = data.OperatorTypeId;
                    entity.Value = data.Value;
                    entity.ConditionTypeId = data.ConditionTypeId;
                    entity.PromotionDetailId = data.PromotionDetailId;
                    entity.CreatedBy = data.CreatedBy;
                    entity.UpdatedBy = data.UpdatedBy;
                    entity.CreatedDate = data.CreatedDate;
                    entity.UpdatedDate = data.UpdatedDate;
                }
                await dbContext.SaveChangesAsync();
            };
        }
    }
}
