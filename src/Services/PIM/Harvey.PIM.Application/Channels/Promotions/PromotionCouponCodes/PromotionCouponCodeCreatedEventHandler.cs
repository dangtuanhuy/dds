﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Promotions.PromotionCouponCodes;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.Promotions.PromotionCouponCodes
{
    public class PromotionCouponCodeCreatedEventHandler : EventHandlerBase<PromotionCouponCodeCreatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, CatalogPromotionCouponCode> _promotionCouponCodeRepository;
        private readonly TransientPimDbContext _transientPimDbContext;
        public PromotionCouponCodeCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
             ILogger<EventHandlerBase<PromotionCouponCodeCreatedEvent>> logger,
             IEfRepository<TransientPimDbContext, CatalogPromotionCouponCode> promotionCouponCodeRepository,
             TransientPimDbContext transientPimDbContext
            ) : base(repository, logger)
        {
            _promotionCouponCodeRepository = promotionCouponCodeRepository;
            _transientPimDbContext = transientPimDbContext;
        }

        protected override async Task ExecuteAsync(PromotionCouponCodeCreatedEvent @event)
        {
            var locationIds = @event.LocationIds;
            if (locationIds != null && locationIds.Any())
            {
                var promotionCouponCode = new CatalogPromotionCouponCode()
                {
                    Id = @event.Id,
                    Code = @event.Code,
                    IsUsed = @event.IsUsed,
                    PromotionDetailId = @event.PromotionDetailId,
                    CreatedBy = @event.CreatedBy,
                    UpdatedBy = @event.UpdatedBy,
                    CreatedDate = @event.CreatedDate,
                    UpdatedDate = @event.UpdatedDate
                };

                foreach (var locationId in locationIds)
                {
                    await AddPromotionCouponCodeToChannel(locationId, promotionCouponCode);
                }
            }
        }

        private async Task AddPromotionCouponCodeToChannel(Guid storeId, CatalogPromotionCouponCode data)
        {
            var channelId = (await _transientPimDbContext.ChannelStoreAssignments.FirstOrDefaultAsync(x => x.StoreId == storeId)).ChannelId;
            if (channelId == Guid.Empty)
            {
                return;
            }
            var channelServerInfomation = (await _transientPimDbContext.Channels.FirstOrDefaultAsync(x => x.Id == channelId)).ServerInformation;

            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channelServerInfomation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var entity = await dbContext.PromotionCouponCodes.FirstOrDefaultAsync(x => x.Id == data.Id);
                if (entity == null)
                {
                    await dbContext.PromotionCouponCodes.AddAsync(data);
                }
                else
                {
                    dbContext.Entry(entity).State = EntityState.Modified;
                    entity.Code = data.Code;
                    entity.IsUsed = data.IsUsed;
                    entity.PromotionDetailId = data.PromotionDetailId;
                    entity.CreatedBy = data.CreatedBy;
                    entity.UpdatedBy = data.UpdatedBy;
                    entity.CreatedDate = data.CreatedDate;
                    entity.UpdatedDate = data.UpdatedDate;
                }
                await dbContext.SaveChangesAsync();
            };
        }
    }
}
