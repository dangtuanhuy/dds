﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Promotions.PromotionDetails;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.Promotions.PromotionDetails
{
    public class PromotionDetailCreatedEventHandler : EventHandlerBase<PromotionDetailCreatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, CatalogPromotionDetail> _promotionDetailRepository;
        private readonly TransientPimDbContext _transientPimDbContext;
        public PromotionDetailCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
             ILogger<EventHandlerBase<PromotionDetailCreatedEvent>> logger,
             IEfRepository<TransientPimDbContext, CatalogPromotionDetail> promotionDetailRepository,
             TransientPimDbContext transientPimDbContext
            ) : base(repository, logger)
        {
            _promotionDetailRepository = promotionDetailRepository;
            _transientPimDbContext = transientPimDbContext;
        }

        protected override async Task ExecuteAsync(PromotionDetailCreatedEvent @event)
        {
            var locationIds = @event.LocationIds;
            if (locationIds != null && locationIds.Any())
            {
                var promotionDetail = new CatalogPromotionDetail()
                {
                    Id = @event.Id,
                    DiscountTypeId = @event.DiscountTypeId,
                    Value = @event.Value,
                    IsUseCouponCodes = @event.IsUseCouponCodes,
                    IsUseConditions = @event.IsUseConditions,
                    CreatedBy = @event.CreatedBy,
                    UpdatedBy = @event.UpdatedBy,
                    CreatedDate = @event.CreatedDate,
                    UpdatedDate = @event.UpdatedDate
                };

                foreach (var locationId in locationIds)
                {
                    await AddPromotionDetailToChannel(locationId, promotionDetail);
                }
            }
        }

        private async Task AddPromotionDetailToChannel(Guid storeId, CatalogPromotionDetail data)
        {
            var channelId = (await _transientPimDbContext.ChannelStoreAssignments.FirstOrDefaultAsync(x => x.StoreId == storeId)).ChannelId;
            if (channelId == Guid.Empty)
            {
                return;
            }
            var channelServerInfomation = (await _transientPimDbContext.Channels.FirstOrDefaultAsync(x => x.Id == channelId)).ServerInformation;

            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channelServerInfomation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var entity = await dbContext.PromotionDetails.FirstOrDefaultAsync(x => x.Id == data.Id);
                if (entity == null)
                {
                    await dbContext.PromotionDetails.AddAsync(data);
                }
                else
                {
                    dbContext.Entry(entity).State = EntityState.Modified;
                    entity.DiscountTypeId = data.DiscountTypeId;
                    entity.Value = data.Value;
                    entity.IsUseCouponCodes = data.IsUseCouponCodes;
                    entity.IsUseConditions = data.IsUseConditions;
                    entity.CreatedBy = data.CreatedBy;
                    entity.UpdatedBy = data.UpdatedBy;
                    entity.CreatedDate = data.CreatedDate;
                    entity.UpdatedDate = data.UpdatedDate;
                }
                await dbContext.SaveChangesAsync();
            };
        }
    }
}
