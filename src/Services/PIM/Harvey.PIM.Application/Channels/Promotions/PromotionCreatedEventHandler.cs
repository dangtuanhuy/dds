﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Promotions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.Promotions
{
    public class PromotionCreatedEventHandler : EventHandlerBase<PromotionCreatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, CatalogPromotion> _promotionRepository;
        private readonly TransientPimDbContext _transientPimDbContext;

        public PromotionCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
             ILogger<EventHandlerBase<PromotionCreatedEvent>> logger,
             IEfRepository<TransientPimDbContext, CatalogPromotion> promotionRepository,
             TransientPimDbContext transientPimDbContext
            ) : base(repository, logger)
        {
            _promotionRepository = promotionRepository;
            _transientPimDbContext = transientPimDbContext;
        }

        protected override async Task ExecuteAsync(PromotionCreatedEvent @event)
        {
            var locationIds = @event.LocationIds;
            if(locationIds != null && locationIds.Any())
            {
                var promotion = new CatalogPromotion()
                {
                    Id = @event.Id,
                    Name = @event.Name,
                    Description = @event.Description,
                    PromotionTypeId = @event.PromotionTypeId,
                    PromotionDetailId = @event.PromotionDetailId,
                    FromDate = @event.FromDate,
                    ToDate = @event.ToDate,
                    Status = (Infrastructure.Domain.Catalog.PromotionStatus)@event.Status,
                    CreatedBy = @event.CreatedBy,
                    UpdatedBy = @event.UpdatedBy,
                    CreatedDate = @event.CreatedDate,
                    UpdatedDate = @event.UpdatedDate
                };

                foreach (var locationId in locationIds)
                {
                    await AddPromotionToChannel(locationId, promotion);
                }
            }
        }

        private async Task AddPromotionToChannel(Guid storeId, CatalogPromotion data)
        {
            var channelId = (await _transientPimDbContext.ChannelStoreAssignments.FirstOrDefaultAsync(x => x.StoreId == storeId)).ChannelId;
            if (channelId == Guid.Empty)
            {
                return;
            }
            var channelServerInfomation = (await _transientPimDbContext.Channels.FirstOrDefaultAsync(x => x.Id == channelId)).ServerInformation;
            
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channelServerInfomation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var entity = await dbContext.Promotions.FirstOrDefaultAsync(x => x.Id == data.Id);
                if (entity == null)
                {
                    await dbContext.Promotions.AddAsync(data);
                }
                else
                {
                    dbContext.Entry(entity).State = EntityState.Modified;
                    entity.Name = data.Name;
                    entity.Description = data.Description;
                    entity.PromotionTypeId = data.PromotionTypeId;
                    entity.PromotionDetailId = data.PromotionDetailId;
                    entity.FromDate = data.FromDate;
                    entity.ToDate = data.ToDate;
                    entity.Status = data.Status;
                    entity.CreatedBy = data.CreatedBy;
                    entity.UpdatedBy = data.UpdatedBy;
                    entity.CreatedDate = data.CreatedDate;
                    entity.UpdatedDate = data.UpdatedDate;
                }
                await dbContext.SaveChangesAsync();
            };
        }
    }
}
