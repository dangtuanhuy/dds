﻿using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.MarketingAutomation;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Harvey.PIM.Application.Channels.Prices
{
    public class ChannelPriceSerializer : IFeedSerializer<CatalogPrice>
    {
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;

        public ChannelPriceSerializer(IEfRepository<TransientPimDbContext, Channel> efRepository)
        {
            _efRepository = efRepository;
        }
        public async Task SerializeAsync(IEnumerable<CatalogPrice> feedItems)
        {
            var channel = await _efRepository.GetByIdAsync(feedItems.First().CorrelationId);
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channel.ServerInformation);

            IEnumerable<CatalogPrice> newPrices = null;
            IEnumerable<CatalogPrice> updatePrices = null;
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var allFeedPriceIds = feedItems.Select(a => a.Id).ToList();
                var existedPriceIds = dbContext.Prices.Where(a => allFeedPriceIds.Contains(a.Id)).Select(x => x.Id).ToList();

                newPrices = feedItems.Where(a => !existedPriceIds.Contains(a.Id)).Select(item =>
                {
                    return new CatalogPrice()
                    {
                        Id = item.Id,
                        ListPrice = item.ListPrice,
                        MemberPrice = item.MemberPrice,
                        StaffPrice = item.StaffPrice
                    };
                });

                updatePrices = feedItems.Where(x => existedPriceIds.Contains(x.Id));
            };
            
            if (newPrices != null && newPrices.Any())
            {
                AddPrices(optionsBuilder.Options, newPrices);
            }

            if (updatePrices != null && updatePrices.Any())
            {
                UpdatePrices(optionsBuilder.Options, updatePrices);
            }

            await UpdateLastSync(optionsBuilder.Options, feedItems.Last().UpdatedDate);
        }

        private void AddPrices(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogPrice> newPrices)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.AddRange(newPrices);
                dbContext.SaveChanges();
            }
        }

        private void UpdatePrices(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogPrice> updatePrices)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.UpdateRange(updatePrices);
                dbContext.SaveChanges();
            }
        }

        private async Task UpdateLastSync(DbContextOptions<CatalogDbContext> catalogOption, DateTime lastSyncDate)
        {
            using (var _dbContext = new CatalogDbContext(catalogOption))
            {
                var lastSync = _dbContext.TimeToFeeds.FirstOrDefault();
                if (lastSync != null)
                {
                    lastSync.LastSyncPrice = lastSyncDate;
                    _dbContext.Update(lastSync);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    var timeToFeed = new Infrastructure.Domain.Catalog.TimeToFeed();
                    timeToFeed.LastSyncPrice = lastSyncDate;
                    _dbContext.Add(timeToFeed);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }
    }
}
