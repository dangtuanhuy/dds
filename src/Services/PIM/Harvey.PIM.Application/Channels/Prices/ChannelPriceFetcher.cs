﻿using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.MarketingAutomation;
using System;
using Harvey.MarketingAutomation.Enums;
using Microsoft.Extensions.Configuration;

namespace Harvey.PIM.Application.Channels.Prices
{
    public class ChannelPriceFetcher : WebApiFetcherBase<Price>
    {
        private readonly TransientPimDbContext _pimDbContext;
        private readonly DateTime _dateTime;
        public ChannelPriceFetcher(IConfiguration configuration)
            : base($"{configuration["ChannelFeedApiUrl"]}?feedType={FeedType.Price}",
                 $"{configuration["ChannelFeedApiUrl"]}/count?feedType={FeedType.Price}")
        {
        }
    }
}
