﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.Extensions.Configuration;


namespace Harvey.PIM.Application.Channels.Variants
{
    public class ChannelVariantFetcher : WebApiFetcherBase<Variant>
    {
        public ChannelVariantFetcher(IConfiguration configuration)
           : base($"{configuration["ChannelFeedApiUrl"]}?feedType={FeedType.Variant}",
                 $"{configuration["ChannelFeedApiUrl"]}/count?feedType={FeedType.Variant}")
        {
        }
    }
}
