﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Variants;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Channels.Services;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.MarketingAutomation.Connectors;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using System;
using Harvey.PIM.Application.Events;

namespace Harvey.PIM.Application.Channels.Variants
{
    
    public class ChannelVariantUpdatedEventHandler : EventHandlerBase<MarketingAutomationEvent<VariantUpdatedEvent>>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;
        private readonly IAssignmentService _assignmentService;
        private readonly IEventBus _eventBus;

        public ChannelVariantUpdatedEventHandler(
                    IAssignmentService assignmentService,
                    IRepository<IdentifiedEvent> repository,
                    ILogger<EventHandlerBase<MarketingAutomationEvent<VariantUpdatedEvent>>> logger,
                    ConnectorInfoCollection connectorInfos,
                    IEfRepository<TransientPimDbContext, Channel> efRepository,
                    IEventBus eventBus) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _efRepository = efRepository;
            _assignmentService = assignmentService;
            _eventBus = eventBus;
        }

        protected override async Task ExecuteAsync(MarketingAutomationEvent<VariantUpdatedEvent> @event)
        {

            var isAssignemt = _assignmentService.IsAssignment(AssortmentAssignmentType.Product, @event.CorrelationId.Value, Guid.Parse(@event.InnerEvent.AggregateId));
            if (isAssignemt)
            {
                var channel = await _efRepository.GetByIdAsync(@event.CorrelationId.Value);
                var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                optionsBuilder.UseNpgsql(channel.ServerInformation);
                using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
                {
                    var entity = dbContext.Variants.FirstOrDefault(x => x.Id == Guid.Parse(@event.InnerEvent.AggregateId));
                    if (entity != null)
                    {
                        entity.Name = @event.InnerEvent.Name;
                        entity.Description = @event.InnerEvent.Description;

                        dbContext.Variants.Update(entity);
                        await dbContext.SaveChangesAsync();

                        await _eventBus.PublishAsync(new SynchronizationCreatedEvent(channel.Id.ToString())
                        {
                            Action = SynchAction.Update,
                            Entity = SynchEntity.Variant,
                            RefId = entity.Id
                        });
                    }
                    if (entity == null)
                    {
                        throw new InvalidOperationException($"category {@event.InnerEvent.Id} is not presented");
                    }

                   
                };
            }
        }
    }
}
