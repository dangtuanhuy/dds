﻿using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.MarketingAutomation;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Harvey.PIM.Application.Channels.Variants
{
    public class ChannelVariantSerializer : IFeedSerializer<CatalogVariant>
    {
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;

        public ChannelVariantSerializer(IEfRepository<TransientPimDbContext,
            Channel> efRepository)
        {
            _efRepository = efRepository;
        }
        public async Task SerializeAsync(IEnumerable<CatalogVariant> feedItems)
        {
            var channel = await _efRepository.GetByIdAsync(feedItems.First().CorrelationId);
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channel.ServerInformation);

            IEnumerable<CatalogVariant> newVariants = null;
            IEnumerable<CatalogVariant> updateVariants = null;
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var allFeedVariantIds = feedItems.Select(a => a.Id).ToList();
                var allExistVariantIds = dbContext.Variants.Where(a => allFeedVariantIds.Contains(a.Id)).Select(b => b.Id).ToList();

                newVariants = feedItems.Where(a => !allExistVariantIds.Contains(a.Id)).Select(item =>
                {
                    return new CatalogVariant()
                    {
                        Id = item.Id,
                        ProductId = item.ProductId,
                        PriceId = item.PriceId,
                        SKUCode = item.SKUCode,
                        UpdatedDate = DateTime.UtcNow,
                        Name = item.Name,
                        Description = item.Description
                    };
                });

                updateVariants = feedItems.Where(a => allExistVariantIds.Contains(a.Id));
            };

            if (newVariants != null && newVariants.Any())
            {
                AddVariants(optionsBuilder.Options, newVariants);
            }

            if (updateVariants != null && updateVariants.Any())
            {
                UpdateVariants(optionsBuilder.Options, updateVariants);
            }

            await UpdateLastSync(optionsBuilder.Options);
        }

        private void AddVariants(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogVariant> newVariants)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.AddRange(newVariants);
                dbContext.SaveChanges();
            }
        }

        private void UpdateVariants(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogVariant> updateVariants)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.UpdateRange(updateVariants);
                dbContext.SaveChanges();
            }
        }

        private async Task UpdateLastSync(DbContextOptions<CatalogDbContext> catalogOption)
        {
            using (var _dbContext = new CatalogDbContext(catalogOption))
            {
                var lastSync = _dbContext.TimeToFeeds.FirstOrDefault();
                if (lastSync != null)
                {
                    lastSync.LastSyncVariant = DateTime.UtcNow;
                    _dbContext.Update(lastSync);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    var timeToFeed = new Infrastructure.Domain.Catalog.TimeToFeed();
                    timeToFeed.LastSyncVariant = DateTime.UtcNow;
                    _dbContext.Add(timeToFeed);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }
    }
}
