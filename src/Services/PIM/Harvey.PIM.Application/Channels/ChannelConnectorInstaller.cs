﻿using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Categories;
using Harvey.EventBus.Events.FieldValues;
using Harvey.EventBus.Events.Prices;
using Harvey.EventBus.Events.Products;
using Harvey.EventBus.Events.StockLevels;
using Harvey.EventBus.Events.Variants;
using Harvey.MarketingAutomation;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Channels.BarCodes;
using Harvey.PIM.Application.Channels.Categories;
using Harvey.PIM.Application.Channels.FieldValues;
using Harvey.PIM.Application.Channels.PaymentMethods;
using Harvey.PIM.Application.Channels.Prices;
using Harvey.PIM.Application.Channels.Products;
using Harvey.PIM.Application.Channels.StockTypes;
using Harvey.PIM.Application.Channels.TransactionTypes;
using Harvey.PIM.Application.Channels.Variants;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Harvey.PIM.Application.Channels
{
    public class ChannelConnectorInstaller
    {
        private readonly IEfRepository<PimDbContext, Channel> _efRepository;
        private readonly IEventBus _eventBus;
        public ChannelConnectorInstaller(
            IEfRepository<PimDbContext, Channel> efRepository,
            IEventBus eventBus)
        {
            _eventBus = eventBus;
            _efRepository = efRepository;
        }
        public void Install(ApplicationBuilder appBuilder)
        {
            var channels = _efRepository.GetAsync().Result;
            foreach (var item in channels)
            {
                if (item.IsProvision)
                {
                    appBuilder.AddConnector(item.Id, item.Name, _eventBus, (connectorRegistration) =>
                    {
                        connectorRegistration
                        .AddProductSyncService(productSyncServiceRegistration =>
                        {
                            productSyncServiceRegistration
                            .UseSyncHandler<MarketingAutomationEvent<ProductCreatedEvent>, ChannelProductCreatedEventHandler>()
                            .UseSyncHandler<MarketingAutomationEvent<ProductUpdatedEvent>, ChannelProductUpdatedEventHandler>()
                            .UseSyncHandler<MarketingAutomationEvent<ProductDeletedEvent>, ChannelProductDeletedEventHandler>();
                        })
                        .AddProductFeedService<ProductFeed, CatalogProductFeed>(productFeedServiceRegistration =>
                        {
                            var fetcherConfig = new WebApiFetcherConfiguration
                            {
                                LastSyncResolver = () =>
                                {
                                    var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                                    optionsBuilder.UseNpgsql(item.ServerInformation);
                                    DateTime? lastSync = null;
                                    using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
                                    {
                                        lastSync = dbContext.TimeToFeeds.FirstOrDefault()?.LastSyncProduct;
                                    };
                                    return lastSync;
                                }
                            };

                            productFeedServiceRegistration
                            .UseFetcher<ChannelProductFetcher, WebApiFetcherConfiguration>(fetcherConfig)
                            .UseFilter<ChannelProductFilter>()
                            .UseConverter<ChannelProductConveter>()
                            .UseSerializer<ChannelProductSerializer>()
                            .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 5, 0));
                        })
                        .AddVariantSyncService(variantSyncServiceRegistration =>
                        {
                            variantSyncServiceRegistration
                            .UseSyncHandler<MarketingAutomationEvent<VariantCreatedEvent>, ChannelVariantCreatedEventHandler>();
                        })
                        .AddVariantFeedService<Variant, CatalogVariant>(productFeedServiceRegistration =>
                        {
                            var fetcherConfig = new WebApiFetcherConfiguration();
                            fetcherConfig.LastSyncResolver = () =>
                            {
                                var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                                optionsBuilder.UseNpgsql(item.ServerInformation);
                                DateTime? lastSync = null;
                                using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
                                {
                                    lastSync = dbContext.TimeToFeeds.FirstOrDefault()?.LastSyncVariant;
                                };
                                return lastSync;
                            };

                            productFeedServiceRegistration
                            .UseFetcher<ChannelVariantFetcher, WebApiFetcherConfiguration>(fetcherConfig)
                            .UseFilter<ChannelVariantFilter>()
                            .UseConverter<ChannelVariantConveter>()
                            .UseSerializer<ChannelVariantSerializer>()
                            .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 5, 0));
                        })
                        .AddCategorySyncService(categorySyncServiceRegistration =>
                        {
                            categorySyncServiceRegistration
                            .UseSyncHandler<MarketingAutomationEvent<CategoryCreatedEvent>, ChannelCategoryCreatedEventHandler>()
                            .UseSyncHandler<MarketingAutomationEvent<CategoryUpdatedEvent>, ChannelCategoryUpdatedEventHandler>();
                        })
                        .AddCategoryFeedService<Category, CatalogCategory>(productFeedServiceRegistration =>
                        {
                            productFeedServiceRegistration
                            .UseFetcher<ChannelCategoryFetcher>()
                            .UseFilter<ChannelCategoryFilter>()
                            .UseConverter<ChannelCategoryConveter>()
                            .UseSerializer<ChannelCategorySerializer>()
                            .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 5, 0));
                        })
                        .AddFieldValueSyncService(fieldValueSyncServiceRegistration =>
                        {
                            fieldValueSyncServiceRegistration
                            .UseSyncHandler<MarketingAutomationEvent<FieldValueCreatedEvent>, ChannelFieldValueCreatedEventHandler>()
                            .UseSyncHandler<MarketingAutomationEvent<FieldValueUpdatedEvent>, ChannelFieldValueUpdatedEventHandler>();
                        })
                        .AddPriceSyncService(priceSyncServiceRegistration =>
                        {
                            priceSyncServiceRegistration
                            .UseSyncHandler<MarketingAutomationEvent<PriceCreatedEvent>, ChannelPriceCreatedEventHandler>();
                        })
                        .AddPriceFeedService<Price, CatalogPrice>(priceFeedServiceRegistration =>
                         {
                             var fetcherConfig = new WebApiFetcherConfiguration();
                             fetcherConfig.LastSyncResolver = () =>
                             {
                                 var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                                 optionsBuilder.UseNpgsql(item.ServerInformation);
                                 DateTime? lastSync = null;
                                 using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
                                 {
                                     lastSync = dbContext.TimeToFeeds.FirstOrDefault()?.LastSyncPrice;
                                 };
                                 return lastSync;
                             };

                             priceFeedServiceRegistration
                             .UseFetcher<ChannelPriceFetcher, WebApiFetcherConfiguration>(fetcherConfig)
                             .UseFilter<ChannelPriceFilter>()
                             .UseConverter<ChannelPriceConveter>()
                             .UseSerializer<ChannelPriceSerializer>()
                             .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 5, 0));
                         })
                        .AddStockTypeFeedService<StockType, CatalogStockType>(stockTypeFeedServiceRegistration =>
                        {
                            stockTypeFeedServiceRegistration
                            .UseFetcher<ChannelStockTypeFetcher>()
                            .UseFilter<ChannelStockTypeFilter>()
                            .UseConverter<ChannelStockTypeConverter>()
                            .UseSerializer<ChannelStockTypeSerializer>()
                            .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 10, 0));
                        })
                        .AddTransactionTypeFeedService<TransactionType, CatalogTransactionType>(transactionTypeFeedServiceRegistration =>
                         {
                             transactionTypeFeedServiceRegistration
                             .UseFetcher<ChannelTransactionTypeFetcher>()
                             .UseFilter<ChannelTransactionTypeFilter>()
                             .UseConverter<ChannelTransactionTypeConverter>()
                             .UseSerializer<ChannelTransactionTypeSerializer>()
                             .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 10, 0));
                         })
                        .AddChannelStockSyncService(channelStockSyncServiceRegistration =>
                        {
                            channelStockSyncServiceRegistration
                           .UseSyncHandler<MarketingAutomationEvent<StockHasTransferedInStoreEvent>, ChannelStockHasTransferedInStoreEventHandler>();
                        })
                        .AddBarCodeFeedService<BarCode, CatalogBarCode>(priceFeedServiceRegistration =>
                        {
                            var fetcherConfig = new WebApiFetcherConfiguration();
                            fetcherConfig.LastSyncResolver = () =>
                            {
                                var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                                optionsBuilder.UseNpgsql(item.ServerInformation);
                                DateTime? lastSync = null;
                                using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
                                {
                                    lastSync = dbContext.TimeToFeeds.FirstOrDefault()?.LastSyncBarCode;
                                };
                                return lastSync;
                            };

                            priceFeedServiceRegistration
                            .UseFetcher<ChannelBarCodeFetcher, WebApiFetcherConfiguration>(fetcherConfig)
                            .UseFilter<ChannelBarCodeFilter>()
                            .UseConverter<ChannelBarCodeConveter>()
                            .UseSerializer<ChannelBarCodeSerializer>()
                            .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 5, 0));
                        })
                        .AddPaymentMethodFeedService<PaymentMethod, CatalogPaymentMethod>(paymentMethodFeedServiceRegistration =>
                        {
                            

                            paymentMethodFeedServiceRegistration
                            .UseFetcher<ChannelPaymentMethodFetcher>()
                            .UseFilter<ChannelPaymentMethodFilter>()
                            .UseConverter<ChannelPaymentMethodConveter>()
                            .UseSerializer<ChannelPaymentMethodSerializer>()
                            .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 10, 0));
                        });
                    });
                }
            }
        }
    }
}
