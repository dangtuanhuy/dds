﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.PIM.Application.Channels.TransactionTypes
{
    public class ChannelTransactionTypeConverter : IFeedConverter<TransactionType, CatalogTransactionType>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<TransactionType>).GetType();

        public IEnumerable<CatalogTransactionType> Convert(IEnumerable<TransactionType> source)
        {
            return source.Select(x => new CatalogTransactionType()
            {
                Id = x.Id,
                Code = x.Code,
                Description = x.Description,
                Name = x.Name
            });
        }
    }
}