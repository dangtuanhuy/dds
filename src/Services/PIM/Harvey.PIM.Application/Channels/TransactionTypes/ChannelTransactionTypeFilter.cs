﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Channels.TransactionTypes
{
    public class ChannelTransactionTypeFilter : IFeedFilter<TransactionType>
    {
        public ChannelTransactionTypeFilter()
        {
        }
        public IEnumerable<TransactionType> Filter(Guid correlationId, IEnumerable<TransactionType> source)
        {
            return source;
        }
    }
}
