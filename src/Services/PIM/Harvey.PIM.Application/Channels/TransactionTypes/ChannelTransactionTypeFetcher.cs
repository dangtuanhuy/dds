﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.TransactionTypes
{
    public class ChannelTransactionTypeFetcher : IFeedFetcher<TransactionType>
    {
        private readonly TransientTransactionDbContext _dbContext;
        public ChannelTransactionTypeFetcher(TransientTransactionDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<TransactionType>> FetchAsync()
        {
            return await _dbContext.TransactionTypes.AsNoTracking().ToListAsync();
        }
    }
}