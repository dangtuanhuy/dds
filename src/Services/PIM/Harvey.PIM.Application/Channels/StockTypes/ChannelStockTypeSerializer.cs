﻿using Harvey.MarketingAutomation;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.StockTypes
{
    public class ChannelStockTypeSerializer : IFeedSerializer<CatalogStockType>
    {
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;

        public ChannelStockTypeSerializer(IEfRepository<TransientPimDbContext, Channel> efRepository)
        {
            _efRepository = efRepository;
        }
        public async Task SerializeAsync(IEnumerable<CatalogStockType> feedItems)
        {
            var channel = await _efRepository.GetByIdAsync(feedItems.First().CorrelationId);
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channel.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                foreach (var item in feedItems)
                {
                    var entity = dbContext.StockTypes.FirstOrDefault(x => x.Id == item.Id);
                    if (entity == null)
                    {
                        entity = new CatalogStockType()
                        {
                            Id = item.Id,
                            Code = item.Code,
                            Description = item.Description,
                            Name = item.Name
                        };
                        dbContext.StockTypes.Add(item);
                    }
                    else
                    {
                        dbContext.Entry(entity).State = EntityState.Modified;
                        entity.Code = item.Code;
                        entity.Description = item.Description;
                        entity.Name = item.Name;
                    }

                }
                await dbContext.SaveChangesAsync();
            };
        }
    }
}
