﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.StockLevels;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Channels.Services;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Harvey.PIM.Application.Channels.StockTypes
{
    public class ChannelStockHasTransferedInStoreEventHandler : EventHandlerBase<MarketingAutomationEvent<StockHasTransferedInStoreEvent>>
    {
        private readonly IAssignmentService _assignmentService;
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;
        public ChannelStockHasTransferedInStoreEventHandler(
            IAssignmentService assignmentService,
            IEfRepository<TransientPimDbContext, Channel> efRepository,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<MarketingAutomationEvent<StockHasTransferedInStoreEvent>>> logger) : base(repository, logger)
        {
            _assignmentService = assignmentService;
            _efRepository = efRepository;
        }

        protected async override Task ExecuteAsync(MarketingAutomationEvent<StockHasTransferedInStoreEvent> @event)
        {
            var channelStoreAssignment = await _assignmentService.GetChannelStoreAssignment(@event.CorrelationId.Value, @event.InnerEvent.StoreId);
            
            if (channelStoreAssignment != null)
            {
                var channel = await _efRepository.GetByIdAsync(channelStoreAssignment.ChannelId);
                if (channel != null)
                {
                    var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                    optionsBuilder.UseNpgsql(channel.ServerInformation);
                    using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
                    {
                        var lastTransaction = dbContext.StockTransactions
                            .Where(x => x.LocationId == @event.InnerEvent.StoreId
                                        && x.VariantId == @event.InnerEvent.VariantId)    
                            .OrderByDescending(x => x.CreatedDate)
                            .FirstOrDefault();

                        var balance = 0;
                        if (lastTransaction != null)
                        {
                            balance = lastTransaction.Balance;
                        }
                       
                        var catalogStockTransaction = new CatalogStockTransaction()
                        {
                            VariantId = @event.InnerEvent.VariantId,
                            LocationId = @event.InnerEvent.StoreId,
                            StockTypeId = Guid.Empty,
                            TransactionTypeId = @event.InnerEvent.TransactionTypeId,
                            Quantity = @event.InnerEvent.Quantity,
                            Balance = balance + @event.InnerEvent.Quantity,
                            CreatedDate = DateTime.UtcNow//TODO should be from event
                        };


                        await dbContext.StockTransactions.AddAsync(catalogStockTransaction);

                        await dbContext.AddSynchronization(SynchAction.Add, SynchEntity.Variant, @event.InnerEvent.VariantId);
                        await dbContext.SaveChangesAsync();
                    }
                }
            }
        }
    }
}
