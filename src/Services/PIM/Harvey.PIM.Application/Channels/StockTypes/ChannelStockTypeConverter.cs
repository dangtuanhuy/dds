﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.PIM.Application.Channels.StockTypes
{
    public class ChannelStockTypeConverter : IFeedConverter<StockType, CatalogStockType>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<StockType>).GetType();

        public IEnumerable<CatalogStockType> Convert(IEnumerable<StockType> source)
        {
            return source.Select(x => new CatalogStockType()
            {
                Id = x.Id,
                Code = x.Code,
                Description = x.Description,
                Name = x.Name
            });
        }
    }
}
