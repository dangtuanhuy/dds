﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Channels.StockTypes
{
    public class ChannelStockTypeFilter : IFeedFilter<StockType>
    {
        public ChannelStockTypeFilter()
        {
        }
        public IEnumerable<StockType> Filter(Guid correlationId, IEnumerable<StockType> source)
        {
            return source;
        }
    }
}
