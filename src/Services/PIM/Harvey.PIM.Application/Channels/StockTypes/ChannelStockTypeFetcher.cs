﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.StockTypes
{
    public class ChannelStockTypeFetcher : IFeedFetcher<StockType>
    {
        private readonly TransientTransactionDbContext _dbContext;
        public ChannelStockTypeFetcher(TransientTransactionDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<StockType>> FetchAsync()
        {
            return await _dbContext.StockTypes.AsNoTracking().ToListAsync();
        }
    }
}
