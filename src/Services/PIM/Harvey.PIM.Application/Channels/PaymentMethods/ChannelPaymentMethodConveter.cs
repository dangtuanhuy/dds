﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.PIM.Application.Channels.PaymentMethods
{
    public class ChannelPaymentMethodConveter : IFeedConverter<PaymentMethod, CatalogPaymentMethod>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PaymentMethod>).GetType();

        public IEnumerable<CatalogPaymentMethod> Convert(IEnumerable<PaymentMethod> source)
        {
            return source.Select(x => new CatalogPaymentMethod()
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code
            });
        }
    }
}
