﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.PaymentMethods
{
    public class ChannelPaymentMethodFetcher : IFeedFetcher<PaymentMethod>
    {
        private readonly TransientPimDbContext _pimDbContext;
        public ChannelPaymentMethodFetcher(TransientPimDbContext pimDbContext)
        {
            _pimDbContext = pimDbContext;
        }

        public async Task<IEnumerable<PaymentMethod>> FetchAsync()
        {
            return await _pimDbContext.PaymentMethods.AsNoTracking().ToListAsync();
        }
    }
}
