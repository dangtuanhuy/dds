﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Channels.Services;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Channels.PaymentMethods
{
    public class ChannelPaymentMethodFilter : IFeedFilter<PaymentMethod>
    {
        private readonly IAssignmentService _assignmentService;
        public ChannelPaymentMethodFilter(IAssignmentService assignmentService)
        {
            _assignmentService = assignmentService;
        }
        public IEnumerable<PaymentMethod> Filter(Guid correlationId, IEnumerable<PaymentMethod> source)
        {
            return source;
        }
    }
}
