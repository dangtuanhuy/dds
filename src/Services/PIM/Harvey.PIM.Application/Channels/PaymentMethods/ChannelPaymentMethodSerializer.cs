﻿using Harvey.MarketingAutomation;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.PaymentMethods
{
    public class ChannelPaymentMethodSerializer : IFeedSerializer<CatalogPaymentMethod>
    {
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;
        public ChannelPaymentMethodSerializer(IEfRepository<TransientPimDbContext, Channel> efRepository)
        {
            _efRepository = efRepository;
        }

        public async Task SerializeAsync(IEnumerable<CatalogPaymentMethod> feedItems)
        {
            var channel = await _efRepository.GetByIdAsync(feedItems.First().CorrelationId);
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channel.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                foreach (var item in feedItems)
                {
                    var entity = dbContext.PaymentMethods.FirstOrDefault(x => x.Id == item.Id);
                    if (entity == null)
                    {
                        entity = new CatalogPaymentMethod()
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Code = item.Code
                        };
                        dbContext.PaymentMethods.Add(item);
                    }
                    else
                    {
                        dbContext.Entry(entity).State = EntityState.Modified;
                        entity.Name = item.Name;
                        entity.Code = item.Code;
                    }

                }
                await dbContext.SaveChangesAsync();
            };
        }
    }
}
