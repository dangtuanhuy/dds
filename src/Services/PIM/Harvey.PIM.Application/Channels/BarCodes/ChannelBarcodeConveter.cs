﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.PIM.Application.Channels.BarCodes
{
    public class ChannelBarCodeConveter : IFeedConverter<BarCode, CatalogBarCode>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<BarCode>).GetType();

        public IEnumerable<CatalogBarCode> Convert(IEnumerable<BarCode> source)
        {
            return source.Select(x => new CatalogBarCode()
            {
                Id = x.Id,
                Code = x.Code,
                VariantId = x.VariantId,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate
            });
        }
    }
}
