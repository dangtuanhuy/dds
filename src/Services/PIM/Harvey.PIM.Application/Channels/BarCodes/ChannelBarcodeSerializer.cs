﻿using Harvey.MarketingAutomation;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Channels.BarCodes
{
    public class ChannelBarCodeSerializer : IFeedSerializer<CatalogBarCode>
    {
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;
        public ChannelBarCodeSerializer(IEfRepository<TransientPimDbContext, Channel> efRepository)
        {
            _efRepository = efRepository;
        }

        public async Task SerializeAsync(IEnumerable<CatalogBarCode> feedItems)
        {
            var channel = await _efRepository.GetByIdAsync(feedItems.First().CorrelationId);
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channel.ServerInformation);


            IEnumerable<CatalogBarCode> newBarCodes = null;
            IEnumerable<CatalogBarCode> updateBarCodes = null;
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var allFeedBarcodeIds = feedItems.Select(a => a.Id).ToList();
                var allExistBarCodeIds = dbContext.BarCodes.Where(a => allFeedBarcodeIds.Contains(a.Id))
                                                             .Select(b => b.Id).ToList();

                newBarCodes = feedItems.Where(a => !allExistBarCodeIds.Contains(a.Id)).Select(item =>
                {
                    return new CatalogBarCode()
                    {
                        Id = item.Id,
                        Code = item.Code,
                        VariantId = item.VariantId,
                        CreatedDate = item.CreatedDate
                    };
                });

                updateBarCodes = feedItems.Where(a => allExistBarCodeIds.Contains(a.Id));
            };

            if (newBarCodes != null && newBarCodes.Any())
            {
                AddBarCodes(optionsBuilder.Options, newBarCodes);
            }

            if (updateBarCodes != null && updateBarCodes.Any())
            {
                UpdateBarCodes(optionsBuilder.Options, updateBarCodes);
            }

            await UpdateLastSync(optionsBuilder.Options);
        }

        private void AddBarCodes(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogBarCode> newBarCodes)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.AddRange(newBarCodes);
                dbContext.SaveChanges();
            }
        }

        private void UpdateBarCodes(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogBarCode> updateBarCodes)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.UpdateRange(updateBarCodes);
                dbContext.SaveChanges();
            }
        }

        private async Task UpdateLastSync(DbContextOptions<CatalogDbContext> catalogOption)
        {
            using (var _dbContext = new CatalogDbContext(catalogOption))
            {
                var lastSync = _dbContext.TimeToFeeds.FirstOrDefault();
                if (lastSync != null)
                {
                    lastSync.LastSyncBarCode = DateTime.UtcNow;
                    _dbContext.Update(lastSync);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    var timeToFeed = new Infrastructure.Domain.Catalog.TimeToFeed();
                    timeToFeed.LastSyncBarCode = DateTime.UtcNow;
                    _dbContext.Add(timeToFeed);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }
    }
}
