﻿using Harvey.MarketingAutomation;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Channels.BarCodes
{
    public class ChannelBarCodeFilter : IFeedFilter<BarCode>
    {
        public ChannelBarCodeFilter()
        {

        }
        public IEnumerable<BarCode> Filter(Guid correlationId, IEnumerable<BarCode> source)
        {
            return source;
        }
    }
}
