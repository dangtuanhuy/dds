﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.Extensions.Configuration;

namespace Harvey.PIM.Application.Channels.BarCodes
{
    public class ChannelBarCodeFetcher : WebApiFetcherBase<BarCode>
    {
        public ChannelBarCodeFetcher(IConfiguration configuration)
             : base($"{configuration["ChannelFeedApiUrl"]}?feedType={FeedType.BarCode}",
                  $"{configuration["ChannelFeedApiUrl"]}/count?feedType={FeedType.BarCode}")
        {
        }

     
    }
}
