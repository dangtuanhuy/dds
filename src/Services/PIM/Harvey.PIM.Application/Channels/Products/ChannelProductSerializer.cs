﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.MarketingAutomation;
using Microsoft.EntityFrameworkCore;
using System;

namespace Harvey.PIM.Application.Channels.Products
{
    public class ChannelProductSerializer : IFeedSerializer<CatalogProductFeed>
    {
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;

        public ChannelProductSerializer(IEfRepository<TransientPimDbContext,
            Channel> efRepository)
        {
            _efRepository = efRepository;
        }

        public async Task SerializeAsync(IEnumerable<CatalogProductFeed> feedItems)
        {
            var channel = await _efRepository.GetByIdAsync(feedItems.First().CorrelationId);
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channel.ServerInformation);

            IEnumerable<CatalogProduct> newProducts = null;
            IEnumerable<CatalogProduct> updateProducts = null;

            IEnumerable<CatalogFieldValue> newFieldValues = null;
            IEnumerable<CatalogFieldValue> updateFieldValues = null;

            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var allFeedProductIds = feedItems.Select(x => x.Id);
                var allFeedFieldValues = feedItems.SelectMany(x => x.FieldValues);
                var allFeedFieldValueIds = allFeedFieldValues.Select(x => x.Id);
                var allProductIds = dbContext.Products.Where(x => allFeedProductIds.Contains(x.Id)).Select(x => x.Id).ToList();
                var allFieldValueIds = dbContext.FieldValues.Where(x => allFeedFieldValueIds.Contains(x.Id)).Select(x => x.Id).ToList();

                newProducts = feedItems.Where(x => !allProductIds.Contains(x.Id)).Select(item =>
                  {
                      return new CatalogProduct()
                      {
                          Id = item.Id,
                          Name = item.Name,
                          Description = item.Description,
                          CategoryId = item.CategoryId,
                          IsDelete = item.IsDelete,
                          UpdatedDate = item.UpdatedDate,
                          CreatedDate = item.CreatedDate
                      };
                  });

                newFieldValues = allFeedFieldValues.Where(x => !allFieldValueIds.Contains(x.Id)).Select(fv =>
                {
                    return new CatalogFieldValue
                    {
                        EntityId = fv.EntityId,
                        FieldId = fv.FieldId,
                        FieldName = fv.FieldName,
                        FieldType = fv.FieldType,
                        FieldValue = fv.FieldValue,
                        FieldValueId = fv.FieldValueId,
                        Id = fv.Id,
                        IsVariantField = fv.IsVariantField,
                        OrderSection = fv.OrderSection,
                        Section = fv.Section
                    };
                });

                updateProducts = feedItems.Where(x => allProductIds.Contains(x.Id));
                updateFieldValues = allFeedFieldValues.Where(x => allFieldValueIds.Contains(x.Id));
            };

            if (newProducts != null && newProducts.Any())
            {
                AddProducts(optionsBuilder.Options, newProducts);
            }

            if (updateProducts != null && updateProducts.Any())
            {
                UpdateProducts(optionsBuilder.Options, updateProducts);
            }

            if (newFieldValues != null && newFieldValues.Any())
            {
                AddFieldValues(optionsBuilder.Options, newFieldValues);
            }

            if (updateFieldValues != null && updateFieldValues.Any())
            {
                UpdateFieldValues(optionsBuilder.Options, updateFieldValues);
            }

            await UpdateLastSync(optionsBuilder.Options, feedItems.Last().UpdatedDate);
        }

        private void AddProducts(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogProduct> newProducts)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.AddRange(newProducts);
                dbContext.SaveChanges();
            }
        }

        private void UpdateProducts(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogProduct> updateProducts)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.UpdateRange(updateProducts);
                dbContext.SaveChanges();
            }
        }

        private void AddFieldValues(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogFieldValue> fieldValues)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.AddRange(fieldValues);
                dbContext.SaveChanges();
            }
        }

        private void UpdateFieldValues(DbContextOptions<CatalogDbContext> catalogOption, IEnumerable<CatalogFieldValue> updateFieldValues)
        {
            using (var dbContext = new CatalogDbContext(catalogOption))
            {
                dbContext.UpdateRange(updateFieldValues);
                dbContext.SaveChanges();
            }
        }

        private async Task UpdateLastSync(DbContextOptions<CatalogDbContext> catalogOption, DateTime lastSyncDate)
        {
            using (var _dbContext = new CatalogDbContext(catalogOption))
            {
                var lastSync = _dbContext.TimeToFeeds.FirstOrDefault();
                if (lastSync != null)
                {
                    lastSync.LastSyncProduct = lastSyncDate;
                    _dbContext.Update(lastSync);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    var timeToFeed = new Infrastructure.Domain.Catalog.TimeToFeed();
                    timeToFeed.LastSyncProduct = lastSyncDate;
                    _dbContext.Add(timeToFeed);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }
    }
}

