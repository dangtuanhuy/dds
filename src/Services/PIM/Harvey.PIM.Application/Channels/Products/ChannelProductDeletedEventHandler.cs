﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Products;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Channels.Services;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.MarketingAutomation.Connectors;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Harvey.PIM.Application.Events;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;

namespace Harvey.PIM.Application.Channels.Products
{
    public class ChannelProductDeletedEventHandler : EventHandlerBase<MarketingAutomationEvent<ProductDeletedEvent>>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;
        private readonly IAssignmentService _assignmentService;
        private readonly IEventBus _eventBus;
        public ChannelProductDeletedEventHandler(IRepository<IdentifiedEvent> repository,
                                                ILogger<EventHandlerBase<MarketingAutomationEvent<ProductDeletedEvent>>> logger,
                                                ConnectorInfoCollection connectorInfos,
                                                IEfRepository<TransientPimDbContext, Channel> efRepository,
                                                IAssignmentService assignmentService,
                                                IEventBus eventBus) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _efRepository = efRepository;
            _assignmentService = assignmentService;
            _eventBus = eventBus;
        }
        protected override async Task ExecuteAsync(MarketingAutomationEvent<ProductDeletedEvent> @event)
        {
            var isAssignemt = _assignmentService.IsAssignment(AssortmentAssignmentType.Product, @event.CorrelationId.Value, Guid.Parse(@event.InnerEvent.AggregateId));
            if (isAssignemt)
            {
                var channel = await _efRepository.GetByIdAsync(@event.CorrelationId.Value);
                var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                optionsBuilder.UseNpgsql(channel.ServerInformation);
                CatalogProduct entity = null;
                using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
                {
                    entity = dbContext.Products.FirstOrDefault(x => x.Id == Guid.Parse(@event.InnerEvent.AggregateId));
                    if (entity != null)
                    {
                        entity.IsDelete = @event.InnerEvent.IsDelete;

                        dbContext.Products.Update(entity);
                        await dbContext.SaveChangesAsync();
                    }
                };

                if (entity == null)
                {
                    throw new InvalidOperationException($"product {@event.InnerEvent.Id} is not presented.");
                }
                else
                {
                    await _eventBus.PublishAsync(new SynchronizationCreatedEvent(channel.Id.ToString())
                    {
                        Action = SynchAction.Delete,
                        Entity = SynchEntity.Product,
                        RefId = entity.Id
                    });
                }
            }
        }
    }
}
