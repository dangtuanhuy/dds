﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Harvey.PIM.Application.Channels.Products
{
    public class ChannelProductFetcher : WebApiFetcherBase<ProductFeed>
    {
        public ChannelProductFetcher(IConfiguration configuration)
            : base($"{configuration["ChannelFeedApiUrl"]}?feedType={FeedType.Product}",
                  $"{configuration["ChannelFeedApiUrl"]}/count?feedType={FeedType.Product}")
        {
        }
    }
}
