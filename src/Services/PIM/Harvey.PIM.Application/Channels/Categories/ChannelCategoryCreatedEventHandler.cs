﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Categories;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Channels.Services;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.MarketingAutomation.Connectors;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Harvey.EventBus.Abstractions;
using Harvey.PIM.Application.Events;

namespace Harvey.PIM.Application.Channels.Categories
{
    public class ChannelCategoryCreatedEventHandler : EventHandlerBase<MarketingAutomationEvent<CategoryCreatedEvent>>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;
        private readonly IAssignmentService _assignmentService;
        private readonly IEventBus _eventBus;

        public ChannelCategoryCreatedEventHandler(IRepository<IdentifiedEvent> repository,
                                                  ILogger<EventHandlerBase<MarketingAutomationEvent<CategoryCreatedEvent>>> logger,
                                                  ConnectorInfoCollection connectorInfos,
                                                  IEfRepository<TransientPimDbContext, Channel> efRepository,
                                                  IAssignmentService assignmentService,
                                                  IEventBus eventBus) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _efRepository = efRepository;
            _assignmentService = assignmentService;
            _eventBus = eventBus;
        }

        protected override async Task ExecuteAsync(MarketingAutomationEvent<CategoryCreatedEvent> @event)
        {
            var isAssignemt = _assignmentService.IsAssignment(AssortmentAssignmentType.Category, @event.CorrelationId.Value, Guid.Parse(@event.InnerEvent.AggregateId));
            if (isAssignemt)
            {
                var channel = await _efRepository.GetByIdAsync(@event.CorrelationId.Value);
                if (channel != null)
                {
                    var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                    optionsBuilder.UseNpgsql(channel.ServerInformation);
                    bool createdNewCategory = false;
                    using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
                    {
                        var entity = dbContext.Products.FirstOrDefault(x => x.Id == Guid.Parse(@event.InnerEvent.AggregateId));
                        if (entity == null)
                        {
                            await dbContext.Categories.AddAsync(new CatalogCategory()
                            {
                                Id = Guid.Parse(@event.InnerEvent.AggregateId),
                                Name = @event.InnerEvent.Name,
                                Description = @event.InnerEvent.Description
                            });
                            await dbContext.SaveChangesAsync();
                            createdNewCategory = true;
                        }
                    };

                    if (createdNewCategory)
                    {
                        await _eventBus.PublishAsync(new SynchronizationCreatedEvent(channel.Id.ToString())
                        {
                            Action = SynchAction.Add,
                            Entity = SynchEntity.Category,
                            RefId = Guid.Parse(@event.InnerEvent.AggregateId)
                        });
                    }
                }
            }

        }
    }
}
