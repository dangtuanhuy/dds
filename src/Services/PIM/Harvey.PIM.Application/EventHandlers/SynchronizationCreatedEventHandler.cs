﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Events;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Harvey.PIM.Application.EventHandlers
{
    public sealed class SynchronizationCreatedEventHandler : EventHandlerBase<SynchronizationCreatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, Channel> _efRepository;
        private readonly TransientPimDbContext _dbContext;
        public SynchronizationCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<SynchronizationCreatedEvent>> logger,
            IEfRepository<TransientPimDbContext, Channel> efRepository,
            TransientPimDbContext dbContext) : base(repository, logger)
        {
            _efRepository = efRepository;
            _dbContext = dbContext;
        }

        protected override async Task ExecuteAsync(SynchronizationCreatedEvent @event)
        {
            var channel = await _efRepository.GetByIdAsync(Guid.Parse(@event.AggregateId));
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channel.ServerInformation);

            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                await dbContext.AddSynchronization(@event.Action, @event.Entity, @event.RefId);
                await dbContext.SaveChangesAsync();
            };
        }
    }
}
