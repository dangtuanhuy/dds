﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.PurchaseOrders
{
    public class PurchaseOrderDeletedEventHandler : EventHandlerBase<PurchaseOrderDeletedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, PurchaseOrder> _poRepository;
        private readonly IEfRepository<TransientPimDbContext, PurchaseOrderItem> _poItemRepository;
        public PurchaseOrderDeletedEventHandler(IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<PurchaseOrderDeletedEvent>> logger,
            IEfRepository<TransientPimDbContext, PurchaseOrder> poRepository,
            IEfRepository<TransientPimDbContext, PurchaseOrderItem> poItemRepository) : base(repository, logger)
        {
            _poRepository = poRepository;
            _poItemRepository = poItemRepository;
        }

        protected override async Task ExecuteAsync(PurchaseOrderDeletedEvent @event)
        {
            var po = await _poRepository.GetByIdAsync(@event.Id);

            if (po != null)
            {
                await _poRepository.DeleteAsync(po);

                var poItems = await _poItemRepository.GetAsync();
                poItems = poItems.Where(x => x.PurchaseOrderId == @event.Id).ToList();

                foreach (var item in poItems)
                {
                    await _poItemRepository.DeleteAsync(item);
                }
            }

            await _poRepository.SaveChangesAsync();
            await _poItemRepository.SaveChangesAsync();
        }
    }
}
