﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.PurchaseOrders
{
    public class PurchaseOrderConfirmStatusEventHandler : EventHandlerBase<PurchaseOrderConfirmStatusEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, PurchaseOrder> _poRepository;
        private readonly IEfRepository<TransientPimDbContext, PurchaseOrderItem> _poItemRepository;
        public PurchaseOrderConfirmStatusEventHandler(IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<PurchaseOrderConfirmStatusEvent>> logger,
            IEfRepository<TransientPimDbContext, PurchaseOrder> poRepository,
            IEfRepository<TransientPimDbContext, PurchaseOrderItem> poItemRepository) : base(repository, logger)
        {
            _poRepository = poRepository;
            _poItemRepository = poItemRepository;
        }
        protected override async Task ExecuteAsync(PurchaseOrderConfirmStatusEvent @event)
        {
            await _poRepository.AddAsync(new PurchaseOrder(){
                Id = @event.Id,
                Name = @event.Name,
                Description = @event.Description,
                VendorId = @event.VendorId,
                ApprovalStatus = (ApprovalStatus)@event.ApprovalStatus,
                Type = (PurchaseOrderType)@event.Type
            });

            foreach (var item in @event.PurchaseOrderItems)
            {
                var poItem = ConvertDynamicToPOI(item);
                await _poItemRepository.AddAsync(new PurchaseOrderItem()
                {
                    Id = poItem.Id,
                    PurchaseOrderId = poItem.PurchaseOrderId,
                    Quantity = poItem.Quantity,
                    StockTypeId = poItem.StockTypeId,
                    VariantId = poItem.VariantId,
                    CurrencyCode = poItem.CurrencyCode,
                    CostValue = poItem.CostValue
                });
            }

            await _poRepository.SaveChangesAsync();
            await _poItemRepository.SaveChangesAsync();
        }

        private PurchaseOrderItem ConvertDynamicToPOI(dynamic item)
        {
            var result = new PurchaseOrderItem();

            foreach (var i in item)
            {
                switch (i.Name)
                {
                    case "id":
                        result.Id = i.Value;
                        break;
                    case "purchaseOrderId":
                        result.PurchaseOrderId = i.Value;
                        break;
                    case "variantId":
                        result.VariantId = i.Value;
                        break;
                    case "stockTypeId":
                        result.StockTypeId = i.Value;
                        break;
                    case "quantity":
                        result.Quantity = i.Value;
                        break;
                    case "costValue":
                        result.CostValue = i.Value;
                        break;
                    case "currencyCode":
                        result.CurrencyCode = i.Value;
                        break;
                }
            }

            return result;
        }
    }
}
