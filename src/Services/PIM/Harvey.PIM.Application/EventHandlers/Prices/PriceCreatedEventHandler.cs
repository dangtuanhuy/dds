﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Prices;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Events.Products
{
    public sealed class PriceCreatedEventHandler : EventHandlerBase<PriceCreatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, Price> _efRepository;
        private readonly IEfRepository<TransientPimDbContext, Variant> _variantEfRepository;
        public PriceCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<PriceCreatedEvent>> logger,
            IEfRepository<TransientPimDbContext, Price> efRepository,
            IEfRepository<TransientPimDbContext, Variant> variantEfRepository) : base(repository, logger)
        {
            _efRepository = efRepository;
            _variantEfRepository = variantEfRepository;
        }
        protected override async Task ExecuteAsync(PriceCreatedEvent @event)
        {
            var price = new Price()
            {
                Id = @event.PriceId,
                StaffPrice = @event.StaffPrice,
                MemberPrice = @event.MemberPrice,
                ListPrice = @event.ListPrice,
                PreOrderPrice = @event.PreOrderPrice,
                HistoryId = @event.VariantId
            };
            await _efRepository.AddAsync(price);
            await _efRepository.SaveChangesAsync();
            var variant = await _variantEfRepository.GetByIdAsync(@event.VariantId);
            if (variant == null)
            {
                Logger.LogInformation($"Variant {@event.VariantId} is not presented. Event Id {@event.Id} is requeued.");
                throw new InvalidOperationException($"Variant {@event.VariantId} is not presented. ");
            }
            variant.PriceId = price.Id;
            await _variantEfRepository.UpdateAsync(variant);
            await _variantEfRepository.SaveChangesAsync();
        }
    }
}
