﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.StockAndPriceLevels
{
    public sealed class StockAndPriceLevelUpdatedEventHandler : EventHandlerBase<StockAndPriceLevelUpdatedEvent>
    {
        private IHubContext<StockAndPriceLevelHub, IStockAndPriceLevelClient> _stockAndPriceLevelHubContext;
        private readonly IEfRepository<TransactionDbContext, StockTransaction> _stockTransactionRepository;
        private readonly IEfRepository<PimDbContext, Location> _locationRepository;
        private readonly IEfRepository<PimDbContext, Product> _productRepository;
        private readonly IEfRepository<PimDbContext, Variant> _variantRepository;
        private readonly IEfRepository<PimDbContext, Category> _categoryRepository;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        private ILogger<EventHandlerBase<StockAndPriceLevelUpdatedEvent>> _logger;

        public StockAndPriceLevelUpdatedEventHandler(IHubContext<StockAndPriceLevelHub, IStockAndPriceLevelClient> stockAndPriceLevelHubContext,
             ILogger<EventHandlerBase<StockAndPriceLevelUpdatedEvent>> logger,
             IRepository<IdentifiedEvent> repository,
             IEfRepository<TransactionDbContext, StockTransaction> stockTransactionRepository,
             IEfRepository<PimDbContext, Location> locationRepository,
             IEfRepository<PimDbContext, Product> productRepository,
             IEfRepository<PimDbContext, Variant> variantRepository,
             IEfRepository<PimDbContext, Category> categoryRepository,
             TransactionDbContext transactionDbContext,
             PimDbContext pimDbContext) : base(repository, logger)
        {
            _stockAndPriceLevelHubContext = stockAndPriceLevelHubContext;
            _stockTransactionRepository = stockTransactionRepository;
            _locationRepository = locationRepository;
            _productRepository = productRepository;
            _variantRepository = variantRepository;
            _categoryRepository = categoryRepository;
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(StockAndPriceLevelUpdatedEvent @event)
        {
            if (@event == null || @event.stockAndPrices == null || !@event.stockAndPrices.Any())
            {
                _logger.LogError("@event or stockAndPrices is null or empty");
                return;
            }

            return;

            var stockAndPricesUpdated = new List<StockAndPriceLevelModel>();
            foreach (var item in @event.stockAndPrices)
            {
                var stockAndPriceLevel = ConvertDynamicToStockAndPriceLevel(item);
                stockAndPricesUpdated.Add(stockAndPriceLevel);
            }
            var variantIds = stockAndPricesUpdated.Select(x => x.VariantId).Distinct();
            var variants = _pimDbContext.Variants.Where(x => variantIds.Contains(x.Id)).ToList();

            var productIds = stockAndPricesUpdated.Select(x => x.ProductId).Distinct();
            var products = _pimDbContext.Products.Where(x => productIds.Contains(x.Id)).ToList();

            var locationIds = stockAndPricesUpdated.Select(x => x.LocationId).Distinct();
            var locations = _pimDbContext.Locations.Where(x => locationIds.Contains(x.Id)).ToList();

            var categoryIds = products.Select(x => x.CategoryId);
            var categories = _pimDbContext.Categories.Where(x => categoryIds.Contains(x.Id)).ToList();

            var priceIds = variants.Select(x => x.PriceId);
            var prices = _pimDbContext.Prices.Where(x => priceIds.Contains(x.Id)).ToList();

            var stockTransactions = _transactionDbContext.StockTransactions.Where(x => variantIds.Contains(x.VariantId) && locationIds.Contains(x.LocationId)).ToList();

            var transactions = (from st in stockTransactions
                                group st by new { st.VariantId, st.LocationId } into groupSt
                                let latestData = groupSt.OrderByDescending(x => x.CreatedDate).FirstOrDefault()
                                join location in locations on latestData.LocationId equals location.Id
                                join variant in variants on latestData.VariantId equals variant.Id
                                join price in prices on variant.PriceId equals price.Id
                                join product in products on variant.ProductId equals product.Id
                                join category in categories on product.CategoryId equals category.Id
                                select new StockAndPriceLevelModel
                                {
                                    LocationType = location.Type,
                                    LocationId = location.Id,
                                    LocationName = location.Name,
                                    CategoryName = category.Name,
                                    ProductId = product.Id,
                                    ProductName = product.Name,
                                    VariantId = variant.Id,
                                    VariantName = variant.Name,
                                    SKUCode = variant.SKUCode,
                                    Quantity = latestData.Balance,
                                    ListPrice = price.ListPrice,
                                    MemberPrice = price.MemberPrice,
                                    StaffPrice = price.StaffPrice
                                }).ToList();

            await _stockAndPriceLevelHubContext.Clients.All.BroadcastMessage(transactions);
        }

        private StockAndPriceLevelModel ConvertDynamicToStockAndPriceLevel(dynamic item)
        {
            var result = new StockAndPriceLevelModel();

            foreach (var i in item)
            {
                switch (i.Name)
                {
                    case "locationType":
                        result.LocationType = i.Value;
                        break;
                    case "locationId":
                        result.LocationId = i.Value;
                        break;
                    case "productId":
                        result.ProductId = i.Value;
                        break;
                    case "variantId":
                        result.VariantId = i.Value;
                        break;
                    case "skuCode":
                        result.SKUCode = i.Value;
                        break;
                    case "quantity":
                        result.Quantity = i.Value;
                        break;
                }
            }

            return result;
        }
    }
}
