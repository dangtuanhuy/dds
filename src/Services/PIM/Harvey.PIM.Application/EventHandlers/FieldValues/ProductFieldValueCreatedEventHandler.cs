﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.FieldValues;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.FieldValues
{
    public class ProductFieldValueCreatedEventHandler : EventHandlerBase<FieldValueCreatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, FieldValue> _efRepository;
        public ProductFieldValueCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<FieldValueCreatedEvent>> logger,
            IEfRepository<TransientPimDbContext, FieldValue> efRepository) : base(repository, logger)
        {
            _efRepository = efRepository;
        }
        protected override async Task ExecuteAsync(FieldValueCreatedEvent @event)
        {
            var fieldValue = FieldValueFactory.CreateFromFieldType(@event.FieldType, @event.FieldValue);
            fieldValue.Id = @event.FieldValueId;
            fieldValue.FieldId = @event.FieldId;
            fieldValue.EntityId = @event.EntityId;
            await _efRepository.AddAsync(fieldValue);
            await _efRepository.SaveChangesAsync();
        }
    }
}
