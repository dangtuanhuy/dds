﻿using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.FieldValues;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure;
using Microsoft.Extensions.Logging;

namespace Harvey.PIM.Application.EventHandlers.FieldValues
{
    public sealed class ProductFieldValueUpdatedEventHandler : EventHandlerBase<FieldValueUpdatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, FieldValue> _efRepository;
        public ProductFieldValueUpdatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<FieldValueUpdatedEvent>> logger,
            IEfRepository<TransientPimDbContext, FieldValue> efRepository) : base(repository, logger)
        {
            _efRepository = efRepository;
        }
        protected override async Task ExecuteAsync(FieldValueUpdatedEvent @event)
        {
            var field = await _efRepository.GetByIdAsync(@event.FieldValueId);
            var fieldValue = FieldValueFactory.CreateFromFieldType(@event.FieldType, @event.FieldValue, field);
            await _efRepository.UpdateAsync(fieldValue);
            await _efRepository.SaveChangesAsync();
        }
    }
}
