﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Products;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.Search.Abstractions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.Products
{
    public sealed class ProductCreatedIndexingEventHandler : EventHandlerBase<ProductCreatedEvent>
    {
        private readonly ISearchService _searchService;
        public ProductCreatedIndexingEventHandler(
            IRepository<IdentifiedEvent> repository, ISearchService searchService,
            ILogger<EventHandlerBase<ProductCreatedEvent>> logger) : base(repository, logger)
        {
            _searchService = searchService;
        }
        protected override async Task ExecuteAsync(ProductCreatedEvent @event)
        {
            var document = new ProductSearchIndexedItem(new ProductSearchItem(@event.AggregateId)
            {
                Name = @event.Name,
                Description = @event.Description,
                IndexingValue = @event.IndexingValue,
                CategoryId = @event.CategoryId,
                CategoryName = @event.CategoryName,
                Code = @event.Code,
                IsDelete = @event.IsDelete,
                CreatedDate = @event.CreatedDate,
                CreatedBy = @event.CreatedBy,
                UpdatedDate = @event.CreatedDate,
                UpdatedBy = @event.CreatedBy
            });
            await _searchService.AddAsync(document);
        }
    }
}
