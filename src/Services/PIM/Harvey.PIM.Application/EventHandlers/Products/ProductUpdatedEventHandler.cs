﻿using System;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Products;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.Search.Abstractions;
using Microsoft.Extensions.Logging;

namespace Harvey.PIM.Application.EventHandlers.Products
{
    public sealed class ProductUpdatedEventHandler : EventHandlerBase<ProductUpdatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, Product> _efRepository;
        private readonly ISearchService _searchService;
        public ProductUpdatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<ProductUpdatedEvent>> logger,
            IEfRepository<TransientPimDbContext, Product> efRepository,
            ISearchService searchService) : base(repository, logger)
        {
            _efRepository = efRepository;
            _searchService = searchService;
        }
        protected override async Task ExecuteAsync(ProductUpdatedEvent @event)
        {
            var product = await _efRepository.GetByIdAsync(Guid.Parse(@event.AggregateId));
            product.Name = @event.Name;
            product.Description = @event.Description;
            product.CategoryId = @event.CategoryId;
            product.Code = @event.Code;
            product.IsDelete = @event.IsDelete;
            product.UpdatedBy = @event.Creator;
            product.CreatedDate = @event.CreatedDate;
            product.UpdatedDate = @event.UpdatedDate;
            await _efRepository.UpdateAsync(product);
            await _efRepository.SaveChangesAsync();
        }
    }
}
