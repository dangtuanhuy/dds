﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Products;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.Search.Abstractions;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.Products
{
    public sealed class ProductCreatedEventHandler : EventHandlerBase<ProductCreatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, Product> _efRepository;
        private readonly ISearchService _searchService;
        public ProductCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<ProductCreatedEvent>> logger,
            IEfRepository<TransientPimDbContext, Product> efRepository,
            ISearchService searchService) : base(repository, logger)
        {
            _efRepository = efRepository;
            _searchService = searchService;
        }

        protected override async Task ExecuteAsync(ProductCreatedEvent @event)
        {
            await _efRepository.AddAsync(new Product()
            {
                Id = Guid.Parse(@event.AggregateId),
                Name = @event.Name,
                CategoryId = @event.CategoryId,
                Description = @event.Description,
                FieldTemplateId = @event.FieldTemplateId,
                Code = @event.Code,
                IsDelete = false,
                CreatedBy = @event.Creator,
                UpdatedBy = @event.Creator,
                CreatedDate = @event.CreatedDate,
                UpdatedDate = @event.CreatedDate
            });
            await _efRepository.SaveChangesAsync();
        }
    }
}
