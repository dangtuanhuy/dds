﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Products;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.Search.Abstractions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.Products
{
    public class ProductDeletedIndexingEventHandler : EventHandlerBase<ProductDeletedEvent>
    {
        private readonly ISearchService _searchService;
        public ProductDeletedIndexingEventHandler(
            IRepository<IdentifiedEvent> repository, ISearchService searchService,
            ILogger<EventHandlerBase<ProductDeletedEvent>> logger) : base(repository, logger)
        {
            _searchService = searchService;
        }

        protected override async Task ExecuteAsync(ProductDeletedEvent @event)
        {
            var document = new ProductSearchIndexedItem(new ProductSearchItem(@event.AggregateId)
            {
                Name = @event.Name,
                Description = @event.Description,
                IndexingValue = @event.IndexingValue,
                CategoryId = @event.CategoryId,
                CategoryName = @event.CategoryName,
                Code = @event.Code,
                IsDelete = @event.IsDelete
            });
            await _searchService.UpdateAsync(document);
        }
    }
}
