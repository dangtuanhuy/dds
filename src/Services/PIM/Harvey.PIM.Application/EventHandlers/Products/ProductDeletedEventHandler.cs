﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Products;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.Search.Abstractions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.Products
{
    public class ProductDeletedEventHandler : EventHandlerBase<ProductDeletedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, Product> _efRepository;
        private readonly ISearchService _searchService;
        public ProductDeletedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<ProductDeletedEvent>> logger,
            IEfRepository<TransientPimDbContext, Product> efRepository,
            ISearchService searchService) : base(repository, logger)
        {
            _efRepository = efRepository;
            _searchService = searchService;
        }
        protected override async Task ExecuteAsync(ProductDeletedEvent @event)
        {
            var product = await _efRepository.GetByIdAsync(Guid.Parse(@event.AggregateId));
            product.IsDelete = @event.IsDelete;
            product.UpdatedDate = @event.UpdatedDate;
            await _efRepository.UpdateAsync(product);
            await _efRepository.SaveChangesAsync();
        }
    }
}
