﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.StockLevels;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.StockLevels
{
    public sealed class StockLevelUpdatedEventHandler : EventHandlerBase<StockLevelUpdatedEvent>
    {
        private readonly ISearchService _searchService;
        private IHubContext<StockLevelHub, IStockLevelClient> _stockLevelHubContext;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        public StockLevelUpdatedEventHandler(
            IHubContext<StockLevelHub, IStockLevelClient> stockLevelHubContext,
            ISearchService searchService,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<StockLevelUpdatedEvent>> logger,
            PimDbContext pimDbContext,
            TransactionDbContext transactionDbContext) : base(repository, logger)
        {
            _searchService = searchService;
            _stockLevelHubContext = stockLevelHubContext;
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
        }

        protected override async Task ExecuteAsync(StockLevelUpdatedEvent @event)
        {
            var locations = _pimDbContext.Locations.ToList();

            //TODO need to fix for paging large 10000 items
            //https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-from-size.html
            //https://www.elastic.co/guide/en/elasticsearch/client/net-api/1.x/scroll.html
            var stockLevelSearchQuery = new StockLevelSearchQuery()
            {
                NumberItemsPerPage = 10000,
                Page = 1,
                QueryText = @event.LocationId != null ? @event.LocationId.ToString() : null
            };

            var stockLevelResult = await _searchService.SearchAsync<StockLevelSearchItem, StockLevelSearchResult>(stockLevelSearchQuery);

            var productIds = stockLevelResult.Results.Select(x => x.Item.ProductId).Distinct();
            var variantIds = stockLevelResult.Results.Select(x => x.Item.VariantId).Distinct();

            var productNames = await _pimDbContext.Products.AsNoTracking()
               .Where(x => productIds.Contains(x.Id))
               .Select(x => new
               {
                   Id = x.Id,
                   Name = x.Name
               })
               .ToListAsync();

            var SKUCodes = await _pimDbContext.Variants.AsNoTracking()
               .Where(x => variantIds.Contains(x.Id))
               .Select(x => new
               {
                   Id = x.Id,
                   SKU = x.SKUCode
               })
               .ToListAsync();

            var fields = await _pimDbContext.FieldValues
                .AsNoTracking()
                .Where(x => variantIds.Contains(x.EntityId))
                .Include(x => x.Field)
                .Select(x => new
                {
                    VariantId = x.EntityId,
                    Name = x.Field.Name,
                    Value = FieldValueFactory.GetFieldValueFromFieldType(x.Field.Type, x)
                })
                .ToListAsync();

            var result = await _searchService.SearchAsync<StockLevelSearchItem, StockLevelSearchResult>(stockLevelSearchQuery);

            var stockLevels = result.Results.Select(x => new StockLevelModel()
            {
                ReferenceId = x.Item.ReferenceId.ToString(),
                LocationId = x.Item.LocationId,
                LocationName = locations.FirstOrDefault(y => y.Id == x.Item.LocationId)?.Name,
                ProductId = x.Item.ProductId,
                VariantId = x.Item.VariantId,
                Balance = x.Item.Balance,
                ProductName = productNames.FirstOrDefault(pro => pro.Id == x.Item.ProductId)?.Name,
                Fields = fields.Where(f => f.VariantId == x.Item.VariantId).Select(f => new VariantField
                {
                    Name = f.Name,
                    Value = f.Value
                }).ToList(),
                BarCodes = null,
                SKUCode = SKUCodes.FirstOrDefault(variant => variant.Id == x.Item.VariantId)?.SKU
            }).ToList();

            await _stockLevelHubContext.Clients.All.BroadcastMessage(stockLevels);
        }
    }
}
