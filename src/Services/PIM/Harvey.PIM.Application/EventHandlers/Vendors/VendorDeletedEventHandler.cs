﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Vendors;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.Vendors
{
    public class VendorDeletedEventHandler : EventHandlerBase<VendorDeletedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, Vendor> _efRepository;
        public VendorDeletedEventHandler(IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<VendorDeletedEvent>> logger,
            IEfRepository<TransientPimDbContext, Vendor> efRepository) : base(repository, logger)
        {
            _efRepository = efRepository;
        }
        protected override async Task ExecuteAsync(VendorDeletedEvent @event)
        {
            var vendor = await _efRepository.GetByIdAsync(@event.Id);
            if (vendor != null )
            {
                await _efRepository.DeleteAsync(vendor);
                await _efRepository.SaveChangesAsync();
            }
        }
    }
}
