﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Vendors;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.Vendors
{
    public class VendorUpdatedEventHandler : EventHandlerBase<VendorUpdatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, Vendor> _efRepository;
        public VendorUpdatedEventHandler(IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<VendorUpdatedEvent>> logger,
            IEfRepository<TransientPimDbContext, Vendor> efRepository) : base(repository, logger)
        {
            _efRepository = efRepository;
        }
        protected override async Task ExecuteAsync(VendorUpdatedEvent @event)
        {
            var vendor = await _efRepository.GetByIdAsync(@event.Id);
            if (vendor != null)
            {
                vendor.Name = @event.Name;
                vendor.Active = @event.Active;
                vendor.Description = @event.Description;
                vendor.Address1 = @event.Address1;
                vendor.Address2 = @event.Address2;
                vendor.Email = @event.Email;
                vendor.VendorUrl = @event.VendorUrl;
                vendor.VendorCode = @event.VendorCode;
                vendor.Country = @event.Country;
                vendor.CityName = @event.CityName;
                vendor.CityCode = @event.CityCode;
                vendor.Phone = @event.Phone;
                vendor.PaymentTermName = @event.PaymentTermName;
                vendor.CurrencyName = @event.CurrencyName;
                vendor.TaxTypeValue = @event.TaxTypeValue;
                vendor.ZipCode = @event.ZipCode;
                vendor.Fax = @event.Fax;
                await _efRepository.UpdateAsync(vendor);
            }
            else
            {
                await _efRepository.AddAsync(new Vendor()
                {
                    Id = @event.Id,
                    Name = @event.Name,
                    Active = @event.Active,
                    Description = @event.Description,
                    Address1 = @event.Address1,
                    Address2 = @event.Address2,
                    Email = @event.Email,
                    VendorUrl = @event.VendorUrl,
                    VendorCode = @event.VendorCode,
                    Country = @event.Country,
                    CityName = @event.CityName,
                    CityCode = @event.CityCode,
                    Phone = @event.Phone,
                    PaymentTermName = @event.PaymentTermName,
                    CurrencyName = @event.CurrencyName,
                    TaxTypeValue = @event.TaxTypeValue,
                    ZipCode = @event.ZipCode,
                    Fax = @event.Fax
                });
            }
            await _efRepository.SaveChangesAsync();
        }
    }
}
