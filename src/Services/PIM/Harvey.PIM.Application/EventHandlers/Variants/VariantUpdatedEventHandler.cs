﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Variants;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.EventHandlers.Variants
{
    public sealed class VariantUpdatedEventHandler : EventHandlerBase<VariantUpdatedEvent>
    {
        private readonly IEfRepository<TransientPimDbContext, Variant> _efRepository;
        public VariantUpdatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<VariantUpdatedEvent>> logger,
            IEfRepository<TransientPimDbContext, Variant> efRepository) : base(repository, logger)
        {
            _efRepository = efRepository;
        }

        protected override async Task ExecuteAsync(VariantUpdatedEvent @event)
        {
            var variant = await _efRepository.GetByIdAsync(Guid.Parse(@event.AggregateId));
            if (variant != null)
            {
                variant.Name = @event.Name;
                variant.Description = @event.Description;
                variant.UpdatedDate = @event.UpdatedDate;
                await _efRepository.UpdateAsync(variant);
                await _efRepository.SaveChangesAsync();
            }
        
        }
    }
}
