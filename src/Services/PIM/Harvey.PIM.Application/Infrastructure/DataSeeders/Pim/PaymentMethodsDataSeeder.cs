﻿using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.DataSeeders.Pim
{
    public static class PaymentMethodsDataSeeder
    {
        private static List<PaymentMethod> PaymentMethods { get; set; } = new List<PaymentMethod>()
        {
            new PaymentMethod { Code = "CASH",  Name = "CASH" },
            new PaymentMethod { Code = "NETS",  Name = "NETS" },
            new PaymentMethod { Code = "VISA",  Name = "VISA" },
            new PaymentMethod { Code = "MASTER", Name = "MASTER" },
            new PaymentMethod { Code = "CASH-CARD", Name = "CASH CARD" },
            new PaymentMethod { Code = "AMEX", Name = "AMEX" },
            new PaymentMethod { Code = "VOUCHER", Name = "VOUCHER" },
            new PaymentMethod { Code = "OCBC", Name = "OCBC" },
            new PaymentMethod { Code = "DBS", Name = "DBS" },
            new PaymentMethod { Code = "E-WALLET", Name = "E-WALLET" },
            new PaymentMethod { Code = "E-VOUCHERS", Name = "E-VOUCHERS" },
            new PaymentMethod { Code = "E-POINT", Name = "E-POINT" },
            new PaymentMethod { Code = "MALL-VOUCHER", Name = "MALL VOUCHER" },
            new PaymentMethod { Code = "SAFRA-VOUCHER", Name = "SAFRA VOUCHER" },
            new PaymentMethod { Code = "UNIONPAY", Name = "UNIONPAY" },
            new PaymentMethod { Code = "DBS-INSTALMENT", Name = "DBS INSTALMENT" },
            new PaymentMethod { Code = "OCBC-INSTALMENT", Name = "OCBC INSTALMENT" },
            new PaymentMethod { Code = "SHOPEE", Name = "SHOPEE" },
            new PaymentMethod { Code = "ALIPAY", Name = "ALIPAY" },
            new PaymentMethod { Code = "DBS-MAX", Name = "DBS MAX" }
        };

        public static async Task SeedPaymentMethods(PimDbContext dbcontext)
        {
            var count = await dbcontext.PaymentMethods.CountAsync();
            if (count == 0)
            {
                await dbcontext.PaymentMethods.AddRangeAsync(PaymentMethods);
            }
        }
    }
}
