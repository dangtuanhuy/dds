﻿using Harvey.PIM.Application.Extensions;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.DataSeeders.Pim
{
    public static class VariantDataSeeder
    {
        public static void UpdateVariant(PimDbContext dbcontext)
        {
            var variants = dbcontext.Variants.Where(x => x.Name == null);
            var products = dbcontext.Products.Where(x => x.IsDelete == false);
            var fieldValues = dbcontext.FieldValues;

            var variantsData = (from v in variants
                                join p in products on v.ProductId equals p.Id
                                let vData = v
                                join f in fieldValues on vData.Id equals f.EntityId
                                into variantFields
                               select new VariantModels
                                {
                                    ProductId = p.Id,
                                    ProductName = p.Name,
                                    Variant = v,
                                   FieldValues = variantFields.ToList()
                               }).ToList();


            if (variantsData.Any())
            {
                var fields = dbcontext.Fields.ToList();
                foreach (var item in variantsData)
                {
                    var variantName = VariantExtensions.GetVariantName(item.FieldValues, fields);
                    var variantDescription = VariantExtensions.GetVariantDescription(item.ProductName, item.FieldValues, fields);

                    item.Variant.Name = variantName;
                    item.Variant.Description = variantDescription;

                    dbcontext.Variants.Update(item.Variant);
                }
            }

        }

        private class VariantModels
        {
            public Guid ProductId { get; set; }
            public string ProductName { get; set; }
            public Variant Variant { get; set; }
            public List<FieldValue> FieldValues { get; set; }
        }
    }
}
