﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.Migrations
{
    public partial class add_column_currency_code_and_cost_value_in_purchase_order_item_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "CostValue",
                table: "purchaseOrderItems",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<string>(
                name: "CurrencyCode",
                table: "purchaseOrderItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CostValue",
                table: "purchaseOrderItems");

            migrationBuilder.DropColumn(
                name: "CurrencyCode",
                table: "purchaseOrderItems");
        }
    }
}
