﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.Migrations
{
    public partial class updatevendormodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address1",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address2",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CityCode",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CityName",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrencyName",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fax",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentTermName",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TaxTypeValue",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VendorCode",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VendorUrl",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ZipCode",
                table: "Vendors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address1",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Address2",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CityCode",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CityName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CurrencyName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Fax",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "PaymentTermName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "TaxTypeValue",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "VendorCode",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "VendorUrl",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "ZipCode",
                table: "Vendors");
        }
    }
}
