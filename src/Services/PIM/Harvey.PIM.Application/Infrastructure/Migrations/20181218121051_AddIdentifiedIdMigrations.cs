﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.Migrations
{
    public partial class AddIdentifiedIdMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IdentifiedId",
                table: "FieldTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IdentifiedId",
                table: "Fields",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IdentifiedId",
                table: "Categories",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdentifiedId",
                table: "FieldTemplates");

            migrationBuilder.DropColumn(
                name: "IdentifiedId",
                table: "Fields");

            migrationBuilder.DropColumn(
                name: "IdentifiedId",
                table: "Categories");
        }
    }
}
