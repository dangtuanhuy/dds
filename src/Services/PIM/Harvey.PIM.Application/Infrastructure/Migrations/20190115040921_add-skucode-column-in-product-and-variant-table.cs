﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.Migrations
{
    public partial class addskucodecolumninproductandvarianttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SKUCode",
                table: "Variants",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SKUCode",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SKUCode",
                table: "Variants");

            migrationBuilder.DropColumn(
                name: "SKUCode",
                table: "Products");
        }
    }
}
