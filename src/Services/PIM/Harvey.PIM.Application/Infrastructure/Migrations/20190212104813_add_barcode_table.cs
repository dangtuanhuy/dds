﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.Migrations
{
    public partial class add_barcode_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BarCode",
                table: "Variants");

            migrationBuilder.CreateTable(
                name: "BarCode",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    VariantId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarCode", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BarCode_Variants_VariantId",
                        column: x => x.VariantId,
                        principalTable: "Variants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BarCode_Code",
                table: "BarCode",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BarCode_VariantId",
                table: "BarCode",
                column: "VariantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BarCode");

            migrationBuilder.AddColumn<string>(
                name: "BarCode",
                table: "Variants",
                nullable: true);
        }
    }
}
