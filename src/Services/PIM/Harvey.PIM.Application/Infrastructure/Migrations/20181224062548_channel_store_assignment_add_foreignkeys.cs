﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.Migrations
{
    public partial class channel_store_assignment_add_foreignkeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ChannelStoreAssignments_ChannelId",
                table: "ChannelStoreAssignments",
                column: "ChannelId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChannelStoreAssignments_Channels_ChannelId",
                table: "ChannelStoreAssignments",
                column: "ChannelId",
                principalTable: "Channels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ChannelStoreAssignments_Locations_StoreId",
                table: "ChannelStoreAssignments",
                column: "StoreId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChannelStoreAssignments_Channels_ChannelId",
                table: "ChannelStoreAssignments");

            migrationBuilder.DropForeignKey(
                name: "FK_ChannelStoreAssignments_Locations_StoreId",
                table: "ChannelStoreAssignments");

            migrationBuilder.DropIndex(
                name: "IX_ChannelStoreAssignments_ChannelId",
                table: "ChannelStoreAssignments");
        }
    }
}
