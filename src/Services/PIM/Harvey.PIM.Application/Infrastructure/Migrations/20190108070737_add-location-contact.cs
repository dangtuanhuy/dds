﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.Migrations
{
    public partial class addlocationcontact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContactPerson",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone1",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone2",
                table: "Locations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContactPerson",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Phone1",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Phone2",
                table: "Locations");
        }
    }
}
