﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.Migrations
{
    public partial class UpdateFieldIdentifiedIdMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_FieldTemplates_IdentifiedId",
                table: "FieldTemplates",
                column: "IdentifiedId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fields_IdentifiedId",
                table: "Fields",
                column: "IdentifiedId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Categories_IdentifiedId",
                table: "Categories",
                column: "IdentifiedId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FieldTemplates_IdentifiedId",
                table: "FieldTemplates");

            migrationBuilder.DropIndex(
                name: "IX_Fields_IdentifiedId",
                table: "Fields");

            migrationBuilder.DropIndex(
                name: "IX_Categories_IdentifiedId",
                table: "Categories");
        }
    }
}
