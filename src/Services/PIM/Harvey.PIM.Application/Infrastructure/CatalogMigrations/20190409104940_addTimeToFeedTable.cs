﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.CatalogMigrations
{
    public partial class addTimeToFeedTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TimeToFeeds",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    LastSyncProduct = table.Column<DateTime>(nullable: true),
                    LastSyncVariant = table.Column<DateTime>(nullable: true),
                    LastSyncCategory = table.Column<DateTime>(nullable: true),
                    LastSyncPrice = table.Column<DateTime>(nullable: true),
                    LastSyncStockType = table.Column<DateTime>(nullable: true),
                    LastSyncTransactionType = table.Column<DateTime>(nullable: true),
                    LastSyncBarCode = table.Column<DateTime>(nullable: true),
                    LastSyncPaymentMethod = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeToFeeds", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TimeToFeeds");
        }
    }
}
