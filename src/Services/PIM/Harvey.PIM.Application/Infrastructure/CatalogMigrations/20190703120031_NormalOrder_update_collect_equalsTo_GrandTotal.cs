﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.CatalogMigrations
{
    public partial class NormalOrder_update_collect_equalsTo_GrandTotal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlCommand = "update public.\"Orders\"";
            sqlCommand += "set \"Collect\" = \"GrandTotal\"";
            sqlCommand += "where \"OrderTransactionType\" = 0";

            migrationBuilder.Sql(sqlCommand);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
