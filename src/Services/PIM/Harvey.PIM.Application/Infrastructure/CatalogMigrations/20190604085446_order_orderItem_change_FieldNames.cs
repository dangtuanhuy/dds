﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.CatalogMigrations
{
    public partial class order_orderItem_change_FieldNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OldOrderId",
                table: "Orders",
                newName: "PreviousOrderId");

            migrationBuilder.RenameColumn(
                name: "OldOrderItemId",
                table: "OrderItems",
                newName: "PreviousOrderItemId");

            migrationBuilder.AddColumn<Guid>(
                name: "DeviceId",
                table: "Orders",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "OriginalOrderId",
                table: "Orders",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "Orders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeviceId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OriginalOrderId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Reason",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "PreviousOrderId",
                table: "Orders",
                newName: "OldOrderId");

            migrationBuilder.RenameColumn(
                name: "PreviousOrderItemId",
                table: "OrderItems",
                newName: "OldOrderItemId");
        }
    }
}
