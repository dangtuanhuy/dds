﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.GIWs
{
    public sealed class GetDetailsGIWByInventoryTransactionIdQuery : IQuery<InventoryTransactionTransferViewModel>
    {
        public Guid InventoryTransactionId { get; set; }
        public GetDetailsGIWByInventoryTransactionIdQuery(Guid id)
        {
            InventoryTransactionId = id;
        }
    }

    public sealed class GetDetailsGIWByInventoryTransactionIdQueryHandler : IQueryHandler<GetDetailsGIWByInventoryTransactionIdQuery, InventoryTransactionTransferViewModel>
    {
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _repository;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        public GetDetailsGIWByInventoryTransactionIdQueryHandler(IEfRepository<TransactionDbContext, InventoryTransaction> repository,
            PimDbContext pimDbContext,
            TransactionDbContext transactionDbContext)
        {
            _repository = repository;
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
        }
        public async Task<InventoryTransactionTransferViewModel> Handle(GetDetailsGIWByInventoryTransactionIdQuery query)
        {
            var inventoryTransaction = await _repository.GetByIdAsync(query.InventoryTransactionId);
            if (inventoryTransaction == null)
            {
                throw new ArgumentException($"Something went wrong. Please try again!");
            }

            var stockTransactions = _transactionDbContext.StockTransactions.AsNoTracking()
                                        .Where(x => x.InventoryTransactionId == inventoryTransaction.Id)
                                        .ToList();
            var itemsDetails = stockTransactions.GroupBy(x => new { x.VariantId }, (key, group) =>
                                new
                                {
                                    key.VariantId,
                                    Details = group
                                }).ToList();
            var variantIds = itemsDetails.Select(x => x.VariantId).Distinct();
            var variants = _pimDbContext.Variants.AsNoTracking().Where(x => variantIds.Contains(x.Id)).ToList();

            var items = new List<InventoryTransactionTransferProduct>();
            foreach (var item in itemsDetails)
            {
                var stockTransactionRefId = item.Details.First().StockTransactionRefId;
                var productId = variants.FirstOrDefault(x => x.Id == item.VariantId).ProductId;
                if (productId == null)
                {
                    throw new ArgumentException($"Variant Id :{item.VariantId} | doesn't existed");
                }
                var inventoryTransactionTransferProduct = new InventoryTransactionTransferProduct
                {
                    Id = Guid.Empty,
                    StockTransactionRefId = stockTransactionRefId,
                    TransactionRefId = stockTransactionRefId,
                    VariantId = item.VariantId,
                    ProductId = productId,
                    StockTypeId = Guid.Empty,
                    Quantity = item.Details.Sum(x => x.Quantity)
                };
                items.Add(inventoryTransactionTransferProduct);
            }

            var inventoryTransactionGIWModel = new InventoryTransactionTransferViewModel
            {
                Id = inventoryTransaction.Id,
                InventoryTransactionRefId = inventoryTransaction.TransactionRefId,
                TransferNumber = inventoryTransaction.Code,
                FromLocationId = inventoryTransaction.FromLocationId,
                FromLocation = _pimDbContext.Vendors.FirstOrDefault(x => x.Id == inventoryTransaction.FromLocationId)?.Name,
                ToLocation = _pimDbContext.Locations.FirstOrDefault(x => x.Id == inventoryTransaction.ToLocationId)?.Name,
                ToLocationId = inventoryTransaction.ToLocationId,
                CreatedDate = inventoryTransaction.CreatedDate,
                Status = inventoryTransaction.Status,
                InventoryTransactionTransferProducts = items,
                Products = GetTransferProducts(variantIds.ToList(), variants)
            };

            return inventoryTransactionGIWModel;
        }

        private List<TransferProductModel> GetTransferProducts(List<Guid> variantIds, List<Variant> variants)
        {
            var variantNames = _pimDbContext.FieldValues
                .AsNoTracking()
                .Where(x => variantIds.Contains(x.EntityId))
                .Include(x => x.Field)
                .GroupBy(x => new { x.EntityId}, (key,group) => new {
                    VariantId = key.EntityId,
                    Fields = group.Select(f => new TransferFieldModel(){
                        Name = f.Field.Name,
                        Value = FieldValueFactory.GetFieldValueFromFieldType(f.Field.Type, f)
                    }).ToList()
                })
                .Select(x => new TransferVariantModel()
                {
                    Id = x.VariantId,
                    Name = GenerateVariantName(x.Fields)
                })
                .ToList();

            var productIds = variants.Select(x => x.ProductId).Distinct();
            var products = _pimDbContext.Products.AsNoTracking().Where(x => productIds.Contains(x.Id)).ToList();

            var productsViewModel = variants.GroupBy(x => new { x.ProductId }, (key, group) => new TransferProductModel()
            {
                Id = key.ProductId,
                Name = products.FirstOrDefault(p => p.Id == key.ProductId)?.Name,
                Variants = group.Select(v => new TransferVariantModel()
                {
                    Id = v.Id,
                    Name = variantNames.FirstOrDefault(n => n.Id == v.Id)?.Name
                }).ToList()
            }).ToList();

            return productsViewModel;
        }

        private string GenerateVariantName(List<TransferFieldModel> fields)
        {
            if(fields.Count() == 0)
            {
                return null;
            }
            var variantName = "";
            foreach(var field in fields)
            {
                variantName += $"{field.Name}: {field.Value};";
            }
            return variantName;
        }
    }
}
