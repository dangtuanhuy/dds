﻿using Harvey.Domain;
using Harvey.Domain.Constant;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.GIWs
{
    public sealed class GetGIWsQuery : IQuery<PagedResult<InventoryTransactionTransferViewModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string StatusIdString { get; }
        public string ToLocationIdString { get; }
        public string FromLocationIdString { get; }
        public string QueryString { get; }
        public GetGIWsQuery(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLcationIdString, string fromLocationIdString, string queryString)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            StatusIdString = statusIdString;
            ToLocationIdString = toLcationIdString;
            FromLocationIdString = fromLocationIdString;
            QueryString = queryString;
        }
    }

    public sealed class GetGIWsQueryHandler : IQueryHandler<GetGIWsQuery, PagedResult<InventoryTransactionTransferViewModel>>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _repository;
        public GetGIWsQueryHandler(
            IEfRepository<TransactionDbContext, InventoryTransaction> repository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
        }

        public async Task<PagedResult<InventoryTransactionTransferViewModel>> Handle(GetGIWsQuery query)
        {
            var transactionTypeGIW = _transactionDbContext.TransactionTypes.AsNoTracking()
                                        .FirstOrDefault(x => x.Code == TransactionTypeCode.GIW.ToString());
            var statusIds = string.IsNullOrEmpty(query.StatusIdString) ? null : query.StatusIdString.Split("|");
            var toLocationIds = string.IsNullOrEmpty(query.ToLocationIdString) ? null : query.ToLocationIdString.Split("|");
            var fromLocationIds = string.IsNullOrEmpty(query.FromLocationIdString) ? null : query.FromLocationIdString.Split("|");

            var predicate = PredicateBuilder.True<InventoryTransaction>();
            if (!string.IsNullOrEmpty(query.QueryString))
            {
                predicate = predicate.And(x => x.TransferNumber.ToUpper().Contains(query.QueryString.ToUpper()) || x.Code.ToUpper().Contains(query.QueryString.ToUpper()));
            }
            if (!string.IsNullOrEmpty(query.StatusIdString))
            {
                predicate = predicate.And(x => statusIds.Contains(x.Status.ToString()));
            }
            if (!string.IsNullOrEmpty(query.ToLocationIdString))
            {
                predicate = predicate.And(x => toLocationIds.Contains(x.ToLocationId.ToString()));
            }
            if (!string.IsNullOrEmpty(query.FromLocationIdString))
            {
                predicate = predicate.And(x => fromLocationIds.Contains(x.FromLocationId.ToString()));
            }

            predicate = predicate.And(x => x.TransactionTypeId == transactionTypeGIW.Id);

            var inventoryTransactions = await _repository.ListAsync(predicate, 0, 0, "");

            var result = new List<InventoryTransactionTransferViewModel>();

            var vendors = _pimDbContext.Vendors.AsNoTracking().ToList();
            var locations = _pimDbContext.Locations.AsNoTracking().ToList();
            foreach (var inventoryTransaction in inventoryTransactions)
            {
                var inventoryTransactionGIWModel = new InventoryTransactionTransferViewModel
                {
                    Id = inventoryTransaction.Id,
                    InventoryTransactionRefId = inventoryTransaction.TransactionRefId,
                    TransferNumber = inventoryTransaction.Code,
                    FromLocationId = inventoryTransaction.FromLocationId,
                    FromLocation = vendors.FirstOrDefault(x => x.Id == inventoryTransaction.FromLocationId)?.Name,
                    ToLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.ToLocationId)?.Name,
                    ToLocationId = inventoryTransaction.ToLocationId,
                    CreatedDate = inventoryTransaction.CreatedDate,
                    Status = inventoryTransaction.Status
                };

                result.Add(inventoryTransactionGIWModel);
            }

            IOrderedEnumerable<InventoryTransactionTransferViewModel> orderData;

            if (!string.IsNullOrEmpty(query.PagingFilterCriteria.SortColumn))
            {
                orderData = query.PagingFilterCriteria.SortDirection == SortDirection.Ascending
                       ? result.OrderBy(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn))
                       : result.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn));
            }
            else
            {
                PropertyInfo propertyInfo =
                     result.GetType().GetGenericArguments()[0].GetProperty(SortColumnConstants.DefaultSortColumn);

                if (propertyInfo != null)
                {
                    orderData = result.OrderByDescending(a => a.UpdatedDate);
                }
                else
                {
                    orderData = result.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, SortColumnConstants.SecondSortColumn));
                }
            }

            var entities = orderData
                 .Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
                 .Take(query.PagingFilterCriteria.NumberItemsPerPage)
                 .ToList();

            return new PagedResult<InventoryTransactionTransferViewModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = result.Count,
                Data = entities
            };
        }
    }
}
