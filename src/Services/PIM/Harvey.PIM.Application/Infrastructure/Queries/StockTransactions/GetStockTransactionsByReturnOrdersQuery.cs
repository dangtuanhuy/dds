﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.StockTransactions
{
    public class GetStockTransactionsByReturnOrdersQuery : IQuery<List<StockTransaction>>
    {
        public List<Guid> Ids { get; set; }
        public GetStockTransactionsByReturnOrdersQuery(List<Guid> ids)
        {
            Ids = ids;
        }
    }

    public class GetStockTransactionsByReturnOrdersQueryHandler : IQueryHandler<GetStockTransactionsByReturnOrdersQuery, List<StockTransaction>>
    {
        private readonly IEfRepository<TransactionDbContext, StockTransaction> _stockTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, TransactionType> _transactionTypeRepository;
        public GetStockTransactionsByReturnOrdersQueryHandler(IEfRepository<TransactionDbContext, StockTransaction> stockTransactionRepository,
            IEfRepository<TransactionDbContext, TransactionType> transactionTypeRepository)
        {
            _stockTransactionRepository = stockTransactionRepository;
            _transactionTypeRepository = transactionTypeRepository;
        }
        public async Task<List<StockTransaction>> Handle(GetStockTransactionsByReturnOrdersQuery query)
        {
            var stockTransactions = await _stockTransactionRepository.GetAsync();
            var transactionTypes = await _transactionTypeRepository.GetAsync();

            var TransactionTypeGRT = transactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.GRT.ToString());
            var TransactionTypeTFO = transactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.TFO.ToString());
            var results = new List<StockTransaction>();

            if (TransactionTypeGRT != null && TransactionTypeTFO != null)
            {
                foreach (var roId in query.Ids)
                {
                    var stocks = stockTransactions
                        .Where(stock => stock.StockTransactionRefId == roId
                                && (stock.TransactionTypeId == TransactionTypeGRT.Id || stock.TransactionTypeId == TransactionTypeTFO.Id))
                        .GroupBy(stock => new { stock.VariantId, stock.StockTypeId, stock.StockTransactionRefId }, (key, group) => new StockTransaction()
                        {
                            VariantId = key.VariantId,
                            StockTypeId = key.StockTypeId,
                            StockTransactionRefId = key.StockTransactionRefId,
                            Balance = group.Sum(x => x.Quantity)
                        }).ToList();
                    results.AddRange(stocks);
                }
            }

            return results;
        }
    }
}

