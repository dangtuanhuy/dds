﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.StockTransactions
{
    public class GetStockTransactionsByPurchaseOrdersQuery: IQuery<List<StockTransaction>>
    {
        public List<Guid> Ids { get; set; }
        public GetStockTransactionsByPurchaseOrdersQuery(List<Guid> ids)
        {
            Ids = ids;
        }
    }

    public sealed class GetStockTransactionsByPurchaseOrdersQueryHandler : IQueryHandler<GetStockTransactionsByPurchaseOrdersQuery, List<StockTransaction>>
    {
        private readonly IEfRepository<TransactionDbContext, StockTransaction> _stockTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, TransactionType> _transactionTypeRepository;
        public GetStockTransactionsByPurchaseOrdersQueryHandler(IEfRepository<TransactionDbContext, StockTransaction> stockTransactionRepository,
            IEfRepository<TransactionDbContext, TransactionType> transactionTypeRepository)
        {
            _stockTransactionRepository = stockTransactionRepository;
            _transactionTypeRepository = transactionTypeRepository;
        }
        public async Task<List<StockTransaction>> Handle(GetStockTransactionsByPurchaseOrdersQuery query)
        {
            var stockTransactions = await _stockTransactionRepository.ListAsync(x => query.Ids.Contains(x.StockTransactionRefId));
            var transactionTypes = await _transactionTypeRepository.GetAsync();

            var TransactionTypeGIW = transactionTypes.FirstOrDefault(x => x.Code == "GIW");
            var TransactionTypeTFO = transactionTypes.FirstOrDefault(x => x.Code == "TFO");
            var results = new List<StockTransaction>();

            if(TransactionTypeGIW != null && TransactionTypeTFO != null)
            {
                foreach(var poId in query.Ids)
                {
                    var stocks = stockTransactions
                        .Where(stock => stock.StockTransactionRefId == poId 
                                && (stock.TransactionTypeId == TransactionTypeGIW.Id || stock.TransactionTypeId == TransactionTypeTFO.Id))
                        .GroupBy(stock => new { stock.VariantId , stock.StockTypeId, stock.StockTransactionRefId }, (key, group) => new StockTransaction()
                        {
                            VariantId = key.VariantId,
                            StockTypeId = key.StockTypeId,
                            StockTransactionRefId = key.StockTransactionRefId,
                            Balance = group.Sum(x => x.Quantity)
                        }).ToList();
                    results.AddRange(stocks);
                }
            }

            return results;
        }
    }
}
