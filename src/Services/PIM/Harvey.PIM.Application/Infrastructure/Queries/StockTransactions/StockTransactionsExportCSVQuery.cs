﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.StockTransactions
{
    public sealed class StockTransactionsExportCSVQuery : IQuery<byte[]>
    {
        public Guid LocationId { get; set; }
        public List<Guid> Categories { get; set; }
        public StockTransactionsExportCSVQuery(Guid locationId, List<Guid> categories)
        {
            LocationId = locationId;
            Categories = categories;
        }
    }

    public sealed class StockTransactionsExportCSVQueryHandler : IQueryHandler<StockTransactionsExportCSVQuery, byte[]>
    {
        private readonly IEfRepository<TransactionDbContext, StockTransaction> _stockTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, StockType> _stockTypeRepository;
        private readonly IEfRepository<PimDbContext, Product> _productRepository;
        private readonly IEfRepository<PimDbContext, Variant> _variantRepository;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        public StockTransactionsExportCSVQueryHandler(
            IEfRepository<TransactionDbContext, StockTransaction> stockTransactionRepository,
            IEfRepository<PimDbContext, Product> productRepository,
            IEfRepository<TransactionDbContext, StockType> stockTypeRepository,
            IEfRepository<PimDbContext, Variant> variantRepository,
            PimDbContext pimDbContext,
            TransactionDbContext transactionDbContext)
        {
            _stockTransactionRepository = stockTransactionRepository;
            _productRepository = productRepository;
            _stockTypeRepository = stockTypeRepository;
            _variantRepository = variantRepository;
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
        }

        public async Task<byte[]> Handle(StockTransactionsExportCSVQuery query)
        {
            var columnHeaders = new string[]
            {
                "ProductId",
                "Product_Name",
                "VariantId",
                "Variant_Name",
                "PreOrderPrice",
                "Variant_SKU",
                "Variant_BarCode",
                "Cost_Value",
                "Currency_Code",
                "LocationId",
                "Location_Name",
                "Category",
                "Stock_On_Hand_SOE",
                "Stock_On_Hand_SOR",
                "Stock_On_Hand_FIRM",
                "Stock_On_Hand_CON",
                "Stock_On_Hand",
                "Actual_Stock_On_Hand",
                "Quantity_SOE",
                "Quantity_SOR",
                "Quantity_FIRM",
                "Quantity_CON",
                "Quantity"
            };

            var locationId = query.LocationId;
            var stockTypes = await _stockTypeRepository.GetAsync();
            var stockType_SOE = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.SOE.ToString());
            var stockType_SOR = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.SOR.ToString());
            var stockType_FIRM = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.FIRM.ToString());
            var stockType_CON = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.CON.ToString());
            var stockType_Initital = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.STI.ToString());
            var categoriesIds = query.Categories;
            IEnumerable<Guid> variantIds;
            IEnumerable<StockTransactionModel> data;
            IQueryable<Product> products;

            var categories = _pimDbContext.Categories.AsNoTracking().Select(x => new
            {
                x.Id,
                x.Name
            }).ToList();

            var location = _pimDbContext.Locations.FirstOrDefault(x => x.Id == locationId);

            if (categoriesIds.Any())
            {
                products = from p in _pimDbContext.Products
                               where !p.IsDelete && categoriesIds.Contains(p.CategoryId.Value) select p;
            }
            else
            {
                products = from p in _pimDbContext.Products
                           where !p.IsDelete select p;
            }

            data = (from p in products                    
                    join c in _pimDbContext.Categories on p.CategoryId equals c.Id
                    join variant in _pimDbContext.Variants on p.Id equals variant.ProductId
                    join price in _pimDbContext.Prices on variant.PriceId equals price.Id
                    into priceResult
                    from pr in priceResult.DefaultIfEmpty()
                    select new StockTransactionModel
                    {
                        ProductId = p.Id,
                        ProductName = p.Name,
                        CategoryId = p.CategoryId ?? Guid.Empty,
                        VariantId = variant.Id,
                        SKUCode = variant.SKUCode,
                        PriceId = variant.PriceId,
                        VariantName = variant.Name,
                        CategoryName = c.Name,
                        PreOrderPrice = pr != null ? pr.PreOrderPrice : 0
                    }).ToList();

            variantIds = data.Select(x => x.VariantId);

            var queryStockTransactions = (from st in _transactionDbContext.StockTransactions
                                          where st.LocationId == locationId
                                          && variantIds.Contains(st.VariantId)
                                          select st).ToListAsync();

            var queryGIWDocumentItems = (from giw in _transactionDbContext.GIWDocuments
                                         join giwItem in _transactionDbContext.GIWDocumentItems
                                         on giw.Id equals giwItem.GIWDocumentId
                                         where giw.LocationId == locationId && giwItem.RemainingQty > 0 && variantIds.Contains(giwItem.VariantId)
                                         select giwItem).ToListAsync();

            bool isStore = location.Type == LocationType.Store;
            bool isWareHouse = location.Type == LocationType.Warehouse;

            var barCodes = await _pimDbContext.BarCode.AsNoTracking().Where(b => variantIds.Contains(b.VariantId)).ToListAsync();

            var stockTransactions = await queryStockTransactions;
            var giwDocumentItems = await queryGIWDocumentItems;

            var groupGIWDocumentItems = from giwItem in giwDocumentItems
                                        group giwItem by new { giwItem.VariantId, giwItem.StockTypeId } into groupGIWItem
                                        select new
                                        {
                                            groupGIWItem.Key.VariantId,
                                            groupGIWItem.Key.StockTypeId,
                                            Quantity = groupGIWItem.Sum(x => x.RemainingQty)
                                        };

            var groupGIWDocumentItemsbyVariant = (from giwItem in groupGIWDocumentItems
                                                  group giwItem by new { giwItem.VariantId } into groupGIWItem
                                                  select new
                                                  {
                                                      groupGIWItem.Key.VariantId,
                                                      Quantity_CON = groupGIWItem.FirstOrDefault(x => x.StockTypeId == stockType_CON.Id)?.Quantity ?? 0,
                                                      Quantity_FIRM = groupGIWItem.FirstOrDefault(x => x.StockTypeId == stockType_FIRM.Id)?.Quantity ?? 0,
                                                      Quantity_SOE = groupGIWItem.FirstOrDefault(x => x.StockTypeId == stockType_SOE.Id)?.Quantity ?? 0,
                                                      Quantity_SOR = groupGIWItem.FirstOrDefault(x => x.StockTypeId == stockType_SOR.Id)?.Quantity ?? 0
                                                  }).ToList();

            var groupStockTransactions = (from st in stockTransactions
                                          group st by st.VariantId into gst
                                          let lastStockTransaction = gst.OrderByDescending(x => x.CreatedDate).FirstOrDefault()
                                          select new
                                          {
                                              variantId = lastStockTransaction.VariantId,
                                              lastStockTransaction.Balance
                                          }).ToList();

            var stockTransactionsCSV = new StringBuilder();

            foreach (var item in data.OrderByDescending(x => x.ProductName))
            {
                var variantId = item.VariantId;

                var variantBarCodes = barCodes.Where(x => x.VariantId == variantId).Select(x => x.Code).ToList();
                var barCode = FormatVariantBarCodes(variantId, variantBarCodes);

                var GIWDocumentItem = groupGIWDocumentItemsbyVariant.FirstOrDefault(x => x.VariantId == variantId);
                var isVariantInstock = GIWDocumentItem != null;

                int stockOnHand = 0;
                int actualStockOnHand = 0;

                var stockTransactionBalance = groupStockTransactions.FirstOrDefault(a => a.variantId == variantId)?.Balance ?? 0;

                if (isStore)
                {
                    stockOnHand = stockTransactionBalance;
                    actualStockOnHand = stockTransactionBalance;
                }
                else if (isVariantInstock && isWareHouse)
                {
                    stockOnHand = GIWDocumentItem.Quantity_CON + GIWDocumentItem.Quantity_FIRM + GIWDocumentItem.Quantity_SOE + GIWDocumentItem.Quantity_SOR;
                    actualStockOnHand = stockTransactionBalance;
                }
                else
                {
                    stockOnHand = 0;
                    actualStockOnHand = stockTransactionBalance;
                }

                var datacsv = new object[]
                {
                            item.ProductId,
                            item.ProductName.Replace(',', ';'),
                            item.VariantId,
                            item.VariantName,
                            item.PreOrderPrice,
                            item.SKUCode,
                            barCode,
                            0,
                            "",
                            location.Id,
                            location.Name,
                            item.CategoryName,
                            isVariantInstock ? GIWDocumentItem.Quantity_SOE : 0,
                            isVariantInstock ? GIWDocumentItem.Quantity_SOR : 0,
                            isVariantInstock ? GIWDocumentItem.Quantity_FIRM : 0,
                            isVariantInstock ? GIWDocumentItem.Quantity_CON: 0,
                            stockOnHand,
                            actualStockOnHand,
                            0,
                            0,
                            0,
                            0,
                            0

                };
                stockTransactionsCSV.AppendLine(string.Join(",", datacsv));

            };

            byte[] buffer = Encoding.UTF8.GetPreamble().Concat(Encoding.UTF8.GetBytes($"{string.Join(",", columnHeaders)}\r\n{stockTransactionsCSV.ToString()}")).ToArray();
            return buffer;
        }

        private string FormatVariantBarCodes(Guid variantId, List<string> barCodes)
        {
            var result = "";
            if (barCodes != null && barCodes.Any())
            {
                foreach (var barCode in barCodes)
                {
                    result += $"{barCode}|";
                }
            }
            return result;
        }

        private class StockTransactionModel
        {
            public Guid ProductId { get; set; }
            public Guid CategoryId { get; set; }
            public string CategoryName { get; set; }
            public string ProductName { get; set; }
            public Guid VariantId { get; set; }
            public string SKUCode { get; set; }
            public Guid PriceId { get; set; }
            public float PreOrderPrice { get; set; }
            public string VariantName { get; set; }
        }
    }
}
