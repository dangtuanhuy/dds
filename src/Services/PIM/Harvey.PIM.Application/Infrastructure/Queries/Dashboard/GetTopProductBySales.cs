﻿using Harvey.Domain;
using Harvey.PIM.Application.Extensions;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Dashboard
{
    public sealed class GetTopProductBySalesQuery : IQuery<List<TopProductSaleModel>>
    {
        public TopProductSaleRequestModel TopProductSaleRequest { get; set; }
        public GetTopProductBySalesQuery(TopProductSaleRequestModel topProductSaleRequest)
        {
            TopProductSaleRequest = topProductSaleRequest;
        }
    }

    public sealed class GetTopProductBySalesQueryHandler : IQueryHandler<GetTopProductBySalesQuery, List<TopProductSaleModel>>
    {
        private readonly PimDbContext _pimDbContext;
        private readonly IConfiguration _configuration;
        public GetTopProductBySalesQueryHandler(
            IConfiguration configuration,
            PimDbContext pimDbContext)
        {
            _pimDbContext = pimDbContext;
            _configuration = configuration;
        }
        public async Task<List<TopProductSaleModel>> Handle(GetTopProductBySalesQuery query)
        {
            var _client = new HttpClient();
            var result = new List<TopProductSaleModel>();


            var requestUrl = $"{_configuration["RetailApi"]}/api/top-product-by-sales?" +
                             $"secretKey={SecretKeyExtension.RetailSecretKey}" +
                             $"&fromDate={query.TopProductSaleRequest.FromDate}&toDate={query.TopProductSaleRequest.ToDate}";
            var response = await _client.GetAsync(requestUrl);
            var contentResponse = await response.Content.ReadAsStringAsync();
            var saleItems = JsonConvert.DeserializeObject<IEnumerable<TopProductSaleModel>>(contentResponse);

            foreach(var saleItem in saleItems)
            {
                var variant = await _pimDbContext.Variants.FirstOrDefaultAsync(x => x.Id == saleItem.VariantId);
                if( variant != null)
                {
                    var product = await _pimDbContext.Products.FirstOrDefaultAsync(x => x.Id == variant.ProductId);
                    result.Add(new TopProductSaleModel
                    {
                        SKUCode = variant.SKUCode,
                        VariantName = variant.Name,
                        TotalPrice = saleItem.TotalPrice,
                        TotalQuantity = saleItem.TotalQuantity,
                        ProductName = product != null ? product.Name : ""
                    });
                }
            }

            return result;
        }
    }
}
