﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Dashboard
{
    public sealed class GetStocksByLocationQuery : IQuery<DashboardModel>
    {
        public Guid LocationId { get; set; }
        public string QueryText { get; set; }
        public GetStocksByLocationQuery(Guid locationId, string queryText)
        {
            LocationId = locationId;
            QueryText = queryText;
        }
    }

    public sealed class GetStocksByLocationQueryHandler : IQueryHandler<GetStocksByLocationQuery, DashboardModel>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly ISearchService _searchService;
        private readonly IEventBus _eventBus;
        public GetStocksByLocationQueryHandler(TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext,
            ISearchService searchService,
            IEventBus eventBus)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _searchService = searchService;
            _eventBus = eventBus;
        }
        public async Task<DashboardModel> Handle(GetStocksByLocationQuery query)
        {
            var result = new DashboardModel();
            result.LocationId = query.LocationId;
            var locations = await _pimDbContext.Locations.AsNoTracking().ToListAsync();
            result.LocationName = locations.FirstOrDefault(x => x.Id == result.LocationId)?.Name;

            //TODO need to fix for paging large 10000 items
            //https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-from-size.html
            //https://www.elastic.co/guide/en/elasticsearch/client/net-api/1.x/scroll.html
            var stockLevelSearchQuery = new StockLevelSearchQuery()
            {
                NumberItemsPerPage = 10000,
                Page = 1,
                QueryText = query.LocationId != null && query.LocationId != Guid.Empty ? query.LocationId.ToString() : null
            };

            var stockLevelResult = await _searchService.SearchAsync<StockLevelSearchItem, StockLevelSearchResult>(stockLevelSearchQuery);

            var productIds = stockLevelResult.Results.Select(x => x.Item.ProductId).Distinct();
            var variantIds = stockLevelResult.Results.Select(x => x.Item.VariantId).Distinct();

            var productNames = await _pimDbContext.Products.AsNoTracking()
                .Where(x => productIds.Contains(x.Id))
                .Select(x => new
                {
                    Name = x.Name,
                    Id = x.Id
                })
                .ToListAsync();

            var SKUCodes = await _pimDbContext.Variants.AsNoTracking()
               .Where(x => variantIds.Contains(x.Id))
               .Select(x => new
               {
                   SKU = x.SKUCode,
                   Id = x.Id
               })
               .ToListAsync();

            var fields = await _pimDbContext.FieldValues
                .AsNoTracking()
                .Where(x => variantIds.Contains(x.EntityId))
                .Include(x => x.Field)
                .Select(x => new
                {
                    VariantId = x.EntityId,
                    Name = x.Field.Name,
                    Value = FieldValueFactory.GetFieldValueFromFieldType(x.Field.Type, x)
                })
                .ToListAsync();

            var stockLevels = stockLevelResult.Results.Select(x => new StockLevelModel()
            {
                ReferenceId = x.Item.ReferenceId.ToString(),
                LocationId = x.Item.LocationId,
                ProductId = x.Item.ProductId,
                VariantId = x.Item.VariantId,
                Balance = x.Item.Balance,
                Fields = fields.Where(f=>f.VariantId == x.Item.VariantId).Select(f=> new VariantField{
                    Name = f.Name,
                    Value = f.Value
                }).ToList(),
                BarCodes = null,
                ProductName = productNames.FirstOrDefault(pro => pro.Id == x.Item.ProductId)?.Name,
                SKUCode = SKUCodes.FirstOrDefault(variant => variant.Id == x.Item.VariantId)?.SKU
            });

            if (stockLevels != null)
            {
                if (string.IsNullOrEmpty(query.QueryText) == false)
                {
                    stockLevels = stockLevels.Where(x => x.ProductName.Replace(" ", "").ToLower().Contains(query.QueryText.Replace(" ", "").ToLower())
                                    || x.SKUCode.Replace(" ", "").ToLower().Contains(query.QueryText.Replace(" ", "").ToLower())).ToList();

                }
                result.Items = stockLevels.GroupBy(item => new { item.ProductId }, (key, group) => new DashboardItemModel()
                {
                    ProductId = key.ProductId,
                    ProductName = group.FirstOrDefault(x => x.ProductId == key.ProductId)?.ProductName,
                    Items = group.Select(x => new DashboardItemDetailModel()
                    {
                        LocationId = x.LocationId,
                        LocationName = locations.FirstOrDefault(y => y.Id == x.LocationId)?.Name,
                        VariantId = x.VariantId,
                        Balance = x.Balance,
                        Fields = x.Fields,
                        BarCodes = null,
                        SKUCode = x.SKUCode
                    }).OrderBy(x => x.LocationId).ThenByDescending(x => x.SKUCode).ToList()
                }).ToList();
            }

            foreach (var r in result.Items.ToList())
            {
                if (r.Items.Count <= 0)
                {
                    result.Items.Remove(r);
                }
            }

            return result;
        }
    }
}
