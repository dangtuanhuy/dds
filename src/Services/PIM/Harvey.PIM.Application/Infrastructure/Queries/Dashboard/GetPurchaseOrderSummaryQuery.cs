﻿

using Harvey.Domain;
using Harvey.PIM.Application.Extensions;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Dashboard
{
    public class GetPurchaseOrderSummaryQuery : IQuery<List<PurchaseOrderSummaryModel>>
    {
        public GetPurchaseOrderSummaryQuery() { }
    }

    public class GetPurchaseOrderSummaryQueryHandler : IQueryHandler<GetPurchaseOrderSummaryQuery, List<PurchaseOrderSummaryModel>>
    {
        private readonly IConfiguration _configuration;
        public GetPurchaseOrderSummaryQueryHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<List<PurchaseOrderSummaryModel>> Handle(GetPurchaseOrderSummaryQuery query)
        {
            var result = new List<PurchaseOrderSummaryModel>();
            var _client = new HttpClient();
            var response = await _client.GetAsync($"{_configuration["PurchaseOrderApi"]}/api/purchase-order-exposure/count?secretKey={SecretKeyExtension.PurchaseOrderSecretKey}");
            if (response.IsSuccessStatusCode)
            {
                var contentResponse = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<List<PurchaseOrderSummaryModel>>(contentResponse);
                if (data != null && data.Any())
                {
                    result.AddRange(data);
                }
            }
            return result;
        }
    }
}
