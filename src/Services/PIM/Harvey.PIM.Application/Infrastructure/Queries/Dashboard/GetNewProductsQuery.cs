﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Dashboard
{
    public sealed class GetNewProductsQuery : IQuery<PagedResult<NewProductModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public GetNewProductsQuery(PagingFilterCriteria pagingFilterCriteria)
        {
            PagingFilterCriteria = pagingFilterCriteria;
        }
    }

    public sealed class GetNewProductsQueryHandler : IQueryHandler<GetNewProductsQuery, PagedResult<NewProductModel>>
    {
        private readonly IEfRepository<PimDbContext, Product> _productRepository;
        private readonly PimDbContext _pimDbContext;
        public GetNewProductsQueryHandler(IEfRepository<PimDbContext, Product> productRepository,
            PimDbContext pimDbContext)
        {
            _productRepository = productRepository;
            _pimDbContext = pimDbContext;
        }
        public async Task<PagedResult<NewProductModel>> Handle(GetNewProductsQuery query)
        {
            var today = DateTime.UtcNow;
            var currentMonth = today.Month;
            var previousMonth = today.AddMonths(-1).Month;
            var currentYear = today.Year;

            var totalNumberProducts = await _productRepository.Count(x => x.CreatedDate.Date.Month >= previousMonth && x.CreatedDate.Date.Month <= currentMonth && x.CreatedDate.Year == currentYear);

            var data = _pimDbContext.Products.Join(_pimDbContext.Categories, product => product.CategoryId, category => category.Id, (product, category) => new { p = product, c = category })
                        .Select(x => new NewProductModel()
                        {
                            ProductId = x.p.Id,
                            ProductName = x.p.Name,
                            ProductDescription = x.p.Description,
                            CategoryName = x.c.Name,
                            CreatedDate = x.p.CreatedDate
                        }).OrderByDescending(x => x.CreatedDate)
                        .Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
                        .Take(query.PagingFilterCriteria.NumberItemsPerPage);


            return new PagedResult<NewProductModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                Data = data,
                TotalItems = totalNumberProducts
            };
        }
    }
}
