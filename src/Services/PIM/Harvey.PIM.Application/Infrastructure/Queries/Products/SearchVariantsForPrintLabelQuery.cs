﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Products
{
    public class SearchVariantsForPrintLabelQuery : IQuery<ProductForPrintLabelResponse>
    {
        public string QueryText { get; }
        public int PageIndex { get; }
        public int PageSize { get; }
        public SearchVariantsForPrintLabelQuery(string queryText, int pageIndex, int pageSize)
        {
            QueryText = queryText;
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
    }

    public sealed class SearchVariantsForPrintLabelQueryHandler : IQueryHandler<SearchVariantsForPrintLabelQuery, ProductForPrintLabelResponse>
    {
        private readonly PimDbContext _pimDbContext;
        private readonly IQueryExecutor _queryExecutor;

        public SearchVariantsForPrintLabelQueryHandler(
           PimDbContext pimDbContext,
           IQueryExecutor queryExecutor)
        {
            _pimDbContext = pimDbContext;
            _queryExecutor = queryExecutor;
        }

        public async Task<ProductForPrintLabelResponse> Handle(SearchVariantsForPrintLabelQuery query)
        {
            ProductForPrintLabelResponse response = new ProductForPrintLabelResponse
            {
                BarCodeDataOnly = false,
                PageIndex = query.PageIndex,
                PageSize = query.PageSize,
                TotalItems = 0
            };
            var searchTextLowerCase = query.QueryText.ToLower();
            var productsFromBarCode = await SearchVariantByBarCode(query.QueryText);
            response.Products = productsFromBarCode;
            if (productsFromBarCode.Count == 1)
            {
                response.BarCodeDataOnly = true;
                response.TotalItems = 1;
                return response;
            }

            var queriedVariantIds = productsFromBarCode.Select(x => x.VariantId).ToList();
            var productsFromSearchBySKUCode = await SearchVariantBySkuCode(query.QueryText, queriedVariantIds);
            response.Products.AddRange(productsFromSearchBySKUCode);

            var totalItemsFromSkuCode = productsFromSearchBySKUCode.Count;
            var totalItemsFromBarCode = productsFromBarCode.Count;

            queriedVariantIds = response.Products.Select(x => x.VariantId).ToList();
            var dataFromSearchByProductName = await SearchVariantByProductName(query.QueryText, queriedVariantIds,
                                                                                    totalItemsFromBarCode + totalItemsFromSkuCode,
                                                                                    query.PageIndex, query.PageSize);
            var totalItemsFromProductName = dataFromSearchByProductName.Item1;
            var productsFromSearchByProductName = dataFromSearchByProductName.Item2;
            response.Products.AddRange(productsFromSearchByProductName);

            response.TotalItems = totalItemsFromBarCode + totalItemsFromSkuCode + totalItemsFromProductName;

            return response;
        }

        private async Task<List<ProductForPrintLabelModel>> SearchVariantByBarCode(string searchText)
        {
            var barCodes = await _pimDbContext.BarCode.Where(x => x.Code == searchText)
                .GroupBy(x => new
                {
                    x.VariantId
                })
                .Select(x => x.OrderByDescending(y => y.CreatedDate).FirstOrDefault())
                .Select(x => new
                {
                    x.VariantId,
                    x.CreatedDate,
                    x.Code
                }).ToListAsync();

            var variantIds = barCodes.Select(x => x.VariantId).ToList();
            var correspondingVariants = await _pimDbContext.Variants
                    .Where(x => x.SKUCode != null && variantIds.Contains(x.Id)).ToListAsync();

            var priceIds = correspondingVariants.Select(x => x.PriceId).ToList();
            var correspondingPrices = await _pimDbContext.Prices
                    .Where(x => priceIds.Contains(x.Id)).ToListAsync();

            List<ProductForPrintLabelModel> products = new List<ProductForPrintLabelModel>();
            foreach (var barCodeEntity in barCodes)
            {
                var correspondingVariant = correspondingVariants.FirstOrDefault(x => x.Id == barCodeEntity.VariantId);
                if (correspondingVariant != null)
                {
                    var correspondingPrice = correspondingPrices.FirstOrDefault(x => x.Id == correspondingVariant.PriceId);

                    var correspondingProduct = await _pimDbContext.Products
                        .FirstOrDefaultAsync(x => !x.IsDelete && x.Id == correspondingVariant.ProductId);

                    if (correspondingProduct != null)
                    {
                        var fieldValues = await _pimDbContext.FieldValues
                            .Where(x => x.EntityId == correspondingVariant.Id)
                            .OrderBy(x => x.FieldId).Select(x => x.PredefinedListValue).ToListAsync();
                        var variantName = correspondingProduct.Name;
                        if (fieldValues != null && fieldValues.Any())
                        {
                            variantName += "-" + string.Join("-", fieldValues);
                        }

                        products.Add(new ProductForPrintLabelModel
                        {
                            BarCode = barCodeEntity.Code,
                            ProductName = correspondingProduct.Name,
                            VariantName = variantName,
                            ListPrice = correspondingPrice?.ListPrice ?? 0,
                            StaffPrice = correspondingPrice?.StaffPrice ?? 0,
                            MemberPrice = correspondingPrice?.MemberPrice ?? 0,
                            SKUCode = correspondingVariant.SKUCode,
                            Quantity = 1,
                            VariantId = correspondingVariant.Id,
                            CreatedDateString = correspondingVariant.CreatedDate.ToString("yyyy-MM-dd h:mm tt")
                        });
                    }
                }

            }

            return products;
        }

        private async Task<List<ProductForPrintLabelModel>> SearchVariantBySkuCode(string searchText, List<Guid> queriedVariantIds)
        {
            var variants = await _pimDbContext.Variants
                .Where(x => !queriedVariantIds.Contains(x.Id) && x.SKUCode == searchText)
                .Select(x => new
                {
                    VariantId = x.Id,
                    x.ProductId,
                    x.PriceId,
                    x.SKUCode,
                    x.CreatedDate
                })
                .ToListAsync();

            var productIds = variants.Select(x => x.ProductId).ToList();
            var correspondingProducts = await _pimDbContext.Products
                    .Where(x => !x.IsDelete && productIds.Contains(x.Id)).ToListAsync();

            var priceIds = variants.Select(x => x.PriceId).ToList();
            var correspondingPrices = await _pimDbContext.Prices
                    .Where(x => priceIds.Contains(x.Id)).ToListAsync();

            List<ProductForPrintLabelModel> products = new List<ProductForPrintLabelModel>();
            foreach (var variant in variants)
            {
                var correspondingPrice = correspondingPrices.FirstOrDefault(x => x.Id == variant.PriceId);
                var correspondingProduct = correspondingProducts.FirstOrDefault(x => x.Id == variant.ProductId);

                var barCode = await _pimDbContext.BarCode.Where(x => x.Code != null && x.VariantId == variant.VariantId)
                    .OrderByDescending(x => x.CreatedDate).Select(x => x.Code).FirstOrDefaultAsync();

                if (correspondingProduct != null && !string.IsNullOrEmpty(barCode))
                {
                    var fieldValues = await _pimDbContext.FieldValues
                            .Where(x => x.EntityId == variant.VariantId)
                            .OrderBy(x => x.FieldId).Select(x => x.PredefinedListValue).ToListAsync();
                    var variantName = correspondingProduct.Name;
                    if (fieldValues != null && fieldValues.Any())
                    {
                        variantName += "-" + string.Join("-", fieldValues);
                    }

                    products.Add(new ProductForPrintLabelModel
                    {
                        BarCode = barCode,
                        ProductName = correspondingProduct.Name,
                        VariantName = variantName,
                        ListPrice = correspondingPrice?.ListPrice ?? 0,
                        StaffPrice = correspondingPrice?.StaffPrice ?? 0,
                        MemberPrice = correspondingPrice?.MemberPrice ?? 0,
                        SKUCode = variant.SKUCode,
                        Quantity = 1,
                        VariantId = variant.VariantId,
                        CreatedDateString = variant.CreatedDate.ToString("yyyy-MM-dd h:mm tt")
                    });
                }
            }

            return products;
        }

        private async Task<Tuple<int, List<ProductForPrintLabelModel>>> SearchVariantByProductName(string searchText, List<Guid> queriedVariantIds,
                                                                                                int currentTotalItems,
                                                                                                int pageIndex, int pageSize)
        {
            var searchTextLower = searchText.ToLower();

            var productsQuery = _pimDbContext.Products.Where(x => !x.IsDelete && x.Name.ToLower().Contains(searchTextLower));
            var products = await productsQuery.ToListAsync();
            
            var productIds = products.Select(x => x.Id).ToList();
            var variantsQuery = _pimDbContext.Variants
                                                .Where(x => x.SKUCode != null && productIds.Contains(x.ProductId))
                                                .Include(x => x.BarCodes)
                                                .Where(x => x.BarCodes.Count > 0)
                                                .Select(x => new
                                                {
                                                    VariantId = x.Id,
                                                    BarCode = x.BarCodes.OrderByDescending(y => y.CreatedDate).FirstOrDefault().Code,
                                                    ProductId = x.ProductId,
                                                    PriceId = x.PriceId,
                                                    SKUCode = x.SKUCode,
                                                    x.CreatedDate
                                                });
            var totalItems = await variantsQuery.CountAsync();

            var skipItemsAmount = pageIndex * pageSize - currentTotalItems;
            skipItemsAmount = skipItemsAmount > 0 ? skipItemsAmount : 0;
            var remainningItemsAmount = (pageIndex + 1) * pageSize - currentTotalItems;
            remainningItemsAmount = remainningItemsAmount > pageSize ? 10 : remainningItemsAmount;

            var variants = await variantsQuery
                                    .Skip(skipItemsAmount)
                                    .Take(remainningItemsAmount)
                                    .ToListAsync();

            List<ProductForPrintLabelModel> result = new List<ProductForPrintLabelModel>();
            foreach (var variant in variants)
            {
                if (!string.IsNullOrEmpty(variant.BarCode))
                {
                    var product = products.FirstOrDefault(x => x.Id == variant.ProductId);
                    var price = await _pimDbContext.Prices.FirstOrDefaultAsync(x => x.Id == variant.PriceId);
                    var fieldValues = await _pimDbContext.FieldValues
                            .Where(x => x.EntityId == variant.VariantId)
                            .OrderBy(x => x.FieldId).Select(x => x.PredefinedListValue).ToListAsync();
                    var variantName = product.Name;
                    if (fieldValues != null && fieldValues.Any())
                    {
                        variantName += "-" + string.Join("-", fieldValues);
                    }

                    result.Add(new ProductForPrintLabelModel
                    {
                        BarCode = variant.BarCode,
                        ProductName = product.Name,
                        VariantName = variantName,
                        VariantId = variant.VariantId,
                        ListPrice = price?.ListPrice ?? 0,
                        StaffPrice = price?.StaffPrice ?? 0,
                        MemberPrice = price?.MemberPrice ?? 0,
                        Quantity = 1,
                        SKUCode = variant.SKUCode,
                        CreatedDateString = variant.CreatedDate.ToString("yyyy-MM-dd h:mm tt")
                    });
                }
            }

            return Tuple.Create(totalItems, result);
        }
    }
}
