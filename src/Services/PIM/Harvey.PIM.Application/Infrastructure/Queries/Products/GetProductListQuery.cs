﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Harvey.PIM.Application.Infrastructure.Queries.Products
{
    public sealed class GetProductListQuery : IQuery<PagedResult<ProductListModel>>
    {
        public string QueryText { get; }
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public GetProductListQuery(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }

    public sealed class GetProductListQueryHandler : IQueryHandler<GetProductListQuery, PagedResult<ProductListModel>>
    {
        private readonly ISearchService _searchService;
        private readonly IMapper _mapper;
        private readonly PimDbContext _pimDbContext;
        public GetProductListQueryHandler(ISearchService searchService, IMapper mapper, PimDbContext pimDbContext)
        {
            _searchService = searchService;
            _mapper = mapper;
            _pimDbContext = pimDbContext;
        }
        public async Task<PagedResult<ProductListModel>> Handle(GetProductListQuery query)
        {
            var queryString = query.QueryText;
            var variantSKU = await _pimDbContext.Variants.FirstOrDefaultAsync(x => x.SKUCode == queryString);

            if (variantSKU != null && variantSKU.SKUCode != null)
            {
                var product = await _pimDbContext.Products.FirstOrDefaultAsync(x => x.Id == variantSKU.ProductId);
                if (product != null)
                {
                    return await MapProductToProductList(product, query);
                }
            }

            var barCode = await _pimDbContext.BarCode.Where(x => x.Code == queryString).OrderByDescending(x => x.CreatedDate).FirstOrDefaultAsync();

            if (barCode != null)
            {
                var variantBarcode = await _pimDbContext.Variants.FirstOrDefaultAsync(x => x.Id == barCode.VariantId);

                if (variantBarcode != null)
                {
                    var product = await _pimDbContext.Products.FirstOrDefaultAsync(x => x.Id == variantBarcode.ProductId);
                    if (product != null)
                    {
                        return await MapProductToProductList(product, query);
                    }
                }
            }

            var productSearchQuery = new ProductSearchQuery()
            {
                QueryText = queryString,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                Page = query.PagingFilterCriteria.Page
            };

            var allNumberItems = query.PagingFilterCriteria.NumberItemsPerPage * (query.PagingFilterCriteria.Page - 1);
            const int maxNumberItemsElasticsearchCanGet = 10000;

            var result = allNumberItems <= maxNumberItemsElasticsearchCanGet ? await _searchService.SearchAsync<ProductSearchItem, ProductSearchResult>(productSearchQuery, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection.ToString())
                                                                             : await _searchService.SearchOverLimitAsync<ProductSearchItem, ProductSearchResult>(productSearchQuery, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection.ToString());
            var data = result.Results.Select(x => new ProductListModel
            {
                Description = x.Item.Description,
                Name = x.Item.Name,
                Id = x.Item.Id,
                CategoryId = x.Item.CategoryId,
                CategoryName = x.Item.CategoryName,
                Code = x.Item.Code,
                IsDelete = x.Item.IsDelete,
                CreatedDate = x.Item.CreatedDate,
                CreatedBy = x.Item.CreatedBy,
                UpdatedDate = x.Item.UpdatedDate,
                UpdatedBy = x.Item.UpdatedBy
            });
            return new PagedResult<ProductListModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = (int)result.TotalItems,
                Data = data
            };
        }

        public async Task<PagedResult<ProductListModel>> MapProductToProductList(Product product, GetProductListQuery query)
        {

            var listProduct = new List<ProductListModel>();
            var category = await _pimDbContext.Categories.FirstOrDefaultAsync(x => x.Id == product.CategoryId);
            listProduct.Add(new ProductListModel
            {
                Description = product.Description,
                Name = product.Name,
                Id = product.Id.ToString(),
                CategoryId = product.CategoryId ?? Guid.Empty,
                CategoryName = category != null ? category.Name : "",
                Code = product.Code,
                IsDelete = product.IsDelete,
                CreatedDate = product.CreatedDate,
                CreatedBy = product.CreatedBy,
                UpdatedDate = product.UpdatedDate,
                UpdatedBy = product.UpdatedBy
            });
            return new PagedResult<ProductListModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = 1,
                Data = listProduct
            };
        }
    }
}
