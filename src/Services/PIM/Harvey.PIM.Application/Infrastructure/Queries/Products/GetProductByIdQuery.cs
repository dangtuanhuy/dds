﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.FieldFramework.Services.Interface;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Harvey.PIM.Application.Infrastructure.Queries.Products
{
    public sealed class GetProductByIdQuery : IQuery<ProductModel>
    {
        public readonly Guid Id;
        public GetProductByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetProductByIdQueryHandler : IQueryHandler<GetProductByIdQuery, ProductModel>
    {
        private readonly PimDbContext _pimDbContext;
        private readonly IEventStoreRepository<Product> _repository;
        private readonly IEntityRefService _entityRefService;
        public GetProductByIdQueryHandler(
            IEventStoreRepository<Product> repository,
            IEntityRefService entityRefService,
            PimDbContext pimDbContext)
        {
            _repository = repository;
            _pimDbContext = pimDbContext;
            _entityRefService = entityRefService;
        }

        public async Task<ProductModel> Handle(GetProductByIdQuery query)
        {
            var product = await _repository.GetByIdAsync(query.Id);
            if (product.IsDelete)
            {
                return null;
            }
            var fieldTemplate = _pimDbContext.FieldTemplates.FirstOrDefault(x => x.Id == product.FieldTemplateId);

            var result = new ProductModel
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                FieldTemplateId = product.FieldTemplateId,
                FieldTemplateName = fieldTemplate?.Name,
                CategoryId = product.CategoryId,
                Code = product.Code,
                CreatedDate = product.CreatedDate,
                CreatedBy = product.CreatedBy,
                UpdatedDate = product.UpdatedDate,
                UpdatedBy = product.UpdatedBy
            };


            var fields = await _pimDbContext
               .Field_FieldTemplates
               .Include(x => x.Field).ToListAsync();

            var fieldsVariant = fields
                .Where(x => x.FieldTemplateId == product.FieldTemplateId && x.IsVariantField)
               .ToList();
            
            var fieldsProduct = fields
                .Where(x => x.FieldTemplateId == product.FieldTemplateId && !x.IsVariantField)
               .GroupBy(x => x.Section).Select(x => new
               {
                   Fields = x,
                   Section = x.Key,
                   Name = x.FirstOrDefault().Section,
                   x.FirstOrDefault().OrderSection,
                   x.FirstOrDefault().IsVariantField
               }).ToList();

            foreach (var item in fieldsProduct)
            {
                var section = new ProductModel.Section
                {
                    Name = item.Name,
                    OrderSection = item.OrderSection,
                    IsVariantField = item.IsVariantField
                };
                var sectionOfProduct = product.Sections.Keys.FirstOrDefault(x => x.Name == item.Name);                               
                foreach (var field_fieldTemplate in item.Fields)
                {
                    if (sectionOfProduct != null)
                    {
                        var field = product.Sections[sectionOfProduct].FirstOrDefault(x => x.FieldId == field_fieldTemplate.FieldId);
                        if (field != null)
                        {
                            field.Field.DefaultValue = field_fieldTemplate.Field?.DefaultValue;
                            section.FieldValues.Add(FieldValueFactory.GetFromFieldValue(field, _entityRefService));
                        }
                        else
                        {
                            section.FieldValues.Add(FieldValueFactory.GetFromFieldValue(FieldValue.Default(field_fieldTemplate.Field), _entityRefService));
                        }
                        continue;
                    }
                    section.FieldValues.Add(FieldValueFactory.GetFromFieldValue(FieldValue.Default(field_fieldTemplate.Field), _entityRefService));
                }
                result.Sections.Add(section);
                
            }

            foreach (var item in product.VariantFieldValues)
            {
                var variant = new ProductModel.VariantModel()
                {
                    Id = item.Key,

                };

                if (product.VariantPrices.ContainsKey(item.Key))
                {
                    variant.Price = product.VariantPrices.FirstOrDefault(x => x.Key == item.Key).Value;
                }

                foreach (var field in item.Value)
                {
                    var dbField = fieldsVariant.FirstOrDefault(x => x.FieldId == field.Field.Id);
                    if (dbField != null)
                    {
                        field.Field.DefaultValue = dbField.Field?.DefaultValue;
                        variant.FieldValues.Add(FieldValueFactory.GetFromFieldValue(field, _entityRefService));
                    }
                }

                variant.Code = _pimDbContext.Variants.FirstOrDefault(x => x.Id == item.Key)?.Code;

                result.Variants.Add(variant);
            }

            return result;
        }
    }
}
