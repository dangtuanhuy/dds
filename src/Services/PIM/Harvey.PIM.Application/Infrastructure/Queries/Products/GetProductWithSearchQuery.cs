﻿using AutoMapper;
using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Harvey.PIM.Application.Infrastructure.Models.ProductModel;

namespace Harvey.PIM.Application.Infrastructure.Queries.Products
{
    public class GetProductWithSearchQuery : IQuery<List<ProductListModel>>
    {
        public string QueryText { get; }
        public GetProductWithSearchQuery(string queryText)
        {
            QueryText = queryText;
        }
    }

    public sealed class GetProductWithSearchQueryHandler : IQueryHandler<GetProductWithSearchQuery, List<ProductListModel>>
    {
        private readonly ISearchService _searchService;
        private readonly IMapper _mapper;
        private readonly PimDbContext _pimDbContext;
        public GetProductWithSearchQueryHandler(ISearchService searchService,
            IMapper mapper,
            PimDbContext pimDbContext)
        {
            _searchService = searchService;
            _mapper = mapper;
            _pimDbContext = pimDbContext;
        }
        public async Task<List<ProductListModel>> Handle(GetProductWithSearchQuery query)
        {
            if (string.IsNullOrEmpty(query.QueryText))
            {
                return new List<ProductListModel>();
            }
            var productSearchQuery = new ProductSearchQuery()
            {
                QueryText = query.QueryText,
                NumberItemsPerPage = 0,
                Page = 0
            };

            var result = await _searchService.SearchAsync<ProductSearchItem, ProductSearchResult>(productSearchQuery);

            var variant = _pimDbContext.Variants.FirstOrDefault(x => x.SKUCode == query.QueryText);

            if (variant == null)
            {
                var barcode = _pimDbContext.BarCode.FirstOrDefault(x => x.Code == query.QueryText);
                if (barcode != null)
                {
                    variant = _pimDbContext.Variants.FirstOrDefault(x => x.Id == barcode.VariantId);
                }      
            }
            var data = new List<ProductListModel>();
            var variantModel = new SearchVariantModel();
            if (variant != null)
            {
                variantModel.Id = variant.Id;
                variantModel.Code = query.QueryText;
                var fieldValues = _pimDbContext.FieldValues.Include(x => x.Field).Where(x => x.EntityId == variant.Id).ToList();
                foreach (var fieldValue in fieldValues)
                {
                    var fieldValueModel = new SearchFieldValueModel()
                    {
                        Name = fieldValue.Field.Name,
                        Value = fieldValue.PredefinedListValue.Split(',').ToList(),
                    };
                    variantModel.FieldValues.Add(fieldValueModel);
                }
                var product = _pimDbContext.Products.SingleOrDefault(x => x.Id == variant.ProductId);
                if (product != null)
                {
                    var productModel = new ProductListModel
                    {
                        Name = product.Name,
                        Id = variant.ProductId.ToString(),
                        Variant = variantModel
                    };
                    data.Add(productModel);
                }
            }
            else
            {
                data = result.Results.Select(x => new ProductListModel
                {
                    Name = x.Item.Name,
                    Id = x.Item.Id,
                    Variant = variantModel
                }).ToList();
            }

            return data;
        }
    }
}
