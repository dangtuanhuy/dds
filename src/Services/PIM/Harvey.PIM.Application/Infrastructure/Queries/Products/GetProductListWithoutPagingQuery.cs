﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Products
{
    public sealed class GetProductListWithoutPagingQuery : IQuery<IEnumerable<ProductListModel>>
    {
        public GetProductListWithoutPagingQuery()
        {
        }
    }

    public sealed class GetProductListWithoutPagingQueryHandler : IQueryHandler<GetProductListWithoutPagingQuery, IEnumerable<ProductListModel>>
    {
        private readonly PimDbContext _pimDbContext;

        public GetProductListWithoutPagingQueryHandler(PimDbContext pimDbContext)
        {
            _pimDbContext = pimDbContext;
        }
        public async Task<IEnumerable<ProductListModel>> Handle(GetProductListWithoutPagingQuery query)
        {
            var result = await _pimDbContext.Products.Include(p => p.Category).Select(p => new ProductListModel
            {
                Id = p.Id.ToString(),
                CategoryId = p.CategoryId.Value,
                CategoryName = p.Category.Name,
                Name = p.Name,
                Description = p.Description,
                Code = p.Code,
                IsDelete = p.IsDelete,
                CreatedDate = p.CreatedDate,
                CreatedBy = p.CreatedBy,
                UpdatedDate = p.UpdatedDate,
                UpdatedBy = p.UpdatedBy
            }).ToListAsync();
            return result;
        }
    }
}
