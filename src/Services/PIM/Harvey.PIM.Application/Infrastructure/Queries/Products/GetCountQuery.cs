﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Products
{
    public sealed class GetCountQuery : IQuery<CountModel>
    {
        public GetCountQuery()
        {

        }
    }

    public sealed class GetCountQueryHandler : IQueryHandler<GetCountQuery, CountModel>
    {
        private readonly PimDbContext _pimDbContext;
        public GetCountQueryHandler(PimDbContext pimDbContext)
        {
            _pimDbContext = pimDbContext;
        }
        public async Task<CountModel> Handle(GetCountQuery query)
        {
            var productsCount = await _pimDbContext.Products.AsNoTracking().CountAsync();
            var variantsCount = await _pimDbContext.Variants.AsNoTracking().CountAsync();

            return new CountModel()
            {
                Products = productsCount,
                Variants = variantsCount,
            };
        }
    }
}
