﻿using Harvey.Domain;
using Harvey.PIM.Application.Channels.Products;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Harvey.PIM.Application.Infrastructure.Queries.Products
{
    public class GetProductFieldValue : IQuery<IEnumerable<ProductFeed>>
    {
        public DateTime LastSync { get; set; }
        public GetProductFieldValue(DateTime lastSync)
        {
            LastSync = lastSync;
        }
    }
    public sealed class GetProductFieldValueHandler : IQueryHandler<GetProductFieldValue, IEnumerable<ProductFeed>>
    {
        private readonly PimDbContext _pimDbContext;
        public GetProductFieldValueHandler(PimDbContext pimDbContext, TransactionDbContext transactionDbContext)
        {
            _pimDbContext = pimDbContext;
        }
        public Task<IEnumerable<ProductFeed>> Handle(GetProductFieldValue query)
        {
            var products = new List<Product>();
            var feedTime = query.LastSync != DateTime.MinValue ? query.LastSync.ToString() : null;
            if (feedTime != null)
            {
                products = _pimDbContext.Products.AsNoTracking().Where(x => !x.IsDelete && (x.UpdatedDate > query.LastSync))
                    .OrderBy(x => x.UpdatedDate)
                    .Take(3000)
                    .ToList();
            }
            else
            {
                products = _pimDbContext.Products
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .OrderBy(x => x.UpdatedDate)
                    .Take(3000)
                    .ToList();
            }
            var result = new ConcurrentBag<ProductFeed>();
            if (products.Count == 0)
            {
                return Task.FromResult(result.AsEnumerable());
            }

            var productIds = products.Select(x => x.Id);
            var productFieldTemplateIds = products.Select(x => x.FieldTemplateId).Distinct();

            var variants = _pimDbContext
                                  .Variants
                                  .AsNoTracking()
                                  .Where(x => productIds.Contains(x.ProductId))
                                  .Select(x => new
                                  {
                                      x.Id,
                                      x.ProductId
                                  })
                                  .ToList();
            var allVariantIds = variants.Select(a => a.Id);

            var allFieldValues = new List<FieldFramework.Entities.FieldValue>();
            allFieldValues = _pimDbContext.FieldValues.AsNoTracking().Where(a => allVariantIds.Contains(a.EntityId) || productIds.Contains(a.EntityId)).Include(x => x.Field).ToList();

            var fieldValuesProduct = allFieldValues
                                    .Where(x => productIds.Contains(x.EntityId))
                                    .Select(x => new
                                    {
                                        entityId = x.EntityId,
                                        fieldValueItem = x
                                    }).ToList();

            var fieldValuesVariant = allFieldValues
                                    .Where(x => allVariantIds.Contains(x.EntityId))
                                    .Select(x => new
                                    {
                                        entityId = variants.FirstOrDefault(y => y.Id == x.EntityId).ProductId,
                                        fieldValueItem = x
                                    }).ToList();
            var fieldValues = fieldValuesProduct.Concat(fieldValuesVariant);
            var fieldTempates = _pimDbContext
                                .Field_FieldTemplates
                                .AsNoTracking()
                                .Where(x => productFieldTemplateIds.Contains(x.FieldTemplateId))
                                .ToList();

            Parallel.ForEach(products, item =>
            {
                var feed = new ProductFeed()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    CategoryId = item.CategoryId,
                    UpdatedDate = item.UpdatedDate
                };

                var fieldValueItems = fieldValues.Where(x => x.entityId == item.Id);

                foreach (var fv in fieldValueItems)
                {
                    var ft = fieldTempates.FirstOrDefault(x => x.FieldTemplateId == item.FieldTemplateId && x.FieldId == fv.fieldValueItem.FieldId);
                    if (ft != null)
                    {
                        var fvf = new CatalogFieldValue()
                        {
                            EntityId = fv.fieldValueItem.EntityId,
                            FieldId = fv.fieldValueItem.FieldId,
                            FieldName = fv.fieldValueItem.Field?.Name,
                            FieldType = fv.fieldValueItem.Field != null ? fv.fieldValueItem.Field.Type : FieldType.Text,
                            FieldValueId = fv.fieldValueItem.Id,
                            IsVariantField = ft.IsVariantField,
                            Id = fv.fieldValueItem.Id,
                            Section = ft.Section,
                            OrderSection = ft.OrderSection
                        };
                        fvf.FieldValue = FieldValueFactory.GetFieldValueFromFieldType(fvf.FieldType, fv.fieldValueItem);
                        feed.FieldValues.Add(fvf);
                    }
                }

                result.Add(feed);

            });

            return Task.FromResult(result.OrderBy(x => x.UpdatedDate).AsEnumerable());
        }
    }
}
