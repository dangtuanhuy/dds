﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Products
{
    public class GetProductListPagingForRebuildIndexQuery : IQuery<IEnumerable<ProductListModel>>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public GetProductListPagingForRebuildIndexQuery(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
    }

    public sealed class GetProductListPagingForRebuildIndexQueryHandler : IQueryHandler<GetProductListPagingForRebuildIndexQuery, IEnumerable<ProductListModel>>
    {
        private readonly PimDbContext _pimDbContext;

        public GetProductListPagingForRebuildIndexQueryHandler(PimDbContext pimDbContext)
        {
            _pimDbContext = pimDbContext;
        }
        public async Task<IEnumerable<ProductListModel>> Handle(GetProductListPagingForRebuildIndexQuery query)
        {
            var result = await _pimDbContext.Products.Include(p => p.Category).Select(p => new ProductListModel
            {
                Id = p.Id.ToString(),
                CategoryId = p.CategoryId.Value,
                CategoryName = p.Category.Name,
                Name = p.Name,
                Description = p.Description,
                Code = p.Code,
                IsDelete = p.IsDelete,
                CreatedDate = p.CreatedDate,
                CreatedBy = p.CreatedBy,
                UpdatedDate = p.UpdatedDate,
                UpdatedBy = p.UpdatedBy
            })
            .Skip(query.PageIndex * query.PageSize)
            .Take(query.PageSize)
            .ToListAsync();
            return result;
        }
    }
}
