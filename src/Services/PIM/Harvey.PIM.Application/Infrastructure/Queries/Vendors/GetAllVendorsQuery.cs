﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Vendors
{
    public class GetAllVendorsQuery: IQuery<List<VendorModel>>
    {
    }

    public sealed class GetAllVendorsQueryHandler : IQueryHandler<GetAllVendorsQuery, List<VendorModel>>
    {
        private readonly IEfRepository<PimDbContext, Vendor> _repository;
        private readonly IMapper _mapper;
        public GetAllVendorsQueryHandler(IEfRepository<PimDbContext, Vendor> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<List<VendorModel>> Handle(GetAllVendorsQuery query)
        {
            var result = await _repository.ListAsync(x => x.Active == true);
            result = result.Where(x => x.VendorCode != "SIV").ToList();
            return _mapper.Map<List<VendorModel>>(result);
        }
    }
}
