﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Fields
{
    public sealed class GetFieldsQuery : IQuery<PagedResult<FieldModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public GetFieldsQuery(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }

    public sealed class GetFieldsQueryHandler : IQueryHandler<GetFieldsQuery, PagedResult<FieldModel>>
    {
        private readonly IEfRepository<PimDbContext, Field> _repository;
        private readonly IMapper _mapper;
        public GetFieldsQueryHandler(IEfRepository<PimDbContext, Field> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<PagedResult<FieldModel>> Handle(GetFieldsQuery query)
        {
            var result = string.IsNullOrEmpty(query.QueryText) ? await _repository.GetAsync(query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection)
                                                                : await _repository.ListAsync(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper()),
                                                                                        query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);

            var fields = _mapper.Map<List<FieldModel>>(result);
            
            var totalPages = string.IsNullOrEmpty(query.QueryText) ? await _repository.Count()
                                                                   : await _repository.Count(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper()));
            return new PagedResult<FieldModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = fields
            };
        }
    }
}
