﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.StockRequests
{
    public class GetStockRequestByIdQuery: IQuery<StockRequestModel>
    {
        public Guid Id { get; }
        public GetStockRequestByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetStockRequestByIdQueryHandler : IQueryHandler<GetStockRequestByIdQuery, StockRequestModel>
    {
        private readonly IEfRepository<TransactionDbContext, StockRequest> _repository;
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        public GetStockRequestByIdQueryHandler(IEfRepository<TransactionDbContext, StockRequest> repository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext)
        {
            _repository = repository;
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
        }

        public async Task<StockRequestModel> Handle(GetStockRequestByIdQuery query)
        {
            var stockRequest = await _repository.GetByIdAsync(query.Id);
            
            if (stockRequest == null)
            {
                throw new ArgumentException($"StockRequest {query.Id} is not presented.");
            }

            var location = await _pimDbContext.Locations.FirstOrDefaultAsync(x => x.Id == stockRequest.ToLocationId);

            var result = new StockRequestModel()
            {
                Id = stockRequest.Id,
                FromLocationId = stockRequest.FromLocationId,
                ToLocationId = stockRequest.ToLocationId,
                LocationName = location?.Name,
                Subject = stockRequest.Subject,
                Description = stockRequest.Description,
                StockRequestStatus = stockRequest.StockRequestStatus,
                CreatedBy = stockRequest.CreatedBy,
                CreatedDate = stockRequest.CreatedDate,
                UpdatedBy = stockRequest.UpdatedBy,
                UpdatedDate = stockRequest.UpdatedDate,
                StockRequestItems = new List<StockRequestItemModel>(),
                DateRequest = stockRequest.DateRequest
            };

            var _stockRequestItems = await _transactionDbContext.StockRequestItems.Where(x => x.StockRequestId == stockRequest.Id).ToListAsync();

            if (!_stockRequestItems.Any())
            {
                return result;
            }

            var variantIds = _stockRequestItems.Select(x => x.VariantId).Distinct().ToList();
            var variants = _pimDbContext.Variants.Where(x => variantIds.Contains(x.Id)).AsNoTracking().ToList();

            var productIds = variants.Select(x => x.ProductId).Distinct().ToList();
            var products = _pimDbContext.Products.Where(x => productIds.Contains(x.Id)).AsNoTracking().ToList();
            
            var stockRequestItems = new List<StockRequestItemModel>();
            foreach(var item in _stockRequestItems)
            {
                var variant = variants.FirstOrDefault(x => x.Id == item.VariantId);
                var product = products.FirstOrDefault(x => x.Id == variant.ProductId);

                stockRequestItems.Add(new StockRequestItemModel()
                {
                    Id = item.Id,
                    ProductId = product != null ? product.Id : Guid.Empty,
                    ProductName = product?.Name,
                    StockRequestId = item.StockRequestId,
                    Variant = variants.FirstOrDefault(x => x.Id == item.VariantId),
                    Quantity = item.Quantity
                });
            }

            result.StockRequestItems = stockRequestItems;

            return result;
        }
    }
}
