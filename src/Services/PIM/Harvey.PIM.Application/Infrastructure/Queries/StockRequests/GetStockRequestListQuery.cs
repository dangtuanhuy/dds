﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.StockRequests
{
    public class GetStockRequestListQuery : IQuery<PagedResult<StockRequestListModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string StatusIdString { get; }
        public string ToLocationIdString { get; }
        public string FromLocationIdString { get; }
        public string QueryString { get; }
        public GetStockRequestListQuery(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLcationIdString, string fromLocationIdString, string queryString)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            StatusIdString = statusIdString;
            ToLocationIdString = toLcationIdString;
            FromLocationIdString = fromLocationIdString;
            QueryString = queryString;
        }
    }

    public sealed class GetStockRequestListQueryHandler : IQueryHandler<GetStockRequestListQuery, PagedResult<StockRequestListModel>>
    {
        private readonly IEfRepository<TransactionDbContext, StockRequest> _repository;
        private readonly IEfRepository<PimDbContext, Location> _locationRepository;
        public GetStockRequestListQueryHandler(IEfRepository<TransactionDbContext, StockRequest> repository,
                                               IEfRepository<PimDbContext, Location> locationRepository)
        {
            _repository = repository;
            _locationRepository = locationRepository;
        }

        public async Task<PagedResult<StockRequestListModel>> Handle(GetStockRequestListQuery query)
        {            
            var locationIds = new List<Guid>();
            var locations = await _locationRepository.GetAsync();
            var statusIds = string.IsNullOrEmpty(query.StatusIdString) ? null : query.StatusIdString.Split("|");
            var toLocationIds = string.IsNullOrEmpty(query.ToLocationIdString) ? null : query.ToLocationIdString.Split("|");
            var fromLocationIds = string.IsNullOrEmpty(query.FromLocationIdString) ? null : query.FromLocationIdString.Split("|");

            var predicate = PredicateBuilder.True<StockRequest>();
            if (!string.IsNullOrEmpty(query.QueryString))
            {                
                predicate = predicate.And(x => x.Subject.ToUpper().Contains(query.QueryString.ToUpper()));
                predicate = predicate.Or(x => x.Description.ToUpper().Contains(query.QueryString.ToUpper()));
            }
            if (!string.IsNullOrEmpty(query.StatusIdString))
            {
                predicate = predicate.And(x => statusIds.Contains(x.StockRequestStatus.ToString()));
            }
            if (!string.IsNullOrEmpty(query.ToLocationIdString))
            {
                predicate = predicate.And(x => toLocationIds.Contains(x.ToLocationId.ToString()));
            }
            if (!string.IsNullOrEmpty(query.FromLocationIdString))
            {
                predicate = predicate.And(x => fromLocationIds.Contains(x.FromLocationId.ToString()));
            }

            var result = await _repository.ListAsync(predicate, query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);            

            var data = result.Select(x => new StockRequestListModel()
            {                
                Id = x.Id,
                ToLocationName = locations.FirstOrDefault(y => y.Id == x.ToLocationId)?.Name,
                FromLocationName = locations.FirstOrDefault(y => y.Id == x.FromLocationId)?.Name,
                Subject = x.Subject,
                Description = x.Description,
                Status = x.StockRequestStatus.ToString(),
                CreatedBy = x.CreatedBy,
                CreatedDate = x.CreatedDate,
                UpdatedBy = x.UpdatedBy,
                UpdatedDate = x.UpdatedDate
            });

            var totalItems = await _repository.Count(predicate);

            return new PagedResult<StockRequestListModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalItems,
                Data = data
            };
        }
    }
}
