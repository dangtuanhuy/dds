﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Categories
{
    public sealed class GetAllCategoriesQuery : IQuery<IEnumerable<CategoryModel>>
    {
        public GetAllCategoriesQuery()
        {
        }
    }

    public sealed class GetAllCategoriesQueryHandler : IQueryHandler<GetAllCategoriesQuery, IEnumerable<CategoryModel>>
    {
        private readonly IEfRepository<PimDbContext, Category> _repository;
        public GetAllCategoriesQueryHandler(IEfRepository<PimDbContext, Category> repository)
        {
            _repository = repository;
        }
        public async Task<IEnumerable<CategoryModel>> Handle(GetAllCategoriesQuery query)
        {
            var categories = await _repository.GetAsync();
            var results = categories.Where(x => x.IsDelete == false).Select(x => new CategoryModel()
            {
                Id = x.Id,
                Code = x.Code,
                Description = x.Description,
                IdentifiedId = x.IdentifiedId,
                Name = x.Name
            }).ToList();
            return results;
        }
    }
}
