﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Models.Feeds;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Categories
{
    public class GetCategoryIdsByProductIdsQuery : IQuery<GetCategoryIdsByProdutIdsReponse>
    {
        public List<Guid> ProductIds { get; set; }

        public GetCategoryIdsByProductIdsQuery(List<Guid> productIds)
        {
            ProductIds = productIds;
        }
    }

    public class GetCategoryIdsByProductIdsQueryHandler : IQueryHandler<GetCategoryIdsByProductIdsQuery, GetCategoryIdsByProdutIdsReponse>
    {
        private readonly PimDbContext _pimDbContext;

        public GetCategoryIdsByProductIdsQueryHandler(PimDbContext pimDbContext)
        {
            _pimDbContext = pimDbContext;
        }

        public async Task<GetCategoryIdsByProdutIdsReponse> Handle(GetCategoryIdsByProductIdsQuery query)
        {
            GetCategoryIdsByProdutIdsReponse result = new GetCategoryIdsByProdutIdsReponse();
            var productIds = query.ProductIds;
            result.data = await _pimDbContext.Products
                                .Where(x => productIds.Contains(x.Id))
                                .Select(x => new GetCategoryIdsByProdutIdsReponseItem
                                {
                                    ProductId = x.Id,
                                    CategoryId = x.CategoryId
                                }).ToListAsync();
            return result;
        }
    }
}
