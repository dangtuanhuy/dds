﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Categories
{
    public sealed class GetCategoriesQuery : IQuery<PagedResult<CategoryModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public GetCategoriesQuery(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }

    public sealed class GetCategoriesQueryHandler : IQueryHandler<GetCategoriesQuery, PagedResult<CategoryModel>>
    {
        private readonly IEfRepository<PimDbContext, Category> _repository;
        private readonly IMapper _mapper;
        public GetCategoriesQueryHandler(IEfRepository<PimDbContext, Category> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<PagedResult<CategoryModel>> Handle(GetCategoriesQuery query)
        {
            var result = string.IsNullOrEmpty(query.QueryText) ? await _repository.ListAsync(x => x.IsDelete == false, query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                                                                        query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection)
                                                               : await _repository.ListAsync(x => (x.Name.ToUpper().Contains(query.QueryText.ToUpper()) && x.IsDelete == false) ||
                                                                                                  (x.Code.ToUpper().Contains(query.QueryText.ToUpper()) && x.IsDelete == false),
                                                                                                  query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                                                                                                  query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);
            var categories = _mapper.Map<List<CategoryModel>>(result);
            var allCategories = await _repository.GetAsync();
            var totalPages = string.IsNullOrEmpty(query.QueryText) ? allCategories.Where(x => x.IsDelete == false).Count() 
                                                                   : await _repository.Count(x => (x.Name.ToUpper().Contains(query.QueryText.ToUpper()) && x.IsDelete == false) ||
                                                                                                  (x.Code.ToUpper().Contains(query.QueryText.ToUpper()) && x.IsDelete == false));
            return new PagedResult<CategoryModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = categories
            };
        }
    }
}
