﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.ReturnOrders
{
    public class GetReturnOrdersByVendorQuery : IQuery<List<ReturnOrderListModel>>
    {
        public Guid VendorId { get; set; }
        public GetReturnOrdersByVendorQuery(Guid vendorId)
        {
            VendorId = vendorId;
        }
    }

    public class GetReturnOrdersByVendorQueryHandler : IQueryHandler<GetReturnOrdersByVendorQuery, List<ReturnOrderListModel>>
    {
        private readonly IEfRepository<PimDbContext, PurchaseOrder> _repository;
        private readonly IEfRepository<PimDbContext, Vendor> _vendorRepository;
        public GetReturnOrdersByVendorQueryHandler(IEfRepository<PimDbContext, PurchaseOrder> repository,
            IEfRepository<PimDbContext, Vendor> vendorRepository)
        {
            _repository = repository;
            _vendorRepository = vendorRepository;
        }

        public async Task<List<ReturnOrderListModel>> Handle(GetReturnOrdersByVendorQuery query)
        {
            var results = new List<ReturnOrderListModel>();
            var vendors = await _vendorRepository.GetAsync();
            var returnOrders = await _repository.GetAsync();
            returnOrders = returnOrders.Where(x => x.VendorId == query.VendorId
                && x.ApprovalStatus == ApprovalStatus.Confirmed
                && x.Type == PurchaseOrderType.ReturnedOrder).ToList();

            foreach (var ro in returnOrders)
            {
                var status = ro.ApprovalStatus.ToString();
                var newRO = new ReturnOrderListModel()
                {
                    Id = ro.Id,
                    Description = ro.Description,
                    Name = ro.Name,
                    Vendor = vendors.Where(x => x.Id == ro.VendorId).Select(x => x.Name).FirstOrDefault()
                };
                results.Add(newRO);
            }

            return results;
        }
    }
}
