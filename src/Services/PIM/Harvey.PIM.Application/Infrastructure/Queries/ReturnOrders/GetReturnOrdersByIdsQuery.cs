﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.ReturnOrders
{
    public class GetReturnOrdersByIdsQuery : IQuery<List<ReturnOrderModel>>
    {
        public List<Guid> Ids { get; set; }
        public GetReturnOrdersByIdsQuery(List<Guid> ids)
        {
            Ids = ids;
        }
    }

    public class GetReturnOrdersByIdsQueryHandler : IQueryHandler<GetReturnOrdersByIdsQuery, List<ReturnOrderModel>>
    {
        private readonly IEfRepository<PimDbContext, PurchaseOrder> _poRepository;
        private readonly IEfRepository<PimDbContext, PurchaseOrderItem> _poItemRepository;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        public GetReturnOrdersByIdsQueryHandler(IEfRepository<PimDbContext, PurchaseOrder> poRepository,
                                           IEfRepository<PimDbContext, PurchaseOrderItem> poItemRepository,
                                           PimDbContext pimDbContext,
                                           TransactionDbContext transactionDbContext)
        {
            _poRepository = poRepository;
            _poItemRepository = poItemRepository;
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
        }
        public async Task<List<ReturnOrderModel>> Handle(GetReturnOrdersByIdsQuery query)
        {
            var results = new List<ReturnOrderModel>();
            var fieldValues = await _pimDbContext.FieldValues.Include(x => x.Field).AsNoTracking().ToListAsync();
            var variants = await _pimDbContext.Variants.AsNoTracking().ToListAsync();
            var products = await _pimDbContext.Products.AsNoTracking().ToListAsync();
            var barCodes = await _pimDbContext.BarCode.AsNoTracking().ToListAsync();
            var stockTypes = await _transactionDbContext.StockTypes.AsNoTracking().ToListAsync();
            foreach (var id in query.Ids)
            {
                var ro = await _poRepository.GetByIdAsync(id);
                if (ro != null)
                {
                    var result = await _poItemRepository.ListAsync(x => x.PurchaseOrderId == ro.Id);
                    var roItems = new List<ReturnOrderItemModel>();
                    foreach (var element in result)
                    {
                        var productId = variants.FirstOrDefault(x => x.Id == element.VariantId)?.ProductId;
                        var grtItems = await _transactionDbContext.GoodsReturnDocumentItems.Where(x => x.TransactionRefId == ro.Id
                                                                                       && x.StockTypeId == element.StockTypeId
                                                                                       && x.VariantId == element.VariantId).ToListAsync();
                        var roItem = new ReturnOrderItemModel()
                        {
                            Id = element.Id,
                            ProductName = products.FirstOrDefault(x => x.Id == productId)?.Name,
                            StockTypeId = element.StockTypeId,
                            StockTypeName = stockTypes.FirstOrDefault(x => x.Id == element.StockTypeId)?.Name,
                            BarCodes = barCodes.Where(x => x.VariantId == element.VariantId).Select(x => x.Code).ToList(),
                            Quantity = grtItems.Sum(x => x.Quantity) > 0 ? element.Quantity - grtItems.Sum(x => x.Quantity) : element.Quantity,
                            Variant = null
                        };

                        var roFieldValues = new List<FieldValueROModel>();
                        var productFieldTemplateId = products.FirstOrDefault(x => x.Id == productId)?.FieldTemplateId;

                        roFieldValues = fieldValues.Where(field => field.EntityId == element.VariantId)
                        .Select(field => new FieldValueROModel()
                        {
                            Name = field.Field.Name,
                            Value = FieldValueFactory.GetFieldValueFromFieldType(field.Field.Type, field)
                        }).ToList();

                        var roVariant = new VariantROModel()
                        {
                            Id = element.VariantId,
                            FieldValues = roFieldValues
                        };

                        roItem.Variant = roVariant;

                        roItems.Add(roItem);
                    }

                    results.Add(new ReturnOrderModel()
                    {
                        Id = ro.Id,
                        Name = ro.Name,
                        Description = ro.Description,
                        VendorId = ro.VendorId,
                        Items = roItems
                    });
                }
            }
            return results;
        }
    }
}

