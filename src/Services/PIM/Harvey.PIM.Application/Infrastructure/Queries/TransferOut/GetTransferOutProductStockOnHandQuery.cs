﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.TransferOut
{
    public sealed class GetTransferOutProductStockOnHandQuery : IQuery<int>
    {
        public Guid FromLocationId { get; }
        public Guid VariantId { get; }
        public GetTransferOutProductStockOnHandQuery(
            Guid fromLocationId,
            Guid variantId
            )
        {
            FromLocationId = fromLocationId;
            VariantId = variantId;
        }
    }

    public sealed class GetTransferOutProductStockOnHandQueryHandler : IQueryHandler<GetTransferOutProductStockOnHandQuery, int>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _repository;
        public GetTransferOutProductStockOnHandQueryHandler(
            IEfRepository<TransactionDbContext, InventoryTransaction> repository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
        }

        public async Task<int> Handle(GetTransferOutProductStockOnHandQuery query)
        {
            var stockTransactions = _transactionDbContext.StockTransactions.Where(x => x.VariantId == query.VariantId).ToList();
            var lastTransferStockTransaction = stockTransactions.OrderByDescending(x => x.CreatedDate)
                                                                .Where(x => x.LocationId == query.FromLocationId
                                                                    && x.VariantId == query.VariantId).FirstOrDefault();
            if (lastTransferStockTransaction != null)
            {
                return lastTransferStockTransaction.Balance;
            }
            return 0;
        }
    }
}
