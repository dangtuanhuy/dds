﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.TransferOut
{
    public sealed class GetTransferOutsByLocationQuery : IQuery<PagedResult<InventoryTransactionTransferViewModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public Guid FromLocationId { get; }
        public Guid ToLocationId { get; }
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public GetTransferOutsByLocationQuery(
            PagingFilterCriteria pagingFilterCriteria,
            Guid fromLocationId,
            Guid toLocationId,
            DateTime fromDate,
            DateTime toDate)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            FromLocationId = fromLocationId;
            ToLocationId = toLocationId;
            FromDate = fromDate;
            ToDate = toDate;
        }
    }

    public sealed class GetTransferOutsByLocationQueryHandler : IQueryHandler<GetTransferOutsByLocationQuery, PagedResult<InventoryTransactionTransferViewModel>>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _repository;
        public GetTransferOutsByLocationQueryHandler(
            IEfRepository<TransactionDbContext, InventoryTransaction> repository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
        }

        public async Task<PagedResult<InventoryTransactionTransferViewModel>> Handle(GetTransferOutsByLocationQuery query)
        {
            DateTime FromDate = new DateTime(query.FromDate.Year, query.FromDate.Month, query.FromDate.Day, 00, 00, 00, 00);
            DateTime ToDate = new DateTime(query.ToDate.Year, query.ToDate.Month, query.ToDate.Day, 23, 59, 59, 100);
            var transactionTypeTFO = _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == TransactionTypeCode.TFO.ToString()).FirstOrDefault();
            var transactionTypeTFI = _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == TransactionTypeCode.TFI.ToString()).FirstOrDefault();

            var listTFOIdByLocationAndDateTime = (await _repository.ListAsync(x => (x.ToLocationId == query.ToLocationId)
                                                                    && (x.CreatedDate >= FromDate)
                                                                    && (x.CreatedDate <= ToDate)
                                                                    && (x.TransactionTypeId == transactionTypeTFO.Id)))
                                                                    .Select(a=>a.Id);

            var allTFOTransactionRefs = (await _repository.ListAsync(x => x.TransactionTypeId == transactionTypeTFI.Id 
                                                                    && listTFOIdByLocationAndDateTime.Contains(x.TransactionRefId)))
                                                                    .Select(x => x.TransactionRefId)
                                                                    .Distinct();

            var inventoryTransactions = await _repository.ListAsync(x => (x.ToLocationId == query.ToLocationId)
                                                                     && (x.CreatedDate >= FromDate)
                                                                     && (x.CreatedDate <= ToDate)
                                                                     && (x.TransactionTypeId == transactionTypeTFO.Id)
                                                                     && !allTFOTransactionRefs.Contains(x.Id),
                                                                         query.PagingFilterCriteria.Page,
                                                                         query.PagingFilterCriteria.NumberItemsPerPage,
                                                                         query.PagingFilterCriteria.SortColumn,
                                                                         query.PagingFilterCriteria.SortDirection);
            var totalPages = await _repository.Count(x => (x.ToLocationId == query.ToLocationId)
                                                       && (x.CreatedDate >= FromDate)
                                                       && (x.CreatedDate <= ToDate)
                                                       && (x.TransactionTypeId == transactionTypeTFO.Id)
                                                       && !allTFOTransactionRefs.Contains(x.Id));


            var inventoryTransactionIds = inventoryTransactions.Select(x => x.Id).Distinct().ToList();

            var stockTransactions = await _transactionDbContext.StockTransactions.AsNoTracking()
                                                                                 .Where(x => inventoryTransactionIds.Contains(x.InventoryTransactionId))
                                                                                 .ToListAsync();


            var locationIds = inventoryTransactions.Select(x => x.FromLocationId).ToList();
            locationIds.AddRange(inventoryTransactions.Select(x => x.ToLocationId));
            locationIds.Distinct();

            var locations = await _pimDbContext.Locations.Where(x => locationIds.Contains(x.Id))
                                                         .Select(x => new {
                                                             Id = x.Id,
                                                             Name = x.Name
                                                         })
                                                         .ToListAsync();
            var outlet = _pimDbContext.Locations.FirstOrDefault(x => x.Id == query.ToLocationId);

            var result = new List<InventoryTransactionTransferViewModel>();

            foreach (var inventoryTransaction in inventoryTransactions)
            {
                var InventoryTransactionTransferOutModel = new InventoryTransactionTransferViewModel
                {
                    Id = inventoryTransaction.Id,
                    InventoryTransactionRefId = inventoryTransaction.TransactionRefId,
                    TransferNumber = inventoryTransaction.TransferNumber,
                    FromLocationId = inventoryTransaction.FromLocationId,
                    ToLocationId = inventoryTransaction.ToLocationId,
                    FromLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.FromLocationId)?.Name,
                    ToLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.ToLocationId)?.Name,
                    CreatedDate = inventoryTransaction.CreatedDate,
                    CreatedBy = inventoryTransaction.CreatedBy,
                    UpdatedBy = inventoryTransaction.UpdatedBy,
                    UpdatedDate = inventoryTransaction.UpdatedDate
                };

                var InventoryTransactionTransferOutProducts = _transactionDbContext.StockTransactions
                                                                        .Where(x => x.InventoryTransactionId == inventoryTransaction.Id);

                foreach (var item in InventoryTransactionTransferOutProducts)
                {
                    var inventoryTransactionTransferOutProduct = new InventoryTransactionTransferProduct
                    {
                        Id = item.Id,
                        Quantity = item.Quantity,
                        VariantId = item.VariantId,
                        StockTypeId = item.StockTypeId,
                        StockTransactionRefId = item.StockTransactionRefId,
                        TransactionRefId = item.StockTransactionRefId
                    };
                    var variant = _pimDbContext.Variants.FirstOrDefault(x => x.Id == item.VariantId);
                    if (variant != null)
                    {
                        inventoryTransactionTransferOutProduct.ProductId = variant.ProductId;
                    }

                    InventoryTransactionTransferOutModel.InventoryTransactionTransferProducts.Add(inventoryTransactionTransferOutProduct);
                }

                result.Add(InventoryTransactionTransferOutModel);
            }

            return new PagedResult<InventoryTransactionTransferViewModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = result
            };
        }
    }
}
