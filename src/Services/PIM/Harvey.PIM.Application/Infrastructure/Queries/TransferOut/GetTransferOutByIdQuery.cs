﻿using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Services.Interface;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.TransferOut
{
    public sealed class GetTransferOutByIdQuery : IQuery<InventoryTransactionTransferViewModel>
    {
        public Guid Id { get; }
        public GetTransferOutByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetTransferOutByIdQueryHandler : IQueryHandler<GetTransferOutByIdQuery, InventoryTransactionTransferViewModel>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _repository;
        private readonly IEntityRefService _entityRefService;
        private readonly IEventStoreRepository<Product> _productRepository;
        public GetTransferOutByIdQueryHandler(
            IEfRepository<TransactionDbContext, InventoryTransaction> repository,
            TransactionDbContext transactionDbContext,
            IEntityRefService entityRefService,
            IEventStoreRepository<Product> productRepository,
            PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
            _productRepository = productRepository;
            _entityRefService = entityRefService;
        }

        public async Task<InventoryTransactionTransferViewModel> Handle(GetTransferOutByIdQuery query)
        {
            var inventoryTransaction = await _repository.GetByIdAsync(query.Id);

            var locationIds = new List<Guid>
            {
                inventoryTransaction.FromLocationId,
                inventoryTransaction.ToLocationId
            };

            var locations = await _pimDbContext.Locations.Where(x => locationIds.Contains(x.Id))
                                                         .Select(x => new
                                                         {
                                                             x.Id,
                                                             x.Name,
                                                             x.Type
                                                         }).ToListAsync();

            var inventoryTransactionTransferOutModel = new InventoryTransactionTransferViewModel
            {
                Id = inventoryTransaction.Id,
                InventoryTransactionRefId = inventoryTransaction.TransactionRefId,
                TransferNumber = inventoryTransaction.TransferNumber,
                FromLocationId = inventoryTransaction.FromLocationId,
                FromLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.FromLocationId)?.Name,
                ToLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.ToLocationId)?.Name,
                ToLocationId = inventoryTransaction.ToLocationId,
                CreatedDate = inventoryTransaction.CreatedDate,
                CreatedBy = inventoryTransaction.CreatedBy,
                UpdatedBy = inventoryTransaction.UpdatedBy,
                UpdatedDate = inventoryTransaction.UpdatedDate,
            };

            var transferOutStockTransactions = _transactionDbContext.StockTransactions.Where(x => x.InventoryTransactionId == inventoryTransaction.Id).ToList();

            var transferOutStockTransactionGroupByVanriants = transferOutStockTransactions.GroupBy(x => x.VariantId).ToList();

            var variantIds = transferOutStockTransactions.Select(x => x.VariantId).Distinct().ToList();
            var variants = await _pimDbContext.Variants.Where(x => variantIds.Contains(x.Id)).ToListAsync();
            var productIds = variants.Select(x => x.ProductId).Distinct().ToList();
            var products = await _pimDbContext.Products.Where(x => productIds.Contains(x.Id)).ToListAsync();

            foreach (var transferOutStockTransaction in transferOutStockTransactionGroupByVanriants)
            {
                var inventoryTransactionTransferOutProduct = await getInventoryTransactionTransferOutProduct(transferOutStockTransaction, variants, products);

                inventoryTransactionTransferOutModel.InventoryTransactionTransferProducts.Add(inventoryTransactionTransferOutProduct);
            }

            return inventoryTransactionTransferOutModel;
        }
        public async Task<InventoryTransactionTransferProduct> getInventoryTransactionTransferOutProduct(IGrouping<Guid, StockTransaction> transferOutStockTransaction, List<Variant> variants, List<Product> products)
        {
            var inventoryTransactionTransferOutProduct = new InventoryTransactionTransferProduct
            {
                Id = transferOutStockTransaction.Key,
                StockTransactionRefId = transferOutStockTransaction.FirstOrDefault().StockTransactionRefId,
                TransactionRefId = transferOutStockTransaction.FirstOrDefault().StockTransactionRefId,
                VariantId = transferOutStockTransaction.FirstOrDefault().VariantId,
                StockTypeId = Guid.Empty,
                Quantity = transferOutStockTransaction.Sum(x => x.Quantity),                
            };

            var variant = variants.FirstOrDefault(x => x.Id == transferOutStockTransaction.FirstOrDefault().VariantId);

            var product = products.FirstOrDefault(x => x.Id == variant.ProductId);
            inventoryTransactionTransferOutProduct.ProductId = product.Id;
            inventoryTransactionTransferOutProduct.ProductName = product.Name;            

            var inventoryVanriants = new List<InventoryTransactionTransferProductVariant>
            {
                new InventoryTransactionTransferProductVariant
                {
                    Id = variant.Id,
                    Name = variant.Name
                }
            };

            inventoryTransactionTransferOutProduct.Variants = inventoryVanriants;
            return inventoryTransactionTransferOutProduct;            
        }
    }
}
