﻿using Harvey.Domain;
using Harvey.Domain.Constant;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.TransferOut
{
    public sealed class GetTransferOutsQuery : IQuery<PagedResult<InventoryTransactionTransferViewModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string StatusIdString { get; }
        public string ToLocationIdString { get; }
        public string FromLocationIdString { get; }
        public string QueryString { get; }
        public GetTransferOutsQuery(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLcationIdString, string fromLocationIdString, string queryString)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            StatusIdString = statusIdString;
            ToLocationIdString = toLcationIdString;
            FromLocationIdString = fromLocationIdString;
            QueryString = queryString;
        }
    }

    public sealed class GetTransferOutsQueryHandler : IQueryHandler<GetTransferOutsQuery, PagedResult<InventoryTransactionTransferViewModel>>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _dbContext;
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _repository;
        public GetTransferOutsQueryHandler(
            IEfRepository<TransactionDbContext, InventoryTransaction> repository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext,
            TransactionDbContext dbcontext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
            _dbContext = dbcontext;
        }

        public async Task<PagedResult<InventoryTransactionTransferViewModel>> Handle(GetTransferOutsQuery query)
        {
            var transactionTypeTFO =  await _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == "TFO").FirstOrDefaultAsync();
            var statusIds = string.IsNullOrEmpty(query.StatusIdString) ? null : query.StatusIdString.Split("|");
            var toLocationIds = string.IsNullOrEmpty(query.ToLocationIdString) ? null : query.ToLocationIdString.Split("|");
            var fromLocationIds = string.IsNullOrEmpty(query.FromLocationIdString) ? null : query.FromLocationIdString.Split("|");

            var predicate = PredicateBuilder.True<InventoryTransaction>();
            if (!string.IsNullOrEmpty(query.QueryString))
            {
                predicate = predicate.And(x => x.TransferNumber.ToUpper().Contains(query.QueryString.ToUpper()));
            }
            if (!string.IsNullOrEmpty(query.ToLocationIdString))
            {
                predicate = predicate.And(x => toLocationIds.Contains(x.ToLocationId.ToString()));
            }
            if (!string.IsNullOrEmpty(query.FromLocationIdString))
            {
                predicate = predicate.And(x => fromLocationIds.Contains(x.FromLocationId.ToString()));
            }

            predicate = predicate.And(x => x.TransactionTypeId == transactionTypeTFO.Id);

            var inventoryTransactions = await _repository.ListAsync(predicate, 0,0, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);

            var totalPages = await _repository.Count(predicate);

            var result = new List<InventoryTransactionTransferViewModel>();


            var locationIds = inventoryTransactions.Select(x => x.FromLocationId).ToList();
            locationIds.AddRange(inventoryTransactions.Select(x => x.ToLocationId));
            locationIds.Distinct();

            var locations = await _pimDbContext.Locations.Where(x => locationIds.Contains(x.Id))
                                                         .Select(x => new { Id = x.Id,
                                                                            Name = x.Name })
                                                         .ToListAsync();

            foreach (var inventoryTransaction in inventoryTransactions)
            {
                var InventoryTransactionTransferOutModel = new InventoryTransactionTransferViewModel
                {
                    Id = inventoryTransaction.Id,
                    InventoryTransactionRefId = inventoryTransaction.TransactionRefId,
                    TransferNumber = inventoryTransaction.TransferNumber,
                    FromLocationId = inventoryTransaction.FromLocationId,
                    FromLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.FromLocationId)?.Name,
                    ToLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.ToLocationId)?.Name,
                    ToLocationId = inventoryTransaction.ToLocationId,
                    CreatedDate = inventoryTransaction.CreatedDate,
                    CreatedBy = inventoryTransaction.CreatedBy,
                    UpdatedBy = inventoryTransaction.UpdatedBy,
                    UpdatedDate = inventoryTransaction.UpdatedDate
                };
                result.Add(InventoryTransactionTransferOutModel);
            }
            IOrderedEnumerable<InventoryTransactionTransferViewModel> orderResult;
            if (!string.IsNullOrEmpty(query.PagingFilterCriteria.SortColumn))
            {
                orderResult = query.PagingFilterCriteria.SortDirection == SortDirection.Ascending
                         ? result.OrderBy(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn))
                         : result.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn));
            }
            else
            {
                PropertyInfo propertyInfo =
                     result.GetType().GetGenericArguments()[0].GetProperty(SortColumnConstants.DefaultSortColumn);

                if (propertyInfo != null)
                {
                    orderResult = result.OrderByDescending(a => a.UpdatedDate);
                }
                else
                {
                    orderResult = result.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, SortColumnConstants.SecondSortColumn));
                }
            }

            var entities = orderResult
               .Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
               .Take(query.PagingFilterCriteria.NumberItemsPerPage)
               .ToList();
            return new PagedResult<InventoryTransactionTransferViewModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = orderResult.Count(),
                Data = entities
            };
        }
    }
}
