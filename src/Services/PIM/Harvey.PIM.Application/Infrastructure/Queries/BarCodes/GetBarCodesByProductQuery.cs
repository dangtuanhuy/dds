﻿using Harvey.Domain;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.BarCodes
{
    public sealed class GetBarCodesByProductQuery : IQuery<BarCodeProductViewModel>
    {
        public Guid ProductId { get; }
        public GetBarCodesByProductQuery(Guid productId)
        {
            ProductId = productId;
        }
    }

    public sealed class GetBarCodesByProductQueryHandler : IQueryHandler<GetBarCodesByProductQuery, BarCodeProductViewModel>
    {
        private readonly PimDbContext _pimDbContext;
        public GetBarCodesByProductQueryHandler(PimDbContext pimDbContext)
        {
            _pimDbContext = pimDbContext;
        }
        public async Task<BarCodeProductViewModel> Handle(GetBarCodesByProductQuery query)
        {
            var barCodeVariantResults = new List<BarCodeVariantViewModel>();
            var variants = await _pimDbContext.Variants.AsNoTracking().Where(x => x.ProductId == query.ProductId)
                                    .OrderBy(x => x.CreatedDate).ToListAsync();
            var variantIds = variants.Select(x => x.Id);
            var barCodes = await _pimDbContext.BarCode.AsNoTracking().Where(x => variantIds.Contains(x.VariantId)).ToListAsync();
            var fieldValues = await _pimDbContext.FieldValues.Include(x => x.Field).AsNoTracking().Where(x => variantIds.Contains(x.EntityId)).ToListAsync();

            foreach (var variant in variants)
            {
                var barCodesVariant = barCodes.Where(x => x.VariantId == variant.Id)
                    .Select(x => new BarCodeVariantItemViewModel() { Id = x.Id, Code = x.Code }).ToList();
                var newBarCodeVariantViewModel = new BarCodeVariantViewModel()
                {
                    VariantId = variant.Id,
                    VariantSKU = variant.SKUCode,
                    Fields = fieldValues.Where(field => field.EntityId == variant.Id)
                                               .Select(field => new VariantField()
                                               {
                                                   Name = field.Field.Name,
                                                   Value = FieldValueFactory.GetFieldValueFromFieldType(field.Field.Type, field)
                                               }).ToList(),
                    BarCodes = barCodesVariant
                };
                barCodeVariantResults.Add(newBarCodeVariantViewModel);
            }

            var product = await _pimDbContext.Products.SingleOrDefaultAsync(x => x.Id == query.ProductId);
            var result = new BarCodeProductViewModel()
            {
                ProductId = product.Id,
                ProductName = product.Name,
                BarCodesVariant = barCodeVariantResults
            };

            return result;
        }
    }
}
