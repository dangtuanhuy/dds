﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Channels
{
    public class GetChannelStoreAssignmentsQuery: IQuery<List<ChannelStoreAssignmentModel>>
    {
        public Guid ChannelId { get; set; }
        public GetChannelStoreAssignmentsQuery(Guid channelId)
        {
            ChannelId = channelId;
        }

        public class GetChannelStoreAssignmentsQueryHandler : IQueryHandler<GetChannelStoreAssignmentsQuery, List<ChannelStoreAssignmentModel>>
        {
            private readonly IEfRepository<PimDbContext, Location> _repository;
            private readonly IEfRepository<PimDbContext, ChannelStoreAssignment> _repositoryChannelStoreAssignment;

            public GetChannelStoreAssignmentsQueryHandler(IEfRepository<PimDbContext, Location> repository,
                                                       IEfRepository<PimDbContext, ChannelStoreAssignment> repositoryChannelStoreAssignment)
            {
                _repository = repository;
                _repositoryChannelStoreAssignment = repositoryChannelStoreAssignment;
            }

            public async Task<List<ChannelStoreAssignmentModel>> Handle(GetChannelStoreAssignmentsQuery query)
            {
                var channels = await _repositoryChannelStoreAssignment.ListAsync(x => x.ChannelId == query.ChannelId);

                var assignmentIds = channels.Select(x => x.StoreId);
                var assignmentEntities = await _repository.ListAsync(x => assignmentIds.Contains(x.Id));

                return assignmentEntities
                        .Select(x => new ChannelStoreAssignmentModel()
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).ToList();
            }
        }
    }
}
