﻿using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Locations
{
    public class SearchLocationsQuery : IQuery<IEnumerable<LocationModel>>
    {
        private readonly LocationType LocationType;
        private readonly string Text;

        public SearchLocationsQuery(LocationType locationType, string text)
        {
            LocationType = locationType;
            Text = text;
        }

        public class SearchLocationsQueryHandler : IQueryHandler<SearchLocationsQuery, IEnumerable<LocationModel>>
        {
            private readonly IEfRepository<PimDbContext, Location> _repository;

            public SearchLocationsQueryHandler(IEfRepository<PimDbContext, Location> repository)
            {
                _repository = repository;
            }

            public async Task<IEnumerable<LocationModel>> Handle(SearchLocationsQuery query)
            {
                var predicate = PredicateBuilder.True<Location>();
                if (!string.IsNullOrEmpty(query.Text))
                {
                    predicate = predicate.And(x => x.Name.ToUpper().Contains(query.Text.ToUpper()));
                }
                if (query.LocationType != LocationType.None)
                {
                    predicate = predicate.And(x => x.Type == query.LocationType);
                }

                var locations = await _repository.ListAsync(predicate);
                return locations.Select(x =>
                                    new LocationModel()
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        Address = x.Address,
                                        LocationCode = x.LocationCode,
                                        Type = x.Type
                                    });
            }

            private bool TextSearchCondition(Location location, string text)
            {
                if (text == null || text == "")
                {
                    return true;
                }

                return location.Name.ToUpper().Contains(text.ToUpper());
            }

            private bool TypeCondition(Location location, LocationType type)
            {
                if (type == 0)
                {
                    return true;
                }

                return location.Type == type;
            }
        }
    }
}
