﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Locations
{
    public sealed class GetLocationsQuery : IQuery<PagedResult<LocationModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public GetLocationsQuery(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }
    public sealed class GetLocationsQueryHandler : IQueryHandler<GetLocationsQuery, PagedResult<LocationModel>>
    {
        private readonly IEfRepository<PimDbContext, Location> _repository;
        private readonly IMapper _mapper;
        public GetLocationsQueryHandler(IEfRepository<PimDbContext, Location> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<PagedResult<LocationModel>> Handle(GetLocationsQuery query)
        {
            var result = string.IsNullOrEmpty(query.QueryText) ? await _repository.GetAsync(query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection)
                                                               : await _repository.ListAsync(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper()),
                                                                    query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);

            var locations = _mapper.Map<List<LocationModel>>(result);
            var totalPages = string.IsNullOrEmpty(query.QueryText) ? await _repository.Count()
                                                                   : await _repository.Count(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper()));
            return new PagedResult<LocationModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = locations
            };
        }
    }

}
