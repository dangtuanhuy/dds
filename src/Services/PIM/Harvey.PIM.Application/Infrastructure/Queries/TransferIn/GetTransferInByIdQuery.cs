﻿using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Services.Interface;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.TransferIn
{
    public sealed class GetTransferInByIdQuery : IQuery<InventoryTransactionTransferViewModel>
    {
        public Guid Id { get; }
        public GetTransferInByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetTransferInByIdQueryHandler : IQueryHandler<GetTransferInByIdQuery, InventoryTransactionTransferViewModel>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _repository;
        private readonly IEventStoreRepository<Product> _productRepository;
        private readonly IEntityRefService _entityRefService;
        public GetTransferInByIdQueryHandler(
            IEfRepository<TransactionDbContext, InventoryTransaction> repository,
            TransactionDbContext transactionDbContext,
            IEventStoreRepository<Product> productRepository,
            IEntityRefService entityRefService,
            PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
            _productRepository = productRepository;
            _entityRefService = entityRefService;
        }

        public async Task<InventoryTransactionTransferViewModel> Handle(GetTransferInByIdQuery query)
        {
            var inventoryTransaction = await _repository.GetByIdAsync(query.Id);

            var locationIds = new List<Guid>();
            locationIds.Add(inventoryTransaction.FromLocationId);
            locationIds.Add(inventoryTransaction.ToLocationId);

            var locations = await _pimDbContext.Locations.Where(x => locationIds.Contains(x.Id))
                                                         .Select(x => new
                                                         {
                                                             Id = x.Id,
                                                             Name = x.Name
                                                         })
                                                         .ToListAsync();

            var inventoryTransactionTransferInModel = new InventoryTransactionTransferViewModel
            {
                Id = inventoryTransaction.Id,
                InventoryTransactionRefId = inventoryTransaction.TransactionRefId,
                TransferNumber = inventoryTransaction.TransferNumber,
                FromLocationId = inventoryTransaction.FromLocationId,
                ToLocationId = inventoryTransaction.ToLocationId,
                FromLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.FromLocationId)?.Name,
                ToLocation = locations.FirstOrDefault(x => x.Id == inventoryTransaction.ToLocationId)?.Name,
                CreatedDate = inventoryTransaction.CreatedDate,
                CreatedBy = inventoryTransaction.CreatedBy,
                UpdatedBy = inventoryTransaction.UpdatedBy,
                UpdatedDate = inventoryTransaction.UpdatedDate
            };

            var transferOutStockTransactions = _transactionDbContext.StockTransactions.Where(x => x.InventoryTransactionId == inventoryTransaction.Id).ToList();

            var transferOutStockTransactionGroupByVanriants = transferOutStockTransactions.GroupBy(x => x.VariantId).ToList();

            var variantIds = transferOutStockTransactions.Select(x => x.VariantId).Distinct().ToList();
            var variants = await _pimDbContext.Variants.Where(x => variantIds.Contains(x.Id)).ToListAsync();
            var productIds = variants.Select(x => x.ProductId).Distinct().ToList();
            var products = await _pimDbContext.Products.Where(x => productIds.Contains(x.Id)).ToListAsync();

            foreach (var transferOutStockTransaction in transferOutStockTransactionGroupByVanriants)
            {
                var inventoryTransactionTransferOutProduct = await getInventoryTransactionTransferInProduct(transferOutStockTransaction, variants, products);

                inventoryTransactionTransferInModel.InventoryTransactionTransferProducts.Add(inventoryTransactionTransferOutProduct);
            }

            return inventoryTransactionTransferInModel;
        }

        public async Task<InventoryTransactionTransferProduct> getInventoryTransactionTransferInProduct(IGrouping<Guid, StockTransaction> transferOutStockTransaction, List<Variant> variants, List<Product> products)
        {
            var inventoryTransactionTransferOutProduct = new InventoryTransactionTransferProduct
            {
                Id = transferOutStockTransaction.Key,
                StockTransactionRefId = transferOutStockTransaction.FirstOrDefault().StockTransactionRefId,
                TransactionRefId = transferOutStockTransaction.FirstOrDefault().StockTransactionRefId,
                VariantId = transferOutStockTransaction.FirstOrDefault().VariantId,
                StockTypeId = Guid.Empty,
                Quantity = transferOutStockTransaction.Sum(x => x.Quantity)
            };

            var variant = variants.FirstOrDefault(x => x.Id == transferOutStockTransaction.FirstOrDefault().VariantId);

            if (variant != null)
            {
                var product = await _productRepository.GetByIdAsync(variant.ProductId);

                if (!product.IsDelete)
                {
                    inventoryTransactionTransferOutProduct.ProductId = product.Id;
                    inventoryTransactionTransferOutProduct.ProductName = product.Name;
                    var fields = await _pimDbContext.Field_FieldTemplates.Where(x => x.FieldTemplateId == product.FieldTemplateId)
                                                    .Include(x => x.Field).ToListAsync();

                    var fieldsVariant = fields.Where(x => x.FieldTemplateId == product.FieldTemplateId && x.IsVariantField)
                                              .ToList();

                    var variantList = new List<ProductModel.VariantModel>();

                    foreach (var item in product.VariantFieldValues)
                    {
                        var newVariant = new ProductModel.VariantModel()
                        {
                            Id = item.Key,

                        };

                        if (product.VariantPrices.ContainsKey(item.Key))
                        {
                            newVariant.Price = product.VariantPrices.FirstOrDefault(x => x.Key == item.Key).Value;
                        }

                        foreach (var field in item.Value)
                        {
                            var dbField = fieldsVariant.FirstOrDefault(x => x.FieldId == field.Field.Id);
                            if (dbField != null)
                            {
                                field.Field.DefaultValue = dbField.Field?.DefaultValue;
                                newVariant.FieldValues.Add(FieldValueFactory.GetFromFieldValue(field, _entityRefService));
                            }
                        }

                        newVariant.Code = _pimDbContext.Variants.FirstOrDefault(x => x.Id == item.Key)?.Code;

                        variantList.Add(newVariant);
                    }

                    var inventoryVanriants = new List<InventoryTransactionTransferProductVariant>();
                    foreach (var existvariant in variantList)
                    {

                        var name = "";

                        foreach (var fieldValue in existvariant.FieldValues)
                        {
                            name += $"{fieldValue.Name}: {string.Join("", fieldValue.Value)};";
                        }

                        inventoryVanriants.Add(new InventoryTransactionTransferProductVariant
                        {
                            Id = existvariant.Id,
                            Name = name
                        });
                    }
                    inventoryTransactionTransferOutProduct.Variants.AddRange(inventoryVanriants);
                }
            }

            return inventoryTransactionTransferOutProduct;
        }
    }
}
