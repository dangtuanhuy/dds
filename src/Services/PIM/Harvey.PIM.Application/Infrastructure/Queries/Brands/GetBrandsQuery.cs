﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Brands
{
    public sealed class GetBrandsQuery : IQuery<PagedResult<BrandModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public GetBrandsQuery(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }

    public sealed class GetBrandsQueryHandler : IQueryHandler<GetBrandsQuery, PagedResult<BrandModel>>
    {
        private readonly IEfRepository<PimDbContext, Brand> _repository;
        private readonly IMapper _mapper;

        public GetBrandsQueryHandler(IEfRepository<PimDbContext, Brand> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<PagedResult<BrandModel>> Handle(GetBrandsQuery query)
        {
            var result = string.IsNullOrEmpty(query.QueryText) ? await _repository.GetAsync(query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                                                                    query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection)
                                                               : await _repository.ListAsync(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper()),
                                                                        query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                                                                        query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);

            var brands = _mapper.Map<List<BrandModel>>(result);
            var totalPages = string.IsNullOrEmpty(query.QueryText) ? await _repository.Count()
                                                                   : await _repository.Count(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper()));
            return new PagedResult<BrandModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = brands
            };
        }
    }
}
