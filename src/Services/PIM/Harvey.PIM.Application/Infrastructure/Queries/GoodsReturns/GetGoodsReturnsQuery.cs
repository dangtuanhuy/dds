﻿using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harvey.Domain.Constant;
using System.Reflection;
namespace Harvey.PIM.Application.Infrastructure.Queries.GoodsReturns
{
    public sealed class GetGoodsReturnsQuery : IQuery<PagedResult<InventoryTransactionModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string StatusIdString { get; }
        public string ToLocationIdString { get; }
        public string FromLocationIdString { get; }
        public string QueryString { get; }
        public GetGoodsReturnsQuery(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLcationIdString, string fromLocationIdString, string queryString)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            StatusIdString = statusIdString;
            ToLocationIdString = toLcationIdString;
            FromLocationIdString = fromLocationIdString;
            QueryString = queryString;
        }
    }
    public sealed class GetGoodsReturnsQueryHandler : IQueryHandler<GetGoodsReturnsQuery, PagedResult<InventoryTransactionModel>>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _repository;
        public GetGoodsReturnsQueryHandler(
           IEfRepository<TransactionDbContext, InventoryTransaction> repository,
           TransactionDbContext transactionDbContext,
           PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
        }
        public async Task<PagedResult<InventoryTransactionModel>> Handle(GetGoodsReturnsQuery query)
        {
            var transactionTypeGRT = _transactionDbContext.TransactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.GRT.ToString());
            var statusIds = string.IsNullOrEmpty(query.StatusIdString) ? null : query.StatusIdString.Split("|");
            var toLocationIds = string.IsNullOrEmpty(query.ToLocationIdString) ? null : query.ToLocationIdString.Split("|");
            var fromLocationIds = string.IsNullOrEmpty(query.FromLocationIdString) ? null : query.FromLocationIdString.Split("|");


            if (transactionTypeGRT == null)
            {
                throw new ArgumentException($"Transaction type Goods Return not found. Please try again!");
            }
            else
            {
                var predicate = PredicateBuilder.True<InventoryTransaction>();
                if (!string.IsNullOrEmpty(query.QueryString))
                {
                    predicate = predicate.And(x => x.TransferNumber.ToUpper().Contains(query.QueryString.ToUpper()) || x.Code.ToUpper().Contains(query.QueryString.ToUpper()));
                }
                if (!string.IsNullOrEmpty(query.ToLocationIdString))
                {
                    predicate = predicate.And(x => toLocationIds.Contains(x.ToLocationId.ToString()));
                }
                if (!string.IsNullOrEmpty(query.FromLocationIdString))
                {
                    predicate = predicate.And(x => fromLocationIds.Contains(x.FromLocationId.ToString()));
                }

                predicate = predicate.And(x => x.TransactionTypeId == transactionTypeGRT.Id);


                var results = new List<InventoryTransactionModel>();

                var inventoryTransactions = await _repository.ListAsync(predicate, 0, 0, "");

                var inventoryTransactionIds = inventoryTransactions.Select(x => x.Id).Distinct().ToList();

                var stockTransactions = await _transactionDbContext.StockTransactions.AsNoTracking()
                                                                                     .Where(x => inventoryTransactionIds.Contains(x.InventoryTransactionId))
                                                                                     .ToListAsync();

                var locations = _pimDbContext.Locations.AsNoTracking().ToList();
                var vendors = _pimDbContext.Vendors.AsNoTracking().ToList();
                foreach (var inventoryTransaction in inventoryTransactions)
                {
                    var inventoryTransactionModel = new InventoryTransactionModel()
                    {
                        Id = inventoryTransaction.Id,
                        FromLocationId = inventoryTransaction.FromLocationId,
                        ToLocationId = inventoryTransaction.ToLocationId,
                        TransferNumber = inventoryTransaction.Code,
                        Code = inventoryTransaction.Code,
                        ToLocationName = vendors.FirstOrDefault(x => x.Id == inventoryTransaction.ToLocationId)?.Name,
                        FromLocationName = locations.FirstOrDefault(x => x.Id == inventoryTransaction.FromLocationId)?.Name,
                        CreatedDate = inventoryTransaction.CreatedDate,
                        Items = null
                    };

                    var goodsReturnStockTransactions = _transactionDbContext.StockTransactions.Where(x => x.InventoryTransactionId == inventoryTransaction.Id);

                    var goodsReturnStockTransactionGroupByVanriants = goodsReturnStockTransactions.GroupBy(x => x.VariantId)
                                                                                              .Select(g => new
                                                                                              {
                                                                                                  StockTransactionId = g.Key,
                                                                                                  StockTransactionDetails = g.ToList()
                                                                                              });
                    var items = new List<InventoryTransactionProductModel>();
                    foreach (var goodsReturnStockTransaction in goodsReturnStockTransactionGroupByVanriants)
                    {
                        var inventoryTransactionProductModel = new InventoryTransactionProductModel
                        {
                            Id = goodsReturnStockTransaction.StockTransactionId,
                            StockTransactionRefId = goodsReturnStockTransaction.StockTransactionDetails.First().StockTransactionRefId,
                            TransactionRefId = goodsReturnStockTransaction.StockTransactionDetails.First().StockTransactionRefId,
                            VariantId = goodsReturnStockTransaction.StockTransactionDetails.First().VariantId,
                            StockTypeId = Guid.Empty,
                            Quantity = goodsReturnStockTransaction.StockTransactionDetails.Sum(x => x.Quantity)
                        };

                        var variant = _pimDbContext.Variants.FirstOrDefault(x => x.Id == goodsReturnStockTransaction.StockTransactionDetails.First().VariantId);
                        if (variant != null)
                        {
                            inventoryTransactionProductModel.ProductId = variant.ProductId;
                        }
                        items.Add(inventoryTransactionProductModel);
                    }
                    inventoryTransactionModel.Items = items;
                    results.Add(inventoryTransactionModel);
                }
                IOrderedEnumerable<InventoryTransactionModel> orderData;

                if (!string.IsNullOrEmpty(query.PagingFilterCriteria.SortColumn))
                {
                    orderData = query.PagingFilterCriteria.SortDirection == SortDirection.Ascending
                           ? results.OrderBy(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn))
                           : results.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn));
                }
                else
                {
                    PropertyInfo propertyInfo =
                         results.GetType().GetGenericArguments()[0].GetProperty(SortColumnConstants.DefaultSortColumn);

                    if (propertyInfo != null)
                    {
                        orderData = results.OrderByDescending(a => a.UpdatedDate);
                    }
                    else
                    {
                        orderData = results.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, SortColumnConstants.SecondSortColumn));
                    }
                }

                var entities = orderData
                     .Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
                     .Take(query.PagingFilterCriteria.NumberItemsPerPage)
                     .ToList();
                return new PagedResult<InventoryTransactionModel>()
                {
                    CurrentPage = query.PagingFilterCriteria.Page,
                    NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                    TotalItems = results.Count,
                    Data = entities
                };
            }
        }
    }
}
