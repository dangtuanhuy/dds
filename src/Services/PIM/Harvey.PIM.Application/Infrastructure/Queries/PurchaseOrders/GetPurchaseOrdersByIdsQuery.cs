﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.PurchaseOrders
{
    public class GetPurchaseOrdersByIdsQuery : IQuery<List<PurchaseOrderModel>>
    {
        public List<Guid> Ids { get; set; }
        public GetPurchaseOrdersByIdsQuery(List<Guid> ids)
        {
            Ids = ids;
        }
    }

    public class GetPurchaseOrdersByIdsQueryHandler : IQueryHandler<GetPurchaseOrdersByIdsQuery, List<PurchaseOrderModel>>
    {
        private readonly IEfRepository<PimDbContext, PurchaseOrder> _poRepository;
        private readonly IEfRepository<PimDbContext, PurchaseOrderItem> _poItemRepository;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        public GetPurchaseOrdersByIdsQueryHandler(IEfRepository<PimDbContext, PurchaseOrder> poRepository,
                                           IEfRepository<PimDbContext, PurchaseOrderItem> poItemRepository,
                                           PimDbContext pimDbContext,
                                           TransactionDbContext transactionDbContext)
        {
            _poRepository = poRepository;
            _poItemRepository = poItemRepository;
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
        }
        public async Task<List<PurchaseOrderModel>> Handle(GetPurchaseOrdersByIdsQuery query)
        {
            var results = new List<PurchaseOrderModel>();

            var tempPOs = await _pimDbContext.PurchaseOrders.Join(_pimDbContext.purchaseOrderItems,
                po => po.Id,
                poItem => poItem.PurchaseOrderId,
                (po, poItem) => new { po = po, poItem = poItem })
                .Where(x => x.po.Id == x.poItem.PurchaseOrderId && query.Ids.Contains(x.po.Id))
                .Select(x => new
                {
                    poId = x.po.Id,
                    poName = x.po.Name,
                    poDescription = x.po.Description,
                    VendorId = x.po.VendorId,
                    poItemId = x.poItem.Id,
                    VariantId = x.poItem.VariantId,
                    StockTypeId = x.poItem.StockTypeId,
                    Quantity = x.poItem.Quantity
                })
                .ToListAsync();

            var variantIds = tempPOs.Select(x => x.VariantId).Distinct();
            var products = await _pimDbContext.Products.Join(_pimDbContext.Variants,
                p => p.Id,
                v => v.ProductId,
                (p, v) => new { product = p, variant = v })
                .Where(x => variantIds.Contains(x.variant.Id))
                .Select(x => new
                {
                    productId = x.product.Id,
                    productName = x.product.Name,
                    VariantId = x.variant.Id
                })
                .ToListAsync();

            var stockTypes = await _transactionDbContext.StockTypes.AsNoTracking().ToListAsync();
            var barCodes = await _pimDbContext.BarCode.AsNoTracking()
                            .Where(x => variantIds.Contains(x.VariantId))
                            .GroupBy(x => new { x.VariantId }, (key, group) => new
                            {
                                key.VariantId,
                                BarCodes = group.Select(x => x.Code).ToList()
                            })
                            .ToListAsync();
            var purchaseOrders = tempPOs.GroupBy(x => new { x.poId }, (key, group) => new PurchaseOrderModel()
            {
                Id = key.poId,
                Name = group.FirstOrDefault(g => g.poId == key.poId)?.poName,
                Description = group.FirstOrDefault(g => g.poId == key.poId)?.poDescription,
                VendorId = group.FirstOrDefault(g => g.poId == key.poId) != null ?
                                group.FirstOrDefault(g => g.poId == key.poId).VendorId : Guid.Empty,
                PurchaseOrderItems = group.Select(g => new PurchaseOrderItemModel()
                {
                    Id = g.poItemId,
                    StockTypeId = g.StockTypeId,
                    StockTypeName = stockTypes.FirstOrDefault(s => s.Id == g.StockTypeId)?.Name,
                    ProductName = products.FirstOrDefault(p => p.VariantId == g.VariantId)?.productName,
                    BarCodes = barCodes.FirstOrDefault(b => b.VariantId == g.VariantId) == null ? new List<string>() : barCodes.FirstOrDefault(b => b.VariantId == g.VariantId).BarCodes,
                    Variant = new VariantPOModel()
                    {
                        Id = g.VariantId,
                        FieldValues = null
                    },
                    Quantity = g.Quantity
                }).ToList()
            }).ToList();

            var giwItems = await _transactionDbContext.GIWDocumentItems
                            .Where(x => query.Ids.Contains(x.TransactionRefId))
                            .ToListAsync();
            var fieldValues = await _pimDbContext.FieldValues.AsNoTracking()
                                .Where(x => variantIds.Contains(x.EntityId))
                                .Include(x => x.Field)
                                .ToListAsync();
            foreach (var po in purchaseOrders)
            {
                foreach (var item in po.PurchaseOrderItems)
                {
                    var poGIWItems = giwItems.Where(x => x.TransactionRefId == po.Id
                                                            && x.VariantId == item.Variant.Id
                                                            && x.StockTypeId == item.StockTypeId).ToList();
                    item.Quantity = poGIWItems.Sum(x => x.Quantity) > 0 ? item.Quantity - poGIWItems.Sum(x => x.Quantity) : item.Quantity;
                    var poFieldValues = fieldValues.Where(field => field.EntityId == item.Variant.Id)
                        .Select(field => new FieldValuePOModel()
                        {
                            Name = field.Field.Name,
                            Value = FieldValueFactory.GetFieldValueFromFieldType(field.Field.Type, field)
                        }).ToList();

                    var poVariant = new VariantPOModel()
                    {
                        Id = item.Variant.Id,
                        FieldValues = poFieldValues
                    };
                    item.Variant = poVariant;
                }
            }
            return purchaseOrders;
        }
    }
}
