﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.PurchaseOrders
{
    public class GetPurchaseOrdersByVendorQuery : IQuery<List<PurchaseOrderListModel>>
    {
        public Guid VendorId { get; set; }
        public GetPurchaseOrdersByVendorQuery(Guid vendorId)
        {
            VendorId = vendorId;
        }
    }

    public class GetPurchaseOrdersByVendorQueryHandler : IQueryHandler<GetPurchaseOrdersByVendorQuery, List<PurchaseOrderListModel>>
    {
        private readonly IEfRepository<PimDbContext, PurchaseOrder> _repository;
        private readonly IEfRepository<PimDbContext, Vendor> _vendorRepository;
        public GetPurchaseOrdersByVendorQueryHandler(IEfRepository<PimDbContext, PurchaseOrder> repository,
            IEfRepository<PimDbContext, Vendor> vendorRepository)
        {
            _repository = repository;
            _vendorRepository = vendorRepository;
        }

        public async Task<List<PurchaseOrderListModel>> Handle(GetPurchaseOrdersByVendorQuery query)
        {
            var results = new List<PurchaseOrderListModel>();
            var vendors = await _vendorRepository.GetAsync();
            var purchaseOrders = await _repository.GetAsync();
            purchaseOrders = purchaseOrders.Where(x => x.VendorId == query.VendorId
                && x.ApprovalStatus == ApprovalStatus.Confirmed
                && x.Type == PurchaseOrderType.PurchaseOrder).ToList();

            foreach (var po in purchaseOrders)
            {
                var status = po.ApprovalStatus.ToString();
                var newPO = new PurchaseOrderListModel()
                {
                    Id = po.Id,
                    Description = po.Description,
                    Name = po.Name,
                    Vendor = vendors.Where(x => x.Id == po.VendorId).Select(x => x.Name).FirstOrDefault()
                };
                results.Add(newPO);
            }

            return results;
        }
    }
}
