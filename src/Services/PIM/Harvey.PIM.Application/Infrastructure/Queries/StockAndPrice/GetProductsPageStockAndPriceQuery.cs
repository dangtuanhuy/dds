﻿using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.StockAndPrice
{
    public sealed class GetProductsPageStockAndPriceQuery : IQuery<PagedResult<StockAndPriceProductModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public GetProductsPageStockAndPriceQuery(PagingFilterCriteria pagingFilterCriteria,
            string queryText)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }

    public sealed class GetProductsPageStockAndPriceQueryHandler : IQueryHandler<GetProductsPageStockAndPriceQuery, PagedResult<StockAndPriceProductModel>>
    {
        private readonly IEfRepository<PimDbContext, Product> _productRepository;
        private readonly IEfRepository<PimDbContext, Variant> _variantRepository;
        private readonly PimDbContext _pimDbContext;
        public GetProductsPageStockAndPriceQueryHandler(IEfRepository<PimDbContext, Product> productRepository,
            IEfRepository<PimDbContext, Variant> variantRepository,
            PimDbContext pimDbContext)
        {
            _productRepository = productRepository;
            _variantRepository = variantRepository;
            _pimDbContext = pimDbContext;
        }

        public async Task<PagedResult<StockAndPriceProductModel>> Handle(GetProductsPageStockAndPriceQuery query)
        {
            var predicate = PredicateBuilder.True<Product>();
            predicate = predicate.And(x => x.IsDelete == false);

            if (!string.IsNullOrEmpty(query.QueryText))
            {
                var queryString = query.QueryText.ToLower().Trim();
                var variants = await _variantRepository.ListAsync(x => !string.IsNullOrEmpty(x.SKUCode) && x.SKUCode.ToLower().Contains(queryString));
                if (variants != null && variants.Any())
                {
                    var productIds = variants.Select(x => x.ProductId);
                    predicate = predicate.And(x => productIds.Contains(x.Id));
                }
                else
                {
                    predicate = predicate.And(x => x.Name.ToLower().Trim().Contains(queryString));
                }
            }

            var products = await _productRepository.ListAsync(predicate, query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage);
            var totalProducts = await _productRepository.Count(predicate);
            var result = products.Select(x => new StockAndPriceProductModel()
            {
                ProductId = x.Id,
                ProductName = x.Name
            });
            return new PagedResult<StockAndPriceProductModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalProducts,
                Data = result
            };
        }
    }
}
