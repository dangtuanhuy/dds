﻿using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harvey.PIM.Application.Extensions;

namespace Harvey.PIM.Application.Infrastructure.Queries.StockAndPrice
{
    public sealed class GetStockAndPriceStoreByProductQuery : IQuery<StockAndPriceItemModel>
    {
        public Guid ProductId { get; set; }
        public string QueryText { get; set; }
        public GetStockAndPriceStoreByProductQuery(Guid productId, string queryText)
        {
            ProductId = productId;
            QueryText = queryText;
        }
    }

    public sealed class GetStockAndPriceStoreByProductQueryHandler : IQueryHandler<GetStockAndPriceStoreByProductQuery, StockAndPriceItemModel>
    {
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<PimDbContext, Product> _productRepository;
        private readonly IEfRepository<PimDbContext, Variant> _variantRepository;
        private readonly IEfRepository<PimDbContext, Location> _locationRepository;
        private readonly IEfRepository<TransactionDbContext, StockTransaction> _stockTransactionRepository;
        private readonly IEfRepository<PimDbContext, Category> _categoryRepository;
        public GetStockAndPriceStoreByProductQueryHandler(
            PimDbContext pimDbContext,
            IEfRepository<PimDbContext, Product> productRepository,
            IEfRepository<PimDbContext, Variant> variantRepository,
            IEfRepository<PimDbContext, Location> locationRepository,
            IEfRepository<TransactionDbContext, StockTransaction> stockTransactionRepository,
            IEfRepository<PimDbContext, Category> categoryRepository)
        {
            _pimDbContext = pimDbContext;
            _productRepository = productRepository;
            _variantRepository = variantRepository;
            _locationRepository = locationRepository;
            _stockTransactionRepository = stockTransactionRepository;
            _categoryRepository = categoryRepository;
        }
        public async Task<StockAndPriceItemModel> Handle(GetStockAndPriceStoreByProductQuery query)
        {
            var product = await _productRepository.GetByIdAsync(query.ProductId);
            if (product == null)
            {
                return null;
            }

            var categoryId = Guid.Parse(product.CategoryId.ToString());
            var category = await _categoryRepository.GetByIdAsync(categoryId);
            if (category == null)
            {
                return null;
            }

            var variants = await _variantRepository.ListAsync(x => x.ProductId == query.ProductId);
            if (variants == null)
            {
                return null;
            }
            if (!string.IsNullOrEmpty(query.QueryText))
            {
                var variantsSKU = variants.Where(x => !string.IsNullOrEmpty(x.SKUCode) && x.SKUCode.ToLower().Contains(query.QueryText.ToLower().Trim()));
                if (variantsSKU != null && variantsSKU.Any())
                {
                    variants = variantsSKU;
                }
            }

            var variantIds = variants.Select(x => x.Id);

            var locations = await _locationRepository.GetAsync();

            var storeIds = locations.Where(x => x.Type == LocationType.Store).Select(x => x.Id);

            var warehouseIds = locations.Where(x => x.Type == LocationType.Warehouse).Select(x => x.Id);

            var storeStockTransactions = await _stockTransactionRepository.ListAsync(x => variantIds.Contains(x.VariantId));

            var data = from st in storeStockTransactions
                    group st by new { st.VariantId, st.LocationId } into groupSt
                    let latestData = groupSt.OrderByDescending(x => x.CreatedDate).FirstOrDefault()
                    join location in locations on latestData.LocationId equals location.Id
                    join variant in variants on latestData.VariantId equals variant.Id
                    join price in _pimDbContext.Prices on variant.PriceId equals price.Id
                    select new
                    {
                        variant.SKUCode,
                        VariantId = variant.Id,
                        VariantName = variant.Name,
                        CategoryName = category.Name,
                        LocationId = location.Id,
                        LocationName = location.Name,
                        price.ListPrice,
                        price.MemberPrice,
                        price.StaffPrice,
                        Quantity = latestData.Balance,
                        ReferenceId = $"{latestData.VariantId}{latestData.LocationId}"
                    };
            var storeItems = from d in data
                             where storeIds.Contains(d.LocationId)
                             select new StockAndPriceStoreItemModel()
                             {
                                 SKUCode = d.SKUCode,
                                 VariantName = d.VariantName,
                                 CategoryName = category.Name,
                                 LocationName = d.LocationName,
                                 ListPrice = d.ListPrice,
                                 MemberPrice = d.MemberPrice,
                                 StaffPrice = d.StaffPrice,
                                 Quantity = d.Quantity,
                                 ReferenceId = $"{d.VariantId}{d.LocationId}"
                             };

            var warehoueItems = from d in data
                             where warehouseIds.Contains(d.LocationId)
                             select new StockAndPriceWarehouseItemModel()
                             {
                                 SKUCode = d.SKUCode,
                                 VariantName = d.VariantName,
                                 LocationName = d.LocationName,
                                 CategoryName = category.Name,
                                 Quantity = d.Quantity,
                                 ReferenceId = $"{d.VariantId}{d.LocationId}"
                             };

            return new StockAndPriceItemModel()
            {
                ProductId = product.Id,
                ProductName = product.Name,
                StoreItems = storeItems,
                WarehouseItems = warehoueItems
            };
        }
    }
}
