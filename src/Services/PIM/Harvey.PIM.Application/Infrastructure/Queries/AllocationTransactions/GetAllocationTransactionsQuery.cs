﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.AllocationTransactions
{
    public sealed class GetAllocationTransactionsQuery : IQuery<PagedResult<AllocationTransactionModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string StatusIdString { get; }
        public string ToLocationIdString { get; }
        public string FromLocationIdString { get; }
        public string QueryString { get; }
        public GetAllocationTransactionsQuery(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLcationIdString, string fromLocationIdString, string queryString)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            StatusIdString = statusIdString;
            ToLocationIdString = toLcationIdString;
            FromLocationIdString = fromLocationIdString;
            QueryString = queryString;
        }
    }

    public sealed class GetAllocationTransactionsQueryHandler : IQueryHandler<GetAllocationTransactionsQuery, PagedResult<AllocationTransactionModel>>
    {
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _repository;
        private readonly IMapper _mapper;
        public GetAllocationTransactionsQueryHandler(IEfRepository<TransactionDbContext, AllocationTransaction> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<PagedResult<AllocationTransactionModel>> Handle(GetAllocationTransactionsQuery query)
        {
            var statusIds = string.IsNullOrEmpty(query.StatusIdString) ? null : query.StatusIdString.Split("|");
            var toLocationIds = string.IsNullOrEmpty(query.ToLocationIdString) ? null : query.ToLocationIdString.Split("|");
            var fromLocationIds = string.IsNullOrEmpty(query.FromLocationIdString) ? null : query.FromLocationIdString.Split("|");

            var predicate = PredicateBuilder.True<AllocationTransaction>();
            if (!string.IsNullOrEmpty(query.QueryString))
            {
                predicate = predicate.And(x => x.Name.ToUpper().Contains(query.QueryString.ToUpper()));
            }
            if (!string.IsNullOrEmpty(query.StatusIdString))
            {
                predicate = predicate.And(x => statusIds.Contains(x.Status.ToString()));
            }
            if (!string.IsNullOrEmpty(query.ToLocationIdString))
            {
                predicate = predicate.And(x => toLocationIds.Contains(x.ToLocationId.ToString()));
            }
            if (!string.IsNullOrEmpty(query.FromLocationIdString))
            {
                predicate = predicate.And(x => fromLocationIds.Contains(x.FromLocationId.ToString()));
            }

            var result =await _repository.ListAsync(predicate, query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage, query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);

            var allocationTransactions = _mapper.Map<List<AllocationTransactionModel>>(result);
            var totalPages = await _repository.Count(predicate);
            return new PagedResult<AllocationTransactionModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = allocationTransactions
            };
        }
    }
}
