﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.AllocationTransactions
{
    public sealed class GetAllocationTransactionByIdQuery : IQuery<AllocationTransactionModel>
    {
        public Guid Id { get; }
        public GetAllocationTransactionByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetAllocationTransactionByIdQueryHandler : IQueryHandler<GetAllocationTransactionByIdQuery, AllocationTransactionModel>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction, AllocationTransactionModel> _repository;
        public GetAllocationTransactionByIdQueryHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction, AllocationTransactionModel> repository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
        }
        public async Task<AllocationTransactionModel> Handle(GetAllocationTransactionByIdQuery query)
        {
            var allocationTransaction = await _repository.GetByIdAsync(query.Id);
            var allocationTransactionDetails = _transactionDbContext.AllocationTransactionDetails.Where(x => x.AllocationTransactionId == query.Id);

            var result = new AllocationTransactionModel
            {
                Id = allocationTransaction.Id,
                Name = allocationTransaction.Name,
                Description = allocationTransaction.Description,
                FromLocationId = allocationTransaction.FromLocationId,
                ToLocationId = allocationTransaction.ToLocationId,
                DeliveryDate = allocationTransaction.DeliveryDate,
                Status = allocationTransaction.Status,
                CreatedBy = allocationTransaction.CreatedBy,
                CreatedDate = allocationTransaction.CreatedDate,
                UpdatedBy = allocationTransaction.UpdatedBy,
                UpdatedDate = allocationTransaction.UpdatedDate
            };

            foreach (var item in allocationTransactionDetails)
            {
                var allocationTransactionDetail = new AllocationTransactionDetailModel
                {
                    Id = item.Id,
                    Quantity = item.Quantity,
                    VariantId = item.VariantId,
                    StockTypeId = item.StockTypeId,
                    AllocationTransactionId = item.AllocationTransactionId
                };
                var variant = _pimDbContext.Variants.FirstOrDefault(x => x.Id == item.VariantId);
                if (variant != null)
                {
                    var product = _pimDbContext.Products.FirstOrDefault(x => x.Id == variant.ProductId);
                    allocationTransactionDetail.ProductId = variant.ProductId;
                    allocationTransactionDetail.ProductName = product.Name;
                }
                result.AllocationTransactionDetails.Add(allocationTransactionDetail);
            }

            return result;
        }
    }
}