﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.AllocationTransactions
{
    public class GetMassAllocationTransationQuery : IQuery<MassAllocationInfoModel>
    {
        public Guid InventoryTransactionId { get; }
        public GetMassAllocationTransationQuery(Guid inventoryTransactionId)
        {
            InventoryTransactionId = inventoryTransactionId;
        }
    }

    public class GetMassAllocationTransationQueryHandler : IQueryHandler<GetMassAllocationTransationQuery, MassAllocationInfoModel>
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<PimDbContext, Location> _locationRepository;
        private readonly IEfRepository<PimDbContext, Variant> _variantRepository;
        private readonly IEfRepository<PimDbContext, Product> _productRepository;
        private readonly IEfRepository<TransactionDbContext, StockType> _stockTypeRepository;
        private readonly IEfRepository<TransactionDbContext, StockRequest> _stockRequestRepository;
        private readonly IEfRepository<TransactionDbContext, StockRequestItem> _stockRequestItemRepository;
        public GetMassAllocationTransationQueryHandler(IQueryExecutor queryExecutor,
                                                        TransactionDbContext transactionDbContext,
                                                        PimDbContext pimDbContext,
                                                        IEfRepository<PimDbContext, Location> locationRepository,
                                                        IEfRepository<PimDbContext, Variant> variantRepository,
                                                        IEfRepository<PimDbContext, Product> productRepository,
                                                        IEfRepository<TransactionDbContext, StockType> stockTypeRepository,
                                                        IEfRepository<TransactionDbContext, StockRequest> stockRequestRepository,
                                                        IEfRepository<TransactionDbContext, StockRequestItem> stockRequestItemRepository)
        {
            _queryExecutor = queryExecutor;
            _pimDbContext = pimDbContext;
            _locationRepository = locationRepository;
            _variantRepository = variantRepository;
            _productRepository = productRepository;
            _stockTypeRepository = stockTypeRepository;
            _transactionDbContext = transactionDbContext;
            _stockRequestItemRepository = stockRequestItemRepository;
            _stockRequestRepository = stockRequestRepository;
        }
        public async Task<MassAllocationInfoModel> Handle(GetMassAllocationTransationQuery query)
        {
            var massAllocations = new MassAllocationInfoModel();
            var locations = await _locationRepository.ListAsync(x => x.Type == LocationType.Store);
            var _locationIds = locations.Select(x => x.Id);
            foreach (var item in locations)
            {
                massAllocations.OutletInfor.Add(new OutletModel
                {
                    Id = item.Id,
                    Name = item.Name
                });
            }

            massAllocations.OutletInfor = massAllocations.OutletInfor.OrderBy(x => x.Name).ToList();
            var allocationTransactions = _transactionDbContext.AllocationTransactions.Where(x => x.TransactionRef == query.InventoryTransactionId.ToString()
                                                                                            && x.Status != AllocationTransactionStatus.Draft)
                                                                                            .Include(x => x.AllocationTransactionDetails).ToList();

            var allocationTransactionDetails = new List<AllocationTransactionDetail>();
            if (allocationTransactions.Count() > 0)
            {
                foreach (var allocationTransaction in allocationTransactions)
                {
                    allocationTransactionDetails.AddRange(allocationTransaction.AllocationTransactionDetails);
                }
            }

            var stockTypes = await _stockTypeRepository.GetAsync();
            var inventoryTransaction = _transactionDbContext.InventoryTransactions.FirstOrDefault(x => x.Id == query.InventoryTransactionId);
            var stockTransactionsByInventoryId = _transactionDbContext.StockTransactions.Where(x => x.InventoryTransactionId == inventoryTransaction.Id).ToList();
            var _variantIds = stockTransactionsByInventoryId.Select(x => x.VariantId).Distinct().ToList();
            var fieldValues = _pimDbContext.FieldValues.Where(x => _variantIds.Contains(x.EntityId)).Include(x => x.Field).ToList();
            var variants = await _variantRepository.ListAsync(x => _variantIds.Contains(x.Id));
            var _productIds = variants.Select(x => x.ProductId);
            var products = await _productRepository.ListAsync(x => _productIds.Contains(x.Id));
            var totalVariantsTransaction = allocationTransactionDetails.GroupBy(x => x.VariantId).Select(x => new { VariantId = x.Key, Total = x.Sum(y => y.Quantity) });
            var stockTransactions = _transactionDbContext.StockTransactions.AsNoTracking().Where(x => _variantIds.Contains(x.VariantId) && _locationIds.Contains(x.LocationId)).ToList();
            foreach (var variantId in _variantIds)
            {
                var productId = variants.FirstOrDefault(x => x.Id == variantId)?.ProductId;
                var product = products.FirstOrDefault(x => x.Id == productId);
                var balance = GetBalanceTransaction(variantId, stockTypes, inventoryTransaction.ToLocationId, stockTransactions);
                var totalVariantTransaction = totalVariantsTransaction.FirstOrDefault(x => x.VariantId == variantId);
                var massAllocation = new MassAllocationViewModel()
                {
                    VariantId = variantId,
                    ProductName = product.Name,
                    SKU = variants.FirstOrDefault(x => x.Id == variantId).SKUCode,
                    GIWQuantity = 0,
                    Balance = balance,
                    VariantFields = fieldValues.Where(x => x.EntityId == variantId)
                                                .Select(x => new VariantField()
                                                {
                                                    Name = x.Field.Name,
                                                    Value = FieldValueFactory.GetFieldValueFromFieldType(x.Field.Type, x)
                                                }).ToList()
                };
                foreach (var allocationProduct in stockTransactionsByInventoryId)
                {
                    if (allocationProduct.VariantId == variantId)
                    {
                        massAllocation.GIWQuantity = massAllocation.GIWQuantity + allocationProduct.Quantity;
                    }
                }

                massAllocation.RemainingBalance = massAllocation.GIWQuantity - (totalVariantTransaction != null ? totalVariantTransaction.Total : 0);

                foreach (var location in locations)
                {

                    var outletQuantity = new OutletQuantityModel
                    {
                        Id = location.Id,
                        QuantityOnHand = GetBalanceTransaction(variantId, stockTypes, location.Id, stockTransactions),
                        Quantity = 0,
                        QuantitySold = 0,
                    };

                    massAllocation.Outlets.Add(outletQuantity);
                }

                massAllocations.MassAllocations.Add(massAllocation);
            }

            return massAllocations;
        }
        private int GetBalanceTransaction(Guid variantId, IEnumerable<StockType> stockTypes, Guid toLocationId, List<StockTransaction> stockTransactions)
        {
            var balance = 0;
            foreach (var stockType in stockTypes)
            {
                var stockTransaction = stockTransactions
                                    .Where(x => x.VariantId == variantId && x.StockTypeId == stockType.Id && x.LocationId == toLocationId)
                                    .OrderByDescending(x => x.CreatedDate)
                                    .FirstOrDefault();

                balance += (stockTransaction == null ? 0 : stockTransaction.Balance);
            }

            return balance;
        }
    }
}
