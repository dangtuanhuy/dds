﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.AllocationTransactions
{
    public class GetMassAllocationByDate : IQuery<List<OutletProductModel>>
    {
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public Guid FromLocationId { get; }
        public GetMassAllocationByDate(DateTime fromDate, DateTime toDate, Guid fromLocationId)
        {
            FromDate = fromDate;
            ToDate = toDate;
            FromLocationId = fromLocationId;
        }
    }

    public class GetMassAllocationByDateHandler : IQueryHandler<GetMassAllocationByDate, List<OutletProductModel>>
    {
        private readonly IEfRepository<PimDbContext, Location> _locationRepository;
        private readonly IEfRepository<TransactionDbContext, StockRequest> _stockRequestRepository;
        private readonly IEfRepository<TransactionDbContext, StockRequestItem> _stockRequestItemRepository;
        private readonly IEfRepository<TransactionDbContext, StockTransaction> _stockTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, TransactionType> _transactionTypeRepository;
        public GetMassAllocationByDateHandler(IEfRepository<PimDbContext, Location> locationRepository,
                                              IEfRepository<TransactionDbContext, StockRequest> stockRequestRepository,
                                              IEfRepository<TransactionDbContext, StockRequestItem> stockRequestItemRepository,
                                              IEfRepository<TransactionDbContext, StockTransaction> stockTransactionRepository,
                                              IEfRepository<TransactionDbContext, TransactionType> transactionTypeRepository)
        {
            _locationRepository = locationRepository;
            _stockRequestRepository = stockRequestRepository;
            _stockRequestItemRepository = stockRequestItemRepository;
            _stockTransactionRepository = stockTransactionRepository;
            _transactionTypeRepository = transactionTypeRepository;
        }
        public async Task<List<OutletProductModel>> Handle(GetMassAllocationByDate query)
        {
            var stockTransactions = new List<StockTransaction>();
            var outletProduct = new List<OutletProductModel>();
            var transactionType = await _transactionTypeRepository.ListAsync(x => x.Code == "Sale");
            var transactionTypeId = transactionType.Count() > 0 ? transactionType.FirstOrDefault().Id : Guid.Empty;
            var stoctRequests = await _stockRequestRepository.ListAsync(x => (x.DateRequest.Date >= query.FromDate.Date && x.DateRequest.Date <= query.ToDate.Date)
                                                                              && (x.FromLocationId == query.FromLocationId)
                                                                              && (x.StockRequestStatus == StockRequestStatus.Opened || x.StockRequestStatus == StockRequestStatus.PartialAllocated));
            foreach (var stockRequest in stoctRequests)
            {
                var stockRequestItems = await _stockRequestItemRepository.ListAsync(x => x.StockRequestId == stockRequest.Id);
                if (stockRequestItems != null)
                {
                    foreach (var stockRequestItem in stockRequestItems)
                    {
                        outletProduct.Add(new OutletProductModel()
                        {
                            OutletId = stockRequest.ToLocationId,
                            VariantId = stockRequestItem.VariantId,
                            Quantity = stockRequestItem.Quantity - stockRequestItem.AllocateQuantity > 0 ? stockRequestItem.Quantity - stockRequestItem.AllocateQuantity
                                                                                                         : 0,
                            QuantitySold = 0
                        });
                    }
                }
            }
            var stores = await _locationRepository.ListAsync(x => x.Type == LocationType.Store);
            foreach(var store in stores)
            {
                var stockTransaction = await _stockTransactionRepository.ListAsync(x => (x.TransactionTypeId == transactionTypeId
                                                                                      && x.CreatedDate.Date >= query.FromDate.Date 
                                                                                      && x.CreatedDate.Date <= query.ToDate.Date 
                                                                                      && x.LocationId == store.Id));
                stockTransactions.AddRange(stockTransaction);
            }

            foreach (var stockTransaction in stockTransactions)
            {
                outletProduct.Add(new OutletProductModel()
                {
                    OutletId = stockTransaction.LocationId,
                    VariantId = stockTransaction.VariantId,
                    Quantity = 0,
                    QuantitySold = stockTransaction.Quantity > 0 ? stockTransaction.Quantity: 0
                });
            }
            var result = outletProduct.GroupBy(x => new { x.OutletId, x.VariantId })
                                      .Select(group => new OutletProductModel()
                                                {
                                                    VariantId = group.Key.VariantId,
                                                    OutletId = group.Key.OutletId,
                                                    Quantity = group.Sum(y => y.Quantity),
                                                    QuantitySold = group.Sum(y => y.QuantitySold)
                                                });
            return result.ToList();
        }
    }
}
