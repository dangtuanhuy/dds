﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.AllocationTransactions
{
    public sealed class GetAllocationTransactionByListIdQuery : ICommand<AllocationTransactionByListIdModel>
    {
        public AllocationTransactionByListIdModel AllocationTransactionByListIdModel { get; set; }
        public GetAllocationTransactionByListIdQuery(AllocationTransactionByListIdModel allocationTransactionByListIdModel)
        {
            AllocationTransactionByListIdModel = allocationTransactionByListIdModel;
        }
    }

    public sealed class GetAllocationTransactionByListIdQueryHandler : ICommandHandler<GetAllocationTransactionByListIdQuery, AllocationTransactionByListIdModel>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _repository;
        public GetAllocationTransactionByListIdQueryHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction> repository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
        }
        public async Task<AllocationTransactionByListIdModel> Handle(GetAllocationTransactionByListIdQuery query)
        {
            var transactionTypeGIW = _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == TransactionTypeCode.GIW.ToString()).FirstOrDefault();
            var transactionTypeTFO = _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == TransactionTypeCode.TFO.ToString()).FirstOrDefault();
            var stockTransactions = await _transactionDbContext.StockTransactions.AsNoTracking().ToListAsync();
            var result = new AllocationTransactionByListIdModel();
            var addingAllocationTransactionDetailModels = new List<AllocationTransactionDetailModel>();
            
            foreach (var allocationTransactionId in query.AllocationTransactionByListIdModel.AllocationTransactionListId)
            {
                var allocationTransaction = await _repository.GetByIdAsync(allocationTransactionId);
                var allocationTransactionModel = new AllocationTransactionModel
                {
                    Id = allocationTransaction.Id,
                    Name = allocationTransaction.Name,
                    Description = allocationTransaction.Description,
                    FromLocationId = allocationTransaction.FromLocationId,
                    ToLocationId = allocationTransaction.ToLocationId,
                    DeliveryDate = allocationTransaction.DeliveryDate,
                    Status = allocationTransaction.Status,
                    CreatedBy = allocationTransaction.CreatedBy,
                    CreatedDate = allocationTransaction.CreatedDate,
                    UpdatedBy = allocationTransaction.UpdatedBy,
                    UpdatedDate = allocationTransaction.UpdatedDate
                };

                var allocationTransactionDetails = await _transactionDbContext.AllocationTransactionDetails
                                                                        .Where(x => x.AllocationTransactionId == allocationTransaction.Id).ToListAsync();
                foreach (var item in allocationTransactionDetails)
                {
                    var lastTransferStockTransactionBalance = await GetLastStockTransactionBalance(item.VariantId, allocationTransaction.FromLocationId);
                    var lastTransferStockTransactionAllocationDetail = stockTransactions.Where(x => x.VariantId == item.VariantId
                                                                                            && x.StockTransactionRefId == item.Id)
                                                                                        .ToList();

                    var allocationTransactionDetail = new AllocationTransactionDetailModel
                    {
                        Id = item.Id,
                        Quantity = (item.Quantity - lastTransferStockTransactionAllocationDetail.Sum(x => x.Quantity)) >= lastTransferStockTransactionBalance
                                    ? lastTransferStockTransactionBalance
                                    : (item.Quantity - lastTransferStockTransactionAllocationDetail.Sum(x => x.Quantity)),
                        VariantId = item.VariantId,
                        StockTypeId = Guid.Empty,
                        AllocationTransactionId = item.AllocationTransactionId,
                        StockOnHand = lastTransferStockTransactionBalance
                    };
                    var variant = _pimDbContext.Variants.FirstOrDefault(x => x.Id == item.VariantId);
                    if (variant != null)
                    {
                        allocationTransactionDetail.ProductId = variant.ProductId;
                    }
                    addingAllocationTransactionDetailModels.Add(allocationTransactionDetail);
                    allocationTransactionModel.AllocationTransactionDetails.Add(allocationTransactionDetail);
                }
                result.AllocationTransactionListId.Add(allocationTransactionId);
                result.AllocationTransactions.Add(allocationTransactionModel);
            }
            
            return result;
        }

        public async Task<int> GetLastStockTransactionBalance(Guid variantId, Guid fromLocationId)
        {
            var stockTransactions =  _transactionDbContext.StockTransactions.Where(x => x.VariantId == variantId).ToList();
           
            var lastTransferStockTransaction = stockTransactions.OrderByDescending(x => x.CreatedDate)
                                                                .Where(x => x.LocationId == fromLocationId
                                                                    && x.VariantId == variantId).FirstOrDefault();

            if (lastTransferStockTransaction != null)
            {
                return lastTransferStockTransaction.Balance;
            }
            return 0;
        }
    }
}