﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.AllocationTransactions
{
    public sealed class GetAllocationTransactionsByLocationQuery : IQuery<PagedResult<AllocationTransactionModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string FromLocationIds { get; }
        public string ToLocationIds { get; }
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public GetAllocationTransactionsByLocationQuery(
            PagingFilterCriteria pagingFilterCriteria,
            string fromLocationIds,
            string toLocationIds,
            DateTime fromDate,
            DateTime toDate)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            FromLocationIds = fromLocationIds;
            ToLocationIds = toLocationIds;
            FromDate = fromDate;
            ToDate = toDate;
        }
    }

    public sealed class GetAllocationTransactionByLocationQueryHandler : IQueryHandler<GetAllocationTransactionsByLocationQuery, PagedResult<AllocationTransactionModel>>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _repository;
        private StockAllocationService _stockAllocationService;
        public GetAllocationTransactionByLocationQueryHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction> repository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext,
            StockAllocationService stockAllocationService)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _repository = repository;
            _stockAllocationService = stockAllocationService;
        }
        public async Task<PagedResult<AllocationTransactionModel>> Handle(GetAllocationTransactionsByLocationQuery query)
        {
            var fromLocationIds = _stockAllocationService.ParseStringToGuids(query.FromLocationIds);
            var toLocationIds = _stockAllocationService.ParseStringToGuids(query.ToLocationIds);
            DateTime FromDate = new DateTime(query.FromDate.Year, query.FromDate.Month, query.FromDate.Day, 00, 00, 00, 00);
            DateTime ToDate = new DateTime(query.ToDate.Year, query.ToDate.Month, query.ToDate.Day, 23, 59, 59, 100);
            var transactionTypeGIW = _transactionDbContext.TransactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.GIW.ToString());
            var transactionTypeTFO = _transactionDbContext.TransactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.TFO.ToString());

            var allocationTransactions = await _repository.ListAsync(x => fromLocationIds.Contains(x.FromLocationId)
                                                                      && toLocationIds.Contains(x.ToLocationId)
                                                                      && (x.DeliveryDate >= FromDate)
                                                                      && (x.Status == AllocationTransactionStatus.Submitted || x.Status == AllocationTransactionStatus.PartialTransfer)
                                                                      && (x.DeliveryDate <= ToDate));

            var totalPages = allocationTransactions.Count();

            allocationTransactions = allocationTransactions.Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage).Take(query.PagingFilterCriteria.NumberItemsPerPage);

            var result = new List<AllocationTransactionModel>();

            foreach (var allocationTransaction in allocationTransactions)
            {
                var allocationTransactionModel = new AllocationTransactionModel
                {
                    Id = allocationTransaction.Id,
                    Name = allocationTransaction.Name,
                    Description = allocationTransaction.Description,
                    FromLocationId = allocationTransaction.FromLocationId,
                    ToLocationId = allocationTransaction.ToLocationId,
                    DeliveryDate = allocationTransaction.DeliveryDate,
                    Status = allocationTransaction.Status,
                    CreatedBy = allocationTransaction.CreatedBy,
                    CreatedDate = allocationTransaction.CreatedDate,
                    UpdatedBy = allocationTransaction.UpdatedBy,
                    UpdatedDate = allocationTransaction.UpdatedDate
                };
                result.Add(allocationTransactionModel);
            }

            return new PagedResult<AllocationTransactionModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = result
            };
        }
    }
}
