﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Reasons
{
    public sealed class GetReasonByIdQuery : IQuery<ReasonModel>
    {
        public Guid Id { get; }
        public GetReasonByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetReasonByIdQueryHandler : IQueryHandler<GetReasonByIdQuery, ReasonModel>
    {
        private readonly IEfRepository<TransactionDbContext, Reason, ReasonModel> _repository;
        public GetReasonByIdQueryHandler(IEfRepository<TransactionDbContext, Reason, ReasonModel> repository)
        {
            _repository = repository;
        }
        public async Task<ReasonModel> Handle(GetReasonByIdQuery query)
        {
            return await _repository.GetByIdAsync(query.Id);
        }
    }
}
