﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Queries.Reasons
{
    public sealed class GetReasonsQuery : IQuery<PagedResult<ReasonModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public GetReasonsQuery(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }
    public sealed class GetReasonsQueryHandler : IQueryHandler<GetReasonsQuery, PagedResult<ReasonModel>>
    {
        private readonly IEfRepository<TransactionDbContext, Reason> _repository;
        private readonly IMapper _mapper;

        public GetReasonsQueryHandler(IEfRepository<TransactionDbContext, Reason> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<PagedResult<ReasonModel>> Handle(GetReasonsQuery query)
        {
            var result = string.IsNullOrEmpty(query.QueryText) ? await _repository.GetAsync(query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                                                                                            query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection)
                                                               : await _repository.ListAsync(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper()),
                                                                                            query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage,
                                                                                            query.PagingFilterCriteria.SortColumn, query.PagingFilterCriteria.SortDirection);

            var reasons = _mapper.Map<List<ReasonModel>>(result);
            var totalPages = string.IsNullOrEmpty(query.QueryText) ? await _repository.Count()
                                                                   : await _repository.Count(x => x.Name.ToUpper().Contains(query.QueryText.ToUpper()));
            return new PagedResult<ReasonModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = reasons
            };
        }
    }
}
