﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class renamegiwdocumentidtotransactionrefid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InventoryTransactions_GIWDocuments_GIWDocumentId",
                table: "InventoryTransactions");

            migrationBuilder.DropIndex(
                name: "IX_InventoryTransactions_GIWDocumentId",
                table: "InventoryTransactions");

            migrationBuilder.RenameColumn(
                name: "GIWDocumentId",
                table: "InventoryTransactions",
                newName: "TransactionRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TransactionRefId",
                table: "InventoryTransactions",
                newName: "GIWDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryTransactions_GIWDocumentId",
                table: "InventoryTransactions",
                column: "GIWDocumentId");

            migrationBuilder.AddForeignKey(
                name: "FK_InventoryTransactions_GIWDocuments_GIWDocumentId",
                table: "InventoryTransactions",
                column: "GIWDocumentId",
                principalTable: "GIWDocuments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
