﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class add_allocation_transaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AllocationTransactions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    FromLocationId = table.Column<Guid>(nullable: false),
                    ToLocationId = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    UpdatedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeliveryDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllocationTransactions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AllocationTransactionDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AllocationTransactionId = table.Column<Guid>(nullable: false),
                    StockTypeId = table.Column<Guid>(nullable: false),
                    VariantId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    UpdatedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllocationTransactionDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AllocationTransactionDetails_AllocationTransactions_Allocat~",
                        column: x => x.AllocationTransactionId,
                        principalTable: "AllocationTransactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AllocationTransactionDetails_StockTypes_StockTypeId",
                        column: x => x.StockTypeId,
                        principalTable: "StockTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AllocationTransactionDetails_AllocationTransactionId",
                table: "AllocationTransactionDetails",
                column: "AllocationTransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_AllocationTransactionDetails_StockTypeId",
                table: "AllocationTransactionDetails",
                column: "StockTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AllocationTransactionDetails");

            migrationBuilder.DropTable(
                name: "AllocationTransactions");
        }
    }
}
