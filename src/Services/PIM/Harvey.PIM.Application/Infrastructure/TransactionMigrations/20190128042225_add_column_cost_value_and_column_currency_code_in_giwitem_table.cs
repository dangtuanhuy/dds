﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class add_column_cost_value_and_column_currency_code_in_giwitem_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "CostValue",
                table: "GIWDocumentItems",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<string>(
                name: "CurrencyCode",
                table: "GIWDocumentItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CostValue",
                table: "GIWDocumentItems");

            migrationBuilder.DropColumn(
                name: "CurrencyCode",
                table: "GIWDocumentItems");
        }
    }
}
