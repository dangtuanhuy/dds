﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class mergestocklocationIdcolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FromLocationId",
                table: "StockTransactions");

            migrationBuilder.RenameColumn(
                name: "ToLocationId",
                table: "StockTransactions",
                newName: "LocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LocationId",
                table: "StockTransactions",
                newName: "ToLocationId");

            migrationBuilder.AddColumn<Guid>(
                name: "FromLocationId",
                table: "StockTransactions",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
