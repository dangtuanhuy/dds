﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class add_goods_return_and_goods_return_items_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GoodsReturnDocuments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    UpdatedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoodsReturnDocuments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GoodsReturnDocumentItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GoodsReturnDocumentId = table.Column<Guid>(nullable: false),
                    VariantId = table.Column<Guid>(nullable: false),
                    StockTypeId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    CostValue = table.Column<float>(nullable: false),
                    CurrencyCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoodsReturnDocumentItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoodsReturnDocumentItems_GoodsReturnDocuments_GoodsReturnDo~",
                        column: x => x.GoodsReturnDocumentId,
                        principalTable: "GoodsReturnDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoodsReturnDocumentItems_StockTypes_StockTypeId",
                        column: x => x.StockTypeId,
                        principalTable: "StockTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GoodsReturnDocumentItems_GoodsReturnDocumentId",
                table: "GoodsReturnDocumentItems",
                column: "GoodsReturnDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_GoodsReturnDocumentItems_StockTypeId",
                table: "GoodsReturnDocumentItems",
                column: "StockTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GoodsReturnDocumentItems");

            migrationBuilder.DropTable(
                name: "GoodsReturnDocuments");
        }
    }
}
