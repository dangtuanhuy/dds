﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class addtransactionrefcolumningoodsreturndocumentitemtableandgoodinwarddocumentitemtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TransactionRefId",
                table: "GIWDocumentItems",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TransactionRefId",
                table: "GoodsReturnDocumentItems",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TransactionRefId",
                table: "GIWDocumentItems");

            migrationBuilder.DropColumn(
                name: "TransactionRefId",
                table: "GoodsReturnDocumentItems");
        }
    }
}
