﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class add_transaction_relationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "InventoryTransactionId",
                table: "StockTransactions",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_StockTransactions_InventoryTransactionId",
                table: "StockTransactions",
                column: "InventoryTransactionId");

            migrationBuilder.AddForeignKey(
                name: "FK_StockTransactions_InventoryTransactions_InventoryTransactio~",
                table: "StockTransactions",
                column: "InventoryTransactionId",
                principalTable: "InventoryTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StockTransactions_InventoryTransactions_InventoryTransactio~",
                table: "StockTransactions");

            migrationBuilder.DropIndex(
                name: "IX_StockTransactions_InventoryTransactionId",
                table: "StockTransactions");

            migrationBuilder.DropColumn(
                name: "InventoryTransactionId",
                table: "StockTransactions");
        }
    }
}
