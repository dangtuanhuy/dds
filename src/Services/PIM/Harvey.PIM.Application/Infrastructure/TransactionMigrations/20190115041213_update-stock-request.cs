﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class updatestockrequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LocationId",
                table: "StockRequests",
                newName: "ToLocationId");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateRequest",
                table: "StockRequests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "FromLocationId",
                table: "StockRequests",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateRequest",
                table: "StockRequests");

            migrationBuilder.DropColumn(
                name: "FromLocationId",
                table: "StockRequests");

            migrationBuilder.RenameColumn(
                name: "ToLocationId",
                table: "StockRequests",
                newName: "LocationId");
        }
    }
}
