﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.PIM.Application.Infrastructure.TransactionMigrations
{
    public partial class removevalidateallocationtransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AllocationTransactionDetails_StockTypes_StockTypeId",
                table: "AllocationTransactionDetails");

            migrationBuilder.DropIndex(
                name: "IX_AllocationTransactionDetails_StockTypeId",
                table: "AllocationTransactionDetails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_AllocationTransactionDetails_StockTypeId",
                table: "AllocationTransactionDetails",
                column: "StockTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_AllocationTransactionDetails_StockTypes_StockTypeId",
                table: "AllocationTransactionDetails",
                column: "StockTypeId",
                principalTable: "StockTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
