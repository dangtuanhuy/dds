﻿using Harvey.EventBus;
using System;

namespace Harvey.PIM.Application.Infrastructure.Publishers
{
    public class SynchronizationPublisher : IPublisher
    {
        public string Name => "channel_catalog_synchronization_operation";

        public Guid? CorrelationId { get; set; }
    }
}
