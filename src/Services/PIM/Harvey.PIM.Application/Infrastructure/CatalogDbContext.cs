﻿using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure
{
    public class CatalogDbContext : DbContext
    {
        public DbSet<CatalogCategory> Categories { get; set; }
        public DbSet<CatalogProduct> Products { get; set; }
        public DbSet<CatalogBarCode> BarCodes { get; set; }
        public DbSet<CatalogVariant> Variants { get; set; }
        public DbSet<CatalogFieldValue> FieldValues { get; set; }
        public DbSet<CatalogPrice> Prices { get; set; }
        public DbSet<Synchronization> Synchronizations { get; set; }
        public DbSet<CatalogStockType> StockTypes { get; set; }
        public DbSet<CatalogStockTransaction> StockTransactions { get; set; }
        public DbSet<CatalogTransactionType> TransactionTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderPromotion> OrderPromotions { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderItemPromotion> OrderItemPromotions { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }

        public DbSet<CatalogPromotion> Promotions { get; set; }
        public DbSet<CatalogPromotionDetail> PromotionDetails { get; set; }
        public DbSet<CatalogPromotionCouponCode> PromotionCouponCodes { get; set; }
        public DbSet<CatalogPromotionCondition> PromotionConditions { get; set; }
        public DbSet<CatalogPromotionLocation> PromotionLocations { get; set; }
        public DbSet<CatalogPaymentMethod> PaymentMethods { get; set; }
        public DbSet<TimeToFeed> TimeToFeeds { get; set; }

        public async Task AddSynchronization(SynchAction synchAction, SynchEntity entity, Guid refId)
        {
            var sync = Synchronizations.FirstOrDefault(x => x.Entity == entity && x.RefId == refId);
            if (sync != null)
            {
                switch (synchAction)
                {
                    case SynchAction.Add:
                        sync.SynchAction = SynchAction.Add;
                        sync.ModifiedDate = DateTime.UtcNow;
                        await SaveChangesAsync();
                        break;
                    case SynchAction.Update:
                        sync.SynchAction = SynchAction.Update;
                        sync.ModifiedDate = DateTime.UtcNow;
                        await SaveChangesAsync();
                        break;
                    case SynchAction.Delete:
                        sync.SynchAction = SynchAction.Delete;
                        sync.ModifiedDate = DateTime.UtcNow;
                        break;
                }
            }
            else
            {
                switch (synchAction)
                {
                    case SynchAction.Add:
                        Synchronizations.Add(new Synchronization()
                        {
                            SynchAction = SynchAction.Add,
                            Entity = entity,
                            RefId = refId,
                            ModifiedDate = DateTime.UtcNow
                        });
                        break;
                }
            }

        }

        public CatalogDbContext(DbContextOptions<CatalogDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<CatalogCategory>());
            Setup(modelBuilder.Entity<CatalogProduct>());
            Setup(modelBuilder.Entity<CatalogVariant>());
            Setup(modelBuilder.Entity<CatalogBarCode>());
            Setup(modelBuilder.Entity<CatalogPrice>());
            Setup(modelBuilder.Entity<Synchronization>());
            Setup(modelBuilder.Entity<Order>());
            Setup(modelBuilder.Entity<OrderPromotion>());
            Setup(modelBuilder.Entity<OrderItem>());
            Setup(modelBuilder.Entity<OrderItemPromotion>());
            Setup(modelBuilder.Entity<Payment>());
            Setup(modelBuilder.Entity<Delivery>());
            Setup(modelBuilder.Entity<CatalogPromotion>());
            Setup(modelBuilder.Entity<CatalogPromotionDetail>());
            Setup(modelBuilder.Entity<CatalogPromotionCouponCode>());
            Setup(modelBuilder.Entity<CatalogPromotionCondition>());
            Setup(modelBuilder.Entity<CatalogPromotionLocation>());
            Setup(modelBuilder.Entity<CatalogPaymentMethod>());
            Setup(modelBuilder.Entity<TimeToFeed>());
        }
        private void Setup(EntityTypeBuilder<TimeToFeed> entityConfig)
        {
            entityConfig.ToTable("TimeToFeeds");
            entityConfig.HasKey(x => x.Id);
        }

        private void Setup(EntityTypeBuilder<CatalogCategory> entityConfig)
        {
            entityConfig.ToTable("Categories");
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        private void Setup(EntityTypeBuilder<CatalogProduct> entityConfig)
        {
            entityConfig.ToTable("Products");
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        private void Setup(EntityTypeBuilder<CatalogBarCode> entityConfig)
        {
            entityConfig.ToTable("BarCodes");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        private void Setup(EntityTypeBuilder<CatalogVariant> entityConfig)
        {
            entityConfig.ToTable("Variants");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        private void Setup(EntityTypeBuilder<CatalogFieldValue> entityConfig)
        {
            entityConfig.ToTable("FieldValues");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        private void Setup(EntityTypeBuilder<CatalogPrice> entityConfig)
        {
            entityConfig.ToTable("Prices");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        private void Setup(EntityTypeBuilder<CatalogStockType> entityConfig)
        {
            entityConfig.ToTable("StockTypes");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        private void Setup(EntityTypeBuilder<CatalogStockTransaction> entityConfig)
        {
            entityConfig.ToTable("StockTransactions");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        private void Setup(EntityTypeBuilder<CatalogTransactionType> entityConfig)
        {
            entityConfig.ToTable("TransactionTypes");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        private void Setup(EntityTypeBuilder<Synchronization> entityConfig)
        {
            entityConfig.ToTable("Synchronizations");
            entityConfig.HasKey(x => x.Id);
        }

        private void Setup(EntityTypeBuilder<Order> entityConfig)
        {
            entityConfig.ToTable("Orders");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

            entityConfig.HasMany(x => x.Items)
                .WithOne(x => x.Order)
                .HasForeignKey(x => x.OrderId);
        }

        private void Setup(EntityTypeBuilder<OrderPromotion> entityConfig)
        {
            entityConfig.ToTable("OrderPromotions");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        private void Setup(EntityTypeBuilder<OrderItem> entityConfig)
        {
            entityConfig.ToTable("OrderItems");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        private void Setup(EntityTypeBuilder<OrderItemPromotion> entityConfig)
        {
            entityConfig.ToTable("OrderItemPromotions");
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        private void Setup(EntityTypeBuilder<Payment> entityConfig)
        {
            entityConfig.ToTable("Payments");
            entityConfig.HasKey(x => x.Id);
        }

        private void Setup(EntityTypeBuilder<Delivery> entityConfig)
        {
            entityConfig.ToTable("Deliveries");
            entityConfig.HasKey(x => x.Id);
        }

        private void Setup(EntityTypeBuilder<CatalogPromotion> entityConfig)
        {
            entityConfig.ToTable("Promotions");
            entityConfig.HasKey(x => x.Id);
        }

        private void Setup(EntityTypeBuilder<CatalogPromotionDetail> entityConfig)
        {
            entityConfig.ToTable("PromotionDetails");
            entityConfig.HasKey(x => x.Id);
        }

        private void Setup(EntityTypeBuilder<CatalogPromotionCouponCode> entityConfig)
        {
            entityConfig.ToTable("PromotionCouponCodes");
            entityConfig.HasKey(x => x.Id);
        }

        private void Setup(EntityTypeBuilder<CatalogPromotionCondition> entityConfig)
        {
            entityConfig.ToTable("PromotionConditions");
            entityConfig.HasKey(x => x.Id);
        }

        private void Setup(EntityTypeBuilder<CatalogPromotionLocation> entityConfig)
        {
            entityConfig.ToTable("PromotionLocations");
            entityConfig.HasKey(x => new { x.PromotionId, x.LocationId });
            entityConfig.Ignore(x => x.Id);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        private void Setup(EntityTypeBuilder<CatalogPaymentMethod> entityConfig)
        {
            entityConfig.ToTable("PaymentMethods");
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }
    }
}
