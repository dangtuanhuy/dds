﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Reasons
{
    public class UpdateReasonCommand : ICommand<ReasonModel>
    {
        public Guid Updater { get; }
        public Guid Id { get; }
        public string Name { get; }
        public string Description { get; }
        public string Code { get; }
        public UpdateReasonCommand(Guid updater, Guid id, string name, string description, string code)
        {
            Updater = updater;
            Id = id;
            Name = name;
            Description = description;
            Code = code;
        }
    }
    public sealed class UpdateReasonCommandHandler : ICommandHandler<UpdateReasonCommand, ReasonModel>
    {
        private readonly IEfRepository<TransactionDbContext, Reason> _repository;
        private readonly IMapper _mapper;

        public UpdateReasonCommandHandler(IEfRepository<TransactionDbContext, Reason> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<ReasonModel> Handle(UpdateReasonCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Reason {command.Id} is not presented.");
            }
            entity.UpdatedBy = command.Updater;
            entity.Name = command.Name;
            entity.Description = command.Description;
            entity.Code = command.Code;
            await _repository.UpdateAsync(entity);

            return _mapper.Map<ReasonModel>(entity);
        }
    }
}
