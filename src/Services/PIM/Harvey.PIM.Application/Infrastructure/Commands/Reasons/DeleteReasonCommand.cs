﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Reasons
{
    public class DeleteReasonCommand : ICommand<bool>
    {
        public Guid Id { get; }
        public DeleteReasonCommand(Guid id)
        {
            Id = id;
        }
    }
    public sealed class DeleteReasonCommandHandler : ICommandHandler<DeleteReasonCommand, bool>
    {
        private readonly IEfRepository<TransactionDbContext, Reason> _repository;
        private readonly IMapper _mapper;

        public DeleteReasonCommandHandler(IEfRepository<TransactionDbContext, Reason> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<bool> Handle(DeleteReasonCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Reason {command.Id} is not presented.");
            }
            await _repository.DeleteAsync(entity);
            return true;
        }
    }
}
