﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Reasons
{
    public class AddReasonCommand : ICommand<ReasonModel>
    {
        public Guid Creator { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public AddReasonCommand(Guid creator, string name, string description,string code)
        {
            Creator = creator;
            Name = name;
            Description = description;
            Code = code;
        }
    }
    public sealed class AddReasonCommandHandler : ICommandHandler<AddReasonCommand, ReasonModel>
    {
        private readonly IEfRepository<TransactionDbContext, Reason> _repository;
        private readonly IMapper _mapper;
        public AddReasonCommandHandler(IEventBus eventBus, IEfRepository<TransactionDbContext, Reason> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<ReasonModel> Handle(AddReasonCommand command)
        {
            var reason = new Reason
            {
                UpdatedBy = command.Creator,
                CreatedBy = command.Creator,
                Name = command.Name,
                Description = command.Description,
                Code = command.Code
            };

            var entity = await _repository.AddAsync(reason);

            return _mapper.Map<ReasonModel>(entity);
        }
    }
}
