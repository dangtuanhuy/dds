﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Extensions;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using static Harvey.PIM.Application.Infrastructure.Models.AddProductModel;

namespace Harvey.PIM.Application.Infrastructure.Commands.Products
{
    public sealed class UpdateProductCommand : ICommand<Product>
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }
        public List<DetailFieldValueModel> ProductFields { get; }
        public Dictionary<AddVariantModel, List<DetailFieldValueModel>> VariantFields { get; }
        public Dictionary<Guid, PriceModel> VariantPrices { get; }
        public Dictionary<Guid, string> VariantCodes { get; }
        public string IndexingValue { get; set; }
        public Guid Creator { get; set; }

        public UpdateProductCommand(
           Guid creator,
           Guid id,
           Guid categoryId,
           string categoryName,
           string name,
           string description,
           bool isDelete,
           List<DetailFieldValueModel> productFields,
           Dictionary<AddVariantModel, List<DetailFieldValueModel>> variantFields,
           Dictionary<Guid, PriceModel> variantPrices,
           Dictionary<Guid, string> variantCodes,
           string indexingValue)
        {
            Creator = creator;
            Id = id;
            CategoryId = categoryId;
            CategoryName = categoryName;
            Name = name;
            Description = description;
            ProductFields = productFields;
            VariantFields = variantFields;
            VariantPrices = variantPrices;
            VariantCodes = variantCodes;
            IndexingValue = indexingValue;
            IsDelete = isDelete;
        }
    }

    public sealed class UpdateProductCommandHandler : ICommandHandler<UpdateProductCommand, Product>
    {
        private readonly IEventStoreRepository<Product> _eventStoreRepository;
        private readonly IEfRepository<PimDbContext, Product> _repository;
        public UpdateProductCommandHandler(
            IEventStoreRepository<Product> eventStoreRepository,
            IEfRepository<PimDbContext, Product> repository)
        {
            _eventStoreRepository = eventStoreRepository;
            _repository = repository;
        }
        public async Task<Product> Handle(UpdateProductCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            var product = new Product()
            {
                Id = command.Id,
                Name = command.Name,
                Description = command.Description,
                CreatedDate = entity != null ? entity.CreatedDate : new DateTime(),
                CreatedBy = entity != null ? entity.CreatedBy : Guid.Empty,
                UpdatedDate = DateTime.UtcNow,
                UpdatedBy = command.Creator
            };

            product.UpdateProduct(command.Creator, command.Id, command.CategoryId,command.CategoryName, command.Name,
                command.Description, command.IsDelete, command.ProductFields, command.IndexingValue,
                product.CreatedDate, product.CreatedBy, product.UpdatedDate, product.UpdatedBy);

            foreach (var item in command.VariantFields)
            {
                switch (item.Key.Action)
                {
                    case ItemActionType.Add:
                        var variantName = VariantExtensions.GetVariantName(command.VariantFields[item.Key]);
                        var variantDescription = VariantExtensions.GetVariantDescription(command.Name, command.VariantFields[item.Key]);
                        product.AddVariant(product.Id, item.Key.Id, command.VariantFields[item.Key], command.VariantPrices[item.Key.Id], command.VariantCodes[item.Key.Id], variantName, variantDescription);
                        break;
                    case ItemActionType.Update:
                        var variantNameUpdate = VariantExtensions.GetVariantName(command.VariantFields[item.Key]);
                        var variantDescriptionUpdate = VariantExtensions.GetVariantDescription(command.Name, command.VariantFields[item.Key]);
                        product.UpdateVariant(product.Id, item.Key.Id, command.VariantFields[item.Key], command.VariantPrices[item.Key.Id], variantNameUpdate, variantDescriptionUpdate);
                        break;
                    case ItemActionType.Delete:
                        break;
                }
            }

            await _eventStoreRepository.SaveAsync(product);

            return product;
        }
    }
}
