﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Products
{
    public sealed class DeleteProductCommand : ICommand<bool>
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }
        public string IndexingValue { get; set; }
        public string Code { get; set; }
        public DeleteProductCommand (
           Guid id)
        {
            Id = id;
        }
    }

    public sealed class DeleteProductCommandHandler : ICommandHandler<DeleteProductCommand, bool>
    {
        private readonly IEventStoreRepository<Product> _eventStoreRepository;
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        public DeleteProductCommandHandler(IEventStoreRepository<Product> eventStoreRepository,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext)
        {
            _eventStoreRepository = eventStoreRepository;
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
    }
        public async Task<bool> Handle(DeleteProductCommand command)
        {
            var product = _pimDbContext.Products.Include(x => x.Category).SingleOrDefault(x => x.Id == command.Id);
            if (product == null)
            {
                throw new ArgumentException($"product {command.Id} is not presented.");
            }
            var variantIds = _transactionDbContext.StockTransactions.Select(x => x.VariantId).ToList();
            var invalidProduct = _pimDbContext.Variants.Where(x => variantIds.Contains(x.Id)).Any(x => x.ProductId == command.Id);
            if (invalidProduct)
            {
                throw new ArgumentException("Cannot delete product!");
            }
            var categoryId = product.Category != null ? product.Category.Id : Guid.Empty;
            var categoryName = product.Category != null ? product.Category.Name : "";
            product.DeleteProduct(command.Id, categoryId, categoryName, product.Name, product.Description, true, command.IndexingValue, command.Code, DateTime.UtcNow);
            await _eventStoreRepository.SaveAsync(product);
            return true;
        }
    }
}
