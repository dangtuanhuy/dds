﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Extensions;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Harvey.PIM.Application.Infrastructure.Commands.Products
{
    public sealed class AddProductCommand : ICommand<Product>
    {
        public Guid Id { get; }
        public string Name { get; }
        public string Description { get; }
        public Guid FieldTemplateId { get; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public List<DetailFieldValueModel> ProductFields { get; }
        public Dictionary<Guid, List<DetailFieldValueModel>> VariantFields { get; }
        public Dictionary<Guid, PriceModel> VariantPrices { get; }
        public Dictionary<Guid, string> VariantCodes { get; set; }
        public string IndexingValue { get; }
        public string Code { get; }
        public Guid Creator { get; }
        public bool IsDelete { get; set; }

        public AddProductCommand(
            Guid creator,
            Guid id,
            Guid categoryId,
            string categoryName,
            string name,
            string description,
            bool isDelete,
            Guid fieldTemplateId,
            List<DetailFieldValueModel> productFields,
            Dictionary<Guid, List<DetailFieldValueModel>> variantFields,
            Dictionary<Guid, PriceModel> variantPrices,
            Dictionary<Guid, string> variantCodes,
            string indexingValue,
            string code
            )
        {
            Creator = creator;
            Id = id;
            Name = name;
            Description = description;
            FieldTemplateId = fieldTemplateId;
            ProductFields = productFields;
            VariantFields = variantFields;
            VariantPrices = variantPrices;
            IndexingValue = indexingValue;
            CategoryId = categoryId;
            CategoryName = categoryName;
            Code = code;
            IsDelete = isDelete;
            VariantCodes = variantCodes;
        }
    }

    public sealed class AddProductCommandHandler : ICommandHandler<AddProductCommand, Product>
    {
        private readonly IEventStoreRepository<Product> _eventStoreRepository;
        private readonly IEfRepository<PimDbContext, Category> _efRepository;
     
        public AddProductCommandHandler(IEventStoreRepository<Product> eventStoreRepository, IEfRepository<PimDbContext, Category> efRepository, PimDbContext pimDbContext)
        {
            _eventStoreRepository = eventStoreRepository;
            _efRepository = efRepository;
        }
        public async Task<Product> Handle(AddProductCommand command)
        {
            var product = new Product()
            {
                Id = command.Id,
                Name = command.Name,
                Description = command.Description,
                FieldTemplateId = command.FieldTemplateId,
            };

            product.AddProduct(command.Creator, command.Id, command.CategoryId, command.CategoryName, command.Name, command.Description, command.FieldTemplateId, command.ProductFields, command.IndexingValue, command.Code, DateTime.UtcNow);

            foreach (var item in command.VariantFields)
            {
                var variantName = VariantExtensions.GetVariantName(command.VariantFields[item.Key]);
                var variantDescription = VariantExtensions.GetVariantDescription(command.Name, command.VariantFields[item.Key]);
                product.AddVariant(product.Id, item.Key, command.VariantFields[item.Key], command.VariantPrices[item.Key], command.VariantCodes[item.Key], variantName, variantDescription);
            }

            await _eventStoreRepository.SaveAsync(product);
            var category = await _efRepository.GetByIdAsync(command.CategoryId);
            product.Category = category;
            product.CreatedDate = DateTime.UtcNow;
            product.CreatedBy = command.Creator;
            product.UpdatedDate = DateTime.UtcNow;
            product.UpdatedBy = command.Creator;
            return product;
        }

    }
}
