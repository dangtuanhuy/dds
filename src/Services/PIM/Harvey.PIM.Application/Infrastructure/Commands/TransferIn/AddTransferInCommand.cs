﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.EventBus.Events.StockLevels;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.TransferIn
{
    public sealed class AddTransferInCommand : ICommand<bool>
    {
        public Guid Creator { get; }
        public List<InventoryTransactionTransferInModel> InventoryTransactionTransferInsModel { get; }
        public AddTransferInCommand(
            Guid creator,
            List<InventoryTransactionTransferInModel> inventoryTransactionTransferInsModel)
        {
            Creator = creator;
            InventoryTransactionTransferInsModel = inventoryTransactionTransferInsModel;
        }
    }
    public sealed class AddTransferInCommandHandler : ICommandHandler<AddTransferInCommand, bool>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly ISearchService _searchService;
        private readonly IEventBus _eventBus;
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _allocationTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, AllocationTransactionDetail> _allocationTransactionDetailRepository;
        private readonly IMapper _mapper;
        public AddTransferInCommandHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction> allocationTransactionRepository,
            IEfRepository<TransactionDbContext, AllocationTransactionDetail> allocationTransactionDetailRepository,
            IMapper mapper,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext,
            ISearchService searchService,
            IEventBus eventBus,
            INextSequenceService<TransientPimDbContext> nextSequenceService
            )
        {
            _allocationTransactionRepository = allocationTransactionRepository;
            _allocationTransactionDetailRepository = allocationTransactionDetailRepository;
            _mapper = mapper;
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _searchService = searchService;
            _eventBus = eventBus;
            _nextSequenceService = nextSequenceService;
        }

        public async Task<bool> Handle(AddTransferInCommand command)
        {
            var updateListStockLevelIndexedItems = new List<StockLevelIndexedItem>();
            var inventoryToLocationIds = command.InventoryTransactionTransferInsModel.Select(x => x.InventoryTransactionToLocation);
            var locations = await _pimDbContext.Locations.Where(x => inventoryToLocationIds.Contains(x.Id)).AsNoTracking().ToListAsync();
            var stockTypes = await _transactionDbContext.StockTypes.AsNoTracking().ToListAsync();
            var stockTransactions = await _transactionDbContext.StockTransactions.AsNoTracking().ToListAsync();
            var transactionTypeTFI = _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == TransactionTypeCode.TFI.ToString()).FirstOrDefault();

            if (command.InventoryTransactionTransferInsModel == null || command.InventoryTransactionTransferInsModel.Count <= 0)
            {
                throw new ArgumentException($"List item is empty. Please try again!");
            }

            if (transactionTypeTFI != null)
            {
                var inventoryTransactionAddings = new List<InventoryTransaction>();
                var stockTransactionAddings = new List<StockTransaction>();
                var events = new List<StockHasTransferedInStoreEvent>();
                var locationCode = "";
                Guid locationId = Guid.Empty;

                foreach (var inventoryTransactionTransferIn in command.InventoryTransactionTransferInsModel)
                {
                    var inventoryTransactionExist = inventoryTransactionAddings.FirstOrDefault(x => x.TransactionRefId == inventoryTransactionTransferIn.InventoryTransactionId);

                    locationCode = locations.FirstOrDefault(x => x.Id == inventoryTransactionTransferIn.InventoryTransactionToLocation)?.LocationCode;
                    locationId = inventoryTransactionTransferIn.InventoryTransactionToLocation;

                    var inventoryTransaction = new InventoryTransaction();

                    if (inventoryTransactionExist == null)
                    {
                        var outlet = locations.FirstOrDefault(x => x.Id == inventoryTransactionTransferIn.InventoryTransactionToLocation);
                        var keyName = $"{TransactionTypeCode.TFI.ToString()}{outlet.LocationCode}{DateTime.UtcNow.ToString("yyMM")}";
                        var valueName = _nextSequenceService.Get(keyName);
                        inventoryTransaction = new InventoryTransaction
                        {
                            Id = Guid.NewGuid(),
                            TransactionRefId = inventoryTransactionTransferIn.InventoryTransactionId,
                            FromLocationId = inventoryTransactionTransferIn.InventoryTransactionFromLocation,
                            ToLocationId = inventoryTransactionTransferIn.InventoryTransactionToLocation,
                            TransactionTypeId = transactionTypeTFI.Id,
                            TransferNumber = $"{keyName}{valueName}",
                            CreatedDate = DateTime.UtcNow,
                            CreatedBy = command.Creator,
                            UpdatedDate = DateTime.UtcNow,
                            UpdatedBy = command.Creator
                        };
                        inventoryTransactionAddings.Add(inventoryTransaction);
                        await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransaction);
                    }

                    var lastTransferStockTransaction = await GetLastStockTransactionBalance(inventoryTransactionTransferIn.VariantId, inventoryTransactionTransferIn.InventoryTransactionToLocation, stockTransactionAddings);

                    var newStockTransaction = new StockTransaction
                    {
                        Id = Guid.NewGuid(),
                        InventoryTransactionId = inventoryTransactionExist != null
                                                    ? inventoryTransactionExist.Id
                                                    : inventoryTransaction.Id,
                        LocationId = inventoryTransactionTransferIn.InventoryTransactionToLocation,
                        VariantId = inventoryTransactionTransferIn.VariantId,
                        StockTypeId = Guid.Empty,
                        TransactionTypeId = transactionTypeTFI.Id,
                        StockTransactionRefId = inventoryTransactionTransferIn.StockTransactionRefId,
                        Quantity = inventoryTransactionTransferIn.Quantity,
                        Balance = (lastTransferStockTransaction != null ? lastTransferStockTransaction.Balance : 0) + inventoryTransactionTransferIn.Quantity,
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = command.Creator,
                        UpdatedBy = command.Creator,
                        UpdatedDate = DateTime.UtcNow
                    };

                    var existStockTransactionAdding = stockTransactionAddings.Where(x => x.LocationId == newStockTransaction.LocationId
                                                                                 && x.VariantId == newStockTransaction.VariantId
                                                                                 && x.StockTypeId == newStockTransaction.StockTypeId)
                                                                             .FirstOrDefault();
                    if (existStockTransactionAdding != null)
                    {
                        newStockTransaction.Balance = existStockTransactionAdding.Balance + newStockTransaction.Quantity;
                    }

                    stockTransactionAddings.Add(newStockTransaction);
                }

                var variantIds = stockTransactionAddings.Select(x => x.VariantId).Distinct().ToList();

                var variants = await _pimDbContext.Variants.Where(x => variantIds.Contains(x.Id)).AsNoTracking().ToListAsync();

                foreach (var stockTransaction in stockTransactionAddings)
                {
                    var variant = variants.FirstOrDefault(x => x.Id == stockTransaction.VariantId);
                    Guid productId = variant.ProductId;
                    var variantSkuCode = variant.SKUCode;

                    var referenceId = $"{variantSkuCode}{locationCode}";
                    var document = new StockLevelIndexedItem(new StockLevelSearchItem());

                    stockTransaction.ReferenceId = referenceId;
                    await _transactionDbContext.StockTransactions.AddAsync(stockTransaction);
                    document = new StockLevelIndexedItem(new StockLevelSearchItem(stockTransaction.ReferenceId)
                    {
                        ReferenceId = stockTransaction.ReferenceId,
                        ProductId = productId,
                        LocationId = stockTransaction.LocationId,
                        VariantId = stockTransaction.VariantId,
                        Balance = stockTransaction.Balance
                    });

                    var updateListStockLevelIndexedItem = updateListStockLevelIndexedItems.FirstOrDefault(x => x.Item.LocationId == document.Item.LocationId
                                                                                                            && x.Item.VariantId == document.Item.VariantId);
                    if (updateListStockLevelIndexedItem == null)
                    {
                        updateListStockLevelIndexedItems.Add(document);
                    }
                    else
                    {
                        updateListStockLevelIndexedItems.FirstOrDefault(x => x.Item.LocationId == document.Item.LocationId
                                                                          && x.Item.VariantId == document.Item.VariantId).Item.Balance += stockTransaction.Quantity;
                    }

                    events.Add(new StockHasTransferedInStoreEvent()
                    {
                        Quantity = stockTransaction.Quantity,
                        StockTypeId = stockTransaction.StockTypeId,
                        StoreId = stockTransaction.LocationId,
                        StockTransactionRefId = stockTransaction.InventoryTransactionId,
                        TransactionTypeId = stockTransaction.TransactionTypeId,
                        VariantId = stockTransaction.VariantId
                    });
                }


                foreach (var updateListStockLevelIndexedItem in updateListStockLevelIndexedItems)
                {
                    var existStockLevelIndex = stockTransactions.FirstOrDefault(x => x.LocationId == updateListStockLevelIndexedItem.Item.LocationId
                                                                                  && x.ReferenceId == updateListStockLevelIndexedItem.Item.ReferenceId);

                    if (existStockLevelIndex == null)
                    {
                        await _searchService.AddAsync(updateListStockLevelIndexedItem);
                    }
                    else
                    {
                        await _searchService.UpdateAsync(updateListStockLevelIndexedItem);
                    }
                }

                await _transactionDbContext.SaveChangesAsync();

                if (events != null)
                {
                    foreach (var item in events)
                    {
                        await _eventBus.PublishAsync(item);
                    }
                }

                await Task.Delay(5000);

                await _eventBus.PublishAsync(new StockLevelUpdatedEvent()
                {
                    LocationId = locationId
                });

                var stockAndPrices = stockTransactionAddings.Select(x => new StockAndPriceLevelModel()
                {
                    LocationId = x.LocationId,
                    ProductId = variants.SingleOrDefault(y => y.Id == x.VariantId).ProductId,
                    VariantId = x.VariantId,
                    SKUCode = variants.SingleOrDefault(y => y.Id == x.VariantId).SKUCode,
                }).Cast<dynamic>().ToList();
                await _eventBus.PublishAsync(new StockAndPriceLevelUpdatedEvent()
                {
                    stockAndPrices = stockAndPrices
                });

                return true;
            }
            else return false;
        }

        public async Task<StockTransaction> GetLastStockTransactionBalance(Guid variantId, Guid toLocationId, List<StockTransaction> stockTransactionAddings)
        {
            var stockTransactions = await _transactionDbContext.StockTransactions.Where(x => x.VariantId == variantId).AsNoTracking().ToListAsync();

            var lastTransferStockTransaction = stockTransactions.OrderByDescending(x => x.CreatedDate)
                                                               .Where(x => x.LocationId == toLocationId
                                                                   && x.VariantId == variantId).FirstOrDefault();

            var existTransferAddingStockTransactions = stockTransactionAddings.Where(x => x.VariantId == variantId);

            if (existTransferAddingStockTransactions != null)
            {
                foreach (var existTransferAddingStockTransaction in existTransferAddingStockTransactions)
                {
                    if (lastTransferStockTransaction != null)
                    {
                        lastTransferStockTransaction.Balance = lastTransferStockTransaction.Balance - existTransferAddingStockTransaction.Quantity;
                    }
                }
            }

            return lastTransferStockTransaction;
        }
    }
}
