﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.EventBus.Events.StockLevels;
using Harvey.NextSequence;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Services;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Harvey.PIM.Application.Infrastructure.Indexing.StockLevelStockTypeSearchItem;

namespace Harvey.PIM.Application.Infrastructure.Commands.Allocations
{
    public sealed class AddGIWCommand : ICommand<Guid>
    {
        public GIWDModel StockAllocation { get; set; }
        public Guid UserId { get; set; }
        public AddGIWCommand(GIWDModel stockAllocation,
            Guid userId)
        {
            StockAllocation = stockAllocation;
            UserId = userId;
        }
    }

    public sealed class AddGIWCommandHandler : ICommandHandler<AddGIWCommand, Guid>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly TransientPimDbContext _transientPimDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        private readonly ISearchService _searchService;
        private readonly IEventBus _eventBus;
        private readonly ProductService _productService;
        private readonly VariantService _variantService;
        private readonly IEfRepository<PimDbContext, PurchaseOrder> _poRepository;
        private readonly IEfRepository<PimDbContext, PurchaseOrderItem> _poItemsRepository;
        private readonly InventoryTransactionService _inventoryTransactionService;
        public AddGIWCommandHandler(TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext,
            TransientPimDbContext transientPimDbContext,
            ISearchService searchService,
            IEventBus eventBus,
            ProductService productService,
            VariantService variantService,
            IEfRepository<PimDbContext, PurchaseOrder> poRepository,
            IEfRepository<PimDbContext, PurchaseOrderItem> poItemsRepository,
            InventoryTransactionService inventoryTransactionService)
        {
            _transactionDbContext = transactionDbContext;
            _transientPimDbContext = transientPimDbContext;
            _pimDbContext = pimDbContext;
            _nextSequenceService = new NextSequenceService<TransientPimDbContext>(_transientPimDbContext);
            _searchService = searchService;
            _eventBus = eventBus;
            _productService = productService;
            _variantService = variantService;
            _poRepository = poRepository;
            _poItemsRepository = poItemsRepository;
            _inventoryTransactionService = inventoryTransactionService;
        }

        public async Task<Guid> Handle(AddGIWCommand command)
        {
            var userId = command.UserId;
            var commandStockAllocations = command.StockAllocation;
            var transactionTypeGIW = _transactionDbContext.TransactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.GIW.ToString());

            if (commandStockAllocations.GIWItems == null || commandStockAllocations.GIWItems.Count <= 0)
            {
                throw new ArgumentException($"List item is empty. Please try again!");
            }

            if (transactionTypeGIW == null)
            {
                throw new ArgumentException($"Can't find Transaction Type GIW!");
            }

            var gIWDocument = new GIWDocument
            {
                Id = Guid.NewGuid(),
                Name = Guid.NewGuid().ToString(),
                Description = Guid.NewGuid().ToString(),
                CreatedBy = command.UserId,
                CreatedDate = DateTime.UtcNow,
                VendorId = commandStockAllocations.FromLocationId,
                LocationId = commandStockAllocations.ToLocationId
            };
            await _transactionDbContext.GIWDocuments.AddAsync(gIWDocument);

            var codeInventoryTransaction = await _inventoryTransactionService.GenerateInventoryTransactionCode(TransactionTypeCode.GIW.ToString());
            var inventoryTransaction = new InventoryTransaction
            {
                Id = Guid.NewGuid(),
                TransactionRefId = gIWDocument.Id,
                FromLocationId = commandStockAllocations.FromLocationId,
                ToLocationId = commandStockAllocations.ToLocationId,
                TransactionTypeId = transactionTypeGIW.Id,
                CreatedDate = DateTime.UtcNow,
                CreatedBy = userId,
                UpdatedBy = userId,
                Code = codeInventoryTransaction,
                Status = InventoryTransactionStatus.Pending
            };
            await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransaction);

            var variantIds = commandStockAllocations.GIWItems.Select(x => x.VariantId).Distinct().ToList();
            var variants = await _pimDbContext.Variants.AsNoTracking().Where(x => variantIds.Contains(x.Id)).ToListAsync();

            var productIds = variants.Select(x => x.ProductId).Distinct().ToList();
            var products = await _pimDbContext.Products.AsNoTracking().Where(x => productIds.Contains(x.Id)).ToListAsync();

            var categoryIds = products.Select(x => x.CategoryId).Distinct().ToList();
            var categories = await _pimDbContext.Categories.AsNoTracking().Where(x => categoryIds.Contains(x.Id)).ToListAsync();

            var location = _pimDbContext.Locations.AsNoTracking().FirstOrDefault(x => x.Id == commandStockAllocations.ToLocationId);
            var vendor = _pimDbContext.Vendors.AsNoTracking().FirstOrDefault(x => x.Id == commandStockAllocations.FromLocationId);

            var barCodes = await _pimDbContext.BarCode.AsNoTracking().Where(x => variantIds.Contains(x.VariantId)).ToListAsync();

            var poIds = commandStockAllocations.GIWItems.Select(x => x.PurchaseOrderId).Distinct().ToList();
            var purchaseOrderItems = await _pimDbContext.purchaseOrderItems.AsNoTracking()
                                        .Where(x => poIds.Contains(x.PurchaseOrderId))
                                        .ToListAsync();

            var stockTransactions = await _transactionDbContext.StockTransactions.AsNoTracking()
                                    .Where(x => variantIds.Contains(x.VariantId) 
                                    && (x.LocationId == commandStockAllocations.FromLocationId || x.LocationId == commandStockAllocations.ToLocationId))
                                    .ToListAsync();

            var numberOfCharsProduct = 5;
            var numberOfCharsVariant = 6;
            var variantKey = "variants";
            var newStockTransactions = new List<StockTransaction>();
            var newBarCodes = new List<BarCode>();
            var stockLevelIndexesAdded = new List<StockLevelSearchItem>();
            var stockLevelIndexesUpdated = new List<StockLevelSearchItem>();
            foreach (var item in commandStockAllocations.GIWItems)
            {
                var poItem = purchaseOrderItems.FirstOrDefault(x => x.PurchaseOrderId == item.PurchaseOrderId && x.VariantId == item.VariantId && x.StockTypeId == item.StockTypeId);
                var newGIWDocumentItem = new GIWDocumentItem()
                {
                    Id = Guid.NewGuid(),
                    GIWDocumentId = gIWDocument.Id,
                    VariantId = item.VariantId,
                    StockTypeId = item.StockTypeId,
                    Quantity = item.Quantity,
                    CostValue = poItem.CostValue,
                    CurrencyCode = poItem.CurrencyCode,
                    RemainingQty = item.Quantity,
                    TransactionRefId = item.PurchaseOrderId
                };
                await _transactionDbContext.GIWDocumentItems.AddAsync(newGIWDocumentItem);

                var variant = variants.FirstOrDefault(x => x.Id == item.VariantId);
                var product = products.FirstOrDefault(x => x.Id == variant.ProductId);
                var category = categories.FirstOrDefault(x => x.Id == product.CategoryId);

                if (product != null && string.IsNullOrEmpty(product.SKUCode))
                {
                    var sKUCode = await _productService.GenerateSKU(category.Code, numberOfCharsProduct);
                    products.FirstOrDefault(x => x.Id == product.Id).SKUCode = sKUCode;
                    product.SKUCode = sKUCode;
                    product.UpdatedDate = DateTime.UtcNow;
                    _pimDbContext.Products.Update(product);
                }
                if (variant != null)
                {
                    if (string.IsNullOrEmpty(variant.SKUCode))
                    {
                        var sKUCode = await _variantService.GenerateSKU(product.SKUCode, variantKey, numberOfCharsVariant);
                        variants.FirstOrDefault(x => x.Id == item.VariantId).SKUCode = sKUCode;
                        variant.SKUCode = sKUCode;
                        variant.UpdatedDate = DateTime.UtcNow;
                        _pimDbContext.Variants.Update(variant);
                    }


                    if (item.BarCodes != null && item.BarCodes.Count > 0)
                    {
                        foreach (var barCode in item.BarCodes)
                        {
                            var latestDbBarCode = barCodes.FirstOrDefault(x => x.Code == barCode);
                            var latestNewBarCode = newBarCodes.FirstOrDefault(x => x.Code == barCode);
                            if (latestDbBarCode == null && latestNewBarCode == null)
                            {
                                var newBarCode = new BarCode()
                                {
                                    Id = Guid.NewGuid(),
                                    Code = barCode,
                                    VariantId = item.VariantId,
                                    CreatedDate = DateTime.UtcNow,
                                    CreatedBy = userId
                                };
                                await _pimDbContext.BarCode.AddAsync(newBarCode);
                                newBarCodes.Add(newBarCode);
                            }
                            else
                            {
                                if ((latestDbBarCode != null && latestDbBarCode.VariantId != item.VariantId) || (latestNewBarCode != null && latestNewBarCode.VariantId != item.VariantId))
                                {
                                    throw new ArgumentException($"Duplicate barcode. Please try again!");
                                }
                            }
                        }
                    }

                }

                var referenceId = $"{variant.SKUCode}{location.LocationCode}";
                var lastStockTransaction = GetLastStockTransaction(referenceId, stockTransactions, newStockTransactions);
                var newStockTransaction = new StockTransaction
                {
                    Id = Guid.NewGuid(),
                    InventoryTransactionId = inventoryTransaction.Id,
                    LocationId = location.Id,
                    VariantId = item.VariantId,
                    StockTypeId = Guid.Empty,
                    TransactionTypeId = transactionTypeGIW.Id,
                    Quantity = item.Quantity,
                    Balance = 0,
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = userId,
                    StockTransactionRefId = item.PurchaseOrderId,
                    ReferenceId = referenceId
                };
                if (lastStockTransaction != null)
                {
                    newStockTransaction.Balance = lastStockTransaction.Balance + item.Quantity;
                    stockLevelIndexesUpdated.Add(new StockLevelSearchItem()
                    {
                        ReferenceId = referenceId,
                        ProductId = variant.ProductId,
                        LocationId = commandStockAllocations.ToLocationId,
                        VariantId = item.VariantId,
                        Balance = newStockTransaction.Balance
                    });
                }
                else
                {
                    newStockTransaction.Balance = item.Quantity;
                    stockLevelIndexesAdded.Add(new StockLevelSearchItem()
                    {
                        ReferenceId = referenceId,
                        ProductId = variant.ProductId,
                        LocationId = commandStockAllocations.ToLocationId,
                        VariantId = item.VariantId,
                        Balance = newStockTransaction.Balance
                    });
                }
                await _transactionDbContext.StockTransactions.AddAsync(newStockTransaction);
                newStockTransactions.Add(newStockTransaction);
            }

            foreach (var item in stockLevelIndexesAdded)
            {
                var document = new StockLevelIndexedItem(new StockLevelSearchItem(item.ReferenceId)
                {
                    ReferenceId = item.ReferenceId,
                    ProductId = item.ProductId,
                    LocationId = item.LocationId,
                    VariantId = item.VariantId,
                    Balance = item.Balance
                });
                await _searchService.AddAsync(document);
            }
            foreach (var item in stockLevelIndexesUpdated)
            {
                var document = new StockLevelIndexedItem(new StockLevelSearchItem(item.ReferenceId)
                {
                    ReferenceId = item.ReferenceId,
                    ProductId = item.ProductId,
                    LocationId = item.LocationId,
                    VariantId = item.VariantId,
                    Balance = item.Balance
                });
                await _searchService.UpdateAsync(document);
            }
            await _transactionDbContext.SaveChangesAsync();
            await _pimDbContext.SaveChangesAsync();

            var purchaseOrderIds = command.StockAllocation.GIWItems
                .GroupBy(x => new { x.PurchaseOrderId }, (key, group) =>
                new
                {
                    Id = key.PurchaseOrderId
                }).ToList();

            await Task.Delay(5000);

            var currentItems = _transactionDbContext.GIWDocumentItems
                .GroupBy(x => new { x.TransactionRefId, x.StockTypeId, x.VariantId }, (key, group) =>
                new GIWItem
                {
                    PurchaseOrderId = key.TransactionRefId,
                    VariantId = key.VariantId,
                    StockTypeId = key.StockTypeId,
                    Quantity = group.Sum(x => x.Quantity)
                }).ToList();

            var updatePurchaseOrderIds = new List<Guid>();
            foreach (var item in purchaseOrderIds)
            {
                var poItems = purchaseOrderItems.Where(x => x.PurchaseOrderId == item.Id).ToList();
                if (IsUpdateStatusPurchaseOrder(currentItems, poItems) == true)
                {
                    updatePurchaseOrderIds.Add(item.Id);
                    var po = await _pimDbContext.PurchaseOrders.FirstOrDefaultAsync(x => x.Id == item.Id);
                    await _poRepository.DeleteAsync(po);
                    await _poItemsRepository.DeleteAsync(poItems);
                }
            }

            await _eventBus.PublishAsync(new PurchaseOrderUpdatedStatusEvent()
            {
                Ids = updatePurchaseOrderIds
            });

            await _eventBus.PublishAsync(new StockLevelUpdatedEvent()
            {
                LocationId = commandStockAllocations.ToLocationId
            });

            var stockAndPrices = newStockTransactions.Select(x => new StockAndPriceLevelModel()
            {
                LocationId = x.LocationId,
                ProductId = variants.SingleOrDefault(y => y.Id == x.VariantId).ProductId,
                VariantId = x.VariantId,
                SKUCode = variants.SingleOrDefault(y => y.Id == x.VariantId).SKUCode,
            }).Cast<dynamic>().ToList();
            await _eventBus.PublishAsync(new StockAndPriceLevelUpdatedEvent()
            {
                stockAndPrices = stockAndPrices
            });

            return inventoryTransaction.Id;
        }

        private StockTransaction GetLastStockTransaction(string referenceId, List<StockTransaction> dbStockTransactions, List<StockTransaction> newStockTransactions)
        {
            var dbStockTransaction = dbStockTransactions.OrderByDescending(x => x.CreatedDate)
                            .FirstOrDefault(x => x.ReferenceId == referenceId);
            var newStockTransaction = newStockTransactions.OrderByDescending(x => x.CreatedDate)
                                        .FirstOrDefault(x => x.ReferenceId == referenceId);
            if (dbStockTransaction != null)
            {
                if (newStockTransaction != null)
                {
                    return newStockTransaction;
                }
                else
                {
                    return dbStockTransaction;
                }
            }
            if (dbStockTransaction == null && newStockTransaction != null)
            {
                return newStockTransaction;
            }
            return null;
        }

        private bool IsUpdateStatusPurchaseOrder(List<GIWItem> currentItems, List<PurchaseOrderItem> poItems)
        {
            var countItem = poItems.Count;
            foreach (var item in poItems)
            {
                var currentItem = currentItems.FirstOrDefault(x => x.PurchaseOrderId == item.PurchaseOrderId
                                                                    && x.VariantId == item.VariantId
                                                                    && x.StockTypeId == item.StockTypeId);
                if (currentItem != null && (item.Quantity - currentItem.Quantity) <= 0)
                {
                    countItem -= 1;
                }
            }

            if (countItem == 0)
            {
                return true;
            }

            return false;
        }
    }
}
