﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.GIWs
{
    public class UpdateGIWStatusCommand : ICommand<bool>
    {
        public Guid Id { get; set; }
        public InventoryTransactionStatus Status { get; set; }
        public Guid Updater { get; set; }
        public UpdateGIWStatusCommand(Guid id, InventoryTransactionStatus status, Guid updater)
        {
            Id = id;
            Status = status;
            Updater = updater;
        }
    }

    public sealed class UpdateGIWStatusCommandHandler : ICommandHandler<UpdateGIWStatusCommand, bool>
    {
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _inventoryRepository;
        private readonly IMapper _mapper;
        public UpdateGIWStatusCommandHandler(IEfRepository<TransactionDbContext, InventoryTransaction> inventoryRepository, IMapper mapper)
        {
            _inventoryRepository = inventoryRepository;
            _mapper = mapper;
        }
        public async Task<bool> Handle(UpdateGIWStatusCommand command)
        {
            var inventoryTransaction = await _inventoryRepository.GetByIdAsync(command.Id);
            if (inventoryTransaction != null)
            {

            }
            inventoryTransaction.Status = command.Status;
            inventoryTransaction.UpdatedBy = command.Updater;

            await _inventoryRepository.UpdateAsync(inventoryTransaction);
            return true;
        }
    }
}
