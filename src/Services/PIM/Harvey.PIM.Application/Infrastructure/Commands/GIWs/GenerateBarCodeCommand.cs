﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.GIWs
{
    public sealed class GenerateBarCodeCommand : ICommand<List<BarCode>>
    {
        public GenerateBarCodeRequestModel Request { get; set; }
        public Guid UserId { get; set; }
        public GenerateBarCodeCommand(GenerateBarCodeRequestModel request,
            Guid userId)
        {
            Request = request;
            UserId = userId;
        }
    }

    public sealed class GenerateBarCodeCommandHandler : ICommandHandler<GenerateBarCodeCommand, List<BarCode>>
    {
        private readonly PimDbContext _pimDbContext;
        private readonly ProductService _productService;
        private readonly VariantService _variantService;
        public GenerateBarCodeCommandHandler(PimDbContext pimDbContext,
            ProductService productService,
            VariantService variantService)
        {
            _pimDbContext = pimDbContext;
            _productService = productService;
            _variantService = variantService;
        }
        public async Task<List<BarCode>> Handle(GenerateBarCodeCommand command)
        {
            var numberOfCharsProduct = 5;
            var numberOfCharsVariant = 6;
            var variantKey = "variants";
            var variantIds = command.Request.VariantIds;
            var variants = await _pimDbContext.Variants.AsNoTracking().Where(x => variantIds.Contains(x.Id)).ToListAsync();

            var productIds = variants.Select(x => x.ProductId).Distinct().ToList();
            var products = await _pimDbContext.Products.AsNoTracking().Where(x => productIds.Contains(x.Id)).ToListAsync();

            var barCodes = await _pimDbContext.BarCode.AsNoTracking().Where(x => variantIds.Contains(x.VariantId)).ToListAsync();

            var categoryIds = products.Select(x => x.CategoryId).Distinct();
            var categories = await _pimDbContext.Categories.AsNoTracking().Where(x => categoryIds.Contains(x.Id)).ToListAsync();

            var newBarCodes = new List<BarCode>();
            foreach(var variantId in command.Request.VariantIds)
            {
                var productId = variants.FirstOrDefault(x => x.Id == variantId)?.ProductId;
                var product = products.FirstOrDefault(x => x.Id == productId);
                var category = _pimDbContext.Categories.FirstOrDefault(x => x.Id == product.CategoryId);
                if (product != null && string.IsNullOrEmpty(product.SKUCode))
                {
                    var skuCode = await _productService.GenerateSKU(category.Code, numberOfCharsProduct);
                    product.SKUCode = skuCode;
                    product.UpdatedDate = DateTime.UtcNow;
                    _pimDbContext.Products.Update(product);
                }

                var variant = variants.FirstOrDefault(x => x.Id == variantId);
                if (variant != null && string.IsNullOrEmpty(variant.SKUCode))
                {
                    var skuCode = await _variantService.GenerateSKU(product.SKUCode, variantKey, numberOfCharsVariant);
                    variant.SKUCode = skuCode;
                    variant.UpdatedDate = DateTime.UtcNow;
                    _pimDbContext.Variants.Update(variant);
                }

                var barCode = barCodes.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.VariantId == variantId);
                var newBarCode = new BarCode();
                if (barCode == null)
                {
                    newBarCode = new BarCode()
                    {
                        Id = Guid.NewGuid(),
                        VariantId = variantId,
                        Code = variant.SKUCode
                    };
                    newBarCodes.Add(newBarCode);
                } else
                {
                    newBarCodes.AddRange(barCodes.Where(x => x.VariantId == variantId).ToList());
                }

                await _pimDbContext.SaveChangesAsync();
            }
            
            return newBarCodes;
        }
    }
}
