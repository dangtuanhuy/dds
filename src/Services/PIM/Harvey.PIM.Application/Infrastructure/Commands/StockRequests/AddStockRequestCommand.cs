﻿
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Harvey.PIM.Application.Services;

namespace Harvey.PIM.Application.Infrastructure.Commands.StockRequests
{
    public sealed class AddStockRequestCommand: ICommand<StockRequestListModel>
    {
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime DateRequest { get; set; }
        public List<StockRequestItemModel> StockRequestItems { get; set; }

        public AddStockRequestCommand(
            Guid fromLocationId,
            Guid toLocationId,
            string subject,
            string description,
            Guid createdBy,
            DateTime dateRequest,
            List<StockRequestItemModel> stockRequestItems)
        {
            FromLocationId = fromLocationId;
            ToLocationId = toLocationId;
            Subject = subject;
            Description = description;
            CreatedBy = createdBy;
            DateRequest = dateRequest;
            StockRequestItems = stockRequestItems;
        }
    }

    public sealed class AddStockRequestCommandHandler : ICommandHandler<AddStockRequestCommand, StockRequestListModel>
    {
        private readonly IEfRepository<TransactionDbContext, StockRequest> _repositoryStockRequest;
        private readonly IEfRepository<TransactionDbContext, StockRequestItem> _repositoryStockRequestItem;
        private readonly PimDbContext _pimDbContext;
        private StockRequestService _stockRequestService;
        public AddStockRequestCommandHandler(
            IEfRepository<TransactionDbContext, StockRequest> repositoryStockRequest,
            IEfRepository<TransactionDbContext, StockRequestItem> repositoryStockRequestItem,
            PimDbContext pimDbContext,
            StockRequestService stockRequestService)
        {
            _repositoryStockRequest = repositoryStockRequest;
            _repositoryStockRequestItem = repositoryStockRequestItem;
            _pimDbContext = pimDbContext;
            _stockRequestService = stockRequestService;
        }
        public async Task<StockRequestListModel> Handle(AddStockRequestCommand command)
        {
            var locations = _pimDbContext.Locations.AsNoTracking().ToList();
            if (command.DateRequest < DateTime.UtcNow.Date)
            {
                throw new ArgumentException($"The date request should be higher.");
            }
            var stockRequest = new StockRequest()
            {
                Subject = string.IsNullOrEmpty(command.Subject) ? await _stockRequestService.GenerateSubjectName() : command.Subject,
                Description = command.Description,
                FromLocationId = command.FromLocationId,
                ToLocationId = command.ToLocationId,
                StockRequestStatus = StockRequestStatus.Opened,
                DateRequest = command.DateRequest.ToUniversalTime(),
                CreatedBy = command.CreatedBy,
                UpdatedBy = command.CreatedBy
            };
            var request = await _repositoryStockRequest.AddAsync(stockRequest);
            var stockRequestItems = new List<StockRequestItem>();

            foreach(var item in command.StockRequestItems)
            {
                stockRequestItems.Add(new StockRequestItem()
                                        {
                                            StockRequestId = request.Id,
                                            VariantId = item.Variant.Id,
                                            Quantity = item.Quantity
                                        });
            }

            var requestItem = await _repositoryStockRequestItem.AddAsync(stockRequestItems);

            var stockRequestModel = new StockRequestListModel()
            {
                Id = request.Id,
                Subject = request.Subject,
                ToLocationName = locations.FirstOrDefault(x => x.Id == request.ToLocationId)?.Name,
                FromLocationName = locations.FirstOrDefault(x => x.Id == request.FromLocationId)?.Name,
                Description = request.Description,
                CreatedBy = request.CreatedBy,
                CreatedDate = request.CreatedDate,
                Status = request.StockRequestStatus.ToString(),
                UpdatedBy = request.UpdatedBy,
                UpdatedDate = request.UpdatedDate
            };

            return stockRequestModel;
        }
    }


}
