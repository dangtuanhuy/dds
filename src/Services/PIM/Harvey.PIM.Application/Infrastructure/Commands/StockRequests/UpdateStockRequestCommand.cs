﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.StockRequests
{
    public sealed class UpdateStockRequestCommand : ICommand<StockRequestListModel>
    {
        public Guid Id { get; set; }
        public Guid LocationId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public Guid UpdatedBy { get; set; }
        public StockRequestStatus StockRequestStatus { get; set; }
        public List<StockRequestItemModel> StockRequestItems { get; set; }
        public DateTime DateRequest { get; set; }
        public bool IsDeleteRequest { get; set; }
        public UpdateStockRequestCommand(Guid id,
            Guid locationId,
            string subject,
            string description,
            List<StockRequestItemModel> stockRequestItems,
            StockRequestStatus stockRequestStatus,
            Guid updatedBy,
            DateTime dateRequest,
            bool isDeleteRequest)
        {
            Id = id;
            LocationId = locationId;
            Subject = subject;
            Description = description;
            StockRequestItems = stockRequestItems;
            StockRequestStatus = StockRequestStatus;
            UpdatedBy = updatedBy;
            DateRequest = dateRequest;
            IsDeleteRequest = isDeleteRequest;
        }
    }

    public sealed class UpdateStockRequestCommandHandler : ICommandHandler<UpdateStockRequestCommand, StockRequestListModel>
    {
        private readonly IEfRepository<TransactionDbContext, StockRequest> _repositoryStockRequest;
        private readonly IEfRepository<TransactionDbContext, StockRequestItem> _repositoryStockRequestItem;
        private readonly PimDbContext _pimDbContext;
        public UpdateStockRequestCommandHandler(
            IEfRepository<TransactionDbContext, StockRequest> repositoryStockRequest,
            IEfRepository<TransactionDbContext, StockRequestItem> repositoryStockRequestItem,
            PimDbContext pimDbContext)
        {
            _repositoryStockRequest = repositoryStockRequest;
            _repositoryStockRequestItem = repositoryStockRequestItem;
            _pimDbContext = pimDbContext;
        }

        public async Task<StockRequestListModel> Handle(UpdateStockRequestCommand command)
        {
            var stockRequest = await _repositoryStockRequest.GetByIdAsync(command.Id);
            var locations = _pimDbContext.Locations.AsNoTracking().ToList();
            if (stockRequest == null )
            {
                throw new ArgumentException($"StockRequest {command.Id} is not presented.");
            }

            if(stockRequest.StockRequestStatus != StockRequestStatus.Opened)
            {
                throw new ArgumentException($"StockRequest {command.Id} is not allowed to update.");
            }

            if (command.DateRequest < DateTime.UtcNow.Date)
            {
                throw new ArgumentException($"The date request should be higher.");
            }

            if (command.IsDeleteRequest == true)
            {
                await _repositoryStockRequest.DeleteAsync(stockRequest);
                foreach(var item in command.StockRequestItems)
                {
                    var _item = await _repositoryStockRequestItem.GetByIdAsync(item.Id);
                    await _repositoryStockRequestItem.DeleteAsync(_item);
                }

                return new StockRequestListModel();
            }
            else
            {
                stockRequest.Subject = command.Subject;
                stockRequest.Description = command.Description;
                stockRequest.ToLocationId = command.LocationId;
                stockRequest.UpdatedBy = command.UpdatedBy;
                stockRequest.StockRequestStatus = command.StockRequestStatus;
                stockRequest.DateRequest = command.DateRequest.ToUniversalTime();

                await _repositoryStockRequest.UpdateAsync(stockRequest);

                var stockRequestItems = await _repositoryStockRequestItem.ListAsync(x => x.StockRequestId == stockRequest.Id);
                foreach (var item in command.StockRequestItems)
                {
                    var stockRequestItem = await _repositoryStockRequestItem.GetByIdAsync(item.Id);
                    if (stockRequestItem != null)
                    {
                        stockRequestItem.VariantId = item.Variant.Id;
                        stockRequestItem.Quantity = item.Quantity;

                        await _repositoryStockRequestItem.UpdateAsync(stockRequestItem);
                        stockRequestItems = stockRequestItems.Where(x => x.Id != item.Id);
                    } else
                    {
                        var stockItem = new StockRequestItem()
                        {
                            StockRequestId = stockRequest.Id,
                            VariantId = item.Variant.Id,
                            Quantity = item.Quantity
                        };
                        await _repositoryStockRequestItem.AddAsync(stockItem);
                    }
                }

                await _repositoryStockRequestItem.DeleteAsync(stockRequestItems.ToList());

                return new StockRequestListModel()
                {
                    Id = stockRequest.Id,
                    Subject = stockRequest.Subject,
                    Description = stockRequest.Description,
                    ToLocationName = locations.FirstOrDefault(x => x.Id == stockRequest.ToLocationId)?.Name,
                    FromLocationName = locations.FirstOrDefault(x => x.Id == stockRequest.FromLocationId)?.Name,
                    CreatedBy = stockRequest.CreatedBy,
                    CreatedDate = stockRequest.CreatedDate,
                    UpdatedBy = stockRequest.UpdatedBy,
                    UpdatedDate = stockRequest.UpdatedDate,
                    Status = stockRequest.StockRequestStatus.ToString()
                };
            } 
            
        }
    }
}
