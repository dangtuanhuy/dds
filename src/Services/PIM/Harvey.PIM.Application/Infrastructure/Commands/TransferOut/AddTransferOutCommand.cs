﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.EventBus.Events.StockLevels;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.TransferOut
{
    public sealed class AddTransferOutCommand : ICommand<bool>
    {
        public Guid Creator { get; }
        public Guid FromLocationId { get; }
        public Guid ToLocationId { get; }
        public List<AllocationTransferOutProduct> AllocationTransferOutProducts { get; }
        public AddTransferOutCommand(
            Guid creator,
            Guid fromLocationId,
            Guid toLocationId,
            List<AllocationTransferOutProduct> allocationTransferOutProducts)
        {
            Creator = creator;
            FromLocationId = fromLocationId;
            ToLocationId = toLocationId;
            AllocationTransferOutProducts = allocationTransferOutProducts;
        }
    }
    public sealed class AddTransferOutCommandHandler : ICommandHandler<AddTransferOutCommand, bool>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly ISearchService _searchService;
        private readonly IEventBus _eventBus;
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _allocationTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, AllocationTransactionDetail> _allocationTransactionDetailRepository;
        private readonly IMapper _mapper;
        public AddTransferOutCommandHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction> allocationTransactionRepository,
            IEfRepository<TransactionDbContext, AllocationTransactionDetail> allocationTransactionDetailRepository,
            IMapper mapper,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext,
            ISearchService searchService,
            IEventBus eventBus,
            INextSequenceService<TransientPimDbContext> nextSequenceService
            )
        {
            _allocationTransactionRepository = allocationTransactionRepository;
            _allocationTransactionDetailRepository = allocationTransactionDetailRepository;
            _mapper = mapper;
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _searchService = searchService;
            _eventBus = eventBus;
            _nextSequenceService = nextSequenceService;
        }

        public async Task<bool> Handle(AddTransferOutCommand command)
        {
            var stockTypes = await _transactionDbContext.StockTypes.AsNoTracking().ToListAsync();
            var location = await _pimDbContext.Locations.FirstOrDefaultAsync(x => x.Id == command.FromLocationId);
            var transactionTypeGIW = await _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == TransactionTypeCode.GIW.ToString()).FirstOrDefaultAsync();
            var transactionTypeTFO = await _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == TransactionTypeCode.TFO.ToString()).FirstOrDefaultAsync();
            var inventoryTransactionAddings = new List<InventoryTransaction>();
            var stockTransactionAddings = new List<StockTransaction>();
            var updateListStockLevelIndexedItems = new List<StockLevelIndexedItem>();
            var listAllocationTransactionId = command.AllocationTransferOutProducts.Select(x => x.TransactionRefId).Distinct();

            if (command.AllocationTransferOutProducts == null || command.AllocationTransferOutProducts.Count <= 0)
            {
                throw new ArgumentException($"List item is empty. Please try again!");
            }

            if (transactionTypeTFO != null)
            {
                var locationCode = location?.LocationCode;
                var events = new List<StockHasTransferedInStoreEvent>();
                Guid locationId = command.FromLocationId;

                var keyName = $"{TransactionTypeCode.TFO.ToString()}{locationCode}{DateTime.UtcNow.ToString("yyMM")}";
                var valueName = _nextSequenceService.Get(keyName);
                var inventoryTransaction = new InventoryTransaction
                {
                    Id = Guid.NewGuid(),
                    TransactionRefId = Guid.Empty,
                    FromLocationId = command.FromLocationId,
                    ToLocationId = command.ToLocationId,
                    TransactionTypeId = transactionTypeTFO.Id,
                    TransferNumber = $"{keyName}{valueName}",
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = command.Creator,
                    UpdatedDate = DateTime.UtcNow,
                    UpdatedBy = command.Creator,
                    StockTransactions = new List<StockTransaction>()
                };

                foreach (var allocationTransferOutProduct in command.AllocationTransferOutProducts)
                {
                    var lastStockTransactionBalance = await GetLastStockTransactionBalance(allocationTransferOutProduct.VariantId, command.FromLocationId, stockTransactionAddings);

                    if (allocationTransferOutProduct.Quantity > lastStockTransactionBalance)
                    {
                        return false;
                    }

                    var totalQuantity = allocationTransferOutProduct.Quantity;
                    if (totalQuantity > 0)
                    {
                        var newStockTransaction = new StockTransaction
                        {
                            Id = Guid.NewGuid(),
                            InventoryTransactionId = inventoryTransaction.Id,
                            LocationId = command.FromLocationId,
                            VariantId = allocationTransferOutProduct.VariantId,
                            StockTypeId = Guid.Empty,
                            TransactionTypeId = transactionTypeTFO.Id,
                            StockTransactionRefId = allocationTransferOutProduct.StockTransactionRefId,
                            Quantity = lastStockTransactionBalance >= totalQuantity
                                              ? totalQuantity
                                              : lastStockTransactionBalance,
                            Balance = lastStockTransactionBalance >= totalQuantity
                                      ? lastStockTransactionBalance - totalQuantity
                                      : 0,
                            CreatedDate = DateTime.UtcNow,
                            CreatedBy = command.Creator,
                            UpdatedBy = command.Creator,
                            UpdatedDate = DateTime.UtcNow
                        };

                        totalQuantity = totalQuantity - newStockTransaction.Quantity;

                        stockTransactionAddings.Add(newStockTransaction);
                    }
                }
                var variantIds = stockTransactionAddings.Select(x => x.VariantId).Distinct().ToList();

                var variants = await _pimDbContext.Variants.Where(x => variantIds.Contains(x.Id)).AsNoTracking().ToListAsync();

                foreach (var stockTransaction in stockTransactionAddings)
                {
                    var variant = variants.FirstOrDefault(x => x.Id == stockTransaction.VariantId);
                    Guid productId = variant.ProductId;
                    var variantSkuCode = variant.SKUCode;

                    var referenceId = $"{variantSkuCode}{locationCode}";
                    var document = new StockLevelIndexedItem(new StockLevelSearchItem());

                    stockTransaction.ReferenceId = referenceId;
                    inventoryTransaction.StockTransactions.Add(stockTransaction);

                    document = new StockLevelIndexedItem(new StockLevelSearchItem(stockTransaction.ReferenceId)
                    {
                        ReferenceId = stockTransaction.ReferenceId,
                        ProductId = productId,
                        LocationId = stockTransaction.LocationId,
                        VariantId = stockTransaction.VariantId,
                        Balance = stockTransaction.Balance
                    });

                    var updateListStockLevelIndexedItem = updateListStockLevelIndexedItems.FirstOrDefault(x => x.Item.LocationId == document.Item.LocationId
                                                                                                            && x.Item.VariantId == document.Item.VariantId);
                    if (updateListStockLevelIndexedItem == null)
                    {
                        updateListStockLevelIndexedItems.Add(document);
                    }
                    else
                    {
                        updateListStockLevelIndexedItems.FirstOrDefault(x => x.Item.LocationId == document.Item.LocationId
                                                                          && x.Item.VariantId == document.Item.VariantId).Item.Balance = document.Item.Balance;
                    }


                    if (location.Type == LocationType.Store)
                    {
                        events.Add(new StockHasTransferedInStoreEvent()
                        {
                            Quantity = -stockTransaction.Quantity,
                            StockTypeId = stockTransaction.StockTypeId,
                            StoreId = stockTransaction.LocationId,
                            StockTransactionRefId = stockTransaction.InventoryTransactionId,
                            TransactionTypeId = stockTransaction.TransactionTypeId,
                            VariantId = stockTransaction.VariantId
                        });
                    }
                }


                if (updateListStockLevelIndexedItems != null)
                {
                    var referenceIds = updateListStockLevelIndexedItems.Select(x => x.Item.ReferenceId);
                    var stockTransactions = await _transactionDbContext.StockTransactions.Where(x => referenceIds.Contains(x.ReferenceId)).AsNoTracking().ToListAsync();

                    foreach (var updateListStockLevelIndexedItem in updateListStockLevelIndexedItems)
                    {
                        var existStockLevelIndex = stockTransactions.FirstOrDefault(x => x.LocationId == updateListStockLevelIndexedItem.Item.LocationId
                                                                                      && x.ReferenceId == updateListStockLevelIndexedItem.Item.ReferenceId);
                        if (existStockLevelIndex == null)
                        {
                            await _searchService.AddAsync(updateListStockLevelIndexedItem);
                        }
                        else
                        {
                            await _searchService.UpdateAsync(updateListStockLevelIndexedItem);
                        }
                    }
                }


                if (events != null)
                {
                    foreach (var item in events)
                    {
                        await _eventBus.PublishAsync(item);
                    }
                }


                await updateAllocationTransactionStatus(listAllocationTransactionId, stockTransactionAddings);

                await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransaction);
                await _transactionDbContext.SaveChangesAsync();

                await Task.Delay(5000);

                await _eventBus.PublishAsync(new StockLevelUpdatedEvent()
                {
                    LocationId = locationId
                });

                var stockAndPrices = stockTransactionAddings.Select(x => new StockAndPriceLevelModel()
                {
                    LocationId = x.LocationId,
                    ProductId = variants.SingleOrDefault(y => y.Id == x.VariantId).ProductId,
                    VariantId = x.VariantId,
                    SKUCode = variants.SingleOrDefault(y => y.Id == x.VariantId).SKUCode,
                }).Cast<dynamic>().ToList();
                await _eventBus.PublishAsync(new StockAndPriceLevelUpdatedEvent()
                {
                    stockAndPrices = stockAndPrices
                });

                return true;
            }
            else return false;
        }
        public async Task<int> GetLastStockTransactionBalance(Guid variantId, Guid fromLocationId, List<StockTransaction> stockTransactionAddings)
        {
            var stockTransactions = await _transactionDbContext.StockTransactions.Where(x => x.VariantId == variantId).AsNoTracking().ToListAsync();
            var stockTypes = await _transactionDbContext.StockTypes.AsNoTracking().ToListAsync();

            var lastTransferStockTransaction = stockTransactions.OrderByDescending(x => x.CreatedDate)
                                                                .Where(x => x.LocationId == fromLocationId
                                                                    && x.VariantId == variantId).FirstOrDefault();

            var existTransferAddingStockTransactions = stockTransactionAddings.Where(x => x.VariantId == variantId);

            if (existTransferAddingStockTransactions != null)
            {
                foreach (var existTransferAddingStockTransaction in existTransferAddingStockTransactions)
                {
                    if (lastTransferStockTransaction != null)
                    {
                        lastTransferStockTransaction.Balance = lastTransferStockTransaction.Balance - existTransferAddingStockTransaction.Quantity;
                    }
                }
            }


            return lastTransferStockTransaction.Balance;
        }

        public async Task<bool> updateAllocationTransactionStatus(IEnumerable<Guid> listAllocationTransactionId, List<StockTransaction> stockTransactionAddings)
        {
            foreach (var AllocationTransactionId in listAllocationTransactionId)
            {
                if (AllocationTransactionId == Guid.Empty)
                {
                    return false;
                }
            }
            var transactionTypeTFO = _transactionDbContext.TransactionTypes.AsNoTracking().Where(x => x.Code == TransactionTypeCode.TFO.ToString()).FirstOrDefault();

            if (transactionTypeTFO != null)
            {
                var listInventoryTransactionAdding = stockTransactionAddings.Select(s => new
                {
                    AllocationTransactionId = _transactionDbContext.AllocationTransactionDetails.FirstOrDefault(t => t.Id == s.StockTransactionRefId).AllocationTransactionId,
                    AllocationTransactionDetails = s
                })
                                                                            .GroupBy(x => x.AllocationTransactionId)
                                                                            .Select(g => new
                                                                            {
                                                                                AllocationTransactionId = g.Key,
                                                                                AllocationTransactionDetails = g.Select(x => x.AllocationTransactionDetails).ToList()
                                                                            }).ToList();

                var allocationTransactionIds = listInventoryTransactionAdding.Select(x => x.AllocationTransactionId);
                var allocationTransactionDetails = await _transactionDbContext
                                                            .AllocationTransactionDetails
                                                            .Where(x => allocationTransactionIds.Contains(x.AllocationTransactionId))
                                                            .ToListAsync();
                var allAllocationTransactionDetailIds = allocationTransactionDetails.Select(x => x.Id).Distinct();
                var allStockTransactions  = await _transactionDbContext.StockTransactions.Where(x => allAllocationTransactionDetailIds.Contains(x.StockTransactionRefId)).ToListAsync();


                foreach (var inventoryTransactionAdding in listInventoryTransactionAdding)
                {
                    if (inventoryTransactionAdding.AllocationTransactionId != Guid.Empty)
                    {
                        var listCheckIsComplete = new List<bool>();

                        var correspondingAllocationTransactionDetails = allocationTransactionDetails
                                                                                .Where(x => x.AllocationTransactionId == inventoryTransactionAdding.AllocationTransactionId)
                                                                                .ToList();

                        var allocationTransactionDetailIds = correspondingAllocationTransactionDetails.Select(x => x.Id)
                                                                                         .Distinct();
                        var stockTransactions = allStockTransactions.Where(x => allocationTransactionDetailIds.Contains(x.StockTransactionRefId)).ToList();

                        foreach (var allocationTransactionDetail in correspondingAllocationTransactionDetails)
                        {
                            var isComplete = false;

                            var lastTransferStockTransaction = stockTransactions.Where(x => x.VariantId == allocationTransactionDetail.VariantId
                                                                                         && x.StockTransactionRefId == allocationTransactionDetail.Id)
                                                                                .ToList();

                            var existStockTransactions = inventoryTransactionAdding.AllocationTransactionDetails.Where(x => x.StockTransactionRefId == allocationTransactionDetail.Id
                                                                                            && x.VariantId == allocationTransactionDetail.VariantId
                                                                                            && x.TransactionTypeId == transactionTypeTFO.Id)
                                                                                   .ToList();

                            var totalQuantity = (lastTransferStockTransaction != null ? lastTransferStockTransaction.Sum(x => x.Quantity) : 0) +
                                                (existStockTransactions != null ? existStockTransactions.Sum(x => x.Quantity) : 0);

                            if (allocationTransactionDetail.Quantity <= totalQuantity)
                            {
                                isComplete = true;
                            }

                            listCheckIsComplete.Add(isComplete);
                        }

                        var entity = await _allocationTransactionRepository.GetByIdAsync(inventoryTransactionAdding.AllocationTransactionId);


                        if (listCheckIsComplete.Contains(false))
                        {
                            entity.Status = AllocationTransactionStatus.PartialTransfer;
                        }
                        else
                        {
                            entity.Status = AllocationTransactionStatus.Complete;
                        }

                        await _allocationTransactionRepository.UpdateAsync(entity);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
