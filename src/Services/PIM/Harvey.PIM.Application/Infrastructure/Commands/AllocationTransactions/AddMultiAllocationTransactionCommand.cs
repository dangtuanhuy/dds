﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.AllocationTransactions
{
    public class AddMultiAllocationTransactionCommand : ICommand<bool>
    {
        public List<AllocationTransactionModel> AllocationTransactions { get; }
        public Guid Creator { get; }
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public AddMultiAllocationTransactionCommand(List<AllocationTransactionModel> allocationTransactions, Guid creator, DateTime fromDate, DateTime toDate)
        {
            AllocationTransactions = allocationTransactions;
            Creator = creator;
            FromDate = fromDate;
            ToDate = toDate;
        }
    }

    public class AddMultiAllocationTransactionCommandHandler : ICommandHandler<AddMultiAllocationTransactionCommand, bool>
    {
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _allocationTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, AllocationTransactionDetail> _allocationTransactionDetailRepository;
        private readonly IEfRepository<TransactionDbContext, StockRequest> _stockRequestRepository;
        private readonly IEfRepository<TransactionDbContext, StockRequestItem> _stockRequestItemRepository;
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        public AddMultiAllocationTransactionCommandHandler(IEfRepository<TransactionDbContext, AllocationTransaction> allocationTransactionRepository,
                                                           IEfRepository<TransactionDbContext, AllocationTransactionDetail> allocationTransactionDetailRepository,
                                                           INextSequenceService<TransientPimDbContext> nextSequenceService,
                                                           IEfRepository<TransactionDbContext, StockRequest> stockRequestRepository,
                                                           IEfRepository<TransactionDbContext, StockRequestItem> stockRequestItemRepository)
        {
            _allocationTransactionRepository = allocationTransactionRepository;
            _allocationTransactionDetailRepository = allocationTransactionDetailRepository;
            _nextSequenceService = nextSequenceService;
            _stockRequestRepository = stockRequestRepository;
            _stockRequestItemRepository = stockRequestItemRepository;
        }
        public async Task<bool> Handle(AddMultiAllocationTransactionCommand command)
        {
            foreach (var allocationTransaction in command.AllocationTransactions)
            {
                var currentTime = DateTime.UtcNow.ToString("yyMM", System.Globalization.CultureInfo.InvariantCulture);
                var keyName = $"SA{currentTime}";
                var SANumber = $"{keyName}{_nextSequenceService.Get(keyName)}";
                var allocationTransactionEntity = new AllocationTransaction
                {
                    Name = SANumber,
                    Description = allocationTransaction.Description,
                    FromLocationId = allocationTransaction.FromLocationId,
                    ToLocationId = allocationTransaction.ToLocationId,
                    DeliveryDate = allocationTransaction.DeliveryDate,
                    Status = allocationTransaction.Status,
                    SANumber = SANumber,
                    CreatedBy = command.Creator,
                    TransactionRef = allocationTransaction.TransactionRef,
                    AllocationTransactionDetails = new List<AllocationTransactionDetail>()
                };
                foreach (var AllocationTransactionDetail in allocationTransaction.AllocationTransactionDetails)
                {
                    var allocationTransactionDetail = new AllocationTransactionDetail
                    {
                        Quantity = AllocationTransactionDetail.Quantity,
                        VariantId = AllocationTransactionDetail.VariantId,
                        StockTypeId = AllocationTransactionDetail.StockTypeId,
                        AllocationTransactionId = allocationTransactionEntity.Id
                    };
                    allocationTransactionEntity.AllocationTransactionDetails.Add(allocationTransactionDetail);
                }
                await _allocationTransactionRepository.AddAsync(allocationTransactionEntity);

                await UpdateStockRequestStatus(allocationTransaction.AllocationTransactionDetails, 
                                                allocationTransaction.FromLocationId, 
                                                allocationTransaction.ToLocationId, 
                                                command.FromDate, 
                                                command.ToDate);
            }
            return true;
        }

        private async Task UpdateStockRequestStatus(List<AllocationTransactionDetailModel> allocationTransactionDetails, Guid FromLocationId, Guid ToLocationId, DateTime FromDate, DateTime ToDate)
        {
            var stockRequests = await _stockRequestRepository.ListAsync(x => x.FromLocationId == FromLocationId 
                                                                               && x.ToLocationId == ToLocationId 
                                                                               && (x.DateRequest.Date >= FromDate.Date && x.DateRequest.Date <= ToDate.Date)
                                                                               && (x.StockRequestStatus == StockRequestStatus.Opened || x.StockRequestStatus == StockRequestStatus.PartialAllocated));

            stockRequests = stockRequests.OrderBy(x => x.DateRequest);

            foreach (var stockRequest in stockRequests)
            {
                var stockRequestItems = await _stockRequestItemRepository.ListAsync(x=> x.StockRequestId == stockRequest.Id);
                if (stockRequestItems != null)
                {
                    foreach (var stockRequestItem in stockRequestItems)
                    {
                        if((stockRequestItem.Quantity - stockRequestItem.AllocateQuantity) > 0)
                        {
                            var allocationTransactionDetail = allocationTransactionDetails.FirstOrDefault(x => x.VariantId == stockRequestItem.VariantId && x.Quantity > 0);
                            if (allocationTransactionDetail != null)
                            {
                                if (allocationTransactionDetail.Quantity < (stockRequestItem.Quantity - stockRequestItem.AllocateQuantity))
                                {
                                    allocationTransactionDetail.Quantity = stockRequestItem.AllocateQuantity + allocationTransactionDetail.Quantity;
                                    stockRequestItem.AllocateQuantity = stockRequestItem.AllocateQuantity + allocationTransactionDetail.Quantity;
                                }
                                else
                                {
                                    allocationTransactionDetail.Quantity = allocationTransactionDetail.Quantity - (stockRequestItem.Quantity - stockRequestItem.AllocateQuantity);
                                    stockRequestItem.AllocateQuantity = stockRequestItem.Quantity;

                                }
                                await _stockRequestItemRepository.UpdateAsync(stockRequestItem);
                            }
                        }
                    }

                    if (stockRequestItems.Where(x => (x.Quantity - x.AllocateQuantity) > 0 && x.AllocateQuantity > 0).ToList() != null)
                    {
                        stockRequest.StockRequestStatus = StockRequestStatus.PartialAllocated;
                    }

                    if (stockRequestItems.FirstOrDefault(x => (x.Quantity - x.AllocateQuantity) > 0) == null)
                    {
                        stockRequest.StockRequestStatus = StockRequestStatus.Allocated;
                    }
                }
                await _stockRequestRepository.UpdateAsync(stockRequest);
            }
        }
    }
}
