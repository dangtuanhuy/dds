﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.AllocationTransactions
{
    public sealed class DeleteAllocationTransactionCommand : ICommand<bool>
    {
        public Guid Id { get; }
        public DeleteAllocationTransactionCommand(Guid id)
        {
            Id = id;
        }
    }

    public sealed class DeleteAllocationTransactionCommandHandler : ICommandHandler<DeleteAllocationTransactionCommand, bool>
    {
        private readonly IEventBus _eventBus;
        private IEfRepository<TransactionDbContext, AllocationTransaction> _repository;

        public DeleteAllocationTransactionCommandHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction> repository,
            IEventBus eventBus)
        {
            _eventBus = eventBus;
            _repository = repository;
        }

        public async Task<bool> Handle(DeleteAllocationTransactionCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Allocation Transaction {command.Id} is not presented.");
            }
            if (entity.Status != AllocationTransactionStatus.Draft)
            {
                throw new ArgumentException($"Can't delete allocation transaction.");
            }
            await _repository.DeleteAsync(entity);

            return true;
        }
    }
}
