﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.AllocationTransactions
{
    public sealed class UpdateStatusAllocationTransactionCommand : ICommand<UpdateStatusAllocationTransactionModel>
    {
        public Guid Id { get; set; }
        public Guid Updater { get; set; }
        public UpdateStatusAllocationTransactionModel UpdateStatusAllocationTransaction { get; set; }
        public UpdateStatusAllocationTransactionCommand(
            Guid id,
            Guid updator,
            UpdateStatusAllocationTransactionModel updateStatusAllocationTransaction)
        {
            Id = id;
            Updater = updator;
            UpdateStatusAllocationTransaction = updateStatusAllocationTransaction;
        }
    }
    public sealed class UpdateStatusAllocationTransactionCommandHandler : ICommandHandler<UpdateStatusAllocationTransactionCommand, UpdateStatusAllocationTransactionModel>
    {
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _allocationTransactionRepository;
        private readonly IMapper _mapper;
        public UpdateStatusAllocationTransactionCommandHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction> allocationTransactionRepository,
            IMapper mapper
            )
        {
            _allocationTransactionRepository = allocationTransactionRepository;
            _mapper = mapper;
        }

        public async Task<UpdateStatusAllocationTransactionModel> Handle(UpdateStatusAllocationTransactionCommand command)
        {
            var entity = await _allocationTransactionRepository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"AllocationTransaction {command.Id} is not presented.");
            }
           
            entity.Status = command.UpdateStatusAllocationTransaction.Status;
            entity.UpdatedBy = command.Updater;

            await _allocationTransactionRepository.UpdateAsync(entity);
            return _mapper.Map<UpdateStatusAllocationTransactionModel>(entity);
        }
    }
}
