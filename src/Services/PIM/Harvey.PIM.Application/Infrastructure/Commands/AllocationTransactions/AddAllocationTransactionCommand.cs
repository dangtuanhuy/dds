﻿using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.AllocationTransactions
{
    public sealed class AddAllocationTransactionCommand : ICommand<AllocationTransactionModel>
    {
        public Guid Creator { get; }
        public string Name { get; }
        public string Description { get; }
        public Guid FromLocationId { get;}
        public Guid ToLocationId { get;}
        public DateTime DeliveryDate { get;}
        public AllocationTransactionStatus Status { get;}
        public List<AllocationTransactionDetailModel> AllocationTransactionDetails { get; set; }
        public AddAllocationTransactionCommand(
            Guid creator,
            string name,
            string description,
            Guid fromLocationId,
            Guid toLocationId,
            DateTime deliveryDate,
            AllocationTransactionStatus status,
            List<AllocationTransactionDetailModel> allocationTransactionDetails)
        {
            Creator = creator;
            Name = name;
            Description = description;
            FromLocationId = fromLocationId;
            ToLocationId = toLocationId;
            DeliveryDate = deliveryDate;
            Status = status;
            AllocationTransactionDetails = allocationTransactionDetails;
        }
    }
    public sealed class AddAllocationTransactionCommandHandler : ICommandHandler<AddAllocationTransactionCommand, AllocationTransactionModel>
    {
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _allocationTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, AllocationTransactionDetail> _allocationTransactionDetailRepository;
        private StockAllocationService _stockAllocationService;
        private readonly IMapper _mapper;
        public AddAllocationTransactionCommandHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction> allocationTransactionRepository,
            IEfRepository<TransactionDbContext, AllocationTransactionDetail> allocationTransactionDetailRepository,
            StockAllocationService stockAllocationService,
            IMapper mapper
            )
        {
            _allocationTransactionRepository = allocationTransactionRepository;
            _allocationTransactionDetailRepository = allocationTransactionDetailRepository;
            _stockAllocationService = stockAllocationService;
            _mapper = mapper;
        }

        public async Task<AllocationTransactionModel> Handle(AddAllocationTransactionCommand command)
        {
            var name = command.Name;
            if(string.IsNullOrEmpty(name))
            {
                name = await _stockAllocationService.GenerateName();
            }
            var allocationTransaction = new AllocationTransaction
            {
                Name = name,
                Description = command.Description,
                FromLocationId = command.FromLocationId,
                ToLocationId = command.ToLocationId,
                DeliveryDate = command.DeliveryDate,
                Status = command.Status,
                UpdatedBy = command.Creator,
                CreatedBy = command.Creator,
                AllocationTransactionDetails = new List<AllocationTransactionDetail>()

            };

            var allocationTransactions = new List<AllocationTransactionDetail>();
            foreach (var AllocationTransactionDetail in command.AllocationTransactionDetails)
            {
                var allocationTransactionDetail = new AllocationTransactionDetail
                {
                    Quantity = AllocationTransactionDetail.Quantity,
                    VariantId = AllocationTransactionDetail.VariantId,
                    StockTypeId = AllocationTransactionDetail.StockTypeId,
                    AllocationTransactionId = allocationTransaction.Id
                };
                allocationTransactions.Add(allocationTransactionDetail);
            }

            allocationTransaction.AllocationTransactionDetails.AddRange(allocationTransactions);

            var entity = await _allocationTransactionRepository.AddAsync(allocationTransaction);
            return _mapper.Map<AllocationTransactionModel>(entity);
        }
    }
}
