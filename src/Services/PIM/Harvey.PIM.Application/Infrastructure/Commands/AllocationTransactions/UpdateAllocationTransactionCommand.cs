﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.AllocationTransactions
{
    public sealed class UpdateAllocationTransactionCommand : ICommand<AllocationTransactionModel>
    {
        public Guid Id { get; set; }
        public Guid Updater { get; set; }
        public AllocationTransactionModel AllocationTransaction { get; set; }
        public UpdateAllocationTransactionCommand(
            Guid id,
            Guid updator,
            AllocationTransactionModel allocationTransaction)
        {
            Id = id;
            Updater = updator;
            AllocationTransaction = allocationTransaction;
        }
    }
    public sealed class UpdateAllocationTransactionCommandHandler : ICommandHandler<UpdateAllocationTransactionCommand, AllocationTransactionModel>
    {
        private readonly IEfRepository<TransactionDbContext, AllocationTransaction> _allocationTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, AllocationTransactionDetail> _allocationTransactionDetailRepository;
        private readonly IMapper _mapper;
        public UpdateAllocationTransactionCommandHandler(
            IEfRepository<TransactionDbContext, AllocationTransaction> allocationTransactionRepository,
            IEfRepository<TransactionDbContext, AllocationTransactionDetail> allocationTransactionDetailRepository,
            IMapper mapper
            )
        {
            _allocationTransactionRepository = allocationTransactionRepository;
            _allocationTransactionDetailRepository = allocationTransactionDetailRepository;
            _mapper = mapper;
        }

        public async Task<AllocationTransactionModel> Handle(UpdateAllocationTransactionCommand command)
        {
            var entity = await _allocationTransactionRepository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"AllocationTransaction {command.Id} is not presented.");
            }
            entity.Name = command.AllocationTransaction.Name;
            entity.Description = command.AllocationTransaction.Description;
            entity.FromLocationId = command.AllocationTransaction.FromLocationId;
            entity.ToLocationId = command.AllocationTransaction.ToLocationId;
            entity.DeliveryDate = command.AllocationTransaction.DeliveryDate;
            entity.Status = command.AllocationTransaction.Status;
            entity.UpdatedBy = command.Updater;

            var allocationTransactionDetails = await
                _allocationTransactionDetailRepository.ListAsync(x => x.AllocationTransactionId == command.Id);


            foreach (var AllocationTransactionDetail in command.AllocationTransaction.AllocationTransactionDetails)
            {
                var allocationTransactionDetailEntity = await
                    _allocationTransactionDetailRepository.GetByIdAsync(AllocationTransactionDetail.Id);
                if (allocationTransactionDetailEntity != null)
                {
                    allocationTransactionDetailEntity.Quantity = AllocationTransactionDetail.Quantity;
                    allocationTransactionDetailEntity.VariantId = AllocationTransactionDetail.VariantId;
                    allocationTransactionDetailEntity.StockTypeId = AllocationTransactionDetail.StockTypeId;
                    await _allocationTransactionDetailRepository.UpdateAsync(allocationTransactionDetailEntity);
                    allocationTransactionDetails = allocationTransactionDetails.Where(x => x.Id != allocationTransactionDetailEntity.Id);
                }
                else
                {
                    var allocationTransactionDetail = new AllocationTransactionDetail
                    {
                        Quantity = AllocationTransactionDetail.Quantity,
                        VariantId = AllocationTransactionDetail.VariantId,
                        StockTypeId = AllocationTransactionDetail.StockTypeId,
                        AllocationTransactionId = entity.Id
                    };
                    await _allocationTransactionDetailRepository.AddAsync(allocationTransactionDetail);
                }
            }
            await _allocationTransactionDetailRepository.DeleteAsync(allocationTransactionDetails.ToList());
            await _allocationTransactionRepository.UpdateAsync(entity);
            return _mapper.Map<AllocationTransactionModel>(entity);
        }
    }
}
