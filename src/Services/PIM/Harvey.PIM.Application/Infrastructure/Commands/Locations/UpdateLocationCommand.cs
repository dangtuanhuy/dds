﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Locations
{
    public sealed class UpdateLocationCommand : ICommand<LocationModel>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public LocationType Type { get; set; }
        public Guid Updater { get; set; }
        public string LocationCode { get; }
        public string ContactPerson { get; }
        public string Phone1 { get; }
        public string Phone2 { get; }
        public UpdateLocationCommand(LocationModel locationModel, Guid updater)
        {
            Id = locationModel.Id;
            Name = locationModel.Name;
            Address = locationModel.Address;
            LocationCode = locationModel.LocationCode;
            Type = locationModel.Type;
            ContactPerson = locationModel.ContactPerson;
            Phone1 = locationModel.Phone1;
            Phone2 = locationModel.Phone2;
            Updater = updater;
        }
    }

    public sealed class UpdateLocationCommandHandler : ICommandHandler<UpdateLocationCommand, LocationModel>
    {
        private readonly IEfRepository<PimDbContext, Location> _repository;
        private readonly IMapper _mapper;
        public UpdateLocationCommandHandler(IEfRepository<PimDbContext, Location> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<LocationModel> Handle(UpdateLocationCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Location {command.Id} is not presented.");
            }

            if (command.LocationCode != entity.LocationCode)
            {
                var location = await _repository.ListAsync(x => x.LocationCode == command.LocationCode);
                if (location.Count() > 0)
                {
                    throw new ArgumentException($"Location code is duplicate.");
                }
            }
            
            entity.Name = command.Name;
            entity.Address = command.Address;
            entity.LocationCode = command.LocationCode;
            entity.Type = command.Type;
            entity.ContactPerson = command.ContactPerson;
            entity.Phone1 = command.Phone1;
            entity.Phone2 = command.Phone2;
            entity.UpdatedBy = command.Updater;

            await _repository.UpdateAsync(entity);

            return _mapper.Map<LocationModel>(entity);
        }
    }
}
