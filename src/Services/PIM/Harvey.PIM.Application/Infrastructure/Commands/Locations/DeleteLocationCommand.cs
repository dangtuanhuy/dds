﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Locations
{
    public sealed class DeleteLocationCommand: ICommand<LocationModel>
    {
        public Guid Id { get; set; }
        public DeleteLocationCommand(Guid id)
        {
            Id = id;
        }
    }
    public sealed class DeleteLocationCommandHandler : ICommandHandler<DeleteLocationCommand, LocationModel>
    {
        private readonly IEfRepository<PimDbContext, Location> _locationRepository;
        private readonly IEfRepository<TransactionDbContext, StockTransaction> _stockTransactionRepository;
        private readonly IEfRepository<TransactionDbContext, StockRequest> _stockRequestRepository;
        private readonly IEfRepository<TransactionDbContext, InventoryTransaction> _inventoryRepository;
        private readonly IMapper _mapper;
        public DeleteLocationCommandHandler(IEfRepository<PimDbContext, Location> locationRepository,
            IEfRepository<TransactionDbContext, StockTransaction> stockTransactionRepository,
            IEfRepository<TransactionDbContext, StockRequest> stockRequestRepository,
            IEfRepository<TransactionDbContext, InventoryTransaction> inventoryRepository,
            IMapper mapper)
        {
            _locationRepository = locationRepository;
            _stockTransactionRepository = stockTransactionRepository;
            _stockRequestRepository = stockRequestRepository;
            _inventoryRepository = inventoryRepository;
            _mapper = mapper;
        }
        public async Task<LocationModel> Handle(DeleteLocationCommand command)
        {
            var entity = await _locationRepository.GetByIdAsync(command.Id);
            if(entity == null)
            {
                throw new ArgumentException($"location {command.Id} is not presented.");
            } else
            {
                var stockRequests = await _stockRequestRepository.GetAsync();
                var stockTransactions = await _stockTransactionRepository.GetAsync();
                var inventoryTransactions = await _inventoryRepository.GetAsync();
                if (stockRequests.FirstOrDefault(x => x.ToLocationId == entity.Id) != null ||
                    stockTransactions.FirstOrDefault(x => x.LocationId == entity.Id) != null ||
                    inventoryTransactions.FirstOrDefault(x => x.FromLocationId == entity.Id) != null ||
                    inventoryTransactions.FirstOrDefault(x => x.ToLocationId == entity.Id) != null )
                {
                    throw new ArgumentException($"Can't delete location. It's being used");
                } else
                {
                    await _locationRepository.DeleteAsync(entity);
                    await _locationRepository.SaveChangesAsync();
                }
            }

            return _mapper.Map<LocationModel>(entity);
        }
    }
}
