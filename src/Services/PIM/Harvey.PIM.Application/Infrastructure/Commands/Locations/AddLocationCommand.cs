﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Locations
{
    public sealed class AddLocationCommand : ICommand<LocationModel>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public LocationType Type { get; set; }
        public Guid Creator { get; }
        public string LocationCode { get;}
        public string ContactPerson { get; }
        public string Phone1 { get; }
        public string Phone2 { get; }
        public AddLocationCommand(LocationModel locationModel, Guid creator)
        {
            Name = locationModel.Name;
            Address = locationModel.Address;
            Type = locationModel.Type;
            LocationCode = locationModel.LocationCode;
            ContactPerson = locationModel.ContactPerson;
            Phone1 = locationModel.Phone1;
            Phone2 = locationModel.Phone2;
            Creator = creator;
        }
    }

    public sealed class AddLocationCommandHandler : ICommandHandler<AddLocationCommand, LocationModel>
    {
        private readonly IEfRepository<PimDbContext, Location> _repository;
        private readonly IMapper _mapper;
        public AddLocationCommandHandler(IEfRepository<PimDbContext, Location> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<LocationModel> Handle(AddLocationCommand command)
        {
            var locations = await _repository.GetAsync();
            var location = locations.FirstOrDefault(x => x.LocationCode == command.LocationCode);
            Location newLocation = new Location();
            if (location == null)
            {
                newLocation = new Location()
                {
                    Name = command.Name,
                    Address = command.Address,
                    Type = command.Type,
                    LocationCode = command.LocationCode,
                    ContactPerson = command.ContactPerson,
                    Phone1 = command.Phone1,
                    Phone2 = command.Phone2,
                    CreatedBy = command.Creator,
                    UpdatedBy = command.Creator
                };

                var entity = await _repository.AddAsync(newLocation);

                return _mapper.Map<LocationModel>(entity);
            }

            return _mapper.Map<LocationModel>(newLocation);
        }
    }
}
