﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Channels.Services;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.AssortmentAssignments
{
    public class AddAssortmentAssignmentCommand : ICommand<bool>
    {
        public List<AddAssortmentAssignmentModel> Assignments { get; }
        public Guid AssortmentId { get; }

        public AddAssortmentAssignmentCommand(List<AddAssortmentAssignmentModel> assignments, Guid assortmentId)
        {
            Assignments = assignments;
            AssortmentId = assortmentId;
        }
    }

    public class AddAssortmentAssignmentCommandHandler : ICommandHandler<AddAssortmentAssignmentCommand, bool>
    {
        private readonly IEfRepository<PimDbContext, AssortmentAssignment> _repository;
        private readonly IEfRepository<PimDbContext, ChannelAssignment> _channelAssignmentRepository;
        private readonly IAssignmentService _assignmentService;
        public AddAssortmentAssignmentCommandHandler(
            IEfRepository<PimDbContext, AssortmentAssignment> repository,
            IAssignmentService assignmentService,
            IEfRepository<PimDbContext, ChannelAssignment> channelAssignmentRepository)
        {
            _repository = repository;
            _assignmentService = assignmentService;
            _channelAssignmentRepository = channelAssignmentRepository;
        }

        public async Task<bool> Handle(AddAssortmentAssignmentCommand command)
        {
            var assignments = new List<AssortmentAssignment>();
            var assignmentSelected = await _repository.ListAsync(x => x.AssortmentId == command.AssortmentId);
            var channelAssignments = await _channelAssignmentRepository.ListAsync(x => x.ReferenceId == command.AssortmentId && x.EntityType == ChannelAssignmentType.Assortment);
            command.Assignments.ForEach(x =>
            {
                var assignmentsSelect = new AssortmentAssignment()
                {
                    AssortmentId = command.AssortmentId,
                    ReferenceId = x.ReferenceId,
                    EntityType = x.EntityType,
                };
                assignments.Add(assignmentsSelect);

            });
            await _repository.DeleteAsync(assignmentSelected.ToList());
            foreach (var item in channelAssignments)
            {
                await _assignmentService.ApplyAssortmentAssignmentToSynchronization(SynchAction.Delete, item.ChannelId, assignmentSelected.ToList());
            }
            await _repository.AddAsync(assignments);
            foreach (var item in channelAssignments)
            {
                await _assignmentService.ApplyAssortmentAssignmentToSynchronization(SynchAction.Add, item.ChannelId, assignments);
            }
            return true;
        }
    }
}
