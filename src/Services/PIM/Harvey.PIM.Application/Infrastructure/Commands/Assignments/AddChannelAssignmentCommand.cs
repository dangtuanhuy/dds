﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Channels.Services;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;

namespace Harvey.PIM.Application.Infrastructure.Commands.Assignments
{
    public class AddChannelAssignmentCommand : ICommand<bool>
    {
        public Guid ChannelId { get; set; }
        public List<AddChannelAssignmentModel> ChannelAssignments { get; set; }
        public AddChannelAssignmentCommand(List<AddChannelAssignmentModel> channelAssignments, Guid channelId)
        {
            ChannelAssignments = channelAssignments;
            ChannelId = channelId;
        }
    }

    public class AddChannelAssignmentCommandHandler : ICommandHandler<AddChannelAssignmentCommand, bool>
    {
        private readonly IEfRepository<PimDbContext, ChannelAssignment> _repository;
        private readonly IAssignmentService _assignmentService;
        public AddChannelAssignmentCommandHandler(
            IEfRepository<PimDbContext, ChannelAssignment> repository,
            IAssignmentService assignmentService)
        {
            _repository = repository;
            _assignmentService = assignmentService;
        }
        public async Task<bool> Handle(AddChannelAssignmentCommand command)
        {
            var channelAssignments = new List<ChannelAssignment>();
            var channelSelected = await _repository.ListAsync(x => x.ChannelId == command.ChannelId);
            command.ChannelAssignments.ForEach(x =>
            {
                var channelAssignment = new ChannelAssignment()
                {
                    ChannelId = command.ChannelId,
                    EntityType = x.EntityType,
                    ReferenceId = x.ReferenceId,
                };
                channelAssignments.Add(channelAssignment);
            });
            await _repository.DeleteAsync(channelSelected.ToList());
            //await _assignmentService.ApplyChannelAssignmentToSynchronization(SynchAction.Delete, command.ChannelId, channelSelected.ToList());
            await _repository.AddAsync(channelAssignments);
            //await _assignmentService.ApplyChannelAssignmentToSynchronization(SynchAction.Add, command.ChannelId, channelAssignments);
            return true;
        }
    }
}
