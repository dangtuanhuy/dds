﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Channels.Products;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Npgsql;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Channels
{
    public class DeleteChannelCommand : ICommand<bool>
    {
        public Guid Id { get; }
        public DeleteChannelCommand(Guid id)
        {
            Id = id;
        }
    }

    public class DeleteChannelCommandHandler : ICommandHandler<DeleteChannelCommand, bool>
    {
        private IEfRepository<PimDbContext, Channel> _repository;
        private IEfRepository<PimDbContext, ChannelAssignment> _assigmentRepository;
        private IEfRepository<PimDbContext, ChannelStoreAssignment> _assigmentStoreRepository;
        private readonly ApplicationBuilder _applicationBuilder;
        private readonly IEventBus _eventBus;
        private readonly ILogger<DeleteChannelCommandHandler> _logger;
        public DeleteChannelCommandHandler(IEfRepository<PimDbContext, Channel> repository,
                                            IEfRepository<PimDbContext, ChannelAssignment> assigmentRepository,
                                            IEfRepository<PimDbContext, ChannelStoreAssignment> assigmentStoreRepository,
                                            ApplicationBuilder applicationBuilder,
                                            IEventBus eventBus,
                                            ILogger<DeleteChannelCommandHandler> _logger)
        {
            _repository = repository;
            _assigmentRepository = assigmentRepository;
            _assigmentStoreRepository = assigmentStoreRepository;
            _applicationBuilder = applicationBuilder;
            _eventBus = eventBus;
        }

        public async Task<bool> Handle(DeleteChannelCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Channel {command.Id} is not presented.");
            }

            var channelAssignment = await _assigmentRepository.ListAsync(x => x.ChannelId == command.Id && x.EntityType == ChannelAssignmentType.Assortment);
            var channelStoreAssignment = await _assigmentStoreRepository.ListAsync(x => x.ChannelId == command.Id);

            if (entity.IsProvision)
            {
                if (channelAssignment.ToList().Count > 0 || channelStoreAssignment.ToList().Count > 0)
                {
                    throw new ArgumentException($"Channel can not delete. Please check again.");
                }
                else
                {
                    await DropDatabase(entity.ServerInformation, entity);
                    _applicationBuilder.AddConnector(entity.Id, entity.Name, _eventBus, (connectorRegistration) =>
                    {
                        connectorRegistration
                        .DeleteFeedService<ProductFeed, CatalogProductFeed>(FeedType.Product)
                        .DeleteFeedService<Variant, CatalogVariant>(FeedType.Variant)
                        .DeleteFeedService<Category, CatalogCategory>(FeedType.Category)
                        .DeleteFeedService<Price, CatalogPrice>(FeedType.Price)
                        .DeleteFeedService<StockType, CatalogPrice>(FeedType.StockType)
                        .DeleteFeedService<TransactionType, CatalogTransactionType>(FeedType.TransactionType);
                    });
                }
            }
            else
            {
                await _repository.DeleteAsync(entity);
            }
            return true;
        }

        private async Task DropDatabase(string connectionString, Channel entity)
        {
            NpgsqlConnection conn = new NpgsqlConnection(connectionString);

            var dropConnectSql = $"SELECT pg_terminate_backend(pg_stat_activity.pid) " +
                                    $"FROM pg_stat_activity " +
                                    $"WHERE pg_stat_activity.datname = '{conn.Database}' " +
                                    $"AND pid<> pg_backend_pid();";

            var dropDBSql = $"DROP DATABASE \"{conn.Database}\"";
            var masterConnectionString = connectionString.Replace(conn.Database, "postgres");
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(masterConnectionString);
            bool gotError = false;
            System.Exception exceiption = null;
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                try
                {
                    dbContext.Database.ExecuteSqlCommand(dropConnectSql);
                    dbContext.Database.ExecuteSqlCommand(dropDBSql);
                    await _repository.DeleteAsync(entity);
                }
                catch(System.Exception ex)
                {
                    gotError = true;
                    exceiption = ex;
                }
            }

            if (gotError)
            {
                _logger.LogError($"Cannot delete database {conn.Database} at server {conn.Host}", exceiption);
                throw new ArgumentException("Check connect database");
            }
        }
    }
}
