﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Channels
{
    public class AddChannelStoreAssignmentCommand : ICommand<bool>
    {
        public Guid ChannelId { get; set; }
        public List<AddChannelStoreAssignmentModel> ChannelStoreAssignments { get; set; }
        public AddChannelStoreAssignmentCommand(List<AddChannelStoreAssignmentModel> channelStoreAssignments, Guid channelId)
        {
            ChannelStoreAssignments = channelStoreAssignments;
            ChannelId = channelId;
        }
    }

    public class AddChannelStoreAssignmentCommandHandler : ICommandHandler<AddChannelStoreAssignmentCommand, bool>
    {
        private readonly IEfRepository<PimDbContext, ChannelStoreAssignment> _repository;
        public AddChannelStoreAssignmentCommandHandler(IEfRepository<PimDbContext, ChannelStoreAssignment> repository)
        {
            _repository = repository;
        }
        public async Task<bool> Handle(AddChannelStoreAssignmentCommand command)
        {
            var existingAssignments = (await _repository.ListAsync(x => x.ChannelId == command.ChannelId)).ToList();
            var channelStoreAssignments = 
                command.ChannelStoreAssignments.Select(x =>
                    new ChannelStoreAssignment()
                    {
                        ChannelId = command.ChannelId,
                        StoreId = x.StoreId
                    }).ToList();

            if (existingAssignments.Count > 0)
            {
                await _repository.DeleteAsync(existingAssignments.ToList());
            }

            if (channelStoreAssignments.Count > 0)
            {
                await _repository.AddAsync(channelStoreAssignments);
            }

            return true;
        }
    }
}
