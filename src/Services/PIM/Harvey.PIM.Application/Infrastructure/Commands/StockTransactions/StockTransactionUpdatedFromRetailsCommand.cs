﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.EventBus.Events.StockLevels;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Services;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Harvey.PIM.Application.Infrastructure.Indexing.StockLevelStockTypeSearchItem;

namespace Harvey.PIM.Application.Infrastructure.Commands.StockTransactions
{
    public class StockTransactionUpdatedFromRetailsCommand : ICommand<List<StockTransactionUpdatedRetailsModel>>
    {
        public Guid LocationId { get; set; }
        public List<OrderItemRequestModel> Items { get; set; }
        public StockTransactionUpdatedFromRetailsCommand(Guid locationId, List<OrderItemRequestModel> items)
        {
            LocationId = locationId;
            Items = items;
        }
    }

    public sealed class StockTransactionUpdatedFromRetailsCommandHandler : ICommandHandler<StockTransactionUpdatedFromRetailsCommand, List<StockTransactionUpdatedRetailsModel>>
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly IEventBus _eventBus;
        private readonly ISearchService _searchService;
        private readonly InventoryTransactionService _inventoryTransactionService;
        private readonly StockTransactionService _stockTransactionService;
        public StockTransactionUpdatedFromRetailsCommandHandler(TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext,
            IEventBus eventBus,
            ISearchService searchService,
            InventoryTransactionService inventoryTransactionService,
            StockTransactionService stockTransactionService)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _eventBus = eventBus;
            _searchService = searchService;
            _inventoryTransactionService = inventoryTransactionService;
            _stockTransactionService = stockTransactionService;
        }
        public async Task<List<StockTransactionUpdatedRetailsModel>> Handle(StockTransactionUpdatedFromRetailsCommand command)
        {
            var newStockTransactionResponse = new List<StockTransactionUpdatedRetailsModel>();
            var inventoryTransactionSale = new InventoryTransaction();
            var inventoryTransactionRefund = new InventoryTransaction();
            var inventoryTransactionExchange = new InventoryTransaction();
            var orderItems = command.Items;
            if (orderItems == null || orderItems.Count == 0)
            {
                return newStockTransactionResponse;
            }

            var newStockTransactions = new List<StockTransaction>();
            var locationId = command.LocationId;

            var _variantIds = orderItems.Select(x => x.VariantId).Distinct();
            var variants = await _pimDbContext.Variants.Where(x => _variantIds.Contains(x.Id)).ToListAsync();
            var dbStockTransactions = await _transactionDbContext.StockTransactions.AsNoTracking()
                                .Where(x => x.LocationId == locationId && _variantIds.Contains(x.VariantId)).ToListAsync();

            var stockTypes = await _transactionDbContext.StockTypes.AsNoTracking().ToListAsync();
            var vendors = await _pimDbContext.Vendors.AsNoTracking().ToListAsync();
            var location = _pimDbContext.Locations.FirstOrDefault(x => x.Id == locationId);
            var orderId = orderItems.FirstOrDefault().OrderId;

            var transactionTypeCodes = new List<string>()
            {
                TransactionTypeCode.Sale.ToString(),
                TransactionTypeCode.Exchange.ToString(),
                TransactionTypeCode.Refund.ToString()
            };
            var transactionTypes = await _transactionDbContext.TransactionTypes.Where(x => transactionTypeCodes.Contains(x.Code)).ToListAsync();
            var transactionTypeSaleId = transactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.Sale.ToString()).Id;
            var transactionTypeExchangeId = transactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.Exchange.ToString()).Id;
            var transactionTypeRefundId = transactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.Refund.ToString()).Id;

            var codeInventoryTransactionSale = await _inventoryTransactionService.GenerateInventoryTransactionCode(TransactionTypeCode.Sale.ToString());
            var codeInventoryTransactionExchange = await _inventoryTransactionService.GenerateInventoryTransactionCode(TransactionTypeCode.Exchange.ToString());
            var codeInventoryTransactionRefund = await _inventoryTransactionService.GenerateInventoryTransactionCode(TransactionTypeCode.Refund.ToString());

            var saleOrderItems = await _stockTransactionService.GetInfoSaleOrderItem(orderItems, variants);

            var isRefund = saleOrderItems.FirstOrDefault(x => x.Status == OrderItemStatus.Refund) != null ? true : false;
            var isExchange = saleOrderItems.FirstOrDefault(x => x.Status == OrderItemStatus.Exchange) != null ? true : false;
            var isSale = saleOrderItems.FirstOrDefault(x => x.Status == OrderItemStatus.Normal) != null ? true : false;

            if (isSale)
            {
                inventoryTransactionSale.Id = Guid.NewGuid();
                inventoryTransactionSale.TransactionRefId = orderId;
                inventoryTransactionSale.FromLocationId = locationId;
                inventoryTransactionSale.ToLocationId = locationId;
                inventoryTransactionSale.TransactionTypeId = transactionTypeSaleId;
                inventoryTransactionSale.Code = codeInventoryTransactionSale;
                inventoryTransactionSale.CreatedBy = Guid.Empty; // TO DO: Need StaffId
                inventoryTransactionSale.CreatedDate = DateTime.UtcNow;
                await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransactionSale);
            }

            if (isRefund)
            {
                inventoryTransactionRefund.Id = Guid.NewGuid();
                inventoryTransactionRefund.TransactionRefId = orderId;
                inventoryTransactionRefund.FromLocationId = locationId;
                inventoryTransactionRefund.ToLocationId = locationId;
                inventoryTransactionRefund.TransactionTypeId = transactionTypeRefundId;
                inventoryTransactionRefund.Code = codeInventoryTransactionRefund;
                inventoryTransactionRefund.CreatedBy = Guid.Empty; // TO DO: Need StaffId
                inventoryTransactionRefund.CreatedDate = DateTime.UtcNow;
                await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransactionRefund);
            }

            if (isExchange)
            {
                inventoryTransactionExchange.Id = Guid.NewGuid();
                inventoryTransactionExchange.TransactionRefId = orderId;
                inventoryTransactionExchange.FromLocationId = locationId;
                inventoryTransactionExchange.ToLocationId = locationId;
                inventoryTransactionExchange.TransactionTypeId = transactionTypeExchangeId;
                inventoryTransactionExchange.Code = codeInventoryTransactionExchange;
                inventoryTransactionExchange.CreatedBy = Guid.Empty; // TO DO: Need StaffId
                inventoryTransactionExchange.CreatedDate = DateTime.UtcNow;
                await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransactionExchange);
            }

            foreach (var orderItem in saleOrderItems)
            {
                var variant = variants.FirstOrDefault(x => x.Id == orderItem.VariantId);
                var vendor = vendors.FirstOrDefault(x => x.Id == orderItem.VendorId);
                var stockType = stockTypes.FirstOrDefault(x => x.Id == orderItem.StockTypeId);

                var referenceIdWithStockType = $"{variant.SKUCode}{stockType.Code}";
                if (vendor != null)
                {
                    referenceIdWithStockType += vendor.VendorCode ?? "";
                }

                var referenceId = $"{variant.SKUCode}{location.LocationCode}";
                var lastestStockTransaction = GetLastestDbStockTransaction(referenceId, dbStockTransactions, newStockTransactions);
                if (lastestStockTransaction != null)
                {
                    var newStockTransaction = new StockTransaction();
                    if (orderItem.Status == OrderItemStatus.Normal)
                    {
                        newStockTransaction.Id = Guid.NewGuid();
                        newStockTransaction.InventoryTransactionId = inventoryTransactionSale.Id;
                        newStockTransaction.LocationId = locationId;
                        newStockTransaction.VariantId = orderItem.VariantId;
                        newStockTransaction.StockTypeId = Guid.Empty;
                        newStockTransaction.TransactionTypeId = transactionTypeSaleId;
                        newStockTransaction.StockTransactionRefId = orderItem.OrderId;
                        newStockTransaction.Quantity = orderItem.Quantity;
                        newStockTransaction.Balance = lastestStockTransaction.Balance - orderItem.Quantity > 0 ? lastestStockTransaction.Balance - orderItem.Quantity : 0;
                        newStockTransaction.CreatedDate = DateTime.UtcNow;
                        newStockTransaction.CreatedBy = Guid.Empty; // Need staffId
                        newStockTransaction.ReferenceId = referenceId;

                        await _transactionDbContext.StockTransactions.AddAsync(newStockTransaction);
                        newStockTransactions.Add(newStockTransaction);
                    }
                    else
                    {
                        newStockTransaction.Id = Guid.NewGuid();
                        newStockTransaction.LocationId = locationId;
                        newStockTransaction.VariantId = orderItem.VariantId;
                        newStockTransaction.StockTypeId = Guid.Empty;
                        newStockTransaction.StockTransactionRefId = orderItem.OrderId;
                        newStockTransaction.Quantity = orderItem.Quantity;
                        newStockTransaction.Balance = lastestStockTransaction.Balance + orderItem.Quantity;
                        newStockTransaction.CreatedDate = DateTime.UtcNow;
                        newStockTransaction.CreatedBy = Guid.Empty; // Need staffId
                        newStockTransaction.ReferenceId = referenceId;

                        if (orderItem.Status == OrderItemStatus.Refund)
                        {
                            newStockTransaction.InventoryTransactionId = inventoryTransactionRefund.Id;
                            newStockTransaction.TransactionTypeId = transactionTypeRefundId;
                        }
                        else if (orderItem.Status == OrderItemStatus.Exchange)
                        {
                            newStockTransaction.InventoryTransactionId = inventoryTransactionExchange.Id;
                            newStockTransaction.TransactionTypeId = transactionTypeExchangeId;
                        }

                        await _transactionDbContext.StockTransactions.AddAsync(newStockTransaction);
                        newStockTransactions.Add(newStockTransaction);
                    }


                    var stockTransactionResponse = new StockTransactionUpdatedRetailsModel()
                    {
                        Id = newStockTransaction.Id,
                        InventoryTransactionId = newStockTransaction.InventoryTransactionId,
                        LocationId = newStockTransaction.LocationId,
                        VariantId = newStockTransaction.VariantId,
                        StockTypeId = orderItem.StockTypeId,
                        TransactionTypeId = newStockTransaction.TransactionTypeId,
                        StockTransactionRefId = orderItem.Id,
                        Quantity = newStockTransaction.Quantity,
                        Balance = newStockTransaction.Balance,
                        CreatedBy = newStockTransaction.CreatedBy,
                        CreatedDate = newStockTransaction.CreatedDate,
                        Cost = orderItem.CostValue,
                        VariantName = orderItem.VariantName,
                        VendorId = orderItem.VendorId,
                        WareHouseId = orderItem.WareHouseId,
                        GIWDocumentItemId = orderItem.GIWDocumentItemId,
                        ReferenceId = referenceIdWithStockType,
                        Status = orderItem.Status
                    };

                    newStockTransactionResponse.Add(stockTransactionResponse);
                }
            }
            
            await _transactionDbContext.SaveChangesAsync();

            //await _eventBus.PublishAsync(new StockLevelUpdatedEvent()
            //{
            //    LocationId = command.LocationId
            //});

            //var stockAndPrices = newStockTransactions.Select(x => new StockAndPriceLevelModel()
            //{
            //    LocationId = x.LocationId,
            //    ProductId = variants.SingleOrDefault(y => y.Id == x.VariantId).ProductId,
            //    VariantId = x.VariantId,
            //    SKUCode = variants.SingleOrDefault(y => y.Id == x.VariantId).SKUCode,
            //}).Cast<dynamic>().ToList();
            //await _eventBus.PublishAsync(new StockAndPriceLevelUpdatedEvent()
            //{
            //    stockAndPrices = stockAndPrices
            //});

            return newStockTransactionResponse;
        }

        private StockTransaction GetLastestDbStockTransaction(string referenceId, List<StockTransaction> dbStockTransactions, List<StockTransaction> newStockTransactions)
        {
            var newStockTransaction = newStockTransactions.Where(x => x.ReferenceId == referenceId)
                                                           .OrderByDescending(x => x.CreatedDate)
                                                           .FirstOrDefault();
            if (newStockTransaction != null)
            {
                return newStockTransaction;
            }

            var dbStockTransaction = dbStockTransactions.Where(x => x.ReferenceId == referenceId)
                                                            .OrderByDescending(x => x.CreatedDate)
                                                            .FirstOrDefault();
            if (dbStockTransaction != null)
            {
                return dbStockTransaction;
            }

            return null;
        }
    }
}
