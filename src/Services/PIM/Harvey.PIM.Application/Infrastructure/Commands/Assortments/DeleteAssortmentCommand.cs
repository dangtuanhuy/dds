﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Assortments
{
    public sealed class DeleteAssortmentCommand : ICommand<bool>
    {
        public Guid Id { get; }
        public DeleteAssortmentCommand(Guid id)
        {
            Id = id;
        }
    }

    public sealed class DeleteAssortmentCommandHandler : ICommandHandler<DeleteAssortmentCommand, bool>
    {
        private readonly IEventBus _eventBus;
        private IEfRepository<PimDbContext, Assortment> _repository;
        private IEfRepository<PimDbContext, ChannelAssignment> _channelAssignmentRepository;

        public DeleteAssortmentCommandHandler(
            IEfRepository<PimDbContext, Assortment> repository,
            IEfRepository<PimDbContext, ChannelAssignment> channelAssignmentRepository,
            IEventBus eventBus)
        {
            _eventBus = eventBus;
            _repository = repository;
            _channelAssignmentRepository = channelAssignmentRepository;
        }

        public async Task<bool> Handle(DeleteAssortmentCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Assortment {command.Id} is not presented.");
            }

            var channelAssignments = await _channelAssignmentRepository.ListAsync(x => x.ReferenceId == command.Id && x.EntityType == ChannelAssignmentType.Assortment);

            if (channelAssignments.ToList().Count > 0)
            {
                throw new ArgumentException($"Assortment is being assigned.");
            }

            await _repository.DeleteAsync(entity);

            return true;
        }
    }
}
