﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Categories;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;

namespace Harvey.PIM.Application.Infrastructure.Commands.Categories
{
    public sealed class UpdateCategoryCommand : ICommand<CategoryModel>
    {
        public Guid Updater { get; }
        public Guid Id { get; }
        public string Name { get; }
        public string Description { get; }
        public string IdentifiedId { get; }
        public UpdateCategoryCommand(Guid updater, Guid id, string name, string description, string identifiedId)
        {
            Updater = updater;
            Id = id;
            Name = name;
            Description = description;
            IdentifiedId = identifiedId;
        }
    }

    public sealed class UpdateCategoryCommandHandler : ICommandHandler<UpdateCategoryCommand, CategoryModel>
    {
        private readonly IEfRepository<PimDbContext, Category> _categoryRepository;
        private readonly IEfRepository<PimDbContext, Product> _productRepository;
        private readonly IEventBus _eventBus;
        private readonly IMapper _mapper;
        public UpdateCategoryCommandHandler(
            IEfRepository<PimDbContext, Category> categoryRepository,
            IEfRepository<PimDbContext, Product> productRepository,
            IEventBus eventBus,
            IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
            _eventBus = eventBus;
            _mapper = mapper;
        }
        public async Task<CategoryModel> Handle(UpdateCategoryCommand command)
        {
            var entity = await _categoryRepository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"category {command.Id} is not presented.");
            }

            entity.UpdatedBy = command.Updater;
            entity.Name = command.Name;
            entity.Description = command.Description;
            entity.IdentifiedId = command.IdentifiedId;
            await _categoryRepository.UpdateAsync(entity);
            await _eventBus.PublishAsync(new CategoryUpdatedEvent(entity.Id.ToString())
            {
                Name = entity.Name,
                Description = entity.Description
            });
            return _mapper.Map<CategoryModel>(entity);
        }
    }
}
