﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.Categories
{
    public sealed class DeleteCategoryCommand : ICommand<bool>
    {
        public Guid Id { get; }
        public DeleteCategoryCommand(Guid id)
        {
            Id = id;
        }
    }

    public sealed class DeleteCategoryCommandHandler : ICommandHandler<DeleteCategoryCommand, bool>
    {
        private readonly IEventBus _eventBus;
        private IEfRepository<PimDbContext, Category> _repository;
        private readonly PimDbContext _pimDbContext;

        public DeleteCategoryCommandHandler(
            IEfRepository<PimDbContext, Category> repository,
            IEventBus eventBus,
            PimDbContext pimDbContext)
        {
            _eventBus = eventBus;
            _repository = repository;
            _pimDbContext = pimDbContext;
        }

        public async Task<bool> Handle(DeleteCategoryCommand command)
        {
            var entity = await _repository.GetByIdAsync(command.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Category {command.Id} is not presented.");
            }

            var product = _pimDbContext.Products.FirstOrDefault(x => x.CategoryId == entity.Id);
            if (product != null)
            {
                throw new ArgumentException($"Can't delete category. It's being used");
            } else
            {
                entity.IsDelete = true;
                await _repository.UpdateAsync(entity);
            }
            return true; 
        }
    }
}
