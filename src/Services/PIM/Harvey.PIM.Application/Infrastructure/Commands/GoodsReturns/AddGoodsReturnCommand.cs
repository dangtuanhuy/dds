﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.EventBus.Events.StockLevels;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Services;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure.Commands.GoodsReturns
{
    public sealed class AddGoodsReturnCommand : ICommand<Guid>
    {
        public GoodsReturnDocumentModel GoodsReturn { get; set; }
        public Guid UserId { get; set; }
        public AddGoodsReturnCommand(GoodsReturnDocumentModel goodsReturn,
            Guid userId)
        {
            GoodsReturn = goodsReturn;
            UserId = userId;
        }
    }

    public sealed class AddGoodsReturnCommandHandler : ICommandHandler<AddGoodsReturnCommand, Guid>
    {
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        private readonly InventoryTransactionService _inventoryTransactionService;
        private readonly ISearchService _searchService;
        private readonly IEfRepository<PimDbContext, PurchaseOrder> _poRepository;
        private readonly IEfRepository<PimDbContext, PurchaseOrderItem> _poItemsRepository;
        private readonly IEventBus _eventBus;
        public AddGoodsReturnCommandHandler(PimDbContext pimDbContext,
            TransactionDbContext transactionDbContext,
            InventoryTransactionService inventoryTransactionService,
            ISearchService searchService,
            IEfRepository<PimDbContext, PurchaseOrder> poRepository,
            IEfRepository<PimDbContext, PurchaseOrderItem> poItemsRepository,
            IEventBus eventBus)
        {
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
            _inventoryTransactionService = inventoryTransactionService;
            _searchService = searchService;
            _poRepository = poRepository;
            _poItemsRepository = poItemsRepository;
            _eventBus = eventBus;
        }

        public async Task<Guid> Handle(AddGoodsReturnCommand command)
        {
            var goodsReturnCommand = command.GoodsReturn;

            if(goodsReturnCommand.Items == null || goodsReturnCommand.Items.Count <= 0)
            {
                throw new ArgumentException($"List item is empty. Please try again!");
            }

            var goodsReturnDocument = new GoodsReturnDocument()
            {
                Id = Guid.NewGuid(),
                Name = goodsReturnCommand.Name,
                Description = goodsReturnCommand.Description,
                CreatedBy = command.UserId,
                CreatedDate = DateTime.UtcNow,
                LocationdId = goodsReturnCommand.FromLocationId,
                VendorId = goodsReturnCommand.ToLocationId
            };
            await _transactionDbContext.GoodsReturnDocuments.AddAsync(goodsReturnDocument);

            var codeInventoryTransaction = await _inventoryTransactionService.GenerateInventoryTransactionCode(TransactionTypeCode.GRT.ToString());
            var TransactionTypeGRT = _transactionDbContext.TransactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.GRT.ToString());
            var inventoryTransaction = new InventoryTransaction
            {
                Id = Guid.NewGuid(),
                TransactionRefId = goodsReturnDocument.Id,
                FromLocationId = goodsReturnCommand.FromLocationId,
                ToLocationId = goodsReturnCommand.ToLocationId,
                TransactionTypeId = TransactionTypeGRT.Id,
                CreatedDate = DateTime.UtcNow,
                CreatedBy = command.UserId,
                Code = codeInventoryTransaction
            };
            await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransaction);

            var variants = _pimDbContext.Variants.AsNoTracking().ToList();
            var location = _pimDbContext.Locations.FirstOrDefault(x => x.Id == goodsReturnCommand.FromLocationId);
            var stockTransactions = _transactionDbContext.StockTransactions.OrderByDescending(x => x.CreatedDate).Where(x => x.LocationId == location.Id).ToList();
            var stockTypes = _transactionDbContext.StockTypes.AsNoTracking().ToList();
            var purchaseOrderItems = _pimDbContext.purchaseOrderItems.AsNoTracking().ToList();

            var newStockTransactions = new List<StockTransaction>();
            var stockLevelIndexesUpdated = new List<StockLevelSearchItem>();
            var goodsReturned = new List<GoodsReturnDocumentItemModel>();

            foreach (var item in goodsReturnCommand.Items)
            {
                var variant = variants.FirstOrDefault(x => x.Id == item.VariantId);
                var stockType = stockTypes.FirstOrDefault(x => x.Id == item.StockTypeId);
                var referenceId = $"{variant.SKUCode}{location.LocationCode}";
                var stockTransaction = stockTransactions.FirstOrDefault(x => x.ReferenceId == referenceId);
                if (stockTransaction != null)
                {
                    var lastStockTransaction = newStockTransactions.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.ReferenceId == referenceId) != null ?
                        newStockTransactions.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.ReferenceId == referenceId) : stockTransaction;
                    var newBalance = lastStockTransaction.Balance - item.Quantity;
                    if (newBalance < 0)
                    {
                        throw new ArgumentException($"Any item does not available stock in {location.Name}. Please try again!");
                    }
                    else
                    {
                        var poItem = purchaseOrderItems.FirstOrDefault(x => x.PurchaseOrderId == item.ReturnOrderId && x.VariantId == item.VariantId && x.StockTypeId == item.StockTypeId);
                        var goodsReturnDocumentItem = new GoodsReturnDocumentItem()
                        {
                            Id = Guid.NewGuid(),
                            GoodsReturnDocumentId = goodsReturnDocument.Id,
                            VariantId = item.VariantId,
                            StockTypeId = item.StockTypeId,
                            Quantity = item.Quantity,
                            CostValue = poItem.CostValue,
                            CurrencyCode = poItem.CurrencyCode,
                            TransactionRefId = item.ReturnOrderId
                        };
                        await _transactionDbContext.GoodsReturnDocumentItems.AddAsync(goodsReturnDocumentItem);

                        var giwDocumentItems = _transactionDbContext.GIWDocuments.Where(x => x.LocationId == command.GoodsReturn.FromLocationId).Include(x => x.GIWDocumentItems).ThenInclude(x => x.StockType)
                            .OrderByDescending(x => x.CreatedDate)
                            .Select(x => new
                            {
                                GIWItems = x.GIWDocumentItems.Where(y => y.VariantId == goodsReturnDocumentItem.VariantId && y.StockType.Id == goodsReturnDocumentItem.StockTypeId && y.RemainingQty > 0),
                            }).SelectMany(x => x.GIWItems, (GIWItem, Item) =>
                            new { Item.Id, Item.GIWDocumentId, Item.VariantId, Item.Quantity, Item.RemainingQty, Item.CostValue })
                            .Where(x => x.VariantId == goodsReturnDocumentItem.VariantId);

                        if (giwDocumentItems != null)
                        {
                            var goodsReturnQuantity = item.Quantity;
                            foreach (var giwItem in giwDocumentItems)
                            {
                                var giwDocumentItem = _transactionDbContext.GIWDocumentItems.FirstOrDefault(x => x.Id == giwItem.Id);
                                if (giwItem.RemainingQty - goodsReturnQuantity >= 0 && goodsReturnQuantity > 0)
                                {
                                    giwDocumentItem.RemainingQty = giwDocumentItem.RemainingQty - goodsReturnQuantity;

                                    goodsReturned.Add(new GoodsReturnDocumentItemModel
                                    {
                                        ReturnOrderId = item.ReturnOrderId,
                                        VariantId = item.VariantId,
                                        StockTypeId = item.StockTypeId,
                                        Quantity = goodsReturnQuantity
                                    });

                                    _transactionDbContext.GIWDocumentItems.Update(giwDocumentItem);
                                    break;
                                }
                                else
                                {
                                    goodsReturnQuantity = goodsReturnQuantity - giwDocumentItem.RemainingQty;

                                    goodsReturned.Add(new GoodsReturnDocumentItemModel
                                    {
                                        ReturnOrderId = item.ReturnOrderId,
                                        VariantId = item.VariantId,
                                        StockTypeId = item.StockTypeId,
                                        Quantity = giwDocumentItem.RemainingQty
                                    });

                                    giwDocumentItem.RemainingQty = 0;
                                    _transactionDbContext.GIWDocumentItems.Update(giwDocumentItem);
                                }
                            }

                        }

                        var newStockTransaction = new StockTransaction
                        {
                            Id = Guid.NewGuid(),
                            InventoryTransactionId = inventoryTransaction.Id,
                            LocationId = goodsReturnCommand.FromLocationId,
                            VariantId = item.VariantId,
                            StockTypeId = Guid.Empty,
                            TransactionTypeId = TransactionTypeGRT.Id,
                            Quantity = item.Quantity,
                            Balance = newBalance,
                            CreatedDate = DateTime.UtcNow,
                            CreatedBy = command.UserId,
                            StockTransactionRefId = item.ReturnOrderId,
                            ReferenceId = referenceId
                        };
                        await _transactionDbContext.StockTransactions.AddAsync(newStockTransaction);
                        newStockTransactions.Add(newStockTransaction);

                        stockLevelIndexesUpdated.Add(new StockLevelSearchItem()
                        {
                            ReferenceId = referenceId,
                            ProductId = variant.ProductId,
                            LocationId = goodsReturnCommand.FromLocationId,
                            VariantId = item.VariantId,
                            Balance = newBalance
                        });
                    }
                }
                else
                {
                    var stock = stockTransactions.FirstOrDefault(x => x.VariantId == item.VariantId && x.LocationId == location.Id);
                    if(stock != null)
                    {
                        throw new ArgumentException($"Any item with stock type does not exist in {location.Name}. Please try again!");
                    } else
                    {
                        throw new ArgumentException($"Any item does not exist in {location.Name}. Please try again!");
                    }
                }
            }
            await _transactionDbContext.SaveChangesAsync();
            await Task.Delay(5000);
            foreach (var item in stockLevelIndexesUpdated)
            {
                var document = new StockLevelIndexedItem(new StockLevelSearchItem(item.ReferenceId)
                {
                    ReferenceId = item.ReferenceId,
                    ProductId = item.ProductId,
                    LocationId = item.LocationId,
                    VariantId = item.VariantId,
                    Balance = item.Balance
                });
                await _searchService.UpdateAsync(document);
            }
            var returnOrderIds = goodsReturnCommand.Items
                .GroupBy(x => new { x.ReturnOrderId }, (key, group) =>
                new
                {
                    Id = key.ReturnOrderId
                }).ToList();
            var currentReturnOrderItems = goodsReturned
                    .GroupBy(x => new { x.ReturnOrderId, x.StockTypeId, x.VariantId }, (key, group) =>
                    new GoodsReturnDocumentItemModel
                    {
                        ReturnOrderId = key.ReturnOrderId,
                        VariantId = key.VariantId,
                        StockTypeId = key.StockTypeId,
                        Quantity = group.Sum(x => x.Quantity)
                    }).ToList();

            var updateReturnOrderIds = new List<Guid>();
            foreach (var item in returnOrderIds)
            {
                var poItems = purchaseOrderItems.Where(x => x.PurchaseOrderId == item.Id).ToList();
                if (IsUpdateStatusReturnOrder(currentReturnOrderItems, poItems) == true)
                {
                    updateReturnOrderIds.Add(item.Id);
                    var po = _pimDbContext.PurchaseOrders.FirstOrDefault(x => x.Id == item.Id);
                    await _poRepository.DeleteAsync(po);
                    await _poItemsRepository.DeleteAsync(poItems);
                }
            }

            await _eventBus.PublishAsync(new PurchaseOrderUpdatedStatusEvent()
            {
                Ids = updateReturnOrderIds
            });

            await _eventBus.PublishAsync(new StockLevelUpdatedEvent()
            {
                LocationId = goodsReturnCommand.FromLocationId
            });

            var stockAndPrices = newStockTransactions.Select(x => new StockAndPriceLevelModel()
            {
                LocationId = x.LocationId,
                ProductId = variants.SingleOrDefault(y => y.Id == x.VariantId).ProductId,
                VariantId = x.VariantId,
                SKUCode = variants.SingleOrDefault(y => y.Id == x.VariantId).SKUCode,
            }).Cast<dynamic>().ToList();
            await _eventBus.PublishAsync(new StockAndPriceLevelUpdatedEvent()
            {
                stockAndPrices = stockAndPrices
            });

            return goodsReturnDocument.Id;
        }

        private bool IsUpdateStatusReturnOrder(List<GoodsReturnDocumentItemModel> currentItems, List<PurchaseOrderItem> poItems)
        {
            var countItem = poItems.Count;
            foreach (var item in poItems)
            {
                var currentItem = currentItems.FirstOrDefault(x => x.ReturnOrderId == item.PurchaseOrderId
                                                                    && x.VariantId == item.VariantId
                                                                    && x.StockTypeId == item.StockTypeId);
                if (currentItem != null && (item.Quantity - currentItem.Quantity) <= 0)
                {
                    countItem -= 1;
                }
            }

            if (countItem == 0)
            {
                return true;
            }

            return false;
        }
    }
}
