﻿using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure.DataSeeders.Pim;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.Polly;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Infrastructure
{
    public class PimDbContextDataSeed
    {
        public async Task SeedAsync(PimDbContext context, ILogger<PimDbContextDataSeed> logger)
        {
            var policy = new DataSeedRetrivalPolicy();
            await policy.ExecuteStrategyAsync(logger, () =>
            {
                using (context)
                {
                    var brand = GetPreconfiguredEntityRef().Single(x => x.Name == "Brand");
                    if (context.EntityRefs.FirstOrDefault(x => brand.Name == x.Name) == null)
                    {
                        context.EntityRefs.Add(brand);
                        context.SaveChanges();
                    }

                    if (!context.Vendors.Any())
                    {
                        context.Vendors.AddRange(GetPreconfiguredVendor());
                    }

                    PaymentMethodsDataSeeder.SeedPaymentMethods(context).Wait();

                    VariantDataSeeder.UpdateVariant(context);
                    context.SaveChanges();
                }
            });
        }

        private List<EntityRef> GetPreconfiguredEntityRef()
        {
            return new List<EntityRef>()
            {
                new EntityRef()
                {
                    Id = new Guid("60B02EB3-33C0-4CDC-84D9-86F6FB051587"),
                    Name =  "Brand",
                    Namespace = typeof(Brand).AssemblyQualifiedName
                }
            };
        }

        private List<Vendor> GetPreconfiguredVendor()
        {
            return new List<Vendor>()
            {
                new Vendor()
                {
                    Id = new Guid("483A1915-A0D6-463B-ABBA-D3EB8518FC14"),
                    Name = "Stock Initial Vendor",
                    Description = "Stock Initial Vendor",
                    Active = true,
                    VendorCode = "SIV"
                }
            };
        }
    }
}
