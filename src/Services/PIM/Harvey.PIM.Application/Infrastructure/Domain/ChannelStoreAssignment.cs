﻿using Harvey.Domain;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class ChannelStoreAssignment : EntityBase
    {
        public Guid ChannelId { get; set; }
        public Guid StoreId { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual Location Store { get; set; }
    }
}
