﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class PurchaseOrder : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid VendorId { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public PurchaseOrderType Type { get; set; }
    }
}
