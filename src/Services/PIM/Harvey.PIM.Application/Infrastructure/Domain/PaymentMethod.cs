﻿using Harvey.Domain;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class PaymentMethod : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
