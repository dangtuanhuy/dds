﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class PurchaseOrderItem : EntityBase
    {
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public float CostValue { get; set; }
        public string CurrencyCode { get; set; }
    }
}
