﻿using System;
using System.ComponentModel.DataAnnotations;
using Harvey.Domain;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class Category : Identifiable, IAuditable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool IsDelete { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
