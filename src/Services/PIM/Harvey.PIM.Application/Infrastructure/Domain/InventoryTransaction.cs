﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class InventoryTransaction : EntityBase, IAuditable
    {
        public Guid TransactionTypeId { get; set; }
        public Guid TransactionRefId { get; set; }
        public string TransferNumber { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Code { get; set; }
        public InventoryTransactionStatus Status { get; set; }
        public virtual List<StockTransaction> StockTransactions { get; set; }
    }
}
