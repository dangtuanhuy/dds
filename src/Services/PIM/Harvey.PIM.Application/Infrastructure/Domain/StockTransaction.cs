﻿using Harvey.Domain;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class StockTransaction : EntityBase, IAuditable
    {
        public Guid TransactionTypeId { get; set; }
        public virtual TransactionType TransactionType { get; set; }
        public Guid InventoryTransactionId { get; set; }
        public Guid StockTransactionRefId { get; set; }
        public InventoryTransaction InventoryTransaction { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public Guid LocationId { get; set; }
        public int Quantity { get; set; }
        public int Balance { get; set; }
        public int BalanceRef { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ReferenceId { get; set; }
        public const string IndexName = "stock_index";
        public const string IndexStockType = "stock_level_stock_type_index";
    }
}
