﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class Location : EntityBase, IAuditable
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string LocationCode { get; set; }
        public LocationType Type { get; set; }
        public string ContactPerson { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual ICollection<ChannelStoreAssignment> ChannelStoreAssignments { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public virtual ICollection<StockTransaction> StockTransactions { get; set; }
    }

    public enum LocationType
    {
        None = 0,
        Warehouse = 1,
        Store = 2
    }
}
