﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class TimeToFeed: EntityBase
    {
        public DateTime LastSync { get; set; }
    }
}
