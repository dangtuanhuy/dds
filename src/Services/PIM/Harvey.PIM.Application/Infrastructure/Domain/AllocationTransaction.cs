﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class AllocationTransaction : EntityBase, IAuditable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string SANumber { get; set; }
        public string TransactionRef { get; set; }
        public AllocationTransactionStatus Status { get; set; }
        public virtual List<AllocationTransactionDetail> AllocationTransactionDetails { get; set; }
    }
}
