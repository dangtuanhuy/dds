﻿using Harvey.Domain;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class TransactionType : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public ICollection<StockTransaction> StockTransactions { get; set; }
    }
}
