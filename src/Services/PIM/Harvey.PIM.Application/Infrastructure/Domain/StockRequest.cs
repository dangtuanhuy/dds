﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class StockRequest : EntityBase, IAuditable
    {
        public StockRequestStatus StockRequestStatus { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime DateRequest { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
