﻿using Harvey.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class GIWDocument : EntityBase, IAuditable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid VendorId { get; set; }
        public Guid LocationId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual ICollection<GIWDocumentItem> GIWDocumentItems { get; set; }
    }
}
