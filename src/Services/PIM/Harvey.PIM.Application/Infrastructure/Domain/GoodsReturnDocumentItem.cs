﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class GoodsReturnDocumentItem : EntityBase
    {
        public Guid GoodsReturnDocumentId { get; set; }
        public GoodsReturnDocument GoodsReturnDocument { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public StockType StockType { get; set; }
        public Guid TransactionRefId { get; set; }
        public int Quantity { get; set; }
        public float CostValue { get; set; }
        public string CurrencyCode { get; set; }
    }
}
