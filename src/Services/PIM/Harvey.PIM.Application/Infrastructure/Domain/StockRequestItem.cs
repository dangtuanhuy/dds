﻿using Harvey.Domain;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class StockRequestItem : EntityBase
    {
        public Guid StockRequestId { get; set; }
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public int AllocateQuantity { get; set; }
    }
}
