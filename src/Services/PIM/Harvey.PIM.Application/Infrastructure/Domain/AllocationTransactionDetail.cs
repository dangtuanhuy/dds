﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class AllocationTransactionDetail : EntityBase, IAuditable
    {
       
        public Guid AllocationTransactionId { get; set; }
        public AllocationTransaction AllocationTransaction { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
