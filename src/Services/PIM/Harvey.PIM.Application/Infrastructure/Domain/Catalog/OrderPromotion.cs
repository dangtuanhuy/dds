﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class OrderPromotion : AggregateRootBase
    {
        public Guid OrderId { get; set; }
        public Guid PromotionId { get; set; }
        public string Reason { get; set; }
        public DiscountType DiscountType { get; set; }
        public float Value { get; set; }
    }
}
