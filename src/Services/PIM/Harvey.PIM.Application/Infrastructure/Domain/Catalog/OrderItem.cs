﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class OrderItem : EntityBase
    {
        public Guid OrderId { get; set; }
        public virtual Order Order { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public float Amount { get; set; }
        public int Quantity { get; set; }
        public string VariantName { get; set; }
        public float CostValue { get; set; }
        public float Price { get; set; }
        public Guid PriceId { get; set; }
        public Guid? PreviousOrderItemId { get; set; }
        public OrderItemStatus Status { get; set; }
    }
}
