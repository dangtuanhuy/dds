﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class TimeToFeed : EntityBase
    {
        public DateTime? LastSyncProduct { get; set; }
        public DateTime? LastSyncVariant { get; set; }
        public DateTime? LastSyncCategory { get; set; }
        public DateTime? LastSyncPrice { get; set; }
        public DateTime? LastSyncStockType { get; set; }
        public DateTime? LastSyncTransactionType { get; set; }
        public DateTime? LastSyncBarCode { get; set; }
        public DateTime? LastSyncPaymentMethod { get; set; }
    }
}
