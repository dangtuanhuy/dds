﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class Payment : EntityBase
    {
        public Guid OrderId { get; set; }
        public Guid PaymentMethodId { get; set; }
        public PaymentState State { get; set; }
        public float Amount { get; set; }
    }
}
