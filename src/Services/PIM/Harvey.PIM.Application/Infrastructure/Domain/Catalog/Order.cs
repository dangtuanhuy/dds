﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class Order : AggregateRootBase
    {
        public string Number { get; set; }
        public Guid? CustomerId { get; set; }
        public OrderState State { get; set; }
        public List<OrderItem> Items { get; set; }
        public Delivery Delivery { get; set; }
        public float SubTotal { get; set; }
        public float GrandTotal { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid LocationId { get; set; }
        public string BillNo { get; set; }
        public float GST { get; set; }
        public bool GSTInclusive { get; set; }
        public Guid CashierId { get; set; }
        public string CashierName { get; set; }
        public Guid? PreviousOrderId { get; set; }
        public Guid OriginalOrderId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public OrderTransactionType OrderTransactionType { get; set; }
        public string Reason { get; set; }
        public Guid DeviceId { get; set; }
        public float Collect { get; set; }
    }
}
