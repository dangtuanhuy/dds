﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class Synchronization : EntityBase
    {
        public SynchAction SynchAction { get; set; }
        public SynchEntity Entity { get; set; }
        public Guid RefId { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
