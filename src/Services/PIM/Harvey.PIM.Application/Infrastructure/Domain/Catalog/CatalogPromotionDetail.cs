﻿using Harvey.MarketingAutomation;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class CatalogPromotionDetail : FeedItemBase
    {
        public int DiscountTypeId { get; set; }
        public long Value { get; set; }
        public bool IsUseCouponCodes { get; set; }
        public bool IsUseConditions { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
