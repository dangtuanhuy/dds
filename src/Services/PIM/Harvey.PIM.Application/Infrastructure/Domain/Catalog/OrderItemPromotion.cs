﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class OrderItemPromotion : AggregateRootBase
    {
        public Guid OrderItemId { get; set; }
        public DiscountType DiscountType { get; set; }
        public float Value { get; set; }
    }
}
