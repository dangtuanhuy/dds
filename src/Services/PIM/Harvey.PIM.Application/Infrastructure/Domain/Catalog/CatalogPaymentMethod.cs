﻿using Harvey.MarketingAutomation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class CatalogPaymentMethod : FeedItemBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
