﻿using Harvey.MarketingAutomation;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class CatalogPromotionLocation : FeedItemBase
    {
        public Guid PromotionId { get; set; }
        public Guid LocationId { get; set; }
    }
}
