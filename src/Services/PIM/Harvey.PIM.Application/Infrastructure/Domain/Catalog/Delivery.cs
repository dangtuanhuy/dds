﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class Delivery : EntityBase
    {
        public Guid OrderId { get; set; }
        public DeliveryState State { get; set; }
    }
}
