﻿using Harvey.MarketingAutomation;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class CatalogPromotionCouponCode : FeedItemBase
    {
        public string Code { get; set; }
        public bool IsUsed { get; set; }
        public Guid PromotionDetailId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
