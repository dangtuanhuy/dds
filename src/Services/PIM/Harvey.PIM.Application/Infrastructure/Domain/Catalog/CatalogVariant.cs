﻿using Harvey.MarketingAutomation;
using System;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class CatalogVariant : FeedItemBase
    {
        public Guid ProductId { get; set; }
        public Guid PriceId { get; set; }
        public string SKUCode { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
