﻿using Harvey.MarketingAutomation;

namespace Harvey.PIM.Application.Infrastructure.Domain.Catalog
{
    public class CatalogStockType : FeedItemBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
