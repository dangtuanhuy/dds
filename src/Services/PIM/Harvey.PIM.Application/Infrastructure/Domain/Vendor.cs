﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class Vendor : EntityBase
    {
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Email { get; set; }
        public string VendorUrl { get; set; }
        public string Country { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string Phone { get; set; }
        public string PaymentTermName { get; set; }
        public string CurrencyName { get; set; }
        public string TaxTypeValue { get; set; }
        public string ZipCode { get; set; }
        public string Fax { get; set; }
        public string VendorCode { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
