﻿using System;
using System.Collections.Generic;
using Harvey.Domain;

namespace Harvey.PIM.Application.Infrastructure.Domain
{
    public class Variant : EntityBase, IAuditable
    {
        public Guid ProductId { get; set; }
        public Guid PriceId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Code { get; set; }
        public string SKUCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<BarCode> BarCodes { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public virtual Product Product { get; set; }
    }
}
