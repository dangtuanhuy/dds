﻿using Harvey.Search;
using Harvey.Search.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Indexing
{
    public class StockLevelStockTypeSearchItem : SearchItem, ISearchItem
    {
        public StockLevelStockTypeSearchItem()
        {

        }

        public StockLevelStockTypeSearchItem(string id) : base(id)
        {
        }
        public string ReferenceId { get; set; }
        public Guid VendorId { get; set; }
        public Guid ProductId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Balance { get; set; }

        public class StockLevelStockTypeIndexedItem : IndexedItem<StockLevelStockTypeSearchItem>
        {
            public StockLevelStockTypeIndexedItem(StockLevelStockTypeSearchItem item) : base(item)
            {
            }

            public override string IndexName => Domain.StockTransaction.IndexStockType;
        }

        public class StockLevelStockTypeSearchQuery : SearchQuery<StockLevelStockTypeSearchItem>, ISearchQuery<StockLevelStockTypeSearchItem>
        {
            public override string IndexName => Domain.StockTransaction.IndexStockType;

            public override List<Expression<Func<StockLevelStockTypeSearchItem, object>>> Matches => new List<Expression<Func<StockLevelStockTypeSearchItem, object>>>()
            {
                x=>x.VendorId
            };

            public override List<Expression<Func<StockLevelStockTypeSearchItem, object>>> Filters => new List<Expression<Func<StockLevelStockTypeSearchItem, object>>>()
            {
            };

            public override List<Expression<Func<StockLevelStockTypeSearchItem, object>>> SortBy => new List<Expression<Func<StockLevelStockTypeSearchItem, object>>>()
            {
            };
        }

        public class StockLevelStockTypeSearchResults : SearchResults<StockLevelStockTypeSearchItem, StockLevelStockTypeSearchResult>, ISearchResults<StockLevelStockTypeSearchItem, StockLevelStockTypeSearchResult>
        {

        }

        public class StockLevelStockTypeSearchResult : SearchResult<StockLevelStockTypeSearchItem>, ISearchResult<StockLevelStockTypeSearchItem>
        {
            public StockLevelStockTypeSearchResult()
            {

            }

            public StockLevelStockTypeSearchResult(StockLevelStockTypeSearchItem item) : base(item)
            {

            }
        }
    }
}
