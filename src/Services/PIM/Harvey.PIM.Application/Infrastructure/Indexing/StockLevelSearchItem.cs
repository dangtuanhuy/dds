﻿using Harvey.Search;
using Harvey.Search.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Harvey.PIM.Application.Infrastructure.Indexing
{
    public class StockLevelSearchItem : SearchItem, ISearchItem
    {
        public StockLevelSearchItem()
        {

        }

        public StockLevelSearchItem(string id) : base(id)
        {
        }
        public string ReferenceId { get; set; }
        public Guid LocationId { get; set; }
        public Guid ProductId { get; set; }
        public Guid VariantId { get; set; }
        public int Balance { get; set; }
    }

    public class StockLevelIndexedItem : IndexedItem<StockLevelSearchItem>
    {
        public StockLevelIndexedItem(StockLevelSearchItem item) : base(item)
        {
        }

        public override string IndexName => Domain.StockTransaction.IndexName;
    }

    public class StockLevelSearchQuery : SearchQuery<StockLevelSearchItem>, ISearchQuery<StockLevelSearchItem>
    {
        public override string IndexName => Domain.StockTransaction.IndexName;

        public override List<Expression<Func<StockLevelSearchItem, object>>> Matches => new List<Expression<Func<StockLevelSearchItem, object>>>()
        {
            x=>x.LocationId
        };

        public override List<Expression<Func<StockLevelSearchItem, object>>> Filters => new List<Expression<Func<StockLevelSearchItem, object>>>()
        {
        };

        public override List<Expression<Func<StockLevelSearchItem, object>>> SortBy => new List<Expression<Func<StockLevelSearchItem, object>>>()
        {
        };
    }

    public class StockLevelSearchResults : SearchResults<StockLevelSearchItem, StockLevelSearchResult>, ISearchResults<StockLevelSearchItem, StockLevelSearchResult>
    {

    }

    public class StockLevelSearchResult : SearchResult<StockLevelSearchItem>, ISearchResult<StockLevelSearchItem>
    {
        public StockLevelSearchResult()
        {

        }

        public StockLevelSearchResult(StockLevelSearchItem item) : base(item)
        {

        }
    }
}
