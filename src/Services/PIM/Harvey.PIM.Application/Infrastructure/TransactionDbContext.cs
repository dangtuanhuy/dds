﻿using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Harvey.PIM.Application.Infrastructure
{
    public class TransactionDbContext : DbContext
    {
        public DbSet<InventoryTransaction> InventoryTransactions { get; set; }
        public DbSet<StockTransaction> StockTransactions { get; set; }
        public DbSet<AllocationTransaction> AllocationTransactions { get; set; }
        public DbSet<AllocationTransactionDetail> AllocationTransactionDetails { get; set; }
        public DbSet<GIWDocument> GIWDocuments { get; set; }
        public DbSet<GIWDocumentItem> GIWDocumentItems { get; set; }
        public DbSet<StockType> StockTypes { get; set; }
        public DbSet<TransactionType> TransactionTypes { get; set; }
        public DbSet<Reason> Reasons { get; set; }
        public DbSet<StockRequest> StockRequests { get; set; }
        public DbSet<StockRequestItem> StockRequestItems { get; set; }
        public DbSet<GoodsReturnDocument> GoodsReturnDocuments { get; set; }
        public DbSet<GoodsReturnDocumentItem> GoodsReturnDocumentItems { get; set; }
        public TransactionDbContext(DbContextOptions<TransactionDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<InventoryTransaction>());
            Setup(modelBuilder.Entity<StockTransaction>());
            Setup(modelBuilder.Entity<AllocationTransaction>());
            Setup(modelBuilder.Entity<AllocationTransactionDetail>());
            Setup(modelBuilder.Entity<GIWDocument>());
            Setup(modelBuilder.Entity<GIWDocumentItem>());
            Setup(modelBuilder.Entity<StockType>());
            Setup(modelBuilder.Entity<TransactionType>());
            Setup(modelBuilder.Entity<Reason>());
            Setup(modelBuilder.Entity<StockRequest>());
            Setup(modelBuilder.Entity<StockRequestItem>());
            Setup(modelBuilder.Entity<GoodsReturnDocument>());
            Setup(modelBuilder.Entity<GoodsReturnDocumentItem>());
        }

        public void Setup(EntityTypeBuilder<InventoryTransaction> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasOne<TransactionType>()
              .WithMany()
              .HasForeignKey(x => x.TransactionTypeId);
            entityConfig.HasMany(x => x.StockTransactions)
               .WithOne(x => x.InventoryTransaction)
               .HasForeignKey(x => x.InventoryTransactionId);
        }

        public void Setup(EntityTypeBuilder<StockTransaction> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Property(x => x.VariantId).IsRequired();
            entityConfig.HasOne(x => x.TransactionType)
                .WithMany(x => x.StockTransactions).HasForeignKey(x => x.TransactionTypeId);
        }

        public void Setup(EntityTypeBuilder<AllocationTransaction> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasMany(x => x.AllocationTransactionDetails)
               .WithOne(x => x.AllocationTransaction)
               .HasForeignKey(x => x.AllocationTransactionId);
        }

        public void Setup(EntityTypeBuilder<AllocationTransactionDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Property(x => x.VariantId).IsRequired();
        }

        public void Setup(EntityTypeBuilder<GIWDocument> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig
                .HasMany(x => x.GIWDocumentItems)
                .WithOne(x => x.GIWDocument)
                .HasForeignKey(x => x.GIWDocumentId);
        }

        public void Setup(EntityTypeBuilder<GIWDocumentItem> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<StockType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
        }

        public void Setup(EntityTypeBuilder<TransactionType> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
        }

        public void Setup(EntityTypeBuilder<Reason> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
        }

        public void Setup(EntityTypeBuilder<StockRequest> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<StockRequestItem> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<GoodsReturnDocument> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasMany(x => x.GoodsReturnItems)
               .WithOne(x => x.GoodsReturnDocument)
               .HasForeignKey(x => x.GoodsReturnDocumentId);
        }

        public void Setup(EntityTypeBuilder<GoodsReturnDocumentItem> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Property(x => x.VariantId).IsRequired();
        }
    }

    public class TransientTransactionDbContext : TransactionDbContext
    {
        public TransientTransactionDbContext(DbContextOptions<TransactionDbContext> options) : base(options)
        {
        }
    }
}
