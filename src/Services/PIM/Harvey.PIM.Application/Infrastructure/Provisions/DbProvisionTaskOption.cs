﻿using System;

namespace Harvey.PIM.Application.Infrastructure.Provisions
{
    public class DbProvisionTaskOption : ITaskOption
    {
        public string ChannelName { get; set; }
        public string ConnectionString { get; }
        public DbProvisionTaskOption(string channelName, string connectionString)
        {
            ChannelName = channelName;
            ConnectionString = connectionString;
        }
    }
}
