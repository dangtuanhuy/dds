﻿using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;

namespace Harvey.PIM.Application
{
    public class PurchaseControlDbContext : DbContext
    {
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<PurchaseOrderItem> PurchaseOrderItems { get; set; }
        public PurchaseControlDbContext(DbContextOptions<PurchaseControlDbContext> options) : base(options)
        {

        }
    }
}
