﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum PaymentState
    {
        Init = 1,
        ExecuteCharge = 2,
        Paid = 3,
        Returned = 4,
        Pending = 5,
        ChargedFail = 6,
        ReturnedFail = 7
    }
}
