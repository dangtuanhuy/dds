﻿namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum InventoryTransactionStatus
    {
        Pending = 1,
        Partial = 2,
        Allocated = 3,
    }
}
