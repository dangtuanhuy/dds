﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum DeliveryState
    {
        Init = 1,
        Delivered = 2
    }
}
