﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum TransactionTypeCode
    {
        GIW = 1,
        TFI = 2,
        TFO = 3,
        Sale = 4,
        GRT = 5,
        Exchange = 6,
        Refund = 7
    }
}
