﻿namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum AllocationTransactionStatus
    {
        Draft = 1,
        Submitted = 2,
        PartialTransfer = 3,
        Complete = 4
    }
}
