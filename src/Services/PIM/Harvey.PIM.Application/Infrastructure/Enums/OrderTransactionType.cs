﻿namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum OrderTransactionType
    {
        Normal = 0,
        Refund = 1,
        Exchange = 2,
        Void = 3
    }
}
