﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum OrderState
    {
        Init = 1,
        Confirmed = 2,
        Processing = 3,
        Completed = 4,
        Attention = 5,
        Returned = 6,
        Cancelled = 7
    }
}
