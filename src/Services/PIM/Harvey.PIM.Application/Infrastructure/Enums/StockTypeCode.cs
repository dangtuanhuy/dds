﻿
namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum StockTypeCode
    {
        FIRM = 1,
        SOE = 2,
        SOR = 3,
        CON = 4,
        STI = 5
    }
}
