﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum PurchaseOrderType
    {
        DraftPO = 1,
        PurchaseOrder = 2,
        ReturnedOrder = 3,
        DraftRO = 4
    }
}
