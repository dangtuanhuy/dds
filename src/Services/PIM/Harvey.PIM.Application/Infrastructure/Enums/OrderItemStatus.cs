﻿namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum OrderItemStatus
    {
        Normal = 0,
        Refund = 1,
        Exchange = 2,
        Old = 3
    }
}
