﻿namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum DiscountType
    {
        Default = 0,
        Money = 1,
        Percent = 2
    }
}
