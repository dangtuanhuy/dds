﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum IndexType
    {
        Product = 0,
        StockLevel = 1
    }
}
