﻿namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum SynchEntity
    {
        Category = 1,
        Product = 2,
        Variant = 3
    }
}
