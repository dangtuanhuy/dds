﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum StockRequestStatus
    {
        Opened = 0,
        Submitted = 1,
        Approved = 2,
        Closed = 3,
        Canceled = 4,
        Rejected = 5,
        PartialAllocated = 6,
        Allocated = 7
    }
}
