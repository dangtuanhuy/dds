﻿namespace Harvey.PIM.Application.Infrastructure.Enums
{
    public enum SynchAction
    {
        Add = 1,
        Update = 2,
        Delete = 3
    }
}
