﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class UpdateChannelStockTransactionResponse
    {
        public string ServerInformation { get; set; }
        public List<StockTransactionUpdatedRetailsModel> StockTransactionUpdatedRetailsModels { get; set; }
    }
}
