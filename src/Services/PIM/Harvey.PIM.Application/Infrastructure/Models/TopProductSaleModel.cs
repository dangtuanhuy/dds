﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class TopProductSaleRequestModel
    {
        public string ToDate { get; set; }
        public string FromDate { get; set; }
    }
    public class TopProductSaleModel
    {
        public string ProductName { get; set; }
        public string VariantName { get; set; }
        public Guid VariantId { get; set; }
        public string SKUCode { get; set; }
        public int TotalQuantity { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
