﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class TransferOutRequestModel : PagingFilterCriteria
    {
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class TransferOutByAllocationRequestModel : PagingFilterCriteria
    {
        public string FromLocationIds { get; set; }
        public string ToLocationIds { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
