﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class SearchVariantModel
    {       
            public Guid Id { get; set; }
            public List<SearchFieldValueModel> FieldValues { get; set; } = new List<SearchFieldValueModel>();
            public string Code { get; set; } 
    }

    public class SearchFieldValueModel
    {
        public string Name { get; set; }
        public List<string> Value { get; set; }
    }
}
