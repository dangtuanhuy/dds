﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class ReturnOrderItemModel
    {
        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public Guid StockTypeId { get; set; }
        public string StockTypeName { get; set; }
        public List<string> BarCodes { get; set; }
        public int Quantity { get; set; }
        public VariantROModel Variant { get; set; }
    }

    public class VariantROModel
    {
        public Guid Id { get; set; }
        public List<FieldValueROModel> FieldValues { get; set; } = new List<FieldValueROModel>();
    }

    public class FieldValueROModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

}
