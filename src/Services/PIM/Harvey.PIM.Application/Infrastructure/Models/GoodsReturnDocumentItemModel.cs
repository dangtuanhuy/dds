﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class GoodsReturnDocumentItemModel
    {
        public Guid Id { get; set; }
        public Guid ReturnOrderId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
        public float CostValue { get; set; }
        public string CurrencyCode { get; set; }
    }
}
