﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using static Harvey.PIM.Application.Infrastructure.Models.ProductModel;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class TransferOutModel
    {
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public List<AllocationTransferOutProduct> AllocationTransferOutProducts { get; set; }
    }
    public class AllocationTransferOutProduct
    {
        public Guid ProductId { get; set; }
        public Guid StockTransactionRefId { get; set; }
        public Guid TransactionRefId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
    }

    public class InventoryTransactionTransferViewModel
    {
        public Guid Id { get; set; }
        public string TransferNumber { get; set; }
        public Guid InventoryTransactionRefId { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public InventoryTransactionStatus Status { get; set; }
        public List<InventoryTransactionTransferProduct> InventoryTransactionTransferProducts { get; set; } = new List<InventoryTransactionTransferProduct>();
        public List<TransferProductModel> Products { get; set; }
    }

    public class InventoryTransactionTransferOutModel
    {
        public Guid Id { get; set; }
        public string TransferNumber { get; set; }
        public Guid InventoryTransactionRefId { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<InventoryTransactionTransferProduct> InventoryTransactionTransferOutProducts { get; set; } = new List<InventoryTransactionTransferProduct>();
    }
    public class InventoryTransactionTransferProduct
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public List<InventoryTransactionTransferProductVariant> Variants { get; set; } = new List<InventoryTransactionTransferProductVariant>();
        public Guid StockTransactionRefId { get; set; }
        public Guid TransactionRefId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
    }

    public class InventoryTransactionTransferProductVariant
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
    public class TransferProductModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<TransferVariantModel> Variants { get; set; }
    }

    public class TransferVariantModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class TransferFieldModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
