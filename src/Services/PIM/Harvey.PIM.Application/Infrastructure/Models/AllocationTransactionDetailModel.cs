﻿using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class AllocationTransactionDetailModel
    {
        public Guid Id { get; set; }
        public Guid AllocationTransactionId { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public int StockOnHand { get; set; }
        public virtual AllocationTransaction AllocationTransaction { get; set; }
    }
}
