﻿using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class StockTransactionModel
    {
        public Guid Id { get; set; }
        public Guid TransactionTypeId { get; set; }
        public Guid InventoryTransactionId { get; set; }
        public Guid StockTransactionRefId { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public int Quantity { get; set; }
        public int Balance { get; set; }
        public int BalanceRef { get; set; }
        public Guid ReferenceId { get; set; }
    }

    public class StockTransactionCsvModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
        public string VariantSKU { get; set; }
        public string PreOrderPrice { get; set; }
        public List<string> VariantBarCodes { get; set; }
        public float CostValue { get; set; }
        public string CurrencyCode { get; set; }
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }
        public string Category { get; set; }
        public string StockOnHandSOE { get; set; }
        public string StockOnHandSOR { get; set; }
        public string StockOnHandFIRM { get; set; }
        public string StockOnHandCON { get; set; }
        public string StockOnHand { get; set; }
        public string ActualStockOnHand { get; set; }
        public int QuantitySOE { get; set; }
        public int QuantitySOR { get; set; }
        public int QuantityFIRM { get; set; }
        public int QuantityCON { get; set; }
        public int Quantity { get; set; }
    }

    public class StockInitialRequest
    {
        public IFormFile FormData { get; set; }
        public string LocationId { get; set; }
    }

    public class StockItemModel
    {
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
        public Guid OrderId { get; set; }
    }

    public class StockTransactionUpdatedRetailsRequest
    {
        public Guid LocationId { get; set; }
        public string ServerInformation { get; set; }
        public List<OrderItemRequestModel> Items { get; set; }
    }

    public class OrderItemRequestModel
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public float Amount { get; set; }
        public int Quantity { get; set; }
        public DateTime CreatedDate { get; set; }
        public OrderItemStatus Status { get; set; }
        public Guid GIWDocumentItemId { get; set; }
    }

    public class StockTransactionUpdatedRetailsModel
    {
        public Guid Id { get; set; }
        public Guid TransactionTypeId { get; set; }
        public Guid InventoryTransactionId { get; set; }
        public Guid StockTransactionRefId { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
        public Guid LocationId { get; set; }
        public Guid WareHouseId { get; set; }
        public Guid GIWDocumentItemId { get; set; }
        public int Quantity { get; set; }
        public int Balance { get; set; }
        public int BalanceRef { get; set; }
        public float Cost { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid VendorId { get; set; }
        public string ReferenceId { get; set; }
        public OrderItemStatus Status { get; set; }
    }
}
