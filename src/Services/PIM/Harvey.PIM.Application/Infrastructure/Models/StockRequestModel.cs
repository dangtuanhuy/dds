﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class StockRequestModel
    {
        public Guid Id { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public string LocationName { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public StockRequestStatus StockRequestStatus { get; set; }
        public DateTime DateRequest { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<StockRequestItemModel> StockRequestItems { get; set; }
    }

    public class StockRequestUpdateModel : StockRequestModel
    {
        public bool IsDeleteRequest { get; set; }
    }

    public class StockRequestViewModel
    {
        public Guid Id { get; set; }
        public Guid LocationId { get; set; }
        public List<StockRequestItemViewModel> StockRequestItems { get; set; }
    }

    public class StockRequestItemViewModel
    {
        public Guid Id { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
    }
}
