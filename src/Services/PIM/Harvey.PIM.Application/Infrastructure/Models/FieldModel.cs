﻿using Harvey.Domain;
using Harvey.PIM.Application.FieldFramework;
using System;
using System.ComponentModel.DataAnnotations;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class FieldModel
    {
        [Required]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public FieldType Type { get; set; }
        public string DefaultValue { get; set; }
        public string IdentifiedId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class FieldCsvModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Entity { get; set; }
        public string Section { get; set; }
        public string Type { get; set; }
        public string DefaultValue { get; set; }
        public string IdentifiedId { get; set; }
    }
}
