﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class AllocationTransactionModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string TransactionRef { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public AllocationTransactionStatus Status { get; set; }
        public List<AllocationTransactionDetailModel> AllocationTransactionDetails { get; set; } = new List<AllocationTransactionDetailModel>();
    }

    public class UpdateStatusAllocationTransactionModel
    {
        public Guid Id { get; set; }
        public AllocationTransactionStatus Status { get; set; }
    }

    public class AllocationTransactionTransferOutModel
    {
        public Guid Id { get; set; }
        public List<AllocationTransactionDetailTransferOutModel> AllocationTransactionTransferDetails { get; set; } = new List<AllocationTransactionDetailTransferOutModel>();
    }

    public class AllocationTransactionDetailTransferOutModel
    {
        public Guid Id { get; set; }
        public int Quantity { get; set; }
    }

    public class AllocationTransactionByListIdModel
    {
        public List<Guid> AllocationTransactionListId { get; set; } = new List<Guid>();
        public List<AllocationTransactionModel> AllocationTransactions { get; set; } = new List<AllocationTransactionModel>();
    }

    public class MassAllocationInfoModel
    {
        public List<OutletModel> OutletInfor { get; set; } = new List<OutletModel>();
        public List<MassAllocationViewModel> MassAllocations { get; set; } = new List<MassAllocationViewModel>();
    }

    public class MassAllocationViewModel
    {
        public string SKU { get; set; }
        public int GIWQuantity { get; set; }
        public int Balance { get; set; }
        public int RemainingBalance { get; set; }
        public string ProductName { get; set; }
        public Guid VariantId { get; set; }
        public List<OutletQuantityModel> Outlets { get; set; } = new List<OutletQuantityModel>();
        public List<VariantField> VariantFields { get; set; }
        public List<MassAllocationItemViewModel> Items { get; set; } = new List<MassAllocationItemViewModel>();
    }

    public class MassAllocationItemViewModel
    {
        public int OutletQuantity { get; set; }
        public string OutletName { get; set; }
        public Guid OutletId { get; set; }
        public int SOHOutlet { get; set; }
        public Guid StockRequestId { get; set; }
    }

    public class OutletModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid StockRequestId { get; set; }
    }

    public class OutletQuantityModel
    {
        public Guid Id { get; set; }
        public int Quantity { get; set; }
        public int QuantityOnHand { get; set; }
        public int QuantitySold { get; set; }
    }

    public class OutletProductModel
    {
        public Guid OutletId { get; set; }
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public int QuantitySold { get; set; }
    }
}
