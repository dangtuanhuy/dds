﻿using Harvey.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class FieldValueModel
    {
        public Guid Id { get; set; }
        public Guid FieldId { get; set; }
        public string FieldValue { get; set; }
    }

    public class DetailFieldValueModel
    {
        public Guid Id { get; set; }
        public Guid EntityId { get; }
        public Guid FieldId { get; }
        public string FieldValue { get; }
        public FieldType FieldType { get; }
        public string Section { get; }
        public bool IsVariantField { get; }
        public int OrderSection { get; }
        public string FieldName { get; }

        public DetailFieldValueModel(Guid productId, string section, int orderSection, bool isVariantField, FieldType fieldType, Guid fieldId, string fieldName, string fieldValue)
        {
            EntityId = productId;
            FieldValue = fieldValue;
            FieldType = fieldType;
            FieldId = fieldId;
            Section = section;
            IsVariantField = isVariantField;
            OrderSection = orderSection;
            FieldName = fieldName;
        }

        public class FieldValueCsvModel: IEquatable<FieldValueCsvModel>
        {
            public string IdentifiedId { get; set; }
            public string FieldValue { get; set; }

            public bool Equals(FieldValueCsvModel other)
            {
                if (other is null)
                    return false;

                return this.IdentifiedId == other.IdentifiedId && this.FieldValue == other.FieldValue;
            }

            public override bool Equals(object obj) => Equals(obj as FieldValueCsvModel);
            public override int GetHashCode() => (IdentifiedId, FieldValue).GetHashCode();
        }
    }
}
