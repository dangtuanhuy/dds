﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class OrderModel
    {
        public string Id { get; set; }
        public List<OrderItemModel> OrderItems { get; set; }
        public string StoreId { get; set; }
    }


    public class OrderItemModel
    {
        public Guid Id { get; set; }
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public float Amount { get; set; }
        public string VariantName { get; set; }
        public float CostValue { get; set; }
        public float Price { get; set; }
    }

    public class SalePerformanceItem
    {
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public float Amount { get; set; }
        public Guid LocationId { get; set; }
        public string Description { get; set; }
        public float CostValue { get; set; }
    }

    public class SaleOrderItemModel
    {
        public string Id { get; set; }
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public class ReturnOrderItem
    {
        public Guid Id { get; set; }
        public Guid WareHouseId { get; set; }
        public Guid GIWDocumentItemId { get; set; }        
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
        public string VariantName { get; set; }
        public float CostValue { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid OrderId { get; set; }
        public Guid VendorId { get; set; }
        public OrderItemStatus Status { get; set; }
    }
}
