﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class ChannelStoreAssignmentModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class AddChannelStoreAssignmentModel
    {
        public Guid StoreId { get; set; }
    }
}
