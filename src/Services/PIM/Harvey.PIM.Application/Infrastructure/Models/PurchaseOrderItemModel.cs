﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class PurchaseOrderItemModel
    {
        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public Guid StockTypeId { get; set; }
        public string StockTypeName { get; set; }
        public List<string> BarCodes { get; set; }
        public int Quantity { get; set; }
        public VariantPOModel Variant { get; set; }
    }

    public class VariantPOModel
    {
        public Guid Id { get; set; }
        public List<FieldValuePOModel> FieldValues { get; set; } = new List<FieldValuePOModel>();
    }

    public class FieldValuePOModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
