﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    
    public class StockTypesBalance
    {   
        public int GrandBalance { get; set; }
        public List<StockTypeModel> StockTypes { get; set; } = new List<StockTypeModel>();
    }

    public class StockTypeModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Balance { get; set; }
    }
}
