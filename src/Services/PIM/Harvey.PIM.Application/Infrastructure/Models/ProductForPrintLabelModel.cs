﻿using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class ProductForPrintLabelResponse
    {
        public bool BarCodeDataOnly { get; set; }
        public List<ProductForPrintLabelModel> Products { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
    }

    public class ProductForPrintLabelModel
    {
        public Guid VariantId { get; set; }
        public string BarCode { get; set; }
        public string SKUCode { get; set; }
        public string ProductName { get; set; }
        public string VariantName { get; set; }
        public string CreatedDateString { get; set; }
        public int Quantity { get; set; }
        public double ListPrice { get; set; }
        public double MemberPrice { get; set; }
        public double StaffPrice { get; set; }
    }
}
