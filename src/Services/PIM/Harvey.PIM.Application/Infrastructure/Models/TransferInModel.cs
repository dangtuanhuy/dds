﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class TransferInModel
    {
        public List<InventoryTransactionTransferInModel> InventoryTransactionTransferInsModel { get; set; } = new List<InventoryTransactionTransferInModel>();
    }
    public class InventoryTransactionTransferInModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
        public Guid StockTransactionRefId { get; set; }
        public Guid TransactionRefId { get; set; }
        public string InventoryTransactionTransferNumber { get; set; }
        public Guid InventoryTransactionId { get; set; }
        public Guid InventoryTransactionFromLocation { get; set; }
        public Guid InventoryTransactionToLocation { get; set; }
        public Guid InventorytransactionRefId { get; set; }
    }
}
