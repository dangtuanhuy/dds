﻿using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class StockRequestItemModel
    {
        public Guid Id { get; set; }
        public Guid StockRequestId { get; set; }
        public Variant Variant { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
    }

    public class VariantField
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
