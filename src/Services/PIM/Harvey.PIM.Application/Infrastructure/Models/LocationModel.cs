﻿using AutoMapper;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class LocationModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string LocationCode { get; set; }
        public LocationType Type { get; set; }
        public string ContactPerson { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class LocationProfile: Profile {
        public LocationProfile()
        {
            CreateMap<Location, LocationModel>().ReverseMap();
        }
    }
}
