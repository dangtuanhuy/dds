﻿using System;
using AutoMapper;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Indexing;
using static Harvey.PIM.Application.Infrastructure.Models.ProductModel;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class ProductListModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Code { get; set; }
        public bool IsDelete { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public SearchVariantModel Variant { get; set; }
    }

    public class ProductListModelProfile : Profile
    {
        public ProductListModelProfile()
        {
            CreateMap<Product, ProductListModel>()
                .ConstructUsing(src =>
                {
                    return new ProductListModel()
                    {
                        Id = src.Id.ToString(),
                        Name = src.Name,
                        Description = src.Description,
                        CategoryId = src.CategoryId.Value,
                        CategoryName = src.Category?.Name,
                        Code = src.Code,
                        IsDelete = src.IsDelete,
                        CreatedDate = src.CreatedDate,
                        CreatedBy = src.CreatedBy,
                        UpdatedDate = src.UpdatedDate,
                        UpdatedBy = src.UpdatedBy
                    };
                })
                .ForAllMembers(opt => opt.Ignore());

            CreateMap<ProductListModel, CatalogProduct>();
        }
    }
}
