﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class CountModel
    {
        public int Products { get; set; }
        public int Variants { get; set; }
    }
}
