﻿using AutoMapper;
using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.ComponentModel.DataAnnotations;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class CategoryModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IdentifiedId { get; set; }
        public string Code { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryModel>().ReverseMap();
        }
    }
}
