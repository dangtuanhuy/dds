﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class DashboardModel
    {
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<DashboardItemModel> Items { get; set; }
    }

    public class DashboardItemModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public List<DashboardItemDetailModel> Items { get; set; }
    }

    public class DashboardItemDetailModel
    {
        public Guid VariantId { get; set; }
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }
        public int Balance { get; set; }
        public List<VariantField> Fields { get; set; }
        public List<string> BarCodes { get; set; }
        public string SKUCode { get; set; }
    }

    public class DashboardRequestModel
    {
        public Guid LocationId { get; set; }
        public string QueryText { get; set; }
    }   
    public class PurchaseOrderSummaryModel
    {
        public string StatusTitle { get; set; }
        public ApprovalStatus Status { get; set; }
        public int CountNumber { get; set; }
        public string Icon { get; set; }
    }

    public class NewProductModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string CategoryName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
