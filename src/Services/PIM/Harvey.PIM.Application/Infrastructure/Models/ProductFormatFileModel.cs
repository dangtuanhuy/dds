﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class ProductFormatFileModel
    {
        public string FileName { get; set; }
        public byte[] data { get; set; }
    }
}
