﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class StockAndPriceProductModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
    }

    public class StockAndPriceModel
    {
        public int ToTalItems { get; set; }
        public List<StockAndPriceItemModel> Items { get; set; }
    }

    public class StockAndPriceItemModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public IEnumerable<StockAndPriceStoreItemModel> StoreItems { get; set; }
        public IEnumerable<StockAndPriceWarehouseItemModel> WarehouseItems { get; set; }
    }

    public class StockAndPriceStoreItemModel
    {
        public string VariantName { get; set; }
        public string SKUCode { get; set; }
        public string CategoryName { get; set; }
        public string LocationName { get; set; }
        public string ReferenceId { get; set; }
        public float ListPrice { get; set; }
        public float MemberPrice { get; set; }
        public float StaffPrice { get; set; }
        public int Quantity { get; set; }
    }

    public class StockAndPriceWarehouseItemModel
    {
        public string VariantName { get; set; }
        public string SKUCode { get; set; }
        public string CategoryName { get; set; }
        public string LocationName { get; set; }
        public string ReferenceId { get; set; }
        public int Quantity { get; set; }
    }
}
