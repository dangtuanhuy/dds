﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class GIWDModel
    {
        public string GIWDName { get; set; }
        public string GIWDDescription { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public List<GIWItem> GIWItems { get; set; }
    }

    public class GIWItem
    {
        public Guid PurchaseOrderId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public List<string> BarCodes { get; set; }
        public int Quantity { get; set; }
    }

    public class StockTransactionGroupingModel
    {
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid LocationId { get; set; }
        public int Quantity { get; set; }
    }

    public class StockGroupingModel
    {
        public Guid VariantId { get; set; }
        public Guid LocationId { get; set; }
        public int Quantity_SOE { get; set; }
        public int Quantity_SOR { get; set; }
        public int Quantity_FIRM { get; set; }
        public int Quantity_CON { get; set; }
        public int Quantity_Initial { get; set; }
    }

    public class UpdateGIWStatusModel
    {
        public Guid Id { get; set; }
        public InventoryTransactionStatus Status { get; set; }
    }
}
