﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class InventoryTransactionModel
    {
        public Guid Id { get; set; }
        public string TransferNumber { get; set; }
        public string Code { get; set; }
        public Guid InventoryTransactionRefId { get; set; }
        public Guid FromLocationId { get; set; }
        public Guid ToLocationId { get; set; }
        public string FromLocationName { get; set; }
        public string ToLocationName { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public InventoryTransactionStatus Status { get; set; }
        public List<InventoryTransactionProductModel> Items { get; set; } = new List<InventoryTransactionProductModel>();
    }

    public class InventoryTransactionProductModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid StockTransactionRefId { get; set; }
        public Guid TransactionRefId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
    }
}
