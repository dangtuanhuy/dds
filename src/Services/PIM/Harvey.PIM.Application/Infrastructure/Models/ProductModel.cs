﻿using Harvey.PIM.Application.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using static Harvey.PIM.Application.Infrastructure.Models.DetailFieldValueModel;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class ProductModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid FieldTemplateId { get; set; }
        //TODO need to return FieldTemplate object
        public string FieldTemplateName { get; set; }
        public List<Section> Sections { get; set; } = new List<Section>();
        public List<VariantModel> Variants { get; set; } = new List<VariantModel>();
        public Guid? CategoryId { get; set; }
        public string Code { get; set; }
        public bool IsDelete { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public class Section : SectionModel
        {
            public List<dynamic> FieldValues = new List<dynamic>();
        }

        public class VariantModel : SectionModel
        {
            public Guid Id { get; set; }
            public List<dynamic> FieldValues = new List<dynamic>();
            public PriceModel Price { get; set; }
            public string Code { get; set; }
        }
    }

    public class AddProductModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid CategoryId { get; set; }
        public string Description { get; set; }
        public Guid FieldTemplateId { get; set; }
        public List<FieldValueModel> ProductFields { get; set; }
        public List<AddVariantModel> Variants { get; set; }
        public string Code { get; set; }
        public bool IsDelete { get; set; }

        public class AddVariantModel
        {
            public Guid Id { get; set; }
            public List<FieldValueModel> VariantFields { get; set; } = new List<FieldValueModel>();
            public PriceModel Prices { get; set; }
            public ItemActionType Action { get; set; } = ItemActionType.Get;
            public string Code { get; set; }

            public string Name { get; set; }

            public string Description{ get; set; }
        }
    }

    public class UpdateProductModel : AddProductModel
    {

    }
    public class AddVariantCsvModel
    {
        public List<FieldValueCsvModel> VariantFields { get; set; } = new List<FieldValueCsvModel>();
        public PriceModel Prices { get; set; }
        public string VariantCode { get; set; }
        public ItemActionType Action { get; set; } = ItemActionType.Get;
    }

    public class AddProductCsvModel
    {
        public AddProductModel ProductModel { get; set; }
        public List<FieldValueCsvModel> ListProductField{ get; set; }
        public List<AddVariantCsvModel> ListVariant { get; set; }
        public string FieldTemplateID { get; set; }
        public string CategoryID { get; set; }
    }

    public class VariantViewModel
    {
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
    }
}
