﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Infrastructure.Models
{
    public class BarCodeModel
    {
        public Guid Id { get; set; }
        public Guid VariantId { get; set; }
        public string Code { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class GenerateBarCodeRequestModel
    {
        public List<Guid> VariantIds { get; set; }
    }
    
    public class BarCodeProductViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public List<BarCodeVariantViewModel> BarCodesVariant { get; set; }
    }

    public class BarCodeVariantViewModel
    {
        public Guid VariantId { get; set; }
        public string VariantSKU { get; set; }
        public List<VariantField> Fields { get; set; }
        public List<BarCodeVariantItemViewModel> BarCodes { get; set; }
    }

    public class BarCodeVariantItemViewModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
    }
}
