﻿using Harvey.Domain;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Harvey.PIM.Application.Infrastructure
{
    public class PimDbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Assortment> Assortments { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<FieldTemplate> FieldTemplates { get; set; }
        public DbSet<Field_FieldTemplate> Field_FieldTemplates { get; set; }
        public DbSet<FieldValue> FieldValues { get; set; }
        public DbSet<EntityRef> EntityRefs { get; set; }
        public DbSet<AppSetting> AppSettings { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Channel> Channels { get; set; }
        public DbSet<Variant> Variants { get; set; }
        public DbSet<AssortmentAssignment> AssortmentAssignments { get; set; }
        public DbSet<ChannelAssignment> ChannelAssignments { get; set; }
        public DbSet<ChannelStoreAssignment> ChannelStoreAssignments { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Harvey.Domain.NextSequence> NextSequences { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<PurchaseOrderItem> purchaseOrderItems { get; set; }
        public DbSet<BarCode> BarCode { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<TimeToFeed> TimeToFeeds { get; set; }

        public PimDbContext(DbContextOptions<PimDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<Assortment>());
            Setup(modelBuilder.Entity<Category>());
            Setup(modelBuilder.Entity<Field>());
            Setup(modelBuilder.Entity<FieldValue>());
            Setup(modelBuilder.Entity<FieldTemplate>());
            Setup(modelBuilder.Entity<Field_FieldTemplate>());
            Setup(modelBuilder.Entity<EntityRef>());
            Setup(modelBuilder.Entity<AppSetting>());
            Setup(modelBuilder.Entity<Brand>());
            Setup(modelBuilder.Entity<Location>());
            Setup(modelBuilder.Entity<Product>());
            Setup(modelBuilder.Entity<Variant>());
            Setup(modelBuilder.Entity<Channel>());
            Setup(modelBuilder.Entity<Price>());
            Setup(modelBuilder.Entity<AssortmentAssignment>());
            Setup(modelBuilder.Entity<ChannelAssignment>());
            Setup(modelBuilder.Entity<ChannelStoreAssignment>());
            Setup(modelBuilder.Entity<Harvey.Domain.NextSequence>());
            Setup(modelBuilder.Entity<Vendor>());
            Setup(modelBuilder.Entity<PurchaseOrder>());
            Setup(modelBuilder.Entity<PurchaseOrderItem>());
            Setup(modelBuilder.Entity<BarCode>());
            Setup(modelBuilder.Entity<PaymentMethod>());
            Setup(modelBuilder.Entity<TimeToFeed>());
        }

        public void Setup(EntityTypeBuilder<Assortment> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
        }
        public void Setup(EntityTypeBuilder<Category> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig.HasIndex(x => x.IdentifiedId).IsUnique();

            entityConfig.HasIndex(x => x.Code).IsUnique();
            entityConfig.Property(x => x.Code).HasMaxLength(3);
        }

        public void Setup(EntityTypeBuilder<Field> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig.HasIndex(x => x.IdentifiedId).IsUnique();
            entityConfig.HasMany(x => x.FieldValues)
                .WithOne(x => x.Field)
                .HasForeignKey(x => x.FieldId);
        }

        public void Setup(EntityTypeBuilder<FieldValue> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);

            entityConfig.HasOne(x => x.Field)
                .WithMany(x => x.FieldValues)
                .HasForeignKey(x => x.FieldId);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        internal object FirstOrDefaul()
        {
            throw new NotImplementedException();
        }

        public void Setup(EntityTypeBuilder<FieldTemplate> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);

            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig.HasIndex(x => x.IdentifiedId).IsUnique();
        }

        public void Setup(EntityTypeBuilder<Field_FieldTemplate> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);

            entityConfig.
                HasIndex(x => new { x.FieldId, x.FieldTemplateId }).IsUnique();
        }

        public void Setup(EntityTypeBuilder<EntityRef> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<AppSetting> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<Brand> entityConfig)
        {
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig.HasIndex(x => x.Code).IsUnique();
        }

        public void Setup(EntityTypeBuilder<Location> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => new { x.Name, x.Type }).IsUnique();
        }

        public void Setup(EntityTypeBuilder<Product> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

            //entityConfig.HasMany(x => x.Variants)
            //    .WithOne(x => x.Product)
            //    .HasForeignKey(x => x.ProductId);
        }

        public void Setup(EntityTypeBuilder<Channel> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Name).IsUnique();
            entityConfig.HasIndex(x => x.ServerInformation).IsUnique();
        }

        public void Setup(EntityTypeBuilder<Variant> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.HasMany(x => x.BarCodes)
               .WithOne(x => x.Variant)
               .HasForeignKey(x => x.VariantId);
        }

        public void Setup(EntityTypeBuilder<Price> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.ToTable("Price");
            entityConfig
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        public void Setup(EntityTypeBuilder<AssortmentAssignment> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => new { x.AssortmentId, x.ReferenceId, x.EntityType }).IsUnique();
        }

        public void Setup(EntityTypeBuilder<ChannelAssignment> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => new { x.ChannelId, x.ReferenceId, x.EntityType }).IsUnique();
        }

        public void Setup(EntityTypeBuilder<ChannelStoreAssignment> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => new { x.StoreId }).IsUnique();
            entityConfig.HasOne(x => x.Channel)
               .WithMany(x => x.ChannelStoreAssignments)
               .HasForeignKey(x => x.ChannelId);
            entityConfig.HasOne(x => x.Store)
               .WithMany(x => x.ChannelStoreAssignments)
               .HasForeignKey(x => x.StoreId);
        }

        public void Setup(EntityTypeBuilder<Harvey.Domain.NextSequence> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Key).IsUnique();
        }

        public void Setup(EntityTypeBuilder<Vendor> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        public void Setup(EntityTypeBuilder<PurchaseOrder> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        public void Setup(EntityTypeBuilder<PurchaseOrderItem> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
        }

        public void Setup(EntityTypeBuilder<BarCode> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
        }

        public void Setup(EntityTypeBuilder<PaymentMethod> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Code).IsUnique();
        }

        public void Setup(EntityTypeBuilder<TimeToFeed> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
    }

    public class TransientPimDbContext : PimDbContext
    {
        public TransientPimDbContext(DbContextOptions<PimDbContext> options) : base(options)
        {
        }
    }
}
