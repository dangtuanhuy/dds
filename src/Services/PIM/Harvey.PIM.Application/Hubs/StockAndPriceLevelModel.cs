﻿using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.PIM.Application.Hubs
{
    public class StockAndPriceLevelModel
    {
        public LocationType LocationType { get; set; }
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
        public string SKUCode { get; set; }
        public string CategoryName { get; set; }
        public float ListPrice { get; set; }
        public float MemberPrice { get; set; }
        public float StaffPrice { get; set; }
        public int Quantity { get; set; }
    }
}
