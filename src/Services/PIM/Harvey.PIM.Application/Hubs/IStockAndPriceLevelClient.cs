﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Hubs
{
    public interface IStockAndPriceLevelClient
    {
        Task BroadcastMessage(List<StockAndPriceLevelModel> stockAndPriceLevels);
    }
}
