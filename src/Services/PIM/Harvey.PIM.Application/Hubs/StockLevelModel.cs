﻿using Harvey.PIM.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;

namespace Harvey.PIM.Application.Hubs
{
    public class StockLevelModel
    {
        public string ReferenceId { get; set; }
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }
        public Guid ProductId { get; set; }
        public Guid VariantId { get; set; }
        public int Balance { get; set; }
        public string ProductName { get; set; }
        public List<VariantField> Fields { get; set; }
        public List<string> BarCodes { get; set; }
        public string SKUCode { get; set; }
    }
}
