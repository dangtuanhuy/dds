﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Hubs
{
    public interface IStockLevelClient
    {
        Task BroadcastMessage(List<StockLevelModel> stockLevels);
    }
}
