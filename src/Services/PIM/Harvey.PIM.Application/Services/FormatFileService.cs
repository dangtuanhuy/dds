﻿using CsvHelper;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public class FormatFileService
    {
        private readonly IEfRepository<PimDbContext, Brand> _efRepository;
        private readonly PimDbContext _pimDbContext;
        private List<string> messages = new List<string>();
        public FormatFileService(IEfRepository<PimDbContext, Brand> efRepository, PimDbContext pimDbContext)
        {
            _efRepository = efRepository;
            _pimDbContext = pimDbContext;
        }

        public StringBuilder FormatProductCSVFile(IFormFile file, Guid userId)
        {
            return FormatFile(file);
        }

        private StringBuilder FormatFile(IFormFile file)
        {
            StringBuilder result = null;
            using (var textReader = new StreamReader(file.OpenReadStream()))
            {
                using (var csv = new CsvParser(textReader))
                {
                    List<List<string>> listToSave = new List<List<string>>();
                    listToSave.Add(new List<string>
                        {
                            "Name",
                            "Description",
                            "FieldTempateID",
                            "CategoryID"
                        });
                    string[] row = null;
                    csv.Read();
                    var currentProductName = "";
                    while ((row = csv.Read()) != null)
                    {
                        List<string> lst = new List<string>();

                        for (int i = 0; i < row.Length; i++)
                        {
                            var cell = row[i];
                            if (string.IsNullOrEmpty(cell))
                            {
                                break;
                            }

                            if (i == 0)
                            {
                                if (cell != currentProductName)
                                {
                                    currentProductName = cell;
                                    lst.Add(cell);
                                    continue;
                                }

                                if (cell == currentProductName)
                                {
                                    i = 9;
                                    lst.Add("");
                                }
                                continue;
                            }

                            if (cell == "&&")
                            {
                                listToSave.Add(lst);
                                lst = new List<string>
                                    {
                                        ""
                                    };

                                continue;
                            }
                            lst.Add(cell);
                        }

                        listToSave.Add(lst);
                    }

                    var records = new List<object[]>();
                    listToSave.ForEach(line =>
                    {
                        var length = line.Count;
                        object[] ob = new object[length];
                        for (int i = 0; i < length; i++)
                        {
                            var cellString = line[i].Contains(",") ? line[i].Replace(",", "\\" + ",") : line[i];
                            ob[i] = cellString;
                        }

                        records.Add(ob);
                    });

                    var stringBuilder = new StringBuilder();
                    records.ForEach(line =>
                    {
                        stringBuilder.AppendLine(string.Join(",", line));
                    });

                    result = stringBuilder;
                }
            }

            return result;
        }
    }

}
