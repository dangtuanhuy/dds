﻿using Harvey.Domain;
using Harvey.NextSequence;
using Harvey.PIM.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public class InventoryTransactionService
    {
        private readonly TransientPimDbContext _transientPimDbContext;
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        public InventoryTransactionService(TransientPimDbContext transientPimDbContext) {
            _transientPimDbContext = transientPimDbContext;
            _nextSequenceService = new NextSequenceService<TransientPimDbContext>(_transientPimDbContext);
        }

        public async Task<string> GenerateInventoryTransactionCode(string keyName)
        {
            DateTime dateTimeNow = DateTime.UtcNow;
            var currentTime = dateTimeNow.ToString("MMyy", System.Globalization.CultureInfo.InvariantCulture);
            var inventoryTransactionCode = string.Format("{0}{1}", keyName ,currentTime);
            var indetityNumber = _nextSequenceService.Get(inventoryTransactionCode);
            inventoryTransactionCode = string.Format("{0}{1}", inventoryTransactionCode, indetityNumber);
            return inventoryTransactionCode;
        }
    }
}
