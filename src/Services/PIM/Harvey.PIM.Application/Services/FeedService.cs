﻿using Harvey.PIM.Application.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Queries.Products;
using Harvey.PIM.Application.Channels.Products;
using Harvey.PIM.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;

namespace Harvey.PIM.Application.Services
{
    public class FeedService
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        public FeedService(
            IQueryExecutor queryExecutor,
            PimDbContext pimDbContext,
            TransactionDbContext transactionDbContext
            )
        {
            _queryExecutor = queryExecutor;
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
        }

        public async Task<IEnumerable<ProductFeed>> GetProducts(DateTime lastSync)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetProductFieldValue(lastSync));
            return result;
        }

        public async Task<int> CountProducts(DateTime? lastSync)
        {
            if (lastSync == null)
            {
                return await _pimDbContext.Products.CountAsync();
            }
            else
            {
                return await _pimDbContext.Products.Where(x => x.UpdatedDate > lastSync).CountAsync();
            }
        }

        public async Task<List<Channel>> GetChannels(DateTime lastSync)
        {
            var result = new List<Channel>();
            var feedTime = lastSync != DateTime.MinValue ? lastSync.ToString() : null;
            if (feedTime != null)
            {
                result = await _pimDbContext.Channels.AsNoTracking().Where(x => x.UpdatedDate > lastSync).ToListAsync();
            }
            else
            {
                result = await _pimDbContext.Channels.AsNoTracking().ToListAsync();
            }
            return result;
        }

        public async Task<List<Location>> GetStores(DateTime lastSync)
        {
            var result = new List<Location>();
            var feedTime = lastSync != DateTime.MinValue ? lastSync.ToString() : null;
            if (feedTime != null)
            {
                result = await _pimDbContext.Locations
                                 .Where(x => x.Type == LocationType.Store && x.UpdatedDate > lastSync)
                                 .AsNoTracking().ToListAsync();
            }
            else
            {
                result = await _pimDbContext.Locations
                                .Where(x => x.Type == LocationType.Store)
                                .AsNoTracking().ToListAsync();
            }
            return result;
        }

        public async Task<List<ChannelStoreAssignment>> GetChannelStoreAssignments()
        {
            var result = await _pimDbContext.ChannelStoreAssignments.AsNoTracking().ToListAsync();
            return result;
        }

        public async Task<List<Variant>> GetVariants(DateTime? lastSync)
        {
            var result = new List<Variant>();
            var feedTime = lastSync != DateTime.MinValue ? lastSync.ToString() : null;
            if (feedTime != null)
            {
                result = await _pimDbContext.Variants.AsNoTracking().Where(x => x.UpdatedDate > lastSync).ToListAsync();
            }
            else
            {
                result = await _pimDbContext.Variants.AsNoTracking().ToListAsync();
            }
            return result;
        }

        public async Task<List<Price>> GetPrices(DateTime? lastSync)
        {
            var result = new List<Price>();
            var feedTime = lastSync != DateTime.MinValue ? lastSync.ToString() : null;
            if (feedTime != null)
            {
                result = await _pimDbContext.Prices.AsNoTracking().Where(x => x.UpdatedDate > lastSync).ToListAsync();
            }
            else
            {
                result = await _pimDbContext.Prices.AsNoTracking().ToListAsync();
            }
            return result;
        }

        public async Task<List<BarCode>> GetBarcodes(DateTime? lastSync)
        {
            var result = new List<BarCode>();
            var feedTime = lastSync != DateTime.MinValue ? lastSync.ToString() : null;
            if (feedTime != null)
            {
                result = await _pimDbContext.BarCode.AsNoTracking().Where(x => x.CreatedDate > lastSync).ToListAsync();
            }
            else
            {
                result = await _pimDbContext.BarCode.AsNoTracking().ToListAsync();
            }
            return result;
        }

        public async Task<List<Category>> GetCategorys(DateTime lastSync)
        {
            var result = new List<Category>();
            var feedTime = lastSync != DateTime.MinValue ? lastSync.ToString() : null;
            if (feedTime != null)
            {
                result = await _pimDbContext.Categories.AsNoTracking().Where(x => x.UpdatedDate > lastSync).ToListAsync();
            }
            else
            {
                result = await _pimDbContext.Categories.AsNoTracking().ToListAsync();
            }
            return result;
        }

        public async Task<List<StockType>> GetStockTypes()
        {
            var result = await _transactionDbContext.StockTypes.AsNoTracking().ToListAsync();
            return result;
        }

        public async Task<List<TransactionType>> GetTransactionTypes()
        {
            var result = await _transactionDbContext.TransactionTypes.AsNoTracking().ToListAsync();
            return result;
        }

        public async Task<List<Location>> GetLocations(DateTime lastSync)
        {
            var result = new List<Location>();
            var feedTime = lastSync != DateTime.MinValue ? lastSync.ToString() : null;
            if (feedTime != null)
            {
                result = await _pimDbContext.Locations.AsNoTracking().Where(x => x.UpdatedDate > lastSync).ToListAsync();
            }
            else
            {
                result = await _pimDbContext.Locations.AsNoTracking().ToListAsync();
            }
            return result;
        }

        public async Task<List<PaymentMethod>> GetPaymentMethods()
        {            
            return await _pimDbContext.PaymentMethods.AsNoTracking().ToListAsync();
        }
    }
}
