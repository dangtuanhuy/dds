﻿using CsvHelper;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public class DeploymentBrandService
    {
        private readonly IEfRepository<PimDbContext, Brand> _efRepository;
        private readonly PimDbContext _pimDbContext;
        private List<string> messages = new List<string>();
        public DeploymentBrandService(IEfRepository<PimDbContext, Brand> efRepository, PimDbContext pimDbContext)
        {
            _efRepository = efRepository;
            _pimDbContext = pimDbContext;
        }

        public async Task<DeploymentResult> Execute(Guid userId, IFormFile file)
        {
            var listBrand = await ExtractBrandFromFileAsync(file);
            var result = DeploymentResult.Success();
            if (messages.Count() > 0)
                return DeploymentResult.Fail(string.Join(",", messages));
            result = await ExecuteInternal(userId, listBrand);
            return result;
        }

        private async Task<List<BrandCSVModel>> ExtractBrandFromFileAsync(IFormFile file)
        {
            var memoryStream = new MemoryStream();
            
            await file.CopyToAsync(memoryStream);
            memoryStream.Position = 0;
            TextReader textReader = new StreamReader(memoryStream);

            var csv = new CsvReader(textReader);

            csv.Configuration.HasHeaderRecord = true;
            csv.Configuration.Delimiter = ",";
            csv.Configuration.MissingFieldFound = null;
            csv.Configuration.IgnoreBlankLines = true;
            csv.Configuration.TrimOptions = CsvHelper.Configuration.TrimOptions.Trim;
            csv.Read();
            csv.Context.Record = csv.Context.Record.Reverse().SkipWhile(string.IsNullOrWhiteSpace).Reverse().ToArray();
            csv.ReadHeader();
            var records = csv.GetRecords<BrandCSVModel>().ToList();

            if (records.Any(x => string.IsNullOrEmpty(x.Name)))
            {
                messages.Add($"Name of Brand(s) is empty!");
            }
            if (records.Any(x => string.IsNullOrEmpty(x.Code)))
            {
                messages.Add($"Code of Field(s) is empty!");
            }

            var brands = _pimDbContext.Brands.AsNoTracking().ToList();

            var brand = brands.FirstOrDefault(x => records.Any(y => y.Name?.Trim().ToUpper() == x.Name?.Trim().ToUpper()));

            if (brand != null)
            {
                messages.Add($"Brand with name {brand.Name} already exists.");
            }

            brand = brands.FirstOrDefault(x => records.Any(y => y.Code?.Trim() == x.Code?.Trim()));

            if (brand != null)
            {
                messages.Add($"Brand with code {brand.Code} already exists.");
            }

            return records;
        }

        private async Task<DeploymentResult> ExecuteInternal(Guid userId, List<BrandCSVModel> brands)
        {
            foreach (var brand in brands)
            {
                await _efRepository.AddAsync(new Brand()
                {
                    Name = brand.Name,
                    Code = brand.Code,
                    Description = brand.Description,
                    CreatedBy = userId,
                    UpdatedBy = userId
                });
            }

            return DeploymentResult.Success();
        }
    }
}
