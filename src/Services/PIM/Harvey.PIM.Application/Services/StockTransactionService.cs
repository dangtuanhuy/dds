﻿using Harvey.Domain;
using Harvey.PIM.Application.FieldFramework.Services.Interface;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public class StockTransactionService
    {
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        public StockTransactionService(
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
        }

        public async Task<List<ReturnOrderItem>> GetInfoSaleOrderItem(List<OrderItemRequestModel> orderItemModel, List<Variant> variants)
        {
            var listReturn = new List<ReturnOrderItem>();
            var variantIds = orderItemModel.Select(x => x.VariantId);

            var allGIWDocumentItemsOfVariants = _transactionDbContext.GIWDocumentItems.Include(x => x.StockType)
                                                        .Where(giwItem => giwItem.RemainingQty > 0 && variantIds.Contains(giwItem.VariantId))
                                                        .Select(giwItem => new
                                                        {
                                                            giwItem.GIWDocument.CreatedDate,
                                                            giwItem.GIWDocument.VendorId,
                                                            giwItem.GIWDocument.LocationId,
                                                            giwItem.Id,
                                                            giwItem.GIWDocumentId,
                                                            giwItem.VariantId,
                                                            giwItem.StockType,
                                                            giwItem.StockTypeId,
                                                            giwItem.Quantity,
                                                            giwItem.CostValue,
                                                            giwItem.CurrencyCode,
                                                            giwItem.RemainingQty
                                                        }).ToList();

            foreach (var orderItem in orderItemModel)
            {
                if(orderItem.Status == OrderItemStatus.Normal)
                {
                    List<String> stockTypes = new List<String> { "FIRM", "SOE", "SOR", "CON" };
                    var GIWDocumentItems = allGIWDocumentItemsOfVariants.Where(x => x.VariantId == orderItem.VariantId)
                            .OrderBy(item => stockTypes.IndexOf(item.StockType.Code)).ThenBy(x => x.CreatedDate).ToList();

                    if (GIWDocumentItems != null && GIWDocumentItems.Count > 0)
                    {
                        var totalSales = _transactionDbContext.StockTransactions.Include(x => x.TransactionType)
                        .Where(
                            x => (TransactionTypeCode)Enum.Parse(typeof(TransactionTypeCode), x.TransactionType.Code) == TransactionTypeCode.Sale
                            && x.VariantId == orderItem.VariantId
                        ).Sum(x => x.Quantity);

                        foreach (var item in GIWDocumentItems)
                        {
                            if (orderItem.Quantity <= 0)
                            {
                                break;
                            }
                            var giwDocumentItem = _transactionDbContext.GIWDocumentItems.FirstOrDefault(x => x.Id == item.Id);
                            if (item.RemainingQty - orderItem.Quantity >= 0)
                            {
                                var newReturnOrder = new ReturnOrderItem()
                                {
                                    Id = orderItem.Id,
                                    VariantId = orderItem.VariantId,
                                    Quantity = orderItem.Quantity,
                                    CreateDate = orderItem.CreatedDate,
                                    VariantName = variants.FirstOrDefault(x => x.Id == orderItem.VariantId).Description,
                                    CostValue = item.CostValue,
                                    StockTypeId = item.StockTypeId,
                                    OrderId = orderItem.OrderId,
                                    VendorId = item.VendorId,
                                    Status = orderItem.Status,
                                    WareHouseId = item.LocationId,
                                    GIWDocumentItemId = giwDocumentItem.Id
                                };
                                listReturn.Add(newReturnOrder);
                                giwDocumentItem.RemainingQty = item.RemainingQty - orderItem.Quantity;
                                orderItem.Quantity = 0;
                                _transactionDbContext.GIWDocumentItems.Update(giwDocumentItem);
                                break;
                            }
                            else
                            {
                                var newReturnOrder = new ReturnOrderItem()
                                {
                                    Id = orderItem.Id,
                                    VariantId = orderItem.VariantId,
                                    Quantity = item.RemainingQty,
                                    CreateDate = orderItem.CreatedDate,
                                    VariantName = variants.FirstOrDefault(x => x.Id == orderItem.VariantId).Description,
                                    CostValue = item.CostValue,
                                    StockTypeId = item.StockTypeId,
                                    OrderId = orderItem.OrderId,
                                    VendorId = item.VendorId,
                                    Status = orderItem.Status,
                                    WareHouseId = item.LocationId,
                                    GIWDocumentItemId = giwDocumentItem.Id
                                };
                                listReturn.Add(newReturnOrder);
                                orderItem.Quantity -= item.RemainingQty;
                                giwDocumentItem.RemainingQty = 0;
                                _transactionDbContext.GIWDocumentItems.Update(giwDocumentItem);
                            }

                        }
                    }
                }
                else if (orderItem.Status == OrderItemStatus.Exchange || orderItem.Status == OrderItemStatus.Refund)
                {
                    var giwDocumentItemsOfVariant = await _transactionDbContext.GIWDocumentItems.Include(x => x.GIWDocument)
                                                         .Where(x => x.Id == orderItem.GIWDocumentItemId)
                                                         .Select(giwItem => new
                                                         {
                                                             giwItem.GIWDocument.CreatedDate,
                                                             giwItem.GIWDocument.VendorId,
                                                             giwItem.GIWDocument.LocationId,
                                                             giwItem.Id,
                                                             giwItem.GIWDocumentId,
                                                             giwItem.VariantId,
                                                             giwItem.StockType,
                                                             giwItem.StockTypeId,
                                                             giwItem.Quantity,
                                                             giwItem.CostValue,
                                                             giwItem.CurrencyCode,
                                                             giwItem.RemainingQty
                                                         }).FirstOrDefaultAsync();
                    var giwDocumentItem = _transactionDbContext.GIWDocumentItems.FirstOrDefault(x => x.Id == giwDocumentItemsOfVariant.Id);
                    var refundOrExchangeOrder = new ReturnOrderItem()
                    {
                        Id = orderItem.Id,
                        VariantId = orderItem.VariantId,
                        Quantity = orderItem.Quantity,
                        CreateDate = orderItem.CreatedDate,
                        VariantName = variants.FirstOrDefault(x => x.Id == orderItem.VariantId).Description,
                        CostValue = giwDocumentItem.CostValue,
                        StockTypeId = giwDocumentItem.StockTypeId,
                        OrderId = orderItem.OrderId,
                        VendorId = giwDocumentItemsOfVariant.VendorId,
                        Status = orderItem.Status,
                        WareHouseId = giwDocumentItemsOfVariant.LocationId,
                        GIWDocumentItemId = giwDocumentItem.Id
                    };

                    listReturn.Add(refundOrExchangeOrder);
                    giwDocumentItem.RemainingQty += orderItem.Quantity;
                    _transactionDbContext.GIWDocumentItems.Update(giwDocumentItem);
                }
            }
            return listReturn;
        }
    }
}
