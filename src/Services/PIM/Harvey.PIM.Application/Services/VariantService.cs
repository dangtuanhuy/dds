﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public class VariantService
    {
        private readonly TransientPimDbContext _pimDbContext;
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        private readonly IEfRepository<PimDbContext, Variant> _variantRepository;
        public VariantService(TransientPimDbContext pimDbContext,
            INextSequenceService<TransientPimDbContext> nextSequenceService,
            IEfRepository<PimDbContext, Variant> variantRepository)
        {
            _pimDbContext = pimDbContext;
            _nextSequenceService = nextSequenceService;
            _variantRepository = variantRepository;
        }

        public async Task<string> GenerateSKU(string productSKU, string variantKey, int numberOfChars)
        {
            var skuCode = string.Empty;
            var variants = await _variantRepository.GetAsync();
            while(string.IsNullOrEmpty(skuCode))
            {
                var identityNumber = _nextSequenceService.Get(variantKey, numberOfChars);
                var hashCode = identityNumber.GetHashCode();
                string formatHashcode = string.Format("{0,10:X}", hashCode).Trim().Substring(4);
                skuCode = string.Format("{0}{1}", productSKU, formatHashcode);
                var variant = variants.FirstOrDefault(x => x.SKUCode == skuCode);
                if (variant != null)
                {
                    skuCode = string.Empty;
                }
            }
            return skuCode;
        }
    }
}
