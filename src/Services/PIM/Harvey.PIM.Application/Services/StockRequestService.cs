﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public class StockRequestService
    {
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        public StockRequestService(INextSequenceService<TransientPimDbContext> nextSequenceService)
        {
            _nextSequenceService = nextSequenceService;
        }

        public async Task<string> GenerateSubjectName()
        {
            DateTime dateTimeNow = DateTime.UtcNow;
            var currentTime = dateTimeNow.ToString("MMyy", System.Globalization.CultureInfo.InvariantCulture);
            var subjectName = string.Format("{0}{1}", "SR", currentTime);
            var indetityNumber = _nextSequenceService.Get(subjectName);
            subjectName = string.Format("{0}{1}", subjectName, indetityNumber);
            return subjectName;
        }
    }
}
