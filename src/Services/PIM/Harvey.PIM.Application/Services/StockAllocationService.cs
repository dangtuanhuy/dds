﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public class StockAllocationService
    {
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        public StockAllocationService(INextSequenceService<TransientPimDbContext> nextSequenceService)
        {
            _nextSequenceService = nextSequenceService;
        }

        public async Task<string> GenerateName()
        {
            DateTime dateTimeNow = DateTime.UtcNow;
            var currentTime = dateTimeNow.ToString("MMyy", System.Globalization.CultureInfo.InvariantCulture);
            var subjectName = string.Format("{0}{1}", "SA", currentTime);
            var indetityNumber = _nextSequenceService.Get(subjectName);
            subjectName = string.Format("{0}{1}", subjectName, indetityNumber);
            return subjectName;
        }

        public List<Guid> ParseStringToGuids(string data)
        {
            List<Guid> result = new List<Guid>();
            if (!string.IsNullOrWhiteSpace(data))
            {
                var arrayData = data.Split("|");
                for (int i = 0; i < arrayData.Length - 1; i++)
                {
                    result.Add(Guid.Parse(arrayData[i]));
                }
            }
            return result;
        }
    }
}
