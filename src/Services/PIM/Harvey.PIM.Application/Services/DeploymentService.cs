﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Exception.Extensions;
using Harvey.Persitance.EF;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.FieldFramework.Services.Interface;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Commands.Fields;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using static Harvey.PIM.Application.Infrastructure.Models.AddProductModel;
using static Harvey.PIM.Application.Infrastructure.Models.DetailFieldValueModel;
using static Harvey.PIM.Application.Infrastructure.Models.FieldTemplateModel;
using Harvey.EventBus.Events.Products;
using Harvey.EventBus.Events.Variants;
using Harvey.EventBus.Events.FieldValues;
using Harvey.EventBus.Events.Prices;
using Microsoft.Extensions.Configuration;
using Harvey.PIM.Application.Extensions;

namespace Harvey.PIM.Application.Services
{
    public class DeploymentService
    {
        private readonly IEventStoreRepository<Product> _eventStoreRepository;
        private ILogger<DeploymentService> _logger;
        private readonly IMapper _mapper;
        private readonly PimDbContext _pimDbContext;
        private readonly IFieldTemplateService _fieldTemplateService;
        private readonly ICommandExecutor _commandExecutor;
        private readonly IEfRepository<PimDbContext, Product> _efRepository;
        private readonly ISearchService _searchService;
        private readonly IConfiguration _configuration;
        private List<Field> listField = new List<Field>();
        private List<string> messages = new List<string>();
        private List<ProductCreatedEvent> _productsToImport = new List<ProductCreatedEvent>();
        private List<VariantCreatedEvent> _variantsToImport = new List<VariantCreatedEvent>();
        private List<FieldValueCreatedEvent> _fieldValuesToImport = new List<FieldValueCreatedEvent>();
        private List<PriceCreatedEvent> _pricesToImport = new List<PriceCreatedEvent>();
        public DeploymentService(
            IMapper mapper,
            PimDbContext pimDbContext,
            IFieldTemplateService fieldTemplateService,
            ICommandExecutor commandExecutor,
            IEfRepository<PimDbContext, Product> efRepository,
            ISearchService searchService,
            ILogger<DeploymentService> logger,
            IEventStoreRepository<Product> eventStoreRepository,
            IConfiguration configuration
            )
        {
            _logger = logger;
            _pimDbContext = pimDbContext;
            _mapper = mapper;
            _fieldTemplateService = fieldTemplateService;
            _commandExecutor = commandExecutor;
            _efRepository = efRepository;
            _searchService = searchService;
            _eventStoreRepository = eventStoreRepository;
            _configuration = configuration;
        }


        public async Task<DeploymentResult> Execute(Guid userId, IFormFile file)
        {
            try
            {
                var fieldTemplateList = ExtractFieldTemplateFromFile(file);
                var fieldList = ExtractFieldFromFile(file);
                var productList = ExtractProductFromFile(file, fieldTemplateList);
                var result = DeploymentResult.Success();
                if (messages.Count() > 0)
                    return DeploymentResult.Fail(string.Join(",", messages));
                result = await ExecuteInternal(userId, fieldTemplateList, fieldList, productList);
                return result;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex.GetTraceLog());
                return DeploymentResult.Fail(ex.Message);
            }

        }

        private async Task<DeploymentResult> ExecuteInternal(
            Guid userId,
            IList<FieldTemplateCsvModel> listFieldTemplateCsv,
            IList<FieldCsvModel> listFieldCsv,
            IList<AddProductCsvModel> listProductCsv)
        {

            var optionsBuilder = new DbContextOptionsBuilder<PimDbContext>();
            optionsBuilder.UseNpgsql(_configuration["ConnectionString"]);
            AddFields(listFieldCsv, userId, optionsBuilder);
            AddFieldTemplate(listFieldTemplateCsv);
            await AddProduct(listProductCsv, userId);
            ImportProduct(optionsBuilder);
            return DeploymentResult.Success();
        }

        private void AddFields(IList<FieldCsvModel> listFieldCsv, Guid userId, DbContextOptionsBuilder<PimDbContext> optionsBuilder)
        {
            if (listFieldCsv != null && listFieldCsv.Count > 0)
            {
                var allEntityRefs = _pimDbContext.EntityRefs.AsNoTracking().ToList();
                foreach (var field in listFieldCsv)
                {
                    var type = Enum.Parse(typeof(FieldType), field.Type);
                    FieldType Type = (FieldType)type;
                    string defaultValue = field.DefaultValue;
                    if (Type == FieldType.EntityReference)
                    {
                        var entityRef = allEntityRefs.SingleOrDefault(x => x.Name == field.DefaultValue);
                        defaultValue = $"{{\"id\": \"{entityRef.Id}\", \"name\": \"{entityRef.Name}\", \"namespace\": \"{entityRef.Namespace}\"}}";
                    }
                    var command = new AddFieldCommand(userId, field.Name, field.Description, field.IdentifiedId, Type, defaultValue);
                    var newField = new Field
                    {
                        Id = Guid.NewGuid(),
                        UpdatedBy = command.Creator,
                        CreatedBy = command.Creator,
                        Name = command.Name,
                        Description = command.Description,
                        DefaultValue = command.DefaultValue,
                        Type = command.Type,
                        IdentifiedId = field.IdentifiedId
                    };
                    listField.Add(newField);
                }
                using (var dbContext = new PimDbContext(optionsBuilder.Options))
                {
                    dbContext.AddRange(listField);
                    dbContext.SaveChanges();
                }
            }
        }

        private void AddFieldTemplate(IList<FieldTemplateCsvModel> listFieldTemplateCsv)
        {
            if (listFieldTemplateCsv != null && listFieldTemplateCsv.Count > 0)
            {
                var allFieldTemplates = _pimDbContext.FieldTemplates.AsNoTracking().ToList();
                var allFields = _pimDbContext.Fields.AsNoTracking().ToList();
                foreach (var item in listFieldTemplateCsv)
                {
                    if (allFieldTemplates.Any(x => x.IdentifiedId == item.IdentifiedId))
                        continue;
                    const string defaultSection = "DefaultSection";
                    const int defaultOrderSection = -1;
                    var listFieldFieldTemplate = new List<Field_FieldTemplate>();
                    int orderSection = 0;
                    item.FieldTemplateModel.IdentifiedId = item.IdentifiedId;
                    var query = item.Listfield.GroupBy(x => new { x.Section, x.Entity })
                        .Select(group => group).ToList();
                    item.FieldTemplateModel.Sections = new List<Section>();
                    foreach (var group in query)
                    {
                        var isVariantField = group.Any(x => x.Entity == "Variant");
                        var section = new Section
                        {
                            Fields = new List<Field_FieldTemplateModel>(),
                            Name = !string.IsNullOrEmpty(group.Key.Section) ? group.Key.Section : (group.Key.Entity == "Variant" ? "Variant" : defaultSection),
                            IsVariantField = isVariantField,
                            OrderSection = group.Any(x => x.Entity == "Variant") ? defaultOrderSection : orderSection
                        };
                        foreach (var fieldCsv in group)
                        {
                            var field = listField.SingleOrDefault(x => x.Name == fieldCsv.Name);
                            if (field == null)
                            {
                                field = allFields.SingleOrDefault(x => x.IdentifiedId == fieldCsv.IdentifiedId);
                            }
                            if (field != null)
                            {
                                var fieldFieldTemplateModel = new Field_FieldTemplateModel
                                {
                                    Field = _mapper.Map<FieldModel>(field),
                                    Id = Guid.Empty,
                                    Action = ItemActionType.Add
                                };
                                section.Fields.Add(fieldFieldTemplateModel);

                                var fieldFieldTemplate = new Field_FieldTemplate();
                                fieldFieldTemplate.OrderSection = section.OrderSection;
                                fieldFieldTemplate.IsVariantField = section.IsVariantField;
                                fieldFieldTemplate.Field = field;
                                fieldFieldTemplate.Section = section.Name;
                                fieldFieldTemplate.FieldId = field.Id;
                                listFieldFieldTemplate.Add(fieldFieldTemplate);
                            }
                        }
                        item.FieldTemplateModel.Sections.Add(section);
                        orderSection++;
                    }
                    var result = SaveFieldTemplate(item.FieldTemplateModel);
                    listFieldFieldTemplate.ForEach(x => x.FieldTemplateId = result.Id);
                }
            }
        }

        private FieldTemplate SaveFieldTemplate(FieldTemplateModel fieldTemplateModel)
        {
            var fieldTemplate = _mapper.Map<FieldTemplateModel, FieldTemplate>(fieldTemplateModel);
            if (fieldTemplate.Id == Guid.Empty)
            {
                _pimDbContext.Add(fieldTemplate);
                fieldTemplateModel.Id = fieldTemplate.Id;
            }
            else
            {
                _pimDbContext.Update(fieldTemplate);
            }

            var fields = fieldTemplateModel.Sections.SelectMany(x => x.Fields);

            var fieldFieldTemplates = _mapper.Map<FieldTemplateModel, List<Field_FieldTemplate>>(fieldTemplateModel);
            fieldFieldTemplates.ForEach(x =>
            {
                if (x.Id == Guid.Empty)
                {
                    _pimDbContext.Add(x);
                }
                else
                {
                    var field = fields.SingleOrDefault(f => f.Id == x.Id);
                    if (field != null)
                    {
                        switch (field.Action)
                        {
                            case ItemActionType.Update:
                                _pimDbContext.Update(x);
                                break;
                            case ItemActionType.Delete:
                                _pimDbContext.Remove(x);
                                break;
                        }
                    }
                }
            });
            return fieldTemplate;
        }

        private async Task AddProduct(IList<AddProductCsvModel> listProductCsv, Guid userId)
        {
            var allCategories = _pimDbContext.Categories.AsNoTracking().ToList();
            var allFieldTemplates = _pimDbContext.FieldTemplates.AsNoTracking().ToList();
            var allFields = _pimDbContext.Fields.AsNoTracking().ToList();
           
            var allFieldFieldTemplates = _pimDbContext
             .Field_FieldTemplates
             .Include(x => x.Field)
             .Include(x => x.FieldTemplate)
             .ToList();

            if (listProductCsv != null && listProductCsv.Count > 0)
            {
                foreach (var item in listProductCsv)
                {
                    var allProductFieldIds = allFieldFieldTemplates.Where(x => x.FieldTemplate.IdentifiedId == item.FieldTemplateID && !x.IsVariantField);

                    var fieldTemplate = allFieldTemplates.FirstOrDefault(x => x.IdentifiedId == item.FieldTemplateID);
                    item.ProductModel.FieldTemplateId = fieldTemplate.Id;
                    var category = allCategories.FirstOrDefault(x => x.IdentifiedId == item.CategoryID);
                    item.ProductModel.CategoryId = category.Id;
                    item.ProductModel.ProductFields = new List<FieldValueModel>();
                    item.ProductModel.ProductFields = item.ProductModel.ProductFields.Concat(GetFieldValues(item.ListProductField, allFields)).ToList();

                    var productFieldIds = item.ProductModel.ProductFields.Select(x => x.FieldId);
                    foreach (var field in allProductFieldIds)
                    {
                        if (!productFieldIds.Contains(field.FieldId))
                        {
                            var newFieldValueModel = new FieldValueModel();
                            newFieldValueModel.Id = Guid.Empty;
                            newFieldValueModel.FieldId = field.FieldId;
                            newFieldValueModel.FieldValue = SetDefaultFieldValue(field.Field.Type);
                            item.ProductModel.ProductFields.Add(newFieldValueModel);
                        }
                    }
                    item.ProductModel.Variants = new List<AddVariantModel>();
                    foreach (var variant in item.ListVariant)
                    {
                        var fieldValues = GetFieldValues(variant.VariantFields, allFields);
                        var variantModel = new AddVariantModel
                        {
                            Action = variant.Action,
                            Prices = variant.Prices,
                            VariantFields = fieldValues,
                            Code = variant.VariantCode,
                            Name = VariantExtensions.GetVariantName(fieldValues, allFields),
                            Description = VariantExtensions.GetVariantDescription(item.ProductModel.Name, fieldValues)
                        };
                        item.ProductModel.Variants.Add(variantModel);
                    }

                    await SaveProduct(userId, item.ProductModel, category.Name, allFieldFieldTemplates.Where(x => x.FieldTemplateId == item.ProductModel.FieldTemplateId).ToList());
                }
            }
        }

        private string SetDefaultFieldValue(FieldType fieldType)
        {
            switch (fieldType)
            {
                case FieldType.Checkbox:
                    return default(Boolean).ToString();
                case FieldType.EntityReference:
                    return string.Empty;
                case FieldType.Numeric:
                    return default(decimal).ToString();
                case FieldType.PredefinedList:
                    return null;
                case FieldType.RichText:
                    return string.Empty;
                case FieldType.Tags:
                    return string.Empty;
                case FieldType.Text:
                    return string.Empty;
                case FieldType.DateTime:
                    return default(DateTime).ToString();
                default:
                    return string.Empty;
            }
        }

        private async Task SaveProduct(Guid userId, AddProductModel productModel, string categoryName, List<Field_FieldTemplate> fields)
        {
            var productId = Guid.NewGuid();

            var product = new Product()
            {
                Id = productId,
                Name = productModel.Name,
                Description = productModel.Description,
                FieldTemplateId = productModel.FieldTemplateId,
                Code = productModel.Code,
                CreatedDate = DateTime.UtcNow,
                CreatedBy = userId,
                UpdatedDate = DateTime.UtcNow,
                UpdatedBy = userId
            };


            var addProductFieldValueModels = new List<DetailFieldValueModel>();
            foreach (var item in productModel.ProductFields)
            {
                var field = fields.Single(x => x.FieldId == item.FieldId);
                addProductFieldValueModels.Add(new DetailFieldValueModel(
                    productId,
                    field.Section,
                    field.OrderSection,
                    field.IsVariantField,
                    field.Field.Type,
                    item.FieldId,
                    field.Field.Name,
                    item.FieldValue)
                {
                    Id = Guid.NewGuid()
                });
            }

            var indexingValue = $"{string.Join(";", productModel.ProductFields.Select(x => x.FieldValue))},{categoryName}";

            product.AddProduct(userId, productId, productModel.CategoryId, categoryName, productModel.Name, productModel.Description, productModel.FieldTemplateId, addProductFieldValueModels, indexingValue, productModel.Code, product.CreatedDate);

            foreach (var item in addProductFieldValueModels)
            {
                _fieldValuesToImport.Add(new FieldValueCreatedEvent()
                {
                    FieldValueId = item.Id,
                    EntityId = productId,
                    FieldId = item.FieldId,
                    FieldValue = item.FieldValue,
                    FieldType = item.FieldType,
                    Section = item.Section,
                    OrderSection = item.OrderSection,
                    IsVariantField = item.IsVariantField,
                    FieldName = item.FieldName,
                    CreatedDate = product.CreatedDate
                });
            }
            _productsToImport.Add(new ProductCreatedEvent(productId.ToString())
            {
                CategoryId = productModel.CategoryId,
                Name = productModel.Name,
                Description = productModel.Description,
                FieldTemplateId = productModel.FieldTemplateId,
                Code = productModel.Code,
                CreatedDate = product.CreatedDate,
                Creator = userId
            });

            var addVariantFieldValueModels = new Dictionary<Guid, List<DetailFieldValueModel>>();
            var variantFieldValues = new List<string>();
            var addVariantPriceModel = new Dictionary<Guid, PriceModel>();
            var addVariantCode = new Dictionary<Guid, string>();

            if (productModel.Variants != null)
            {
                var variantFields = productModel.Variants.SelectMany(p => p.VariantFields).ToList();
                variantFieldValues = variantFields.Select(v => !String.IsNullOrEmpty(v.FieldValue) ? v.FieldValue.ToString() : String.Empty).ToList();

                foreach (var item in productModel.Variants)
                {
                    var variantId = Guid.NewGuid();
                    foreach (var variantField in item.VariantFields)
                    {
                        var field = fields.Single(x => x.FieldId == variantField.FieldId);
                        if (addVariantFieldValueModels.ContainsKey(variantId))
                        {
                            addVariantFieldValueModels[variantId].Add(
                                new DetailFieldValueModel(
                                   productId,
                                   field.Section,
                                   field.OrderSection,
                                   field.IsVariantField,
                                   field.Field.Type,
                                   variantField.FieldId,
                                   field.Field.Name,
                                   variantField.FieldValue)
                                {
                                    Id = Guid.NewGuid()
                                });

                            addVariantPriceModel[variantId] = item.Prices;
                        }
                        else
                        {
                            addVariantFieldValueModels.Add(
                              variantId,
                              new List<DetailFieldValueModel>() {
                                   new DetailFieldValueModel(
                                   productId,
                                   field.Section,
                                   field.OrderSection,
                                   field.IsVariantField,
                                   field.Field.Type,
                                   variantField.FieldId,
                                   field.Field.Name,
                                   variantField.FieldValue)
                                   {
                                       Id = Guid.NewGuid()
                                   }
                              });
                            addVariantPriceModel.Add(variantId, item.Prices);
                        }
                    }

                    addVariantCode.Add(variantId, item.Code);

                    product.AddVariant(product.Id, variantId, addVariantFieldValueModels[variantId], addVariantPriceModel[variantId], addVariantCode[variantId]);

                    _variantsToImport.Add(new VariantCreatedEvent()
                    {
                        ProductId = product.Id,
                        VariantId = variantId,
                        Code = addVariantCode[variantId],
                        CreatedDate = product.CreatedDate,
                        Name = item.Name,
                        Desciption = item.Description
                    });

                    foreach (var field in addVariantFieldValueModels[variantId])
                    {
                        var Id = Guid.NewGuid();
                        if (item.Id != Guid.Empty)
                        {
                            Id = item.Id;
                        }

                        _fieldValuesToImport.Add(new FieldValueCreatedEvent()
                        {
                            FieldValueId = Id,
                            EntityId = variantId,
                            FieldId = field.FieldId,
                            FieldValue = field.FieldValue,
                            FieldType = field.FieldType,
                            Section = field.Section,
                            OrderSection = field.OrderSection,
                            IsVariantField = field.IsVariantField,
                            FieldName = field.FieldName,
                            CreatedDate = product.CreatedDate
                        });

                        _pricesToImport.Add(new PriceCreatedEvent()
                        {
                            PriceId = Guid.NewGuid(),
                            VariantId = variantId,
                            ListPrice = addVariantPriceModel[variantId].ListPrice,
                            MemberPrice = addVariantPriceModel[variantId].MemberPrice,
                            StaffPrice = addVariantPriceModel[variantId].StaffPrice,
                            PreOrderPrice = addVariantPriceModel[variantId].PreOrderPrice,
                            CreatedDate = product.CreatedDate
                        });
                    }
                }
            }

            await _eventStoreRepository.SaveAsync(product, storeOnly: true);
        }

        private void ImportProduct(DbContextOptionsBuilder<PimDbContext> optionsBuilder)
        {
            var products = _productsToImport;
            var variants = _variantsToImport;
            var fieldValues = _fieldValuesToImport;
            var prices = _pricesToImport;

            using (var dbContext = new PimDbContext(optionsBuilder.Options))
            {
                dbContext.Products.AddRange(products.Select(@env => new Product()
                {
                    Id = Guid.Parse(@env.AggregateId),
                    Name = @env.Name,
                    CategoryId = @env.CategoryId,
                    Description = @env.Description,
                    FieldTemplateId = @env.FieldTemplateId,
                    Code = @env.Code,
                    IsDelete = false,
                    CreatedBy = @env.Creator,
                    UpdatedBy = @env.Creator,
                    CreatedDate = @env.CreatedDate,
                    UpdatedDate = @env.CreatedDate
                }));
                dbContext.SaveChanges();

            }
            
            using (var dbContext = new PimDbContext(optionsBuilder.Options))
            {
                dbContext.Variants.AddRange(variants.Select(@env => new Variant()
                {
                    Id = @env.VariantId,
                    ProductId = @env.ProductId,
                    Code = @env.Code,
                    PriceId = prices.FirstOrDefault(x => x.VariantId == env.VariantId) == null ? Guid.Empty : prices.FirstOrDefault(x => x.VariantId == env.VariantId).PriceId,
                    CreatedDate = @env.CreatedDate,
                    UpdatedDate = @env.CreatedDate,
                    Name = @env.Name,
                    Description = @env.Desciption
                }));
                dbContext.SaveChanges();

            }

            using (var dbContext = new PimDbContext(optionsBuilder.Options))
            {
                dbContext.FieldValues.AddRange(fieldValues.Select(@env =>
                {
                    var fieldValue = FieldValueFactory.CreateFromFieldType(@env.FieldType, @env.FieldValue);
                    fieldValue.Id = @env.FieldValueId;
                    fieldValue.FieldId = @env.FieldId;
                    fieldValue.EntityId = @env.EntityId;
                    fieldValue.DateTimeValue = @env.CreatedDate;
                    return fieldValue;
                }));
                dbContext.SaveChanges();

            }

            using (var dbContext = new PimDbContext(optionsBuilder.Options))
            {
                dbContext.Prices.AddRange(prices.Select(@env => new Price()
                {
                    Id = @env.PriceId,
                    StaffPrice = @env.StaffPrice,
                    MemberPrice = @env.MemberPrice,
                    ListPrice = @env.ListPrice,
                    PreOrderPrice = @env.PreOrderPrice,
                    HistoryId = @env.VariantId,
                    CreatedDate = @env.CreatedDate,
                    UpdatedDate = @env.CreatedDate
                }));

                dbContext.SaveChanges();
            }

        }

        private List<FieldValueModel> GetFieldValues(List<FieldValueCsvModel> listFieldValuesCsv, List<Field> allFields)
        {
            var result = new List<FieldValueModel>();
            foreach (var fieldValue in listFieldValuesCsv)
            {
                var field = listField.SingleOrDefault(x => x.IdentifiedId == fieldValue.IdentifiedId);
                if (field == null)
                {
                    field = allFields.SingleOrDefault(x => x.IdentifiedId == fieldValue.IdentifiedId);
                }
                result.Add(new FieldValueModel()
                {
                    Id = Guid.Empty,
                    FieldId = field.Id,
                    FieldValue = fieldValue.FieldValue
                });
            }
            return result;
        }

        #region helpers
        private List<FieldTemplateCsvModel> ExtractFieldTemplateFromFile(IFormFile file)
        {
            const string fieldTemplateText = "fieldtemplate";
            var listFieldTemplate = new List<FieldTemplateCsvModel>();
            using (var stream = file.OpenReadStream())
            using (var archive = new ZipArchive(stream))
            {
                foreach (var entry in archive.Entries)
                {
                    if (entry.FullName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
                    {
                        if (entry.FullName.Trim().ToLower().Contains(fieldTemplateText))
                        {
                            var result = ReadFieldTemplateFile(entry);
                            var fieldTemplate = new FieldTemplateCsvModel
                            {
                                FieldTemplateModel = new FieldTemplateModel()
                            };
                            fieldTemplate.FieldTemplateModel.Name = entry.Name.Substring(entry.Name.IndexOf("_") + 1, entry.Name.Length - 5 - entry.Name.IndexOf("_"));
                            fieldTemplate.IdentifiedId = entry.Name.Substring(0, entry.Name.IndexOf("_"));
                            fieldTemplate.Listfield = result;
                            listFieldTemplate.Add(fieldTemplate);
                        }
                    }
                }
            }
            return listFieldTemplate;
        }

        private List<FieldCsvModel> ExtractFieldFromFile(IFormFile file)
        {
            const string fieldTemplateText = "fieldtemplate";
            var allFields = _pimDbContext.Fields.AsNoTracking().ToList();
            var listFieldTemplate = new List<FieldTemplateCsvModel>();
            var listField = new List<FieldCsvModel>();
            using (var stream = file.OpenReadStream())
            using (var archive = new ZipArchive(stream))
            {
                foreach (var entry in archive.Entries)
                {
                    if (entry.FullName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
                    {
                        if (entry.FullName.Trim().ToLower().Contains(fieldTemplateText))
                        {
                            var result = ReadFieldTemplateFile(entry);
                            var fieldTemplate = new FieldTemplateCsvModel
                            {
                                FieldTemplateModel = new FieldTemplateModel()
                            };
                            fieldTemplate.FieldTemplateModel.Name = entry.Name.Substring(entry.Name.IndexOf("_") + 1, entry.Name.Length - 5 - entry.Name.IndexOf("_"));
                            fieldTemplate.IdentifiedId = entry.Name.Substring(0, entry.Name.IndexOf("_"));
                            fieldTemplate.Listfield = result;
                            listFieldTemplate.Add(fieldTemplate);
                            var validFields = result.Where(x => !allFields.Any(y => y.IdentifiedId == x.IdentifiedId)).ToList();
                            listField.AddRange(validFields.Where(p2 => listField.All(p1 => p1.IdentifiedId != p2.IdentifiedId)));
                        }
                    }
                }
            }
            var allEntityRefs = _pimDbContext.EntityRefs.AsNoTracking().ToList();
            foreach (var field in listField)
            {
                var type = new Object();
                if (!Enum.TryParse(typeof(FieldType), field.Type, true, out type))
                {
                    messages.Add($"Type of field {field.Name} is incorrect!");
                }
                FieldType Type = (FieldType)type;
                if (Type == FieldType.EntityReference)
                {
                    var entityRef = allEntityRefs.SingleOrDefault(x => x.Name == field.DefaultValue);
                    if (entityRef == null)
                    {
                        messages.Add($"Default value of field {field.Name} is invalid.");
                    }

                }
            }
            return listField;
        }

        private List<AddProductCsvModel> ExtractProductFromFile(IFormFile file, List<FieldTemplateCsvModel> fieldTemplates)
        {
            const string productText = "product";
            var listProduct = new List<AddProductCsvModel>();
            using (var stream = file.OpenReadStream())
            using (var archive = new ZipArchive(stream))
            {
                foreach (var entry in archive.Entries)
                {
                    if (entry.FullName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
                    {
                        if (entry.FullName.Trim().ToLower().Contains(productText))
                        {
                            var result = ReadProductFile(entry, fieldTemplates);
                            listProduct = listProduct.Concat(result).ToList();
                        }
                    }
                }
                return listProduct;
            }
        }

        private List<FieldCsvModel> ReadFieldTemplateFile(ZipArchiveEntry entry)
        {
            string[] EntityType = { "Product", "Variant" };
            var listFieldCsvFile = new List<FieldCsvModel>();
            using (var streamReader = new StreamReader(entry.Open()))
            {
                using (var csv = new CsvReader(streamReader))
                {
                    ConfigCsvHelper(csv);
                    csv.Read();
                    csv.Context.Record = csv.Context.Record.Reverse().SkipWhile(string.IsNullOrWhiteSpace).Reverse().ToArray();
                    csv.ReadHeader();
                    var records = csv.GetRecords<FieldCsvModel>().ToList();
                    var allNameOfFields = records.Select(x => x.Name.Trim().ToUpper()).ToList();
                    var allIdentifieldIdOfFields = records.Select(x => x.IdentifiedId).ToList();

                    if (records.Any(x => string.IsNullOrEmpty(x.Name)))
                    {
                        messages.Add($"File {entry.Name}: Name of Field(s) is empty!");
                    }
                    if (records.Any(x => string.IsNullOrEmpty(x.Entity)))
                    {
                        messages.Add($"File {entry.Name}: Entity of Field(s) is empty!");
                    }
                    if (records.Any(x => string.IsNullOrEmpty(x.Type)))
                    {
                        messages.Add($"File {entry.Name}: Type of Field(s) is empty!");
                    }
                    if (records.Any(x => string.IsNullOrEmpty(x.IdentifiedId)))
                    {
                        messages.Add($"File {entry.Name}: IdentifiedId of Field(s) is empty!");
                    }
                    if (records.Any(x => !EntityType.Contains(x.Entity)))
                    {
                        messages.Add($"File {entry.Name}: Entity is incorrect");
                    }
                    if (allNameOfFields.Count() != allNameOfFields.Distinct().Count())
                    {
                        var duplicates = allNameOfFields.GroupBy(p => p).Where(g => g.Count() > 1).Select(g => g.Key).ToList();
                        messages.Add($"File {entry.Name}: Duplicate field name:  {string.Join(";", duplicates)}");
                    }
                    if (allIdentifieldIdOfFields.Count() != allIdentifieldIdOfFields.Distinct().Count())
                    {
                        var duplicates = allIdentifieldIdOfFields.GroupBy(p => p).Where(g => g.Count() > 1).Select(g => g.Key).ToList();
                        messages.Add($"File {entry.Name}: Duplicate field IdentifiedId:  {string.Join(";", duplicates)}");
                    }
                    if (records.Any(x => x.Entity == EntityType[1] && x.Type != FieldType.PredefinedList.ToString()))
                    {
                        messages.Add($"File {entry.Name} is invalid. Only support variant with field type is PredefinedList.");
                    }

                    var fields = _pimDbContext.Fields.AsNoTracking().ToList();
                    var field = fields.FirstOrDefault(x => records.Any(y => y.Name.Trim().ToUpper() == x.Name.Trim().ToUpper() && y.IdentifiedId != x.IdentifiedId));
                    if (field != null)
                    {
                        messages.Add($"Field with name {field.Name} already exists.");
                    }
                    field = fields.FirstOrDefault(x => records.Any(y => y.IdentifiedId == x.IdentifiedId && y.Name.Trim().ToUpper() != x.Name.Trim().ToUpper()));
                    if (field != null)
                    {
                        messages.Add($"Field with identifiedId {field.IdentifiedId} already exists.");
                    }
                    if (records != null && records.Count() > 0)
                    {
                        foreach (var record in records)
                        {
                            if (!listFieldCsvFile.Any(x => x.IdentifiedId == record.IdentifiedId || x.Name == record.Name))
                            {
                                listFieldCsvFile.Add(record);
                            }
                            else
                            {
                                messages.Add($"Field with {record.IdentifiedId} already exists.");
                            }
                        }
                    }
                }
            }
            return listFieldCsvFile;
        }

        private List<AddProductCsvModel> ReadProductFile(ZipArchiveEntry entry, List<FieldTemplateCsvModel> fieldTemplates)
        {
            var listProduct = new List<AddProductCsvModel>();
            using (TextReader reader = new StreamReader(entry.Open()))
            {
                var parsedCsv = new CsvParser(reader);
                string[] row = null;
                parsedCsv.Read();
                parsedCsv.Configuration.BadDataFound = null;

                var allFieldTemplates = _pimDbContext.FieldTemplates.AsNoTracking().ToList();
                var allProducts = _pimDbContext.Products.AsNoTracking().ToList();
                var allCategories = _pimDbContext.Categories.AsNoTracking().ToList();
                var allFields = _pimDbContext.Fields.AsNoTracking().ToList();
                while ((row = parsedCsv.Read()) != null)
                {
                    var fieldModel = new FieldModel();
                    if (string.IsNullOrEmpty(row[0]))
                    {
                        ReadVariantLine(row, ref listProduct);
                    }
                    else
                    {
                        ReadProductLine(row, fieldTemplates, allFieldTemplates, allProducts, allCategories, allFields, ref listProduct);
                    }

                }
            }

            var allFieldFieldTemplates = _pimDbContext
             .Field_FieldTemplates
             .Include(x => x.Field)
             .Include(x => x.FieldTemplate)
             .ToList();
            foreach (var item in listProduct)
            {
                var fieldIds = allFieldFieldTemplates.Where(x => x.FieldTemplate.IdentifiedId == item.FieldTemplateID).Select(x => x.Field.IdentifiedId);
                if (fieldIds.Count() == 0)
                {
                    fieldIds = fieldTemplates.FirstOrDefault(x => x.IdentifiedId == item.FieldTemplateID)?.Listfield.Select(x => x.IdentifiedId).ToList();
                }
                var productFieldIds = item
                    .ListProductField
                    .Select(x => x.IdentifiedId)
                    .Union(item.ListVariant.SelectMany(x => x.VariantFields).Select(x => x.IdentifiedId));

                foreach (var field in productFieldIds)
                {
                    if (fieldIds != null && !fieldIds.Any(x => field == x))
                    {
                        messages.Add($"Product {item.ProductModel.Name} includes field(s): {field} not in template.");
                    }
                }
                var productFieldIdentifiedId = item.ListProductField.Select(x => x.IdentifiedId).ToList();
                if (productFieldIdentifiedId.Count() != productFieldIdentifiedId.Distinct().Count())
                {
                    var duplicates = productFieldIdentifiedId.GroupBy(p => p).Where(g => g.Count() > 1).Select(g => g.Key).ToList();
                    messages.Add($"Product {item.ProductModel.Name} duplicate product field(s): {string.Join(";", duplicates)}");
                }

                foreach (var variant in item.ListVariant)
                {
                    var allVariantFieldIds = allFieldFieldTemplates.Where(x => x.FieldTemplate.IdentifiedId == item.FieldTemplateID && x.IsVariantField).Select(x => x.Field.IdentifiedId).ToList();
                    var variantFieldIds = variant.VariantFields.Select(x => x.IdentifiedId).ToList();
                    var invalidVariantFields = allVariantFieldIds.Except(variantFieldIds);
                    if (invalidVariantFields.Count() > 0)
                    {
                        messages.Add($"Product {item.ProductModel.Name} missing value for variant field(s): {string.Join(";", invalidVariantFields)}");

                    }
                    if (allVariantFieldIds.Count() != variantFieldIds.Count())
                    {
                        var duplicates = variantFieldIds.GroupBy(p => p).Where(g => g.Count() > 1).Select(g => g.Key).ToList();
                        if (duplicates.Count() > 0)
                        {
                            messages.Add($"Product {item.ProductModel.Name} duplicate variant field(s): {string.Join(";", duplicates)}");
                        }
                    }
                }
            }
            return listProduct;
        }

        private void ReadVariantLine(string[] data, ref List<AddProductCsvModel> listProduct)
        {
            if (listProduct.Count > 0)
            {
                var addVariantCsv = new AddVariantCsvModel
                {
                    Prices = new PriceModel()
                };
                for (int i = 0; i < data.Length; i++)
                {
                    switch (i)
                    {
                        case 1:
                            var listPrice = default(float);
                            if (string.IsNullOrEmpty(data[i]))
                            {
                                addVariantCsv.Prices.ListPrice = listPrice;
                            }
                            else
                            {
                                if (float.TryParse(data[i], out listPrice))
                                    addVariantCsv.Prices.ListPrice = listPrice;
                                else
                                    messages.Add($"Cannot parse {listPrice} to float type.");
                            }
                            break;
                        case 2:
                            var staffPrice = default(float);
                            if (string.IsNullOrEmpty(data[i]))
                            {
                                addVariantCsv.Prices.ListPrice = staffPrice;
                            }
                            else
                            {
                                if (float.TryParse(data[i], out staffPrice))
                                    addVariantCsv.Prices.StaffPrice = staffPrice;
                                else
                                    messages.Add($"Cannot parse {staffPrice} to float type.");
                            }
                            break;
                        case 3:
                            var memberPrice = default(float);
                            if (string.IsNullOrEmpty(data[i]))
                            {
                                addVariantCsv.Prices.ListPrice = memberPrice;
                            }
                            else
                            {
                                if (float.TryParse(data[i], out memberPrice))
                                    addVariantCsv.Prices.MemberPrice = memberPrice;
                                else
                                    messages.Add($"Cannot parse {memberPrice} to float type.");
                            }
                            break;
                        case 4:
                            var preOrderPrice = default(float);
                            if (string.IsNullOrEmpty(data[i]))
                            {
                                addVariantCsv.Prices.ListPrice = preOrderPrice;
                            }
                            else
                            {
                                if (float.TryParse(data[i], out preOrderPrice))
                                    addVariantCsv.Prices.PreOrderPrice = preOrderPrice;
                                else
                                    messages.Add($"Cannot parse {preOrderPrice} to float type.");
                            }
                            break;
                        default:
                            if (!string.IsNullOrEmpty(data[i]))
                            {
                                var fieldValueModel = new FieldValueCsvModel();
                                if (data[i].IndexOf("|") < 0)
                                {
                                    messages.Add($"Wrong format: {data[i]}.");
                                }
                                else
                                {
                                    fieldValueModel.IdentifiedId = data[i].Substring(0, data[i].IndexOf("|"));
                                    fieldValueModel.FieldValue = data[i].Substring(data[i].IndexOf("|") + 1, data[i].Length - data[i].IndexOf("|") - 1);
                                }
                                addVariantCsv.VariantFields.Add(fieldValueModel);
                            }
                            break;
                    }
                    addVariantCsv.Action = ItemActionType.Add;
                }
                var listVariant = listProduct.Last().ListVariant;
                if (listVariant.Any(x => x.VariantFields.All(addVariantCsv.VariantFields.Contains)))
                {
                    messages.Add($"{listProduct.Last().ProductModel.Name} Duplicate variant.");
                }
                listProduct.Last().ListVariant.Add(addVariantCsv);
            }
        }

        private void ReadProductLine(
            string[] data,
            List<FieldTemplateCsvModel> fieldTemplates,
            List<FieldTemplate> existedFieldTemplates,
            List<Product> allProducts,
            List<Category> allCategories,
            List<Field> allFields,
            ref List<AddProductCsvModel> listProduct)
        {
            var productCSV = new AddProductCsvModel
            {
                ListProductField = new List<FieldValueCsvModel>(),
                ListVariant = new List<AddVariantCsvModel>()
            };

            var allCategoryID = allCategories.Select(x => x.IdentifiedId).ToList();
            var product = new AddProductModel();
            for (int i = 0; i < data.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        if (string.IsNullOrEmpty(data[i]))
                        {
                            messages.Add("Name of product(s) is empty.");
                        }
                        else
                        {
                            var nameOfProducts = listProduct.Select(x => x.ProductModel.Name).ToList();
                            var duplicateItems = allProducts.Where(x => x.Name == data[i] && !x.IsDelete).Select(x => x.Name).ToList();
                            if (duplicateItems.Count() > 0)
                            {
                                messages.Add($"Product {string.Join(";", duplicateItems)} has exists.");
                            }
                            if (nameOfProducts.Any(x => x.Trim().ToUpper() == data[i].Trim().ToUpper()))
                            {
                                messages.Add($"Duplicate Product Name.{data[i]}");
                            }
                        }
                        product.Name = data[i];
                        break;
                    case 1:
                        if (string.IsNullOrEmpty(data[i]))
                        {
                            messages.Add("Description of product(s) is empty.");
                        }
                        product.Description = data[i];
                        break;
                    case 2:
                        if (string.IsNullOrEmpty(data[i]))
                        {
                            messages.Add("FieldTemplateID of product(s) is empty.");
                        }
                        if (!fieldTemplates.Any(x => x.IdentifiedId == data[i]) && !existedFieldTemplates.Any(x => x.IdentifiedId == data[i]))
                        {
                            messages.Add("Missing field template.");
                        }
                        productCSV.FieldTemplateID = data[i];
                        break;
                    case 3:
                        if (string.IsNullOrEmpty(data[i]))
                        {
                            messages.Add("CategoryID of product(s) is empty.");
                        }
                        if (!allCategoryID.Any(x => x == data[i]))
                        {
                            messages.Add($"Categories {data[i]} is not exists.");
                        }
                        productCSV.CategoryID = data[i];
                        break;
                    default:
                        if (!string.IsNullOrEmpty(data[i]))
                        {
                            var fieldValueModel = new FieldValueCsvModel();
                            if (data[i].IndexOf("|") < 0)
                            {
                                messages.Add($"Wrong format: {data[i]}.");
                            }
                            else
                            {
                                fieldValueModel.IdentifiedId = data[i].Substring(0, data[i].IndexOf("|"));
                                fieldValueModel.FieldValue = data[i].Substring(data[i].IndexOf("|") + 1, data[i].Length - data[i].IndexOf("|") - 1);
                                var fieldCsv = fieldTemplates.FirstOrDefault(x => x.IdentifiedId == productCSV.FieldTemplateID)?.Listfield.FirstOrDefault(x => x.IdentifiedId == fieldValueModel.IdentifiedId);
                                if (fieldCsv == null)
                                {
                                    var field = allFields.SingleOrDefault(x => x.IdentifiedId == fieldValueModel.IdentifiedId);
                                    if (field == null)
                                    {
                                        messages.Add($"Product {product.Name}: Field is not exists.");
                                    }
                                }
                                else
                                {
                                    if ((fieldCsv.Type == FieldType.Numeric.ToString() && !decimal.TryParse(fieldValueModel.FieldValue, out decimal resDecimal)) ||
                                    (fieldCsv.Type == FieldType.Checkbox.ToString() && !bool.TryParse(fieldValueModel.FieldValue, out bool resBool)) ||
                                    (fieldCsv.Type == FieldType.DateTime.ToString() && !DateTime.TryParse(fieldValueModel.FieldValue, out DateTime resDateTime)))
                                    {
                                        messages.Add($"Product {product.Name}: Field value is not map with field type.");
                                    }
                                }
                            }
                            productCSV.ListProductField.Add(fieldValueModel);
                        }
                        break;
                }
            }
            productCSV.ProductModel = product;
            listProduct.Add(productCSV);
        }

        private void ConfigCsvHelper(CsvReader csv)
        {
            csv.Configuration.HasHeaderRecord = true;
            csv.Configuration.Delimiter = ",";
            csv.Configuration.MissingFieldFound = null;
            csv.Configuration.IgnoreBlankLines = true;
            csv.Configuration.TrimOptions = CsvHelper.Configuration.TrimOptions.Trim;
        }
        #endregion
    }

    public class DeploymentResult
    {
        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; }
        public static DeploymentResult Success() => new DeploymentResult() { IsSuccess = true };
        public static DeploymentResult Fail(string message) => new DeploymentResult() { IsSuccess = false, ErrorMessage = message };
    }
}
