﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.EventBus.Events.StockLevels;
using Harvey.Exception;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public class DeploymentStockTransactionService
    {
        private readonly IConfiguration _configuration;
        private readonly TransactionDbContext _transactionDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly InventoryTransactionService _inventoryTransactionService;
        private readonly ProductService _productService;
        private readonly ISearchService _searchService;
        private readonly IEventBus _eventBus;
        private readonly VariantService _variantService;
        private ILogger<DeploymentStockTransactionService> _logger;
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        public DeploymentStockTransactionService(
            IConfiguration configuration,
            TransactionDbContext transactionDbContext,
            PimDbContext pimDbContext,
            InventoryTransactionService inventoryTransactionService,
            ProductService productService,
            VariantService variantService,
            ISearchService searchService,
            IEventBus eventBus,
            ILogger<DeploymentStockTransactionService> logger,
            INextSequenceService<TransientPimDbContext> nextSequenceService)
        {
            _transactionDbContext = transactionDbContext;
            _pimDbContext = pimDbContext;
            _inventoryTransactionService = inventoryTransactionService;
            _productService = productService;
            _variantService = variantService;
            _searchService = searchService;
            _eventBus = eventBus;
            _logger = logger;
            _nextSequenceService = nextSequenceService;
            _configuration = configuration;
        }

        public async Task<DeploymentStockTransactionResult> Execute(Guid userId, IFormFile file, Guid fromLocationId, Guid toLocationId)
        {
            var location = _pimDbContext.Locations.FirstOrDefault(x => x.Id == toLocationId);
            if (location.Type == LocationType.Store)
            {
                var channel = _pimDbContext.Channels.FirstOrDefault(a => a.IsProvision == true
                                                                                && a.Id == a.ChannelStoreAssignments.FirstOrDefault(b => b.StoreId == location.Id).ChannelId);
                if (channel == null)
                {
                    throw new NotFoundException("Store is not assigned to Channel.");
                }
            }

            string message = "";
            int count = 0;
            if (file.Length > 0)
            {
                var stockTransactionCSVs = new List<StockTransactionCsvModel>();
                using (var streamReader = new StreamReader(file.OpenReadStream()))
                {
                    string line = "";
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        count += 1;
                        try
                        {
                            if (count > 1)
                            {
                                var data = line.Split(new[] { ',' });
                                var variantBarCodes = new List<string>();
                                variantBarCodes.AddRange(data[6].Split(new[] { '|' }));
                                var model = new StockTransactionCsvModel()
                                {
                                    ProductId = Guid.Parse(data[0]),
                                    ProductName = data[1],
                                    VariantId = Guid.Parse(data[2]),
                                    VariantName = data[3],
                                    PreOrderPrice = data[4],
                                    VariantSKU = data[5],
                                    VariantBarCodes = variantBarCodes,
                                    CostValue = float.Parse(data[7], CultureInfo.InvariantCulture.NumberFormat),
                                    CurrencyCode = data[8],
                                    LocationId = Guid.Parse(data[9]),
                                    LocationName = data[10],
                                    Category = data[11],
                                    StockOnHandSOE = data[12],
                                    StockOnHandSOR = data[13],
                                    StockOnHandFIRM = data[14],
                                    StockOnHandCON = data[15],
                                    StockOnHand = data[16],
                                    ActualStockOnHand = data[17],
                                    QuantitySOE = Convert.ToInt32(data[18]),
                                    QuantitySOR = Convert.ToInt32(data[19]),
                                    QuantityFIRM = Convert.ToInt32(data[20]),
                                    QuantityCON = Convert.ToInt32(data[21]),
                                    Quantity = Convert.ToInt32(data[22])

                                };
                                stockTransactionCSVs.Add(model);
                            }
                        }
                        catch (System.Exception e)
                        {
                            message += $"Line no {count} : Error : {e}";
                            return DeploymentStockTransactionResult.Fail(message);
                        }
                    }
                }

                if (stockTransactionCSVs.Count > 0)
                {
                    var isAddGIWsSuccess = await AddStockInitial(stockTransactionCSVs, userId, fromLocationId, toLocationId);
                    if (isAddGIWsSuccess)
                    {
                        return DeploymentStockTransactionResult.Success();
                    }
                    else
                    {
                        return DeploymentStockTransactionResult.Fail("Import failed");
                    }
                }
            }

            return DeploymentStockTransactionResult.Fail("Empty csv file");

        }

        private async Task<bool> AddStockInitial(List<StockTransactionCsvModel> stockInitials, Guid userId, Guid fromLocationId, Guid toLocationId)
        {

            var TransactionTypeGIW = _transactionDbContext.TransactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.GIW.ToString());
            var TransactionTypeTFO = _transactionDbContext.TransactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.TFO.ToString());

            var location = _pimDbContext.Locations.FirstOrDefault(x => x.Id == toLocationId);
            var vendorInitial = _pimDbContext.Vendors.FirstOrDefault(x => x.VendorCode == "SIV");
            var newBarCodes = new List<BarCode>();
            var updateStockLevelSearchItems = new List<StockLevelSearchItem>();
            var addStockLevelSearchItems = new List<StockLevelSearchItem>();
            if (TransactionTypeGIW == null || location == null || vendorInitial == null)
            {
                return false;
            }
            else
            {
                var _variantIds = stockInitials.Select(a => a.VariantId).ToList();
                var _productIds = stockInitials.Select(a => a.ProductId).ToList();
                var variants = stockInitials.Count > 20000 ? _pimDbContext.Variants.AsNoTracking().ToList() : _pimDbContext.Variants.Where(a => _variantIds.Contains(a.Id)).AsNoTracking().ToList();
                var products = stockInitials.Count > 20000 ? _pimDbContext.Products.AsNoTracking().ToList() : _pimDbContext.Products.Where(a => _productIds.Contains(a.Id)).AsNoTracking().ToList();
                var categories = _pimDbContext.Categories.AsNoTracking().ToList();
                var barCodes = stockInitials.Count > 20000 ? _pimDbContext.BarCode.AsNoTracking().ToList() : _pimDbContext.BarCode.Where(a => _variantIds.Contains(a.VariantId)).AsNoTracking().ToList();
                var stockTypes = _transactionDbContext.StockTypes.AsNoTracking().ToList();
                var stockType_SOE = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.SOE.ToString());
                var stockType_SOR = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.SOR.ToString());
                var stockType_FIRM = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.FIRM.ToString());
                var stockType_CON = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.CON.ToString());
                var stockTypeSTI = stockTypes.FirstOrDefault(x => x.Code == StockTypeCode.STI.ToString());


                //var nextSequences = GetListNextSequence();
                var numberOfCharsProduct = 5;
                var numberOfCharsVariant = 6;
                var variantKey = "variants";
                var datetimeNow = DateTime.Now;
                var newGIWItems = new List<GIWDocumentItem>();
                var newStockTransactions = new List<StockTransaction>();
                var updateProducts = new List<Product>();
                var updateVariants = new List<Variant>();
                var insertBarcodes = new List<BarCode>();
                var newGIWDocument = new GIWDocument();

                var inventoryTransaction = new InventoryTransaction();

                var optionbuilder = new DbContextOptionsBuilder<PimDbContext>();
                optionbuilder.UseNpgsql(_configuration["ConnectionString"]);

                if (location.Type == LocationType.Warehouse)
                {
                    var stockTransactions = stockInitials.Count > 20000 ? _transactionDbContext.StockTransactions.Where(x => x.LocationId == location.Id).AsNoTracking().ToList() :
                                                                     _transactionDbContext.StockTransactions.Where(x => x.LocationId == location.Id && _variantIds.Contains(x.VariantId)).AsNoTracking().ToList();

                    stockTransactions = stockTransactions.OrderByDescending(x => x.CreatedDate).ToList();

                    var inventoryTransactionCode = await _inventoryTransactionService.GenerateInventoryTransactionCode(TransactionTypeCode.GIW.ToString());

                    newGIWDocument = new GIWDocument()
                    {
                        Id = Guid.NewGuid(),
                        Name = Guid.NewGuid().ToString(),
                        Description = Guid.NewGuid().ToString(),
                        VendorId = vendorInitial.Id,
                        LocationId = location.Id,
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = userId
                    };

                    inventoryTransaction = new InventoryTransaction
                    {
                        Id = Guid.NewGuid(),
                        Code = inventoryTransactionCode,
                        FromLocationId = fromLocationId,
                        ToLocationId = toLocationId,
                        TransactionTypeId = TransactionTypeGIW.Id,
                        TransactionRefId = newGIWDocument.Id,
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = userId,
                    };

                    int indexforSKU = 0;
                    int stockIniQty = stockInitials.Count();

                    var currentTime = datetimeNow.ToString("MMyy", CultureInfo.InvariantCulture);
                    var prefixProductSKUCodes = categories.Select(x => new
                    {
                        CategoryId = x.Id,
                        Value = string.Format("{0}{1}", x.Code, currentTime)
                    });
                    var prefixProductSKUCodeValues = prefixProductSKUCodes.Select(x => x.Value);
                    var productNextSequences = _pimDbContext.NextSequences.Where(x => prefixProductSKUCodeValues.Contains(x.Key)).ToList();

                    var variantNextSequence = await _pimDbContext.NextSequences.FirstOrDefaultAsync(ns => ns.Key == variantKey);
                    if (variantNextSequence == null)
                    {
                        variantNextSequence = new Domain.NextSequence
                        {
                            Key = variantKey,
                            Value = 0.ToString().PadLeft(numberOfCharsVariant, '0')
                        };
                    }

                    foreach (var item in stockInitials)
                    {
                        bool isFirstOrLast = indexforSKU == 0 || indexforSKU == stockIniQty ? true : false;
                        var totalQuantity = item.QuantityCON + item.QuantityFIRM + item.QuantitySOE + item.QuantitySOR;
                        var variant = variants.FirstOrDefault(x => x.Id == item.VariantId);
                        var product = products.FirstOrDefault(x => x.Id == item.ProductId);
                        if (!string.IsNullOrEmpty(item.VariantSKU) || totalQuantity != 0)
                        {
                            if (product != null && string.IsNullOrEmpty(product.SKUCode))
                            {
                                var category = categories.FirstOrDefault(x => x.Id == product.CategoryId);
                                var prefixProductSKU = string.Format("{0}{1}", category.Code, currentTime);

                                var nextSequence = productNextSequences.FirstOrDefault(x => x.Key == prefixProductSKU);
                                var identityNumber = "0";
                                if (nextSequence != null)
                                {
                                    identityNumber = (int.Parse(nextSequence.Value) + 1)
                                                            .ToString().PadLeft(numberOfCharsProduct, '0');
                                    nextSequence.Value = identityNumber;
                                }
                                else
                                {
                                    var newProductNextSequence = new Domain.NextSequence
                                    {
                                        Key = prefixProductSKU,
                                        Value = 1.ToString().PadLeft(numberOfCharsProduct, '0')
                                    };
                                    identityNumber = newProductNextSequence.Value;
                                    productNextSequences.Add(newProductNextSequence);
                                }

                                var sKUCode = $"{prefixProductSKU}{identityNumber}";
                                product.SKUCode = sKUCode;
                                product.UpdatedDate = DateTime.UtcNow;
                                updateProducts.Add(product);
                            }

                            if (variant != null && string.IsNullOrEmpty(variant.SKUCode))
                            {
                                var prefixVariantSKUCode = product.SKUCode;

                                var identityNumber = (int.Parse(variantNextSequence.Value) + 1).ToString().PadLeft(numberOfCharsVariant, '0');
                                variantNextSequence.Value = identityNumber;

                                var sKUCode = await GenerateVariantSKU(prefixVariantSKUCode, identityNumber);
                                variant.SKUCode = sKUCode;
                                variant.UpdatedDate = DateTime.UtcNow;
                                updateVariants.Add(variant);
                            }
                        }
                        if (item.VariantBarCodes.Count > 0)
                        {
                            foreach (var variantBarCode in item.VariantBarCodes)
                            {
                                if (string.IsNullOrEmpty(variantBarCode) == false)
                                {
                                    var barCode = GetLastestBarCode(variantBarCode, barCodes, newBarCodes);
                                    if (barCode != null)
                                    {
                                        if (barCode.VariantId != item.VariantId)
                                        {
                                            throw new ArgumentException($"Product {product.Name} - Variant {item.VariantName} have barCode {variantBarCode} is duplicated. Please try again!");
                                        }
                                    }
                                    else
                                    {
                                        var newBarCode = new BarCode()
                                        {
                                            Id = Guid.NewGuid(),
                                            Code = variantBarCode,
                                            VariantId = item.VariantId,
                                            CreatedBy = userId,
                                            CreatedDate = DateTime.UtcNow
                                        };
                                        newBarCodes.Add(newBarCode);
                                    }
                                }
                            }
                        }

                        if (totalQuantity > 0)
                        {
                            var lastStockTranasction = stockTransactions.FirstOrDefault(x => x.VariantId == item.VariantId);
                            var lastestBalance = lastStockTranasction != null ? lastStockTranasction.Balance : 0;
                            var referenceId = $"{variant.SKUCode}{location.LocationCode}";
                            var newStockTransaction = new StockTransaction()
                            {
                                Id = Guid.NewGuid(),
                                InventoryTransactionId = inventoryTransaction.Id,
                                LocationId = location.Id,
                                VariantId = item.VariantId,
                                StockTypeId = Guid.Empty,
                                TransactionTypeId = TransactionTypeGIW.Id,
                                Quantity = totalQuantity,
                                Balance = lastestBalance + totalQuantity,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = userId,
                                StockTransactionRefId = Guid.Empty,
                                ReferenceId = referenceId
                            };
                            newStockTransactions.Add(newStockTransaction);

                            if (item.QuantityCON != 0)
                            {
                                //var referenceIdWithStockType = $"{variant.SKUCode}{stockType_CON.Code}{vendorInitial.VendorCode}";

                                var newGIWDItem = new GIWDocumentItem()
                                {
                                    Id = Guid.NewGuid(),
                                    GIWDocumentId = newGIWDocument.Id,
                                    VariantId = item.VariantId,
                                    StockTypeId = stockType_CON.Id,
                                    CostValue = item.CostValue,
                                    CurrencyCode = item.CurrencyCode,
                                    Quantity = item.QuantityCON,
                                    RemainingQty = item.QuantityCON,
                                    TransactionRefId = Guid.Empty
                                };
                                newGIWItems.Add(newGIWDItem);
                            }
                            if (item.QuantityFIRM != 0)
                            {
                                //var referenceIdWithStockType = $"{variant.SKUCode}{stockType_FIRM.Code}{vendorInitial.VendorCode}";

                                var newGIWDItem = new GIWDocumentItem()
                                {
                                    Id = Guid.NewGuid(),
                                    GIWDocumentId = newGIWDocument.Id,
                                    VariantId = item.VariantId,
                                    StockTypeId = stockType_FIRM.Id,
                                    CostValue = item.CostValue,
                                    CurrencyCode = item.CurrencyCode,
                                    Quantity = item.QuantityFIRM,
                                    RemainingQty = item.QuantityFIRM,
                                    TransactionRefId = Guid.Empty
                                };
                                newGIWItems.Add(newGIWDItem);
                            }
                            if (item.QuantitySOE != 0)
                            {
                                //var referenceIdWithStockType = $"{variant.SKUCode}{stockType_SOE.Code}{vendorInitial.VendorCode}";

                                var newGIWDItem = new GIWDocumentItem()
                                {
                                    Id = Guid.NewGuid(),
                                    GIWDocumentId = newGIWDocument.Id,
                                    VariantId = item.VariantId,
                                    StockTypeId = stockType_SOE.Id,
                                    CostValue = item.CostValue,
                                    CurrencyCode = item.CurrencyCode,
                                    Quantity = item.QuantitySOE,
                                    RemainingQty = item.QuantitySOE,
                                    TransactionRefId = Guid.Empty
                                };
                                newGIWItems.Add(newGIWDItem);
                            }
                            if (item.QuantitySOR != 0)
                            {
                                //var referenceIdWithStockType = $"{variant.SKUCode}{stockType_SOR.Code}{vendorInitial.VendorCode}";

                                var newGIWDItem = new GIWDocumentItem()
                                {
                                    Id = Guid.NewGuid(),
                                    GIWDocumentId = newGIWDocument.Id,
                                    VariantId = item.VariantId,
                                    StockTypeId = stockType_SOR.Id,
                                    CostValue = item.CostValue,
                                    CurrencyCode = item.CurrencyCode,
                                    Quantity = item.QuantitySOR,
                                    RemainingQty = item.QuantitySOR,
                                    TransactionRefId = Guid.Empty
                                };
                                newGIWItems.Add(newGIWDItem);
                            }
                        }

                        indexforSKU++;
                    }

                    var newProductNextSequences = productNextSequences.Where(x => x.Id == Guid.Empty);
                    var updateProductNextSequences = productNextSequences.Where(x => x.Id != Guid.Empty);
                    AddSKUCodeNextSequences(optionbuilder.Options, newProductNextSequences);
                    UpdateSKUCodeNextSequences(optionbuilder.Options, updateProductNextSequences);

                    if (variantNextSequence.Id == Guid.Empty)
                    {
                        _pimDbContext.NextSequences.Add(variantNextSequence);
                    }
                    else
                    {
                        _pimDbContext.NextSequences.Update(variantNextSequence);
                    }

                    await _transactionDbContext.GIWDocuments.AddAsync(newGIWDocument);
                    await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransaction);
                }
                else
                {
                    var fromlocation = _pimDbContext.Locations.FirstOrDefault(x => x.Id == fromLocationId);

                    var stockTransactions = _transactionDbContext.StockTransactions
                                                        .Where(x => x.LocationId == fromLocationId && _variantIds.Contains(x.VariantId))
                                                        .OrderByDescending(x => x.CreatedDate).AsNoTracking().ToList();

                    var keyName = $"{TransactionTypeCode.TFO.ToString()}{fromlocation?.LocationCode}{DateTime.UtcNow.ToString("yyMM")}";
                    var valueName = _nextSequenceService.Get(keyName);
                    inventoryTransaction = new InventoryTransaction
                    {
                        Id = Guid.NewGuid(),
                        TransactionRefId = Guid.Empty,
                        FromLocationId = fromLocationId,
                        ToLocationId = toLocationId,
                        TransferNumber = $"{keyName}{valueName}",
                        TransactionTypeId = TransactionTypeTFO.Id,
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = userId,
                    };

                    foreach (var item in stockInitials)
                    {
                        var totalQuantity = item.Quantity;
                        var variant = variants.FirstOrDefault(x => x.Id == item.VariantId);
                        var product = products.FirstOrDefault(x => x.Id == item.ProductId);

                        if (totalQuantity > 0)
                        {
                            var lastStockTransaction = stockTransactions.FirstOrDefault(x => x.VariantId == item.VariantId);
                            var lastestBalance = lastStockTransaction != null ? lastStockTransaction.Balance : 0;
                            var referenceId = $"{variant.SKUCode}{fromlocation.LocationCode}";


                            var newStockTransaction = new StockTransaction()
                            {
                                Id = Guid.NewGuid(),
                                InventoryTransactionId = inventoryTransaction.Id,
                                LocationId = fromLocationId,
                                VariantId = item.VariantId,
                                StockTypeId = Guid.Empty,
                                TransactionTypeId = TransactionTypeTFO.Id,
                                Quantity = totalQuantity,
                                Balance = lastestBalance - totalQuantity,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = userId,
                                StockTransactionRefId = Guid.Empty,
                                ReferenceId = referenceId
                            };
                            newStockTransactions.Add(newStockTransaction);
                        }
                    }
                    await _transactionDbContext.GIWDocuments.AddAsync(newGIWDocument);
                    await _transactionDbContext.InventoryTransactions.AddAsync(inventoryTransaction);
                }

                await _transactionDbContext.SaveChangesAsync();

                int i = 0;
                while (0 == 0)
                {
                    var udpateProductsPack = updateProducts.Skip(i * 300).Take(300).ToList();
                    var udpateVariantsPack = updateVariants.Skip(i * 300).Take(300).ToList();
                    var InsertBarcodesPack = newBarCodes.Skip(i * 300).Take(300).ToList();
                    if (udpateProductsPack.Count == 0 && udpateVariantsPack.Count == 0 && InsertBarcodesPack.Count == 0)
                    {
                        break;
                    }
                    using (var dbcontext = new PimDbContext(optionbuilder.Options))
                    {

                        if (udpateProductsPack.Count > 0)
                        {
                            dbcontext.UpdateRange(udpateProductsPack);
                        }
                        if (udpateVariantsPack.Count > 0)
                        {
                            dbcontext.UpdateRange(udpateVariantsPack);
                        }
                        if (InsertBarcodesPack.Count > 0)
                        {
                            dbcontext.AddRange(InsertBarcodesPack);
                        }
                        dbcontext.SaveChanges();
                    }
                    i++;
                }
                var optionTbuilder = new DbContextOptionsBuilder<TransactionDbContext>();
                optionTbuilder.UseNpgsql(_configuration["ConnectionString"]);

                int j = 0;
                while (0 == 0)
                {
                    var newStockTransactionsPack = newStockTransactions.Skip(j * 500).Take(500).ToList();
                    var newGIWItemsPack = newGIWItems.Skip(j * 500).Take(500).ToList();
                    if (newStockTransactionsPack.Count == 0 && newGIWItemsPack.Count == 0)
                    {
                        break;
                    }
                    using (var dbcontext = new TransactionDbContext(optionTbuilder.Options))
                    {
                        if (newStockTransactionsPack.Count > 0)
                        {
                            dbcontext.AddRange(newStockTransactionsPack);
                        }
                        if (newGIWItemsPack.Count > 0)
                        {
                            dbcontext.AddRange(newGIWItemsPack);
                        }
                        dbcontext.SaveChanges();
                    }
                    j++;
                }
                await Task.Delay(5000);

                if (location.Type != LocationType.Warehouse)
                {
                    MassTransferIn(inventoryTransaction, variants, location, stockInitials, fromLocationId, toLocationId, userId);
                }

                await _eventBus.PublishAsync(new StockLevelUpdatedEvent()
                {
                    LocationId = location.Id
                });

                var stockAndPrices = newStockTransactions.Select(x => new StockAndPriceLevelModel()
                {
                    LocationId = x.LocationId,
                    ProductId = variants.SingleOrDefault(y => y.Id == x.VariantId).ProductId,
                    VariantId = x.VariantId,
                    SKUCode = variants.SingleOrDefault(y => y.Id == x.VariantId).SKUCode,
                }).Cast<dynamic>().ToList();
                await _eventBus.PublishAsync(new StockAndPriceLevelUpdatedEvent()
                {
                    stockAndPrices = stockAndPrices
                });
            }
            return true;
        }

        private void AddSKUCodeNextSequences(DbContextOptions<PimDbContext> catalogOption, IEnumerable<Harvey.Domain.NextSequence> data)
        {
            int index = 0;
            while (true)
            {
                var currentData = data.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PimDbContext(catalogOption))
                {
                    dbContext.AddRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }

        private void UpdateSKUCodeNextSequences(DbContextOptions<PimDbContext> catalogOption, IEnumerable<Harvey.Domain.NextSequence> data)
        {
            int index = 0;
            while (true)
            {
                var currentData = data.Skip(index * 500).Take(500);
                if (currentData == null || !currentData.Any())
                {
                    break;
                }

                using (var dbContext = new PimDbContext(catalogOption))
                {
                    dbContext.UpdateRange(currentData);
                    dbContext.SaveChanges();
                }
                index++;
            }
        }

        private bool MassTransferIn(InventoryTransaction transferOut, List<Variant> variants, Location location, List<StockTransactionCsvModel> stockInitials, Guid fromLocationId, Guid toLocationId, Guid userId)
        {
            Channel channel = _pimDbContext.Channels.FirstOrDefault(a => a.Id == a.ChannelStoreAssignments.FirstOrDefault(b => b.StoreId == location.Id).ChannelId);

            var optioncatalogTbuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optioncatalogTbuilder.UseNpgsql(channel.ServerInformation);

            var variantsId = variants.Select(a => a.Id).ToList();
            var TransactionTypeTFI = _transactionDbContext.TransactionTypes.FirstOrDefault(x => x.Code == TransactionTypeCode.TFI.ToString());

            var stockTransactions = stockInitials.Count() > 20000 ? _transactionDbContext.StockTransactions.Where(x => x.LocationId == location.Id).OrderByDescending(x => x.CreatedDate).AsNoTracking().ToList()
                                                                   : _transactionDbContext.StockTransactions.Where(x => x.LocationId == location.Id && variantsId.Contains(x.VariantId)).OrderByDescending(x => x.CreatedDate).AsNoTracking().ToList();

            var catalogTransactions = new List<CatalogStockTransaction>();
            using (var dbcontext = new CatalogDbContext(optioncatalogTbuilder.Options))
            {
                catalogTransactions = stockInitials.Count() > 20000 ? dbcontext.StockTransactions.Where(a => a.LocationId == location.Id).OrderByDescending(a => a.CreatedDate).AsNoTracking().ToList()
                                                                    : dbcontext.StockTransactions.Where(a => a.LocationId == location.Id && variantsId.Contains(a.VariantId)).OrderByDescending(a => a.CreatedDate).AsNoTracking().ToList();
            }

            var newStockTransactions = new List<StockTransaction>();
            var catalogStockTransactions = new List<CatalogStockTransaction>();


            var outlet = location;
            var keyName = $"{TransactionTypeCode.TFI.ToString()}{outlet.LocationCode}{DateTime.UtcNow.ToString("yyMM")}";
            var valueName = _nextSequenceService.Get(keyName);
            var inventoryTransaction = new InventoryTransaction
            {
                Id = Guid.NewGuid(),
                TransactionRefId = transferOut.Id,
                FromLocationId = fromLocationId,
                ToLocationId = toLocationId,
                TransactionTypeId = TransactionTypeTFI.Id,
                TransferNumber = $"{keyName}{valueName}",
                CreatedDate = DateTime.UtcNow,
                CreatedBy = userId,
                UpdatedDate = DateTime.UtcNow,
                UpdatedBy = userId
            };
            _transactionDbContext.Add(inventoryTransaction);
            _transactionDbContext.SaveChanges();

            foreach (var item in stockInitials)
            {
                var totalQuantity = item.Quantity;
                var variant = variants.FirstOrDefault(x => x.Id == item.VariantId);

                if (totalQuantity > 0)
                {
                    var lastStockTransaction = stockTransactions.FirstOrDefault(x => x.VariantId == item.VariantId);
                    var lastestBalance = lastStockTransaction != null ? lastStockTransaction.Balance : 0;
                    var referenceId = $"{variant.SKUCode}{location.LocationCode}";


                    var newStockTransaction = new StockTransaction()
                    {
                        Id = Guid.NewGuid(),
                        InventoryTransactionId = inventoryTransaction.Id,
                        LocationId = toLocationId,
                        VariantId = item.VariantId,
                        StockTypeId = Guid.Empty,
                        TransactionTypeId = TransactionTypeTFI.Id,
                        Quantity = totalQuantity,
                        Balance = lastestBalance + totalQuantity,
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = userId,
                        StockTransactionRefId = Guid.Empty,
                        ReferenceId = referenceId
                    };
                    newStockTransactions.Add(newStockTransaction);

                    var chanelLastestBallance = catalogTransactions.FirstOrDefault(a => a.VariantId == item.VariantId)?.Balance ?? 0;
                    var catalogStockTransaction = new CatalogStockTransaction()
                    {
                        Id = Guid.NewGuid(),
                        LocationId = toLocationId,
                        VariantId = item.VariantId,
                        StockTypeId = Guid.Empty,
                        TransactionTypeId = TransactionTypeTFI.Id,
                        Quantity = totalQuantity,
                        Balance = chanelLastestBallance + totalQuantity,
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = userId,
                        StockTransactionRefId = Guid.Empty,
                    };
                    catalogStockTransactions.Add(catalogStockTransaction);

                }
            }


            var optionTbuilder = new DbContextOptionsBuilder<TransactionDbContext>();
            optionTbuilder.UseNpgsql(_configuration["ConnectionString"]);


            int j = 0;
            while (0 == 0)
            {
                var newStockTransactionsPack = newStockTransactions.Skip(j * 500).Take(500).ToList();
                if (newStockTransactionsPack.Count == 0)
                {
                    break;
                }
                using (var dbcontext = new TransactionDbContext(optionTbuilder.Options))
                {
                    dbcontext.AddRange(newStockTransactionsPack);
                    dbcontext.SaveChanges();
                }
                j++;
            }



            int c = 0;
            while (0 == 0)
            {
                var catalogStockTransactionPack = catalogStockTransactions.Skip(c * 500).Take(500).ToList();
                if (catalogStockTransactionPack.Count == 0)
                {
                    break;
                }
                using (var dbcontext = new CatalogDbContext(optioncatalogTbuilder.Options))
                {
                    dbcontext.AddRange(catalogStockTransactionPack);
                    dbcontext.SaveChanges();
                }
                c++;
            }

            return true;
        }
        private BarCode GetLastestBarCode(string barCode, List<BarCode> dbBarCodes, List<BarCode> newBarCodes)
        {
            var dbBarCode = dbBarCodes.FirstOrDefault(x => x.Code == barCode);
            var newBarCode = newBarCodes.FirstOrDefault(x => x.Code == barCode);
            if (dbBarCode != null)
            {
                return dbBarCode;
            }
            if (newBarCode != null)
            {
                return newBarCode;
            }
            return null;
        }

        private List<Domain.NextSequence> GetListNextSequence()
        {
            var nextSequences = _nextSequenceService.GetAllCurrentNextSequence();
            return nextSequences;
        }


        public async Task<string> GenerateVariantSKU(string productSKU, string identityNumber)
        {
            var skuCode = string.Empty;
            while (string.IsNullOrEmpty(skuCode))
            {
                var hashCode = identityNumber.GetHashCode();
                string formatHashcode = string.Format("{0,10:X}", hashCode).Trim().Substring(4);
                skuCode = string.Format("{0}{1}", productSKU, formatHashcode);
            }
            return skuCode;
        }

        private string GetIdentityNumber(List<Domain.NextSequence> nextSequences, string key, int numberOfChars)
        {
            var identityNumber = string.Empty;
            var item = nextSequences.FirstOrDefault(s => s.Key != null && s.Key.Trim().Equals(key.Trim()));
            if (item == null)
            {
                var entity = _nextSequenceService.AddNextSequence(key, numberOfChars);
                nextSequences.Add(entity);
                identityNumber = entity.Value;
            }
            else
            {
                item.Value = (Int32.Parse(item.Value) + 1).ToString().PadLeft(numberOfChars, '0');
                identityNumber = item.Value;
            }
            return identityNumber;
        }
    }

    public class DeploymentStockTransactionResult
    {
        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; }
        public static DeploymentStockTransactionResult Success() => new DeploymentStockTransactionResult() { IsSuccess = true };
        public static DeploymentStockTransactionResult Fail(string message) => new DeploymentStockTransactionResult() { IsSuccess = false, ErrorMessage = message };
    }
}
