﻿using Harvey.Domain;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.FieldFramework.Services.Interface;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Harvey.PIM.Application.Infrastructure.Models.ProductModel;

namespace Harvey.PIM.Application.Services
{
    public class ProductService
    {
        private readonly TransientPimDbContext _pimDbContext;
        private readonly IEventStoreRepository<Product> _repository;
        private readonly IEntityRefService _entityRefService;
        private readonly INextSequenceService<TransientPimDbContext> _nextSequenceService;
        public ProductService(TransientPimDbContext pimDbContext,
            IEventStoreRepository<Product> repository,
            IEntityRefService entityRefService,
            INextSequenceService<TransientPimDbContext> nextSequenceService)
        {
            _pimDbContext = pimDbContext;
            _repository = repository;
            _entityRefService = entityRefService;
            _nextSequenceService = nextSequenceService;
        }

        public async Task<ProductModel> Get(Guid productId)
        {
            var product = await _repository.GetByIdAsync(productId);
            var fieldTemplate = _pimDbContext.FieldTemplates.FirstOrDefault(x => x.Id == product.FieldTemplateId);
            
            var result = new ProductModel
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                FieldTemplateId = product.FieldTemplateId,
                FieldTemplateName = fieldTemplate?.Name,
                CategoryId = product.CategoryId,
                IsDelete = product.IsDelete,
                CreatedDate = product.CreatedDate,
                UpdatedDate = product.UpdatedDate
            };


            var fields = await _pimDbContext
               .Field_FieldTemplates
               .Include(x => x.Field)
               .Where(x => x.FieldTemplateId == product.FieldTemplateId).ToListAsync();

            foreach (var item in product.Sections.Keys)
            {
                if (!item.IsVariantField)
                {
                    var section = new Section
                    {
                        Name = item.Name,
                        OrderSection = item.OrderSection,
                        IsVariantField = item.IsVariantField
                    };
                    foreach (var value in product.Sections[item])
                    {
                        var field = fields.FirstOrDefault(x => x.FieldId == value.Field.Id);
                        if (field != null)
                        {
                            value.Field.DefaultValue = field.Field?.DefaultValue;
                            section.FieldValues.Add(FieldValueFactory.GetFromFieldValue(value, _entityRefService));
                        }
                    }
                    result.Sections.Add(section);
                }
            }

            foreach (var item in product.VariantFieldValues)
            {
                var variant = new VariantModel()
                {
                    Id = item.Key,

                };

                if (product.VariantPrices.ContainsKey(item.Key))
                {
                    variant.Price = product.VariantPrices.FirstOrDefault(x => x.Key == item.Key).Value;
                }

                foreach (var field in item.Value)
                {
                    var dbField = fields.FirstOrDefault(x => x.FieldId == field.Field.Id);
                    if (dbField != null)
                    {
                        field.Field.DefaultValue = dbField.Field?.DefaultValue;
                        variant.FieldValues.Add(FieldValueFactory.GetFromFieldValue(field, _entityRefService));
                    }
                }

                result.Variants.Add(variant);
            }

            return result;
        }

        public async Task<List<ProductModel>> Get(List<Guid> productIds)
        {
            var result = new List<ProductModel>();

            foreach(var item in productIds)
            {
                result.Add(await Get(item));
            }

            return result;
        }

        public async Task<string> GenerateSKU(string categoryCode, int numberOfChars)
        {
            DateTime dateTimeNow = DateTime.UtcNow;
            var currentTime = dateTimeNow.ToString("MMyy", System.Globalization.CultureInfo.InvariantCulture);
            var productSKU = string.Format("{0}{1}", categoryCode, currentTime);
            var indetityNumber = _nextSequenceService.Get(productSKU, numberOfChars);
            productSKU = string.Format("{0}{1}", productSKU, indetityNumber);
            return productSKU;
        }
    }
}
