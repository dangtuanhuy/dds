﻿using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.Search.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.PIM.Application.Services
{
    public sealed class SalePerformanceRequest
    {
        public List<OrderItemModel> OrderItems { get; set; }
        public Guid LocationId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public SalePerformanceRequest()
        {

        }
    }

    public sealed class SalePerformanceRespone
    {
        public List<SalePerformanceResponeItem> Data { get; set; }
    }

    public sealed class SalePerformanceResponeItem
    {
        public Guid VariantId { get; set; }
        public string InventoryCode { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
        public string Category { get; set; }
        public string Brand { get; set; }
        public string QtySold { get; set; }
        public string HQStock { get; set; }
        public string TotalQOH { get; set; }
        public string ActualSales { get; set; }
        public string CostOfSales { get; set; }
        public string GP { get; set; }
        public string AmountVariant { get; set; }
        public Guid ProductId { get; set; }
        public Guid CategoryId { get; set; }
    }

    public class SalePerformanceService
    {
        private readonly TransientTransactionDbContext _transientTransactionDbContext;
        private readonly TransientPimDbContext _transientPimDbContext;
        private readonly PimDbContext _pimDbContext;
        private readonly ISearchService _searchService;
        public SalePerformanceService(TransientTransactionDbContext transientTransactionDbContext,
                                      TransientPimDbContext transientPimDbContext,
                                      PimDbContext pimDbContext,
                                      ISearchService searchService)
        {
            _transientTransactionDbContext = transientTransactionDbContext;
            _transientPimDbContext = transientPimDbContext;
            _pimDbContext = pimDbContext;
            _searchService = searchService;
        }

        public async Task<byte[]> ExecuteAsync(SalePerformanceRequest request)
        {
            var locationId = request.LocationId;
            var columnHeaders = new string[]
            {
                "InventoryCode",
                "Description" ,
                "Department ",
                "Store Name",
                "Category",
                "Brand",
                "QtySold",
                "HQStock",
                "TotalQOH",
                "ActualSales",
                "CostOfSales",
                "GP"
             };
            
            var salePerformanceItems = TranformData(request.LocationId, request.OrderItems);
            
            if (salePerformanceItems.Count() == 0)
            {
                return Encoding.UTF8.GetPreamble().Concat(Encoding.UTF8.GetBytes($"{string.Join(",", columnHeaders)}")).ToArray(); ;
            }
            var variantIds = salePerformanceItems.Select(v => v.VariantId);
            var saleItems = salePerformanceItems.Select(v => new SalePerformanceResponeItem
            {
                VariantId = v.VariantId,
                ActualSales = ConvertToDecimal(v.Amount),
                Description = v.Description,
                QtySold = v.Quantity.ToString(),
                CostOfSales = ConvertToDecimal(v.CostValue),
            }).ToList();

            var locationQuery = _transientPimDbContext.Locations.AsQueryable();
            
            var data = (from v in _transientPimDbContext.Variants.Where(v => variantIds.Contains(v.Id))
                        join p in _transientPimDbContext.Products.Include(x => x.Category)
                        on v.ProductId equals p.Id
                        select new SalePerformanceResponeItem
                        {
                            VariantId = v.Id,
                            InventoryCode = v.SKUCode,
                            Category = p.Category.Name,
                            ProductId = v.ProductId
                        }).ToList();
            
            var listProductId = data.Select(x => x.ProductId);
            
            var queryStockTransactions = await (from s in _transientTransactionDbContext.StockTransactions
                                                     where variantIds.Contains(s.VariantId)
                                                group s by new { s.VariantId, s.LocationId } into gs
                                                let fdata = gs.OrderByDescending(x => x.CreatedDate).FirstOrDefault()
                                                select new
                                                {
                                                    variantId = gs.Key.VariantId,
                                                    locationId = fdata.LocationId,
                                                    latestBalance = fdata.Balance
                                                }).ToArrayAsync();
            
            var hqId = locationQuery.Where(l => l.Type.HasFlag(LocationType.Warehouse)).Select(l => l.Id).ToList();
            var countingHQStock = queryStockTransactions.Where(x => hqId.Contains(x.locationId))
                                                        .GroupBy(x => x.variantId)
                                                        .Select(x => new { variantId = x.Key, totalBalance = x.Sum(y => y.latestBalance) });

            var countingTotalQOH = queryStockTransactions.GroupBy(x => x.variantId)
                                                         .Select(x => new { variantId = x.Key, totalBalance = x.Sum(y => y.latestBalance) });

            var assortmentQuery = _pimDbContext.Assortments.AsNoTracking();

            var assortmentsByProduct = _pimDbContext.AssortmentAssignments.Where(x => listProductId.Contains(x.ReferenceId) && x.EntityType == AssortmentAssignmentType.Product)
                                                                .Join(assortmentQuery, a => a.AssortmentId, b => b.Id,
                                                                (a, b) => new { ProductId = a.ReferenceId, AssortmentName = b.Name }).ToList();

            var categorys = _pimDbContext.Products.Where(x => listProductId.Contains(x.Id)).Select(x => new { ProId = x.Id, CateId = x.CategoryId }).ToList();

            var assormentByCategory = _pimDbContext.AssortmentAssignments.Where(x => categorys.Any(y => y.CateId == x.ReferenceId) && x.EntityType == AssortmentAssignmentType.Category)
                                                                .Join(assortmentQuery, a => a.AssortmentId, b => b.Id,
                                                                (a, b) => new { CategoryId = a.ReferenceId, AssortmentName = b.Name }).ToList();

            var assormentProduct = categorys.Join(assormentByCategory, a => a.CateId, b => b.CategoryId, (a, b) => new { ProductId = a.ProId, AssortmentName = b.AssortmentName });

            var assortments = assortmentsByProduct.Concat(assormentProduct).Distinct().GroupBy(x => x.ProductId);

            var productBrands = _pimDbContext.FieldValues.Where(x => listProductId.Contains(x.EntityId) && x.EntityReferenceValue != null)
                                                         .Select(x => new { productId = x.EntityId, brand = x.EntityReferenceValue });


            foreach (var item in data)
            {
                item.HQStock = countingHQStock.FirstOrDefault(x => x.variantId == item.VariantId)?.totalBalance.ToString();
                item.TotalQOH = countingTotalQOH.FirstOrDefault(x => x.variantId == item.VariantId)?.totalBalance.ToString();

                var assortment =  assortments.FirstOrDefault(x => x.Key == item.ProductId);
                item.Department = string.Join("; ", assortment.Select(x => x.AssortmentName).ToArray());
                item.Brand = productBrands.FirstOrDefault(x => x.productId == item.ProductId)?.brand;
                var saleItem = saleItems.FirstOrDefault(x => x.VariantId == item.VariantId);
                item.ActualSales = saleItem.ActualSales;
                item.Description = saleItem.Description;
                item.QtySold = saleItem.QtySold;
                item.CostOfSales = saleItem.CostOfSales;
                item.GP = ((float.Parse(
                                item.ActualSales != null 
                                    ? item.ActualSales 
                                    : 0.ToString(), CultureInfo.InvariantCulture.NumberFormat) 
                            - float.Parse(item.CostOfSales != null 
                                            ? item.CostOfSales 
                                            : 0.ToString(), CultureInfo.InvariantCulture.NumberFormat)) 
                          / float.Parse(item.ActualSales, CultureInfo.InvariantCulture.NumberFormat)).ToString();
            }
            
            var locationName = (await locationQuery.FirstOrDefaultAsync(x => x.Id == locationId))?.Name ?? "";

            var res = new List<object[]>();

            var stringBuilder = new StringBuilder();
            foreach (var item in data)
            {
                res.Add(new object[] {
                            item.InventoryCode,
                            item.Description,
                            item.Department,
                            locationName,
                            item.Category,
                            item.Brand,
                            item.QtySold,
                            item.HQStock,
                            item.TotalQOH,
                            item.ActualSales,
                            item.CostOfSales,
                            item.GP
                        });
            }

            res.ForEach(line =>
            {
                stringBuilder.AppendLine(string.Join(",", line));
            });

            byte[] buffer = Encoding.UTF8.GetPreamble().Concat(Encoding.UTF8.GetBytes($"{string.Join(",", columnHeaders)}\r\n{stringBuilder.ToString()}")).ToArray();
            return buffer;
        }

        private List<SalePerformanceItem> TranformData(Guid locationId, List<OrderItemModel> orderItems)
        {
            var salePerformanceItems = new List<SalePerformanceItem>();

            if (orderItems.Any())
            {
                foreach (var orderItem in orderItems)
                {
                    var salePerformanceItem = salePerformanceItems.Find(s => s.VariantId == orderItem.VariantId);
                    if (salePerformanceItem == null)
                    {
                        salePerformanceItems.Add(new SalePerformanceItem()
                        {
                            VariantId = orderItem.VariantId,
                            Amount = orderItem.Price * orderItem.Quantity,
                            Quantity = orderItem.Quantity,
                            LocationId = locationId,
                            Description = orderItem.VariantName,
                            CostValue = orderItem.CostValue
                        });
                    }
                    else
                    {
                        salePerformanceItem.Amount += (orderItem.Price * orderItem.Quantity);
                        salePerformanceItem.Quantity += orderItem.Quantity;
                        salePerformanceItem.CostValue += orderItem.CostValue;
                    }
                }
            }
            return salePerformanceItems;
        }

        private string ConvertToDecimal(object value)
        {
            return String.Format("{0:0.00}", value);
        }
    }
}
