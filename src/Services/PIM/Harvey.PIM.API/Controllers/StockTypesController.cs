﻿using Harvey.Persitance.EF;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Authorize (Policy = "InventoryTransfer")]
    [Route("api/{stocktypes}")]
    public class StockTypesController : ControllerBase
    {
        private readonly IEfRepository<TransactionDbContext, StockType> _efRepository;
        public StockTypesController(IEfRepository<TransactionDbContext, StockType> efRepository)
        {
            _efRepository = efRepository;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StockType>>> GetAll()
        {
            var result = await _efRepository.GetAsync();
            result = result.Where(x => x.Code != "STI");
            return Ok(result);
        }
    }
}
