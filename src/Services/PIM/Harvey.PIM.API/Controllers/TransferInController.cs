﻿using Harvey.Domain;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.Application.Infrastructure.Commands.TransferIn;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.TransferIn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/transferin")]
    [Authorize (Policy = "InventoryTransfer")]
    public class TransferInController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        public TransferInController(
           ICommandExecutor commandExecutor,
            IQueryExecutor queryExecutor)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<InventoryTransactionTransferViewModel>>> Get(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLocationIdString, string fromLocationIdString, string QueryString)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetTransferInsQuery(pagingFilterCriteria, statusIdString, toLocationIdString, fromLocationIdString, QueryString));
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<InventoryTransactionTransferViewModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetTransferInByIdQuery(id));
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] TransferInModel transferInModel)
        {
            var userId = User.GetUserId();

            var result = await _commandExecutor.ExecuteAsync(new AddTransferInCommand(User.GetUserId(),
                                                                                       transferInModel.InventoryTransactionTransferInsModel));

            if (result == true)
            {
                return Ok();
            }
            else
            {
                return BadRequest("Can't find Transaction Type TFI");
            }
        }
    }
}


