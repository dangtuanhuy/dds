﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.PIM.Application.FieldFramework;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.PIM.Application.Infrastructure.Queries.Products;
using Harvey.Search;
using Harvey.Search.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/search")]
    [Authorize(Policy = "RequireAdministratorRole")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly PimDbContext _pimDbContext;
        private readonly ISearchService _searchService;
        private readonly TransactionDbContext _transactionDbContext;
        public SearchController(IQueryExecutor queryExecutor,
            PimDbContext pimDbContext,
            ISearchService searchService,
            TransactionDbContext transactionDbContext)
        {
            _queryExecutor = queryExecutor;
            _pimDbContext = pimDbContext;
            _searchService = searchService;
            _transactionDbContext = transactionDbContext;
        }

        [HttpPost]
        [Route("indexes/{indexType}/rebuild-index")]
        public async Task<ActionResult> RebuildIndex(IndexType indexType)
        {
            switch (indexType)
            {
                case IndexType.Product:
                    var indexProduct = await RebuildIndexProduct();
                    return Ok(indexProduct);
                case IndexType.StockLevel:
                    var indexStockLevel = await RebuildIndexStockLevel();
                    return Ok(indexStockLevel);
                default:
                    return Ok();
            }
        }

        private async Task<bool> RebuildIndexStockLevel()
        {
            var stockLevelIndex = "stock_index";
            await _searchService.DeleteByQueryAsync<StockLevelSearchItem>(stockLevelIndex);
            
            var locations = _pimDbContext.Locations.ToList();

            var stockLevels = (from st in _transactionDbContext.StockTransactions
                               group st by st.ReferenceId into gst
                               let lastData = gst.OrderByDescending(x => x.CreatedDate).FirstOrDefault()
                               select new StockLevelSearchItem
                               {
                                   Id = lastData.ReferenceId,
                                   ReferenceId = lastData.ReferenceId,
                                   LocationId = lastData.LocationId,
                                   VariantId = lastData.VariantId,
                                   Balance = lastData.Balance
                               }).ToList();

            var _variantIds = stockLevels.Select(x => x.VariantId).Distinct();
            var variants = _pimDbContext.Variants.AsNoTracking().Where(x => _variantIds.Contains(x.Id))
                            .Select(x => new
                            {
                                x.ProductId,
                                x.Id
                            }).ToList();


            bool isContinueInsertElastissearch = false;
            foreach (var location in locations)
            {
                var itemOfLocation = stockLevels.Where(s => s.LocationId == location.Id).ToList();
                if (itemOfLocation.Any())
                {
                    var stockLevelsSearchIndexItems = new List<IndexedItem<StockLevelSearchItem>>();
                    for (var i = 0; i < itemOfLocation.Count(); i++)
                    {
                        var item = itemOfLocation[i];
                        item.ProductId = variants.SingleOrDefault(x => x.Id == item.VariantId).ProductId;
                        stockLevelsSearchIndexItems.Add(new StockLevelIndexedItem(itemOfLocation[i]));
                    }

                    var dataSize = 1000;
                    var page = 1;

                    while (true)
                    {
                        var skip = dataSize * (page - 1);
                        if (skip >= itemOfLocation.Count())
                        {                            
                            break;
                        }
                        var packageStockLevelsSearchIndexItems = stockLevelsSearchIndexItems.Skip(skip).Take(dataSize).ToList();
                        isContinueInsertElastissearch = await _searchService.InsertDocumentsWithoutDeleteIndexAsync(packageStockLevelsSearchIndexItems);
                        
                        if (!isContinueInsertElastissearch)
                        {
                            return isContinueInsertElastissearch;
                        }
                        page++;
                    }                 
                }                
            }           
            return isContinueInsertElastissearch;
        }

        private async Task<bool> RebuildIndexProduct()
        {
            await _searchService.DeleteByQueryAsync<ProductSearchItem>(Product.IndexName);

            int pageIndex = 0;
            int pageSize = 2000;
            bool haveError = false;
            while (!haveError)
            {
                try
                {
                    var products = await _queryExecutor.ExecuteAsync(new GetProductListPagingForRebuildIndexQuery(pageIndex, pageSize));
                    if (!products.Any())
                    {
                        break;
                    }

                    var productIds = products.Select(x => x.Id).ToList();
                    var variantIds = _pimDbContext.Variants.Where(x => productIds.Contains(x.ProductId.ToString()))
                        .Select(x => new { x.Id, x.ProductId }).ToList();
                    var fieldValues = _pimDbContext
                        .FieldValues
                        .Where(x => productIds.Contains(x.EntityId.ToString()) || variantIds.Select(v => v.Id).Contains(x.EntityId))
                        .Include(x => x.Field)
                        .Select(x => new
                        {
                            fieldValue = x,
                            productId = productIds.Contains(x.EntityId.ToString()) ? x.EntityId : variantIds.FirstOrDefault(y => y.Id == x.EntityId).ProductId
                        }).ToList();

                    var productSearchIndexedItems = new List<IndexedItem<ProductSearchItem>>();

                    foreach (var item in products)
                    {
                        var searchItem = new ProductSearchItem(item.Id.ToString())
                        {
                            Name = item.Name,
                            ChineseSearchName = item.Name,
                            Description = item.Description,
                            ChineseSearchDescription = item.Description,
                            CategoryId = item.CategoryId,
                            CategoryName = item.CategoryName,
                            Code = item.Code,
                            IsDelete = item.IsDelete,
                            CreatedDate = item.CreatedDate,
                            CreatedBy = item.CreatedBy,
                            UpdatedDate = item.UpdatedDate,
                            UpdatedBy = item.UpdatedBy
                        };

                        var productVariantIds = variantIds.Where(x => x.ProductId.ToString() == item.Id).Select(x => x.Id);
                        var indexingValue = fieldValues
                            .Where(x => x.productId.ToString() == item.Id)
                            .Select(x => FieldValueFactory.GetFieldValueFromFieldType(x.fieldValue.Field.Type, x.fieldValue)).ToList();

                        indexingValue.Add(item.CategoryName);
                        indexingValue.Add(item.Code);
                        searchItem.IndexingValue = string.Join(",", indexingValue);
                        searchItem.ChineseSearchIndexingValue = searchItem.IndexingValue;

                        productSearchIndexedItems.Add(new ProductSearchIndexedItem(searchItem));

                    }
                    var res = await _searchService.InsertDocumentsWithoutDeleteIndexAsync(productSearchIndexedItems);
                    if (res)
                    {
                        pageIndex++;
                    }
                    else
                    {
                        haveError = true;
                    }
                }
                catch (System.Exception)
                {
                    haveError = true;
                }
            }

            return haveError ? false : true;
        }
    }
}