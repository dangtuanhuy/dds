﻿using Harvey.Domain;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.StockAndPrice;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/stock-and-price")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class StockAndPriceController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        public StockAndPriceController(
            IQueryExecutor queryExecutor
            )
        {
            _queryExecutor = queryExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<StockAndPriceProductModel>>> Get(PagingFilterCriteria pagingFilterCriteria, string QueryText)
        {
            return await _queryExecutor.ExecuteAsync(new GetProductsPageStockAndPriceQuery(pagingFilterCriteria, QueryText));
        }

        [HttpGet("getByProduct")]
        public async Task<ActionResult<StockAndPriceItemModel>> GetByProduct(Guid productId, string queryText)
        {
            return await _queryExecutor.ExecuteAsync(new GetStockAndPriceStoreByProductQuery(productId, queryText));
        } 

    }
}
