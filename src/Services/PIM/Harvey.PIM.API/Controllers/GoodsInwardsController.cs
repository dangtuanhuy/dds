﻿using Harvey.Domain;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Commands.Allocations;
using Harvey.PIM.Application.Infrastructure.Commands.GIWs;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.GIWs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/goods-inwards")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class GoodsInwardsController : ControllerBase
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
        public GoodsInwardsController(
            ICommandExecutor commandExecutor,
            IQueryExecutor queryExecutor
            )
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpPost]
        [Authorize(Policy = "InventoryStock")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<Guid>> Add([FromBody]GIWDModel giwdModel)
        {
            var result = await _commandExecutor.ExecuteAsync(new AddGIWCommand(giwdModel, User.GetUserId()));
            return Ok(result);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, WarehouseStaff, InventoryManager")]
        public async Task<PagedResult<InventoryTransactionTransferViewModel>> Get(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLocationIdString, string fromLocationIdString, string QueryString)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetGIWsQuery(pagingFilterCriteria, statusIdString, toLocationIdString, fromLocationIdString, QueryString));
            return result;
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        [Route("barcodes")]
        public async Task<List<BarCode>> GenerateBarCode([FromBody]GenerateBarCodeRequestModel request)
        {
            var result = await _commandExecutor.ExecuteAsync(new GenerateBarCodeCommand(request, User.GetUserId()));
            return result;
        }

        [HttpPut]
        [ServiceFilter(typeof(EfUnitOfWork))]

        public async Task<ActionResult<Guid>> UpdateStatus([FromBody]UpdateGIWStatusModel command)
        {
            var result = await _commandExecutor.ExecuteAsync(new UpdateGIWStatusCommand(command.Id, command.Status, User.GetUserId()));
            return Ok(result);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, WarehouseStaff, InventoryManager")]
        [Route("{inventoryTransactionId}")]
        public async Task<InventoryTransactionTransferViewModel> GetDetailsGIWByInventoryTransactionId(Guid inventoryTransactionId)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetDetailsGIWByInventoryTransactionIdQuery(inventoryTransactionId));
            return result;
        }
    }
}
