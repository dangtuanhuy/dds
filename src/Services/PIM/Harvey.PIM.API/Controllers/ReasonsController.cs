﻿using Harvey.Domain;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Commands.Reasons;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.Reasons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/reasons")]
    [Authorize (Roles = "Administrator, AdminStaff, InventoryManager")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class ReasonsController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;

        public ReasonsController(IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<ReasonModel>>> Get(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetReasonsQuery(pagingFilterCriteria, queryText));
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetReasonByIdQuery(id));
            return Ok(result);
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<ReasonModel>> Add([FromBody] ReasonModel reason)
        {
            if (string.IsNullOrEmpty(reason.Name))
            {
                return BadRequest("Name is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new AddReasonCommand(User.GetUserId(), reason.Name, reason.Description,reason.Code));
            if (result != null && result.Id != Guid.Empty)
            {
                return CreatedAtAction("Add", result);
            }
            else
            {
                throw new InvalidOperationException("Cannot add reason. Please try again.");
            }
        }

        [HttpPut]
        [Route("{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Update(Guid id, [FromBody] ReasonModel reason)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new UpdateReasonCommand(User.GetUserId(), reason.Id, reason.Name, reason.Description,reason.Code));
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot update reason. Please try again.");
            }
        }

        [HttpDelete("{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required");
            }
            await _commandExecutor.ExecuteAsync(new DeleteReasonCommand(id));
            return Ok();
        }
    }
}
