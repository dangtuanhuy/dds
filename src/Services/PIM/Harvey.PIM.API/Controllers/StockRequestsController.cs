﻿using Harvey.Domain;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Commands.StockRequests;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.StockRequests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Authorize (Roles = "AdminStaff, Administrator, WarehouseStaff")]
    [Route("api/stockrequest")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class StockRequestsController : ControllerBase
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
        public StockRequestsController(ICommandExecutor commandExecutor,
            IQueryExecutor queryExecutor)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<StockRequestListModel>> Add([FromBody] StockRequestModel stockRequest)
        {
            if (Guid.Empty == stockRequest.ToLocationId || Guid.Empty == stockRequest.FromLocationId)
            {
                return BadRequest("Location is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(
                new AddStockRequestCommand(
                    stockRequest.FromLocationId,
                    stockRequest.ToLocationId,
                    stockRequest.Subject,
                    stockRequest.Description,
                    User.GetUserId(),
                    stockRequest.DateRequest,
                    stockRequest.StockRequestItems
                ));
            if (result != null && result.Id != Guid.Empty)
            {
                return CreatedAtAction("Add", result);
            }
            else
            {
                throw new InvalidOperationException("Cannot add stock request. Please try again.");
            }
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<StockRequestListModel>>> Get(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLocationIdString, string fromLocationIdString, string QueryString)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetStockRequestListQuery(pagingFilterCriteria, statusIdString, toLocationIdString, fromLocationIdString, QueryString));
            return Ok(result);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<StockRequestModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetStockRequestByIdQuery(id));
            return result;
        }

        [HttpPut]
        [ServiceFilter(typeof(EfUnitOfWork))]
        [Route("{id}")]
        public async Task<ActionResult> Update(Guid id,[FromBody]StockRequestUpdateModel request)
        {
            if (request.Id == Guid.Empty)
            {
                return BadRequest("Id is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new UpdateStockRequestCommand(request.Id, 
                                                                                           request.ToLocationId, 
                                                                                           request.Subject, 
                                                                                           request.Description,
                                                                                           request.StockRequestItems, 
                                                                                           request.StockRequestStatus,
                                                                                           User.GetUserId(), 
                                                                                           request.DateRequest,
                                                                                           request.IsDeleteRequest));
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot not update stock request. Please try again.");
            }
        }

    }
}
