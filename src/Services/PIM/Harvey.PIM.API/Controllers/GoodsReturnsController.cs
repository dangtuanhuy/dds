﻿using Harvey.Domain;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Commands.GoodsReturns;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.GoodsReturns;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/goods-returns")]
    [Authorize(Roles = "Administrator, InventoryManager, WarehouseStaff")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class GoodsReturnsController : ControllerBase
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
        public GoodsReturnsController(
            ICommandExecutor commandExecutor,
            IQueryExecutor queryExecutor
            )
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<Guid>> Add([FromBody]GoodsReturnDocumentModel gRTDModel)
        {
            var result = await _commandExecutor.ExecuteAsync(new AddGoodsReturnCommand(gRTDModel, User.GetUserId()));
            return Ok(result);
        }

        [HttpGet]
        public async Task<PagedResult<InventoryTransactionModel>> Get(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLocationIdString, string fromLocationIdString, string QueryString)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetGoodsReturnsQuery(pagingFilterCriteria, statusIdString, toLocationIdString, fromLocationIdString, QueryString));
            return result;
        }
    }
}
