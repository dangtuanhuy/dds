﻿using Harvey.Domain;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Commands.AllocationTransactions;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.AllocationTransactions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/allocation/transaction")]
    [Authorize(Policy = "InventoryManagerRole")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class AllocationTransactionsController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        public AllocationTransactionsController(
            IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<AllocationTransactionModel>>> Get(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLocationIdString, string fromLocationIdString, string queryString)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetAllocationTransactionsQuery(pagingFilterCriteria, statusIdString, toLocationIdString, fromLocationIdString, queryString));
            return Ok(result);
        }

        [HttpGet("location")]
        public async Task<ActionResult<PagedResult<AllocationTransactionModel>>> Get(TransferOutByAllocationRequestModel transferOutByAllocationRequestModel)
        {
            var pagingFilterCriteria = new PagingFilterCriteria
            {
                Page = transferOutByAllocationRequestModel.Page,
                NumberItemsPerPage = transferOutByAllocationRequestModel.NumberItemsPerPage
            };
            var result = await _queryExecutor.ExecuteAsync(new GetAllocationTransactionsByLocationQuery(pagingFilterCriteria,
                                                                                                        transferOutByAllocationRequestModel.FromLocationIds,
                                                                                                        transferOutByAllocationRequestModel.ToLocationIds,
                                                                                                        transferOutByAllocationRequestModel.FromDate,
                                                                                                        transferOutByAllocationRequestModel.ToDate));
            return Ok(result);
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<AllocationTransactionModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetAllocationTransactionByIdQuery(id));
            return Ok(result);
        }

        [HttpGet("mass")]
        public async Task<ActionResult<AllocationTransactionModel>> GetMassAllocation(Guid inventoryTransactionId)
        {
            var massStockAllocations = await _queryExecutor.ExecuteAsync(new GetMassAllocationTransationQuery(inventoryTransactionId));
            return Ok(massStockAllocations);
        }

        [HttpGet("outlet")]
        public async Task<ActionResult<List<OutletProductModel>>> GetMassAllocationByDate(DateTime FromDate, DateTime ToDate, Guid FromLocationId)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetMassAllocationByDate(FromDate, ToDate, FromLocationId));
            return Ok(result);
        }

        [HttpPost("bylistid")]
        public async Task<ActionResult<AllocationTransactionModel>> GetAllocationTransactionByListId([FromBody] AllocationTransactionByListIdModel allocationTransactionByListIdModel)
        {
            if (allocationTransactionByListIdModel == null)
            {
                return BadRequest("id is required.");
            }
            var result = await _commandExecutor.ExecuteAsync(new GetAllocationTransactionByListIdQuery(allocationTransactionByListIdModel));
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot get allocationTransactions.");
            }
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<AllocationTransactionModel>> Add([FromBody] AllocationTransactionModel allocationTransaction)
        {
            var result = await _commandExecutor.ExecuteAsync(new AddAllocationTransactionCommand(User.GetUserId(),
                                                                                                 allocationTransaction.Name,
                                                                                                 allocationTransaction.Description,
                                                                                                 allocationTransaction.FromLocationId,
                                                                                                 allocationTransaction.ToLocationId,
                                                                                                 allocationTransaction.DeliveryDate,
                                                                                                 allocationTransaction.Status,
                                                                                                 allocationTransaction.AllocationTransactionDetails));
            if (result != null && result.Id != Guid.Empty)
            {
                return CreatedAtAction("Add", result);
            }
            else
            {
                throw new InvalidOperationException("Cannot add allocationTransaction. Please try again.");
            }
        }

        [HttpPost]
        [Route("multi")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<AllocationTransactionModel>> AddMulti([FromBody] List<AllocationTransactionModel> allocationTransactions, DateTime FromDate, DateTime ToDate)
        {
            var result = await _commandExecutor.ExecuteAsync(new AddMultiAllocationTransactionCommand(allocationTransactions,
                                                                                                      User.GetUserId(),
                                                                                                      FromDate,
                                                                                                      ToDate));
            if (result)
            {
                return CreatedAtAction("Add", result);
            }
            else
            {
                throw new InvalidOperationException("Cannot add Allocation Transaction. Please try again.");
            }
        }

        [HttpPut]
        [Route("{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Update(Guid id, [FromBody] AllocationTransactionModel allocationTransaction)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Id is required.");
            }

            var result = await _commandExecutor.ExecuteAsync(new UpdateAllocationTransactionCommand(id, User.GetUserId(), allocationTransaction));
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new InvalidOperationException("Cannot not update allocationTransaction. Please try again.");
            }
        }

        [HttpPut]
        [Route("updateStatus/{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> UpdateStatus(Guid id, [FromBody] UpdateStatusAllocationTransactionModel updateStatusAllocationTransaction)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Id is required.");
            }

            var result = await _commandExecutor.ExecuteAsync(new UpdateStatusAllocationTransactionCommand(id, User.GetUserId(), updateStatusAllocationTransaction));
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new InvalidOperationException("Cannot not update allocationTransaction. Please try again.");
            }
        }

        [HttpDelete("{id}")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Id is required.");
            }
            await _commandExecutor.ExecuteAsync(new DeleteAllocationTransactionCommand(id));
            return Ok();
        }
    }
}
