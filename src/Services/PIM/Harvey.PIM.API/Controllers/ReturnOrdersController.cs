﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Queries.ReturnOrders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/return-order")]
    [Authorize]
    public class ReturnOrdersController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;

        public ReturnOrdersController(IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet("vendor/{VendorId}")]
        public async Task<ActionResult> GetByVendor(Guid VendorId)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetReturnOrdersByVendorQuery(VendorId));
            return Ok(result);
        }

        [HttpGet("grt")]
        public async Task<ActionResult> GetByIds([FromQuery]List<Guid> ids)
        {
            if (ids == null)
                return BadRequest();
            var result = await _queryExecutor.ExecuteAsync(new GetReturnOrdersByIdsQuery(ids));
            return Ok(result);
        }
    }
}
