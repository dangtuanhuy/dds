﻿using Harvey.Domain;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.BarCodes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/barcode")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class BarCodesController : ControllerBase
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
        public BarCodesController(
            IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet("{productId}")]
        public async Task<ActionResult<BarCodeProductViewModel>> GetByProduct(Guid productId)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetBarCodesByProductQuery(productId));
            return result;
        }
    }
}
