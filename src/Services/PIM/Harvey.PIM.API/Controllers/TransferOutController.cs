﻿using Harvey.Domain;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.Application.Infrastructure.Commands.TransferOut;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.TransferOut;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/transferout")]
    [Authorize (Policy = "InventoryTransfer")]
    public class TransferOutController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        public TransferOutController(
            ICommandExecutor commandExecutor,
            IQueryExecutor queryExecutor
         )
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<InventoryTransactionTransferViewModel>>> Get(PagingFilterCriteria pagingFilterCriteria, string statusIdString, string toLocationIdString, string fromLocationIdString, string QueryString)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetTransferOutsQuery(pagingFilterCriteria, statusIdString, toLocationIdString, fromLocationIdString, QueryString));
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<InventoryTransactionTransferViewModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetTransferOutByIdQuery(id));
            return Ok(result);
        }

        [HttpGet("stockOnHand")]
        public async Task<ActionResult<int>> GetStockOnHand(Guid fromLocationId, Guid variantId)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetTransferOutProductStockOnHandQuery(fromLocationId, variantId));
            return Ok(result);
        }

        [HttpGet("location")]
        public async Task<ActionResult<PagedResult<InventoryTransactionTransferViewModel>>> Get(TransferOutRequestModel transferOutRequestModel)
        {
            var pagingFilterCriteria = new PagingFilterCriteria
            {
                Page = transferOutRequestModel.Page,
                NumberItemsPerPage = transferOutRequestModel.NumberItemsPerPage
            };
            var result = await _queryExecutor.ExecuteAsync(new GetTransferOutsByLocationQuery(pagingFilterCriteria,
                                                                                                        transferOutRequestModel.FromLocationId,
                                                                                                        transferOutRequestModel.ToLocationId,
                                                                                                        transferOutRequestModel.FromDate,
                                                                                                        transferOutRequestModel.ToDate));
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody]TransferOutModel transferOutModel)
        {
            var result = await _commandExecutor.ExecuteAsync(new AddTransferOutCommand(User.GetUserId(),
                                                                                       transferOutModel.FromLocationId,
                                                                                       transferOutModel.ToLocationId,
                                                                                       transferOutModel.AllocationTransferOutProducts));

            if (result == true)
            {
                return Ok();
            }
            else
            {
                throw new InvalidOperationException("Out of stock.");
            }
        }
    }
}
