﻿using Harvey.Domain;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.Vendors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/vendors")]
    [Authorize]
    [ServiceFilter(typeof(ActivityTracking))]
    public class VendorsController : ControllerBase
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
        public VendorsController(
            ICommandExecutor commandExecutor,
            IQueryExecutor queryExecutor
            )
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpGet("all")]
        public async Task<ActionResult<PagedResult<VendorModel>>> Get()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetAllVendorsQuery());
            return Ok(result);
        }
    }
}
