﻿using Harvey.Domain;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.Dashboard;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/dashboard")]
    [Authorize(Policy = "InventoryTransfer")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class DashboardController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        public DashboardController(IQueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<DashboardModel>> GetByLocation(DashboardRequestModel request)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetStocksByLocationQuery(request.LocationId, request.QueryText));
            return Ok(result);
        }

        [HttpGet]
        [Route("top-product-sale")]
        public async Task<ActionResult<List<TopProductSaleModel>>> GetTopProductBySales(TopProductSaleRequestModel topProductSaleRequest)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetTopProductBySalesQuery(topProductSaleRequest));
            return result;
        }
        [HttpGet("purchaseOrderSummary")]
        public async Task<ActionResult<List<PurchaseOrderSummaryModel>>> GetPurchaseOrderSummary()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetPurchaseOrderSummaryQuery());
            return Ok(result);
        }

        [HttpGet("newProducts")]
        public async Task<ActionResult<PagedResult<NewProductModel>>> GetNewProducts(PagingFilterCriteria pagingFilterCriteria)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetNewProductsQuery(pagingFilterCriteria));
            return Ok(result);
        }
    }
}
