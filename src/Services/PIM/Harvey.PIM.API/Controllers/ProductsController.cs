﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Commands.Products;
using Harvey.PIM.Application.Infrastructure.Enums;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.Products;
using Harvey.Search.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Harvey.PIM.Application.Infrastructure.Models.AddProductModel;
using System.IO;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.Application.Services;
using System.IO.Compression;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/products")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class ProductsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
        private readonly PimDbContext _pimDbContext;
        private readonly ISearchService _searchService;
        private readonly IEfRepository<PimDbContext, Product, ProductListModel> _efRepository;
        private readonly DeploymentService _deploymentService;
        private readonly FormatFileService _formatFileService;
        public ProductsController(
            IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor,
            IMapper mapper,
            PimDbContext pimDbContext,
            ISearchService searchService,
            IEfRepository<PimDbContext, Product, ProductListModel> efRepository,
            DeploymentService deploymentService,
            FormatFileService formatFileService)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
            _mapper = mapper;
            _pimDbContext = pimDbContext;
            _searchService = searchService;
            _efRepository = efRepository;
            _deploymentService = deploymentService;
            _formatFileService = formatFileService;
        }

        [Authorize(Policy = "IMS")]
        [Route("all")]
        public async Task<ActionResult<IEnumerable<ProductListModel>>> GetAll()
        {
            return Ok(await _efRepository.GetAsync());
        }

        [Authorize(Policy = "ProductInventoryManagement")]
        [HttpGet]
        public async Task<ActionResult<PagedResult<ProductListModel>>> GetAll(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            return await _queryExecutor.ExecuteAsync(new GetProductListQuery(pagingFilterCriteria, queryText));
        }

        [Authorize(Policy = "IMS")]
        [HttpGet("total")]
        public async Task<ActionResult<List<ProductListModel>>> GetProductsWithSearchText(string querySearch)
        {
            return await _queryExecutor.ExecuteAsync(new GetProductWithSearchQuery(querySearch));
        }

        [Authorize(Policy = "IMS")]
        [HttpGet("print-label-variants")]
        public async Task<ActionResult<ProductForPrintLabelResponse>> SearchVariantsForPrintLabel(string querySearch,
                                                                                                int pageIndex = 0, int pageSize = 10)
        {
            return await _queryExecutor.ExecuteAsync(new SearchVariantsForPrintLabelQuery(querySearch, pageIndex, pageSize));
        }

        [Authorize(Policy = "ProductInventoryManagement")]
        [HttpGet]
        [Route("template/{id}")]
        public async Task<ActionResult<ProductModel>> GetFromTemplate(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetProductFromTemplateIdQuery(id));
            return result;
        }

        [Authorize(Policy = "IMS")]
        [HttpGet()]
        [Route("{id}")]
        public async Task<ActionResult<ProductModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetProductByIdQuery(id));
            return result;
        }

        [Authorize(Policy = "ProductInventoryManagement")]
        [HttpPost]
        public async Task<ActionResult<ProductListModel>> Add([FromBody]AddProductModel productModel)
        {
            if (productModel == null)
            {
                return BadRequest("Model is required.");
            }
            if (productModel.CategoryId == Guid.Empty)
            {
                throw new ArgumentException("Category is required.");
            }
            if (string.IsNullOrEmpty(productModel.Name))
            {
                throw new ArgumentException("Name is required.");
            }
            
            if (productModel.FieldTemplateId == Guid.Empty)
            {
                throw new ArgumentException("Product requires a field template.");
            }
            var queryProduct = _pimDbContext
                .Products
                .Where(x => x.Name.Trim().ToLower() == productModel.Name.Trim().ToLower() && !x.IsDelete).FirstOrDefault();
            if (queryProduct != null)
            {
                throw new ArgumentException("Name is duplicate!");
            }

            var fields = await _pimDbContext
               .Field_FieldTemplates
               .Include(x => x.Field)
               .Where(x => x.FieldTemplateId == productModel.FieldTemplateId).ToListAsync();

            var fieldIds = fields.Select(x => x.FieldId);
            var productFieldIds = productModel
                .ProductFields
                .Select(x => x.FieldId)
                .Union(productModel.Variants.SelectMany(x => x.VariantFields).Select(x => x.FieldId));

            foreach (var item in productFieldIds)
            {
                if (!fieldIds.Any(x => item == x))
                {
                    return BadRequest("Product includes field(s) not in template");
                }
            }

            var productId = Guid.NewGuid();
            var addProductFieldValueModels = new List<DetailFieldValueModel>();
            var addVariantFieldValueModels = new Dictionary<Guid, List<DetailFieldValueModel>>();
            var addVariantPriceModel = new Dictionary<Guid, PriceModel>();
            var addVariantCodeModels = new Dictionary<Guid, string>();
            foreach (var item in productModel.ProductFields)
            {
                var field = fields.Single(x => x.FieldId == item.FieldId);
                addProductFieldValueModels.Add(new DetailFieldValueModel(
                    productId,
                    field.Section,
                    field.OrderSection,
                    field.IsVariantField,
                    field.Field.Type,
                    item.FieldId,
                    field.Field.Name,
                    item.FieldValue)
                {
                    Id = Guid.NewGuid()
                });
            }

            var fieldValues = productModel.ProductFields.Select(p => !String.IsNullOrEmpty(p.FieldValue) ? p.FieldValue.ToString() : String.Empty).ToList();
            var categoryName = _pimDbContext.Categories.FirstOrDefault(c => c.Id == productModel.CategoryId)?.Name;

            var variantFieldValues = new List<string>();

            if (productModel.Variants != null)
            {
                var variantFields = productModel.Variants.SelectMany(p => p.VariantFields).ToList();
                variantFieldValues = variantFields.Select(v => !String.IsNullOrEmpty(v.FieldValue) ? v.FieldValue.ToString() : String.Empty).ToList();

                foreach (var item in productModel.Variants)
                {
                    var variantId = Guid.NewGuid();
                    addVariantCodeModels[variantId] = item.Code;
                    foreach (var variantField in item.VariantFields)
                    {
                        var field = fields.Single(x => x.FieldId == variantField.FieldId);
                        if (addVariantFieldValueModels.ContainsKey(variantId))
                        {
                            addVariantFieldValueModels[variantId].Add(
                                new DetailFieldValueModel(
                                   productId,
                                   field.Section,
                                   field.OrderSection,
                                   field.IsVariantField,
                                   field.Field.Type,
                                   variantField.FieldId,
                                   field.Field.Name,
                                   variantField.FieldValue)
                                {
                                    Id = Guid.NewGuid()
                                });

                            addVariantPriceModel[variantId] = item.Prices;
                        }
                        else
                        {
                            addVariantFieldValueModels.Add(
                              variantId,
                              new List<DetailFieldValueModel>() {
                                   new DetailFieldValueModel(
                                   productId,
                                   field.Section,
                                   field.OrderSection,
                                   field.IsVariantField,
                                   field.Field.Type,
                                   variantField.FieldId,
                                   field.Field.Name,
                                   variantField.FieldValue)
                                   {
                                       Id = Guid.NewGuid()
                                   }
                              });
                            addVariantPriceModel.Add(variantId, item.Prices);
                        }
                    }
                }
            }

            fieldValues = fieldValues.Concat(variantFieldValues).ToList();
            fieldValues.Add(categoryName);
            var indexingValue = string.Join(",", fieldValues);

            var product = await _commandExecutor.ExecuteAsync(
                new AddProductCommand(
                    User.GetUserId(),
                    productId,
                    productModel.CategoryId,
                    categoryName,
                    productModel.Name,
                    productModel.Description,
                    productModel.IsDelete,
                    productModel.FieldTemplateId,
                    addProductFieldValueModels,
                    addVariantFieldValueModels,
                    addVariantPriceModel,
                    addVariantCodeModels,
                    indexingValue,
                    productModel.Code
                   ));

            return _mapper.Map<ProductListModel>(product);
        }

        [Authorize(Policy = "ProductInventoryManagement")]
        [HttpPut]
        [Route("{id}")]
        public async Task<ActionResult<ProductModel>> Update(Guid id, [FromBody] UpdateProductModel productModel)
        {
            if (productModel.CategoryId == Guid.Empty)
            {
                throw new ArgumentException("Category is required.");
            }

            if (string.IsNullOrEmpty(productModel.Name))
            {
                throw new ArgumentException("Name is required.");
            }
            
            if (productModel.FieldTemplateId == Guid.Empty)
            {
                throw new ArgumentException("Product requires a field template.");
            }
            var queryProduct = _pimDbContext
                .Products
                .Where(x => x.Name.Trim().ToLower() == productModel.Name.Trim().ToLower() && x.Id != productModel.Id && !x.IsDelete).FirstOrDefault();
            if (queryProduct != null)
            {
                throw new ArgumentException("Name is duplicate!");
            }
            var fields = await _pimDbContext
               .Field_FieldTemplates
               .Include(x => x.Field)
               .Where(x => x.FieldTemplateId == productModel.FieldTemplateId).ToListAsync();

            var fieldIds = fields.Select(x => x.FieldId);
            var productFieldIds = productModel
                .ProductFields
                .Select(x => x.FieldId)
                .Union(productModel.Variants.SelectMany(x => x.VariantFields).Select(x => x.FieldId));

            foreach (var item in productFieldIds)
            {
                if (!fieldIds.Any(x => item == x))
                {
                    return BadRequest("Product includes field(s) not in template");
                }
            }

            var productId = productModel.Id;
            var productFieldValueModels = new List<DetailFieldValueModel>();
            var variantFieldValueModels = new Dictionary<AddVariantModel, List<DetailFieldValueModel>>();
            var variantPriceModel = new Dictionary<Guid, PriceModel>();
            var variantCodeModels = new Dictionary<Guid, string>();
            foreach (var item in productModel.ProductFields)
            {
                var field = fields.Single(x => x.FieldId == item.FieldId);
                productFieldValueModels.Add(new DetailFieldValueModel(
                    productId,
                    field.Section,
                    field.OrderSection,
                    field.IsVariantField,
                    field.Field.Type,
                    item.FieldId,
                    field.Field.Name,
                    item.FieldValue)
                {
                    Id = item.Id
                });
            }

            var fieldValues = productModel.ProductFields.Select(p => !String.IsNullOrEmpty(p.FieldValue) ? p.FieldValue.ToString() : String.Empty).ToList();
            var categoryName = _pimDbContext.Categories.FirstOrDefault(c => c.Id == productModel.CategoryId)?.Name;
            var variantFieldValues = new List<string>();

            if (productModel.Variants != null)
            {
                var variantFields = productModel.Variants.SelectMany(p => p.VariantFields).ToList();
                variantFieldValues = variantFields.Select(v => !String.IsNullOrEmpty(v.FieldValue) ? v.FieldValue.ToString() : String.Empty).ToList();

                foreach (var item in productModel.Variants)
                {
                    var variantId = Guid.NewGuid();
                    if (item.Id != Guid.Empty)
                    {
                        variantId = item.Id;
                    }
                    variantCodeModels[variantId] = item.Code;
                    foreach (var variantField in item.VariantFields)
                    {
                        var field = fields.Single(x => x.FieldId == variantField.FieldId);
                        //var variantModelKeyValuePair = variantFieldValueModels.FirstOrDefault(x => x.Key.Id == variantId);
                        AddVariantModel variantModel = null;
                        foreach (var vm in variantFieldValueModels)
                        {
                            if (vm.Key.Id == variantId)
                            {
                                variantModel = vm.Key;
                                break;
                            }
                        }
                        if (variantModel == null)
                        {
                            variantModel = new AddVariantModel()
                            {
                                Id = variantId,
                                Action = variantId != item.Id ? ItemActionType.Add : ItemActionType.Update
                            };
                        }

                        if (variantFieldValueModels.ContainsKey(variantModel))
                        {
                            variantFieldValueModels[variantModel].Add(
                                new DetailFieldValueModel(
                                   productId,
                                   field.Section,
                                   field.OrderSection,
                                   field.IsVariantField,
                                   field.Field.Type,
                                   variantField.FieldId,
                                   field.Field.Name,
                                   variantField.FieldValue)
                                {
                                    Id = variantField.Id
                                });

                            variantPriceModel[variantId] = item.Prices;
                        }
                        else
                        {
                            variantFieldValueModels.Add(
                              variantModel,
                              new List<DetailFieldValueModel>() {
                                   new DetailFieldValueModel(
                                   productId,
                                   field.Section,
                                   field.OrderSection,
                                   field.IsVariantField,
                                   field.Field.Type,
                                   variantField.FieldId,
                                   field.Field.Name,
                                   variantField.FieldValue)
                                   {
                                       Id = variantField.Id
                                   }
                              });
                            variantPriceModel.Add(variantId, item.Prices);
                        }
                    }
                }
            }
            fieldValues = fieldValues.Concat(variantFieldValues).ToList();
            fieldValues.Add(categoryName);
            var indexingValue = string.Join(",", fieldValues);

            var product = await _commandExecutor.ExecuteAsync(
                new UpdateProductCommand(
                    User.GetUserId(),
                    productId,
                    productModel.CategoryId,
                    categoryName,
                    productModel.Name,
                    productModel.Description,
                    productModel.IsDelete,
                    productFieldValueModels,
                    variantFieldValueModels,
                    variantPriceModel,
                    variantCodeModels,
                    indexingValue
                    ));

            return Ok(product);
        }

        [Authorize(Policy = "ProductInventoryManagement")]
        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            await _commandExecutor.ExecuteAsync(new DeleteProductCommand(id));
            return Ok();
        }

        [Authorize(Policy = "ProductInventoryManagement")]
        [HttpPost("deployment")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<DeploymentResult>> Deployment()
        {
            var file = Request.Form.Files[0];
            if (Path.GetExtension(file.FileName) != ".zip")
            {
                return BadRequest("Only support zip file.");
            }
            var result = await _deploymentService.Execute(User.GetUserId(), file);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("count")]
        public async Task<ActionResult<CountModel>> Count()
        {
            return await _queryExecutor.ExecuteAsync(new GetCountQuery());
        }
        
        [HttpPost("format-csv")]
        public async Task<IActionResult> FormatCSVProducts()
        {
            var files = Request.Form.Files;
            if (files == null || files.Count == 0)
            {
                return BadRequest("File not found.");
            }
            var file = files[0];
            if (Path.GetExtension(file.FileName) != ".csv")
            {
                return BadRequest("Only support csv file.");
            }

            var stringBuilder = _formatFileService.FormatProductCSVFile(file, User.GetUserId());
            return FileExtension.GetFileUTF8(stringBuilder, "FormatedProduct.csv");
        }
    }
}
