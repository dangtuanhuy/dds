﻿using Harvey.MarketingAutomation.Enums;
using Harvey.PIM.Application.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.PIM.Application.Services;
using System;
using System.Collections.Generic;
using Harvey.PIM.Application.Infrastructure.Queries.Categories;
using Harvey.PIM.Application.Infrastructure.Models.Feeds;

namespace Harvey.PIM.API.Controllers
{
    //[Authorize]
    [Route("api/feeds")]
    public class FeedsController : ControllerBase
    {
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        private readonly IQueryExecutor _queryExecutor;
        private readonly FeedService _feedService;
        public FeedsController(PimDbContext pimDbContext,
            TransactionDbContext transactionDbContext,
            IQueryExecutor queryExecutor,
            FeedService feedService
            )
        {
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
            _queryExecutor = queryExecutor;
            _feedService = feedService;
        }

        public async Task<ActionResult> Get(FeedType feedType, DateTime lastSync)
        {
            switch (feedType)
            {
                case FeedType.Product:
                    return Ok(await _feedService.GetProducts(lastSync));
                case FeedType.Channel:
                    return Ok(await _feedService.GetChannels(lastSync));
                case FeedType.Store:
                    return Ok(await _feedService.GetStores(lastSync));
                case FeedType.ChannelStoreAssignment:
                    return Ok(await _feedService.GetChannelStoreAssignments());
                case FeedType.Variant:
                    return Ok(await _feedService.GetVariants(lastSync));
                case FeedType.Category:
                    return Ok(await _feedService.GetCategorys(lastSync));
                case FeedType.StockType:
                    return Ok(await _feedService.GetStockTypes());
                case FeedType.TransactionType:
                    return Ok(await _feedService.GetTransactionTypes());
                case FeedType.Location:
                    return Ok(await _feedService.GetLocations(lastSync));
                case FeedType.Price:
                    return Ok(await _feedService.GetPrices(lastSync));
                case FeedType.BarCode:
                    return Ok(await _feedService.GetBarcodes(lastSync));
                case FeedType.PaymentMethod:
                    return Ok(await _feedService.GetPaymentMethods());
            }
            return BadRequest($"Feed don't support type {feedType.ToString()}");
        }

        [Route("count")]
        [HttpGet]
        public async Task<ActionResult> Count(FeedType feedType, DateTime? lastSync)
        {
            switch (feedType)
            {
                case FeedType.Product:
                    return Ok(await _feedService.CountProducts(lastSync));
            }
            return BadRequest($"Feed don't support type {feedType.ToString()}");
        }

        [Route("categoryIds")]
        [HttpPost]
        public async Task<ActionResult> GetCategoryIdsByProductIds([FromBody] GetCategoryIdsByProdutIdsRequest request)
        {
            return Ok(await _queryExecutor.ExecuteAsync(new GetCategoryIdsByProductIdsQuery(request.ProductIds)));
        }
    }
}
