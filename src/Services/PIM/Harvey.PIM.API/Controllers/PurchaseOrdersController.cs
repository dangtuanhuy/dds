﻿using Harvey.Domain;
using Harvey.PIM.Application.Infrastructure.Queries.PurchaseOrders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/purchase-order")]
    [Authorize]
    public class PurchaseOrdersController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;

        public PurchaseOrdersController(IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet("vendor/{VendorId}")]
        public async Task<ActionResult> GetByVendor(Guid VendorId)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetPurchaseOrdersByVendorQuery(VendorId));
            return Ok(result);
        }

        [HttpGet("giw")]
        public async Task<ActionResult> GetByIds([FromQuery]List<Guid> ids)
        {
            if (ids == null)
                return BadRequest();
            var result = await _queryExecutor.ExecuteAsync(new GetPurchaseOrdersByIdsQuery(ids));
            return Ok(result);
        }
    }
}
