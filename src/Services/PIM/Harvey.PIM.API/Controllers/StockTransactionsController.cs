﻿using Harvey.Domain;
using Harvey.PIM.API.Extensions;
using Harvey.PIM.API.Filters;
using Harvey.PIM.Application.Infrastructure.Commands.StockTransactions;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Queries.StockTransactions;
using Harvey.PIM.Application.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/stock-transaction")]
    [Authorize(Policy = "InventoryTransfer")]
    [ServiceFilter(typeof(ActivityTracking))]
    public class StockTransactionsController : ControllerBase
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
        private readonly DeploymentStockTransactionService _deploymentService;
        public StockTransactionsController(ICommandExecutor commandExecutor,
            IQueryExecutor queryExecutor,
            DeploymentStockTransactionService deploymentService)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _deploymentService = deploymentService;
        }

        [HttpGet("ids")]
        public async Task<ActionResult> GetByPurchaseOrders([FromQuery]List<Guid> ids)
        {
            if (ids == null)
                return BadRequest();
            var result = await _queryExecutor.ExecuteAsync(new GetStockTransactionsByPurchaseOrdersQuery(ids));
            return Ok(result);
        }

        [HttpGet("exportcsv")]
        public async Task<IActionResult> ExportCSV([FromQuery]Guid locationId, List<Guid> categories)
        {
            var buffer = await _queryExecutor.ExecuteAsync(new StockTransactionsExportCSVQuery(locationId, categories));
            return FileExtension.GetFileUTF8(buffer, "StockTransactions.csv");
        }

        [HttpPost("importcsv")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<DeploymentStockTransactionResult>> ImportCSV(string fromLocationId, string toLocationId)
        {
            Guid _fromLocationId = Guid.Parse(fromLocationId);
            Guid _toLocationId = Guid.Parse(toLocationId);

            var file = Request.Form.Files[0];
            if (Path.GetExtension(file.FileName) != ".csv")
            {
                return BadRequest("Only support csv file.");
            }
            var result = await _deploymentService.Execute(User.GetUserId(), file, _fromLocationId, _toLocationId);
            return Ok(result);
        }

        [HttpGet("returnorders")]
        public async Task<ActionResult> GetByReturnOrders([FromQuery]List<Guid> ids)
        {
            if (ids == null)
                return BadRequest();
            var result = await _queryExecutor.ExecuteAsync(new GetStockTransactionsByReturnOrdersQuery(ids));
            return Ok(result);
        }

        [HttpPost("updatedfromretails")]
        [ServiceFilter(typeof(EfUnitOfWork))]
        [AllowAnonymous]
        public async Task<ActionResult<List<StockTransactionUpdatedRetailsModel>>> UpdatedFromRetails([FromBody] StockTransactionUpdatedRetailsRequest request)
        {
            var result = await _commandExecutor.ExecuteAsync(new StockTransactionUpdatedFromRetailsCommand(request.LocationId, request.Items));
            var response = new UpdateChannelStockTransactionResponse
            {
                ServerInformation = request.ServerInformation,
                StockTransactionUpdatedRetailsModels = result
            };
            return Ok(response);
        }
    }
}
