﻿using Harvey.PIM.API.Extensions;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Services;
using Harvey.Reporting;
using Harvey.Reporting.ReportStatementModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Controllers
{
    [Route("api/reports")]
    [Authorize]
    public class ReportsController : ControllerBase
    {
        private readonly ReportServer _reportService;
        private readonly PimDbContext _pimDbContext;
        private readonly TransactionDbContext _transactionDbContext;
        private readonly SalePerformanceService _salePerformanceService;
        public ReportsController(ReportServer reportService, PimDbContext pimDbContext, TransactionDbContext transactionDbContext, SalePerformanceService salePerformanceService)
        {
            _reportService = reportService;
            _pimDbContext = pimDbContext;
            _transactionDbContext = transactionDbContext;
            _salePerformanceService = salePerformanceService;
        }

        [HttpPost("transfer-in")]
        public async Task<ActionResult<string>> TransferIn([FromBody] InventoryTransactionTransferViewModel inventoryTransactionTransferViewModel)
        {
            var reportId = inventoryTransactionTransferViewModel.Id.ToString();

            var transferInStatement = new TransferInStatementHeader()
            {
                ReportId = reportId,
                FromLocation = inventoryTransactionTransferViewModel.FromLocation,
                ToLocation = inventoryTransactionTransferViewModel.ToLocation,
                TransferNo = inventoryTransactionTransferViewModel.TransferNumber,
                TransferDate = inventoryTransactionTransferViewModel.CreatedDate.ToString("MM/dd/yyyy"),
                TotalQuantity = 0
            };

            foreach(var InventoryTransactionTransferProduct in inventoryTransactionTransferViewModel.InventoryTransactionTransferProducts)
            {
                var barcode = await _pimDbContext.BarCode.OrderByDescending(x => x.CreatedDate)
                                                   .FirstOrDefaultAsync(x => x.VariantId == InventoryTransactionTransferProduct.VariantId);
                var variant = await _pimDbContext.Variants.FirstOrDefaultAsync(x => x.Id == InventoryTransactionTransferProduct.VariantId);
                var fieldValues = await _pimDbContext.FieldValues.Where(x => x.EntityId == InventoryTransactionTransferProduct.VariantId).ToListAsync();
                var product = await _pimDbContext.Products.FirstOrDefaultAsync(x => x.Id == InventoryTransactionTransferProduct.ProductId);

                var variantName = "";
                foreach (var fieldValue in fieldValues)
                {
                    if (variantName == "")
                    {
                        variantName = fieldValue?.PredefinedListValue;
                    }
                    else
                    {
                        variantName = variantName + " " + fieldValue?.PredefinedListValue;
                    }
                }


                var transferInStatementDetail = new TransferInStatementDetail()
                {
                    ReportId = reportId,
                    BarCode = barcode?.Code,
                    Description = $"{product?.Name} - {variantName}",
                    InventoryCode = variant?.SKUCode,
                    TransQuantity = InventoryTransactionTransferProduct.Quantity,
                    UOM = "PCS"
                };

                transferInStatement.Details.Add(transferInStatementDetail);

            }
            
            foreach(var transferInStatementDetail in transferInStatement.Details)
            {
                transferInStatement.TotalQuantity = transferInStatement.TotalQuantity + transferInStatementDetail.TransQuantity;
            }
            
            var result = await _reportService.PrepareData(new ReportModel<TransferInStatementHeader>()
            {
                Type = Reporting.Enums.ReportType.TransferIn,
                ReportId = reportId,
                DataSource = transferInStatement
            });
            return Ok(result);
        }

        [HttpPost("transfer-out")]
        public async Task<ActionResult<string>> TransferOut([FromBody] InventoryTransactionTransferViewModel inventoryTransactionTransferViewModel)
        {
            var reportId = inventoryTransactionTransferViewModel.Id.ToString();

            var transferOutStatement = new TransferOutStatementHeader()
            {
                ReportId = reportId,
                FromLocation = inventoryTransactionTransferViewModel.FromLocation,
                ToLocation = inventoryTransactionTransferViewModel.ToLocation,
                TransferNo = inventoryTransactionTransferViewModel.TransferNumber,
                TransferDate = inventoryTransactionTransferViewModel.CreatedDate.ToString("MM/dd/yyyy"),
                TotalQuantity = 0,
                TotalTransferPrice = ConvertToDecimal(0)
            };

            foreach (var InventoryTransactionTransferProduct in inventoryTransactionTransferViewModel.InventoryTransactionTransferProducts)
            {
                var barCode = await _pimDbContext.BarCode.OrderByDescending(x => x.CreatedDate)
                                                   .FirstOrDefaultAsync(x => x.VariantId == InventoryTransactionTransferProduct.VariantId);
                var inventoryTransaction = await _pimDbContext.Variants.FirstOrDefaultAsync(x => x.Id == InventoryTransactionTransferProduct.VariantId);
                var product = await _pimDbContext.Products.FirstOrDefaultAsync(x => x.Id == InventoryTransactionTransferProduct.ProductId);
                var variant = await _pimDbContext.Variants.FirstOrDefaultAsync(x => x.Id == InventoryTransactionTransferProduct.VariantId);
                var fieldValues = await _pimDbContext.FieldValues.Where(x => x.EntityId == InventoryTransactionTransferProduct.VariantId).ToListAsync();
                var price = await _pimDbContext.Prices.FirstOrDefaultAsync(x => x.Id == variant.PriceId);
                
                var variantName = "";
                foreach (var fieldValue in fieldValues)
                {
                    if (variantName == "")
                    {
                        variantName = fieldValue?.PredefinedListValue;
                    }
                    else
                    {
                        variantName = variantName + " " + fieldValue?.PredefinedListValue;
                    }
                }

                var transferOutStatementDetail = new TransferOutStatementDetail()
                {
                    ReportId = reportId,
                    BarCode = barCode?.Code,
                    Description = $"{product?.Name} - {variantName}",
                    InventoryCode = inventoryTransaction?.SKUCode,
                    TransQuantity = InventoryTransactionTransferProduct.Quantity,
                    UOM = "PCS",
                    Price = price?.ListPrice != null ? ConvertToDecimal(price?.ListPrice).ToString() : "0",
                    TotalProductPrice = ConvertToDecimal(((price?.ListPrice != null ? price?.ListPrice : 0) * InventoryTransactionTransferProduct.Quantity)).ToString()
                };

                transferOutStatement.Details.Add(transferOutStatementDetail);
            }

            foreach (var transferOutStatementDetail in transferOutStatement.Details)
            {
                transferOutStatement.TotalQuantity = transferOutStatement.TotalQuantity
                                                      + transferOutStatementDetail.TransQuantity;

                transferOutStatement.TotalTransferPrice = ConvertToDecimal((Double.Parse(transferOutStatement.TotalTransferPrice)
                                                                                   + Double.Parse(transferOutStatementDetail.TotalProductPrice)));
            }

            transferOutStatement.TotalTransferPrice = ConvertToDecimal(transferOutStatement.TotalTransferPrice);

            var result = await _reportService.PrepareData(new ReportModel<TransferOutStatementHeader>()
            {
                Type = Reporting.Enums.ReportType.TransferOut,
                ReportId = reportId,
                DataSource = transferOutStatement
            });
            return Ok(result);
        }

        [HttpPost("goods-inward")]
        public async Task<ActionResult<string>> GoodsInward([FromBody] InventoryTransactionTransferViewModel inventoryTransactionTransferViewModel)
        {
            var reportId = inventoryTransactionTransferViewModel.Id.ToString();

            var vendor = _pimDbContext.Vendors.FirstOrDefault(x => x.Id == inventoryTransactionTransferViewModel.FromLocationId);
            
            var goodsInwardStatement = new GoodsInwardStatementHeader()
            {
                ReportId = reportId,
                VendorName = vendor.Name,
                Address = vendor?.Address1 != null ? vendor?.Address1 : vendor?.Address2,
                City = vendor?.CityCode,
                Country = vendor?.Country,
                ZipCode = vendor?.ZipCode,
                Phone = vendor?.Phone,
                Fax = vendor?.Fax,
                InvoiceNo = "",
                GIDNo = inventoryTransactionTransferViewModel.TransferNumber,
                GSTRegNo = "201229862H",
                GIDDate = inventoryTransactionTransferViewModel.CreatedDate.ToString("MM/dd/yyyy"),
                Discount = ConvertToDecimal(0),
                SubTotal = ConvertToDecimal(0),
                Tax = ConvertToDecimal(0),
                NetTotal = ConvertToDecimal(0)
            };

            foreach (var InventoryTransactionTransferProduct in inventoryTransactionTransferViewModel.InventoryTransactionTransferProducts)
            {
                var variant = await _pimDbContext.Variants.FirstOrDefaultAsync(x => x.Id == InventoryTransactionTransferProduct.VariantId);
                var fieldValues = await _pimDbContext.FieldValues.Where(x => x.EntityId == InventoryTransactionTransferProduct.VariantId).ToListAsync();
                var product = await _pimDbContext.Products.FirstOrDefaultAsync(x => x.Id == InventoryTransactionTransferProduct.ProductId);
                var goodsInwardItem = await _transactionDbContext.GIWDocumentItems.FirstOrDefaultAsync(x => x.VariantId == InventoryTransactionTransferProduct.VariantId
                                                                                                         && x.GIWDocumentId == inventoryTransactionTransferViewModel.InventoryTransactionRefId);

                var variantName = "";
                foreach (var fieldValue in fieldValues)
                {
                    if( variantName == "")
                    {
                        variantName = fieldValue?.PredefinedListValue;
                    } else
                    {
                        variantName = variantName + " " + fieldValue?.PredefinedListValue;
                    }                    
                }

                var goodsInwardStatementDetail = new GoodsInwardStatementDetail()
                {
                    ReportId = reportId,
                    Description = $"{product?.Name} - {variantName}",
                    InventoryCode = variant?.SKUCode,
                    UOM = "PCS",
                    Quantity = ConvertToDecimal(InventoryTransactionTransferProduct.Quantity),
                    Cost = ConvertToDecimal(goodsInwardItem?.CostValue),
                    TotalValue = ConvertToDecimal(((goodsInwardItem?.CostValue != null ? goodsInwardItem?.CostValue : 0) * InventoryTransactionTransferProduct.Quantity)).ToString()
                };

                goodsInwardStatement.Details.Add(goodsInwardStatementDetail);

            }

            foreach (var goodsInwardStatementDetail in goodsInwardStatement.Details)
            {
                goodsInwardStatement.SubTotal = ConvertToDecimal(( Double.Parse(goodsInwardStatement.SubTotal)
                                                                          + Double.Parse(goodsInwardStatementDetail.TotalValue)));
            }
            
            goodsInwardStatement.Tax = ConvertToDecimal((Double.Parse(vendor.TaxTypeValue != null ? vendor.TaxTypeValue : "0") * Double.Parse(goodsInwardStatement.SubTotal) / 100));
            goodsInwardStatement.NetTotal = ConvertToDecimal((Double.Parse(goodsInwardStatement.SubTotal)
                                                                      + Double.Parse(goodsInwardStatement.Tax)));

            var result = await _reportService.PrepareData(new ReportModel<GoodsInwardStatementHeader>()
            {
                Type = Reporting.Enums.ReportType.GoodsInWard,
                ReportId = reportId,
                DataSource = goodsInwardStatement
            });
            return Ok(result);
        }

        public string ConvertToDecimal (object value)
        {
            return String.Format("{0:0.00}", value);
        }
       

        [HttpPost("sale-performance")]
        public async Task<IActionResult> SalePerformance([FromBody] SalePerformanceRequest request)
        {
            var buffer = await _salePerformanceService.ExecuteAsync(request);
            return FileExtension.GetFileUTF8(buffer, "SalePerformance.csv");
        }
    }
}
