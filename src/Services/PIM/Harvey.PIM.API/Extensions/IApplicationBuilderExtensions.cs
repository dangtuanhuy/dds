﻿using Hangfire;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events;
using Harvey.EventBus.Events.FieldValues;
using Harvey.EventBus.Events.Prices;
using Harvey.EventBus.Events.Products;
using Harvey.EventBus.Events.Promotions;
using Harvey.EventBus.Events.Promotions.PromotionConditions;
using Harvey.EventBus.Events.Promotions.PromotionCouponCodes;
using Harvey.EventBus.Events.Promotions.PromotionDetails;
using Harvey.EventBus.Events.PurchaseOrders;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.EventBus.Events.StockLevels;
using Harvey.EventBus.Events.Variants;
using Harvey.EventBus.Events.Vendors;
using Harvey.EventBus.Publishers;
using Harvey.Exception;
using Harvey.Exception.Extensions;
using Harvey.Exception.Handlers;
using Harvey.Job;
using Harvey.Job.Hangfire;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.EventHandlers;
using Harvey.MarketingAutomation.Events;
using Harvey.PIM.API.Filters;
using Harvey.PIM.API.Hubs;
using Harvey.PIM.Application.Channels;
using Harvey.PIM.Application.Channels.Promotions;
using Harvey.PIM.Application.Channels.Promotions.PromotionConditions;
using Harvey.PIM.Application.Channels.Promotions.PromotionCouponCodes;
using Harvey.PIM.Application.Channels.Promotions.PromotionDetails;
using Harvey.PIM.Application.EventHandlers;
using Harvey.PIM.Application.EventHandlers.FieldValues;
using Harvey.PIM.Application.EventHandlers.Products;
using Harvey.PIM.Application.EventHandlers.PurchaseOrders;
using Harvey.PIM.Application.EventHandlers.StockAndPriceLevels;
using Harvey.PIM.Application.EventHandlers.StockLevels;
using Harvey.PIM.Application.EventHandlers.Variants;
using Harvey.PIM.Application.EventHandlers.Vendors;
using Harvey.PIM.Application.Events;
using Harvey.PIM.Application.Events.Products;
using Harvey.PIM.Application.Hubs;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Provisions;
using Harvey.PIM.Application.Infrastructure.Publishers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Linq;
using System.Net;

namespace Harvey.PIM.API.Extensions
{
    public static class IApplicationBuilderExtensions
    {
        public static void ConfigureEventBus(this IApplicationBuilder app, IHostingEnvironment env)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.AuthorIdResolver = () =>
            {
                var httpContextAccessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>();
                if (httpContextAccessor.HttpContext == null)
                {
                    return default(Guid);
                }
                var idClaim = httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "sub");
                if (idClaim == null)
                {
                    return default(Guid);
                }
                return Guid.Parse(idClaim.Value);
            };
            eventBus.AddSubcription<ConnectorIntergrationEvent, ConnectorIntergrationEventHandler>();
            eventBus.AddSubcription<LoggingPublisher, LoggingEvent, LoggingEventHandler>();
            eventBus.AddSubcription<ProductPublisher, ProductCreatedEvent, ProductCreatedEventHandler>();
            eventBus.AddSubcription<ProductPublisher, ProductUpdatedEvent, ProductUpdatedEventHandler>();
            eventBus.AddSubcription<ProductPublisher, ProductDeletedEvent, ProductDeletedEventHandler>();
            eventBus.AddSubcription<ProductPublisher, FieldValueCreatedEvent, ProductFieldValueCreatedEventHandler>();
            eventBus.AddSubcription<ProductPublisher, FieldValueUpdatedEvent, ProductFieldValueUpdatedEventHandler>();
            eventBus.AddSubcription<ProductPublisher, VariantCreatedEvent, VariantCreatedEventHandler>();
            eventBus.AddSubcription<ProductPublisher, VariantUpdatedEvent, VariantUpdatedEventHandler>();
            eventBus.AddSubcription<ProductPublisher, PriceCreatedEvent, PriceCreatedEventHandler>();
            eventBus.AddSubcription<StockLevelPublisher, StockLevelUpdatedEvent, StockLevelUpdatedEventHandler>();
            eventBus.AddSubcription<ProductPublisher, ProductCreatedEvent, ProductCreatedIndexingEventHandler>();
            eventBus.AddSubcription<ProductPublisher, ProductUpdatedEvent, ProductUpdatedIndexingEventHandler>();
            eventBus.AddSubcription<ProductPublisher, ProductDeletedEvent, ProductDeletedIndexingEventHandler>();
            eventBus.AddSubcription<SynchronizationPublisher, SynchronizationCreatedEvent, SynchronizationCreatedEventHandler>();
            eventBus.AddSubcription<PurchasePOtoPIMPublisher, VendorCreatedEvent, VendorCreatedEventHandler>();
            eventBus.AddSubcription<PurchasePOtoPIMPublisher, VendorUpdatedEvent, VendorUpdatedEventHandler>();
            eventBus.AddSubcription<PurchasePOtoPIMPublisher, VendorDeletedEvent, VendorDeletedEventHandler>();
            eventBus.AddSubcription<PurchasePOtoPIMPublisher, PurchaseOrderConfirmStatusEvent, PurchaseOrderConfirmStatusEventHandler>();
            eventBus.AddSubcription<PurchasePOtoPIMPublisher, PurchaseOrderRejectStatusEvent, PurchaseOrderRejectStatusEventHandler>();
            eventBus.AddSubcription<PurchasePOtoPIMPublisher, PurchaseOrderDeletedEvent, PurchaseOrderDeletedEventHandler>();

            eventBus.AddSubcription<PromotionToPIMPublisher, PromotionCreatedEvent, PromotionCreatedEventHandler>();
            eventBus.AddSubcription<PromotionToPIMPublisher, PromotionDetailCreatedEvent, PromotionDetailCreatedEventHandler>();
            eventBus.AddSubcription<PromotionToPIMPublisher, PromotionConditionCreatedEvent, PromotionConditionCreatedEventHander>();
            eventBus.AddSubcription<PromotionToPIMPublisher, PromotionCouponCodeCreatedEvent, PromotionCouponCodeCreatedEventHandler>();
            eventBus.AddSubcription<StockAndPriceLevelPublisher, StockAndPriceLevelUpdatedEvent, StockAndPriceLevelUpdatedEventHandler>();
            eventBus.Commit();
        }

        public static void ConfigureExceptionHandler(this IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionHandler(errApp =>
            {
                errApp.Run(async context =>
                {
                    var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                    var exception = errorFeature.Error;
                    var hasHandle = false;
                    ProblemDetails result = null;
                    DebugProblemDetails debugResult = null;
                    result = context.RequestServices.GetRequiredService<ForBiddenExceptionHandler>().Handle(exception, ref hasHandle);

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<NotFoundExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<BadModelExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<ArgumentExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<EfUniqueConstraintExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<SqlExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = new ProblemDetails()
                        {
                            Title = "Internal Server Error",
                            Status = (int)HttpStatusCode.InternalServerError,
                            Detail = exception.Message,
                        };

                    }
                    //TODO need to revisit
                    // for now, hack allow all cors when exception occur 
                    context.Response.ContentType = "application/json";
                    context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                    context.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
                    context.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Accept-Encoding, Content-Length, Content-MD5, Date, X-Api-Version, X-File-Name");
                    context.Response.Headers.Add("Access-Control-Allow-Methods", "POST,GET,PUT,PATCH,DELETE,OPTIONS");

                    var logger = context.RequestServices.GetService<ILogger<DebugProblemDetails>>();

                    if (env.IsDevelopment())
                    {
                        debugResult = new DebugProblemDetails(result)
                        {
                            DebugInfo = exception.GetTraceLog()
                        };
                        context.Response.StatusCode = debugResult.Status;
                        var message = JsonConvert.SerializeObject(debugResult, new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                        logger.LogError($" [{Logging.Application.PIM}] {message}");
                        await context.Response.WriteAsync(message);
                    }
                    else
                    {
                        var message = JsonConvert.SerializeObject(result, new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });
                        logger.LogError($" [{Logging.Application.PIM}] {message}");
                        await context.Response.WriteAsync(message);
                    }
                });
            });
        }

        public static void ConfigureMarketingAutomation(this IApplicationBuilder app, IHostingEnvironment env)
        {
            var pimDbContext = app.ApplicationServices.GetRequiredService<TransientPimDbContext>();
            var provisionTask = app.ApplicationServices.GetRequiredService<IProvisionTask<DbProvisionTaskOption>>();
            foreach (var item in pimDbContext.Channels.Where(x => x.IsProvision))
            {
                var option = new DbProvisionTaskOption(item.Name, item.ServerInformation);
                provisionTask.ExecuteAsync(option).Wait();
            };
            var applicationBuilder = app.ApplicationServices.GetRequiredService<ApplicationBuilder>();
            var channelConnectorInstaller = app.ApplicationServices.GetRequiredService<ChannelConnectorInstaller>();
            channelConnectorInstaller.Install(applicationBuilder);
            var marketingAutomationService = app.ApplicationServices.GetRequiredService<MarketingAutomationService>();
            marketingAutomationService.RegisterProductCreated();
            marketingAutomationService.RegisterProductUpdated();
            marketingAutomationService.RegisterProductDeleted();
            marketingAutomationService.RegisterCategoryCreated();
            marketingAutomationService.RegisterCategoryUpdated();
            marketingAutomationService.RegisterVariantCreated();
            marketingAutomationService.RegisterVariantUpdated();
            marketingAutomationService.RegisterFieldValueCreated();
            marketingAutomationService.RegisterFieldValueUpdated();
            marketingAutomationService.RegisterPriceCreated();
            marketingAutomationService.RegisterStockHasTransferedInStore();
        }

        public static void ConfigureJobManager(this IApplicationBuilder app, IHostingEnvironment env)
        {
            GlobalConfiguration.Configuration.UseActivator(new HangfireActivator(app.ApplicationServices));
            app.UseHangfireServer();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new DashboardAuthorizationFilter() }
            });
        }

        public static void ConfigureSignalR(this IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSignalR(routes =>
            {
                routes.MapHub<StockLevelHub>("/stock-level");
                //routes.MapHub<StockAndPriceLevelHub>("/stock-and-price-level");
            });

            var jobManager = app.ApplicationServices.GetRequiredService<IJobManager>();
            jobManager.RegisterRecurringJob<StockLevelJobWorker>(Guid.NewGuid(), "Stock Level Hub Worker", () => { }, new TimeSpan(0, 0, 5), new TimeSpan(0, 1, 0));
            //jobManager.RegisterRecurringJob<StockAndPriceLevelJobWorker>(Guid.NewGuid(), "Stock And Price Level Hub Worker", () => { }, new TimeSpan(0, 0, 5), new TimeSpan(0, 1, 0));
        }
    }
}
