﻿using System;
using IdentityModel;
using System.Linq;
using System.Security.Claims;

namespace Harvey.PIM.API.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static Guid GetUserId(this ClaimsPrincipal user)
        {
            var result = user.Claims.FirstOrDefault(x => x.Type == "sub");
            if (result == null)
            {
                return Guid.Empty;
            }
            return Guid.Parse(result.Value);
        }
        public static string GetUsername(this ClaimsPrincipal user)
        {
            var result = user.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Email);
            if (result == null)
            {
                return Guid.Empty.ToString();
            }
            return result.Value;
        }

    }
}
