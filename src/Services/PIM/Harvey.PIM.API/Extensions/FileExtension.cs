﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Text;

namespace Harvey.PIM.API.Extensions
{
    public static class FileExtension
    {
        public static FileContentResult GetFileUTF8(StringBuilder stringBuilder, string fileName)
        {
            byte[] buffer = Encoding.UTF8.GetPreamble().Concat(Encoding.UTF8.GetBytes(stringBuilder.ToString())).ToArray();
            return GetFileUTF8(buffer, fileName);
        }

        public static FileContentResult GetFileUTF8(byte[] bufferUTF8, string fileName)
        {
            var bom = new byte[] { 0xEF, 0xBB, 0xBF };
            var all = new byte[bom.Length + bufferUTF8.Length];
            Array.Copy(bom, all, bom.Length);
            Array.Copy(bufferUTF8, 0, all, bom.Length, bufferUTF8.Length);
            return new FileContentResult(all, "text/csv")
            {
                FileDownloadName = "FormatedProduct.csv"
            };
        }
    }
}
