﻿using AutoMapper;
using Hangfire;
using Hangfire.PostgreSql;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.EventStore.Marten;
using Harvey.EventBus.RabbitMQ;
using Harvey.EventBus.RabbitMQ.Policies;
using Harvey.Exception.Handlers;
using Harvey.Job;
using Harvey.Job.Hangfire;
using Harvey.Logging;
using Harvey.Logging.SeriLog;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Connectors;
using Harvey.MarketingAutomation.EventHandlers;
using Harvey.NextSequence;
using Harvey.Persitance.EF;
using Harvey.PIM.API.Filters;
using Harvey.PIM.API.Hubs;
using Harvey.PIM.Application.Channels;
using Harvey.PIM.Application.Channels.BarCodes;
using Harvey.PIM.Application.Channels.Categories;
using Harvey.PIM.Application.Channels.FieldValues;
using Harvey.PIM.Application.Channels.PaymentMethods;
using Harvey.PIM.Application.Channels.Prices;
using Harvey.PIM.Application.Channels.Products;
using Harvey.PIM.Application.Channels.Promotions;
using Harvey.PIM.Application.Channels.Promotions.PromotionConditions;
using Harvey.PIM.Application.Channels.Promotions.PromotionCouponCodes;
using Harvey.PIM.Application.Channels.Promotions.PromotionDetails;
using Harvey.PIM.Application.Channels.Services;
using Harvey.PIM.Application.Channels.StockTypes;
using Harvey.PIM.Application.Channels.TransactionTypes;
using Harvey.PIM.Application.Channels.Variants;
using Harvey.PIM.Application.EventHandlers;
using Harvey.PIM.Application.EventHandlers.FieldValues;
using Harvey.PIM.Application.EventHandlers.Products;
using Harvey.PIM.Application.EventHandlers.PurchaseOrders;
using Harvey.PIM.Application.EventHandlers.StockAndPriceLevels;
using Harvey.PIM.Application.EventHandlers.StockLevels;
using Harvey.PIM.Application.EventHandlers.Variants;
using Harvey.PIM.Application.EventHandlers.Vendors;
using Harvey.PIM.Application.Events.Products;
using Harvey.PIM.Application.FieldFramework.Entities;
using Harvey.PIM.Application.FieldFramework.Services.Implementation;
using Harvey.PIM.Application.FieldFramework.Services.Interface;
using Harvey.PIM.Application.Infrastructure;
using Harvey.PIM.Application.Infrastructure.Commands.Allocations;
using Harvey.PIM.Application.Infrastructure.Commands.AllocationTransactions;
using Harvey.PIM.Application.Infrastructure.Commands.Assignments;
using Harvey.PIM.Application.Infrastructure.Commands.AssortmentAssignments;
using Harvey.PIM.Application.Infrastructure.Commands.Assortments;
using Harvey.PIM.Application.Infrastructure.Commands.Brands;
using Harvey.PIM.Application.Infrastructure.Commands.Categories;
using Harvey.PIM.Application.Infrastructure.Commands.Channels;
using Harvey.PIM.Application.Infrastructure.Commands.Fields;
using Harvey.PIM.Application.Infrastructure.Commands.GIWs;
using Harvey.PIM.Application.Infrastructure.Commands.GoodsReturns;
using Harvey.PIM.Application.Infrastructure.Commands.Locations;
using Harvey.PIM.Application.Infrastructure.Commands.Products;
using Harvey.PIM.Application.Infrastructure.Commands.Reasons;
using Harvey.PIM.Application.Infrastructure.Commands.StockRequests;
using Harvey.PIM.Application.Infrastructure.Commands.StockTransactions;
using Harvey.PIM.Application.Infrastructure.Commands.TransferIn;
using Harvey.PIM.Application.Infrastructure.Commands.TransferOut;
using Harvey.PIM.Application.Infrastructure.Domain;
using Harvey.PIM.Application.Infrastructure.Domain.Catalog;
using Harvey.PIM.Application.Infrastructure.Models;
using Harvey.PIM.Application.Infrastructure.Models.Feeds;
using Harvey.PIM.Application.Infrastructure.Provisions;
using Harvey.PIM.Application.Infrastructure.Queries.AllocationTransactions;
using Harvey.PIM.Application.Infrastructure.Queries.Assignments;
using Harvey.PIM.Application.Infrastructure.Queries.Assortments;
using Harvey.PIM.Application.Infrastructure.Queries.BarCodes;
using Harvey.PIM.Application.Infrastructure.Queries.Brands;
using Harvey.PIM.Application.Infrastructure.Queries.Categories;
using Harvey.PIM.Application.Infrastructure.Queries.Channels;
using Harvey.PIM.Application.Infrastructure.Queries.Dashboard;
using Harvey.PIM.Application.Infrastructure.Queries.Fields;
using Harvey.PIM.Application.Infrastructure.Queries.GIWs;
using Harvey.PIM.Application.Infrastructure.Queries.GoodsReturns;
using Harvey.PIM.Application.Infrastructure.Queries.Locations;
using Harvey.PIM.Application.Infrastructure.Queries.Products;
using Harvey.PIM.Application.Infrastructure.Queries.PurchaseOrders;
using Harvey.PIM.Application.Infrastructure.Queries.Reasons;
using Harvey.PIM.Application.Infrastructure.Queries.ReturnOrders;
using Harvey.PIM.Application.Infrastructure.Queries.StockAndPrice;
using Harvey.PIM.Application.Infrastructure.Queries.StockRequests;
using Harvey.PIM.Application.Infrastructure.Queries.StockTransactions;
using Harvey.PIM.Application.Infrastructure.Queries.TransferIn;
using Harvey.PIM.Application.Infrastructure.Queries.TransferOut;
using Harvey.PIM.Application.Infrastructure.Queries.Vendors;
using Harvey.PIM.Application.Services;
using Harvey.Reporting;
using Harvey.Search.Abstractions;
using Harvey.Search.NEST;
using Harvey.Setting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using static Harvey.PIM.Application.Infrastructure.Queries.Channels.GetChannelStoreAssignmentsQuery;
using static Harvey.PIM.Application.Infrastructure.Queries.Locations.SearchLocationsQuery;

namespace Harvey.PIM.API.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IEventStore>(sp =>
            {
                return new MartenEventStore(configuration["ConnectionString"]);
            });
            services.AddTransient<LoggingEventHandler>();
            services.AddTransient<ProductCreatedEventHandler>();
            services.AddTransient<ProductUpdatedEventHandler>();
            services.AddTransient<ProductDeletedEventHandler>();
            services.AddTransient<ProductFieldValueCreatedEventHandler>();
            services.AddTransient<ProductFieldValueUpdatedEventHandler>();
            services.AddTransient<VariantCreatedEventHandler>();
            services.AddTransient<VariantUpdatedEventHandler>();
            services.AddTransient<PriceCreatedEventHandler>();
            services.AddTransient<ProductCreatedIndexingEventHandler>();
            services.AddTransient<ProductUpdatedIndexingEventHandler>();
            services.AddTransient<ProductDeletedIndexingEventHandler>();
            services.AddTransient<StockLevelUpdatedEventHandler>();
            services.AddTransient<SynchronizationCreatedEventHandler>();
            services.AddTransient<VendorCreatedEventHandler>();
            services.AddTransient<VendorUpdatedEventHandler>();
            services.AddTransient<VendorDeletedEventHandler>();
            services.AddTransient<PurchaseOrderConfirmStatusEventHandler>();
            services.AddTransient<PurchaseOrderRejectStatusEventHandler>();
            services.AddTransient<PurchaseOrderDeletedEventHandler>();
            services.AddTransient<PromotionCreatedEventHandler>();
            services.AddTransient<PromotionDetailCreatedEventHandler>();
            services.AddTransient<PromotionCouponCodeCreatedEventHandler>();
            services.AddTransient<PromotionConditionCreatedEventHander>();
            services.AddTransient<StockAndPriceLevelUpdatedEventHandler>();
            services.AddSingleton(sp =>
            {
                var logger = sp.GetRequiredService<ILogger<MasstransitPersistanceConnection>>();
                var endPoint = configuration["RabbitMqConfig:RabbitMqUrl"];
                var id = configuration["RabbitMqConfig:Username"];
                var pass = configuration["RabbitMqConfig:Password"];
                return new MasstransitPersistanceConnection(new BusCreationRetrivalPolicy(), logger, endPoint, id, pass);
            });
            services.AddSingleton<IEventBus>(sp =>
            {
                return new MasstransitEventBus("Harvey_PIM_API", sp.GetRequiredService<MasstransitPersistanceConnection>(), sp);
            });

            services.AddDbContext<IdentifiedEventDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddTransient<IRepository<IdentifiedEvent>, EfRepository<IdentifiedEventDbContext, IdentifiedEvent>>();
            return services;
        }

        public static IServiceCollection AddServiceDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<PimDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddDbContext<TransientPimDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddDbContext<ActivityLogDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddDbContext<TransactionDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            });

            services.AddDbContext<TransientTransactionDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            return services;
        }

        public static IServiceCollection AddCQRS(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ICommandExecutor, CommandExecutor>();
            services.AddTransient<IQueryExecutor, QueryExecutor>();

            services.AddTransient<ICommandHandler<AddCategoryCommand, CategoryModel>, AddCategoryCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateCategoryCommand, CategoryModel>, UpdateCategoryCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteCategoryCommand, bool>, DeleteCategoryCommandHandler>();
            services.AddTransient<IQueryHandler<GetCategoriesQuery, PagedResult<CategoryModel>>, GetCategoriesQueryHandler>();
            services.AddTransient<IQueryHandler<GetAllCategoriesQuery, IEnumerable<CategoryModel>>, GetAllCategoriesQueryHandler>();
            services.AddTransient<IQueryHandler<GetCategoryByIdQuery, CategoryModel>, GetCategoryByIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetCategoryIdsByProductIdsQuery, GetCategoryIdsByProdutIdsReponse>, GetCategoryIdsByProductIdsQueryHandler>();

            services.AddTransient<IQueryHandler<GetFieldByIdQuery, FieldModel>, GetFieldByIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetFieldsQuery, PagedResult<FieldModel>>, GetFieldsQueryHandler>();
            services.AddTransient<IQueryHandler<GetAllFieldsQuery, IEnumerable<FieldModel>>, GetAllFieldsQueryHandler>();
            services.AddTransient<ICommandHandler<AddFieldCommand, FieldModel>, AddFieldCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateFieldCommand, FieldModel>, UpdateFieldCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteFieldCommand, bool>, DeleteFieldCommandHandler>();
            services.AddTransient<IQueryHandler<GetFieldByIdQuery, FieldModel>, GetFieldByIdQueryHandler>();
            services.AddTransient<ICommandHandler<AddAssortmentCommand, AssortmentModel>, AddAssortmentCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateAssortmentCommand, AssortmentModel>, UpdateAssortmentCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteAssortmentCommand, bool>, DeleteAssortmentCommandHandler>();
            services.AddTransient<IQueryHandler<GetAssortmentsQuery, PagedResult<AssortmentModel>>, GetAssortmentsQueryHandler>();
            services.AddTransient<IQueryHandler<GetAssortmentByIdQuery, AssortmentModel>, GetAssortmentByIdQueryHandler>();

            services.AddTransient<ICommandHandler<AddBrandCommand, BrandModel>, AddBrandCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateBrandCommand, BrandModel>, UpdateBrandCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteBrandCommand, bool>, DeleteBrandCommandHandler>();
            services.AddTransient<IQueryHandler<GetBrandsQuery, PagedResult<BrandModel>>, GetBrandsQueryHandler>();
            services.AddTransient<IQueryHandler<GetBrandByIdQuery, BrandModel>, GetBrandByIdQueryHandler>();


            services.AddTransient<IQueryHandler<GetLocationsQuery, PagedResult<LocationModel>>, GetLocationsQueryHandler>();
            services.AddTransient<IQueryHandler<GetLocationByIdQuery, LocationModel>, GetLocationByIdQueryHandler>();
            services.AddTransient<IQueryHandler<SearchLocationsQuery, IEnumerable<LocationModel>>, SearchLocationsQueryHandler>();
            services.AddTransient<ICommandHandler<AddLocationCommand, LocationModel>, AddLocationCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateLocationCommand, LocationModel>, UpdateLocationCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteLocationCommand, LocationModel>, DeleteLocationCommandHandler>();

            services.AddTransient<IQueryHandler<GetProductFromTemplateIdQuery, ProductModel>, GetProductFromTemplateIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetProductListQuery, PagedResult<ProductListModel>>, GetProductListQueryHandler>();
            services.AddTransient<IQueryHandler<GetProductWithSearchQuery, List<ProductListModel>>, GetProductWithSearchQueryHandler>();
            services.AddTransient<IQueryHandler<SearchVariantsForPrintLabelQuery, ProductForPrintLabelResponse>, SearchVariantsForPrintLabelQueryHandler>();
            services.AddTransient<IQueryHandler<GetProductListWithoutPagingQuery, IEnumerable<ProductListModel>>, GetProductListWithoutPagingQueryHandler>();
            services.AddTransient<IQueryHandler<GetProductListPagingForRebuildIndexQuery, IEnumerable<ProductListModel>>, GetProductListPagingForRebuildIndexQueryHandler>();
            services.AddTransient<IQueryHandler<GetProductFieldValue, IEnumerable<ProductFeed>>, GetProductFieldValueHandler>();
            services.AddTransient<IQueryHandler<GetProductByIdQuery, ProductModel>, GetProductByIdQueryHandler>();
            services.AddTransient<ICommandHandler<AddProductCommand, Product>, AddProductCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateProductCommand, Product>, UpdateProductCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteProductCommand, bool>, DeleteProductCommandHandler>();

            services.AddTransient<IQueryHandler<GetChannelsQuery, PagedResult<ChannelModel>>, GetChannelsQueryHandler>();
            services.AddTransient<IQueryHandler<GetChannelByIdQuery, ChannelModel>, GetChannelByIdQueryHandler>();
            services.AddTransient<ICommandHandler<AddChannelCommand, ChannelModel>, AddChannelCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateChannelCommand, ChannelModel>, UpdateChannelCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteChannelCommand, bool>, DeleteChannelCommandHandler>();
            services.AddTransient<ICommandHandler<CheckServerInfomation, bool>, CheckServerInfomationHandler>();

            services.AddTransient<IQueryHandler<GetAssortmentAssignmentByName, List<AssortmentAssignmentModel>>, GetAssortmentAssignmentByNameHandler>();
            services.AddTransient<IQueryHandler<GetAssortmentAssignmentSelected, List<AssortmentAssignmentModel>>, GetAssortmentAssignmentSelectedHandler>();
            services.AddTransient<IQueryHandler<GetAllAssortmentAssignment, List<AssortmentAssignmentModel>>, GetAllAssortmentAssignmentHandler>();
            services.AddTransient<ICommandHandler<AddAssortmentAssignmentCommand, bool>, AddAssortmentAssignmentCommandHandler>();

            services.AddTransient<IQueryHandler<GetChannelAssignmentByName, List<ChannelAssignmentModel>>, GetChannelAssignmentByNameHandler>();
            services.AddTransient<IQueryHandler<GetChannelAssignmentSelected, List<ChannelAssignmentModel>>, GetChannelAssignmentSelectedHandler>();
            services.AddTransient<IQueryHandler<GetAllChannelAssignment, List<ChannelAssignmentModel>>, GetAllChannelAssignmentHandler>();
            services.AddTransient<ICommandHandler<AddChannelAssignmentCommand, bool>, AddChannelAssignmentCommandHandler>();

            services.AddTransient<IQueryHandler<GetChannelStoreAssignmentsQuery, List<ChannelStoreAssignmentModel>>, GetChannelStoreAssignmentsQueryHandler>();
            services.AddTransient<ICommandHandler<AddChannelStoreAssignmentCommand, bool>, AddChannelStoreAssignmentCommandHandler>();

            services.AddTransient<IQueryHandler<GetLocationsByTypeQuery, IEnumerable<LocationModel>>, GetLocationByTypeQueryHandler>();

            services.AddTransient<ICommandHandler<AddReasonCommand, ReasonModel>, AddReasonCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateReasonCommand, ReasonModel>, UpdateReasonCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteReasonCommand, bool>, DeleteReasonCommandHandler>();
            services.AddTransient<IQueryHandler<GetReasonsQuery, PagedResult<ReasonModel>>, GetReasonsQueryHandler>();
            services.AddTransient<IQueryHandler<GetReasonByIdQuery, ReasonModel>, GetReasonByIdQueryHandler>();

            services.AddTransient<ICommandHandler<AddStockRequestCommand, StockRequestListModel>, AddStockRequestCommandHandler>();
            services.AddTransient<IQueryHandler<GetStockRequestListQuery, PagedResult<StockRequestListModel>>, GetStockRequestListQueryHandler>();
            services.AddTransient<IQueryHandler<GetStockRequestByIdQuery, StockRequestModel>, GetStockRequestByIdQueryHandler>();

            services.AddTransient<ICommandHandler<AddAllocationTransactionCommand, AllocationTransactionModel>, AddAllocationTransactionCommandHandler>();
            services.AddTransient<ICommandHandler<AddMultiAllocationTransactionCommand, bool>, AddMultiAllocationTransactionCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateAllocationTransactionCommand, AllocationTransactionModel>, UpdateAllocationTransactionCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateStatusAllocationTransactionCommand, UpdateStatusAllocationTransactionModel>, UpdateStatusAllocationTransactionCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteAllocationTransactionCommand, bool>, DeleteAllocationTransactionCommandHandler>();
            services.AddTransient<IQueryHandler<GetAllocationTransactionsQuery, PagedResult<AllocationTransactionModel>>, GetAllocationTransactionsQueryHandler>();
            services.AddTransient<IQueryHandler<GetAllocationTransactionByIdQuery, AllocationTransactionModel>, GetAllocationTransactionByIdQueryHandler>();
            services.AddTransient<ICommandHandler<GetAllocationTransactionByListIdQuery, AllocationTransactionByListIdModel>, GetAllocationTransactionByListIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetMassAllocationTransationQuery, MassAllocationInfoModel>, GetMassAllocationTransationQueryHandler>();
            services.AddTransient<IQueryHandler<GetMassAllocationByDate, List<OutletProductModel>>, GetMassAllocationByDateHandler>();

            services.AddTransient<IQueryHandler<GetAllocationTransactionsByLocationQuery, PagedResult<AllocationTransactionModel>>, GetAllocationTransactionByLocationQueryHandler>();
            services.AddTransient<ICommandHandler<UpdateStockRequestCommand, StockRequestListModel>, UpdateStockRequestCommandHandler>();
            services.AddTransient<ICommandHandler<AddGIWCommand, Guid>, AddGIWCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateGIWStatusCommand, bool>, UpdateGIWStatusCommandHandler>();
            services.AddTransient<IQueryHandler<GetGIWsQuery, PagedResult<InventoryTransactionTransferViewModel>>, GetGIWsQueryHandler>();
            services.AddTransient<IQueryHandler<GetStockTransactionsByPurchaseOrdersQuery, List<StockTransaction>>, GetStockTransactionsByPurchaseOrdersQueryHandler>();

            services.AddTransient<ICommandHandler<AddTransferInCommand, bool>, AddTransferInCommandHandler>();
            services.AddTransient<ICommandHandler<AddTransferOutCommand, bool>, AddTransferOutCommandHandler>();
            services.AddTransient<IQueryHandler<GetTransferOutsByLocationQuery, PagedResult<InventoryTransactionTransferViewModel>>, GetTransferOutsByLocationQueryHandler>();
            services.AddTransient<IQueryHandler<GetTransferOutsQuery, PagedResult<InventoryTransactionTransferViewModel>>, GetTransferOutsQueryHandler>();
            services.AddTransient<IQueryHandler<GetTransferOutByIdQuery, InventoryTransactionTransferViewModel>, GetTransferOutByIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetTransferInByIdQuery, InventoryTransactionTransferViewModel>, GetTransferInByIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetTransferInsQuery, PagedResult<InventoryTransactionTransferViewModel>>, GetTransferInsQueryHandler>();
            services.AddTransient<IQueryHandler<GetTransferOutProductStockOnHandQuery, int>, GetTransferOutProductStockOnHandQueryHandler>();

            services.AddTransient<IQueryHandler<GetAllVendorsQuery, List<VendorModel>>, GetAllVendorsQueryHandler>();
            services.AddTransient<IQueryHandler<GetPurchaseOrdersByVendorQuery, List<PurchaseOrderListModel>>, GetPurchaseOrdersByVendorQueryHandler>();
            services.AddTransient<IQueryHandler<GetPurchaseOrdersByIdsQuery, List<PurchaseOrderModel>>, GetPurchaseOrdersByIdsQueryHandler>();

            services.AddTransient<IQueryHandler<StockTransactionsExportCSVQuery, byte[]>, StockTransactionsExportCSVQueryHandler>();
            services.AddTransient<ICommandHandler<GenerateBarCodeCommand, List<BarCode>>, GenerateBarCodeCommandHandler>();

            services.AddTransient<ICommandHandler<AddGoodsReturnCommand, Guid>, AddGoodsReturnCommandHandler>();
            services.AddTransient<IQueryHandler<GetGoodsReturnsQuery, PagedResult<InventoryTransactionModel>>, GetGoodsReturnsQueryHandler>();
            services.AddTransient<IQueryHandler<GetReturnOrdersByVendorQuery, List<ReturnOrderListModel>>, GetReturnOrdersByVendorQueryHandler>();
            services.AddTransient<IQueryHandler<GetReturnOrdersByIdsQuery, List<ReturnOrderModel>>, GetReturnOrdersByIdsQueryHandler>();
            services.AddTransient<IQueryHandler<GetStockTransactionsByReturnOrdersQuery, List<StockTransaction>>, GetStockTransactionsByReturnOrdersQueryHandler>();
            services.AddTransient<ICommandHandler<StockTransactionUpdatedFromRetailsCommand, List<StockTransactionUpdatedRetailsModel>>, StockTransactionUpdatedFromRetailsCommandHandler>();

            services.AddTransient<IQueryHandler<GetStocksByLocationQuery, DashboardModel>, GetStocksByLocationQueryHandler>();

            services.AddTransient<IQueryHandler<GetBarCodesByProductQuery, BarCodeProductViewModel>, GetBarCodesByProductQueryHandler>();

            services.AddTransient<IQueryHandler<GetCountQuery, CountModel>, GetCountQueryHandler>();
            services.AddTransient<IQueryHandler<GetDetailsGIWByInventoryTransactionIdQuery, InventoryTransactionTransferViewModel>, GetDetailsGIWByInventoryTransactionIdQueryHandler>();

            services.AddTransient<IQueryHandler<GetProductsPageStockAndPriceQuery, PagedResult<StockAndPriceProductModel>>, GetProductsPageStockAndPriceQueryHandler>();
            services.AddTransient<IQueryHandler<GetStockAndPriceStoreByProductQuery, StockAndPriceItemModel>, GetStockAndPriceStoreByProductQueryHandler>();
            services.AddTransient<IQueryHandler<GetTopProductBySalesQuery, List<TopProductSaleModel>>, GetTopProductBySalesQueryHandler>();
            services.AddTransient<IQueryHandler<GetPurchaseOrderSummaryQuery, List<PurchaseOrderSummaryModel>>, GetPurchaseOrderSummaryQueryHandler>();
            services.AddTransient<IQueryHandler<GetNewProductsQuery, PagedResult<NewProductModel>>, GetNewProductsQueryHandler>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<EfUnitOfWork>();
            services.AddScoped<IEfRepository<PimDbContext, Category, CategoryModel>, EfRepository<PimDbContext, Category, CategoryModel>>();
            services.AddScoped<IEfRepository<PimDbContext, Category>, EfRepository<PimDbContext, Category>>();

            services.AddScoped<IEfRepository<PimDbContext, Field>, EfRepository<PimDbContext, Field>>();
            services.AddScoped<IEfRepository<PimDbContext, Field, FieldModel>, EfRepository<PimDbContext, Field, FieldModel>>();
            services.AddScoped<IEfRepository<PimDbContext, Assortment>, EfRepository<PimDbContext, Assortment>>();
            services.AddScoped<IEfRepository<PimDbContext, Assortment, AssortmentModel>, EfRepository<PimDbContext, Assortment, AssortmentModel>>();

            services.AddScoped<IEfRepository<PimDbContext, Brand>, EfRepository<PimDbContext, Brand>>();
            services.AddScoped<IEfRepository<PimDbContext, Brand, BrandModel>, EfRepository<PimDbContext, Brand, BrandModel>>();

            services.AddScoped<IEfRepository<PimDbContext, Location, LocationModel>, EfRepository<PimDbContext, Location, LocationModel>>();
            services.AddScoped<IEfRepository<PimDbContext, Location>, EfRepository<PimDbContext, Location>>();

            services.AddScoped<IEventStoreRepository<Product>, EvenStoreRepository<Product>>();
            services.AddScoped<IEfRepository<PimDbContext, Product>, EfRepository<PimDbContext, Product>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, Product>, EfRepository<TransientPimDbContext, Product>>();
            services.AddScoped<IEfRepository<PimDbContext, Product, ProductListModel>, EfRepository<PimDbContext, Product, ProductListModel>>();
            services.AddScoped<IEfRepository<PimDbContext, FieldTemplate>, EfRepository<PimDbContext, FieldTemplate>>();
            services.AddScoped<IEfRepository<PimDbContext, FieldValue>, EfRepository<PimDbContext, FieldValue>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, FieldValue>, EfRepository<TransientPimDbContext, FieldValue>>();
            services.AddScoped<IEfRepository<PimDbContext, Variant>, EfRepository<PimDbContext, Variant>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, Variant>, EfRepository<TransientPimDbContext, Variant>>();
            services.AddScoped<IEfRepository<PimDbContext, Price>, EfRepository<PimDbContext, Price>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, Price>, EfRepository<TransientPimDbContext, Price>>();
            services.AddScoped<IEfRepository<PimDbContext, Price, PriceModel>, EfRepository<PimDbContext, Price, PriceModel>>();

            services.AddScoped<IEfRepository<PimDbContext, Channel, ChannelModel>, EfRepository<PimDbContext, Channel, ChannelModel>>();
            services.AddScoped<IEfRepository<PimDbContext, Channel>, EfRepository<PimDbContext, Channel>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, Channel>, EfRepository<TransientPimDbContext, Channel>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, Category>, EfRepository<TransientPimDbContext, Category>>();

            services.AddScoped<IEfRepository<PimDbContext, AssortmentAssignment>, EfRepository<PimDbContext, AssortmentAssignment>>();

            services.AddScoped<IEfRepository<PimDbContext, ChannelAssignment>, EfRepository<PimDbContext, ChannelAssignment>>();
            services.AddScoped<IEfRepository<PimDbContext, ChannelStoreAssignment>, EfRepository<PimDbContext, ChannelStoreAssignment>>();

            services.AddScoped<IEfRepository<TransactionDbContext, StockType>, EfRepository<TransactionDbContext, StockType>>();

            services.AddScoped<IEfRepository<TransactionDbContext, Reason>, EfRepository<TransactionDbContext, Reason>>();
            services.AddScoped<IEfRepository<TransactionDbContext, Reason, ReasonModel>, EfRepository<TransactionDbContext, Reason, ReasonModel>>();

            services.AddScoped<IEfRepository<TransactionDbContext, StockRequest>, EfRepository<TransactionDbContext, StockRequest>>();
            services.AddScoped<IEfRepository<TransactionDbContext, StockRequestItem>, EfRepository<TransactionDbContext, StockRequestItem>>();

            services.AddScoped<IEfRepository<TransactionDbContext, AllocationTransaction>, EfRepository<TransactionDbContext, AllocationTransaction>>();
            services.AddScoped<IEfRepository<TransactionDbContext, AllocationTransaction, AllocationTransactionModel>, EfRepository<TransactionDbContext, AllocationTransaction, AllocationTransactionModel>>();

            services.AddScoped<IEfRepository<TransactionDbContext, AllocationTransactionDetail>, EfRepository<TransactionDbContext, AllocationTransactionDetail>>();
            services.AddScoped<IEfRepository<TransactionDbContext, AllocationTransactionDetail, AllocationTransactionDetailModel>, EfRepository<TransactionDbContext, AllocationTransactionDetail, AllocationTransactionDetailModel>>();

            services.AddScoped<IEfRepository<TransactionDbContext, InventoryTransaction>, EfRepository<TransactionDbContext, InventoryTransaction>>();
            services.AddScoped<IEfRepository<TransactionDbContext, StockTransaction>, EfRepository<TransactionDbContext, StockTransaction>>();
            services.AddScoped<IEfRepository<TransactionDbContext, TransactionType>, EfRepository<TransactionDbContext, TransactionType>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, Vendor>, EfRepository<TransientPimDbContext, Vendor>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, PurchaseOrder>, EfRepository<TransientPimDbContext, PurchaseOrder>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, PurchaseOrderItem>, EfRepository<TransientPimDbContext, PurchaseOrderItem>>();
            services.AddScoped<IEfRepository<PimDbContext, PurchaseOrder>, EfRepository<PimDbContext, PurchaseOrder>>();
            services.AddScoped<IEfRepository<PimDbContext, PurchaseOrderItem>, EfRepository<PimDbContext, PurchaseOrderItem>>();

            services.AddTransient<IEfRepository<PimDbContext, Vendor>, EfRepository<PimDbContext, Vendor>>();
            services.AddTransient<IEfRepository<PimDbContext, PurchaseOrder>, EfRepository<PimDbContext, PurchaseOrder>>();
            services.AddTransient<IEfRepository<PimDbContext, PurchaseOrderItem>, EfRepository<PimDbContext, PurchaseOrderItem>>();

            services.AddTransient<IEfRepository<TransactionDbContext, GIWDocument>, EfRepository<TransactionDbContext, GIWDocument>>();
            services.AddTransient<IEfRepository<TransactionDbContext, GIWDocumentItem>, EfRepository<TransactionDbContext, GIWDocumentItem>>();

            services.AddTransient<IEfRepository<TransientPimDbContext, CatalogPromotion>, EfRepository<TransientPimDbContext, CatalogPromotion>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, CatalogPromotionDetail>, EfRepository<TransientPimDbContext, CatalogPromotionDetail>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, CatalogPromotionCouponCode>, EfRepository<TransientPimDbContext, CatalogPromotionCouponCode>>();
            services.AddTransient<IEfRepository<TransientPimDbContext, CatalogPromotionCondition>, EfRepository<TransientPimDbContext, CatalogPromotionCondition>>();
            return services;
        }

        public static IServiceCollection AddTrackingActivity(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ActivityTracking>();
            services.AddTransient<IEfRepository<ActivityLogDbContext, ActivityLog>, EfRepository<ActivityLogDbContext, ActivityLog>>();
            return services;
        }

        public static IServiceCollection AddLogger(this IServiceCollection services, IConfiguration configuration)
        {
            new SeriLogger().Initilize(new List<IDatabaseLoggingConfiguration>()
            {
                new DatabaseLoggingConfiguration()
                {
                    ConnectionString = configuration["Logging:DataLogger:ConnectionString"],
                    TableName = configuration["Logging:DataLogger:TableName"],
                    LogLevel = (LogLevel)Enum.Parse( typeof(LogLevel), configuration["Logging:DataLogger:LogLevel"],true)
                }
            }, new List<ICentralizeLoggingConfiguration>() {
                new CentralizeLoggingConfiguration()
                {
                    Url = configuration["Logging:CentralizeLogger:Url"],
                    LogLevel = (LogLevel)Enum.Parse( typeof(LogLevel), configuration["Logging:CentralizeLogger:LogLevel"],true)
                }
            });
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper();
            return services;
        }

        public static IServiceCollection AddFieldFramework(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IFieldService, FieldService>();
            services.AddTransient<IFieldTemplateService, FieldTemplateService>();
            services.AddTransient<IFieldValueService, FieldValueService>();
            services.AddTransient<IEntityRefService, EntityRefService>();
            services.AddTransient<IEntityRefValueService<Brand>, BrandEntityRefValueService>();
            services.AddTransient<IFieldTemplateService, FieldTemplateService>();
            return services;
        }

        public static IServiceCollection AddAppSetting(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAppSettingService, AppSettingService>();
            return services;
        }

        public static IServiceCollection AddNextSequence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<INextSequenceService<TransientPimDbContext>, NextSequenceService<TransientPimDbContext>>();
            return services;
        }

        public static IServiceCollection AddAssignmentServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAssignmentService, AssignmentService>();
            return services;
        }

        public static IServiceCollection AddProvisionTask(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IProvisionTask<DbProvisionTaskOption>, DbProvisionTask>();
            return services;
        }

        public static IServiceCollection AddExceptionHandlers(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ArgumentExceptionHandler>();
            services.AddScoped<EfUniqueConstraintExceptionHandler>();
            services.AddScoped<SqlExceptionHandler>();
            services.AddScoped<ForBiddenExceptionHandler>();
            services.AddScoped<NotFoundExceptionHandler>();
            services.AddScoped<BadModelExceptionHandler>();
            return services;
        }

        public static IServiceCollection AddMarketingAutomation(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<MarketingAutomationService>();
            services.AddSingleton<ApplicationBuilder>();
            services.AddSingleton<ConnectorInfoCollection>();
            services.AddTransient<ChannelConnectorInstaller>();
            services.AddSingleton<FeedWorker>();
            services.AddTransient<ConnectorIntergrationEventHandler>();
            services.AddTransient<MarketingProductCreatedEventHandler>();
            services.AddTransient<MarketingProductUpdatedEventHandler>();
            services.AddTransient<MarketingProductDeletedEventHandler>();
            services.AddTransient<MarketingCategoryCreatedEventHandler>();
            services.AddTransient<MarketingCategoryUpdatedEventHandler>();
            services.AddTransient<MarketingVariantCreatedEventHandler>();
            services.AddTransient<MarketingVariantUpdatedEventHandler>();
            services.AddTransient<MarketingFieldValueCreatedEventHandler>();
            services.AddTransient<MarketingFieldValueUpdatedEventHandler>();
            services.AddTransient<MarketingPriceCreatedEventHandler>();
            services.AddTransient<MarketingStockHasTransferedInStoreEventHandler>();

            services.AddTransient<ChannelProductCreatedEventHandler>();
            services.AddTransient<ChannelProductUpdatedEventHandler>();
            services.AddTransient<ChannelProductDeletedEventHandler>();
            services.AddTransient<ChannelProductFetcher>();
            services.AddTransient<ChannelProductFilter>();
            services.AddTransient<ChannelProductConveter>();
            services.AddTransient<ChannelProductSerializer>();

            services.AddTransient<ChannelVariantCreatedEventHandler>();
            services.AddTransient<ChannelVariantUpdatedEventHandler>();
            services.AddTransient<ChannelVariantFetcher>();
            services.AddTransient<ChannelVariantFilter>();
            services.AddTransient<ChannelVariantConveter>();
            services.AddTransient<ChannelVariantSerializer>();

            services.AddTransient<ChannelCategoryCreatedEventHandler>();
            services.AddTransient<ChannelCategoryUpdatedEventHandler>();
            services.AddTransient<ChannelCategoryFetcher>();
            services.AddTransient<ChannelCategoryFilter>();
            services.AddTransient<ChannelCategoryConveter>();
            services.AddTransient<ChannelCategorySerializer>();

            services.AddTransient<ChannelFieldValueCreatedEventHandler>();
            services.AddTransient<ChannelFieldValueUpdatedEventHandler>();

            services.AddTransient<ChannelPriceCreatedEventHandler>();
            services.AddTransient<ChannelPriceFetcher>();
            services.AddTransient<ChannelPriceFilter>();
            services.AddTransient<ChannelPriceConveter>();
            services.AddTransient<ChannelPriceSerializer>();

            services.AddTransient<ChannelStockTypeFetcher>();
            services.AddTransient<ChannelStockTypeFilter>();
            services.AddTransient<ChannelStockTypeConverter>();
            services.AddTransient<ChannelStockTypeSerializer>();

            services.AddTransient<ChannelTransactionTypeFetcher>();
            services.AddTransient<ChannelTransactionTypeFilter>();
            services.AddTransient<ChannelTransactionTypeConverter>();
            services.AddTransient<ChannelTransactionTypeSerializer>();
            services.AddTransient<ChannelStockHasTransferedInStoreEventHandler>();

            services.AddTransient<ChannelBarCodeFetcher>();
            services.AddTransient<ChannelBarCodeFilter>();
            services.AddTransient<ChannelBarCodeConveter>();
            services.AddTransient<ChannelBarCodeSerializer>();

            services.AddTransient<ChannelPaymentMethodFetcher>();
            services.AddTransient<ChannelPaymentMethodFilter>();
            services.AddTransient<ChannelPaymentMethodConveter>();
            services.AddTransient<ChannelPaymentMethodSerializer>();

            return services;
        }

        public static IServiceCollection AddJobManager(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IJobManager, HangfireJobManager>();
            services.AddHangfire(options => options.UseStorage(new PostgreSqlStorage(configuration["ConnectionString"]))
            .UseColouredConsoleLogProvider());
            return services;
        }

        public static IServiceCollection AddSearchService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ISearchService, SearchService>();
            services.AddTransient<SearchSettings>(sp =>
            {
                return new SearchSettings(configuration["ELASTICSEARCHURL"]);
            });
            return services;
        }

        public static IServiceCollection AddSignalR(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<StockLevelJobWorker>();
            services.AddSingleton<StockAndPriceLevelJobWorker>();
            services.AddSignalR();
            return services;
        }

        public static IServiceCollection AddReportingServer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(x => new ReportServer(configuration["ReportServer"]));
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ProductService>();
            services.AddTransient<DeploymentService>();
            services.AddTransient<VariantService>();
            services.AddTransient<DeploymentStockTransactionService>();
            services.AddTransient<InventoryTransactionService>();
            services.AddTransient<SalePerformanceService>();
            services.AddTransient<StockTransactionService>();
            services.AddTransient<DeploymentBrandService>();
            services.AddTransient<FormatFileService>();
            services.AddTransient<StockRequestService>();
            services.AddTransient<StockAllocationService>();
            services.AddTransient<FeedService>();
            return services;
        }
    }
}
