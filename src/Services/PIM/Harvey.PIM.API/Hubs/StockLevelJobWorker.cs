﻿using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.StockLevels;
using Harvey.Job;
using Harvey.PIM.Application.Infrastructure.Indexing;
using Harvey.Search.Abstractions;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.PIM.API.Hubs
{
    public class StockLevelJobWorker : IWorker
    {
        private readonly IEventBus _eventBus;
        public StockLevelJobWorker(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }
        public void Execute(Guid correlationId, string jobName)
        {
            _eventBus.PublishAsync(new StockLevelUpdatedEvent());
        }
    }
}