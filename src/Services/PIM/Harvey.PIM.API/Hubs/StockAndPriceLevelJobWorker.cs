﻿using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.StockAndPriceLevels;
using Harvey.Job;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.PIM.API.Hubs
{
    public class StockAndPriceLevelJobWorker : IWorker
    {
        private readonly IEventBus _eventBus;
        public StockAndPriceLevelJobWorker(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }
        public void Execute(Guid correlationId, string jobName)
        {
            _eventBus.PublishAsync(new StockAndPriceLevelUpdatedEvent());
        }
    }
}
