﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Events.Orders;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers
{
    public sealed class CaculateGrandTotalEventHandler : EventHandlerBase<CaculateGrandTotalEvent>
    {
        private CatalogContext _catalogContext;
        public CaculateGrandTotalEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<CaculateGrandTotalEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(CaculateGrandTotalEvent @event)
        {
            _catalogContext = new CatalogContext(@event.ChannelContext);
            await _catalogContext.CaculateGrandTotal(@event.OrderId, @event.Cart);
        }
    }
}
