﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Events.Orders;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers
{
    public sealed class ApplyPromotionEventHandler : EventHandlerBase<ApplyPromotionEvent>
    {
        private CatalogContext _catalogContext;
        public ApplyPromotionEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<ApplyPromotionEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(ApplyPromotionEvent @event)
        {
            _catalogContext = new CatalogContext(@event.ChannelContext);
            await _catalogContext.ApplyPromotion(@event.OrderId, @event.Cart);
        }
    }
}
