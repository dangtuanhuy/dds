﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Events.Orders;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers.Orders
{
    public sealed class CaculateSubTotalEventHandler : EventHandlerBase<CaculateSubTotalEvent>
    {
        private CatalogContext _catalogContext;
        public CaculateSubTotalEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<CaculateSubTotalEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(CaculateSubTotalEvent @event)
        {
            _catalogContext = new CatalogContext(@event.ChannelContext);
            await _catalogContext.CaculateSubTotal(@event.OrderId, @event.Cart);
        }
    }
}
