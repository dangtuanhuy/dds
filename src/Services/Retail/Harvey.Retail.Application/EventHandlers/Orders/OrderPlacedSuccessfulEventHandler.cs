﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Events;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers.Orders
{
    public class OrderPlacedSuccessfulEventHandler : EventHandlerBase<OrderPlacedSuccessfulEvent>
    {
        public OrderPlacedSuccessfulEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<OrderPlacedSuccessfulEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(OrderPlacedSuccessfulEvent @event)
        {

        }
    }
}
