﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Events;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers.Orders
{
    public sealed class PaymentExecutedSuccessfulEventHandler : EventHandlerBase<PaymentExecutedSuccessfulEvent>
    {
        public PaymentExecutedSuccessfulEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<PaymentExecutedSuccessfulEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(PaymentExecutedSuccessfulEvent @event)
        {
            
        }
    }
}
