﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Events;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers.Orders
{
    public class OrderDeliveryExecutedEventHandler : EventHandlerBase<OrderDeliveryExecutedEvent>
    {
        public OrderDeliveryExecutedEventHandler(
          IRepository<IdentifiedEvent> repository,
          ILogger<EventHandlerBase<OrderDeliveryExecutedEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(OrderDeliveryExecutedEvent @event)
        {

        }
    }
}
