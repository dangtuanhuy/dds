﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Events;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers.Orders
{
    public class OrderPlacedFailEventHandler : EventHandlerBase<OrderPlacedFailEvent>
    {
        public OrderPlacedFailEventHandler(
          IRepository<IdentifiedEvent> repository,
          ILogger<EventHandlerBase<OrderPlacedFailEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(OrderPlacedFailEvent @event)
        {

        }
    }
}
