﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Events.Payments;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers.Payments
{
    public class PaymentExecutedEventHandler : EventHandlerBase<PaymentExecutedEvent>
    {
        private CatalogContext _catalogContext;
        public PaymentExecutedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<PaymentExecutedEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(PaymentExecutedEvent @event)
        {
            _catalogContext = new CatalogContext(@event.ChannelContext);
        }
    }
}
