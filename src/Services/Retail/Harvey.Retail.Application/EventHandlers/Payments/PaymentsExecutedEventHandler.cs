﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.Retail.Application.Events;
using Harvey.Retail.Application.Events.Payments;
using Harvey.Retail.Application.Payment;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers.Payments
{
    public class PaymentsExecutedEventHandler : EventHandlerBase<PaymentsExecutedEvent>
    {
        private IPaymentMethod _paymentMethod;
        private readonly IEventBus _eventBus;
        public PaymentsExecutedEventHandler(
            IEventBus eventBus,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<PaymentsExecutedEvent>> logger,
            IPaymentMethod paymentMethod
            ) : base(repository, logger)
        {
            _eventBus = eventBus;
            _paymentMethod = paymentMethod;
        }

        protected override async Task ExecuteAsync(PaymentsExecutedEvent @event)
        {
            foreach (var paymentExecutedEvent in @event.PaymentExecutedEvents)
            {
                await _paymentMethod.ExecuteAsync(paymentExecutedEvent);
                await _eventBus.PublishAsync(new PaymentExecutedSuccessfulEvent()
                {
                    OrderId = paymentExecutedEvent.OrderId,
                    PaymentId = paymentExecutedEvent.PaymentId
                });
            }
        }
    }
}
