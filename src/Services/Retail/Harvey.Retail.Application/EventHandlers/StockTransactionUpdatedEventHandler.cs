﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Enums;
using Harvey.Retail.Application.Events.StockTransactions;
using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers
{
    public class StockTransactionUpdatedEventHandler : EventHandlerBase<StockTransactionUpdatedEvent>
    {
        private readonly IConfiguration _configuration;
        private readonly RetailDbContext _retailDbContext;
        private CatalogContext _catalogContext;

        public StockTransactionUpdatedEventHandler(
            IConfiguration configuration,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<StockTransactionUpdatedEvent>> logger,
            RetailDbContext retailDbContext
            ) : base(repository, logger)
        {
            _configuration = configuration;
            _retailDbContext = retailDbContext;
        }

        protected override async Task ExecuteAsync(StockTransactionUpdatedEvent @event)
        {
            if (@event.ChannelContext == null || string.IsNullOrEmpty(@event.ChannelContext.ServerInformation))
            {
                return;
            }
            var _client = new HttpClient();

            var updateStockTransaction = await GetUpdateStockTransactionRequest(@event);

            var oldOrderItemDetails = updateStockTransaction.oldStockTransactionOrderItems;

            if (oldOrderItemDetails != null && oldOrderItemDetails.Any())
            {
                foreach (var oldOrderItemDetail in oldOrderItemDetails)
                {
                    var oldOrderItem = @event.Items.FirstOrDefault(x => (x.Status == OrderItemStatus.Old || x.Status == OrderItemStatus.Normal) && x.VariantId == oldOrderItemDetail.VariantId);
                    if (oldOrderItem != null)
                    {
                        oldOrderItemDetail.OrderItemId = oldOrderItem.Id;
                    }
                }
            }

            var updateStockTransactionRequest = new UpdateStockTransactionRequest
            {
                LocationId = @event.LocationId,
                ServerInformation = @event.ServerInformation,
                Items = updateStockTransaction.updateStockTransactionOrderItems
            };

            var content = JsonConvert.SerializeObject(updateStockTransactionRequest);
            var response = await _client.PostAsync($"{_configuration["PIMApi"]}/api/stock-transaction/updatedfromretails",
                new StringContent(content, Encoding.UTF8, "application/json"));

            var contentResponse = await response.Content.ReadAsStringAsync();
            var updateChannelStockTransactionResponse = JsonConvert.DeserializeObject<UpdateChannelStockTransactionResponse>(contentResponse);
            var saleItems = updateChannelStockTransactionResponse.StockTransactionUpdatedRetailsModels;

            if (saleItems != null && saleItems.Any())
            {
                foreach (var saleItem in saleItems)
                {
                    var orderItem = @event.Items.FirstOrDefault(x => x.VariantId == saleItem.VariantId);
                    if (orderItem != null)
                    {
                        saleItem.Price = (decimal)orderItem.Price;
                    }
                }
            }

            await AddOrderItemDetails(saleItems, oldOrderItemDetails);

            _catalogContext = new CatalogContext(@event.ChannelContext);
            await _catalogContext.UpdatedOrderItems(saleItems, updateChannelStockTransactionResponse.ServerInformation);

            try
            {
                var stockTransactions = saleItems.Select(x => new StockTransaction
                {
                    Id = x.Id,
                    StockTransactionRefId = x.StockTransactionRefId,
                    TransactionTypeId = x.TransactionTypeId,
                    StockTypeId = x.StockTypeId,
                    LocationId = x.LocationId,
                    CreatedDate = x.CreatedDate,
                    Quantity = x.Quantity,
                    Balance = x.Balance,
                    UpdatedDate = x.CreatedDate,
                    VariantId = x.VariantId,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.CreatedBy
                }).ToList();
                await _catalogContext.AddStockTransactions(stockTransactions, updateChannelStockTransactionResponse.ServerInformation);
            }
            catch (System.Exception e)
            {

                throw;
            }

        }
        private async Task<bool> AddOrderItemDetails(List<saleStockTransactionModel> saleItems, List<OrderItemDetail> oldOrderItemDetails)
        {
            try
            {
                var orderItemDetails = new List<OrderItemDetail>();
                orderItemDetails.AddRange(oldOrderItemDetails);

                var newOrderItemDetails = saleItems.Select(x => new OrderItemDetail
                {
                    Id = x.Id,
                    StockTypeId = x.StockTypeId,
                    VariantId = x.VariantId,
                    WareHouseId = x.WareHouseId,
                    StoreId = x.LocationId,
                    GIWDocumentItemId = x.GIWDocumentItemId,
                    Quantity = x.Quantity,
                    CostValue = x.Cost,
                    OrderItemId = x.StockTransactionRefId,
                    Status = x.Status,
                    Price = x.Price,
                    CreatedDate = DateTime.UtcNow
                }).ToList();

                orderItemDetails.AddRange(newOrderItemDetails);

                _retailDbContext.OrderItemDetails.AddRange(orderItemDetails);
                await _retailDbContext.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return true;
        }
        private async Task<UpdateStockTransactionItemResponse> GetUpdateStockTransactionRequest(StockTransactionUpdatedEvent @event)
        {
            var stockUpdateItems = @event.Items.Where(x => x.Status != Enums.OrderItemStatus.Old).ToList();
            var updateStockTransactionItemResponse = new UpdateStockTransactionItemResponse();
            updateStockTransactionItemResponse.updateStockTransactionOrderItems.AddRange(stockUpdateItems.Where(x => x.Status == Enums.OrderItemStatus.Normal)
                .Select(x => new UpdateStockTransactionItem
                {
                    Id = x.Id,
                    Amount = x.Amount,
                    GIWDocumentItemId = Guid.Empty,
                    OrderId = x.OrderId,
                    Quantity = x.Quantity,
                    Status = x.Status,
                    StockTypeId = x.StockTypeId,
                    VariantId = x.VariantId
                }).ToList());

            var exchangeOrRefundOrderItems = stockUpdateItems.Where(x => x.Status == Enums.OrderItemStatus.Refund
                                                           || x.Status == Enums.OrderItemStatus.Exchange);
            if (exchangeOrRefundOrderItems != null && exchangeOrRefundOrderItems.ToList().Any())
            {
                var previousOrderItemIds = exchangeOrRefundOrderItems.Select(x => x.PreviousOrderItemId.Value);

                List<String> stockTypes = new List<String> { "CON", "SOR", "SOE", "FIRM" };

                var orderItemDetails = await _retailDbContext.OrderItemDetails.Include(x => x.StockType)
                                                                        .Where(x => previousOrderItemIds.Contains(x.OrderItemId)
                                                                                 && x.Quantity > 0
                                                                                 && (x.Status == OrderItemStatus.Normal || x.Status == OrderItemStatus.Old))
                                                                        .ToListAsync();

                foreach (var correspondingOrderItem in exchangeOrRefundOrderItems)
                {
                    var orderItemDetailByVariants = orderItemDetails.Where(x => x.VariantId == correspondingOrderItem.VariantId
                                                                             && x.OrderItemId == correspondingOrderItem.PreviousOrderItemId)
                                                                    .OrderBy(item => stockTypes.IndexOf(item.StockType.Code))
                                                                    .ToList();

                    if (orderItemDetailByVariants != null && orderItemDetailByVariants.Any())
                    {
                        var refunfOrExchangeQuantityOrderItem = correspondingOrderItem.Quantity;
                        foreach (var orderItemDetailByVariant in orderItemDetailByVariants)
                        {
                            if (refunfOrExchangeQuantityOrderItem <= 0)
                            {
                                updateStockTransactionItemResponse.oldStockTransactionOrderItems.Add(new OrderItemDetail
                                {
                                    StockTypeId = orderItemDetailByVariant.StockTypeId,
                                    VariantId = orderItemDetailByVariant.VariantId,
                                    WareHouseId = orderItemDetailByVariant.WareHouseId,
                                    StoreId = orderItemDetailByVariant.StoreId,
                                    GIWDocumentItemId = orderItemDetailByVariant.GIWDocumentItemId,
                                    Quantity = orderItemDetailByVariant.Quantity,
                                    CostValue = orderItemDetailByVariant.CostValue,
                                    Price = orderItemDetailByVariant.Price,
                                    CreatedDate = DateTime.UtcNow,
                                    OrderItemId = Guid.Empty,
                                    Status = OrderItemStatus.Old
                                });
                            }
                            else if (orderItemDetailByVariant.Quantity - refunfOrExchangeQuantityOrderItem > 0)
                            {
                                var updateStockTransactionOrderItem = new UpdateStockTransactionItem
                                {
                                    Id = correspondingOrderItem.Id,
                                    Amount = correspondingOrderItem.Amount,
                                    GIWDocumentItemId = orderItemDetailByVariant.GIWDocumentItemId,
                                    StockTypeId = orderItemDetailByVariant.StockTypeId,
                                    OrderId = correspondingOrderItem.OrderId,
                                    Quantity = correspondingOrderItem.Quantity,
                                    Status = correspondingOrderItem.Status,
                                    VariantId = correspondingOrderItem.VariantId
                                };

                                updateStockTransactionItemResponse.updateStockTransactionOrderItems.Add(updateStockTransactionOrderItem);

                                updateStockTransactionItemResponse.oldStockTransactionOrderItems.Add(new OrderItemDetail
                                {
                                    StockTypeId = orderItemDetailByVariant.StockTypeId,
                                    VariantId = orderItemDetailByVariant.VariantId,
                                    WareHouseId = orderItemDetailByVariant.WareHouseId,
                                    StoreId = orderItemDetailByVariant.StoreId,
                                    GIWDocumentItemId = orderItemDetailByVariant.GIWDocumentItemId,
                                    Quantity = orderItemDetailByVariant.Quantity - updateStockTransactionOrderItem.Quantity,
                                    CostValue = orderItemDetailByVariant.CostValue,
                                    Price = orderItemDetailByVariant.Price,
                                    CreatedDate = DateTime.UtcNow,
                                    OrderItemId = Guid.Empty,
                                    Status = OrderItemStatus.Old
                                });

                                refunfOrExchangeQuantityOrderItem = 0;
                            }
                            else
                            {
                                var updateStockTransactionOrderItem = new UpdateStockTransactionItem
                                {
                                    Id = correspondingOrderItem.Id,
                                    Amount = correspondingOrderItem.Amount,
                                    GIWDocumentItemId = orderItemDetailByVariant.GIWDocumentItemId,
                                    StockTypeId = orderItemDetailByVariant.StockTypeId,
                                    OrderId = correspondingOrderItem.OrderId,
                                    Quantity = orderItemDetailByVariant.Quantity,
                                    Status = correspondingOrderItem.Status,
                                    VariantId = correspondingOrderItem.VariantId
                                };

                                updateStockTransactionItemResponse.updateStockTransactionOrderItems.Add(updateStockTransactionOrderItem);
                                refunfOrExchangeQuantityOrderItem -= orderItemDetailByVariant.Quantity;
                            }
                        }
                    }
                }
            }

            return updateStockTransactionItemResponse;
        }
    }
}
