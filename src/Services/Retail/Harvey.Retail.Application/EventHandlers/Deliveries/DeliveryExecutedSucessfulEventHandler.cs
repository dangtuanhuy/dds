﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Events;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers.Deliveries
{
    public class DeliveryExecutedSucessfulEventHandler : EventHandlerBase<DeliveryExecutedSucessfulEvent>
    {
        public DeliveryExecutedSucessfulEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<DeliveryExecutedSucessfulEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(DeliveryExecutedSucessfulEvent @event)
        {
            
        }
    }
}
