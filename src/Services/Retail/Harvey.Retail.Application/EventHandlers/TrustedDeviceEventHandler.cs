﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Events.Devices;
using Harvey.Retail.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers
{
    public sealed class TrustedDeviceEventHandler : EventHandlerBase<TrustedDeviceEvent>
    {
        private readonly RetailDbContext _retailDbContext;

        public TrustedDeviceEventHandler(
            IRepository<IdentifiedEvent> repository,
             ILogger<EventHandlerBase<TrustedDeviceEvent>> logger,
             RetailDbContext retailDbContext
            ) : base(repository, logger)
        {
            _retailDbContext = retailDbContext;
        }

        protected override async Task ExecuteAsync(TrustedDeviceEvent @event)
        {
            var device = await _retailDbContext.Devices.AsNoTracking().FirstOrDefaultAsync(x => x.UserCode == @event.UserCode);
            if (device == null)
            {
                throw new InvalidOperationException("Device is not presented.");
            }

            _retailDbContext.Entry(device).State = EntityState.Modified;
            device.Trusted = true;
            device.UpdatedBy = @event.UserId;
            device.UpdatedDate = DateTime.UtcNow;
            await _retailDbContext.SaveChangesAsync();
        }
    }
}
