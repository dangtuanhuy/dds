﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Events.Orders;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.EventHandlers
{
    public sealed class OrderCreatedEventHandler : EventHandlerBase<OrderCreatedEvent>
    {
        private CatalogContext _catalogContext;
        public OrderCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<OrderCreatedEvent>> logger) : base(repository, logger)
        {

        }

        protected override async Task ExecuteAsync(OrderCreatedEvent @event)
        {
            var catalogContext = new CatalogContext(new ChannelContext
            {
                ServerInformation = @event.ServerInformation
            });

            var order = new Order
            {
                Id = new System.Guid(@event.AggregateId),
                State = @event.State,
                GrandTotal = @event.GrandTotal,
                CreatedDate = @event.CreatedDate,
                LocationId = @event.LocationId,
                BillNo = @event.BillNo,
                GST = @event.GST,
                GSTInclusive = @event.GSTInclusive,
                CustomerId = @event.CustomerId,
                CustomerName = @event.CustomerName,
                CustomerCode = @event.CustomerCode,
                CashierId = @event.CashierId,
                CashierName = @event.CashierName,
                PreviousOrderId = @event.PreviousOrderId,
                OrderTransactionType = @event.OrderTransactionType,
                OriginalOrderId = @event.OriginalOrderId,
                Reason = @event.Reason,
                DeviceId = @event.DeviceId,
                Collect = @event.Collect
            };

            await catalogContext.AddOrder(order, @event.OrderPromotions);
        }
    }
}
