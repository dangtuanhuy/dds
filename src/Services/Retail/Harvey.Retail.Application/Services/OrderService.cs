﻿using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Services
{
    public class OrderService
    {
        private readonly RetailDbContext _retailDbContext;
        public OrderService(RetailDbContext retailDbContext)
        {
            _retailDbContext = retailDbContext;
        }

        public async Task<List<OrderModel>> GetAsync(Guid locationId, DateTime fromDate, DateTime toDate)
        {
            var result = new List<OrderModel>();
            var serverInfomation = await GetChannelInfomation(locationId);
            if (string.IsNullOrEmpty(serverInfomation))
            {
                return result;
            }

            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(serverInfomation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var orders = dbContext.Orders
                                        .Where(o => o.LocationId == locationId
                                                    && (fromDate.Date <= o.CreatedDate.Date)
                                                    && (o.CreatedDate.Date <= toDate.Date)).Include(o => o.Items).ToList();
                if (orders.Any())
                {
                    var variantIds = orders.SelectMany(x => x.Items.Select(y => y.VariantId));
                    var productVariants = await dbContext.Variants.AsNoTracking()
                        .Where(x => variantIds.Contains(x.Id))
                        .Select(x => new
                        {
                            x.ProductId,
                            VariantId = x.Id
                        })
                        .ToListAsync();
                    var productIds = productVariants.Select(x => x.ProductId);
                    var products = dbContext.Products.AsNoTracking().Where(x => productIds.Contains(x.Id))
                        .Select(x => new
                        {
                            x.Id,
                            x.Name
                        }).ToList();

                    foreach (var order in orders)
                    {
                        var orderItemModels = new List<OrderItemModel>();
                        foreach (var orderItem in order.Items)
                        {
                            var productId = productVariants.FirstOrDefault(x => x.VariantId == orderItem.VariantId)?.ProductId;
                            var productName = products.FirstOrDefault(x => x.Id == productId)?.Name;
                            orderItemModels.Add(new OrderItemModel
                            {
                                Id = orderItem.Id,
                                VariantId = orderItem.VariantId.ToString(),
                                Quantity = orderItem.Quantity,
                                Amount = orderItem.Amount,
                                CostValue = orderItem.CostValue,
                                VariantName = productName,
                                Price = orderItem.Price
                            });
                        }

                        result.Add(new OrderModel
                        {
                            Id = order.Id,
                            OrderItems = orderItemModels,
                            StoreId = order.LocationId,
                            CreatedDate = order.CreatedDate
                        });
                    }
                }
            };
            
            return result;
        }

        public async Task<List<OrderItemModel>> GetOrderItemAsync(Guid locationId, DateTime fromDate, DateTime toDate)
        {
            var result = new List<OrderItemModel>();
            var serverInfomation = await GetChannelInfomation(locationId);
            if (string.IsNullOrEmpty(serverInfomation))
            {
                return result;
            }

            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(serverInfomation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var orders = dbContext.Orders
                                        .Where(o => o.LocationId == locationId
                                                    && (fromDate.Date <= o.CreatedDate.Date)
                                                    && (o.CreatedDate.Date <= toDate.Date)).Select(o => new { id = o.Id, createdDate = o.CreatedDate }).ToList();
                var orderIds = orders.Select(x => x.id);

                if (orderIds.Any())
                {
                    var orderItems = dbContext.OrderItems.Where(ot => orderIds.Contains(ot.OrderId)).ToList();

                    var variantIds = orderItems.Select(y => y.VariantId);
                    var productVariants = await dbContext.Variants.AsNoTracking()
                                                        .Where(x => variantIds.Contains(x.Id))
                                                        .Select(x => new
                                                        {
                                                            x.ProductId,
                                                            VariantId = x.Id
                                                        })
                                                        .ToListAsync();
                    var productIds = productVariants.Select(x => x.ProductId);
                    var products = dbContext.Products.AsNoTracking().Where(x => productIds.Contains(x.Id))
                      .Select(x => new
                      {
                          x.Id,
                          x.Name
                      }).ToList();

                    foreach (var orderItem in orderItems)
                    {
                        var productId = productVariants.FirstOrDefault(x => x.VariantId == orderItem.VariantId)?.ProductId;
                        var productName = products.FirstOrDefault(x => x.Id == productId)?.Name;
                        result.Add(new OrderItemModel
                        {
                            Id = orderItem.Id,
                            VariantId = orderItem.VariantId.ToString(),
                            Quantity = orderItem.Quantity,
                            Amount = orderItem.Amount,
                            CostValue = orderItem.CostValue,
                            VariantName = productName,
                            Price = orderItem.Price
                        });
                    }
                }
            };

            return result;
        }

        private async Task<string> GetChannelInfomation(Guid locationId)
        {
            var channelAssignment = await _retailDbContext.ChannelStoreAssignments.FirstOrDefaultAsync(x => x.StoreId == locationId);
            if (channelAssignment == null)
            {
                throw new InvalidOperationException("Channel is not assigned");
            }

            var channel = await _retailDbContext.Channels.FirstOrDefaultAsync(x => x.Id == channelAssignment.ChannelId);
            if (channel != null)
                return channel.ServerInformation;
            return string.Empty;
        }
    }
}
