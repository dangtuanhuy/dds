﻿using Harvey.Domain;
using Harvey.Retail.Application.Common;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Services
{
    public class SaleTransactionService
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly INextSequenceService<TransientRetailDbContext> _nextSequenceService;
        private readonly CatalogContext _catalogContext;

        public SaleTransactionService(RetailDbContext retailDbContext,
            INextSequenceService<TransientRetailDbContext> nextSequenceService,
            CatalogContext catalogContext)
        {
            _retailDbContext = retailDbContext;
            _nextSequenceService = nextSequenceService;
            _catalogContext = catalogContext;
        }

        public async Task<Tuple<byte[], string, string>> GetSaleTransactionsForGTOReport(SaleTransactionForGTOReportModel model, string ServerInformation = "")
        {
            var data = await GetSaleTransactionsDataForGTOReport(model, ServerInformation);
            var content = data.Item1;
            var fileName = data.Item2;
            var extentionFile = data.Item3;
            
            string[] temp = { fileName, content };
            var fileContent = string.Join("|", temp);

            return Tuple.Create<byte[], string, string>(Encoding.Default.GetBytes(fileContent), fileName, extentionFile);
        }

        public async Task<Tuple<byte[], string, string>> GetSaleTransactionsForGTOReportWithNotPrefixFileName(SaleTransactionForGTOReportModel model, string ServerInformation = "")
        {
            var data = await GetSaleTransactionsDataForGTOReport(model, ServerInformation);
            var content = data.Item1;
            var fileName = data.Item2;
            var extentionFile = data.Item3;

            return Tuple.Create<byte[], string, string>(Encoding.Default.GetBytes(content), fileName, extentionFile);
        }

        private async Task<Tuple<string, string, string>> GetSaleTransactionsDataForGTOReport(SaleTransactionForGTOReportModel model, string ServerInformation)
        {
            string nextSequenceKey = !string.IsNullOrEmpty(model.MachineId)
                                            ? model.MachineId
                                            : RetailConstants.MachineIdNextSequenceDefault;
            var data = await _catalogContext.GetSaleTransactionsForGTOReport(model.FromDate, model.ToDate, new Guid(model.LocationId), model.MachineId, model.ReportType, 11, ServerInformation);
            var extentionFile = _nextSequenceService.GetExtentionSaleTransactionFile(nextSequenceKey);
            var fileName = $"{model.ReportType + model.MachineId}.{extentionFile}";

            return Tuple.Create<string, string, string>(data, fileName, extentionFile);
        }
    }
}
