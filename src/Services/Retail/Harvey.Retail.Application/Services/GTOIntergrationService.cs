﻿using Harvey.Retail.Application.Configs;
using Renci.SshNet;
using System.IO;

namespace Harvey.Retail.Application.Services
{
    public class GTOIntergrationService
    {
        public static bool SFTPUploadFile(byte[] data, string fileName, SFTPConfig config)
        {
            try
            {
                using (var client = new SftpClient(config.Host, config.Port, config.UserName, config.Password))
                {
                    client.Connect();
                    if (client.IsConnected)
                    {
                        using (var ms = new MemoryStream(data))
                        {
                            client.BufferSize = (uint)ms.Length;
                            var filePath = $"{config.FolderPath.TrimEnd('/')}/{fileName}";
                            client.UploadFile(ms, filePath);
                        }
                    }

                    client.Disconnect();

                    return true;
                }
            }
            catch (System.Exception e)
            {
                return false;
            }
        }
    }
}
