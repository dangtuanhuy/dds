﻿using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Services
{
    public interface IExportService
    {
        Task<Tuple<byte[], string, string>> ExportEndDayReport(EndDayReportModel model, string ServerInformation = "");
    }

    public class ExportService : IExportService
    {
        private readonly RetailDbContext _retailDbContext;

        public ExportService(RetailDbContext retailDbContext)
        {
            _retailDbContext = retailDbContext;
        }

        public async Task<Tuple<byte[], string, string>> ExportEndDayReport(EndDayReportModel model, string ServerInformation = "")
        {
            var content = new StringBuilder();
            var fileName = "EndDayReport";
            var extentionFile = "csv";

            var deviceId = _retailDbContext.Devices.SingleOrDefault(x => x.DeviceCode == model.DeviceCode).Id;
            var posEndDay = await _retailDbContext.POSEndDays
                                            .Include(x => x.EndDayPaymentItems)
                                            .Include("EndDayPaymentItems.PaymentMethod")
                                            .Include(x => x.Store)
                                            .FirstOrDefaultAsync(c => c.CreatedDate.Date == model.ToDate.Date && c.DeviceId == deviceId);

            var breakLine = "------------------------------------------";

            if (posEndDay != null)
            {
                var store = posEndDay.Store;
                var endDayPaymentItems = posEndDay.EndDayPaymentItems;
                var posCashItemsPerDay = _retailDbContext.POSCashItems.Where(x => x.DocumentStatus == Domain.DocumentStatus.EndDay && x.DocumentId == posEndDay.Id)
                                                                       .Include(x => x.Currency).ToList();

                content.AppendLine(string.Join(",", "Location", store.Address.Replace(",", "")));
                content.AppendLine(string.Join(",", "Cashier", posEndDay.CashierName));
                content.AppendLine(string.Join(",", "Settlement Date", TimeZone.CurrentTimeZone.ToLocalTime(posEndDay.CreatedDate)));
                content.AppendLine(breakLine);
                content.AppendLine(string.Join(",", "Currency", "Qty", "Amount"));

                decimal totalCash = 0;

                foreach (var item in posCashItemsPerDay)
                {
                    var currency = item.Currency.Value;
                    var quantity = item.Quantity;

                    var contentcsv = string.Join(",", currency, quantity, (currency * quantity));
                    content.AppendLine(contentcsv);

                    totalCash += currency * quantity;
                }

                content.AppendLine(breakLine);
                content.AppendLine(string.Join(",", "Total Cash", totalCash));

                var paymentMethodWithCash = endDayPaymentItems.FirstOrDefault(x => x.PaymentMethod.Code.ToLower().Equals(PaymentType.Cash.ToString().ToLower()));
                var posCashSales = paymentMethodWithCash?.Amount;

                content.AppendLine(string.Join(",", "POS CASH SALES", posCashSales));

                content.AppendLine("Paymode Details");

                content.AppendLine(string.Join(",", "Cash", totalCash));

                var paymentMethodsWithoutCash = endDayPaymentItems.Where(x => !x.PaymentMethod.Code.ToLower().Equals(PaymentType.Cash.ToString().ToLower()));
                var totalAmountPaymentWithoutCash = paymentMethodsWithoutCash.Sum(x => x.Amount);

                foreach (var item in paymentMethodsWithoutCash)
                {
                    var contentcsv = string.Join(",", item.PaymentMethod.Name, item.Amount);
                    content.AppendLine(contentcsv);
                }

                var totalCashOpenDay = (from o in _retailDbContext.POSOpenDays
                                        where o.CreatedDate.Date == model.ToDate.Date
                                        join p in _retailDbContext.POSCashItems.Include(p => p.Currency)
                                        on o.Id equals p.DocumentId
                                        select p).ToList();
                decimal totalOpenCash = 0;

                foreach (var item in totalCashOpenDay)
                {
                    var currency = item.Currency.Value;
                    var quantity = item.Quantity;

                    totalOpenCash += currency * quantity;
                }

                var total = totalCash + totalAmountPaymentWithoutCash;
                var tillAmount = totalOpenCash;
                var totalCollection = total;                
                var grandTotal = totalCollection - tillAmount;
                var systemTotal = posCashSales + totalAmountPaymentWithoutCash;
                var shortageExcess = grandTotal - systemTotal;

                content.AppendLine(breakLine);
                content.AppendLine(string.Join(",", "Total", total));
                content.AppendLine(string.Join(",", "Till Amount", tillAmount));

                content.AppendLine(breakLine);
                content.AppendLine(string.Join(",", "Total Collection", totalCollection));
                content.AppendLine(string.Join(",", "Grand Total", grandTotal));
                content.AppendLine(string.Join(",", "System Total", systemTotal));
                content.AppendLine(string.Join(",", "Shortage / Excess", shortageExcess));
                content.AppendLine(breakLine);
                content.AppendLine(string.Join(",", "Note", posEndDay.Note.Replace(",", " ")));
            }

            string[] temp = { fileName, content.ToString() };
            var fileContent = string.Join("|", temp);

            return Tuple.Create<byte[], string, string>(Encoding.Default.GetBytes(fileContent), fileName, extentionFile);
        }
    }
}

