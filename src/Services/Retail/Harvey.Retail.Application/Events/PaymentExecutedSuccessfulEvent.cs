﻿using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using System;

namespace Harvey.Retail.Application.Events
{
    public class PaymentExecutedSuccessfulEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public Guid PaymentId { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public PaymentExecutedSuccessfulEvent()
        {
        }

        public PaymentExecutedSuccessfulEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
