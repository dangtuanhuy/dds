﻿using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using System;

namespace Harvey.Retail.Application.Events
{
    public class OrderDeliveryExecutedEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public Guid DeliveryId { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public OrderDeliveryExecutedEvent()
        {
        }

        public OrderDeliveryExecutedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
