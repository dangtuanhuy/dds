﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Events.StockTransactions
{
    public class StockTransactionUpdatedEvent : EventBase
    {
        public Guid LocationId { get; set; }
        public List<OrderItem> Items { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public string ServerInformation { get; set; }
        public StockTransactionUpdatedEvent()
        {
        }

        public StockTransactionUpdatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
