﻿using Harvey.EventBus;
using System;

namespace Harvey.Retail.Application.Events
{
    public class PaymentExecutedFailEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public Guid PaymentId { get; set; }
        public PaymentExecutedFailEvent()
        {
        }

        public PaymentExecutedFailEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
