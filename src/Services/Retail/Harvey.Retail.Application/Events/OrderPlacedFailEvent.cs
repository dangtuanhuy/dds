﻿using Harvey.EventBus;
using System;

namespace Harvey.Retail.Application.Events
{
    public class OrderPlacedFailEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public OrderPlacedFailEvent()
        {
        }

        public OrderPlacedFailEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
