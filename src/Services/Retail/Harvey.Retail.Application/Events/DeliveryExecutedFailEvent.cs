﻿using Harvey.EventBus;
using System;

namespace Harvey.Retail.Application.Events
{
    public class DeliveryExecutedFailEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public Guid DeliveryId { get; set; }
        public DeliveryExecutedFailEvent()
        {
        }

        public DeliveryExecutedFailEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
