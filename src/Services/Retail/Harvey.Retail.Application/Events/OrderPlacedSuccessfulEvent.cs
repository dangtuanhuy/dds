﻿using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using System;

namespace Harvey.Retail.Application.Events
{
    public class OrderPlacedSuccessfulEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public OrderPlacedSuccessfulEvent()
        {
        }

        public OrderPlacedSuccessfulEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
