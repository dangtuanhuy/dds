﻿using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using System;

namespace Harvey.Retail.Application.Events
{
    public class DeliveryExecutedSucessfulEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public Guid DeliveryId { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public DeliveryExecutedSucessfulEvent()
        {
        }

        public DeliveryExecutedSucessfulEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
