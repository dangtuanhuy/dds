﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Checkout.POS;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Events.Deliveries
{
    public class DeliveryCreatedEvent : EventBase
    {
        public Guid DeliveryId { get; set; }
        public DeliveryState State { get; set; }
        public DeliveryCreatedEvent()
        {
        }

        public DeliveryCreatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }

    public class DeliveryCreatedEventHandler : EventHandlerBase<DeliveryCreatedEvent>
    {
        private readonly IEventBus _eventBus;
        public DeliveryCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<DeliveryCreatedEvent>> logger,
            IEventBus eventBus) : base(repository, logger)
        {
            _eventBus = eventBus;
        }

        protected override Task ExecuteAsync(DeliveryCreatedEvent @event)
        {
            return Task.CompletedTask;
        }
    }
}
