﻿using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using System;

namespace Harvey.Retail.Application.Events
{
    public class OrderSubmittedEvent : EventBase
    {
        public ChannelContext ChannelContext { get; set; }
        public Guid OrderId { get; set; }
        public Guid? CustomerId { get; set; }
        public Cart Cart { get; set; }
        public string ServerInformation { get; set; }
        public OrderSubmittedEvent()
        {
        }

        public OrderSubmittedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
