﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Events.Payments
{
    public class PaymentCreatedEvent : EventBase
    {
        public Guid PaymentId { get; set; }
        public float Amount { get; set; }
        public PaymentState State { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public Guid OrderId { get; set; }
        public string PaymentCode { get; set; }
        public PaymentCreatedEvent()
        {
        }

        public PaymentCreatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }

    public class PaymentCreatedEventHandler : EventHandlerBase<PaymentCreatedEvent>
    {
        private CatalogContext _catalogContext;
        public PaymentCreatedEventHandler(
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<PaymentCreatedEvent>> logger
            ) : base(repository, logger)
        {
        }

        protected override async Task ExecuteAsync(PaymentCreatedEvent @event)
        {
            _catalogContext = new CatalogContext(@event.ChannelContext);
            await _catalogContext.AddPayment(@event.PaymentId, @event.OrderId, @event.State, @event.Amount, @event.PaymentCode);
        }
    }
}
