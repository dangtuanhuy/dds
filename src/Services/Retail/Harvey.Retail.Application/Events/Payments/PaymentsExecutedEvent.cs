﻿using Harvey.EventBus;
using Harvey.Retail.Application.Contexts;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Events.Payments
{
    public class PaymentsExecutedEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public List<PaymentExecutedEvent> PaymentExecutedEvents { get; set; }

        public PaymentsExecutedEvent()
        {
        }

        public PaymentsExecutedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
