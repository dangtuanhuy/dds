﻿using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using System;

namespace Harvey.Retail.Application.Events.Payments
{
    public class PaymentExecutedEvent : EventBase
    {
        public Guid PaymentId { get; set; }
        public PaymentState State { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public Guid OrderId { get; set; }
        public PaymentType Method { get; set; }
        public PaymentExecutedEvent()
        {
        }

        public PaymentExecutedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
