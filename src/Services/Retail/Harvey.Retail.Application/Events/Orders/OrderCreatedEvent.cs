﻿using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Events.Orders
{
    public class OrderCreatedEvent : EventBase
    {
        public OrderState State { get; set; }
        public Guid? CustomerId { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public float GrandTotal { get; set; }
        public Guid LocationId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string BillNo { get; set; }
        public float GST { get; set; }
        public bool GSTInclusive { get; set; }
        public Guid CashierId { get; set; }
        public string CashierName { get; set; }
        public Guid? PreviousOrderId { get; set; }
        public Guid OriginalOrderId { get; set; }
        public string Reason { get; set; }
        public Guid DeviceId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public OrderTransactionType OrderTransactionType { get; set; }
        public float Collect { get; set; }
        public string ServerInformation { get; set; }
        public List<OrderPromotion> OrderPromotions { get; set; }

        public OrderCreatedEvent()
        {
        }

        public OrderCreatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
