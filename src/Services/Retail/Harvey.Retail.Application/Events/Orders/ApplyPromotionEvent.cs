﻿using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Events.Orders
{
    public class ApplyPromotionEvent : EventBase
    {
        public ChannelContext ChannelContext { get; set; }
        public Cart Cart { get; set; }
        public Guid OrderId { get; set; }
        public ApplyPromotionEvent()
        {
        }

        public ApplyPromotionEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
