﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Enums;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Events.Orders
{
    public class OrderItemAddedEvent : EventBase
    {
        public Guid LineItemId { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public Guid OrderId { get; set; }
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
        public Guid StockTypeId { get; set; }
        public float Amount { get; set; }
        public int Quantity { get; set; }
        public List<CatalogOrderItemPromotion> OrderItemPromotions { get; set; }
        public float Price { get; set; }
        public Guid PriceId { get; set; }
        public Guid? PreviousOrderItemId { get; set; }
        public OrderItemStatus Status { get; set; }
        public string ServerInformation { get; set; }

        public OrderItemAddedEvent()
        {
        }

        public OrderItemAddedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }

    public class OrderItemAddedEventHandler : EventHandlerBase<OrderItemAddedEvent>
    {
        private CatalogContext _catalogContext;
        public OrderItemAddedEventHandler(IRepository<IdentifiedEvent> repository, ILogger<EventHandlerBase<OrderItemAddedEvent>> logger) : base(repository, logger)
        {
        }

        protected override async Task ExecuteAsync(OrderItemAddedEvent @event)
        {
            var catalogContext = new CatalogContext(new ChannelContext
            {
                ServerInformation = @event.ServerInformation
            });

            var orderItem = new OrderItem
            {
                Id = @event.LineItemId,
                OrderId = @event.OrderId,
                VariantId = @event.VariantId,
                Quantity = @event.Quantity,
                Amount = @event.Amount,
                StockTypeId = @event.StockTypeId,
                Price = @event.Price,
                PriceId = @event.PriceId,
                PreviousOrderItemId = @event.PreviousOrderItemId,
                Status = @event.Status,
                VariantName = @event.VariantName
            };

            await catalogContext.AddOrderItem(orderItem, @event.OrderItemPromotions);
        }
    }
}
