﻿using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Microsoft.Extensions.Logging;

namespace Harvey.Retail.Application.Events.Orders
{
    public class OrderStateUpdatedEvent : EventBase
    {
        public OrderState State { get; set; }
        public OrderStateUpdatedEvent()
        {
        }

        public OrderStateUpdatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }

    public class OrderStateUpdatedEventHandler : EventHandlerBase<OrderStateUpdatedEvent>
    {
        public OrderStateUpdatedEventHandler(IRepository<IdentifiedEvent> repository, ILogger<EventHandlerBase<OrderStateUpdatedEvent>> logger) : base(repository, logger)
        {
        }

        protected override Task ExecuteAsync(OrderStateUpdatedEvent @event)
        {
            return Task.CompletedTask;
        }
    }
}
