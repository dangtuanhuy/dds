﻿using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using System;

namespace Harvey.Retail.Application.Events.Orders
{
    public class CaculateSubTotalEvent : EventBase
    {
        public ChannelContext ChannelContext { get; set; }
        public Cart Cart { get; set; }
        public Guid OrderId { get; set; }
        public CaculateSubTotalEvent()
        {
        }

        public CaculateSubTotalEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
