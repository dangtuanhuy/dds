﻿using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Events.OrderPayments
{
    public class OrderPaymentsExecutedEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public List<CartPayment> CartPayments { get; set; }

        public OrderPaymentsExecutedEvent()
        {
        }

        public OrderPaymentsExecutedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
