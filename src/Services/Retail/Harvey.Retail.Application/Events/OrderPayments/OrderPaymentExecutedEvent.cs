﻿using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using System;

namespace Harvey.Retail.Application.Events.OrderPayments
{
    public class OrderPaymentExecutedEvent : EventBase
    {
        public Guid OrderId { get; set; }
        public Guid PaymentId { get; set; }
        public float Amount { get; set; }
        public ChannelContext ChannelContext;
        public PaymentType Method { get; set; }
        public string PaymentCode { get; set; }
        public OrderPaymentExecutedEvent()
        {
        }

        public OrderPaymentExecutedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
