﻿using System;

namespace Harvey.Retail.Application.Checkout
{
    public class CartPayment
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public string PaymentCode { get; set; }
        public float Amount { get; set; }
    }
}
