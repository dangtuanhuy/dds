﻿namespace Harvey.Retail.Application.Checkout
{
    public enum DeliveryState
    {
        Init = 1,
        Delivered = 2
    }
}
