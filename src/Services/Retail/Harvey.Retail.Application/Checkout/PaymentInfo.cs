﻿using System;

namespace Harvey.Retail.Application.Checkout
{
    public class PaymentInfo
    {
        public Guid OrderId { get; set; }
        public Guid Id { get; set; }
        public float Amount { get; set; }
    }
}
