﻿using System;

namespace Harvey.Retail.Application.Checkout
{
    public class CustomerInfo
    {
        public Guid CustomerId { get; set; }
        public Address BillingAddress { get; set; }
        public Address DeliveryAddress { get; set; }
    }
}
