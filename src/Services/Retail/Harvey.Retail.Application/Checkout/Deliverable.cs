﻿using System;

namespace Harvey.Retail.Application.Checkout
{
    public class Deliverable
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
    }
}
