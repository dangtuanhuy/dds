﻿using System;
using System.Collections.Generic;
using Automatonymous;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Marten.Schema;

namespace Harvey.Retail.Application.Checkout.POS
{
    public class OrderSagaStateMachineInstance : SagaStateMachineInstance
    {
        [Identity]
        public Guid CorrelationId { get; set; }
        public string CurrentState { get; set; }
        public Order Order { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public Guid StoreId { get; set; }
        public string ServerInformation { get; set; }
    }
}
