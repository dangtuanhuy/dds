﻿using Automatonymous;
using Harvey.EventBus.Abstractions;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Events;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Harvey.Domain;
using System.Linq;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Events.Payments;
using Harvey.Retail.Application.Events.StockTransactions;
using Harvey.Retail.Application.Events.Orders;
using Harvey.Retail.Application.Events.OrderPayments;

namespace Harvey.Retail.Application.Checkout.POS
{
    //TODO for now saga only works when fire two events at the same time
    //Have to fix it
    public class POSCheckoutFlow : ICheckoutFlow
    {
        private readonly IEventBus _eventBus;
        private readonly ChannelContext _channelContext;
        public POSCheckoutFlow(
            IEventBus eventBus,
            ChannelContext channelContext)
        {
            _eventBus = eventBus;
            _channelContext = channelContext;
        }
        public async Task<bool> PlaceOrder(Cart cart, CustomerInfo customerInfo, string serverInformation)
        {
            await _eventBus.PublishAsync(new OrderSubmittedEvent()
            {
                OrderId = cart.CorrelationId,
                CreatedDate = cart.CreatedDate,
                Cart = cart,
                CustomerId = customerInfo?.CustomerId,
                ChannelContext = _channelContext,
                ServerInformation = serverInformation
            });

            return true;
        }
    }

    public class POSCheckoutFlowSaga : MassTransitStateMachine<OrderSagaStateMachineInstance>
    {
        public State Confirmed { get; private set; }
        public State ExecuteCharge { get; private set; }
        public State ExecutePromotion { get; private set; }
        public State CaculateSubTotal { get; private set; }
        public State CaculateGrandTotal { get; private set; }
        public State ExecutePayments { get; private set; }
        public State Paid { get; private set; }
        public State ExecuteDelivery { get; private set; }
        public State Delivered { get; private set; }
        public State Completed { get; set; }
        public State Attention { get; set; }

        public Event<OrderSubmittedEvent> OrderSubmitted { get; private set; }
        public Event<ApplyPromotionEvent> ApplyPromotionEventExecuted { get; set; }
        public Event<CaculateSubTotalEvent> CaculateSubTotalEventExecuted { get; set; }
        public Event<CaculateGrandTotalEvent> CaculateGrandTotalEventExecuted { get; set; }
        public Event<OrderPaymentsExecutedEvent> OrderPaymentsExecuted { get; private set; }
        public Event<OrderDeliveryExecutedEvent> OrderDeliveryExecuted { get; private set; }
        public Event<PaymentExecutedSuccessfulEvent> PaymentExecutedSuccessful { get; private set; }
        public Event<DeliveryExecutedSucessfulEvent> DeliveryExecutedSucessful { get; private set; }
        public Event<DeliveryExecutedFailEvent> DeliveryExecutedFail { get; private set; }
        public Event<PaymentExecutedFailEvent> PaymentExecutedFail { get; private set; }
        public Event<OrderPlacedSuccessfulEvent> OrderPlacedSuccessful { get; private set; }
        public Event<OrderPlacedFailEvent> OrderPlacedFail { get; private set; }
        private readonly IServiceProvider _serviceProvider;
        private IEventStoreRepository<Order> _repository
        {
            get
            {
                return _serviceProvider.GetRequiredService<IEventStoreRepository<Order>>();
            }
        }
        public POSCheckoutFlowSaga(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            InstanceState(x => x.CurrentState);
            Event(() => OrderSubmitted, x => x.CorrelateById(ctx => ctx.Message.OrderId).SelectId(ctx => ctx.Message.OrderId));
            Event(() => ApplyPromotionEventExecuted, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => CaculateSubTotalEventExecuted, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => CaculateGrandTotalEventExecuted, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => OrderPaymentsExecuted, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => OrderDeliveryExecuted, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => PaymentExecutedSuccessful, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => DeliveryExecutedSucessful, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => DeliveryExecutedFail, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => DeliveryExecutedFail, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => PaymentExecutedFail, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => OrderPlacedSuccessful, x => x.CorrelateById(ctx => ctx.Message.OrderId));
            Event(() => OrderPlacedFail, x => x.CorrelateById(ctx => ctx.Message.OrderId));

            //start
            Initially(
                When(OrderSubmitted)
                    .Then(context =>
                    {
                        context.Instance.CorrelationId = context.Data.OrderId;
                        context.Instance.StoreId = context.Data.Cart.LocationId;
                        context.Instance.ServerInformation = context.Data.ServerInformation;
                        var cart = context.Data.Cart;
                        
                        var order = new Order
                        {
                            BillNo = cart.BillNo,
                            GST = (float)cart.GST,
                            GSTInclusive = cart.GSTInclusive,
                            CustomerCode = cart.CustomerCode,
                            CustomerId = cart.CustomerId,
                            CustomerName = cart.CustomerName,
                            CashierId = cart.CashierId,
                            CashierName = cart.CashierName,
                            OrderTransactionType = cart.OrderTransactionType,
                            GrandTotal = cart.Amount,
                            OriginalOrderId = cart.OriginalOrderId,
                            PreviousOrderId = cart.PreviousOrderId,
                            Reason = cart.Reason,
                            DeviceId = cart.DeviceId,
                            Collect = cart.Collect
                        };

                        order.OrderPromotions = cart.CartPromotions.Select(x => new OrderPromotion
                        {
                            Id = x.Id,
                            OrderId = x.OrderId,
                            DiscountType = x.DiscountType,
                            PromotionId = x.PromotionId,
                            Reason = x.Reason,
                            Value = (float)x.Value
                        }).ToList();

                        order.SetChannelContext(context.Data.ChannelContext);
                        order.SetLocationId(cart.LocationId);

                        context.Instance.Order = order;
                    })
                    .ThenAsync(async context =>
                    {
                        context.Instance.Order.AddOrder(context.Data.OrderId, context.Data.CreatedDate, context.Instance.ServerInformation);
                        context.Instance.Order.SetCustomerInfo(context.Data.CustomerId);
                        context.Instance.Order.SetShippingInfo(context.Data.CustomerId);

                        context.Data.Cart.Items.ForEach(cartItem =>
                        {
                            context.Instance.Order.AddLineItem(context.Data.OrderId, cartItem, context.Instance.ServerInformation);
                        });

                        await _repository.SaveAsync(context.Instance.Order);
                    })
                    .TransitionTo(ExecutePromotion)
                    .Publish(context =>
                    {
                        return new ApplyPromotionEvent()
                        {
                            OrderId = context.Data.OrderId,
                            Cart = context.Data.Cart,
                            ChannelContext = context.Data.ChannelContext
                        };
                    })
                );

            During(ExecutePromotion,
            When(ApplyPromotionEventExecuted)
                .TransitionTo(CaculateSubTotal)
                .Publish(context =>
                {
                    return new CaculateSubTotalEvent()
                    {
                        OrderId = context.Data.OrderId,
                        Cart = context.Data.Cart,
                        ChannelContext = context.Data.ChannelContext
                    };
                })
            );

            //Payment event
            During(CaculateSubTotal,
                When(CaculateSubTotalEventExecuted)
                    .TransitionTo(Confirmed)
                     .Publish(context =>
                     {
                         return new OrderPaymentsExecutedEvent()
                         {
                             OrderId = context.Data.OrderId,
                             CartPayments = context.Data.Cart.CartPayments,
                             ChannelContext = context.Data.ChannelContext,
                         };
                     })
                );

            //process payment
            During(Confirmed,
            When(OrderPaymentsExecuted)
                .ThenAsync(async context =>
                {
                    context.Data.CartPayments.ForEach(x =>
                    {
                        context.Instance.Order.AddPayment(x.Id, x.Amount, context.Data.OrderId, x.PaymentCode);
                    });

                    context.Instance.Order.UpdateState(OrderState.Processing);
                    await _repository.SaveAsync(context.Instance.Order);
                })
                .TransitionTo(ExecuteCharge)
                .Publish(context => new PaymentsExecutedEvent()
                {
                    OrderId = context.Data.OrderId,
                    ChannelContext = context.Data.ChannelContext,
                    PaymentExecutedEvents = context.Data.CartPayments.Select(x => new PaymentExecutedEvent
                    {
                        OrderId = x.OrderId,
                        PaymentId = x.Id,
                        State = PaymentState.Paid,
                        ChannelContext = context.Data.ChannelContext
                    }).ToList()
                })
                .Publish(context => new PaymentExecutedSuccessfulEvent()
                {
                    OrderId = context.Data.OrderId,
                    ChannelContext = context.Data.ChannelContext
                })
             );

            During(ExecuteCharge,
              When(PaymentExecutedSuccessful)
                   .TransitionTo(Paid)
                   .Publish(context => new OrderDeliveryExecutedEvent()
                   {
                       OrderId = context.Data.OrderId,
                       DeliveryId = Guid.NewGuid(),
                       ChannelContext = context.Data.ChannelContext
                   }),
              When(PaymentExecutedFail)
                  .TransitionTo(Attention)
                  .Publish(context => new OrderPlacedFailEvent()
                  {
                      OrderId = context.Data.OrderId
                  })
               );

            //process delivery
            During(Paid,
              When(OrderDeliveryExecuted)
                  .ThenAsync(async context =>
                  {
                      context.Instance.Order.AddDelivery(context.Data.DeliveryId);
                      context.Instance.Order.UpdateState(OrderState.Processing);
                      await _repository.SaveAsync(context.Instance.Order);
                  })
                  .TransitionTo(ExecuteDelivery)
                  .Publish(context => new DeliveryExecutedSucessfulEvent()
                  {
                      OrderId = context.Data.OrderId,
                      DeliveryId = context.Data.DeliveryId,
                      ChannelContext = context.Data.ChannelContext
                  })
               );

            During(ExecuteDelivery,
             When(DeliveryExecutedSucessful)
                 .TransitionTo(Delivered)
                 .Publish(context => new OrderPlacedSuccessfulEvent()
                 {
                     OrderId = context.Data.OrderId,
                     ChannelContext = context.Data.ChannelContext
                 }),
             When(DeliveryExecutedFail)
                 .TransitionTo(Attention)
                 .Publish(context => new OrderPlacedFailEvent()
                 {
                     OrderId = context.Data.OrderId
                 })
              );

            //fail
            During(Attention,
                When(OrderPlacedFail)
                 .ThenAsync(async context =>
                 {
                     context.Instance.Order.UpdateState(OrderState.Attention);
                     await _repository.SaveAsync(context.Instance.Order);
                 })
                .Finalize());

            //complete
            During(Delivered,
                When(OrderPlacedSuccessful)
                .ThenAsync(async context =>
                {
                    context.Instance.Order.UpdateState(OrderState.Completed);
                    await _repository.SaveAsync(context.Instance.Order);
                })
                .Publish(context =>
                {
                    return new StockTransactionUpdatedEvent()
                    {
                        LocationId = context.Instance.Order.LocationId,
                        Items = context.Instance.Order.Items.ToList(),
                        ChannelContext = context.Data.ChannelContext,
                        ServerInformation = context.Instance.ServerInformation
                    };
                })
                .Finalize());

            SetCompletedWhenFinalized();
        }
    }
}
