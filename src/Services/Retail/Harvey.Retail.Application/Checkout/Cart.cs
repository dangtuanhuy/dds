﻿using Harvey.Retail.Application.Enums;
using Harvey.Retail.Application.Models;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Checkout
{
    public class Cart
    {
        public Guid CorrelationId { get; set; }
        public float Amount { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public bool IsDelete { get; set; }
        public PaymentType Method { get; set; }
        public Guid LocationId { get; set; }
        public double GST { get; set; }
        public bool GSTInclusive { get; set; }
        public string BillNo { get; set; }
        public Guid CashierId { get; set; }
        public string CashierName { get; set; }
        public Guid? PreviousOrderId { get; set; }
        public Guid OriginalOrderId { get; set; }
        public Guid DeviceId { get; set; }
        public string Reason { get; set; }
        public OrderTransactionType OrderTransactionType { get; set; }
        public float Collect { get; set; }
        public List<CartPromotion> CartPromotions { get; set; }
        public List<CartPayment> CartPayments { get; set; }
        public List<CartItem> Items { get; set; }
    }
    public class CartPromotion
    {
        public Guid CorrelationId { get; set; }
        public Guid Id { get; set; }
        public Guid PromotionId { get; set; }
        public Guid OrderId { get; set; }
        public DiscountType DiscountType { get; set; }
        public string Reason { get; set; }
        public double Value { get; set; }
    }

}
