﻿using System.Threading.Tasks;

namespace Harvey.Retail.Application.Checkout
{
    public interface ICheckoutFlow
    {
        Task<bool> PlaceOrder(Cart cart, CustomerInfo customerInfo, string serverInformation);
    }
}
