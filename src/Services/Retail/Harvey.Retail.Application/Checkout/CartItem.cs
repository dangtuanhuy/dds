﻿using Harvey.Retail.Application.Enums;
using Harvey.Retail.Application.Models;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Checkout
{
    public class CartItem
    {
        public Guid CorrelationId { get; set; }
        public Guid Id { get; set; }
        public float Amount { get; set; }
        public string VariantId { get; set; }
        public string VariantName { get; set; }
        public string PriceId { get; set; }
        public string StockTypeId { get; set; }
        public float Price { get; set; }
        public int Quantity { get; set; }
        public string SKUCode { get; set; }
        public bool IsDelete { get; set; }
        public Guid? PreviousOrderItemId { get; set; }
        public OrderItemStatus Status { get; set; }
        public List<CartItemPromotion> ItemPromotions { get; set; }
        
    }

    public class CartItemPromotion
    {
        public Guid Id { get; set; }
        public DiscountType DiscountType { get; set; }
        public Guid CartItemId { get; set; }
        public double Value { get; set; }
    }
}
