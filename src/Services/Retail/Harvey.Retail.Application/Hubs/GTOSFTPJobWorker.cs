﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.Job;
using Harvey.Retail.Application.Infrastructure.Commands.Stores;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Hubs
{
    public class GTOSFTPJobWorker : IWorker
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IEventBus _eventBus;
        public GTOSFTPJobWorker(ICommandExecutor commandExecutor, IEventBus eventBus)
        {
            _commandExecutor = commandExecutor;
            _eventBus = eventBus;
        }
        public void Execute(Guid correlationId, string jobName)
        {
            var utcNow = DateTime.UtcNow;
            var fromDate = utcNow.Date;
            var toDate = utcNow.Date.AddDays(1);
            var result = _commandExecutor.ExecuteAsync(new PostGTOSFTPCommand(new PostGTOSFTPRequest
            {
                ReportType = "D",
                PostGTOSFTPRequestItems = new List<PostGTOSFTPRequestItem>(),
                FromDate = fromDate,
                ToDate = toDate
            })).Result;
        }
    }
}
