﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.Job;
using Harvey.Retail.Application.Infrastructure.Commands.Stores;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Hubs
{
    public class GTOSFTPJobWorkerMonthly : IWorker
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IEventBus _eventBus;
        public GTOSFTPJobWorkerMonthly(ICommandExecutor commandExecutor, IEventBus eventBus)
        {
            _commandExecutor = commandExecutor;
            _eventBus = eventBus;
        }
        public void Execute(Guid correlationId, string jobName)
        {
            var date = DateTime.UtcNow;
            var fromDate = date.AddMonths(-1).Date;
            var toDate = date.Date;
            var result = _commandExecutor.ExecuteAsync(new PostGTOSFTPCommand(new PostGTOSFTPRequest
            {
                ReportType = "T",
                FromDate = fromDate,
                ToDate = toDate,
                PostGTOSFTPRequestItems = new List<PostGTOSFTPRequestItem>()
            })).Result;
        }
    }
}
