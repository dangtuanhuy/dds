﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Payment
{
    public interface IPaymentMethod
    {
        Task ExecuteAsync(dynamic TEvent);

        Task ExecuteAsync();

        Task ExecuteSuccessful();

        Task ExecuteFail();
    }
}
