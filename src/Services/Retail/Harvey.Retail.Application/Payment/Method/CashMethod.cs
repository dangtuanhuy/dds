﻿using Harvey.Retail.Application.Contexts;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Payment.Method
{
    public class CashMethod : IPaymentMethod
    {
        private CatalogContext _catalogContext;

        public CashMethod()
        {

        }

        public async Task ExecuteAsync()
        {
            
        }

        public async Task ExecuteAsync(dynamic @event)
        {
            _catalogContext = new CatalogContext(@event.ChannelContext);
            await _catalogContext.ExecutePayment(@event.PaymentId, @event.State);
        }

        public Task ExecuteFail()
        {
            throw new NotImplementedException();
        }

        public Task ExecuteSuccessful()
        {
            throw new NotImplementedException();
        }
    }
}
