﻿using System;

namespace Harvey.Retail.Application.Utilities
{
    public static class PriceUtility
    {
        public static double GetPriceBeforeTax(double priceInclusive, double tax)
        {
            return Round(priceInclusive / (1 + tax), 2);
        }

        public static double Round(double value, int decimalPlaces)
        {
            return Math.Round(value, decimalPlaces, MidpointRounding.AwayFromZero);
        }
    }
}
