﻿namespace Harvey.Retail.Application.Enums
{
    public enum Gender
    {
        Male,
        Female
    }
}
