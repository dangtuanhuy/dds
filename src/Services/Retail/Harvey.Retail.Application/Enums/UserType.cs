﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Enums
{
    public enum UserType
    {
        Admin,
        Staff,
        Member,
        AdminStaff
    }
}
