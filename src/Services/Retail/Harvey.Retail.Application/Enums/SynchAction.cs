﻿namespace Harvey.Retail.Application.Enums
{
    public enum SynchAction
    {
        Add = 1,
        Update = 2,
        Delete = 3
    }
}
