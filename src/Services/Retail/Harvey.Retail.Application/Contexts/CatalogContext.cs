﻿using Harvey.EventBus;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Enums;
using Harvey.Retail.Application.Events.Orders;
using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Models;
using Harvey.Retail.Application.Models.Feeds;
using Harvey.Retail.Application.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Contexts
{
    public class CatalogContext
    {
        private readonly ChannelContext _channelContext;

        public CatalogContext(ChannelContext channelContext)
        {
            _channelContext = channelContext;
        }

        public async Task<List<Category>> GetCategories()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var result = new List<Category>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.Categories.ToListAsync();
            };
            return result;
        }


        public async Task<dynamic> SynchSaleOrder(List<OrderModel> orders)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            int result = 0;
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                foreach (OrderModel item in orders)
                {
                    var newSynchronization = new Synchronization
                    {
                        SynchAction = SynchAction.Update,
                        Entity = SynchEntity.SaleOrder,
                        RefId = item.Id,
                        ModifiedDate = DateTime.UtcNow
                    };
                    await dbContext.AddAsync(newSynchronization);
                };
                result = await dbContext.SaveChangesAsync();
            }
            return result;
        }

        private List<SynchronizationItemModel> GetSyncSaleOrders(CatalogDbContext catalogDbContext, List<Synchronization> synchronizationsToSync)
        {
            if (!synchronizationsToSync.Any())
            {
                return new List<SynchronizationItemModel>();
            }
            return catalogDbContext.Synchronizations
                .Where(x => x.Entity == SynchEntity.SaleOrder)
                .Select(x => new SynchronizationItemModel()
                {
                    Action = x.SynchAction,
                    Type = x.Entity,
                    Entity = x.RefId,
                    CreatedDate = x.ModifiedDate
                }).ToList();
        }

        private List<SynchronizationItemModel> GetSyncCategories(CatalogDbContext catalogDbContext, List<Synchronization> catagoriesToSync)
        {
            if (!catagoriesToSync.Any())
            {
                return new List<SynchronizationItemModel>();
            }
            var ids = catagoriesToSync.Where(x => x.SynchAction != SynchAction.Delete).Select(x => x.RefId);
            var categoriesToDeleted = catagoriesToSync
                                        .Where(x => x.SynchAction == SynchAction.Delete)
                                        .Select(x => new SynchronizationItemModel
                                        {
                                            Entity = new { Id = x.RefId },
                                            Type = SynchEntity.Category,
                                            Action = SynchAction.Delete,
                                            CreatedDate = x.ModifiedDate
                                        });
            return catalogDbContext.Categories
                .Where(x => ids.Contains(x.Id))
                .Select(x => new SynchronizationItemModel()
                {
                    Action = catagoriesToSync.Any(y => y.RefId == x.Id && y.SynchAction == SynchAction.Update) ? SynchAction.Update : SynchAction.Add,
                    Type = SynchEntity.Category,
                    Entity = x,
                    CreatedDate = catagoriesToSync.First(y => y.RefId == x.Id && y.SynchAction != SynchAction.Delete).ModifiedDate
                }).ToList()
                .Union(categoriesToDeleted)
                 .ToList();
        }

        private List<SynchronizationItemModel> GetSyncProducts(CatalogDbContext catalogDbContext, List<Synchronization> productsToSync)
        {
            if (!productsToSync.Any())
            {
                return new List<SynchronizationItemModel>();
            }
            var ids = productsToSync.Where(x => x.SynchAction != SynchAction.Delete).Select(x => x.RefId);
            var productsToDeleted = productsToSync
                                       .Where(x => x.SynchAction == SynchAction.Delete)
                                       .Select(x => new SynchronizationItemModel
                                       {
                                           Entity = new { Id = x.RefId },
                                           Type = SynchEntity.Product,
                                           Action = SynchAction.Delete,
                                           CreatedDate = x.ModifiedDate
                                       });
            var result = catalogDbContext.Products
               .Where(x => ids.Contains(x.Id))
               .ToList()
                .Select(x => new SynchronizationItemModel()
                {
                    Action = productsToSync.Any(y => y.RefId == x.Id && y.SynchAction == SynchAction.Update) ? SynchAction.Update : SynchAction.Add,
                    Type = SynchEntity.Product,
                    Entity = x,
                    CreatedDate = productsToSync.First(y => y.RefId == x.Id && y.SynchAction != SynchAction.Delete).ModifiedDate
                }).ToList()
               .Union(productsToDeleted)
               .ToList();
            return result;
        }



        public async Task<List<Product>> GetProducts(DateTime lastSync)
        {
            var syncDate = lastSync.AddMinutes(-1);
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var result = new List<Product>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.Products
                                      .Where(x => x.UpdatedDate >= lastSync)
                                      .AsNoTracking().ToListAsync();
            };
            return result;
        }

        public async Task<List<BarCode>> GetBarCodes(DateTime? lastSync)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var result = new List<BarCode>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {

                if (lastSync == null)
                {
                    result = await dbContext.BarCodes.AsNoTracking().ToListAsync();
                }
                else
                {
                    var syncDate = lastSync.Value.AddMinutes(-1);
                    result = await dbContext.BarCodes
                   .AsNoTracking()
                   .Where(x => x.CreatedDate >= syncDate).ToListAsync();
                }
            };
            return result;
        }

        public async Task<List<CatalogPaymentMethod>> GetPaymentMethods()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var result = new List<CatalogPaymentMethod>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.PaymentMethods.ToListAsync();
            };
            return result;
        }

        public async Task AddPayment(Guid paymentId, Guid orderId, Checkout.PaymentState state, float amount, string paymentCode)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var correspondingPaymentMethod = await dbContext.PaymentMethods
                                    .FirstOrDefaultAsync(x => x.Code == paymentCode);
                if (correspondingPaymentMethod != null)
                {
                    var payment = new Domain.Payment
                    {
                        Id = paymentId,
                        State = state,
                        Amount = amount,
                        OrderId = orderId,
                        PaymentMethodId = correspondingPaymentMethod.Id
                    };
                    await dbContext.Payments.AddAsync(payment);
                    await dbContext.SaveChangesAsync();
                }
            };
        }

        public async Task ExecutePayment(Guid paymentId, Checkout.PaymentState state)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var payment = await dbContext.Payments.FirstOrDefaultAsync(p => p.Id == paymentId);
                if (payment != null)
                {
                    payment.State = state;
                    await dbContext.SaveChangesAsync();
                }
            };
        }

        public async Task AddOrder(Order order, List<OrderPromotion> orderPromotions)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var saveData = false;
                var existedOrder = await dbContext.Orders.FirstOrDefaultAsync(x => x.Id == order.Id);
                if (existedOrder == null)
                {
                    await dbContext.Orders.AddAsync(order);
                    saveData = true;
                }

                if (orderPromotions != null && orderPromotions.Any())
                {
                    foreach (var orderPromotion in orderPromotions)
                    {
                        var existedOrderPromotion = await dbContext.OrderPromotions.FirstOrDefaultAsync(x => x.Id == orderPromotion.Id);
                        if (existedOrderPromotion == null)
                        {
                            saveData = true;
                            await dbContext.OrderPromotions.AddAsync(orderPromotion);
                        }
                    }
                }

                if (saveData)
                {
                    await dbContext.SaveChangesAsync();
                }
            };
        }
        
        //public async Task AddOrderPromotion(Guid id, Guid orderId, Guid promotionId, string reason, DiscountType discountType, float value)
        //{
        //    var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
        //    optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
        //    using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
        //    {
        //        var existedOrderPromotion = await dbContext.OrderPromotions.FirstOrDefaultAsync(x => x.Id == id);
        //        if (existedOrderPromotion == null)
        //        {
        //            var orderPromotion = new OrderPromotion
        //            {
        //                Id = id,
        //                OrderId = orderId,
        //                PromotionId = promotionId,
        //                Reason = reason,
        //                DiscountType = discountType,
        //                Value = value
        //            };
        //            await dbContext.OrderPromotions.AddAsync(orderPromotion);
        //            await dbContext.SaveChangesAsync();
        //        }
        //    };
        //}

        public async Task AddOrderItem(OrderItem orderItem, List<CatalogOrderItemPromotion> orderItemPromotions)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var saveData = false;
                var existedOrderItem = await dbContext.OrderItems.FirstOrDefaultAsync(x => x.Id == orderItem.Id);
                if (existedOrderItem == null)
                {
                    await dbContext.OrderItems.AddAsync(orderItem);
                    saveData = true;
                }

                if (orderItemPromotions != null && orderItemPromotions.Any())
                {
                    foreach (var orderItemPromotion in orderItemPromotions)
                    {
                        var existedOrderItemPromotion = await dbContext.OrderItemPromotions.FirstOrDefaultAsync(x => x.Id == orderItemPromotion.Id);
                        if (existedOrderItemPromotion == null)
                        {
                            saveData = true;
                            await dbContext.OrderItemPromotions.AddAsync(orderItemPromotion);
                        }
                    }
                }

                if (saveData)
                {
                    await dbContext.SaveChangesAsync();
                }
            };
        }

        //public async Task AddOrderItemPromotion(Guid id, Guid orderItemId, float value, DiscountType discountType)
        //{
        //    var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
        //    optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
        //    using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
        //    {
        //        var existedOrderItemPromotion = await dbContext.OrderItemPromotions.FirstOrDefaultAsync(x => x.Id == id);
        //        if (existedOrderItemPromotion == null)
        //        {
        //            var orderItemPromotion = new CatalogOrderItemPromotion
        //            {
        //                Id = id,
        //                OrderItemId = orderItemId,
        //                DiscountType = discountType,
        //                Value = value
        //            };
        //            await dbContext.OrderItemPromotions.AddAsync(orderItemPromotion);
        //            await dbContext.SaveChangesAsync();
        //        }
        //    };
        //}

        public async Task<List<CatalogVariantModel>> GetVariants(DateTime? lastSync, string locationId)
        {
            await Task.Yield();
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var catalogVariants = new List<CatalogVariantModel>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                List<Variant> variants = null;
                if (lastSync == null)
                {
                    variants = dbContext.Variants.AsNoTracking().ToList();
                }
                else
                {
                    var date = lastSync.Value.AddMinutes(-1);
                    variants = dbContext.Variants.Where(a => a.UpdatedDate >= date).AsNoTracking().ToList();
                }

                if (variants.Count > 0)
                {
                    catalogVariants = GetVariantsInternal(dbContext, variants, locationId);
                }
            };
            return catalogVariants;
        }


        public async Task<List<CatalogStockBalanceModel>> GetStockBalance(DateTime? lastSync, Guid locationId)
        {
            await Task.Yield();
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var stockBalances = new List<CatalogStockBalanceModel>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                List<StockTransaction> stockTransactions = null;
                if (lastSync == null)
                {
                    stockTransactions = dbContext.StockTransactions.Where(a => a.LocationId == locationId).OrderByDescending(a => a.CreatedDate).AsNoTracking().ToList();
                }
                else
                {
                    var date = lastSync.Value.AddMinutes(-1);
                    stockTransactions = dbContext.StockTransactions.Where(a => a.LocationId == locationId && a.CreatedDate >= date)
                                        .OrderByDescending(a => a.CreatedDate).AsNoTracking().ToList();
                }

                if (stockTransactions.Count > 0)
                {
                    stockBalances = GetStockBalanceInternal(dbContext, stockTransactions);
                }
            };
            return stockBalances;
        }

        public async Task<List<CatalogPromotionModel>> GetPromotions(DateTime lastSync)
        {
            var syncDate = lastSync.AddMinutes(-1);

            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var result = new List<CatalogPromotionModel>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.Promotions
                    .Where(x => x.UpdatedDate >= syncDate)
                    .Select(promotion => new
                    {
                        Promotion = promotion,
                        PromotionDetail = dbContext.PromotionDetails.FirstOrDefault(x => x.Id == promotion.PromotionDetailId)
                    })
                    .Select(x => new CatalogPromotionModel
                    {
                        Id = x.Promotion.Id,
                        PromotionDetailId = x.Promotion.PromotionDetailId,
                        Name = x.Promotion.Name,
                        Description = x.Promotion.Description,
                        PromotionTypeId = x.Promotion.PromotionTypeId,
                        Value = x.PromotionDetail != null ? x.PromotionDetail.Value : 0,
                        PromotionStatus = x.Promotion.Status,
                        DiscountTypeId = x.PromotionDetail != null ? x.PromotionDetail.DiscountTypeId : 0,
                        IsUseConditions = x.PromotionDetail != null ? x.PromotionDetail.IsUseConditions : false,
                        IsUseCouponCodes = x.PromotionDetail != null ? x.PromotionDetail.IsUseConditions : false,
                        FromDate = x.Promotion.FromDate,
                        ToDate = x.Promotion.ToDate
                    }).ToListAsync();
            };
            return result;
        }

        public async Task<List<PromotionCondition>> GetPromotionConditions()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var result = new List<PromotionCondition>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.PromotionConditions.ToListAsync();
            };
            return result;
        }

        public async Task<List<PromotionCouponCode>> GetPromotionCouponCodes()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var result = new List<PromotionCouponCode>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.PromotionCouponCodes.ToListAsync();
            };

            return result;
        }

        private List<CatalogStockBalanceModel> GetStockBalanceInternal(CatalogDbContext dbContext, List<StockTransaction> stockTransactions)
        {
            var catalogStockBalanceModels = new List<CatalogStockBalanceModel>();
            var variantIds = stockTransactions.Select(a => a.VariantId).Distinct();
            foreach (var item in variantIds)
            {
                var stockTransaction = stockTransactions.FirstOrDefault(a => a.VariantId == item);
                var catalogStockBalanceModel = new CatalogStockBalanceModel()
                {
                    VariantId = item,
                    Quantity = stockTransaction?.Balance ?? 0,
                    CreatedDate = stockTransaction?.CreatedDate ?? DateTime.UtcNow
                };
                catalogStockBalanceModels.Add(catalogStockBalanceModel);
            }
            return catalogStockBalanceModels;
        }

        private List<CatalogVariantModel> GetVariantsInternal(CatalogDbContext catalogDbContext, List<Variant> variants, string locationId)
        {
            var priceIds = variants.Select(a => a.PriceId).ToList();
            var variantIds = variants.Select(a => a.Id).ToList();
            var productIds = variants.Select(a => a.ProductId).ToList();
            var prices = variants.Count > 10000
                            ? catalogDbContext.Prices.AsNoTracking().ToList()
                            : catalogDbContext.Prices.Where(a => priceIds.Contains(a.Id)).AsNoTracking().ToList();
            var products = variants.Count > 10000
                                ? catalogDbContext.Products.AsNoTracking().ToList()
                                : catalogDbContext.Products.Where(a => productIds.Contains(a.Id)).AsNoTracking().ToList();
            var allfieldValues = variants.Count > 10000
                                    ? catalogDbContext.FieldValues.AsNoTracking().ToList()
                                    : catalogDbContext.FieldValues.Where(a => variantIds.Contains(a.EntityId)).AsNoTracking().ToList();
            var catalogVariantList = new List<CatalogVariantModel>();

            var locationGuid = new Guid(locationId);
            foreach (var item in variants)
            {
                var product = products.FirstOrDefault(a => a.Id == item.ProductId);
                if (product != null)
                {
                    var price = prices.FirstOrDefault(a => a.Id == item.PriceId);
                    if (price != null)
                    {
                        var fieldValues = allfieldValues.Where(a => a.EntityId == item.Id).OrderBy(a => a.FieldId).Select(s => s.FieldValue).ToArray();

                        var catalogVariantModel = new CatalogVariantModel()
                        {
                            Id = item.Id,
                            VariantId = item.Id,
                            SKUCode = item.SKUCode ?? "",
                            ProductId = item.ProductId,
                            ListPrice = price.ListPrice,
                            MemberPrice = price.MemberPrice,
                            StaffPrice = price.StaffPrice,
                            PriceId = item.PriceId,
                            Description = $"{product.Name}-{string.Join("-", fieldValues)}",
                            Quantity = 0,
                            StockTypeId = Guid.Empty,
                            StockType = "",
                            LocationId = locationGuid,
                            IsDelete = product.IsDelete
                        };
                        catalogVariantList.Add(catalogVariantModel);
                    }
                }

            }

            return catalogVariantList;
        }

        public async Task<string> GetSaleTransactionsForGTOReport(DateTime fromDate, DateTime toDate, Guid locationId, string machineId, string reportType, int numberOfChars, string ServerInformation = "")
        {
            if (string.IsNullOrEmpty(ServerInformation))
            {
                ServerInformation = _channelContext.ServerInformation;
            }

            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(ServerInformation);
            string result = "";
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var orders = await dbContext.Orders.Where(o => o.LocationId == locationId && o.CreatedDate > fromDate && o.CreatedDate < toDate)
                                        .Select(x => new
                                        {
                                            x.Collect,
                                            x.GST
                                        }).ToListAsync();

                var ordersAmountBeforeTax = orders.Sum(x => PriceUtility.GetPriceBeforeTax(x.Collect, (double)x.GST / 100));

                var totalWithFormat = String.Format("{0:0.00}", ordersAmountBeforeTax).PadLeft(numberOfChars, '0');

                var format = "yyyyMMdd";
                if (reportType == "T")
                {
                    format = "yyyyMM";
                }
                var transactionDate = fromDate.ToString(format);

                result = string.Format("{0}{1}{2}", reportType + machineId, transactionDate, totalWithFormat);
            };

            return result;
        }

        public async Task ApplyPromotion(Guid orderId, Cart cart)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var order = dbContext.Orders.FirstOrDefault(x => x.Id == orderId);
                var variantIds = cart.Items.Select(x => new Guid(x.VariantId));
                var priceIds = dbContext.Variants.Where(x => variantIds.Contains(x.Id)).Select(x => x.PriceId);
                var prices = dbContext.Prices.Where(x => priceIds.Contains(x.Id)).AsNoTracking().ToList();
                if (order != null)
                {
                    float orderTotalPrice = 0;
                    foreach (var cartItem in cart.Items)
                    {
                        var cartItemPrice = prices.FirstOrDefault(x => x.Id == Guid.Parse(cartItem.PriceId));
                        var cartItemTotalPrice = cart.CustomerId == null ? (cartItemPrice.ListPrice * cartItem.Quantity) : (cartItemPrice.MemberPrice * cartItem.Quantity);
                        float cartItemPromotionTotal = 0;
                        foreach (var cartItemPromotion in cartItem.ItemPromotions)
                        {
                            switch (cartItemPromotion.DiscountType)
                            {
                                case DiscountType.Money:
                                    cartItemPromotionTotal += (float)cartItemPromotion.Value;
                                    break;
                                case DiscountType.Percent:
                                    var promotion = (cartItemTotalPrice / 100) * cartItemPromotion.Value;
                                    cartItemPromotionTotal += (float)promotion;
                                    break;
                            }
                        }
                        cartItemTotalPrice = cartItemTotalPrice - cartItemPromotionTotal;
                        orderTotalPrice += cartItemTotalPrice;
                    }

                    float orderTotalPromotion = 0;
                    foreach (var orderPromotion in cart.CartPromotions)
                    {
                        switch (orderPromotion.DiscountType)
                        {
                            case DiscountType.Money:
                                orderTotalPromotion += (float)orderPromotion.Value;
                                break;
                            case DiscountType.Percent:
                                var promotion = (orderTotalPrice / 100) * orderPromotion.Value;
                                orderTotalPromotion += (float)promotion;
                                break;
                        }
                    }

                    var orderGST = cart.GSTInclusive == false ? (orderTotalPrice / 100) * cart.GST : 0;
                    orderTotalPrice = orderTotalPrice - orderTotalPromotion + (float)orderGST;

                    dbContext.Orders.Update(order);
                    await dbContext.SaveChangesAsync();
                }
            }
        }

        public async Task CaculateSubTotal(Guid orderId, Cart cart)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var order = dbContext.Orders.FirstOrDefault(x => x.Id == orderId);

                if (order != null)
                {
                    double orderPrice = cart.Items.Sum(x => x.Price * x.Quantity);
                    if (order.GSTInclusive)
                    {
                        orderPrice = PriceUtility.GetPriceBeforeTax(orderPrice, (double)order.GST / 100);
                    }

                    order.SubTotal = (float)orderPrice;
                    dbContext.Orders.Update(order);
                    await dbContext.SaveChangesAsync();
                }
            }
        }

        public async Task CaculateGrandTotal(Guid orderId, Cart cart)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                //var order = dbContext.Orders.FirstOrDefault(x => x.Id == orderId);

                //if (order != null)
                //{
                //    double subTotal = cart.Items.Sum(x => x.Price * x.Quantity);
                //    if (order.GSTInclusive)
                //    {
                //        subTotal = PriceUtility.GetPriceBeforeTax(subTotal, (double)order.GST);
                //    }

                //    order.GrandTotal = (float)subTotal;
                //    dbContext.Orders.Update(order);
                //    await dbContext.SaveChangesAsync();
                //}
            }
        }

        public async Task<List<Guid>> GetSyncedOrderIds(List<Guid> orderIds)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            var result = new List<Guid>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.Orders.Where(x => orderIds.Contains(x.Id)).Select(x => x.Id).ToListAsync();
            };
            return result;
        }

        public async Task<List<StockTransaction>> AddStockTransactions(List<StockTransaction> stockTransactions, string serverInformation)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(serverInformation);
            var result = new List<StockTransaction>();
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                foreach (var stockTransaction in stockTransactions)
                {
                    await dbContext.StockTransactions.AddAsync(stockTransaction);
                }

                await dbContext.SaveChangesAsync();
                result = stockTransactions;
            };
            return result;
        }

        public async Task<List<OrderItem>> UpdatedOrderItems(List<saleStockTransactionModel> stockTransactions, string serverInformation)
        {
            var optionBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionBuilder.UseNpgsql(serverInformation);
            var result = new List<OrderItem>();
            using (var dbContext = new CatalogDbContext(optionBuilder.Options))
            {
                var orderItemIds = stockTransactions.Select(x => x.Id);
                var orderItems = dbContext.OrderItems.Where(x => orderItemIds.Contains(x.Id)).ToList();
                var udpatedOrderItems = stockTransactions.GroupBy(x => new { x.StockTransactionRefId }, (key, group) => new
                {
                    Id = key.StockTransactionRefId,
                    Cost = group.Sum(x => x.Cost * x.Quantity)
                }).ToList();
                foreach (var item in udpatedOrderItems)
                {
                    var orderItem = orderItems.FirstOrDefault(x => x.Id == item.Id);
                    if (orderItem != null)
                    {
                        orderItem.CostValue = item.Cost;
                    }
                }
                await dbContext.SaveChangesAsync();
                result = orderItems;
            }
            return result;
        }

        public async Task UpdateCategoryIdsByProductIds(List<GetCategoryIdsByProdutIdsReponseItem> data)
        {
            var productIds = data.Select(x => x.ProductId).ToList();
            var optionBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionBuilder.UseNpgsql(_channelContext.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionBuilder.Options))
            {
                var products = await dbContext.Products.Where(x => productIds.Contains(x.Id)).ToListAsync();
                foreach (var product in products)
                {
                    product.CategoryId = data.First(x => x.ProductId == product.Id).CategoryId;
                }

                await dbContext.SaveChangesAsync();
            }
        }

        public async Task<int> CountProductsAsync(string serverInformation)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(serverInformation);
            int result = 0;
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.Products.CountAsync();
            };
            return result;
        }

        public async Task<int> CountVariantsAsync(string serverInformation)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(serverInformation);
            int result = 0;
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                result = await dbContext.Variants.CountAsync();
            };
            return result;
        }
    }
}

