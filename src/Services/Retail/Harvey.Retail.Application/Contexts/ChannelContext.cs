﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Harvey.Retail.Application.Contexts
{
    public class ChannelContext
    {
        [Key]
        public string ServerInformation { get; set; }
        public Guid DeviceId { get; set; }
    }
}
