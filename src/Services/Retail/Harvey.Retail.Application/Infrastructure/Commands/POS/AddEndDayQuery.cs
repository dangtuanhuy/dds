﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Report
{
    public class AddEndDayQuery : IQuery<bool>
    {
        public EndDayModel EndDayModel { get; set; }
        public AddEndDayQuery(EndDayModel endDayModel)
        {
            EndDayModel = endDayModel;
        }
    }

    public sealed class AddEndDayQueryHandler : IQueryHandler<AddEndDayQuery, bool>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IEfRepository<RetailDbContext, POSEndDay> _posEndDayRepo;
        private readonly IEfRepository<RetailDbContext, POSCashItem> _posCashItemRepo;
        public AddEndDayQueryHandler(RetailDbContext retailDbContext,
            IEfRepository<RetailDbContext, POSEndDay> posEndDayRepo,
            IEfRepository<RetailDbContext, POSCashItem> posCashItemRepo
            )
        {
            _retailDbContext = retailDbContext;
            _posEndDayRepo = posEndDayRepo;
            _posCashItemRepo = posCashItemRepo;
        }

        public async Task<bool> Handle(AddEndDayQuery query)
        {
            var endDayModel = query.EndDayModel;

            var deviceId = _retailDbContext.Devices.FirstOrDefault(x => x.DeviceCode == endDayModel.DeviceCode).Id;
            var currencies = _retailDbContext.Currency.ToList();

            var endDayId = Guid.NewGuid();

            var posEndDay = new POSEndDay()
            {
                Id = endDayId,
                CashierId = endDayModel.CashierId,
                CashierName = endDayModel.CashierName,
                DeviceId = deviceId,
                StoreId = endDayModel.StoreId,
                Note = endDayModel.Note,
                CreatedDate = DateTime.UtcNow,
            };

            List<EndDayPaymentItem> endDayPaymentItems = new List<EndDayPaymentItem>();

            if (endDayModel.PaymentModeDetails != null && endDayModel.PaymentModeDetails.Any())
            {
                foreach (var item in endDayModel.PaymentModeDetails)
                {
                    var endDayPaymentItem = new EndDayPaymentItem
                    {
                        PaymentMethodId = item.PaymentMethodId,
                        Amount = item.Amount,
                        POSEndDayId = endDayId
                    };

                    endDayPaymentItems.Add(endDayPaymentItem);
                }
            }

            posEndDay.EndDayPaymentItems = endDayPaymentItems;

            if (endDayModel.Currencies != null && endDayModel.Currencies.Any())
            {
                foreach (var item in endDayModel.Currencies)
                {
                    var currencyId = currencies.FirstOrDefault(c => c.Value == item.Value)?.Id;
                    var posCashItem = new POSCashItem
                    {
                        CurrencyId = currencyId.Value,
                        DocumentStatus = DocumentStatus.EndDay,
                        Quantity = item.Quantity,
                        DocumentId = endDayId,
                    };

                    await _posCashItemRepo.AddAsync(posCashItem);
                }
            }

            await _posEndDayRepo.AddAsync(posEndDay);

            await _posEndDayRepo.SaveChangesAsync();

            return true;
        }
    }
}
