﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Report
{
    public class AddOpenDayQuery : IQuery<bool>
    {
        public OpenDayModel OpenDayModel { get; set; }
        public AddOpenDayQuery(OpenDayModel openDayModel)
        {
            OpenDayModel = openDayModel;
        }
    }

    public sealed class AddOpenDayQueryHandler : IQueryHandler<AddOpenDayQuery, bool>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IEfRepository<RetailDbContext, POSOpenDay> _posOpenDayRepo;
        private readonly IEfRepository<RetailDbContext, POSCashItem> _posCashItemRepo;
        public AddOpenDayQueryHandler(RetailDbContext retailDbContext,
            IEfRepository<RetailDbContext, POSOpenDay> posOpenDayRepo,
            IEfRepository<RetailDbContext, POSCashItem> posCashItemRepo
            )
        {
            _retailDbContext = retailDbContext;
            _posOpenDayRepo = posOpenDayRepo;
            _posCashItemRepo = posCashItemRepo;
        }

        public async Task<bool> Handle(AddOpenDayQuery query)
        {
            var openDayModel = query.OpenDayModel;

            var deviceId = _retailDbContext.Devices.FirstOrDefault(x => x.DeviceCode == openDayModel.DeviceCode).Id;
            var currencies = _retailDbContext.Currency.ToList();

            var openDayId = Guid.NewGuid();

            var posOpenDay = new POSOpenDay()
            {
                Id = openDayId,
                CashierId = openDayModel.CashierId,
                CashierName = openDayModel.CashierName,
                DeviceId = deviceId,
                StoreId = openDayModel.StoreId,                
                CreatedDate = DateTime.UtcNow,
            };
                        

            if (openDayModel.Currencies != null && openDayModel.Currencies.Any())
            {
                foreach (var item in openDayModel.Currencies)
                {
                    var currencyId = currencies.FirstOrDefault(c => c.Value == item.Value)?.Id;
                    var posCashItem = new POSCashItem
                    {
                        CurrencyId = currencyId.Value,
                        DocumentStatus = DocumentStatus.OpenDay,
                        Quantity = item.Quantity,
                        DocumentId = openDayId,
                    };

                    await _posCashItemRepo.AddAsync(posCashItem);
                }
            }

            await _posOpenDayRepo.AddAsync(posOpenDay);

            await _posOpenDayRepo.SaveChangesAsync();

            return true;
        }
    }
}
