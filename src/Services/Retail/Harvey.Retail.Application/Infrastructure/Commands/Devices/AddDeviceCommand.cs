﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Commands.Devices
{
    public class AddDeviceCommand : ICommand<DeviceModel>
    {
        public Guid Creator { get; }
        public DeviceModel DeviceModel { get; }

        public AddDeviceCommand(Guid creator, DeviceModel deviceModel)
        {
            DeviceModel = deviceModel;
            Creator = creator;
        }
    }

    public class AddDeviceCommandHandler : ICommandHandler<AddDeviceCommand, DeviceModel>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IEfRepository<RetailDbContext, Device> _repository;
        private readonly IMapper _mapper;

        public AddDeviceCommandHandler(
            RetailDbContext retailDbContext,
            IEfRepository<RetailDbContext, Device> repository,
            IMapper mapper
            )
        {
            _retailDbContext = retailDbContext;
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<DeviceModel> Handle(AddDeviceCommand command)
        {
            var deviceModel = command.DeviceModel;
            var device = new Device
            {
                Name = deviceModel.Name,
                Description = deviceModel.Description,
                StoreId = deviceModel.StoreId,
                DeviceCode = deviceModel.DeviceCode,
                UserCode = deviceModel.UserCode,
                Trusted = false,
                Status = DeviceStatus.Active,
                CreatedBy = command.Creator,
                UpdatedBy = command.Creator
            };

            var entity = await _repository.AddAsync(device);

            var deviceModelResult = new DeviceModel
            {
                Id = entity.Id,
                Description = entity.Description,
                DeviceCode = entity.DeviceCode,
                Name = entity.Name,
                StoreId = entity.StoreId,
                StoreName = "",
                Trusted = entity.Trusted,
                UserCode = entity.UserCode,
                Status = entity.Status,
                CreatedBy = entity.CreatedBy,
                CreatedDate = entity.CreatedDate,
                UpdatedBy = entity.UpdatedBy,
                UpdatedDate = entity.UpdatedDate
            };
            var store = await _retailDbContext.Stores.FirstOrDefaultAsync(x => x.Id == entity.StoreId);
            if (store != null)
            {
                deviceModelResult.StoreName = store.Name;
            }

            return deviceModelResult;
        }
    }
}
