﻿using Harvey.Domain;
using Harvey.Retail.Application.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Commands.Devices
{
    public class SoftDeleteDeviceCommand : ICommand<bool>
    {
        public Guid Id { get; }

        public SoftDeleteDeviceCommand(Guid id)
        {
            Id = id;
        }
    }

    public class SoftDeleteDeviceCommandHandler : ICommandHandler<SoftDeleteDeviceCommand, bool>
    {
        private readonly RetailDbContext _retailDbContext;

        public SoftDeleteDeviceCommandHandler(
            RetailDbContext retailDbContext
            )
        {
            _retailDbContext = retailDbContext;
        }

        public async Task<bool> Handle(SoftDeleteDeviceCommand command)
        {
            var device = await _retailDbContext.Devices.FirstOrDefaultAsync(x => x.Id == command.Id);
            if (device == null)
            {
                return false;
            }

            device.Status = DeviceStatus.Deleted;
            await _retailDbContext.SaveChangesAsync();

            return true;
        }
    }
}
