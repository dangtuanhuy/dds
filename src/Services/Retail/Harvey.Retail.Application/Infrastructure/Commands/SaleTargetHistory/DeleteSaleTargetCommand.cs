﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Commands.SaleTargetHistory
{
    public class DeleteSaleTargetCommand: ICommand<bool>
    {
        public Guid SaleTargetId { get; }
        public DeleteSaleTargetCommand(Guid saleTargetId)
        {
            SaleTargetId = saleTargetId;
        }
    }
    public class DeleteSaleTargetCommandHandler : ICommandHandler<DeleteSaleTargetCommand, bool>
    {
        private readonly IEfRepository<RetailDbContext, SaleTarget> _saleTargetRepository;
        public DeleteSaleTargetCommandHandler(IEfRepository<RetailDbContext, SaleTarget> saleTargetRepository)
        {
            _saleTargetRepository = saleTargetRepository;
        }
        public async Task<bool> Handle(DeleteSaleTargetCommand command)
        {
            var entity = await _saleTargetRepository.GetByIdAsync(command.SaleTargetId);
            if (entity.ToDate.Date < DateTime.UtcNow.Date)
            {
                throw new ArgumentException($"Sale Target can't delete.");
            }

            entity.IsDelete = true;
            return true;
        }
    }
}
