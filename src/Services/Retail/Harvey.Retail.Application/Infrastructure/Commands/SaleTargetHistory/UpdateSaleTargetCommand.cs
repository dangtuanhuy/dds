﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Commands.SaleTargetHistory
{
    public class UpdateSaleTargetCommand: ICommand<bool>
    {
        public UpdateSaleTargetModel SaleTarget { get; }
        public UpdateSaleTargetCommand(UpdateSaleTargetModel saleTarget)
        {
            SaleTarget = saleTarget;
        }
    }
    public class UpdateSaleTargetCommandHandler: ICommandHandler<UpdateSaleTargetCommand, bool>
    {
        private readonly IEfRepository<RetailDbContext, SaleTarget> _saleTargetRepository;
        public UpdateSaleTargetCommandHandler(IEfRepository<RetailDbContext, SaleTarget> saleTargetRepository)
        {
            _saleTargetRepository = saleTargetRepository;
        }

        public async Task<bool> Handle(UpdateSaleTargetCommand command)
        {
            var entity = await _saleTargetRepository.GetByIdAsync(command.SaleTarget.Id);
            if (entity == null)
            {
                throw new ArgumentException($"Sale Target is not presented.");
            }
            if (entity.ToDate.Date < DateTime.UtcNow.Date && entity.IsDelete == false)
            {
                throw new ArgumentException("Sale Target is not current.");
            }

            entity.Target = command.SaleTarget.Target;
            entity.UpdatedDate = DateTime.UtcNow;

            await _saleTargetRepository.UpdateAsync(entity);

            return true;
        }
    }
}
