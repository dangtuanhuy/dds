﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Commands.SaleTargetHistory
{
    public class AddSaleTargetCommand: ICommand<SaleTargetModel>
    {
        public AddSaleTargetModel SaleTarget { get; }
        public AddSaleTargetCommand(AddSaleTargetModel saleTarget)
        {
            SaleTarget = saleTarget;
        }
    }
    public class AddSaleTargetCommandHandler : ICommandHandler<AddSaleTargetCommand, SaleTargetModel>
    {
        private readonly IEfRepository<RetailDbContext, SaleTarget> _saleTargetRepository;
        private readonly IMapper _mapper;
        public AddSaleTargetCommandHandler(IEfRepository<RetailDbContext, SaleTarget> saleTargetRepository, IMapper mapper)
        {
            _saleTargetRepository = saleTargetRepository;
            _mapper = mapper;
        }
        public async Task<SaleTargetModel> Handle(AddSaleTargetCommand command)
        {
            if(DateTime.Parse(command.SaleTarget.FromDate).Date > DateTime.Parse(command.SaleTarget.ToDate).Date)
            {
                throw new ArgumentException($"From date can't greater than to date.");
            }
            var entity = await _saleTargetRepository.AddAsync(new SaleTarget()
            {
                FromDate = DateTime.Parse(command.SaleTarget.FromDate),
                ToDate = DateTime.Parse(command.SaleTarget.ToDate),
                Target = command.SaleTarget.Target,
                StoreId = command.SaleTarget.StoreId,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow
            });

            return _mapper.Map<SaleTargetModel>(entity);
        }
    }
}
