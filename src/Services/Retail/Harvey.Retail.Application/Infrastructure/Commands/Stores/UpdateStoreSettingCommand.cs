﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Commands.Stores
{
    public class UpdateStoreSettingCommand : ICommand<StoreSettingModel>
    {
        public string Id { get; set; }
        public string Mall { get; set; }
        public string MachineId { get; set; }
        public string ServerIP { get; set; }
        public string AccountId { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string FolderPath { get; set; }
        public UpdateStoreSettingCommand()
        {
        }
    }

    public class UpdateStoreSettingCommandHandler : ICommandHandler<UpdateStoreSettingCommand, StoreSettingModel>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IMapper _mapper;
        private readonly IEfRepository<RetailDbContext, StoreSetting> _storeSettingRepository;

        public UpdateStoreSettingCommandHandler(
            RetailDbContext retailDbContext,
            IEfRepository<RetailDbContext, StoreSetting> storeSettingRepository,
            IMapper mapper
            )
        {
            _retailDbContext = retailDbContext;
            _storeSettingRepository = storeSettingRepository;
            _mapper = mapper;
        }

        public async Task<StoreSettingModel> Handle(UpdateStoreSettingCommand command)
        {
            var devides = _retailDbContext.Devices.AsNoTracking();
            var storeSettings = _retailDbContext.StoreSettings.AsNoTracking().Include(x => x.Store);

            var entity = await _retailDbContext.StoreSettings.FirstOrDefaultAsync(s => s.Id.ToString() == command.Id);

            var storeSetting = await devides
                    .Join(storeSettings,
                    d => d.StoreId,
                    s => s.StoreId,
                    (d, s) => new { d, s })
                    .FirstOrDefaultAsync(sc => sc.d.Status != DeviceStatus.Deleted && sc.s.Id.ToString() == command.Id);

            if (entity != null)
            {
                entity.Mall = command.Mall;
                entity.MachineId = command.MachineId;
                entity.ServerIP = command.ServerIP;
                entity.AccountId = command.AccountId;
                entity.Password = command.Password;
                entity.Port = command.Port;
                entity.FolderPath = command.FolderPath;
            }
            await _storeSettingRepository.UpdateAsync(entity);

            var res = new StoreSettingModel
            {
                Id = storeSetting.s != null ? storeSetting.s.Store.Id : Guid.NewGuid(),
                Name = storeSetting.d.Name,
                DevideCode = storeSetting.d.DeviceCode,
                StoreName = storeSetting.s != null ? storeSetting.s.Store.Name : "",
                Mall = entity.Mall,
                MachineId = entity.MachineId,
                ServerIP = entity.ServerIP,
                AccountId = entity.AccountId,
                Password = entity.Password,
                Port = entity.Port,
                FolderPath = entity.FolderPath,
                Address = storeSetting.s != null ? storeSetting.s.Store.Address : "",
                DevideName = storeSetting.d.Name
            };

            return res;

        }
    }
}
