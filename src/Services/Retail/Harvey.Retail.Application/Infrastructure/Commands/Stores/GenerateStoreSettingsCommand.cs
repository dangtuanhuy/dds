﻿using Harvey.Domain;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Commands.Stores
{
    public class GenerateStoreSettingsCommand : ICommand<List<StoreSettingModel>>
    {
    }

    public sealed class GenerateStoreSettingsCommandHandler : ICommandHandler<GenerateStoreSettingsCommand, List<StoreSettingModel>>
    {
        private readonly RetailDbContext _retailDbContext;

        public GenerateStoreSettingsCommandHandler(RetailDbContext retailDbContext)
        {
            _retailDbContext = retailDbContext;
        }
        public async Task<List<StoreSettingModel>> Handle(GenerateStoreSettingsCommand command)
        {
            var storeIdsWithExistStoreSetting = await _retailDbContext.StoreSettings.Select(x => x.StoreId).ToListAsync();
            var storeIdsWithNotExistStoreSetting = await _retailDbContext.Stores
                                            .Where(x => !storeIdsWithExistStoreSetting.Contains(x.Id)).Select(x => x.Id).ToListAsync();

            if (storeIdsWithNotExistStoreSetting == null || !storeIdsWithNotExistStoreSetting.Any())
            {
                return new List<StoreSettingModel>();
            }

            var storeSettings = storeIdsWithNotExistStoreSetting.Select(x => new StoreSetting
            {
                StoreId = x
            });

            await _retailDbContext.StoreSettings.AddRangeAsync(storeSettings);
            await _retailDbContext.SaveChangesAsync();

            return storeSettings.Select(x => new StoreSettingModel
            {
                Id = x.Id,
                StoreId = x.StoreId
            }).ToList();
        }
    }
}
