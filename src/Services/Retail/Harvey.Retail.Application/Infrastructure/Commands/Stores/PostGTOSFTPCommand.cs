﻿using Harvey.Domain;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Services;
using Harvey.Retail.Application.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Commands.Stores
{
    public class PostGTOSFTPCommand : ICommand<bool>
    {
        public PostGTOSFTPRequest Request { get; }

        public PostGTOSFTPCommand(PostGTOSFTPRequest request)
        {
            Request = request;
        }
    }

    public class PostGTOSFTPCommandHandler : ICommandHandler<PostGTOSFTPCommand, bool>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly CatalogContext _catalogContext;
        private readonly INextSequenceService<TransientRetailDbContext> _nextSequenceService;
        private readonly SaleTransactionService _saleTransactionService;

        public PostGTOSFTPCommandHandler(
            RetailDbContext retailDbContext,
            CatalogContext catalogContext,
            INextSequenceService<TransientRetailDbContext> nextSequenceService,
            SaleTransactionService saleTransactionService
            )
        {
            _nextSequenceService = nextSequenceService;
            _retailDbContext = retailDbContext;
            _catalogContext = catalogContext;
            _saleTransactionService = saleTransactionService;
        }

        public async Task<bool> Handle(PostGTOSFTPCommand command)
        {
            var requestItems = command.Request.PostGTOSFTPRequestItems;

            List<StoreSetting> storeSettings;
            if (requestItems != null && requestItems.Any())
            {
                var machineIds = requestItems.Select(x => x.MachineId);
                storeSettings = await _retailDbContext.StoreSettings.AsNoTracking().Where(x => machineIds.Contains(x.MachineId)).ToListAsync();
            }
            else
            {
                storeSettings = await _retailDbContext.StoreSettings.AsNoTracking().ToListAsync();
                requestItems = new List<PostGTOSFTPRequestItem>();
            }

            var requestFromDate = command.Request.FromDate;
            var requestToDate = command.Request.ToDate;
            var requestReportType = command.Request.ReportType;

            foreach (var storeSetting in storeSettings)
            {
                if (!string.IsNullOrEmpty(storeSetting.AccountId)
                    && !string.IsNullOrEmpty(storeSetting.ServerIP)
                    && !string.IsNullOrEmpty(storeSetting.Password)
                    && storeSetting.Port > 0
                    )
                {
                    try
                    {
                        var channel = await _retailDbContext.ChannelStoreAssignments.FirstOrDefaultAsync(x => x.StoreId == storeSetting.StoreId);
                        if (channel == null)
                        {
                            continue;
                        }
                        var channelId = channel.ChannelId;
                        var channelServerInformation = (await _retailDbContext.Channels.FirstOrDefaultAsync(x => x.Id == channelId))?.ServerInformation ?? "";
                        if (string.IsNullOrEmpty(channelServerInformation))
                        {
                            continue;
                        }

                        var fromDate = requestFromDate;
                        var toDate = requestToDate;
                        var reportType = requestReportType;
                        var requestItem = requestItems.FirstOrDefault(x => x.MachineId == storeSetting.MachineId);
                        if (requestItem != null)
                        {
                            fromDate = requestItem.FromDate;
                            toDate = requestItem.ToDate;
                            reportType = requestItem.ReportType;
                        }

                        var data = await _saleTransactionService.GetSaleTransactionsForGTOReportWithNotPrefixFileName(new Application.Models.SaleTransactionForGTOReportModel
                        {
                            LocationId = storeSetting.StoreId.ToString(),
                            MachineId = storeSetting.MachineId,
                            ReportType = reportType,
                            FromDate = fromDate,
                            ToDate = toDate
                        }, channelServerInformation);
                        var result = data.Item1;
                        var fileName = data.Item2;
                        var extentionFile = data.Item3;

                        GTOIntergrationService.SFTPUploadFile(result, $"{fileName}", new Configs.SFTPConfig
                        {
                            Host = storeSetting.ServerIP,
                            Port = storeSetting.Port,
                            UserName = storeSetting.AccountId,
                            Password = EnDecryptUtility.Decrypt(storeSetting.Password),
                            FolderPath = storeSetting.FolderPath
                        });
                    }
                    catch (System.Exception e)
                    {

                    }
                }
            }

            return true;
        }
    }

    public class PostGTOSFTPRequest
    {
        public string ReportType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public List<PostGTOSFTPRequestItem> PostGTOSFTPRequestItems { get; set; }
    }

    public class PostGTOSFTPRequestItem
    {
        public string MachineId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ReportType { get; set; }
    }
}
