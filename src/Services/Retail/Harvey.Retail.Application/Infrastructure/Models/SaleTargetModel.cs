﻿using System;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class SaleTargetModel
    {
        public Guid Id { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public float Target { get; set; }
        public Guid StoreId { get; set; }
        public bool IsDelete { get; set; }
    }

    public class AddSaleTargetModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public float Target { get; set; }
        public Guid StoreId { get; set; }
    }

    public class UpdateSaleTargetModel
    {
        public Guid Id { get; set; }
        public float Target { get; set; }
    }
}
