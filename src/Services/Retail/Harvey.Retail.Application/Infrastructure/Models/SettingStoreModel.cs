﻿using AutoMapper;
using Harvey.Retail.Application.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class StoreSettingModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string DevideCode { get; set; }
        public string DevideName { get; set; }
        public string Mall { get; set; }
        public string MachineId { get; set; }
        public string AccountId { get; set; }
        public string ServerIP { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string FolderPath { get; set; }
        public string StoreName { get; set; }
        public Guid StoreId { get; set; }
    }

    public class StoreSettingProfile : Profile
    {
        public StoreSettingProfile()
        {
            CreateMap<StoreSetting, StoreSettingModel>().ReverseMap();
        }
    }
}
