﻿using System;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class ReportSaleTarget
    {
        public Guid StoreId { get; set; }
        public string Store { get; set; }
        public float SaleTarget { get; set; }
        public float CurrentSale { get; set; }
        public float ShortFall { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
