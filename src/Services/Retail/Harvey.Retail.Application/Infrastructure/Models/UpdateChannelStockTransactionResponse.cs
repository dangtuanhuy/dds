﻿using Harvey.Retail.Application.Models;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    class UpdateChannelStockTransactionResponse
    {
        public string ServerInformation { get; set; }
        public List<saleStockTransactionModel> StockTransactionUpdatedRetailsModels { get; set; }
    }
}
