﻿using Harvey.Retail.Application.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class SynchronizationItemModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public SynchAction Action { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public SynchEntity Type { get; set; }
        public dynamic Entity { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
