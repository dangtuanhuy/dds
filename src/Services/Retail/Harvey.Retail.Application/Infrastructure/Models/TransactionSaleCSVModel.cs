﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class TransactionSaleCSVModel
    {
        public string StoreName { get; set; }
        public Guid OrderId { get; set; }
        public string ProductName { get; set; }
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
        public string VariantCode { get; set; }
        public float UnitPrice { get; set; }
        public int Quantity { get; set; }
        public float Amount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string POSDeviceNumber { get; set; }
        public string BillNo { get; set; }
        public Guid CashierId { get; set; }
        public string CashierName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
