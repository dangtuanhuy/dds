﻿using Harvey.Retail.Application.Domain;
using System;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class DeviceModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid StoreId { get; set; }
        public string StoreName { get; set; }
        public string DeviceCode { get; set; }
        public string UserCode { get; set; }
        public bool Trusted { get; set; }
        public DeviceStatus Status { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
