﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class TopProductSaleRequestModel
    {
        public string ToDate { get; set; }
        public string FromDate { get; set; }
    }
    public class TopProductSaleModel
    {
        public Guid VariantId { get; set; }
        public int TotalQuantity { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
