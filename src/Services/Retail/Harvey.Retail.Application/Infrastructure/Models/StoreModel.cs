﻿using Harvey.Retail.Application.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class StoreModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public IList<SaleTargetModel> SaleTargets { get; set; }
    }

    public class GetStoreModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
