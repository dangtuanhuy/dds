﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Infrastructure.Models
{
    public class StockModel
    {
        public Guid LocationId { get; set; }
        public Guid VariantId { get; set; }
        public string LocationName { get; set; }
        public int VariantQuantity { get; set; }
        public float ListPrice { get; set; }
        public float StaffPrice { get; set; }
        public float MemberPrice { get; set; }
    }
}
