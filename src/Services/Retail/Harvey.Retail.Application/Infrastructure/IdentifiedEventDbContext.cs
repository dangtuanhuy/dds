﻿using Harvey.EventBus;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Infrastructure
{
    public class IdentifiedEventDbContext : DbContext
    {
        public DbSet<IdentifiedEvent> IdentifiedEvents { get; set; }
        public IdentifiedEventDbContext(DbContextOptions<IdentifiedEventDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<IdentifiedEvent>());
        }

        public void Setup(EntityTypeBuilder<IdentifiedEvent> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
    }
}
