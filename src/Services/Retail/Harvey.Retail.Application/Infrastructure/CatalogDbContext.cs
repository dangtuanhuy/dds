﻿using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace Harvey.Retail.Application.Infrastructure
{
    public class CatalogDbContext : DbContext
    {
        public CatalogDbContext(DbContextOptions<CatalogDbContext> options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<BarCode> BarCodes { get; set; }
        public DbSet<Variant> Variants { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<CatalogFieldValue> FieldValues { get; set; }
        public DbSet<Synchronization> Synchronizations { get; set; }
        public DbSet<StockTransaction> StockTransactions { get; set; }
        public DbSet<StockType> StockTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderPromotion> OrderPromotions { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<CatalogOrderItemPromotion> OrderItemPromotions { get; set; }
        public DbSet<Domain.Payment> Payments { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<PromotionDetail> PromotionDetails { get; set; }
        public DbSet<PromotionCouponCode> PromotionCouponCodes { get; set; }
        public DbSet<PromotionCondition> PromotionConditions { get; set; }
        public DbSet<CatalogPaymentMethod> PaymentMethods { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<Order>());
        }

        public void Setup(EntityTypeBuilder<Order> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.ChannelContext);
        }
    }
}
