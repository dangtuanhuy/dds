﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure
{
    public class IdentifiedEventDbContextDataSeed
    {
        public Task SeedAsync(IdentifiedEventDbContext context, ILogger<IdentifiedEventDbContextDataSeed> logger)
        {
            return Task.CompletedTask;
        }
    }
}
