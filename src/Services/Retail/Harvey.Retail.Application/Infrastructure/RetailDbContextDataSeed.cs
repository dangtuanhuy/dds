﻿using Harvey.Polly;
using Harvey.Retail.Application.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure
{
    public class RetailDbContextDataSeed
    {
        public async Task SeedAsync(RetailDbContext context, ILogger<RetailDbContextDataSeed> logger)
        {
            var policy = new DataSeedRetrivalPolicy();
            await policy.ExecuteStrategyAsync(logger, () =>
            {
                using (context)
                {
                    if (!context.Currency.Any())
                    {
                        context.Currency.AddRange(SeedDataForCurrency());
                    }
                    context.SaveChanges();
                }
            });
        }

        private List<Currency> SeedDataForCurrency()
        {
            return new List<Currency>()
            {
                new Currency()
                {
                    Value = 100
                },
                new Currency()
                {
                    Value = 50
                },
                new Currency()
                {
                    Value = 20
                },
                new Currency()
                {
                    Value = 10
                },
                new Currency()
                {
                    Value = 5
                },
                new Currency()
                {
                    Value = 1
                },
                new Currency()
                {
                    Value = 0.50m
                },
                new Currency()
                {
                    Value = 0.10m
                },
                new Currency()
                {
                    Value = 0.05m
                },
                new Currency()
                {
                    Value = 0.01m
                },
            };
        }
    }
}
