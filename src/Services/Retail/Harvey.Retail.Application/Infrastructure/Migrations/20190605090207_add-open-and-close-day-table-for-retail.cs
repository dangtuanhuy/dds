﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public partial class addopenandclosedaytableforretail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CatelogPaymentMethod",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CatelogPaymentMethod", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "POSEndDays",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    CashierName = table.Column<string>(nullable: true),
                    CashierId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_POSEndDays", x => x.Id);
                    table.ForeignKey(
                        name: "FK_POSEndDays_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "POSOpenDays",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    CashierName = table.Column<string>(nullable: true),
                    CashierId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_POSOpenDays", x => x.Id);
                    table.ForeignKey(
                        name: "FK_POSOpenDays_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "POSCashItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CurrencyId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    DocumentId = table.Column<Guid>(nullable: false),
                    DocumentStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_POSCashItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_POSCashItems_Currency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EndDayPaymentItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    POSEndDayId = table.Column<Guid>(nullable: false),
                    PaymentMethodId = table.Column<Guid>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EndDayPaymentItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EndDayPaymentItems_POSEndDays_POSEndDayId",
                        column: x => x.POSEndDayId,
                        principalTable: "POSEndDays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EndDayPaymentItems_CatelogPaymentMethod_PaymentMethodId",
                        column: x => x.PaymentMethodId,
                        principalTable: "CatelogPaymentMethod",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EndDayPaymentItems_POSEndDayId",
                table: "EndDayPaymentItems",
                column: "POSEndDayId");

            migrationBuilder.CreateIndex(
                name: "IX_EndDayPaymentItems_PaymentMethodId",
                table: "EndDayPaymentItems",
                column: "PaymentMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_POSCashItems_CurrencyId",
                table: "POSCashItems",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_POSEndDays_StoreId",
                table: "POSEndDays",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_POSOpenDays_StoreId",
                table: "POSOpenDays",
                column: "StoreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EndDayPaymentItems");

            migrationBuilder.DropTable(
                name: "POSCashItems");

            migrationBuilder.DropTable(
                name: "POSOpenDays");

            migrationBuilder.DropTable(
                name: "POSEndDays");

            migrationBuilder.DropTable(
                name: "CatelogPaymentMethod");

            migrationBuilder.DropTable(
                name: "Currency");
        }
    }
}
