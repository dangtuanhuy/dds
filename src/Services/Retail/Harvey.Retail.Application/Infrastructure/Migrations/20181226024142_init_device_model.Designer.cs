﻿// <auto-generated />
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Harvey.Retail.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    [DbContext(typeof(RetailDbContext))]
    [Migration("20181226024142_init_device_model")]
    partial class init_device_model
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Harvey.Retail.Application.Domain.Channel", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("ServerInformation");

                    b.HasKey("Id");

                    b.ToTable("Channels");
                });

            modelBuilder.Entity("Harvey.Retail.Application.Domain.ChannelStoreAssignment", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ChannelId");

                    b.Property<Guid>("StoreId");

                    b.HasKey("Id");

                    b.HasIndex("StoreId")
                        .IsUnique();

                    b.ToTable("ChannelStoreAssignments");
                });

            modelBuilder.Entity("Harvey.Retail.Application.Domain.Device", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("DeviceCode");

                    b.Property<string>("Name");

                    b.Property<Guid>("StoreId");

                    b.Property<bool>("Trusted");

                    b.Property<string>("UserCode");

                    b.HasKey("Id");

                    b.ToTable("Devices");
                });

            modelBuilder.Entity("Harvey.Retail.Application.Domain.Store", b =>
                {
                    b.Property<Guid>("Id")
                        .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

                    b.Property<string>("Address");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Stores");
                });
#pragma warning restore 612, 618
        }
    }
}
