﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public partial class adddevideIdnoteforenddayopenday : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DeviceId",
                table: "POSOpenDays",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "DeviceId",
                table: "POSEndDays",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "POSEndDays",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeviceId",
                table: "POSOpenDays");

            migrationBuilder.DropColumn(
                name: "DeviceId",
                table: "POSEndDays");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "POSEndDays");
        }
    }
}
