﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public partial class addpricecolumnorderitemdetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "OrderItemDetails",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "OrderItemDetails",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_OrderItemDetails_StockTypeId",
                table: "OrderItemDetails",
                column: "StockTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderItemDetails_StockTypes_StockTypeId",
                table: "OrderItemDetails",
                column: "StockTypeId",
                principalTable: "StockTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderItemDetails_StockTypes_StockTypeId",
                table: "OrderItemDetails");

            migrationBuilder.DropIndex(
                name: "IX_OrderItemDetails_StockTypeId",
                table: "OrderItemDetails");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "OrderItemDetails");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "OrderItemDetails");
        }
    }
}
