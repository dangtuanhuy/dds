﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public partial class addsaletargetunique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SaleTargetHistory_StoreId",
                table: "SaleTargetHistory");

            migrationBuilder.CreateIndex(
                name: "IX_SaleTargetHistory_StoreId_FromDate_ToDate",
                table: "SaleTargetHistory",
                columns: new[] { "StoreId", "FromDate", "ToDate" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SaleTargetHistory_StoreId_FromDate_ToDate",
                table: "SaleTargetHistory");

            migrationBuilder.CreateIndex(
                name: "IX_SaleTargetHistory_StoreId",
                table: "SaleTargetHistory",
                column: "StoreId");
        }
    }
}
