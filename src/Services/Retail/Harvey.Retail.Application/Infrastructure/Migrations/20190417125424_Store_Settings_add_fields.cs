﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public partial class Store_Settings_add_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountId",
                table: "StoreSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FolderPath",
                table: "StoreSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Mall",
                table: "StoreSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "StoreSettings",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Port",
                table: "StoreSettings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ServerIP",
                table: "StoreSettings",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "StoreSettings");

            migrationBuilder.DropColumn(
                name: "FolderPath",
                table: "StoreSettings");

            migrationBuilder.DropColumn(
                name: "Mall",
                table: "StoreSettings");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "StoreSettings");

            migrationBuilder.DropColumn(
                name: "Port",
                table: "StoreSettings");

            migrationBuilder.DropColumn(
                name: "ServerIP",
                table: "StoreSettings");
        }
    }
}
