﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public partial class addfieldsaletarget : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SaleTargetHistory_StoreId_FromDate_ToDate",
                table: "SaleTargetHistory");

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                table: "SaleTargetHistory",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "SaleTargetHistory",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "SaleTargetHistory",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedBy",
                table: "SaleTargetHistory",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "SaleTargetHistory",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_SaleTargetHistory_StoreId",
                table: "SaleTargetHistory",
                column: "StoreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SaleTargetHistory_StoreId",
                table: "SaleTargetHistory");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "SaleTargetHistory");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "SaleTargetHistory");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "SaleTargetHistory");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "SaleTargetHistory");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "SaleTargetHistory");

            migrationBuilder.CreateIndex(
                name: "IX_SaleTargetHistory_StoreId_FromDate_ToDate",
                table: "SaleTargetHistory",
                columns: new[] { "StoreId", "FromDate", "ToDate" },
                unique: true);
        }
    }
}
