﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public partial class device_add_status_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Devices",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Devices");
        }
    }
}
