﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public partial class addtablepaymentmethodretail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EndDayPaymentItems_CatelogPaymentMethod_PaymentMethodId",
                table: "EndDayPaymentItems");

            migrationBuilder.DropTable(
                name: "CatelogPaymentMethod");

            migrationBuilder.CreateTable(
                name: "PaymentMethod",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethod", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_EndDayPaymentItems_PaymentMethod_PaymentMethodId",
                table: "EndDayPaymentItems",
                column: "PaymentMethodId",
                principalTable: "PaymentMethod",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EndDayPaymentItems_PaymentMethod_PaymentMethodId",
                table: "EndDayPaymentItems");

            migrationBuilder.DropTable(
                name: "PaymentMethod");

            migrationBuilder.CreateTable(
                name: "CatelogPaymentMethod",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CatelogPaymentMethod", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_EndDayPaymentItems_CatelogPaymentMethod_PaymentMethodId",
                table: "EndDayPaymentItems",
                column: "PaymentMethodId",
                principalTable: "CatelogPaymentMethod",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
