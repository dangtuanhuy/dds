﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Harvey.Retail.Application.Infrastructure.Migrations
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<RetailDbContext>
    {
        public RetailDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<RetailDbContext>();
            var connectionString = "Server=localhost;port=5432;Database=harveyretail;UserId=postgres;Password=123456";
            builder.UseNpgsql(connectionString);
            return new RetailDbContext(builder.Options);
        }
    }
}
