﻿using Harvey.Domain;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Enums;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.TopProductBySales
{
    public sealed class PimGetProductBySalesQuery : IQuery<IEnumerable<TopProductSaleModel>>
    {
        public string FromDate { get; }
        public string ToDate { get; }
        public PimGetProductBySalesQuery(string fromDate, string toDate)
        {
            FromDate = fromDate;
            ToDate = toDate;
        }
    }

    public sealed class PimGetProductBySalesQueryHandler : IQueryHandler<PimGetProductBySalesQuery, IEnumerable<TopProductSaleModel>>
    {
        private readonly RetailDbContext _retailDbContext;

        public PimGetProductBySalesQueryHandler(RetailDbContext retailDbContext)
        {
            _retailDbContext = retailDbContext;
        }
        public async Task<IEnumerable<TopProductSaleModel>> Handle(PimGetProductBySalesQuery query)
        {
            DateTime fromDate = DateTime.Parse(query.FromDate);
            DateTime toDate = DateTime.Parse(query.ToDate);

            var orderItemDetails = await (from o in _retailDbContext.OrderItemDetails
                                          where (o.CreatedDate <= toDate && o.CreatedDate >= fromDate && o.Status != OrderItemStatus.Old)
                                          select o).ToListAsync();

            var groupOrderItemByVariant = from orderItemDetail in orderItemDetails
                                          group orderItemDetail by new { orderItemDetail.VariantId, orderItemDetail.Status, orderItemDetail.Price } into g
                                          select new
                                          {
                                              g.Key.VariantId,
                                              g.Key.Status,
                                              quantity = g.Sum(x => x.Quantity),
                                              totalPrice = g.Sum(x => x.Quantity) * g.Key.Price,
                                          };

            var groupOrderItemsByPrice = from g in groupOrderItemByVariant
                                         group g by new { g.VariantId, g.Status } into gg
                                         select new
                                         {
                                             gg.Key.VariantId,
                                             quantity = gg.Sum(x => x.quantity),
                                             totalPrice = gg.Sum(x => x.totalPrice),
                                             gg.Key.Status,
                                         };

            var result = from a in groupOrderItemsByPrice
                         group a by a.VariantId into gg
                         let totalQuantity = gg.Where(x => x.Status == OrderItemStatus.Normal).Sum(x => x.quantity) - gg.Where(x => x.Status != OrderItemStatus.Normal).Sum(x => x.quantity)
                         let totalPrice = gg.Where(x => x.Status == OrderItemStatus.Normal).Sum(x => x.totalPrice) - gg.Where(x => x.Status != OrderItemStatus.Normal).Sum(x => x.totalPrice)
                         select new TopProductSaleModel
                         {
                             VariantId = gg.Key,
                             TotalPrice = (decimal)totalPrice,
                             TotalQuantity = totalQuantity
                         };

            return result.OrderByDescending(x => x.TotalQuantity).Take(5);
        }
    }
}
