﻿using Harvey.Domain;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stock
{
    public class GetStocksByLocationQuery : IQuery<List<StockModel>>
    {
        public List<Guid> VariantIds { get; set; }
        public GetStocksByLocationQuery(List<Guid> variantIds)
        {
            VariantIds = variantIds;
        }
    }

    public class GetStocksByLocationQueryHandler : IQueryHandler<GetStocksByLocationQuery, List<StockModel>>
    {
        private readonly ChannelContext _channelContext;
        private readonly RetailDbContext _retailDbContext;

        public GetStocksByLocationQueryHandler(ChannelContext channelContext, RetailDbContext retailDbContext)
        {
            _channelContext = channelContext;
            _retailDbContext = retailDbContext;
        }

        public async Task<List<StockModel>> Handle(GetStocksByLocationQuery query)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(_channelContext.ServerInformation);
            List<StockModel> result = new List<StockModel>();

            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                var variantIds = query.VariantIds;
                var variants = await dbContext.Variants.Where(v => variantIds.Contains(v.Id)).ToListAsync();
                var priceIds = variants.Select(x => x.PriceId);

                var stockTransactions = await (from s in dbContext.StockTransactions
                                               where variantIds.Contains(s.VariantId)
                                               group s by new { s.VariantId, s.LocationId } into gs
                                               let fdata = gs.OrderByDescending(x => x.CreatedDate).FirstOrDefault()
                                               select new
                                               {
                                                   key = gs.Key,
                                                   locationId = fdata.LocationId,
                                                   latestBalance = fdata.Balance
                                               }).ToArrayAsync();
                var locationIds = stockTransactions.Select(x => x.locationId);
                var locations = await _retailDbContext.Stores.Where(x => locationIds.Contains(x.Id))
                                            .Select(x => new
                                            {
                                                x.Id,
                                                x.Name
                                            }).ToListAsync();

                var prices = await dbContext.Prices.Where(p => priceIds.Contains(p.Id)).ToListAsync();

                foreach (var item in stockTransactions)
                {
                    var locationId = item.locationId;
                    var variant = variants.SingleOrDefault(x => x.Id == item.key.VariantId);
                    var itemPrice = prices.SingleOrDefault(p => p.Id == variant.PriceId);
                    var locationName = locations.SingleOrDefault(x => x.Id == locationId)?.Name ?? "";

                    result.Add(new StockModel()
                    {
                        VariantId = item.key.VariantId,
                        MemberPrice = itemPrice.MemberPrice,
                        ListPrice = itemPrice.ListPrice,
                        StaffPrice = itemPrice.StaffPrice,
                        VariantQuantity = item.latestBalance,
                        LocationId = locationId,
                        LocationName = locationName
                    });
                }
            }

            return result;
        }
    }
}
