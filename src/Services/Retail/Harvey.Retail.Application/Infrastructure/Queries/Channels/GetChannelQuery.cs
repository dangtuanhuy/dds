﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Channels
{
    public class GetChannelQuery: IQuery<IList<ChannelModel>>
    {
        public GetChannelQuery()
        {

        }
    }

    public class GetChannelQueryHandler : IQueryHandler<GetChannelQuery, IList<ChannelModel>>
    {
        private readonly IEfRepository<TransientRetailDbContext, Channel> _channelRepository;
        private readonly IMapper _mapper;
        public GetChannelQueryHandler(IEfRepository<TransientRetailDbContext, Channel> channelRepository, IMapper mapper)
        {
            _channelRepository = channelRepository;
            _mapper = mapper;
        }
        public async Task<IList<ChannelModel>> Handle(GetChannelQuery query)
        {
            var channels = await _channelRepository.GetAsync();
            return _mapper.Map<IList<ChannelModel>>(channels);
        }
    }
}
