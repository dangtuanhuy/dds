﻿using Harvey.Domain;
using Harvey.Retail.Application.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Channels
{
    public class CountChannelDataQuery : IQuery<int>
    {
        public string ChannelName { get; }
        public CountChannelDataType DataType { get; }
        public CountChannelDataQuery(string channelName, CountChannelDataType dataType)
        {
            ChannelName = channelName;
            DataType = dataType;
        }
    }

    public class CountChannelDataQueryHandler : IQueryHandler<CountChannelDataQuery, int>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly CatalogContext _catalogContext;
        public CountChannelDataQueryHandler(RetailDbContext retailDbContext, CatalogContext catalogContext)
        {
            _retailDbContext = retailDbContext;
            _catalogContext = catalogContext;
        }

        public async Task<int> Handle(CountChannelDataQuery query)
        {
            var channel = await _retailDbContext.Channels.FirstOrDefaultAsync(x => x.Name == query.ChannelName);
            if (channel == null)
            {
                return -1;
            }

            var serverInformation = channel.ServerInformation;
            var numberOfItems = 0;
            
            switch (query.DataType)
            {
                case CountChannelDataType.Product:
                    numberOfItems = await _catalogContext.CountProductsAsync(serverInformation);
                    break;
                case CountChannelDataType.Variant:
                    numberOfItems = await _catalogContext.CountVariantsAsync(serverInformation);
                    break;
                default:
                    numberOfItems = -2;
                    break;
            }

            return numberOfItems;
        }
    }

    public enum CountChannelDataType
    {
        Product = 1,
        Variant = 2
    }
}
