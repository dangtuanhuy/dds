﻿using Harvey.Domain;
using Harvey.Domain.Constant;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stores
{
    public sealed class GetStoresQuery : IQuery<PagedResult<StoreSettingModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public GetStoresQuery(PagingFilterCriteria pagingFilterCriteria)
        {
            PagingFilterCriteria = pagingFilterCriteria;
        }
    }

    public sealed class GetStoresQueryHandler : IQueryHandler<GetStoresQuery, PagedResult<StoreSettingModel>>
    {
        private readonly RetailDbContext _retailDbContext;

        public GetStoresQueryHandler(RetailDbContext retailDbContext)
        {
            _retailDbContext = retailDbContext;
        }
        public async Task<PagedResult<StoreSettingModel>> Handle(GetStoresQuery query)
        {
            var devides = _retailDbContext.Devices.AsNoTracking();
            var storeSettings = _retailDbContext.StoreSettings.AsNoTracking().Include(x => x.Store);

            var result = devides
                    .Join(storeSettings,
                    d => d.StoreId,
                    s => s.StoreId,
                    (d, s) => new { d, s })
                    .Where(sc => sc.d.Status != DeviceStatus.Deleted)
                    .Select(sc => new
                    {
                        Device = sc.d,
                        StoreSettings = sc.s
                    });

            var res = result.Select(x => new StoreSettingModel
            {
                Id = x.StoreSettings.Store != null ? x.StoreSettings.Store.Id : Guid.NewGuid(),
                Name = x.Device.Name,
                DevideCode = x.Device.DeviceCode,
                StoreName = x.StoreSettings.Store != null ? x.StoreSettings.Store.Name : "",
                Mall = x.StoreSettings.Mall,
                MachineId = x.StoreSettings.MachineId,
                ServerIP = x.StoreSettings.ServerIP,
                AccountId = x.StoreSettings.AccountId,
                Password = "",
                Port = x.StoreSettings.Port,
                FolderPath = x.StoreSettings.FolderPath,
                Address = x.StoreSettings.Store != null ? x.StoreSettings.Store.Address : "",
                DevideName = x.Device.Name
            });

            if (!string.IsNullOrEmpty(query.PagingFilterCriteria.SortColumn))
            {
                res = query.PagingFilterCriteria.SortDirection == SortDirection.Ascending
                    ? res.OrderBy(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn))
                    : res.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn));
            }
            else
            {
                PropertyInfo propertyInfo = 
                    res.GetType().GetGenericArguments()[0].GetProperty(SortColumnConstants.DefaultSortColumn);
                if (propertyInfo != null)
                {
                    res = res.OrderByDescending(x => x.DevideName);
                }
                else
                {
                    res = res.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, SortColumnConstants.SecondSortColumn));
                }
            }

            var entities = res
                .Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
                .Take(query.PagingFilterCriteria.NumberItemsPerPage)
                .ToList();

            var totelPages = res.Count();


            return new PagedResult<StoreSettingModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totelPages,
                Data = entities
            };
        }
    }
}
