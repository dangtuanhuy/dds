﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Harvey.Retail.Application.Domain;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stores
{
    public sealed class GetStoreManagementByIdQuery : IQuery<StoreSettingModel>
    {
        public Guid Id { get; set; }
        public GetStoreManagementByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetStoreManagementByIdQueryHandler : IQueryHandler<GetStoreManagementByIdQuery, StoreSettingModel>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IMapper _mapper;
        public GetStoreManagementByIdQueryHandler(RetailDbContext retailDbContext,
            IMapper mapper)
        {
            _retailDbContext = retailDbContext;
            _mapper = mapper;
        }
        public async Task<StoreSettingModel> Handle(GetStoreManagementByIdQuery query)
        {
            var storeSetting = await _retailDbContext.StoreSettings.Include(s => s.Store).FirstOrDefaultAsync(x => x.StoreId == query.Id);
            var device = await _retailDbContext.Devices.FirstOrDefaultAsync(x => x.StoreId == storeSetting.StoreId && x.Status != DeviceStatus.Deleted);
            var result = new StoreSettingModel()
            {
                Id = storeSetting.Store != null ? storeSetting.Store.Id : Guid.NewGuid(),
                Name = device.Name,
                DevideCode = device.DeviceCode,
                StoreName = storeSetting.Store != null ? storeSetting.Store.Name : "",
                MachineId = storeSetting.MachineId,
                Address = storeSetting.Store != null ? storeSetting.Store.Address : "",
                DevideName = device.Name
            };
            return _mapper.Map<StoreSettingModel>(result);
        }
    }
}
