﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stores
{
    public class GetAllStoreToSyncPOSQuery : IQuery<IList<GetAllStoreToSyncPOS>>
    {
    }

    public class GetAllStoreToSyncPOSQueryHandler : IQueryHandler<GetAllStoreToSyncPOSQuery, IList<GetAllStoreToSyncPOS>>
    {
        private readonly IEfRepository<RetailDbContext, Store> _repository;
        public GetAllStoreToSyncPOSQueryHandler(IEfRepository<RetailDbContext, Store> repository)
        {
            _repository = repository;
        }
        public async Task<IList<GetAllStoreToSyncPOS>> Handle(GetAllStoreToSyncPOSQuery query)
        {
            return (await _repository.GetAsync()).Select(x => new GetAllStoreToSyncPOS
            {
                Id = x.Id,
                Name = x.Name,
                Address = x.Address ?? ""
            }).ToList();
        }
    }

    public class GetAllStoreToSyncPOS
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
