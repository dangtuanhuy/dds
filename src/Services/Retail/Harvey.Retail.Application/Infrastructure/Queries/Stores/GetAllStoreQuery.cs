﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stores
{
    public class GetAllStoreQuery: IQuery<IList<GetStoreModel>>
    {
    }
    public class GetAllStoreQueryHandler : IQueryHandler<GetAllStoreQuery, IList<GetStoreModel>>
    {
        private readonly IEfRepository<RetailDbContext, Store> _repository;
        private readonly IMapper _mapper;
        public GetAllStoreQueryHandler(IEfRepository<RetailDbContext, Store> repository , IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<IList<GetStoreModel>> Handle(GetAllStoreQuery query)
        {
            var stores = await _repository.GetAsync();
            return _mapper.Map<List<GetStoreModel>>(stores);
        }
    }
}
