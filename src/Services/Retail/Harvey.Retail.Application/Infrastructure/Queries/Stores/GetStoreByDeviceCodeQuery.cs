﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stores
{
    public class GetStoreByDeviceCodeQuery : IQuery<StoreModel>
    {
        public string DeviceCode { get; }
        public GetStoreByDeviceCodeQuery(string deviceCode)
        {
            DeviceCode = deviceCode;
        }
    }

    public sealed class GetStoreByDeviceCodeQueryHandler : IQueryHandler<GetStoreByDeviceCodeQuery, StoreModel>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IMapper _mapper;
        public GetStoreByDeviceCodeQueryHandler(RetailDbContext retailDbContext,
            IMapper mapper)
        {
            _retailDbContext = retailDbContext;
            _mapper = mapper;
        }

        public async Task<StoreModel> Handle(GetStoreByDeviceCodeQuery query)
        {
            var device = await _retailDbContext.Devices.FirstOrDefaultAsync(x => x.DeviceCode == query.DeviceCode);
            if (device == null)
            {
                return null;
            }

            var store = await _retailDbContext.Stores.FirstOrDefaultAsync(x => x.Id == device.StoreId);

            return new StoreModel
            {
                Id = store.Id,
                Address = store.Address,
                Name = store.Name
            };
        }
    }

}
