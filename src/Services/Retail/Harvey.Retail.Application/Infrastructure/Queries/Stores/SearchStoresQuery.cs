﻿using Harvey.Domain;
using Harvey.Extensions;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stores
{
    public class SearchStoresQuery : IQuery<IEnumerable<StoreModel>>
    {
        private readonly string Text;

        public SearchStoresQuery(string text)
        {
            Text = text;
        }

        public class SearchStoresQueryHandler : IQueryHandler<SearchStoresQuery, IEnumerable<StoreModel>>
        {
            private readonly IEfRepository<RetailDbContext, Store> _repository;

            public SearchStoresQueryHandler(IEfRepository<RetailDbContext, Store> repository)
            {
                _repository = repository;
            }

            public async Task<IEnumerable<StoreModel>> Handle(SearchStoresQuery query)
            {
                var predicate = PredicateBuilder.True<Store>();
                if (!string.IsNullOrEmpty(query.Text))
                {
                    predicate = predicate.And(x => x.Name.ToUpper().Contains(query.Text.ToUpper()));
                }

                var stores = await _repository.ListAsync(predicate);
                return stores.Select(x =>
                                    new StoreModel()
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        Address = x.Address
                                    });
            }
        }
    }
}
