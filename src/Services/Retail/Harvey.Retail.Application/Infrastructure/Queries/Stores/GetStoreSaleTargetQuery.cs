﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stores
{
    public class GetStoreSaleTargetQuery : IQuery<PagedResult<StoreModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public GetStoreSaleTargetQuery(PagingFilterCriteria pagingFilterCriteria)
        {
            PagingFilterCriteria = pagingFilterCriteria;
        }
    }
    public sealed class GetStoreSaleTargetQueryHandler : IQueryHandler<GetStoreSaleTargetQuery, PagedResult<StoreModel>>
    {
        private readonly IEfRepository<RetailDbContext, Store> _repository;
        private readonly IEfRepository<RetailDbContext, SaleTarget> _saleTargetRepository;
        private readonly IMapper _mapper;
        public GetStoreSaleTargetQueryHandler(IEfRepository<RetailDbContext, Store> repository, 
                                              IEfRepository<RetailDbContext, SaleTarget> saleTargetRepository, 
                                              IMapper mapper)
        {
            _repository = repository;
            _saleTargetRepository = saleTargetRepository;
            _mapper = mapper;
        }
        public async Task<PagedResult<StoreModel>> Handle(GetStoreSaleTargetQuery query)
        {
            var stores = await _repository.GetAsync(query.PagingFilterCriteria.Page, query.PagingFilterCriteria.NumberItemsPerPage);
            var task = new List<Task>();

            var result = new List<StoreModel>();
            foreach (var store in stores)
            {
                var saleTargets = await _saleTargetRepository.ListAsync(x => x.StoreId == store.Id && x.IsDelete == false);
                var storeModel = new StoreModel()
                {
                    Id = store.Id,
                    Name = store.Name,
                    Address = store.Address,
                    SaleTargets = saleTargets != null ? _mapper.Map<List<SaleTarget>, List<SaleTargetModel>>(saleTargets.OrderByDescending(x => x.ToDate).ToList()) : null
                };
                result.Add(storeModel);
            }


            return new PagedResult<StoreModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = await _repository.Count(),
                Data = result
            };
        }
    }
}
