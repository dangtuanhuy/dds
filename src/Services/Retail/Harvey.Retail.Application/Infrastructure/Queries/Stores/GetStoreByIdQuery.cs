﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Stores
{

    public sealed class GetStoreByIdQuery : IQuery<StoreSettingModel>
    {
        public Guid? Id { get; }
        public GetStoreByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetStoreByIdQueryHandler : IQueryHandler<GetStoreByIdQuery, StoreSettingModel>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IMapper _mapper;
        public GetStoreByIdQueryHandler(RetailDbContext retailDbContext,
            IMapper mapper)
        {
            _retailDbContext = retailDbContext;
            _mapper = mapper;
        }
        public async Task<StoreSettingModel> Handle(GetStoreByIdQuery query)
        {
            var storeSetting = await _retailDbContext.StoreSettings.Include(s => s.Store).FirstOrDefaultAsync(x => x.StoreId == query.Id);
            return _mapper.Map<StoreSettingModel>(storeSetting);
        }
    }
}
