﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Devices
{
    public class GetDeviceByDeviceCodeQuery : IQuery<DeviceModel>
    {
        public string DeviceCode { get; }
        public GetDeviceByDeviceCodeQuery(string deviceCode)
        {
            DeviceCode = deviceCode;
        }
    }

    public sealed class GetDeviceByDeviceCodeQueryHandler : IQueryHandler<GetDeviceByDeviceCodeQuery, DeviceModel>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IMapper _mapper;
        public GetDeviceByDeviceCodeQueryHandler(
            RetailDbContext retailDbContext,
            IMapper mapper)
        {
            _retailDbContext = retailDbContext;
            _mapper = mapper;
        }
        public async Task<DeviceModel> Handle(GetDeviceByDeviceCodeQuery query)
        {
            var device = await _retailDbContext.Devices.FirstOrDefaultAsync(x => x.DeviceCode == query.DeviceCode);
            if (device != null)
            {
                return new DeviceModel
                {
                    Id = device.Id,
                    Description = device.Description,
                    DeviceCode = device.DeviceCode,
                    Name = device.Name,
                    StoreId = device.StoreId,
                    Trusted = device.Trusted,
                    StoreName = "",
                    UserCode = device.UserCode,
                    CreatedBy = device.CreatedBy,
                    CreatedDate = device.CreatedDate,
                    UpdatedBy = device.UpdatedBy,
                    UpdatedDate = device.UpdatedDate
                };
            }
            return null;
        }
    }
}
