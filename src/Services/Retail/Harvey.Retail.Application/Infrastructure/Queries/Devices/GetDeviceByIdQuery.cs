﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Devices
{
    public sealed class GetDeviceByIdQuery : IQuery<DeviceModel>
    {
        public Guid Id { get; }
        public GetDeviceByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    public sealed class GetDeviceByIdQueryHandler : IQueryHandler<GetDeviceByIdQuery, DeviceModel>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IEfRepository<RetailDbContext, Device, DeviceModel> _repository;
        public GetDeviceByIdQueryHandler(IEfRepository<RetailDbContext, Device, DeviceModel> repository,
            RetailDbContext retailDbContext)
        {
            _repository = repository;
            _retailDbContext = retailDbContext;
        }
        public async Task<DeviceModel> Handle(GetDeviceByIdQuery query)
        {
            var device = await _retailDbContext.Devices.FirstOrDefaultAsync(x => x.Id == query.Id && x.Status != DeviceStatus.Deleted);
            if (device == null)
            {
                throw new System.InvalidOperationException($"Device with Id {query.Id} not found.");
            }
            var result = new DeviceModel
            {
                Id = device.Id,
                Description = device.Description,
                StoreId = device.StoreId,
                DeviceCode = device.DeviceCode,
                UserCode = device.UserCode,
                Trusted = device.Trusted,
                Name = device.Name,
                Status = device.Status,
                CreatedBy = device.CreatedBy,
                CreatedDate = device.CreatedDate,
                UpdatedBy = device.UpdatedBy,
                UpdatedDate = device.UpdatedDate
            };

            var store = await _retailDbContext.Stores.FirstOrDefaultAsync(x => x.Id == device.StoreId);
            if (store != null)
            {
                result.StoreName = store.Name;
            }

            return result;
        }
    }
}
