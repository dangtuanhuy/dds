﻿using Harvey.Domain;
using Harvey.Domain.Constant;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Devices
{
    public class GetDevicesQuery : IQuery<PagedResult<DeviceModel>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public string QueryText { get; }
        public GetDevicesQuery(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            QueryText = queryText;
        }
    }

    public sealed class GetDevicesQueryHandler : IQueryHandler<GetDevicesQuery, PagedResult<DeviceModel>>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IEfRepository<RetailDbContext, Device, DeviceModel> _repository;
        public GetDevicesQueryHandler(IEfRepository<RetailDbContext, Device, DeviceModel> repository,
            RetailDbContext retailDbContext)
        {
            _repository = repository;
            _retailDbContext = retailDbContext;
        }
        public async Task<PagedResult<DeviceModel>> Handle(GetDevicesQuery query)
        {
            var dataQuery = string.IsNullOrEmpty(query.QueryText)
                                ? _retailDbContext.Devices.Where(x => x.Status != DeviceStatus.Deleted)
                                : _retailDbContext.Devices.Where(x => x.Status != DeviceStatus.Deleted && x.Name.ToUpper().Contains(query.QueryText.ToUpper()));

            var result =  dataQuery
                            .AsNoTracking()
                            .Select(device => new
                            {
                                Device = device,
                                Store = _retailDbContext.Stores.FirstOrDefault(y => y.Id == device.StoreId)
                            })
                            .Select(x => new DeviceModel
                            {
                                Id = x.Device.Id,
                                Name = x.Device.Name,
                                StoreId = x.Device.StoreId,
                                Description = x.Device.Description,
                                DeviceCode = x.Device.DeviceCode,
                                Trusted = x.Device.Trusted,
                                UserCode = x.Device.UserCode,
                                Status = x.Device.Status,
                                StoreName = x.Store != null ? x.Store.Name : "",
                                CreatedBy = x.Device.CreatedBy,
                                CreatedDate = x.Device.CreatedDate,
                                UpdatedBy = x.Device.UpdatedBy,
                                UpdatedDate = x.Device.UpdatedDate
                            });

            if (!string.IsNullOrEmpty(query.PagingFilterCriteria.SortColumn))
            {
                result = query.PagingFilterCriteria.SortDirection == SortDirection.Ascending
                       ? result.OrderBy(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn))
                       : result.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, query.PagingFilterCriteria.SortColumn));
            }
            else
            {
                PropertyInfo propertyInfo =
                     result.GetType().GetGenericArguments()[0].GetProperty(SortColumnConstants.DefaultSortColumn);

                if (propertyInfo != null)
                {
                    result = result.OrderByDescending(a => a.UpdatedDate);
                }
                else
                {
                    result = result.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, SortColumnConstants.SecondSortColumn));
                }
            }

            var entities = result
                 .Skip((query.PagingFilterCriteria.Page - 1) * query.PagingFilterCriteria.NumberItemsPerPage)
                 .Take(query.PagingFilterCriteria.NumberItemsPerPage)
                 .ToList();

            var totalPages = result.Count();

            return new PagedResult<DeviceModel>()
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = totalPages,
                Data = entities
            };
        }
    }
}
