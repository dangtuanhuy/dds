﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Report
{
    public class GetSaleReportQuery : IQuery<PagedResult<ReportSaleTarget>>
    {
        public PagingFilterCriteria PagingFilterCriteria { get; }
        public Guid ChannelId { get; set; }
        public DateTime Date { get; set; }
        public GetSaleReportQuery(PagingFilterCriteria pagingFilterCriteria, Guid channelId, DateTime date)
        {
            PagingFilterCriteria = pagingFilterCriteria;
            ChannelId = channelId;
            Date = date;
        }
    }
    public class GetSaleReportQueryHandler : IQueryHandler<GetSaleReportQuery, PagedResult<ReportSaleTarget>>
    {
        private readonly IEfRepository<TransientRetailDbContext, Channel> _channelRepository;
        private readonly IEfRepository<TransientRetailDbContext, ChannelStoreAssignment> _channelAssignmentRepository;
        private readonly IEfRepository<RetailDbContext, Store> _storeTargetRepository;
        private readonly IEfRepository<RetailDbContext, SaleTarget> _saleTargetRepository;
        public GetSaleReportQueryHandler(IEfRepository<TransientRetailDbContext, Channel> channelRepository,
                                         IEfRepository<TransientRetailDbContext, ChannelStoreAssignment> channelAssignmentRepository,
                                         IEfRepository<RetailDbContext, Store> storeRepository,
                                         IEfRepository<RetailDbContext, SaleTarget> saleTargetRepository)
        {
            _channelRepository = channelRepository;
            _channelAssignmentRepository = channelAssignmentRepository;
            _storeTargetRepository = storeRepository;
            _saleTargetRepository = saleTargetRepository;
        }
        public async Task<PagedResult<ReportSaleTarget>> Handle(GetSaleReportQuery query)
        {
            var reportSaleTarget = new List<ReportSaleTarget>();
            var channel = await _channelRepository.GetByIdAsync(query.ChannelId);
            var channelAssignments = await _channelAssignmentRepository.ListAsync(x => x.ChannelId == query.ChannelId);

            var optionsBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
            optionsBuilder.UseNpgsql(channel.ServerInformation);
            using (var dbContext = new CatalogDbContext(optionsBuilder.Options))
            {
                foreach (var channelAssignment in channelAssignments)
                {
                    var saleTargets = await _saleTargetRepository.ListAsync(x => x.StoreId == channelAssignment.StoreId
                                                                           && (x.FromDate.Date <= query.Date.Date && x.ToDate.Date >= query.Date.Date)
                                                                           && x.IsDelete == false);
                    var currentSaleTarget = saleTargets.OrderBy(x => x.ToDate).FirstOrDefault();
                    if(currentSaleTarget != null)
                    {
                        var currentSale = dbContext.Orders.Where(x => x.LocationId == channelAssignment.StoreId
                                                            && (x.CreatedDate.Date >= currentSaleTarget.FromDate.Date && x.CreatedDate.Date <= query.Date.Date))
                                                               .Sum(x => x.GrandTotal);
                        var store = await _storeTargetRepository.GetByIdAsync(channelAssignment.StoreId);
                        reportSaleTarget.Add(new ReportSaleTarget()
                        {
                            StoreId = channelAssignment.Id,
                            Store = store.Name,
                            CurrentSale = currentSale,
                            SaleTarget = currentSaleTarget.Target,
                            ShortFall = currentSaleTarget.Target - currentSale,
                            FromDate = currentSaleTarget.FromDate,
                            ToDate = currentSaleTarget.ToDate
                        });
                    }
                    
                }         
            }

            return new PagedResult<ReportSaleTarget>
            {
                CurrentPage = query.PagingFilterCriteria.Page,
                NumberItemsPerPage = query.PagingFilterCriteria.NumberItemsPerPage,
                TotalItems = reportSaleTarget.Count(),
                Data = reportSaleTarget
            };
        }
    }
}
