﻿using Harvey.Domain;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.TransactionSale
{
    public class ExportTransactionSalesCSVQuery : IQuery<byte[]>
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public List<Guid> StoreIds { get; set; }
        public ExportTransactionSalesCSVQuery(DateTime fromDate,
            DateTime toDate,
            List<Guid> storeIds)
        {
            FromDate = fromDate;
            ToDate = toDate;
            StoreIds = storeIds;
        }
    }
    public sealed class ExportTransactionSalesCSVQueryHandler : IQueryHandler<ExportTransactionSalesCSVQuery, byte[]>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly ChannelContext _channelContext;
        public ExportTransactionSalesCSVQueryHandler(RetailDbContext retailDbContext,
            ChannelContext channelContext)
        {
            _retailDbContext = retailDbContext;
            _channelContext = channelContext;
        }
        public async Task<byte[]> Handle(ExportTransactionSalesCSVQuery query)
        {
            var orderItems = new List<TransactionSaleCSVModel>();
            foreach (var storeId in query.StoreIds)
            {
                var items = GetTransactionSalesStore(storeId, query.FromDate, query.ToDate);
                if (items != null)
                {
                    orderItems.AddRange(items);
                }
            }

            orderItems = orderItems.OrderByDescending(x => x.CreatedDate).ToList();

            var columnHeaders = new string[]
                {
                    "Store_Name",
                    "OrderId",
                    "Product_Name",
                    "VariantId",
                    "Variant_Name",
                    "SKU_Code",
                    "Price",
                    "Quantity",
                    "Amount",
                    "Customer_Name",
                    "Customer_Code",
                    "POS_Device_Number",
                    "Bill_No",
                    "CashierId",
                    "Cashier_Name",
                    "Create_Date"
                };
            var transactionsRecords = new List<object[]>();
            foreach (var item in orderItems)
            {
                transactionsRecords.Add(new object[] {
                            item.StoreName,
                            item.OrderId,
                            item.ProductName,
                            item.VariantId,
                            item.VariantName,
                            item.VariantCode,
                            item.UnitPrice,
                            item.Quantity,
                            item.Amount,
                            item.CustomerName,
                            item.CustomerCode,
                            item.POSDeviceNumber,
                            item.BillNo,
                            item.CashierId,
                            item.CashierName,
                            item.CreatedDate
                        });
            }
            var transactionsCSV = new StringBuilder();
            transactionsRecords.ForEach(line =>
            {
                transactionsCSV.AppendLine(string.Join(",", line));
            });
            byte[] buffer = Encoding.UTF8.GetPreamble().Concat(Encoding.UTF8.GetBytes($"{string.Join(",", columnHeaders)}\r\n{transactionsCSV.ToString()}")).ToArray();
            return buffer;
        }

        private List<TransactionSaleCSVModel> GetTransactionSalesStore(Guid storeId, DateTime fromDate, DateTime toDate)
        {
            var serverInformation = GetChannelInfomation(storeId);
            if (string.IsNullOrEmpty(serverInformation) == false)
            {
                var optionBuilder = new DbContextOptionsBuilder<CatalogDbContext>();
                optionBuilder.UseNpgsql(serverInformation);
                using (var dbContext = new CatalogDbContext(optionBuilder.Options))
                {
                    var storeName = _retailDbContext.Stores.AsNoTracking().FirstOrDefault(x => x.Id == storeId)?.Name;

                    var newTempOrderItems = (from orderItem in dbContext.OrderItems
                                          join order in dbContext.Orders
                                          on orderItem.OrderId equals order.Id
                                          where order.LocationId == storeId
                                          join variant in dbContext.Variants
                                          on orderItem.VariantId equals variant.Id
                                          join product in dbContext.Products
                                          on variant.ProductId equals product.Id
                                          select new
                                          {
                                              OrderId = order.Id,
                                              ProductName = product.Name,
                                              orderItem.VariantId,
                                              orderItem.VariantName,
                                              VariantSKUCode = variant.SKUCode,
                                              orderItem.Price,
                                              orderItem.Quantity,
                                              ItemAmount = orderItem.Amount,
                                              order.CustomerName,
                                              order.CustomerCode,
                                              order.BillNo,
                                              order.CashierId,
                                              order.CashierName,
                                              order.CreatedDate,
                                          }).ToList();

                    var orderItems = newTempOrderItems.OrderByDescending(x => x.CreatedDate)
                                        .Select(x => new TransactionSaleCSVModel()
                                        {
                                            StoreName = storeName,
                                            OrderId = x.OrderId,
                                            ProductName = x.ProductName,
                                            VariantId = x.VariantId,
                                            VariantName = x.VariantName,
                                            VariantCode = x.VariantSKUCode,
                                            UnitPrice = x.Price,
                                            Quantity = x.Quantity,
                                            Amount = x.ItemAmount,
                                            CustomerCode = x.CustomerCode,
                                            CustomerName = x.CustomerName,
                                            POSDeviceNumber = "1", // Apply multi device in the future
                                            BillNo = x.BillNo,
                                            CashierId = x.CashierId,
                                            CashierName = x.CashierName,
                                            CreatedDate = x.CreatedDate.ToLocalTime()
                                        });

                    if (fromDate != null && toDate != null)
                    {
                        orderItems = orderItems.Where(x => x.CreatedDate.Date >= fromDate.Date && x.CreatedDate.Date <= toDate.Date);
                    }

                    return orderItems.ToList();
                }
            }
            return null;
        }

        private string GetChannelInfomation(Guid storeId)
        {
            var channelAssignment = _retailDbContext.ChannelStoreAssignments.FirstOrDefault(x => x.StoreId == storeId);
            if (channelAssignment == null)
            {
                throw new InvalidOperationException($"Store Id {storeId}: Channel is not assigned");
            }

            var channel = _retailDbContext.Channels.FirstOrDefault(x => x.Id == channelAssignment.ChannelId);
            if (channel != null)
                return channel.ServerInformation;
            return string.Empty;
        }
    }
}
