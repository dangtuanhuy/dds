﻿using AutoMapper;
using Harvey.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.Infrastructure.Queries.Targets
{
    public class GetSaleTargetsByStoreQuery : IQuery<List<SaleTargetModel>>
    {
        public Guid? StoreId { get; }
        public DateTime? LastSync { get; }

        public GetSaleTargetsByStoreQuery(Guid? storeId, DateTime? lastSync)
        {
            StoreId = storeId;
            LastSync = lastSync;
        }
    }

    public sealed class GetSaleTargetsByStoreQueryHandler : IQueryHandler<GetSaleTargetsByStoreQuery, List<SaleTargetModel>>
    {
        private readonly RetailDbContext _retailDbContext;
        private readonly IMapper _mapper;
        public GetSaleTargetsByStoreQueryHandler(RetailDbContext retailDbContext,
            IMapper mapper)
        {
            _retailDbContext = retailDbContext;
            _mapper = mapper;
        }

        public async Task<List<SaleTargetModel>> Handle(GetSaleTargetsByStoreQuery query)
        {
            var lastSync = query.LastSync == null ? DateTime.UtcNow : query.LastSync.Value;
            
            return await _retailDbContext.SaleTargetHistory
                .Where(x => x.StoreId == query.StoreId && lastSync <= x.UpdatedDate)
                .Select(x => new SaleTargetModel
                {
                    Id = x.Id,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Target = x.Target,
                    StoreId = x.StoreId,
                    IsDelete = x.IsDelete
                })
                .ToListAsync();
        }
    }
}
