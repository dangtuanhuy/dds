﻿using Harvey.Domain;
using Harvey.Retail.Application.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace Harvey.Retail.Application.Infrastructure
{
    public class RetailDbContext : DbContext
    {
        public DbSet<Channel> Channels { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<ChannelStoreAssignment> ChannelStoreAssignments { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<AppSetting> AppSettings { get; set; }
        public DbSet<Harvey.Domain.NextSequence> NextSequences { get; set; }
        public DbSet<StoreSetting> StoreSettings { get; set; }
        public DbSet<SaleTarget> SaleTargetHistory { get; set; }
        public DbSet<TimeToFeed> TimeToFeeds { get; set; }
        public DbSet<POSOpenDay> POSOpenDays { get; set; }
        public DbSet<POSEndDay> POSEndDays { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<POSCashItem> POSCashItems { get; set; }
        public DbSet<EndDayPaymentItem> EndDayPaymentItems { get; set; }        
        public DbSet<OrderItemDetail> OrderItemDetails { get; set; }
        public DbSet<StockType> StockTypes { get; set; }


        public RetailDbContext(DbContextOptions<RetailDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Setup(modelBuilder.Entity<Channel>());
            Setup(modelBuilder.Entity<Store>());
            Setup(modelBuilder.Entity<ChannelStoreAssignment>());
            Setup(modelBuilder.Entity<Device>());
            Setup(modelBuilder.Entity<AppSetting>());
            Setup(modelBuilder.Entity<Harvey.Domain.NextSequence>());
            Setup(modelBuilder.Entity<StoreSetting>());
            Setup(modelBuilder.Entity<SaleTarget>());
            Setup(modelBuilder.Entity<OrderItemDetail>());
        }

        public void Setup(EntityTypeBuilder<Channel> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);
        }

        public void Setup(EntityTypeBuilder<Store> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig
               .Property(x => x.Id)
               .ValueGeneratedNever()
               .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);
            entityConfig.Ignore(x => x.CorrelationId);

            entityConfig.HasMany(x => x.StoreSettings)
                .WithOne(x => x.Store)
                .HasForeignKey(x => x.StoreId);

            entityConfig.HasMany(x => x.SaleTargets)
                .WithOne(x => x.Store)
                .HasForeignKey(x => x.StoreId);
        }

        public void Setup(EntityTypeBuilder<ChannelStoreAssignment> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.Ignore(x => x.CorrelationId);
            entityConfig.HasIndex(x => new { x.StoreId }).IsUnique();
        }

        public void Setup(EntityTypeBuilder<Device> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<AppSetting> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<Harvey.Domain.NextSequence> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasIndex(x => x.Key).IsUnique();
        }

        public void Setup(EntityTypeBuilder<StoreSetting> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }

        public void Setup(EntityTypeBuilder<SaleTarget> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
            entityConfig.HasOne(x => x.Store)
                        .WithMany(x => x.SaleTargets)
                        .HasForeignKey(x => x.StoreId);
        }
        public void Setup(EntityTypeBuilder<OrderItemDetail> entityConfig)
        {
            entityConfig.HasKey(x => x.Id);
        }
    }

    public class TransientRetailDbContext : RetailDbContext
    {
        public TransientRetailDbContext(DbContextOptions<RetailDbContext> options) : base(options)
        {
        }
    }
}
