﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class CatalogFieldValue : EntityBase
    {
        public Guid EntityId { get; set; }
        public Guid FieldId { get; set; }
        public string FieldValue { get; set; }
    }
}
