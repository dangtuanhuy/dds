﻿using Harvey.Domain;
using Harvey.Retail.Application.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Domain
{
    public class OrderItem : EntityBase
    {
        public Guid OrderId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public float Amount { get; set; }
        public int Quantity { get; set; }
        public string VariantName { get; set; }
        public float CostValue { get; set; }
        public float Price { get; set; }
        public Guid PriceId { get; set; }
        public OrderItemStatus Status { get; set; }
        public Guid? PreviousOrderItemId { get; set; }
    }
}
