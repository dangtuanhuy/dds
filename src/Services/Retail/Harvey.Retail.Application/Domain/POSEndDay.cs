﻿using Harvey.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Domain
{
    public class POSEndDay : EntityBase
    {
        public Store Store { get; set; }
        public Guid StoreId { get; set; }
        public string CashierName { get; set; }
        public Guid CashierId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid DeviceId { get; set; }
        public string Note { get; set; }

        public virtual ICollection<EndDayPaymentItem> EndDayPaymentItems { get; set; }
    }
}
