﻿using Harvey.Domain;
using Harvey.Retail.Application.Enums;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class Synchronization : EntityBase
    {
        public SynchAction SynchAction { get; set; }
        public SynchEntity Entity { get; set; }
        public Guid RefId { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
