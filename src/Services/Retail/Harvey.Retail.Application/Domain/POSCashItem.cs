﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class POSCashItem : EntityBase
    {
        public Currency Currency { get; set; }
        public Guid CurrencyId { get; set; }
        public int Quantity { get; set; }
        public Guid DocumentId { get; set; }
        public DocumentStatus DocumentStatus { get; set; }
    }

    public enum DocumentStatus
    {
        OpenDay = 1,
        EndDay = 2,
    }
}
