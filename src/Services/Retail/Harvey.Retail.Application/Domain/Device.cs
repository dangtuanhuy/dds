﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class Device : EntityBase, IAuditable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid StoreId { get; set; }

        public string DeviceCode { get; set; }
        public string UserCode { get; set; }

        public bool Trusted { get; set; }
        public DeviceStatus Status { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public enum DeviceStatus
    {
        Default = 0,
        Active = 1,
        Deleted = 2
    }
}
