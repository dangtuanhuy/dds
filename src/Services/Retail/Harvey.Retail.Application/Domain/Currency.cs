﻿using Harvey.Domain;

namespace Harvey.Retail.Application.Domain
{
    public class Currency : EntityBase
    {
        public decimal Value { get; set; }
    }
}
