﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class Category : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
