﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain.PIM
{
    public class PimChannel : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ServerInformation { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
    }
}
