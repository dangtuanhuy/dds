﻿using Harvey.MarketingAutomation;

namespace Harvey.Retail.Application.Domain.PIM
{
    public class PimStockType : FeedItemBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
