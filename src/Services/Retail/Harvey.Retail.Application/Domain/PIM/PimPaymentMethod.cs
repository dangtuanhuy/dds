﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Domain.PIM
{
    public class PimPaymentMethod : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
