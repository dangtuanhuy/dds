﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Domain.PIM
{
    public class PimStore : EntityBase
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
    }
}
