﻿using Harvey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Domain.PIM
{
    public class PimChannelStoreAssignment : EntityBase
    {
        public Guid ChannelId { get; set; }
        public Guid StoreId { get; set; }
    }
}
