﻿using Harvey.Domain;
using Harvey.Retail.Application.Checkout;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class Delivery : EntityBase
    {
        public Guid OrderId { get; set; }
        public DeliveryState State { get; set; }
    }
}
