﻿using Harvey.Domain;

namespace Harvey.Retail.Application.Domain
{
    public class StockType : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
