﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class SaleTarget: EntityBase, IAuditable
    {
        public DateTime FromDate { get; set; } 
        public DateTime ToDate { get; set; }
        public float Target { get; set; }
        public Guid StoreId { get; set; }
        public Store Store { get; set; }
        public bool IsDelete { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
