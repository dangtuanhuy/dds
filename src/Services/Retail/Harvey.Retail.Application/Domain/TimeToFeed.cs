﻿using Harvey.Domain;
using Harvey.MarketingAutomation.Enums;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class TimeToFeed: EntityBase
    {
        public DateTime LastSync { get; set; }
        public FeedType FeedType { get; set; }
    }
}
