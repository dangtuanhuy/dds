﻿using Harvey.Domain;
using Harvey.Retail.Application.Checkout;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class Payment : EntityBase
    {
        public Guid OrderId { get; set; }
        public Guid PaymentMethodId { get; set; }
        public PaymentState State { get; set; }
        public float Amount { get; set; }
    }
}
