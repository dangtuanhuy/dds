﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class EndDayPaymentItem : EntityBase
    {
        public POSEndDay POSEndDay { get; set; }
        public Guid POSEndDayId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public Guid PaymentMethodId { get; set; }
        public decimal Amount { get; set; }
    }
}
