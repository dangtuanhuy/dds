﻿using Harvey.Domain;
using Harvey.Retail.Application.Models;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class OrderPromotion : EntityBase
    {
        public Guid OrderId { get; set; }
        public Guid PromotionId { get; set; }
        public string Reason { get; set; }
        public DiscountType DiscountType { get; set; }
        public float Value { get; set; }
    }
}
