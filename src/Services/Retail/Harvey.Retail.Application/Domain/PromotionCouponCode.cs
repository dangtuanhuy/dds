﻿using Harvey.MarketingAutomation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Domain
{
    public class PromotionCouponCode: FeedItemBase
    {
        public string Code { get; set; }
        public bool IsUsed { get; set; }
        public Guid PromotionDetailId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
