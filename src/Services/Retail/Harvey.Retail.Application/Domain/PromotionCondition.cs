﻿using Harvey.MarketingAutomation;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class PromotionCondition : FeedItemBase
    {
        public int OperatorTypeId { get; set; }
        public long Value { get; set; }
        public int ConditionTypeId { get; set; }
        public Guid PromotionDetailId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
