﻿using Harvey.Domain;
using Harvey.Retail.Application.Checkout;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Domain
{
    public class CatalogPaymentMethod : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
