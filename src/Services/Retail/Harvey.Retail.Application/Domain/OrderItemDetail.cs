﻿using Harvey.Domain;
using Harvey.Retail.Application.Enums;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class OrderItemDetail : EntityBase
    {
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public Guid WareHouseId { get; set; }
        public Guid StoreId { get; set; }
        public Guid GIWDocumentItemId { get; set; }
        public int Quantity { get; set; }
        public double CostValue { get; set; }
        public Guid OrderItemId { get; set; }
        public OrderItemStatus Status { get; set; }
        public StockType StockType { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
