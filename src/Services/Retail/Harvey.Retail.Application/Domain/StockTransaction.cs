﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class StockTransaction : EntityBase
    {
        public Guid StockTransactionRefId { get; set; }
        public Guid TransactionTypeId { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public Guid LocationId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public int Quantity { get; set; }
        public int Balance { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
