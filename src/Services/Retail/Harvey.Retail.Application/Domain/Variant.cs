﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class Variant : EntityBase
    {
        public Guid ProductId { get; set; }
        public Guid PriceId { get; set; }
        public string SKUCode { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
