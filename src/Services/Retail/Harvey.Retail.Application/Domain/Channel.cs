﻿using Harvey.MarketingAutomation;

namespace Harvey.Retail.Application.Domain
{
    public class Channel : FeedItemBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ServerInformation { get; set; }
    }
}
