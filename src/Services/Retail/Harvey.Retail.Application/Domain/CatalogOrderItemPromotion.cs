﻿using Harvey.Domain;
using Harvey.Retail.Application.Models;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class CatalogOrderItemPromotion : EntityBase
    {
        public Guid OrderItemId { get; set; }
        public DiscountType DiscountType { get; set; }
        public float Value { get; set; }
    }
}
