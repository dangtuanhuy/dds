﻿using Harvey.MarketingAutomation;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class PromotionDetail : FeedItemBase
    {
        public int DiscountTypeId { get; set; }
        public long Value { get; set; }
        public bool IsUseCouponCodes { get; set; }
        public bool IsUseConditions { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
