﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class StoreSetting : EntityBase
    {
        public string Mall { get; set; }
        public string MachineId { get; set; }
        public string AccountId { get; set; }
        public string Password { get; set; }
        public string ServerIP { get; set; }
        public int Port { get; set; }
        public string FolderPath { get; set; }
        public string Description { get; set; }
        public Guid StoreId { get; set; }
        public virtual Store Store { get; set; }
    }
}
