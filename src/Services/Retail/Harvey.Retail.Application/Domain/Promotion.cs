﻿using Harvey.MarketingAutomation;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class Promotion : FeedItemBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int PromotionTypeId { get; set; }
        public Guid PromotionDetailId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public PromotionStatus Status { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public enum PromotionStatus
    {
        Default = 0,
        Active = 1,
        InActive = 2
    }
}
