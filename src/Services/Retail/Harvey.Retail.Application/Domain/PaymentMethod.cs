﻿using Harvey.MarketingAutomation;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Harvey.Retail.Application.Domain
{
    public class PaymentMethod : FeedItemBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        [NotMapped]
        public Guid CorrelationId { get; set; }
    }
}
