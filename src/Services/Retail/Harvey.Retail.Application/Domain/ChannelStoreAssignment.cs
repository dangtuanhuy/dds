﻿using Harvey.MarketingAutomation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Domain
{
    public class ChannelStoreAssignment : FeedItemBase
    {
        public Guid ChannelId { get; set; }
        public Guid StoreId { get; set; }
    }
}
