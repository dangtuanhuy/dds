﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class Product : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? CategoryId { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
