﻿using Harvey.Domain;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Enums;
using Harvey.Retail.Application.Events.Deliveries;
using Harvey.Retail.Application.Events.Orders;
using Harvey.Retail.Application.Events.Payments;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.Retail.Application.Domain
{
    public class Order : AggregateRootBase
    {
        public string Number { get; set; }
        public Guid? CustomerId { get; set; }
        public OrderState State { get; set; }
        public List<OrderItem> Items { get; set; }
        public Payment Payment { get; set; }
        public Delivery Delivery { get; set; }
        public ChannelContext ChannelContext { get; set; }
        public float SubTotal { get; set; }
        public float GrandTotal { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid LocationId { get; set; }
        public string BillNo { get; set; }
        public float GST { get; set; }
        public bool GSTInclusive { get; set; }
        public Guid CashierId { get; set; }
        public string CashierName { get; set; }
        public Guid? PreviousOrderId { get; set; }
        public Guid OriginalOrderId { get; set; }
        public string Reason { get; set; }
        public Guid DeviceId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public OrderTransactionType OrderTransactionType { get; set; }
        public float Collect { get; set; }
        public List<OrderPromotion> OrderPromotions { get; set; }

        public void SetChannelContext(ChannelContext channelContext)
        {
            this.ChannelContext = channelContext;
        }

        public void SetLocationId(Guid locationId)
        {
            this.LocationId = locationId;
        }

        public void SetShippingInfo(Guid? customerId)
        {
            //SetShippingInfo.execute
        }

        public void SetCustomerInfo(Guid? customerId)
        {
            //order.customerid = id
        }

        public void AddOrder(Guid id, DateTime createdDate, string serverInformation)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException($"id is required.");
            }

            RaiseEvent(new OrderCreatedEvent(id.ToString())
            {
                GrandTotal = this.GrandTotal,
                State = OrderState.Confirmed,
                ChannelContext = this.ChannelContext,
                LocationId = this.LocationId,
                CreatedDate = createdDate,
                BillNo = this.BillNo,
                OrderPromotions = this.OrderPromotions,
                GST = this.GST,
                GSTInclusive = this.GSTInclusive,
                CustomerId = this.CustomerId,
                CustomerName = this.CustomerName,
                CustomerCode = this.CustomerCode,
                CashierId = this.CashierId,
                CashierName = this.CashierName,
                OrderTransactionType = this.OrderTransactionType,
                OriginalOrderId = this.OriginalOrderId,
                Reason = this.Reason,
                DeviceId = this.DeviceId,
                PreviousOrderId = this.PreviousOrderId,
                Collect = this.Collect,
                ServerInformation = serverInformation
            });
        }
        
        public void AddLineItem(Guid orderId, CartItem cartItem, string serverInformation)
        {
            if (cartItem.Id == Guid.Empty)
            {
                throw new ArgumentNullException($"OrderItemId required.");
            }

            var orderItemPromotions = cartItem.ItemPromotions.Select(y => new CatalogOrderItemPromotion
            {
                Id = y.Id,
                DiscountType = y.DiscountType,
                OrderItemId = y.CartItemId,
                Value = (float)y.Value
            }).ToList();

            RaiseEvent(new OrderItemAddedEvent(Id.ToString())
            {
                LineItemId = cartItem.Id,
                OrderId = orderId,
                Amount = cartItem.Amount,
                Quantity = cartItem.Quantity,
                VariantId = new Guid(cartItem.VariantId),
                VariantName = cartItem.VariantName,
                StockTypeId = Guid.Empty,
                OrderItemPromotions = orderItemPromotions,
                ChannelContext = this.ChannelContext,
                Price = cartItem.Price,
                PriceId = new Guid(cartItem.PriceId),
                PreviousOrderItemId = cartItem.PreviousOrderItemId,
                Status = cartItem.Status,
                ServerInformation = serverInformation
            });
        }

        public void AddPayment(Guid paymentId, float amount, Guid orderId, string paymentCode)
        {
            if (paymentId == Guid.Empty)
            {
                throw new ArgumentNullException($"PaymentId is required.");
            }

            RaiseEvent(new PaymentCreatedEvent(Id.ToString())
            {
                OrderId = orderId,
                PaymentId = paymentId,
                Amount = amount,
                State = PaymentState.Init,
                PaymentCode = paymentCode,
                ChannelContext = this.ChannelContext
            });
        }

        public void AddDelivery(Guid deliveryId)
        {
            if (deliveryId == Guid.Empty)
            {
                throw new ArgumentNullException($"DeliveryId is required.");
            }

            RaiseEvent(new DeliveryCreatedEvent(Id.ToString())
            {
                DeliveryId = deliveryId,
                State = DeliveryState.Delivered
            });
        }

        public void UpdateState(OrderState state)
        {
            RaiseEvent(new OrderStateUpdatedEvent(Id.ToString())
            {
                State = state
            });
        }

        public void Apply(OrderCreatedEvent @event)
        {
            Id = Guid.Parse(@event.AggregateId);
            CustomerId = @event.CustomerId;
            State = @event.State;
            LocationId = @event.LocationId;
        }

        public void Apply(OrderItemAddedEvent @event)
        {
            if (Items == null)
            {
                Items = new List<OrderItem>();
            }
            Items.Add(new OrderItem()
            {
                OrderId = Guid.Parse(@event.AggregateId),
                Id = @event.LineItemId,
                Amount = @event.Amount,
                Quantity = @event.Quantity,
                VariantId = @event.VariantId,
                Price = @event.Price,
                StockTypeId = @event.StockTypeId,
                Status = @event.Status,
                PreviousOrderItemId = @event.PreviousOrderItemId
            });
        }

        public void Apply(PaymentCreatedEvent @event)
        {
            Payment = new Payment()
            {
                Amount = @event.Amount,
                Id = @event.PaymentId,
                OrderId = Guid.Parse(@event.AggregateId),
                State = @event.State
            };
        }

        public void Apply(OrderStateUpdatedEvent @event)
        {
            State = @event.State;
        }

        public void Apply(DeliveryCreatedEvent @event)
        {
            Delivery = new Delivery()
            {
                OrderId = Guid.Parse(@event.AggregateId),
                Id = @event.DeliveryId,
                State = @event.State
            };
        }

        public void Apply(ApplyPromotionEvent @event)
        {

        }

        public void Apply(CaculateGrandTotalEvent @event)
        {

        }
    }
}
