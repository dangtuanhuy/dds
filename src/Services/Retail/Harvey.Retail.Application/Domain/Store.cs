﻿using Harvey.MarketingAutomation;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Domain
{
    public class Store : FeedItemBase
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public Guid StoreSettingId { get; set; }
        public virtual ICollection<StoreSetting> StoreSettings { get; set; }
        public virtual IList<SaleTarget> SaleTargets { get; set; }
    }
}
