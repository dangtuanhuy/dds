﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class POSOpenDay : EntityBase
    {
        public Store Store { get; set; }
        public Guid StoreId { get; set; }
        public string CashierName { get; set; }
        public Guid CashierId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid DeviceId { get; set; }
    }
}
