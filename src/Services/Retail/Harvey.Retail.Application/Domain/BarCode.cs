﻿using Harvey.Domain;
using System;

namespace Harvey.Retail.Application.Domain
{
    public class BarCode : EntityBase
    {
        public Guid VariantId { get; set; }
        public string Code { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
