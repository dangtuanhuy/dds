﻿using Harvey.EventBus.Abstractions;
using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;
using Harvey.Retail.Application.PIM.Channels;
using Harvey.Retail.Application.PIM.ChannelStoreAssignments;
using Harvey.Retail.Application.PIM.Stores;
using Harvey.Retail.Application.PIM.PaymentMethods;
using System;
using Harvey.Retail.Application.PIM.StockTypes;

namespace Harvey.Retail.Application.PIM
{
    public class PimConnectorInstaller
    {
        private Guid _id = Guid.Parse("9daf23b5-8294-47f4-aae1-6cc6d3686243");
        private readonly string _jobName = "pim_to_retail";
        private readonly IEventBus _eventBus;

        public PimConnectorInstaller(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public void Install(ApplicationBuilder appBuilder)
        {
            appBuilder.AddConnector(_id, _jobName, _eventBus, (connectorRegistration) =>
            {
                connectorRegistration
                .AddChannelFeedService<PimChannel, Channel>(channelFeedServiceRegistration =>
                {
                    channelFeedServiceRegistration
                    .UseFetcher<PimChannelFetcher>()
                    .UseFilter<PimChannelFilter>()
                    .UseConverter<PimChannelConveter>()
                    .UseSerializer<PimChannelSerializer>()
                    .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 3, 0));
                }, rebuildOnExecution: true)
                .AddStoreFeedService<PimStore, Store>(storeFeedServiceRegistration =>
                {
                    storeFeedServiceRegistration
                    .UseFetcher<PimStoreFetcher>()
                    .UseFilter<PimStoreFilter>()
                    .UseConverter<PimStoreConveter>()
                    .UseSerializer<PimStoreSerializer>()
                    .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 3, 0));
                }, rebuildOnExecution: true)
                .AddChannelStoreAssignmentFeedService<PimChannelStoreAssignment, ChannelStoreAssignment>(channelStoreAssignmentFeedServiceRegistration =>
                {
                    channelStoreAssignmentFeedServiceRegistration
                    .UseFetcher<PimChannelStoreAssignmentFetcher>()
                    .UseFilter<PimChannelStoreAssignmentFilter>()
                    .UseConverter<PimChannelStoreAssignmentConveter>()
                    .UseSerializer<PimChannelStoreAssignmentSerializer>()
                    .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 3, 0));
                }, rebuildOnExecution: true)
                .AddPaymentMethodFeedService<PimPaymentMethod, PaymentMethod>(channelStoreAssignmentFeedServiceRegistration =>
                {
                    channelStoreAssignmentFeedServiceRegistration
                    .UseFetcher<PimPaymentMethodFetcher>()
                    .UseFilter<PimPaymentMethodFilter>()
                    .UseConverter<PimPaymentMethodConveter>()
                    .UseSerializer<PimPaymentMethodSerializer>()
                    .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 10, 0));
                })
                .AddStockTypeFeedService<StockType, PimStockType>(stockTypeFeedServiceRegistration =>
                {
                    stockTypeFeedServiceRegistration
                    .UseFetcher<PimStockTypeFetcher>()
                    .UseFilter<PimStockTypeFilter>()
                    .UseConverter<PimStockTypeConverter>()
                    .UseSerializer<PimStockTypeSerializer>()
                    .SetScheduler(new TimeSpan(0, 0, 5), new TimeSpan(0, 3, 0));
                });
            });
        }
    }
}
