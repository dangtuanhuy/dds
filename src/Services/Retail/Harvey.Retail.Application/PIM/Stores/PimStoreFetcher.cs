﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Retail.Application.Domain.PIM;
using Harvey.Retail.Application.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace Harvey.Retail.Application.PIM.Stores
{
    public class PimStoreFetcher : WebApiFetcherBase<PimStore>
    {
        public PimStoreFetcher(IConfiguration configuration, TransientRetailDbContext dbContext) 
            : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.Store}" +
                  $"&lastSync={dbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Store)?.LastSync}")
        {
        }
    }
}
