﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.PIM.Stores
{
    public class PimStoreSerializer: IFeedSerializer<Store>
    {
        private readonly IEfRepository<TransientRetailDbContext, Store> _efRepository;
        private readonly IEfRepository<TransientRetailDbContext, StoreSetting> _storeSettingRepository;
        private readonly TransientRetailDbContext _retailDbContext;
        public PimStoreSerializer(IEfRepository<TransientRetailDbContext, Store> efRepository, 
                                  IEfRepository<TransientRetailDbContext, StoreSetting> storeSettingRepository,
                                  TransientRetailDbContext retailDbContext)
        {
            _efRepository = efRepository;
            _storeSettingRepository = storeSettingRepository;
            _retailDbContext = retailDbContext;
        }

        public async Task SerializeAsync(IEnumerable<Store> feedItems)
        {
            foreach (var item in feedItems)
            {
                var entity = await _efRepository.GetByIdAsync(item.Id);
                if (entity == null)
                {
                    var storeSetting = new StoreSetting()
                    {
                        Id = Guid.NewGuid(),
                        StoreId = item.Id
                    };
                    await _storeSettingRepository.AddAsync(storeSetting);

                    entity = new Store()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Address = item.Address,
                        StoreSettingId = storeSetting.Id
                    };
                    await _efRepository.AddAsync(entity);
                }
                else
                {
                    entity.Name = item.Name;
                    entity.Address = item.Address;
                }
            }
            await _efRepository.SaveChangesAsync();
            await _storeSettingRepository.SaveChangesAsync();

            var timeFeed = _retailDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Store);
            if (timeFeed != null)
            {
                timeFeed.LastSync = DateTime.UtcNow;
                _retailDbContext.TimeToFeeds.Update(timeFeed);
            }
            else
            {
                _retailDbContext.TimeToFeeds.Add(new TimeToFeed
                {
                    LastSync = DateTime.UtcNow,
                    FeedType = FeedType.Store
                });
            }
            await _retailDbContext.SaveChangesAsync();
        }
    }
}
