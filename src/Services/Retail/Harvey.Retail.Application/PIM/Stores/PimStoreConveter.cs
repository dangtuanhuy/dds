﻿using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.Retail.Application.PIM.Stores
{
    public class PimStoreConveter : IFeedConverter<PimStore, Store>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimStore>).GetType();

        public IEnumerable<Store> Convert(IEnumerable<PimStore> source)
        {
            return source.Select(x => new Store()
            {
                Id = x.Id,
                Name = x.Name,
                Address = x.Address
            });
        }
    }
}
