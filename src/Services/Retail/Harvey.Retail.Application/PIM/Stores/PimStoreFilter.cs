﻿using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.PIM.Stores
{
    public class PimStoreFilter : IFeedFilter<PimStore>
    {
        public IEnumerable<PimStore> Filter(Guid CorrelationId, IEnumerable<PimStore> source)
        {
            return source;
        }
    }
}
