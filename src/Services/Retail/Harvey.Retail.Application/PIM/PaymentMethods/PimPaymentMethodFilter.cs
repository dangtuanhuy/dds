﻿using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.PIM.PaymentMethods
{
    public class PimPaymentMethodFilter : IFeedFilter<PimPaymentMethod>
    {
        public IEnumerable<PimPaymentMethod> Filter(Guid CorrelationId, IEnumerable<PimPaymentMethod> source)
        {
            return source;
        }
    }
}
