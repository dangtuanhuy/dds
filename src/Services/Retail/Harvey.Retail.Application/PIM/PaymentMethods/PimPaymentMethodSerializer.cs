﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;
using Harvey.Retail.Application.Infrastructure;

namespace Harvey.Retail.Application.PIM.PaymentMethods
{
    public class PimPaymentMethodSerializer : IFeedSerializer<PaymentMethod>
    {
        private readonly IEfRepository<TransientRetailDbContext, PaymentMethod> _efRepository;
        private readonly TransientRetailDbContext _retailDbContext;
        public PimPaymentMethodSerializer(IEfRepository<TransientRetailDbContext, PaymentMethod> efRepository,
                                    TransientRetailDbContext retailDbContext)
        {
            _efRepository = efRepository;
            _retailDbContext = retailDbContext;
        }

        public async Task SerializeAsync(IEnumerable<PaymentMethod> feedItems)
        {
            foreach (var item in feedItems)
            {
                var entity = await _efRepository.GetByIdAsync(item.Id);
                if (entity == null)
                {
                    entity = new PaymentMethod()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Code = item.Code,
                    };
                    await _efRepository.AddAsync(entity);
                }
                else
                {
                    entity.Name = item.Name;
                    entity.Code = item.Code;
                }
            }
            await _efRepository.SaveChangesAsync();

            var timeFeed = _retailDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.PaymentMethod);
            if (timeFeed != null)
            {
                timeFeed.LastSync = DateTime.UtcNow;
                _retailDbContext.TimeToFeeds.Update(timeFeed);
            }
            else
            {
                _retailDbContext.TimeToFeeds.Add(new TimeToFeed
                {
                    LastSync = DateTime.UtcNow,
                    FeedType = FeedType.PaymentMethod
                });
            }
            await _retailDbContext.SaveChangesAsync();
        }
    }
}
