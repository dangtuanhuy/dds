﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;

namespace Harvey.Retail.Application.PIM.PaymentMethods
{
    public class PimPaymentMethodConveter : IFeedConverter<PimPaymentMethod, PaymentMethod>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimChannel>).GetType();

        public IEnumerable<PaymentMethod> Convert(IEnumerable<PimPaymentMethod> source)
        {
            return source.Select(x => new PaymentMethod()
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code
            });
        }
    }
}
