﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Retail.Application.Domain.PIM;
using Microsoft.Extensions.Configuration;

namespace Harvey.Retail.Application.PIM.PaymentMethods
{
    public class PimPaymentMethodFetcher : WebApiFetcherBase<PimPaymentMethod>
    {
        public PimPaymentMethodFetcher(IConfiguration configuration) : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.PaymentMethod}")
        {

        }
    }
}
