﻿using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.PIM.StockTypes
{
    public class PimStockTypeFilter : IFeedFilter<StockType>
    {
        public IEnumerable<StockType> Filter(Guid CorrelationId, IEnumerable<StockType> source)
        {
            return source;
        }
    }
}
