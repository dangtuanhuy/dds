﻿using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Harvey.Retail.Application.PIM.StockTypes
{
    public class PimStockTypeConverter : IFeedConverter<StockType, PimStockType>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<StockType>).GetType();

        public IEnumerable<PimStockType> Convert(IEnumerable<StockType> source)
        {
            return source.Select(x => new PimStockType()
            {
                Id = x.Id,
                Code = x.Code,
                Description = x.Description,
                Name = x.Name
            });
        }
    }
}
