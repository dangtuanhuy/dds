﻿

using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.PIM.StockTypes
{
    public class PimStockTypeFetcher : WebApiFetcherBase<StockType>
    {
        public PimStockTypeFetcher(IConfiguration configuration) : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.StockType}")
        {

        }
    }
}
