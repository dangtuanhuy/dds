﻿using Harvey.MarketingAutomation;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;
using Harvey.Retail.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.PIM.StockTypes
{
    public class PimStockTypeSerializer : IFeedSerializer<PimStockType>
    {
        private readonly IEfRepository<TransientRetailDbContext, StockType> _efRepository;
        public PimStockTypeSerializer(IEfRepository<TransientRetailDbContext, StockType> efRepository)
        {
            _efRepository = efRepository;
        }
        public async Task SerializeAsync(IEnumerable<PimStockType> feedItems)
        {
            foreach (var item in feedItems)
            {
                var entity = await _efRepository.GetByIdAsync(item.Id);
                if (entity == null)
                {
                    entity = new StockType()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Code = item.Code,
                        Description = item.Description
                    };
                    await _efRepository.AddAsync(entity);
                }
                else
                {
                    entity.Name = item.Name;
                    entity.Code = item.Code;
                    entity.Description = item.Description;
                }

            }
            await _efRepository.SaveChangesAsync();
        }
    }
}
