﻿using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.PIM.Channels
{
    public class PimChannelFilter : IFeedFilter<PimChannel>
    {
        public IEnumerable<PimChannel> Filter(Guid CorrelationId, IEnumerable<PimChannel> source)
        {
            return source;
        }
    }
}
