﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure;

namespace Harvey.Retail.Application.PIM.Channels
{
    public class PimChannelSerializer : IFeedSerializer<Channel>
    {
        private readonly IEfRepository<TransientRetailDbContext, Channel> _efRepository;
        private readonly TransientRetailDbContext _retailDbContext;
        public PimChannelSerializer(IEfRepository<TransientRetailDbContext, Channel> efRepository,
                                    TransientRetailDbContext retailDbContext)
        {
            _efRepository = efRepository;
            _retailDbContext = retailDbContext;
        }

        public async Task SerializeAsync(IEnumerable<Channel> feedItems)
        {
            foreach (var item in feedItems)
            {
                var entity = await _efRepository.GetByIdAsync(item.Id);
                if (entity == null)
                {
                    entity = new Channel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Description = item.Description,
                        ServerInformation = item.ServerInformation
                    };
                    await _efRepository.AddAsync(entity);
                }
                else
                {
                    entity.Name = item.Name;
                    entity.Description = item.Description;
                    entity.ServerInformation = item.ServerInformation;
                }
            }
            await _efRepository.SaveChangesAsync();

            var timeFeed = _retailDbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Channel);
            if (timeFeed != null)
            {
                timeFeed.LastSync = DateTime.UtcNow;
                _retailDbContext.TimeToFeeds.Update(timeFeed);
            }
            else
            {
                _retailDbContext.TimeToFeeds.Add(new TimeToFeed
                {
                    LastSync = DateTime.UtcNow,
                    FeedType = FeedType.Channel
                });
            }
            await _retailDbContext.SaveChangesAsync();
        }
    }
}
