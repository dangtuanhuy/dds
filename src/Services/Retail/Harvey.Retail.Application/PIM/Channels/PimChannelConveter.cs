﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;

namespace Harvey.Retail.Application.PIM.Channels
{
    public class PimChannelConveter : IFeedConverter<PimChannel, Channel>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimChannel>).GetType();

        public IEnumerable<Channel> Convert(IEnumerable<PimChannel> source)
        {
            return source.Select(x => new Channel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                ServerInformation = x.ServerInformation
            });
        }
    }
}
