﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Retail.Application.Domain.PIM;
using Harvey.Retail.Application.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace Harvey.Retail.Application.PIM.Channels
{ 
    public class PimChannelFetcher : WebApiFetcherBase<PimChannel>
    {
        public PimChannelFetcher(IConfiguration configuration, TransientRetailDbContext dbContext) 
                                : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.Channel}" +
                                      $"&lastSync={dbContext.TimeToFeeds.FirstOrDefault(x => x.FeedType == FeedType.Channel)?.LastSync}")
        {
        }
    }
}
