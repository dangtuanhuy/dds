﻿using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.PIM.ChannelStoreAssignments
{
    public class PimChannelStoreAssignmentFilter : IFeedFilter<PimChannelStoreAssignment>
    {
        public IEnumerable<PimChannelStoreAssignment> Filter(Guid CorrelationId, IEnumerable<PimChannelStoreAssignment> source)
        {
            return source;
        }
    }
}
