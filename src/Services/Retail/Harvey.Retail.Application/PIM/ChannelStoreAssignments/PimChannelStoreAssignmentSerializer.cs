﻿using Harvey.MarketingAutomation;
using Harvey.Persitance.EF;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.Application.PIM.ChannelStoreAssignments
{
    public class PimChannelStoreAssignmentSerializer : IFeedSerializer<ChannelStoreAssignment>
    {
        private readonly IEfRepository<TransientRetailDbContext, ChannelStoreAssignment> _efRepository;
        public PimChannelStoreAssignmentSerializer(IEfRepository<TransientRetailDbContext, ChannelStoreAssignment> efRepository)
        {
            _efRepository = efRepository;
        }

        public async Task SerializeAsync(IEnumerable<ChannelStoreAssignment> feedItems)
        {
            var existingAssignments = (await _efRepository.GetAsync()).ToList();
            if (existingAssignments.Count > 0)
            {
                await _efRepository.DeleteAsync(existingAssignments);
            }
            
            foreach (var item in feedItems)
            {
                var entity = new ChannelStoreAssignment()
                {
                    Id = item.Id,
                    ChannelId = item.ChannelId,
                    StoreId = item.StoreId
                };
                await _efRepository.AddAsync(entity);
            }
            await _efRepository.SaveChangesAsync();
        }
    }
}
