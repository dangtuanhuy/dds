﻿using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Enums;
using Harvey.Retail.Application.Domain.PIM;
using Microsoft.Extensions.Configuration;

namespace Harvey.Retail.Application.PIM.ChannelStoreAssignments
{
    public class PimChannelStoreAssignmentFetcher: WebApiFetcherBase<PimChannelStoreAssignment>
    {
        public PimChannelStoreAssignmentFetcher(IConfiguration configuration) : base($"{configuration["PimFeedApiUrl"]}?feedType={FeedType.ChannelStoreAssignment}")
        {
        }
    }
}
