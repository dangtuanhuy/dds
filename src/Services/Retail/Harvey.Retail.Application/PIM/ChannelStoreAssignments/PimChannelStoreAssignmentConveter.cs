﻿using Harvey.MarketingAutomation;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvey.Retail.Application.PIM.ChannelStoreAssignments
{
    public class PimChannelStoreAssignmentConveter : IFeedConverter<PimChannelStoreAssignment, ChannelStoreAssignment>
    {
        public bool CanConvert(Type type) => type.GetType() == typeof(List<PimChannelStoreAssignment>).GetType();

        public IEnumerable<ChannelStoreAssignment> Convert(IEnumerable<PimChannelStoreAssignment> source)
        {
            return source.Select(x => new ChannelStoreAssignment()
            {
                Id = x.Id,
                ChannelId = x.ChannelId,
                StoreId = x.StoreId
            });
        }
    }
}
