﻿namespace Harvey.Retail.Application.Configs
{
    public class SFTPConfig
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FolderPath { get; set; }
        public bool IsValid()
        {
            return !string.IsNullOrEmpty(Host) || !string.IsNullOrEmpty(UserName) || !string.IsNullOrEmpty(Password) || !string.IsNullOrEmpty(FolderPath);
        }
    }
}
