﻿using System;
using Harvey.EventBus;

namespace Harvey.Retail.Application.Publishers
{
    public class CheckoutPublisher : IPublisher
    {
        public string Name => "harvey_retail_checkout_flow_state";

        public Guid? CorrelationId { get; set; }
    }
}
