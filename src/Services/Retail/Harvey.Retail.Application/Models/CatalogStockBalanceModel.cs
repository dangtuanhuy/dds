﻿using System;

namespace Harvey.Retail.Application.Models
{
    public class CatalogStockBalanceModel
    {
        public Guid VariantId { get; set; }
        public int Quantity { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
