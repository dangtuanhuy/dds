﻿using Harvey.Retail.Application.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Models
{
    public class EndDayModel
    {
        public Guid StoreId { get; set; }
        public string DeviceCode { get; set; }
        public string CashierName { get; set; }
        public Guid CashierId { get; set; }
        public List<CloseDayReportCurrency> Currencies { get; set; }
        public List<CloseDayReportPaymentModeDetail> PaymentModeDetails { get; set; }
        public string Note { get; set; }

    }

    public class CloseDayReportCurrency
    {
        public Guid CurrencyId { get; set; }
        public decimal Value { get; set; }
        public int Quantity { get; set; }
        public Guid DocumentId { get; set; }
        public DocumentStatus DocumentStatus { get; set; }
    }

    public class CloseDayReportPaymentModeDetail
    {
        public Guid POSEndDayId { get; set; }
        public Guid PaymentMethodId { get; set; }
        public decimal Amount { get; set; }
    }
}
