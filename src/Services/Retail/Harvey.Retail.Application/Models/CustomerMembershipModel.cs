﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Models
{
    public class CustomerMembershipModel
    {
        public Guid CustomerId { get; set; }
        public CustomerMembershipTypeModel MembershipType { get; set; }
    }

    public class CustomerMembershipTypeModel
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}
