﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Models
{
    public class ChannelModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
