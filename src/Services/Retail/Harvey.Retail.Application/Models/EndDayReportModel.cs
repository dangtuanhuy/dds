﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Models
{
    public class EndDayReportModel
    {
        public string LocationId { get; set; }
        public string DeviceCode { get; set; }        
        public DateTime ToDate { get; set; }
    }
}
