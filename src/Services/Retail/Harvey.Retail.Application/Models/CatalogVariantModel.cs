﻿using System;

namespace Harvey.Retail.Application.Models
{
    public class CatalogVariantModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public float ListPrice { get; set; }
        public float StaffPrice { get; set; }
        public float MemberPrice { get; set; }
        public string Description { get; set; }
        public Guid PriceId { get; set; }
        public int Quantity { get; set; }
        public Guid StockTypeId { get; set; }
        public string StockType { get; set; }
        public string SKUCode { get; set; }
        public Guid LocationId { get; set; }
        public bool IsDelete { get; set; }
        public Guid VariantId { get; set; }
    }
}
