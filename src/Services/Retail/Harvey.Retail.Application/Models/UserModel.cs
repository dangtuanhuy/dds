﻿using Harvey.Retail.Application.Enums;
using System;

namespace Harvey.Retail.Application.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CustomerCode { get; set; }
        public string Email { get; set; }
        public string User { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Gender? Gender { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string PhoneNumber { get; set; }
        public decimal WalletPoint { get; set; }
        public decimal LoyaltyPoint { get; set; }
    }
}
