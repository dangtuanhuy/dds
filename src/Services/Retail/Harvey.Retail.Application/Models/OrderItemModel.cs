﻿using Harvey.Retail.Application.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Models
{
    public class OrderItemModel
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public string VariantId { get; set; }
        public string PriceId { get; set; }
        public string StockTypeId { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }
        public float Amount { get; set; }
        public bool IsDelete { get; set; }
        public string SKUCode { get; set; }
        public string VariantName { get; set; }
        public float CostValue { get; set; }
        public Guid? PreviousOrderItemId { get; set; }
        public Guid OriginalOrderItemId { get; set; }
        public OrderItemStatus Status { get; set; }
        public List<OrderItemPromotion> OrderItemPromotions { get; set; }
    }

    public class OrderItemPromotion
    {
        public Guid Id { get; set; }
        public DiscountType DiscountType { get; set; }
        public Guid OrderItemId { get; set; }
        public double Value { get; set; }
    }

    public class saleStockTransactionModel
    {
        public Guid Id { get; set; }
        public Guid TransactionTypeId { get; set; }
        public Guid InventoryTransactionId { get; set; }
        public Guid StockTransactionRefId { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
        public Guid LocationId { get; set; }
        public Guid WareHouseId { get; set; }
        public Guid GIWDocumentItemId { get; set; }
        public int Quantity { get; set; }
        public int Balance { get; set; }
        public int BalanceRef { get; set; }
        public float Cost { get; set; }
        public decimal Price { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public OrderItemStatus Status { get; set; }
    }
}
