﻿using Harvey.Retail.Application.Domain;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Models
{
    public class OpenDayModel
    {
        public Guid StoreId { get; set; }
        public string DeviceCode { get; set; }
        public string CashierName { get; set; }
        public Guid CashierId { get; set; }
        public List<OpenDayCurrency> Currencies { get; set; }        

    }

    public class OpenDayCurrency
    {
        public Guid CurrencyId { get; set; }
        public decimal Value { get; set; }
        public int Quantity { get; set; }
        public Guid DocumentId { get; set; }
        public DocumentStatus DocumentStatus { get; set; }
    }
}
