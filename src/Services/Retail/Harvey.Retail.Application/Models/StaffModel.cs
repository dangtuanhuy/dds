﻿using Harvey.Retail.Application.Enums;

namespace Harvey.Retail.Application.Models
{
    public class StaffModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Pin { get; set; }
        public string PinHash { get; set; }
        public bool IsActive { get; set; }
        public UserType UserType { get; set; }
        public string PhoneNumber { get; set; }
    }
}
