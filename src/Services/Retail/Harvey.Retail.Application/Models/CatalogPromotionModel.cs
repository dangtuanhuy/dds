﻿using Harvey.Retail.Application.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Models
{
    public class CatalogPromotionModel
    {
        public Guid Id { get; set; }
        public Guid PromotionDetailId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PromotionTypeId { get; set; }
        public long Value { get; set; }
        public PromotionStatus PromotionStatus { get; set; }
        public int DiscountTypeId { get; set; }
        public bool IsUseCouponCodes { get; set; }
        public bool IsUseConditions { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
