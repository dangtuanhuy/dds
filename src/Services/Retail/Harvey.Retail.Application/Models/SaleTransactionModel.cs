﻿using System;

namespace Harvey.Retail.Application.Models
{
    public class SaleTransactionModel
    {
        public string LocationId { get; set; }
        public string MachineId { get; set; }
        public string ReportType { get; set; }
        public DateTime ToDate { get; set; }
    }
}
