﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Models
{
    public class SaleTransactionForGTOReportModel
    {
        public string LocationId { get; set; }
        public string MachineId { get; set; }
        public string ReportType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
