﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Models.Feeds
{
    public class GetCategoryIdsByProdutIdsRequest
    {
        public List<Guid> ProductIds { get; set; }
    }

    public class GetCategoryIdsByProdutIdsReponse
    {
        public List<GetCategoryIdsByProdutIdsReponseItem> data { get; set; }
    }

    public class GetCategoryIdsByProdutIdsReponseItem
    {
        public Guid ProductId { get; set; }
        public Guid? CategoryId { get; set; }
    }
}
