﻿using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Retail.Application.Models
{
    public class OrderModel
    {
        public Guid Id { get; set; }
        public Guid? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public Guid StoreId { get; set; }
        public double GST { get; set; }
        public bool GSTInclusive { get; set; }
        public float Amount { get; set; }
        public bool IsDelete { get; set; }
        public bool Synced { get; set; }
        public string BillNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public PaymentType Method { get; set; }
        public Guid CashierId { get; set; }
        public string CashierName { get; set; }
        public Guid? PreviousOrderId { get; set; }
        public Guid OriginalOrderId { get; set; }
        public string Reason { get; set; }
        public float Collect { get; set; }
        public OrderTransactionType OrderTransactionType { get; set; }
        public List<OrderPromotionModel> OrderPromotions { get; set; }
        public List<OrderPaymentMethodModel> OrderPayments { get; set; }
        public List<OrderItemModel> OrderItems { get; set; }
    }

    public class OrderPromotionModel
    {
        public Guid Id { get; set; }
        public Guid PromotionId { get; set; }
        public Guid OrderId { get; set; }
        public DiscountType DiscountType { get; set; }
        public string Reason { get; set; }
        public double Value { get; set; }
    }

    public enum PromotionType
    {
        Default = 0,
        Discount = 1,
        Bundle = 2,
        ManualDiscount = 3
    }

    public enum DiscountType
    {
        Default = 0,
        Money = 1,
        Percent = 2
    }
}
