﻿using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Enums;
using System;
using System.Collections.Generic;

namespace Harvey.Retail.Application.Models
{
    public class UpdateStockTransactionRequest
    {
        public Guid LocationId { get; set; }
        public string ServerInformation { get; set; }
        public List<UpdateStockTransactionItem> Items { get; set; }
    }

    public class UpdateStockTransactionItem
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public float Amount { get; set; }
        public int Quantity { get; set; }
        public OrderItemStatus Status { get; set; }
        public Guid GIWDocumentItemId { get; set; }
    }

    public class UpdateStockTransactionItemResponse
    {
        public List<UpdateStockTransactionItem> updateStockTransactionOrderItems = new List<UpdateStockTransactionItem>();
        public List<OrderItemDetail> oldStockTransactionOrderItems = new List<OrderItemDetail>();
    }
}
