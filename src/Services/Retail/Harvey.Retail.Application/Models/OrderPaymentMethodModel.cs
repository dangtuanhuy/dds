﻿using System;

namespace Harvey.Retail.Application.Models
{
    public class OrderPaymentMethodModel
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public string PaymentCode { get; set; }
        public float Amount { get; set; }
    }
}
