﻿using System;
using System.Collections.Generic;
using Harvey.Exception;
using Harvey.Polly;

namespace Harvey.Retail.API.Policies
{
    public class CheckoutRetrivalPolicy : IRetrivalPolicy
    {
        public int NumbersOfRetrival => 5;

        public RetrivalStategy RetrivalStategy => RetrivalStategy.Intervals;

        public int Delay => 2;

        public List<System.Exception> HandledExceptions => new List<System.Exception>() { new CheckoutException() };
    }
}
