﻿using Hangfire;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events;
using Harvey.EventBus.Events.Devices;
using Harvey.EventBus.Publishers;
using Harvey.EventBus.RabbitMQ.Policies;
using Harvey.Exception;
using Harvey.Exception.Extensions;
using Harvey.Exception.Handlers;
using Harvey.Job;
using Harvey.Job.Hangfire;
using Harvey.MarketingAutomation;
using Harvey.Polly;
using Harvey.Retail.API.Filters;
using Harvey.Retail.Application.Checkout.POS;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.EventHandlers;
using Harvey.Retail.Application.EventHandlers.Deliveries;
using Harvey.Retail.Application.EventHandlers.Orders;
using Harvey.Retail.Application.EventHandlers.Payments;
using Harvey.Retail.Application.Events;
using Harvey.Retail.Application.Events.Orders;
using Harvey.Retail.Application.Events.Payments;
using Harvey.Retail.Application.Events.StockTransactions;
using Harvey.Retail.Application.Hubs;
using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.PIM;
using Harvey.Retail.Application.Publishers;
using Marten;
using MassTransit;
using MassTransit.MartenIntegration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Linq;
using System.Net;


namespace Harvey.Retail.API.Extensions
{
    public static class IApplicationBuilderExtensions
    {
        public static void ConfigureEventBus(this IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.AuthorIdResolver = () =>
            {
                var httpContextAccessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>();
                if (httpContextAccessor.HttpContext == null)
                {
                    return default(Guid);
                }
                var idClaim = httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "sub");
                if (idClaim == null)
                {
                    return default(Guid);
                }
                return Guid.Parse(idClaim.Value);
            };
            eventBus.AddSubcription<LoggingPublisher, LoggingEvent, LoggingEventHandler>();
            eventBus.AddSubcription<TrustedDeviceEvent, TrustedDeviceEventHandler>();
            eventBus.AddSubcription<OrderCreatedEvent, OrderCreatedEventHandler>();
            eventBus.AddSubcription<OrderItemAddedEvent, OrderItemAddedEventHandler>();
            eventBus.AddSubcription<PaymentCreatedEvent, PaymentCreatedEventHandler>();
            eventBus.AddSubcription<PaymentsExecutedEvent, PaymentsExecutedEventHandler>();
            eventBus.AddSubcription<ApplyPromotionEvent, ApplyPromotionEventHandler>();
            eventBus.AddSubcription<CaculateSubTotalEvent, CaculateSubTotalEventHandler>();
            eventBus.AddSubcription<PaymentExecutedEvent, PaymentExecutedEventHandler>();
            eventBus.AddSubcription<PaymentExecutedSuccessfulEvent, PaymentExecutedSuccessfulEventHandler>();
            eventBus.AddSubcription<CaculateGrandTotalEvent, CaculateGrandTotalEventHandler>();
            eventBus.AddSubcription<StockTransactionUpdatedEvent, StockTransactionUpdatedEventHandler>();
            eventBus.AddSubcription<OrderDeliveryExecutedEvent, OrderDeliveryExecutedEventHandler>();
            eventBus.AddSubcription<OrderPlacedFailEvent, OrderPlacedFailEventHandler>();
            eventBus.AddSubcription<DeliveryExecutedSucessfulEvent, DeliveryExecutedSucessfulEventHandler>();
            eventBus.AddSubcription<OrderPlacedSuccessfulEvent, OrderPlacedSuccessfulEventHandler>();
            eventBus.Commit();
        }

        public static void ConfigureExceptionHandler(this IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionHandler(errApp =>
            {
                errApp.Run(async context =>
                {
                    var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                    var exception = errorFeature.Error;
                    var hasHandle = false;
                    ProblemDetails result = null;
                    DebugProblemDetails debugResult = null;
                    result = context.RequestServices.GetRequiredService<ForBiddenExceptionHandler>().Handle(exception, ref hasHandle);

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<NotFoundExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<BadModelExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<ArgumentExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<EfUniqueConstraintExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = context.RequestServices.GetRequiredService<SqlExceptionHandler>().Handle(exception, ref hasHandle);
                    }

                    if (!hasHandle)
                    {
                        result = new ProblemDetails()
                        {
                            Title = "Internal Server Error",
                            Status = (int)HttpStatusCode.InternalServerError,
                            Detail = exception.Message,
                        };

                    }
                    //TODO need to revisit
                    // for now, hack allow all cors when exception occur 
                    context.Response.ContentType = "application/json";
                    context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                    context.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
                    context.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Accept-Encoding, Content-Length, Content-MD5, Date, X-Api-Version, X-File-Name");
                    context.Response.Headers.Add("Access-Control-Allow-Methods", "POST,GET,PUT,PATCH,DELETE,OPTIONS");

                    var logger = context.RequestServices.GetService<ILogger<DebugProblemDetails>>();

                    if (env.IsDevelopment())
                    {
                        debugResult = new DebugProblemDetails(result)
                        {
                            DebugInfo = exception.GetTraceLog()
                        };
                        context.Response.StatusCode = debugResult.Status;
                        var message = JsonConvert.SerializeObject(debugResult, new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                        logger.LogError($" [{Logging.Application.PIM}] {message}");
                        await context.Response.WriteAsync(message);
                    }
                    else
                    {
                        var message = JsonConvert.SerializeObject(result, new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });
                        logger.LogError($" [{Logging.Application.PIM}] {message}");
                        await context.Response.WriteAsync(message);
                    }
                });
            });
        }

        public static void ConfigureJobManager(this IApplicationBuilder app, IHostingEnvironment env)
        {
            GlobalConfiguration.Configuration.UseActivator(new HangfireActivator(app.ApplicationServices));
            app.UseHangfireServer();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new DashboardAuthorizationFilter() }
            });
        }

        public static void ConfigureMarketingAutomation(this IApplicationBuilder app)
        {
            var applicationBuilder = app.ApplicationServices.GetRequiredService<ApplicationBuilder>();
            var pimConnectorInstaller = app.ApplicationServices.GetRequiredService<PimConnectorInstaller>();
            pimConnectorInstaller.Install(applicationBuilder);
        }

        public static void ConfigureContext(this IApplicationBuilder app)
        {
            app.Use(async (context, next) =>
            {
                if (context.Request.Headers.TryGetValue("x-device-code", out StringValues deviceCode))
                {
                    var dbContext = context.RequestServices.GetRequiredService<RetailDbContext>();
                    var device = dbContext.Devices.FirstOrDefault(x => x.DeviceCode == deviceCode && x.Trusted);
                    if (device == null)
                    {
                        throw new NotFoundException("Fail to recognize device.");
                    }

                    var channelAssignment = dbContext.ChannelStoreAssignments.FirstOrDefault(x => x.StoreId == device.StoreId);
                    if (channelAssignment == null)
                    {
                        throw new InvalidOperationException("Device has not assigned to channel.");
                    }

                    var channel = dbContext.Channels.FirstOrDefault(x => x.Id == channelAssignment.ChannelId);

                    if (channel == null)
                    {
                        throw new InvalidOperationException("Device has not assigned to channel.");
                    }
                    var channelContext = context.RequestServices.GetRequiredService<ChannelContext>();
                    channelContext.ServerInformation = channel.ServerInformation;
                    channelContext.DeviceId = device.Id;
                }

                await next();
            });
        }

        public static void ConfigureCheckoutFlow(this IApplicationBuilder app)
        {
            var configuration = app.ApplicationServices.GetRequiredService<IConfiguration>();
            var checkoutFlow = new POSCheckoutFlowSaga(app.ApplicationServices);

            var store = DocumentStore.For(configuration["ConnectionString"]);
            var repository = new MartenSagaRepository<OrderSagaStateMachineInstance>(store);

            var retrivalPolicy = new BusHandlerRetrivalPolicy();
            var logger = app.ApplicationServices.GetRequiredService<ILogger<POSCheckoutFlowSaga>>();
            retrivalPolicy.ExecuteStrategyAsync(logger, () =>
            {
                var busControl = Bus.Factory.CreateUsingRabbitMq(x =>
                {
                    var host = x.Host(new Uri(configuration["RabbitMqConfig:RabbitMqUrl"]), h =>
                    {
                        h.Username(configuration["RabbitMqConfig:Username"]);
                        h.Password(configuration["RabbitMqConfig:Password"]);
                    });

                    x.ReceiveEndpoint(host, $"Harvey_Retail_API_{new CheckoutPublisher().Name}", e =>
                    {
                        e.UseInMemoryOutbox();
                        e.StateMachineSaga(checkoutFlow, repository);
                    });
                });

                busControl.Start();
            }).Wait();

        }

        public static void ConfigureGTOSFTP(this IApplicationBuilder app)
        {
            var jobManager = app.ApplicationServices.GetRequiredService<IJobManager>();
            jobManager.RegisterRecurringJobDaily<GTOSFTPJobWorker>(Guid.NewGuid(), "GTO SFTP JobWorker", () => { }, 17, 1);
            jobManager.RegisterRecurringJobMonthly<GTOSFTPJobWorkerMonthly>(Guid.NewGuid(), "GTO SFTP JobWorker Monthly", () => { }, 1, 0, 1);
        }
    }
}
