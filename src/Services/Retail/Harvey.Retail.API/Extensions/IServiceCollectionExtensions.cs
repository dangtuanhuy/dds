﻿using AutoMapper;
using Hangfire;
using Hangfire.PostgreSql;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.EventStore.Marten;
using Harvey.EventBus.RabbitMQ;
using Harvey.EventBus.RabbitMQ.Policies;
using Harvey.Exception.Handlers;
using Harvey.Job;
using Harvey.Job.Hangfire;
using Harvey.Logging;
using Harvey.Logging.SeriLog;
using Harvey.MarketingAutomation;
using Harvey.MarketingAutomation.Connectors;
using Harvey.NextSequence;
using Harvey.Persitance.EF;
using Harvey.Retail.API.Filters;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Checkout.POS;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Domain.PIM;
using Harvey.Retail.Application.EventHandlers;
using Harvey.Retail.Application.EventHandlers.Deliveries;
using Harvey.Retail.Application.EventHandlers.Orders;
using Harvey.Retail.Application.EventHandlers.Payments;
using Harvey.Retail.Application.Events.Deliveries;
using Harvey.Retail.Application.Events.Orders;
using Harvey.Retail.Application.Events.Payments;
using Harvey.Retail.Application.Hubs;
using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.Infrastructure.Commands.Devices;
using Harvey.Retail.Application.Infrastructure.Commands.SaleTargetHistory;
using Harvey.Retail.Application.Infrastructure.Commands.Stores;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Infrastructure.Queries.Channels;
using Harvey.Retail.Application.Infrastructure.Queries.Devices;
using Harvey.Retail.Application.Infrastructure.Queries.Report;
using Harvey.Retail.Application.Infrastructure.Queries.Stock;
using Harvey.Retail.Application.Infrastructure.Queries.Stores;
using Harvey.Retail.Application.Infrastructure.Queries.Targets;
using Harvey.Retail.Application.Infrastructure.Queries.TopProductBySales;
using Harvey.Retail.Application.Infrastructure.Queries.TransactionSale;
using Harvey.Retail.Application.Models;
using Harvey.Retail.Application.Payment;
using Harvey.Retail.Application.Payment.Method;
using Harvey.Retail.Application.PIM;
using Harvey.Retail.Application.PIM.Channels;
using Harvey.Retail.Application.PIM.ChannelStoreAssignments;
using Harvey.Retail.Application.PIM.PaymentMethods;
using Harvey.Retail.Application.PIM.StockTypes;
using Harvey.Retail.Application.PIM.Stores;
using Harvey.Retail.Application.Services;
using Harvey.Search.Abstractions;
using Harvey.Search.NEST;
using Harvey.Setting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using static Harvey.Retail.Application.Infrastructure.Queries.Stores.SearchStoresQuery;

namespace Harvey.Retail.API.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IEventStore>(sp =>
            {
                return new MartenEventStore(configuration["ConnectionString"]);
            });
            services.AddTransient<LoggingEventHandler>();
            services.AddTransient<TrustedDeviceEventHandler>();

            services.AddDbContext<IdentifiedEventDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddTransient<IRepository<IdentifiedEvent>, EfRepository<IdentifiedEventDbContext, IdentifiedEvent>>();

            services.AddSingleton(sp =>
            {
                var logger = sp.GetRequiredService<ILogger<MasstransitPersistanceConnection>>();
                var endPoint = configuration["RabbitMqConfig:RabbitMqUrl"];
                var id = configuration["RabbitMqConfig:Username"];
                var pass = configuration["RabbitMqConfig:Password"];
                return new MasstransitPersistanceConnection(new BusCreationRetrivalPolicy(), logger, endPoint, id, pass);
            });

            services.AddSingleton<IEventBus>(sp =>
            {
                return new MasstransitEventBus("Harvey_Retail_API", sp.GetRequiredService<MasstransitPersistanceConnection>(), sp);
            });

            return services;
        }

        public static IServiceCollection AddServiceDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<RetailDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddDbContext<TransientRetailDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            services.AddDbContext<ActivityLogDbContext>(options =>
            {
                var connectionString = configuration["ConnectionString"];
                options.UseNpgsql(connectionString);
            }, ServiceLifetime.Transient);

            return services;
        }

        public static IServiceCollection AddCQRS(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ICommandExecutor, CommandExecutor>();
            services.AddTransient<IQueryExecutor, QueryExecutor>();

            services.AddTransient<ICommandHandler<AddDeviceCommand, DeviceModel>, AddDeviceCommandHandler>();
            services.AddTransient<ICommandHandler<SoftDeleteDeviceCommand, bool>, SoftDeleteDeviceCommandHandler>();
            services.AddTransient<IQueryHandler<GetDevicesQuery, PagedResult<DeviceModel>>, GetDevicesQueryHandler>();
            services.AddTransient<IQueryHandler<GetDeviceByIdQuery, DeviceModel>, GetDeviceByIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetDeviceByDeviceCodeQuery, DeviceModel>, GetDeviceByDeviceCodeQueryHandler>();
            services.AddTransient<IQueryHandler<GetStoresQuery, PagedResult<StoreSettingModel>>, GetStoresQueryHandler>();
            services.AddTransient<IQueryHandler<GetAllStoreToSyncPOSQuery, IList<GetAllStoreToSyncPOS>>, GetAllStoreToSyncPOSQueryHandler>();
            services.AddTransient<IQueryHandler<SearchStoresQuery, IEnumerable<StoreModel>>, SearchStoresQueryHandler>();
            services.AddTransient<ICommandHandler<UpdateStoreSettingCommand, StoreSettingModel>, UpdateStoreSettingCommandHandler>();
            services.AddTransient<ICommandHandler<GenerateStoreSettingsCommand, List<StoreSettingModel>>, GenerateStoreSettingsCommandHandler>();
            services.AddTransient<ICommandHandler<PostGTOSFTPCommand, bool>, PostGTOSFTPCommandHandler>();
            services.AddTransient<IQueryHandler<GetStoreByIdQuery, StoreSettingModel>, GetStoreByIdQueryHandler>();
            services.AddTransient<IQueryHandler<GetStoreByDeviceCodeQuery, StoreModel>, GetStoreByDeviceCodeQueryHandler>();
            services.AddTransient<IQueryHandler<GetSaleTargetsByStoreQuery, List<SaleTargetModel>>, GetSaleTargetsByStoreQueryHandler>();
            services.AddTransient<IQueryHandler<GetSaleReportQuery, PagedResult<ReportSaleTarget>>, GetSaleReportQueryHandler>();
            services.AddTransient<IQueryHandler<GetStoreSaleTargetQuery, PagedResult<StoreModel>>, GetStoreSaleTargetQueryHandler>();
            services.AddTransient<ICommandHandler<AddSaleTargetCommand, SaleTargetModel>, AddSaleTargetCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateSaleTargetCommand, bool>, UpdateSaleTargetCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteSaleTargetCommand, bool>, DeleteSaleTargetCommandHandler>();
            services.AddTransient<IQueryHandler<GetChannelQuery, IList<ChannelModel>>, GetChannelQueryHandler>();
            services.AddTransient<IQueryHandler<GetAllStoreQuery, IList<GetStoreModel>>, GetAllStoreQueryHandler>();
            services.AddTransient<IQueryHandler<GetStoreManagementByIdQuery, StoreSettingModel>, GetStoreManagementByIdQueryHandler>();
            services.AddTransient<IQueryHandler<ExportTransactionSalesCSVQuery, byte[]>, ExportTransactionSalesCSVQueryHandler>();
            services.AddTransient<IQueryHandler<CountChannelDataQuery, int>, CountChannelDataQueryHandler>();
            services.AddTransient<IQueryHandler<GetStocksByLocationQuery, List<StockModel>>, GetStocksByLocationQueryHandler>();
            services.AddTransient<IQueryHandler<AddEndDayQuery, bool>, AddEndDayQueryHandler>();
            services.AddTransient<IQueryHandler<AddOpenDayQuery, bool>, AddOpenDayQueryHandler>();
            services.AddTransient<IQueryHandler<PimGetProductBySalesQuery, IEnumerable<TopProductSaleModel>>, PimGetProductBySalesQueryHandler>();
            return services;
        }

        public static IServiceCollection AddTrackingActivity(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ActivityTracking>();
            services.AddTransient<IEfRepository<ActivityLogDbContext, ActivityLog>, EfRepository<ActivityLogDbContext, ActivityLog>>();
            return services;
        }

        public static IServiceCollection AddLogger(this IServiceCollection services, IConfiguration configuration)
        {
            new SeriLogger().Initilize(new List<IDatabaseLoggingConfiguration>()
            {
                new DatabaseLoggingConfiguration()
                {
                    ConnectionString = configuration["Logging:DataLogger:ConnectionString"],
                    TableName = configuration["Logging:DataLogger:TableName"],
                    LogLevel = (LogLevel)Enum.Parse( typeof(LogLevel), configuration["Logging:DataLogger:LogLevel"],true)
                }
            }, new List<ICentralizeLoggingConfiguration>() {
                new CentralizeLoggingConfiguration()
                {
                    Url = configuration["Logging:CentralizeLogger:Url"],
                    LogLevel = (LogLevel)Enum.Parse( typeof(LogLevel), configuration["Logging:CentralizeLogger:LogLevel"],true)
                }
            });
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper();
            return services;
        }

        public static IServiceCollection AddExceptionHandlers(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ArgumentExceptionHandler>();
            services.AddScoped<EfUniqueConstraintExceptionHandler>();
            services.AddScoped<SqlExceptionHandler>();
            services.AddScoped<ForBiddenExceptionHandler>();
            services.AddScoped<NotFoundExceptionHandler>();
            services.AddScoped<BadModelExceptionHandler>();
            return services;
        }

        public static IServiceCollection AddSearchService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ISearchService, SearchService>();
            services.AddTransient<SearchSettings>(sp =>
            {
                return new SearchSettings(configuration["ELASTICSEARCHURL"]);
            });
            return services;
        }

        public static IServiceCollection AddJobManager(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IJobManager, HangfireJobManager>();
            services.AddHangfire(options => options.UseStorage(new PostgreSqlStorage(configuration["ConnectionString"]))
            .UseColouredConsoleLogProvider());
            return services;
        }

        public static IServiceCollection AddMarketingAutomation(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<ApplicationBuilder>();
            services.AddSingleton<ConnectorInfoCollection>();
            services.AddSingleton<FeedWorker>();

            services.AddTransient<PimConnectorInstaller>();

            services.AddTransient<PimChannelFetcher>();
            services.AddTransient<PimChannelFilter>();
            services.AddTransient<PimChannelConveter>();
            services.AddTransient<PimChannelSerializer>();

            services.AddTransient<PimStoreFetcher>();
            services.AddTransient<PimStoreFilter>();
            services.AddTransient<PimStoreConveter>();
            services.AddTransient<PimStoreSerializer>();

            services.AddTransient<PimChannelStoreAssignmentFetcher>();
            services.AddTransient<PimChannelStoreAssignmentFilter>();
            services.AddTransient<PimChannelStoreAssignmentConveter>();
            services.AddTransient<PimChannelStoreAssignmentSerializer>();

            services.AddTransient<PimPaymentMethodFetcher>();
            services.AddTransient<PimPaymentMethodFilter>();
            services.AddTransient<PimPaymentMethodConveter>();
            services.AddTransient<PimPaymentMethodSerializer>();

            services.AddTransient<PimStockTypeFetcher>();
            services.AddTransient<PimStockTypeFilter>();
            services.AddTransient<PimStockTypeConverter>();
            services.AddTransient<PimStockTypeSerializer>();

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<EfUnitOfWork>();

            services.AddTransient<IEfRepository<TransientRetailDbContext, Channel>, EfRepository<TransientRetailDbContext, Channel>>();
            services.AddTransient<IEfRepository<TransientRetailDbContext, Store>, EfRepository<TransientRetailDbContext, Store>>();
            services.AddTransient<IEfRepository<TransientRetailDbContext, ChannelStoreAssignment>, EfRepository<TransientRetailDbContext, ChannelStoreAssignment>>();
            services.AddTransient<IEfRepository<TransientRetailDbContext, StoreSetting>, EfRepository<TransientRetailDbContext, StoreSetting>>();
            services.AddTransient<IEfRepository<RetailDbContext, StoreSetting>, EfRepository<RetailDbContext, StoreSetting>>();
            services.AddTransient<IEfRepository<RetailDbContext, Device>, EfRepository<RetailDbContext, Device>>();
            services.AddTransient<IEfRepository<RetailDbContext, Device, DeviceModel>, EfRepository<RetailDbContext, Device, DeviceModel>>();
            services.AddTransient<IEfRepository<RetailDbContext, Store, StoreModel>, EfRepository<RetailDbContext, Store, StoreModel>>();
            services.AddScoped<IEfRepository<RetailDbContext, Store>, EfRepository<RetailDbContext, Store>>();
            services.AddTransient<IEfRepository<RetailDbContext, StoreSetting, StoreSettingModel>, EfRepository<RetailDbContext, StoreSetting, StoreSettingModel>>();
            services.AddTransient<IEfRepository<RetailDbContext, SaleTarget>, EfRepository<RetailDbContext, SaleTarget>>();
            services.AddScoped<IEventStoreRepository<Order>, EvenStoreRepository<Order>>();
            services.AddTransient<IEfRepository<RetailDbContext, POSEndDay>, EfRepository<RetailDbContext, POSEndDay>>();
            services.AddTransient<IEfRepository<RetailDbContext, POSOpenDay>, EfRepository<RetailDbContext, POSOpenDay>>();
            services.AddTransient<IEfRepository<RetailDbContext, POSCashItem>, EfRepository<RetailDbContext, POSCashItem>>();
            services.AddTransient<IEfRepository<TransientRetailDbContext, PaymentMethod>, EfRepository<TransientRetailDbContext, PaymentMethod>>();
            services.AddTransient<IEfRepository<TransientRetailDbContext, StockType>, EfRepository<TransientRetailDbContext, StockType>>();

            return services;
        }

        public static IServiceCollection AddAppSetting(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAppSettingService, AppSettingService>();
            return services;
        }

        public static IServiceCollection AddContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ChannelContext>();
            services.AddScoped<CatalogContext>();
            return services;
        }

        public static IServiceCollection AddPaymentMethod(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IPaymentMethod, CashMethod>();
            return services;
        }

        public static IServiceCollection AddCheckoutFlow(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<DeliveryCreatedEventHandler>();
            services.AddTransient<OrderCreatedEventHandler>();
            services.AddTransient<OrderItemAddedEventHandler>();
            services.AddTransient<OrderStateUpdatedEventHandler>();
            services.AddTransient<PaymentCreatedEventHandler>();
            services.AddTransient<PaymentExecutedEventHandler>();
            services.AddTransient<PaymentsExecutedEventHandler>();
            services.AddTransient<ICheckoutFlow, POSCheckoutFlow>();
            services.AddTransient<ApplyPromotionEventHandler>();
            services.AddTransient<CaculateSubTotalEventHandler>();
            services.AddTransient<PaymentExecutedSuccessfulEventHandler>();
            services.AddTransient<CaculateGrandTotalEventHandler>();
            services.AddTransient<StockTransactionUpdatedEventHandler>();
            services.AddTransient<OrderDeliveryExecutedEventHandler>();
            services.AddTransient<OrderPlacedFailEventHandler>();
            services.AddTransient<DeliveryExecutedSucessfulEventHandler>();
            services.AddTransient<OrderPlacedSuccessfulEventHandler>();
            return services;
        }

        public static IServiceCollection AddNextSequence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<INextSequenceService<TransientRetailDbContext>, NextSequenceService<TransientRetailDbContext>>();
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<OrderService>();
            services.AddTransient<SaleTransactionService>();
            services.AddTransient<IExportService, ExportService>();
            return services;
        }

        public static IServiceCollection AddGTOSFTP(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<GTOSFTPJobWorker>();
            services.AddSingleton<GTOSFTPJobWorkerMonthly>();
            return services;
        }
    }
}
