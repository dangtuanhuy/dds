﻿using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;

namespace Harvey.Retail.API.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static Guid GetUserId(this ClaimsPrincipal user)
        {
            var result = user.Claims.FirstOrDefault(x => x.Type == "sub");
            if (result == null)
            {
                throw new UnauthorizedAccessException();
            }
            return Guid.Parse(result.Value);
        }
        public static string GetUsername(this ClaimsPrincipal user)
        {
            var result = user.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Email);
            if (result == null)
            {
                throw new UnauthorizedAccessException();
            }
            return result.Value;
        }

    }
}
