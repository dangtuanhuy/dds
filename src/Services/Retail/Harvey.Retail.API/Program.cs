﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Harvey.Retail.Application.Infrastructure;

namespace Harvey.Retail.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .Build()
                .MigrateDbContext<RetailDbContext>((context, services) =>
                {
                    var logger = services.GetService<ILogger<RetailDbContextDataSeed>>();
                    new RetailDbContextDataSeed().SeedAsync(context, logger).Wait();
                })
               .MigrateDbContext<ActivityLogDbContext>((context, services) =>
               {

               })
                .MigrateDbContext<IdentifiedEventDbContext>((context, services) =>
                {
                    var logger = services.GetService<ILogger<IdentifiedEventDbContextDataSeed>>();
                    new IdentifiedEventDbContextDataSeed().SeedAsync(context, logger).Wait();
                })
               .Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseDefaultServiceProvider(options => options.ValidateScopes = false);
    }
}
