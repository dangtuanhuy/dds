﻿using Harvey.Retail.Application.Infrastructure;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.API.Filters
{
    public class EfUnitOfWork : IActionFilter
    {
        private readonly RetailDbContext _retailDbContext;
        public EfUnitOfWork(RetailDbContext retailDbContext)
        {
            _retailDbContext = retailDbContext;
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception == null)
            {
                _retailDbContext.SaveChangesAsync().Wait();
            }
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }
    }
}
