﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.Retail.Application.Infrastructure.Queries.Channels;
using Harvey.Retail.Application.Models;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.Retail.API.Controllers
{
    [Route("api/channel")]
    [ApiController]
    public class ChannelsController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;

        public ChannelsController(IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<IList<ChannelModel>>> Get()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetChannelQuery());
            return Ok(result);
        }
    }
}