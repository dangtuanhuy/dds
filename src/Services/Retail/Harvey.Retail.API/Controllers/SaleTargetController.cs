﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.Retail.API.Filters;
using Harvey.Retail.Application.Infrastructure.Commands.SaleTargetHistory;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Infrastructure.Queries.Report;
using Harvey.Retail.Application.Infrastructure.Queries.Stores;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.Retail.API.Controllers
{
    [Authorize]
    [Route("api/sale-target")]
    public class SaleTargetController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;

        public SaleTargetController(IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<StoreModel>>> Get(PagingFilterCriteria pagingFilterCriteria)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetStoreSaleTargetQuery(pagingFilterCriteria));
            return Ok(result);
        }

        [HttpGet]
        [Route("report")]
        public async Task<ActionResult<PagedResult<ReportSaleTarget>>> GetReport(PagingFilterCriteria pagingFilterCriteria, Guid channelId, DateTime date )
        {
            if(channelId == Guid.Empty)
            {
                return BadRequest("Select channel is required.");
            }

            var result = await _queryExecutor.ExecuteAsync(new GetSaleReportQuery(pagingFilterCriteria, channelId, date));
            return Ok(result);
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<SaleTargetModel>> Add([FromBody] AddSaleTargetModel saleTarget)
        {
            var result = await _commandExecutor.ExecuteAsync(new AddSaleTargetCommand(saleTarget));
            return result;
        }

        [HttpPut]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<bool>> Update([FromBody] UpdateSaleTargetModel saleTarget)
        {
            var result = await _commandExecutor.ExecuteAsync(new UpdateSaleTargetCommand(saleTarget));
            return result;
        }

        [HttpDelete]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<bool>> Delete(Guid Id)
        {
            var result = await _commandExecutor.ExecuteAsync(new DeleteSaleTargetCommand(Id));
            return result;
        }
    }
}