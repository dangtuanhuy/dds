﻿using Harvey.Domain;
using Harvey.Retail.Application.Infrastructure.Commands.Stores;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Infrastructure.Queries.Stores;
using Harvey.Retail.Application.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.Retail.API.Controllers
{
    [Authorize]
    [Route("api/stores")]
    public class StoresController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        public StoresController(
            IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpGet]
        [Route("search")]
        public async Task<IEnumerable<StoreModel>> Search([FromQuery] string text)
        {
            var result = await _queryExecutor.ExecuteAsync(new SearchStoresQuery(text));
            return result;
        }

        [HttpGet]

        public async Task<ActionResult<PagedResult<StoreSettingModel>>> Get(PagingFilterCriteria pagingFilterCriteria)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetStoresQuery(pagingFilterCriteria));
            return result;
        }

        [HttpGet]
        [Route("all")]
        public async Task<ActionResult<List<StoreModel>>> GetAll()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetAllStoreQuery());
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<StoreSettingModel> Update([FromBody] StoreSettingModel model)
        {
            var command = new UpdateStoreSettingCommand
            {
                Id = model.Id.ToString(),
                Mall = model.Mall,
                MachineId = model.MachineId,
                ServerIP = model.ServerIP,
                AccountId = model.AccountId,
                Password = EnDecryptUtility.Encrypt(model.Password),
                Port = model.Port,
                FolderPath = model.FolderPath
            };
            var result = await _commandExecutor.ExecuteAsync(command);
            return result;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<StoreSettingModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetStoreByIdQuery(id));
            return Ok(result);
        }

        [HttpGet("management/{id}")]
        public async Task<ActionResult<StoreSettingModel>> GetById(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetStoreManagementByIdQuery(id));
            return Ok(result);
        }

        [HttpPost("storeSettings/generation")]
        public async Task<ActionResult<List<StoreSettingModel>>> GenerateStoreSettings()
        {
            var result = await _commandExecutor.ExecuteAsync(new GenerateStoreSettingsCommand());
            return Ok(result);
        }
    }
}
