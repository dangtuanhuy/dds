﻿using Harvey.Domain;
using Harvey.Persitance.EF;
using Harvey.Retail.API.Extensions;
using Harvey.Retail.API.Filters;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.Infrastructure.Commands.Devices;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Infrastructure.Queries.Devices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Harvey.Retail.API.Controllers
{
    [Authorize (Policy = "Retail")]
    [Route("api/devices")]
    public class DevicesController : ControllerBase
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly IConfiguration _configuration;
        private readonly HttpClient _client;
        private readonly IQueryExecutor _queryExecutor;
        public DevicesController(IConfiguration configuration,
            IQueryExecutor queryExecutor,
            ICommandExecutor commandExecutor)
        {
            _configuration = configuration;
            _client = new HttpClient();
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<DeviceModel>>> Get(PagingFilterCriteria pagingFilterCriteria, string queryText)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetDevicesQuery(pagingFilterCriteria, queryText));
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DeviceModel>> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetDeviceByIdQuery(id));
            return Ok(result);
        }

        [HttpGet("deviceCode/{deviceCode}")]
        [AllowAnonymous]
        public async Task<ActionResult<DeviceModel>> GetByDeviceCode(string deviceCode)
        {
            if (string.IsNullOrEmpty(deviceCode))
            {
                return BadRequest("Device code is required.");
            }
            var result = await _queryExecutor.ExecuteAsync(new GetDeviceByDeviceCodeQuery(deviceCode));
            return Ok(result);
        }

        [HttpPost]
        [ServiceFilter(typeof(EfUnitOfWork))]
        public async Task<ActionResult<DeviceModel>> Add([FromBody] DeviceModel device)
        {
            var deviceAuthorizationEndpoint = $"{_configuration["Authority"]}/connect/deviceauthorization";
            //PHI TODO using IOption
            var parameters = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("client_id","device"),
                new KeyValuePair<string, string>("client_secret","secret"),
                new KeyValuePair<string, string>("client_id","device"),
                new KeyValuePair<string, string>("scope","openid profile email phone harvey.rims.api roles")
            };

            var req = new HttpRequestMessage(HttpMethod.Post, deviceAuthorizationEndpoint)
            {
                Content = new FormUrlEncodedContent(parameters)
            };
            var res = await _client.SendAsync(req);
            if (!res.IsSuccessStatusCode)
            {
                throw new System.InvalidOperationException("Cannot authorize device. Please try again.");
            }

            var deviceInfomation = JsonConvert.DeserializeObject<dynamic>(await res.Content.ReadAsStringAsync());
            
            device.DeviceCode = deviceInfomation.device_code;
            device.UserCode = deviceInfomation.user_code;
            var result = await _commandExecutor.ExecuteAsync(new AddDeviceCommand(User.GetUserId(), device));
            return result;
        }


        [HttpDelete("{id}/soft")]
        public async Task<ActionResult> SoftDelete(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("id is required");
            }
            var result = await _commandExecutor.ExecuteAsync(new SoftDeleteDeviceCommand(id));
            if (!result)
            {
                return BadRequest("Delete failure!");
            }

            return Ok();
        }
    }
}
