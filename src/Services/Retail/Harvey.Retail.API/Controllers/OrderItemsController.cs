﻿using Harvey.Retail.Application.Models;
using Harvey.Retail.Application.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class OrderItemsController : ControllerBase
    {
        private readonly OrderService _orderService;
        public OrderItemsController(OrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        public async Task<IEnumerable<OrderItemModel>> Get([FromQuery] Guid locationId, [FromQuery] DateTime fromDate, [FromQuery] DateTime toDate)
        {
            var result = await _orderService.GetOrderItemAsync(locationId, fromDate, toDate);
            return result;
        }
    }
}
