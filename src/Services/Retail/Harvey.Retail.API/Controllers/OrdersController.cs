﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Retail.Application.Models;
using Harvey.Retail.Application.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.Retail.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class OrdersController : ControllerBase
    {
        private readonly OrderService _orderService;
        public OrdersController(OrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        public async Task<IEnumerable<OrderModel>> Get([FromQuery] Guid locationId, [FromQuery] DateTime fromDate, [FromQuery] DateTime toDate)
        {
            var result = await _orderService.GetAsync(locationId, fromDate, toDate);
            return result;
        }
    }
}