﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Infrastructure.Queries.Stock;
using Microsoft.AspNetCore.Mvc;

namespace Harvey.Retail.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StocksController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        private readonly ICommandExecutor _commandExecutor;
        public StocksController(IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
        {
            _queryExecutor = queryExecutor;
            _commandExecutor = commandExecutor;
        }

        [HttpPost]
        [Route("all")]
        public async Task<IEnumerable<StockModel>> GetAll([FromBody] List<Guid> variantIds)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetStocksByLocationQuery(variantIds));
            return result;
        }
    }
}