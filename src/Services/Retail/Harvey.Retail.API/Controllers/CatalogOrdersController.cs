﻿using Harvey.Retail.Application.Contexts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Harvey.Retail.API.Controllers
{
    [Route("api/catalog/orders")]
    public class CatalogOrdersController : ControllerBase
    {
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;
        private readonly CatalogContext _catalogContext;

        public CatalogOrdersController(CatalogContext catalogContext,
             IConfiguration configuration)
        {
            _client = new HttpClient();
            _configuration = configuration;
            _catalogContext = catalogContext;
        }
        [HttpPost]
        [Route("list/checkSyncStatus")]
        public async Task<ActionResult<IEnumerable<Guid>>> CheckOrdersSyncStatus([FromBody] List<Guid> orderIds)
        {
            return Ok(await _catalogContext.GetSyncedOrderIds(orderIds));
        }
    }
}
