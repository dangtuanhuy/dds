﻿using Harvey.Domain;
using Harvey.Exception;
using Harvey.Polly;
using Harvey.Retail.API.Extensions;
using Harvey.Retail.API.Policies;
using Harvey.Retail.Application.Checkout;
using Harvey.Retail.Application.Contexts;
using Harvey.Retail.Application.Domain;
using Harvey.Retail.Application.Infrastructure;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Infrastructure.Queries.Channels;
using Harvey.Retail.Application.Infrastructure.Queries.Report;
using Harvey.Retail.Application.Infrastructure.Queries.Stores;
using Harvey.Retail.Application.Infrastructure.Queries.Targets;
using Harvey.Retail.Application.Models;
using Harvey.Retail.Application.Models.Feeds;
using Harvey.Retail.Application.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Retail.API.Controllers
{
    [Route("api/syncs")]
    //[Authorize]
    public class SyncsController : ControllerBase
    {
        private readonly ILogger<SyncsController> _logger;
        private readonly CatalogContext _catalogContext;
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;
        private readonly ICheckoutFlow _checkoutFlow;
        private readonly IEventStoreRepository<Order> _eventStoreRepository;
        private readonly INextSequenceService<TransientRetailDbContext> _nextSequenceService;
        private readonly IQueryExecutor _queryExecutor;
        private readonly RetailDbContext _retailDbContext;
        private readonly SaleTransactionService _saleTransactionService;
        private readonly IExportService _exportService;
        private readonly ICommandExecutor _commandExecutor;
        private readonly ChannelContext _channelContext;

        public SyncsController(
            CatalogContext catalogContext,
            IConfiguration configuration,
            ICheckoutFlow checkoutFlow,
            ILogger<SyncsController> logger,
            IEventStoreRepository<Order> eventStoreRepository,
            INextSequenceService<TransientRetailDbContext> nextSequenceService,
            IQueryExecutor queryExecutor,
            RetailDbContext retailDbContext,
            SaleTransactionService saleTransactionService,
            ICommandExecutor commandExecutor,
            ChannelContext channelContext,
            IExportService exportService
            )
        {
            _logger = logger;
            _eventStoreRepository = eventStoreRepository;
            _checkoutFlow = checkoutFlow;
            _configuration = configuration;
            _catalogContext = catalogContext;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _nextSequenceService = nextSequenceService;
            _queryExecutor = queryExecutor;
            _retailDbContext = retailDbContext;
            _saleTransactionService = saleTransactionService;
            _commandExecutor = commandExecutor;
            _channelContext = channelContext;
            _exportService = exportService;
        }

        [Route("serverDetection")]
        [AllowAnonymous]
        public ActionResult<bool> ServerDetection()
        {
            return true;
        }

        [Route("catalog/categories")]
        public async Task<ActionResult<IEnumerable<Category>>> GetCategories()
        {
            return Ok(await _catalogContext.GetCategories());
        }

        [Route("catalog/products")]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts(DateTime? lastSync)
        {
            if (lastSync == null)
            {
                lastSync = DateTime.UtcNow;
            }
            var products = await _catalogContext.GetProducts(lastSync.Value);
            var productsWithCategoryIsNull = products.Where(x => x.CategoryId == null || x.CategoryId == Guid.Empty);
            if (productsWithCategoryIsNull != null && productsWithCategoryIsNull.Any())
            {
                var productIdsWithCategoryIsNull = productsWithCategoryIsNull.Select(x => x.Id).ToList();
                var endpoint = $"{_configuration["PimFeedApiUrl"]}/categoryIds";
                GetCategoryIdsByProdutIdsRequest request = new GetCategoryIdsByProdutIdsRequest();
                request.ProductIds = productIdsWithCategoryIsNull;
                var content = JsonConvert.SerializeObject(request);
                var response = await _client.PostAsync(endpoint,
                    new StringContent(content, Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        var getCategoryIdsResponse = JsonConvert.DeserializeObject<GetCategoryIdsByProdutIdsReponse>(jsonString);
                        var productCategoryIds = getCategoryIdsResponse.data;
                        if (productCategoryIds != null && productCategoryIds.Any())
                        {
                            await _catalogContext.UpdateCategoryIdsByProductIds(productCategoryIds);
                        }

                        foreach (var item in productsWithCategoryIsNull)
                        {
                            item.CategoryId = productCategoryIds.First(x => x.ProductId == item.Id).CategoryId;
                        }
                    }
                }
            }


            return Ok(products);
        }

        [Route("catalog/variants")]
        public async Task<ActionResult<IEnumerable<CatalogVariantModel>>> GetVariants(DateTime? lastSync, string locationId)
        {
            return Ok(await _catalogContext.GetVariants(lastSync, locationId));
        }

        [Route("catalog/stockbalance")]
        public async Task<ActionResult<IEnumerable<Product>>> GetStockBalance(DateTime? lastSync, string locationId)
        {
            return Ok(await _catalogContext.GetStockBalance(lastSync, Guid.Parse(locationId)));
        }

        [Route("catalog/barcodes")]
        public async Task<ActionResult<IEnumerable<BarCode>>> GetBarCodes(DateTime? lastSync)
        {
            return Ok(await _catalogContext.GetBarCodes(lastSync));
        }

        [Route("catalog/promotions")]
        public async Task<ActionResult<IEnumerable<CatalogPromotionModel>>> GetPromotions(DateTime? lastSync)
        {
            if (lastSync == null)
            {
                lastSync = DateTime.UtcNow;
            }

            return Ok(await _catalogContext.GetPromotions(lastSync.Value));
        }

        [Route("catalog/promotionConditions")]
        public async Task<ActionResult<IEnumerable<PromotionCondition>>> GetPromotionConditions()
        {
            return Ok(await _catalogContext.GetPromotionConditions());
        }

        [Route("catalog/promotionCouponcodes")]
        public async Task<ActionResult<IEnumerable<Promotion>>> GetPromotionCouponcodes()
        {
            return Ok(await _catalogContext.GetPromotionCouponCodes());
        }

        [Route("catalog/paymentMethods")]
        public async Task<ActionResult<IEnumerable<CatalogPaymentMethod>>> GetPaymentMethods()
        {
            return Ok(await _catalogContext.GetPaymentMethods());
        }

        [Route("customers")]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetCustomers(DateTime? lastSync)
        {
            var lastSyncString = "";
            if (lastSync != null)
            {
                lastSyncString = lastSync.Value.AddMinutes(-1).ToString("yyyy-MM-dd HH:mm:ss");
            }
            var endpoint = $"{_configuration["Crmloyalty"]}/api/feed/customers";

            endpoint += $"?secretKey={SecretKeyExtension.CrmSecretKey}";
            endpoint += string.IsNullOrEmpty(lastSyncString) ? "" : $"&lastSync={lastSyncString}";
            
            try
            {
                var response = await _client.GetAsync(endpoint);
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var data = await ParseResponse<UserModel>(response);
                        return Ok(data.OrderByDescending(x => x.CreatedDate));
                    }
                    catch (System.Exception e)
                    {
                        
                    }
                }
            }
            catch (System.Exception)
            {

            }

            return new List<UserModel>();
        }

        [Route("customer/membership-inspection")]
        public async Task<ActionResult<CustomerMembershipModel>> CheckCustomerMembership(Guid customerId)
        {
            if (customerId == Guid.Empty)
            {
                return null;
            }

            var endpoint = $"{_configuration["Crmloyalty"]}/api/feed/customers/membership-inspection";
            endpoint += $"?customerId={customerId}";
            endpoint += $"&secretKey={SecretKeyExtension.CrmSecretKey}";

            try
            {
                var response = await _client.GetAsync(endpoint);
                if (response.IsSuccessStatusCode)
                {
                    var data = await ParseResponseModel<CustomerMembershipModel>(response);
                    return Ok(data);
                }
            }
            catch (System.Exception)
            {

            }

            return null;
        }

        [Route("staffs")]
        public async Task<ActionResult<IEnumerable<StaffModel>>> GetStaffss(DateTime? lastSync)
        {
            var endpoint = $"{_configuration["Authority"]}/api/users/staffs";
            if (lastSync != null)
            {
                var lastSyncString = lastSync.Value.ToString("yyyy-MM-dd HH:mm:ss");
                endpoint += $"?lastSync={lastSyncString}";
            }

            try
            {
                var response = await _client.GetAsync(endpoint);
                if (response.IsSuccessStatusCode)
                {
                    var staffs = await ParseResponse<StaffModel>(response);
                    foreach (var item in staffs)
                    {
                        MD5 md5 = new MD5CryptoServiceProvider();
                        var pin = string.IsNullOrEmpty(item.Pin) ? "123456" : item.Pin;
                        md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(pin));
                        byte[] pinHash = md5.Hash;
                        StringBuilder strBuilder = new StringBuilder();
                        for (int i = 0; i < pinHash.Length; i++)
                        {
                            strBuilder.Append(pinHash[i].ToString("x2"));
                        }

                        item.PinHash = strBuilder.ToString();
                        item.Pin = "";
                    }
                    return Ok(staffs);
                }
                throw new HttpClientException(response.StatusCode, $"Cannot get data from endpoint {endpoint}");
            }
            catch (System.Exception e)
            {

                throw;
            }

        }

        [Route("store/{deviceCode}")]
        public async Task<ActionResult<StoreModel>> GetStoreByDeviceCode(string deviceCode)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetStoreByDeviceCodeQuery(deviceCode));
            return Ok(result);
        }

        [Route("saleTargets")]
        public async Task<ActionResult<IEnumerable<SaleTargetModel>>> GetSaleTargets(DateTime? lastSync, Guid? storeId)
        {
            var result = await _queryExecutor.ExecuteAsync(new GetSaleTargetsByStoreQuery(storeId, lastSync));
            return Ok(result);
        }



        [HttpGet("locations")]
        public async Task<ActionResult> GetLocations()
        {
            var result = await _queryExecutor.ExecuteAsync(new GetAllStoreToSyncPOSQuery());
            return Ok(result);
        }

        [HttpPost("sales")]
        public async Task<ActionResult> Sales([FromBody]List<OrderModel> orders)
        {
            if (orders == null || !orders.Any())
            {
                return Ok();
            }

            orders = orders.OrderBy(x => x.CreatedDate).ToList();
            foreach (OrderModel order in orders)
            {
                var orderId = order.Id;
                var cartItems = new List<CartItem>();
                if (order.OrderItems != null && order.OrderItems.Count > 0)
                {
                    foreach (OrderItemModel orderItem in order.OrderItems)
                    {
                        var cartItemPromotions = new List<CartItemPromotion>();
                        if (orderItem.OrderItemPromotions != null && orderItem.OrderItemPromotions.Any())
                        {
                            foreach (var orderItemPromotion in orderItem.OrderItemPromotions)
                            {
                                cartItemPromotions.Add(new CartItemPromotion
                                {
                                    Id = orderItemPromotion.Id,
                                    CartItemId = orderItem.Id,
                                    DiscountType = orderItemPromotion.DiscountType,
                                    Value = orderItemPromotion.Value
                                });
                            }
                        }

                        cartItems.Add(new CartItem()
                        {
                            CorrelationId = orderId,
                            Id = orderItem.Id,
                            Amount = orderItem.Amount,
                            VariantId = orderItem.VariantId,
                            VariantName = orderItem.VariantName,
                            PriceId = orderItem.PriceId,
                            StockTypeId = orderItem.StockTypeId,
                            Quantity = orderItem.Quantity,
                            IsDelete = orderItem.IsDelete,
                            Price = orderItem.Price,
                            SKUCode = orderItem.SKUCode,
                            ItemPromotions = cartItemPromotions,
                            PreviousOrderItemId = orderItem.PreviousOrderItemId,
                            Status = orderItem.Status
                        });
                    }

                }
                var cartPromotions = new List<CartPromotion>();
                foreach (var orderPromotion in order.OrderPromotions)
                {
                    cartPromotions.Add(new CartPromotion()
                    {
                        CorrelationId = orderId,
                        Id = orderPromotion.Id,
                        DiscountType = orderPromotion.DiscountType,
                        OrderId = orderPromotion.OrderId,
                        PromotionId = orderPromotion.PromotionId,
                        Reason = orderPromotion.Reason,
                        Value = orderPromotion.Value
                    });
                }
                var cartPayments = order.OrderPayments.Select(x => new CartPayment
                {
                    Id = x.Id,
                    OrderId = x.OrderId,
                    PaymentCode = x.PaymentCode,
                    Amount = x.Amount
                }).ToList();
                var cart = new Cart()
                {
                    CorrelationId = orderId,
                    Amount = order.Amount,
                    CreatedDate = order.CreatedDate,
                    CustomerId = order.CustomerId,
                    IsDelete = order.IsDelete,
                    Items = cartItems,
                    Method = order.Method,
                    LocationId = order.StoreId,
                    CartPromotions = cartPromotions,
                    CartPayments = cartPayments,
                    GST = order.GST,
                    GSTInclusive = order.GSTInclusive,
                    BillNo = order.BillNo,
                    CashierId = order.CashierId,
                    CashierName = order.CashierName,
                    CustomerCode = order.CustomerCode,
                    CustomerName = order.CustomerName,
                    PreviousOrderId = order.PreviousOrderId,
                    OriginalOrderId = order.OriginalOrderId,
                    OrderTransactionType = order.OrderTransactionType,
                    Reason = order.Reason,
                    DeviceId = _channelContext.DeviceId,
                    Collect = order.Collect
                };
                var customerInfo = new CustomerInfo();
                var paymentInfo = new PaymentInfo()
                {
                    OrderId = orderId,
                    Id = Guid.NewGuid(),
                    Amount = 100
                };
                var delivery = new Deliverable()
                {
                    OrderId = orderId,
                    Id = Guid.NewGuid()
                };

                var policy = new CheckoutRetrivalPolicy();

                await _checkoutFlow.PlaceOrder(cart, customerInfo, _channelContext.ServerInformation);

                await ((IRetrivalPolicy)policy).ExecuteStrategyAsync(_logger, () =>
                {
                    var savedOrder = _eventStoreRepository.GetByIdAsync(orderId).Result;
                    if (savedOrder != null
                        && savedOrder.State == OrderState.Completed
                        && savedOrder.Payment != null && savedOrder.Payment.State == PaymentState.Paid
                        && savedOrder.Delivery.State == DeliveryState.Delivered)
                    {
                    }
                    else
                    {
                        //throw new CheckoutException("Cannot place order. Please try again.");
                    }

                });
            }
            return Ok();
        }

        private async Task<IEnumerable<T>> ParseResponse<T>(HttpResponseMessage response)
        {
            var jsonString = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(jsonString))
            {
                throw new HttpClientException(response.StatusCode, $"Unexpected Json result. Result was null");
            }

            return JsonConvert.DeserializeObject<IEnumerable<T>>(jsonString);

        }

        private async Task<T> ParseResponseModel<T>(HttpResponseMessage response)
        {
            var jsonString = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(jsonString))
            {
                throw new HttpClientException(response.StatusCode, $"Unexpected Json result. Result was null");
            }

            return JsonConvert.DeserializeObject<T>(jsonString);

        }


        [HttpPost("catalog/saleTransactions")]
        public async Task<ActionResult> GetSaleTransactions([FromBody]SaleTransactionModel model)
        {
            var fromDate = model.ToDate.Date;
            var toDate = model.ToDate.Date.AddDays(1);
            var saleTransactionForGTOReportModel = new SaleTransactionForGTOReportModel
            {
                LocationId = model.LocationId,
                MachineId = model.MachineId,
                ReportType = model.ReportType,
                FromDate = fromDate,
                ToDate = toDate
            };
            var data = await _saleTransactionService.GetSaleTransactionsForGTOReport(saleTransactionForGTOReportModel);
            var result = data.Item1;
            var fileName = data.Item2;
            var extentionFile = data.Item3;

            return File(result, "text/csv", $"{fileName}.{extentionFile}");
        }

        [HttpGet("catalog/countChannelData")]
        [AllowAnonymous]
        public async Task<ActionResult> CountChannelData(string channelName, CountChannelDataType dataType)
        {
            var result = await _queryExecutor.ExecuteAsync(new CountChannelDataQuery(channelName, dataType));
            return Ok(result);
        }        

        [HttpPost("store/saveOpenDay")]
        public async Task<ActionResult> SaveOpenDay([FromBody]OpenDayModel model)
        {
            var result = await _queryExecutor.ExecuteAsync(new AddOpenDayQuery(model));
            return Ok(result);
        }

        [HttpPost("store/saveEndDay")]        
        public async Task<ActionResult> SaveEndDay([FromBody]EndDayModel model)
        {
            var result = await _queryExecutor.ExecuteAsync(new AddEndDayQuery(model));
            return Ok(result);            
        }

        [HttpPost("catalog/exportEndDayReport")]
        public async Task<ActionResult> ExportEndDayReport([FromBody]EndDayReportModel model)
        {
            var data = await _exportService.ExportEndDayReport(model);
            var result = data.Item1;
            var fileName = data.Item2;
            var extentionFile = data.Item3;
            return FileExtension.GetFileUTF8(result, fileName + "." + extentionFile);
        }
    }
}
