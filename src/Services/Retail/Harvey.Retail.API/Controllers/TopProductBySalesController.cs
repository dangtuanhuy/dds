﻿using Harvey.Domain;
using Harvey.Retail.API.Extensions;
using Harvey.Retail.Application.Infrastructure.Models;
using Harvey.Retail.Application.Infrastructure.Queries.TopProductBySales;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.API.Controllers
{
    [Authorize]
    [Route("api/top-product-by-sales")]
    public class TopProductBySalesController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        public TopProductBySalesController(IQueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<TopProductSaleModel>> GetTopProductBySales(TopProductSaleRequestModel topProductBySalesRequest, string secretKey)
        {
            if (secretKey != SecretKeyExtension.SystemSecretKey)
            {
                return new List<TopProductSaleModel>();
            }
            var result = await _queryExecutor.ExecuteAsync(new PimGetProductBySalesQuery(topProductBySalesRequest.FromDate, topProductBySalesRequest.ToDate));
            return result;
        }
    }
}
