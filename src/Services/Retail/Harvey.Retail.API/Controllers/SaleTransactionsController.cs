﻿using Harvey.Domain;
using Harvey.Retail.API.Extensions;
using Harvey.Retail.Application.Infrastructure.Queries.TransactionSale;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.Retail.API.Controllers
{
    [Authorize]
    [Route("api/sale-transaction")]
    public class SaleTransactionsController : ControllerBase
    {
        private readonly IQueryExecutor _queryExecutor;
        public SaleTransactionsController(IQueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }

        [HttpGet]
        [Route("exportcsv")]
        public async Task<IActionResult> ExportCSV([FromQuery]DateTime fromDate, [FromQuery]DateTime toDate, [FromQuery]List<Guid> storeIds)
        {
            var buffer = await _queryExecutor.ExecuteAsync(new ExportTransactionSalesCSVQuery(fromDate, toDate, storeIds));
            return FileExtension.GetFileUTF8(buffer, "SaleTransactions.csv");
        }
    }
}
