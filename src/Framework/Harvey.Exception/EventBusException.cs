﻿namespace Harvey.Exception
{
    public class EventBusException : ProductionException
    {
        public EventBusException()
        {
        }

        public EventBusException(string message) :
            base(message)
        {
        }

        public EventBusException(string message, System.Exception inner) :
            base(message, inner)
        {
        }
    }
}
