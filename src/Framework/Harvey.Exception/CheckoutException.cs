﻿namespace Harvey.Exception
{
    public class CheckoutException : ProductionException
    {
        public CheckoutException()
        {
        }

        public CheckoutException(string message) :
            base(message)
        {
        }

        public CheckoutException(string message, System.Exception inner) :
            base(message, inner)
        {
        }
    }
}
