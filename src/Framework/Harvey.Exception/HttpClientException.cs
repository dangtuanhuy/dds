﻿using System.Net;

namespace Harvey.Exception
{
    public class HttpClientException : System.Exception
    {
        public HttpStatusCode StatusCode { get; set; }
        public HttpClientException()
        {
        }

        public HttpClientException(HttpStatusCode statusCode, string message, System.Exception innerException) : base(message, innerException)
        {
            StatusCode = statusCode;
        }

        public HttpClientException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}
