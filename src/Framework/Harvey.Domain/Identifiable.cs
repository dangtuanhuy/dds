﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Domain
{
    public class Identifiable : EntityBase
    {
        public string IdentifiedId { get; set; }
    }
}
