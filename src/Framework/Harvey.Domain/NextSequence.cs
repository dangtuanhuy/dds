﻿namespace Harvey.Domain
{
    public class NextSequence : EntityBase
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
