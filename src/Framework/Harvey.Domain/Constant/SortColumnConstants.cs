﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Domain.Constant
{
    public static class SortColumnConstants
    {
        public const string DefaultSortColumn = "UpdatedDate";
        public const string SecondSortColumn = "CreatedDate";
    }

    public enum SortDirection
    {
        Ascending = 0,
        Descending = 1
    }
}
