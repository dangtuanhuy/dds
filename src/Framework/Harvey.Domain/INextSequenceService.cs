﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Harvey.Domain
{
    public interface INextSequenceService<TContext>
        where TContext : DbContext
    {
        string Get(string key, int numberOfChars = 5);
        NextSequence GetNextEntity(string key, int numberOfChars = 5);
        void UpdateRange(Dictionary<string, int> values, int numberOfChars = 5);
        string GetExtentionSaleTransactionFile(string key);
        List<NextSequence> GetAllCurrentNextSequence();
        NextSequence AddNextSequence(string key, int numberOfChars);
    }
}
