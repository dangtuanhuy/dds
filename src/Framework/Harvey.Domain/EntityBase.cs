﻿using System;

namespace Harvey.Domain
{
    public abstract class EntityBase
    {
        public EntityBase()
        {

        }
        public Guid Id { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public DateTime UpdatedDate { get; set; }
    }
}
