﻿namespace Harvey.Logging
{
    public enum Application
    {
        PIM = 1,
        PurchaseOrder = 2,
        Retail = 3,
        Promotion = 4,
        Pos = 5
    }
}
