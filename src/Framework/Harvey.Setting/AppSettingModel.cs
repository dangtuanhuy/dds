﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Setting
{
    public class AppSettingModel
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
