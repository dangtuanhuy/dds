﻿using Harvey.Reporting.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Harvey.Reporting
{
    public class ReportModel<T>
    {
        public string ReportId { get; set; }
        public ReportType Type { get; set; }
        public T DataSource { get; set; }
        public Dictionary<string, object> Paramemters { get; set; }

        public ReportModel<string> ToPostModel()
        {
            return new ReportModel<string>()
            {
                ReportId = this.ReportId,
                Paramemters = this.Paramemters,
                Type = this.Type,
                DataSource = JsonConvert.SerializeObject(this.DataSource)
            };
        }
    }
}
