﻿using Harvey.Reporting.Enums;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.Reporting
{
    public class ReportServer
    {
        private const string CONTENT_TYPE = "application/json";
        public class Result
        {
            public string ReportId { get; private set; }
            public string EndPoint { get; private set; }
            public ReportServerStatus Status { get; private set; }
            public static Result Ready(string reportId, string endPoint)
            {
                return new Result()
                {
                    EndPoint = endPoint,
                    ReportId = reportId,
                    Status = ReportServerStatus.Ready
                };
            }

            public static Result NotReady()
            {
                return new Result()
                {
                    Status = ReportServerStatus.NotReady
                };
            }
        }
        private readonly HttpClient _client;
        private readonly string _serverUrl;
        public ReportServer(string serverUrl)
        {
            _serverUrl = serverUrl;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(CONTENT_TYPE));

        }
        public async Task<Result> PrepareData<T>(ReportModel<T> reportData)
        {
            var response = await _client.PostAsync(_serverUrl, new StringContent(JsonConvert.SerializeObject(reportData.ToPostModel()), Encoding.UTF8, CONTENT_TYPE));
            var reportId = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

            if (response.IsSuccessStatusCode)
            {
                if(reportId != null)
                {
                    return Result.Ready(reportId.ToString(), $"reportType={(int)reportData.Type}&reportId={reportId.ToString()}");
                }
                return Result.Ready(reportData.ReportId, $"reportType={(int)reportData.Type}&reportId={reportData.ReportId}");
            }
            else
            {
                return Result.NotReady();
            }
        }
    }
}
