﻿namespace Harvey.Reporting.Enums
{
    public enum ReportServerStatus
    {
        Ready = 1,
        NotReady = 2
    }
}
