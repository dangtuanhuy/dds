﻿namespace Harvey.Reporting.Enums
{
    public enum ReportType
    {
        TransferIn = 1,
        TransferOut = 2,
        GoodsInWard = 3,
        PurchaseOrder = 4,
        PurchaseReturn = 5,
        SalesInvoice = 6
    }
}
