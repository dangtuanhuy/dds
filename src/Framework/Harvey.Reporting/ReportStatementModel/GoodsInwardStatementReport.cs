﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Reporting.ReportStatementModel
{
    public class GoodsInwardStatementHeader : ReportBase
    {
        public Guid Id { get; set; }
        public string VendorName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string GIDNo { get; set; }
        public string GIDDate { get; set; }
        public string InvoiceNo { get; set; }
        public string GSTRegNo { get; set; }
        public string Discount { get; set; }
        public string SubTotal { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Tax { get; set; }
        public string NetTotal { get; set; }
        public List<GoodsInwardStatementDetail> Details { get; set; } = new List<GoodsInwardStatementDetail>();
    }

    public class GoodsInwardStatementDetail : ReportBase
    {
        public Guid Id { get; set; }
        public string InventoryCode { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public string Quantity { get; set; }
        public string Cost { get; set; }
        public string TotalValue { get; set; }
    }
}
