﻿using System;
using System.Collections.Generic;

namespace Harvey.Reporting.ReportStatementModel
{
    public class SalesInvoiceHeader : ReportBase
    {
        public Guid Id { get; set; }
        public string VendorName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public string GSTRegNo { get; set; }
        public string PaymentTerms { get; set; }
        public string DueDate { get; set; }
        public string Discount { get; set; }
        public string SubTotal { get; set; }
        public string Tax { get; set; }
        public string TaxRoundOff { get; set; }
        public string NetTotal { get; set; }
        public string Fax { get; set; }
        public string SoDo { get; set; }
        public List<SalesInvoiceDetail> Details { get; set; } = new List<SalesInvoiceDetail>();
    }

    public class SalesInvoiceDetail : ReportBase
    {
        public Guid Id { get; set; }
        public string InventoryCode { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public string Quantity { get; set; }
        public string UnitPrice { get; set; }
        public string TotalAmount { get; set; }
    }
}
