﻿using System;
using System.Collections.Generic;

namespace Harvey.Reporting.ReportStatementModel
{
    public class TransferOutStatementHeader : ReportBase
    {
        public Guid Id { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public string TransferNo { get; set; }
        public string TransferDate { get; set; }
        public int TotalQuantity { get; set; }
        public string TotalTransferPrice { get; set; }
        public List<TransferOutStatementDetail> Details { get; set; } = new List<TransferOutStatementDetail>();
    }

    public class TransferOutStatementDetail : ReportBase
    {
        public Guid Id { get; set; }
        public string BarCode { get; set; }
        public string InventoryCode { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public string Price { get; set; }
        public int TransQuantity { get; set; }
        public string TotalProductPrice { get; set; }
    }
}
