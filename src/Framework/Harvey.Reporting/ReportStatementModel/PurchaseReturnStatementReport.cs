﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Reporting.ReportStatementModel
{
    public class PurchaseReturnStatementHeader : ReportBase
    {
        public Guid Id { get; set; }
        public string VendorName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string PRNo { get; set; }
        public string PRDate { get; set; }
        public string GSTRegNo { get; set; }
        public string SubTotal { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Tax { get; set; }
        public string NetTotal { get; set; }
        public List<PurchaseReturnStatementDetail> Details { get; set; } = new List<PurchaseReturnStatementDetail>();
    }

    public class PurchaseReturnStatementDetail : ReportBase
    {
        public Guid Id { get; set; }
        public string InventoryCode { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public string Quantity { get; set; }
        public string UnitCost { get; set; }
        public string NetCost { get; set; }
    }
}
