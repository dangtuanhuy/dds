﻿using System;
using System.Collections.Generic;

namespace Harvey.Reporting.ReportStatementModel
{
    public class TransferInStatementHeader : ReportBase
    {
        public Guid Id { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public string TransferNo { get; set; }
        public string TransferDate { get; set; }
        public int TotalQuantity { get; set; }
        public List<TransferInStatementDetail> Details { get; set; } = new List<TransferInStatementDetail>();
    }

    public class TransferInStatementDetail : ReportBase
    {
        public Guid Id { get; set; }
        public string BarCode { get; set; }
        public string InventoryCode { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public int TransQuantity { get; set; }
    }
}
