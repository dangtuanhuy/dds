﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.EventBus.RabbitMQ.Tests.MockModels
{
    public class AnotherMockEventHandler2 : EventHandlerBase<AnotherMockEvent>
    {
        public bool HasExecuted = false;
        public AnotherMockEventHandler2(IRepository<IdentifiedEvent> repository, ILogger<AnotherMockEventHandler2> logger) : base(repository, logger)
        {

        }
        protected override async Task ExecuteAsync(AnotherMockEvent @event)
        {
            HasExecuted = true;
            await Task.CompletedTask;
        }
    }
}
