﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.EventBus.RabbitMQ.Tests.MockModels
{
    public class MockEventHandler : EventHandlerBase<MockEvent>
    {
        public bool HasExecuted = false;
        public MockEventHandler(IRepository<IdentifiedEvent> repository, ILogger<MockEventHandler> logger) : base(repository, logger)
        {

        }
        protected override async Task ExecuteAsync(MockEvent @event)
        {
            HasExecuted = true;
            await Task.CompletedTask;
        }
    }
}
