﻿using Harvey.Domain;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Policies;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.EventBus.RabbitMQ.Tests.MockModels
{
    public class AnotherMockEventHandler : EventHandlerBase<MockEvent>
    {
        public bool HasExecuted = false;
        public AnotherMockEventHandler(IRepository<IdentifiedEvent> repository, ILogger<AnotherMockEventHandler> logger) : base(repository, logger)
        {

        }
        protected override async Task ExecuteAsync(MockEvent @event)
        {
            HasExecuted = true;
            await Task.CompletedTask;
        }
    }
}
