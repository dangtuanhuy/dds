﻿using Harvey.Exception;
using Harvey.Polly;
using System.Collections.Generic;

namespace Harvey.EventBus.RabbitMQ.Policies
{
    public class BusHandlerRetrivalPolicy : IRetrivalPolicy
    {
        public int NumbersOfRetrival => 10;
        public RetrivalStategy RetrivalStategy => RetrivalStategy.Exponential;
        public int Delay => 2;
        public List<System.Exception> HandledExceptions => new List<System.Exception>() {
           new EventBusException()
        };
    }
}