﻿using Harvey.Domain;
using Harvey.EventBus.Policies;
using Harvey.Exception.Extensions;
using Harvey.Polly;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harvey.EventBus
{
    public abstract class EventHandlerBase<TEvent> where TEvent : EventBase
    {
        private readonly DbContextIdempotentPolicy<TEvent> _dbContextIdempotentPolicy;
        private readonly IRepository<IdentifiedEvent> _repository;
        protected readonly ILogger<EventHandlerBase<TEvent>> Logger;
        protected virtual List<IIdempotentPolicy<TEvent>> AdditionalIdempotentPolicies => new List<IIdempotentPolicy<TEvent>>();
        public EventHandlerBase(IRepository<IdentifiedEvent> repository, ILogger<EventHandlerBase<TEvent>> logger)
        {
            _repository = repository;
            Logger = logger;
            _dbContextIdempotentPolicy = new DbContextIdempotentPolicy<TEvent>(repository);
        }
        protected abstract Task ExecuteAsync(TEvent @event);

        public virtual Task<HandlerResult> Handle(TEvent @event)
        {
            var eventIdempotentDetected = true;
            var additonalIdempotentDetected = true;

            ((IIdempotentPolicy<TEvent>)_dbContextIdempotentPolicy).ExecuteStategyAsync(@event, Logger, () =>
            {
                eventIdempotentDetected = false;
            });

            if (AdditionalIdempotentPolicies.Any())
            {
                AdditionalIdempotentPolicies.ForEach(x => x.ExecuteStategyAsync(@event, Logger, () =>
                {
                    additonalIdempotentDetected = false;
                }));
            }
            else
            {
                additonalIdempotentDetected = false;
            }

            var htype = this.GetType();

            if (!eventIdempotentDetected && !additonalIdempotentDetected)
            {
                return ExecuteAsync(@event).ContinueWith((t) =>
            {
                if (t.IsCompleted && !t.IsFaulted && !t.IsCanceled)
                {
                    Logger.LogInformation($"[EventBus] [Success] [Handler {htype.FullName} {typeof(TEvent).Name}] {JsonConvert.SerializeObject(@event)}");
                    _repository.AddAsync(new IdentifiedEvent()
                    {
                        EventId = @event.Id,
                        Data = JsonConvert.SerializeObject(@event),
                        Handler = htype.FullName
                    }).Wait();
                    _repository.SaveChangesAsync().Wait();
                    return HandlerResult.Success();
                }
                else
                {
                    Logger.LogInformation($"[EventBus] [Failure] [Handler {htype.FullName} {typeof(TEvent).Name}] {JsonConvert.SerializeObject(@event)}");
                    Logger.LogError(t.Exception.GetTraceLog());
                    var existedIdentifiedEvents = _repository.ListAsync(x => x.EventId == @event.Id && x.Handler == htype.FullName).Result.ToList();
                    foreach (var item in existedIdentifiedEvents)
                    {
                        _repository.DeleteAsync(item).Wait();
                    }
                    _repository.SaveChangesAsync().Wait();
                    return HandlerResult.Fail(t.Exception);
                }
            });
            }
            else
            {
                Logger.LogInformation($"[EventBus] [Idempotent Detected] [Handler {htype.FullName} {typeof(TEvent).Name}] {JsonConvert.SerializeObject(@event)}");
                return Task.FromResult(HandlerResult.Success());
            }
        }
    }
}
