﻿using Harvey.Domain;
using System;

namespace Harvey.EventBus
{
    public class IdentifiedEvent : EntityBase
    {
        public Guid EventId { get; set; }
        public string Data { get; set; }
        public string Handler { get; set; }
    }
}
