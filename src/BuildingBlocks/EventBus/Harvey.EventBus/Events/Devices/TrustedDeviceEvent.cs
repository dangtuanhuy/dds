﻿using System;

namespace Harvey.EventBus.Events.Devices
{
    public class TrustedDeviceEvent : EventBase
    {
        public string UserCode { get; set; }
        public Guid UserId {get;set;}

        public TrustedDeviceEvent()
        {
        }

        public TrustedDeviceEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
