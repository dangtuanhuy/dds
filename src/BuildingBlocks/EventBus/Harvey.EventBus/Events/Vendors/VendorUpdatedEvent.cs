﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.Vendors
{
    public class VendorUpdatedEvent : EventBase
    {
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Email { get; set; }
        public string VendorUrl { get; set; }
        public string Country { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string Phone { get; set; }
        public string PaymentTermName { get; set; }
        public string CurrencyName { get; set; }
        public string TaxTypeValue { get; set; }
        public string ZipCode { get; set; }
        public string Fax { get; set; }
        public string VendorCode { get; set; }

        public VendorUpdatedEvent()
        {

        }
        public VendorUpdatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
