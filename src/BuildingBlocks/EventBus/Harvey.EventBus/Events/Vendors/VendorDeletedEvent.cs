﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.Vendors
{
    public class VendorDeletedEvent : EventBase
    {
        public string Name { get; set; }
        public bool Active { get; set; }

        public VendorDeletedEvent()
        {

        }
        public VendorDeletedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
