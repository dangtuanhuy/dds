﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.PurchaseOrders
{
    public class PurchaseOrderConfirmStatusEvent : EventBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid VendorId { get; set; }
        public int ApprovalStatus { get; set; }
        public List<dynamic> PurchaseOrderItems { get; set; }
        public int Type { get; set; }
        public PurchaseOrderConfirmStatusEvent()
        {

        }
        public PurchaseOrderConfirmStatusEvent(string aggregateId) : base(aggregateId)
        {
        }
    }


}
