﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.PurchaseOrders
{
    public class PurchaseOrderDeletedEvent : EventBase
    {
        public PurchaseOrderDeletedEvent()
        {

        }
        public PurchaseOrderDeletedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
