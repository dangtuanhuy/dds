﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.PurchaseOrders
{
    public class PurchaseOrderUpdatedStatusEvent : EventBase
    {
        public List<Guid> Ids { get; set; }
        public PurchaseOrderUpdatedStatusEvent()
        {

        }
        public PurchaseOrderUpdatedStatusEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
