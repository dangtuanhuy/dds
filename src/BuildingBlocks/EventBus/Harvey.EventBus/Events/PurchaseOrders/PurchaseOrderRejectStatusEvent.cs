﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.PurchaseOrders
{
    public class PurchaseOrderRejectStatusEvent : EventBase
    {
        public PurchaseOrderRejectStatusEvent()
        {

        }
        public PurchaseOrderRejectStatusEvent(string aggregateId) : base(aggregateId)
        {
        }
    }

}
