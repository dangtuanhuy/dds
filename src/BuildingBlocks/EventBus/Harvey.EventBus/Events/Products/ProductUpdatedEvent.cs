﻿using System;

namespace Harvey.EventBus.Events.Products
{
    public class ProductUpdatedEvent : EventBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string IndexName { get; set; }
        public string IndexingValue { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Code { get; set; }
        public bool IsDelete { get; set; }
        public Guid Creator { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid UpdatedBy { get; set; }
        public ProductUpdatedEvent()
        {

        }
        public ProductUpdatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
