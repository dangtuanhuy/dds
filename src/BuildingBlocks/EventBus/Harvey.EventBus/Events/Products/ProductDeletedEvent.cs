﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.Products
{
    public class ProductDeletedEvent : EventBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Code { get; set; }
        public string IndexName { get; set; }
        public string IndexingValue { get; set; }
        public bool IsDelete { get; set; }
        public DateTime UpdatedDate { get; set; }
        public ProductDeletedEvent()
        {

        }
        public ProductDeletedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
