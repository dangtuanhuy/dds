﻿using System;
using System.Collections.Generic;

namespace Harvey.EventBus.Events.Promotions.PromotionCouponCodes
{
    public class PromotionCouponCodeCreatedEvent : EventBase
    {
        public string Code { get; set; }
        public bool IsUsed { get; set; }
        public Guid PromotionDetailId { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<Guid> LocationIds { get; set; }
    }
}
