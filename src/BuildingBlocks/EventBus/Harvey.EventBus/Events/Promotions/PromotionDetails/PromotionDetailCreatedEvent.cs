﻿using System;
using System.Collections.Generic;

namespace Harvey.EventBus.Events.Promotions.PromotionDetails
{
    public class PromotionDetailCreatedEvent : EventBase
    {
        public int DiscountTypeId { get; set; }
        public long Value { get; set; }
        public bool IsUseCouponCodes { get; set; }
        public bool IsUseConditions { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<Guid> LocationIds { get; set; }

    }
}
