﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.StockAndPriceLevels
{
    public class StockAndPriceLevelUpdatedEvent : EventBase
    {
        public List<dynamic> stockAndPrices { get; set; }
        public StockAndPriceLevelUpdatedEvent()
        {

        }
        public StockAndPriceLevelUpdatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
