﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Events.StockLevels
{
    public class StockLevelUpdatedEvent : EventBase
    {
        public Guid LocationId { get; set; }
        public StockLevelUpdatedEvent()
        {
        }
        public StockLevelUpdatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
