﻿using System;

namespace Harvey.EventBus.Events.StockLevels
{
    public class StockHasTransferedInStoreEvent : EventBase
    {
        public Guid VariantId { get; set; }
        public Guid StockTypeId { get; set; }
        public int Quantity { get; set; }
        public Guid StoreId { get; set; }
        public Guid StockTransactionRefId { get; set; }
        public Guid TransactionTypeId { get; set; }
        public StockHasTransferedInStoreEvent()
        {
        }

        public StockHasTransferedInStoreEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
