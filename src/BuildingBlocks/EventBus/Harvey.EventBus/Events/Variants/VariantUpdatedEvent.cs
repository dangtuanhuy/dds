﻿using System;

namespace Harvey.EventBus.Events.Variants
{
    public class VariantUpdatedEvent : EventBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime UpdatedDate { get; set; }
        public VariantUpdatedEvent()
        {

        }
        public VariantUpdatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
