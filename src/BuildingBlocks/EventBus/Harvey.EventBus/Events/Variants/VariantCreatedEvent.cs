﻿using System;

namespace Harvey.EventBus.Events.Variants
{
    public class VariantCreatedEvent : EventBase
    {
        public Guid VariantId { get; set; }
        public Guid ProductId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Desciption { get; set; }

        public VariantCreatedEvent()
        {

        }
        public VariantCreatedEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
