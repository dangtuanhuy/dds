﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Publishers
{
    public class StockAndPriceLevelPublisher : IPublisher
    {
        public string Name => "harvey_stock_and_price_level_publisher_queue";

        public Guid? CorrelationId { get; set; }
    }
}
