﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Publishers
{
    public class PurchasePIMtoPOPublisher : IPublisher
    {
        public string Name => "pim_purchase_order_po_operation";

        public Guid? CorrelationId { get; set; }
    }
}
