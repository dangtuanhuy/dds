﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Publishers
{
    public class StockLevelPublisher : IPublisher
    {
        public string Name => "harvey_stock_level_publisher_queue";

        public Guid? CorrelationId { get; set; }
    }
}
