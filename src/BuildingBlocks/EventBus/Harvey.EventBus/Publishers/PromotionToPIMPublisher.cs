﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Publishers
{
    public class PromotionToPIMPublisher : IPublisher
    {
        public string Name => "promotion_to_pim_operation";

        public Guid? CorrelationId { get; set; }
    }
}
