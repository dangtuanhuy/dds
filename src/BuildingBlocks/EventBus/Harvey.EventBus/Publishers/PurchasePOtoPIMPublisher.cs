﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.EventBus.Publishers
{
    public class PurchasePOtoPIMPublisher : IPublisher
    {
        public string Name => "po_purchase_order_pim_operation";

        public Guid? CorrelationId { get; set; }
    }
}
