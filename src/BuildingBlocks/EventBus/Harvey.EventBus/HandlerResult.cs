﻿namespace Harvey.EventBus
{
    public class HandlerResult
    {
        public bool IsFaulted { get; set; }
        public System.Exception Exception { get; set; }

        public static HandlerResult Success()
        {
            return new HandlerResult()
            {
                IsFaulted = false
            };
        }
        public static HandlerResult Fail(System.Exception ex)
        {
            return new HandlerResult()
            {
                IsFaulted = true,
                Exception = ex
            };
        }

    }
}
