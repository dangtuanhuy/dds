﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.EventBus.Abstractions
{
    public interface IEventStore
    {
        Task<IEnumerable<EventBase>> ReadEventsAsync(string aggregateId);
        Task<bool> AppendEventAsync(EventBase @event);
    }
}
