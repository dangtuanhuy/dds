﻿using Harvey.Domain;
using Harvey.Polly;

namespace Harvey.EventBus.Policies
{
    public class DbContextIdempotentPolicy<TEvent> : IIdempotentPolicy<TEvent>
        where TEvent : EventBase
    {
        private readonly IRepository<IdentifiedEvent> _repository;
        public DbContextIdempotentPolicy(IRepository<IdentifiedEvent> repository)
        {
            _repository = repository;
        }
        public void Handle(TEvent entity)
        {
        }

        public bool ShouldHandle(TEvent entity)
        {
            var htype = typeof(EventHandlerBase<>).MakeGenericType(typeof(TEvent));
            return _repository.Count(x => x.EventId == entity.Id && x.Handler == htype.FullName).Result > 0;
        }
    }
}
