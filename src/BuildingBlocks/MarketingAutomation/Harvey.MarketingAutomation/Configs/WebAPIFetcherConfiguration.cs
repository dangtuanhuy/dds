﻿using System;

namespace Harvey.MarketingAutomation.Configs
{
    public class WebAPIFetcherConfiguration : ConfigBase
    {
        public DateTime? LastSync { get; set; }
    }
}
