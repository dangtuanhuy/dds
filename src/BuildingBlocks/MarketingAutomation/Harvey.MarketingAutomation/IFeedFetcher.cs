﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Harvey.MarketingAutomation
{
    public interface IFeedFetcher<TModel>
    {
        Task<IEnumerable<TModel>> FetchAsync();
    }

    public interface IFeedFetcher<TModel, TConfig> : IFeedFetcher<TModel>
    {
        TConfig Configuration { get; }

        void SetConfiguration(TConfig config);

    }
}
