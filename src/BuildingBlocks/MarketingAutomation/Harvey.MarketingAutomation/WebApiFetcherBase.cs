﻿using Harvey.Exception;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Harvey.MarketingAutomation
{

    public class WebApiFetcherConfiguration
    {
        public Func<DateTime?> LastSyncResolver { get; set; }
        public TimeSpan Timeout { get; set; } = TimeSpan.FromMinutes(5);
    }

    public class WebApiFetcherBase<TModel> : IFeedFetcher<TModel, WebApiFetcherConfiguration>
    {
        private string _endPoint;
        private string _countEndPoint;
        public WebApiFetcherBase(string endPoint)
        {
            _endPoint = endPoint;
            //_client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
        }

        public WebApiFetcherBase(string endPoint, string countEndPoint) : this(endPoint)
        {
            _countEndPoint = countEndPoint;
        }

        public WebApiFetcherConfiguration Configuration { get; private set; }

        public virtual async Task<IEnumerable<TModel>> FetchAsync()
        {

            var result = await ApplyConfiguration();
            var response = await result.Item2.GetAsync(result.Item1);
            if (response.IsSuccessStatusCode)
            {
                return await ParseResponse<TModel>(response);
            }
            throw new HttpClientException(response.StatusCode, $"Cannot get data from endpoint {_endPoint}");

        }

        public void SetConfiguration(WebApiFetcherConfiguration config)
        {
            Configuration = config;
        }

        private async Task<Tuple<string, HttpClient>> ApplyConfiguration()
        {
            var configuredEndPoint = string.Empty;
            var configuredCountEndPoint = string.Empty;
            var client = CreateHttpClient();

            if (Configuration != null && Configuration.LastSyncResolver != null)
            {
                var lastSync = Configuration.LastSyncResolver() ?? null;
                configuredEndPoint = $"{_endPoint}&lastSync={lastSync}";
                if (!string.IsNullOrEmpty(_countEndPoint))
                {
                    configuredCountEndPoint = $"{_countEndPoint}&lastSync={lastSync}";
                }
            }
            else
            {
                configuredEndPoint = _endPoint;
                configuredCountEndPoint = _countEndPoint;
            }
            if (!string.IsNullOrEmpty(configuredCountEndPoint))
            {

                var countClient = CreateHttpClient();
                var countResponseResult = await countClient.GetAsync(configuredCountEndPoint);

                if (countResponseResult.IsSuccessStatusCode)
                {
                    var jsonString = await countResponseResult.Content.ReadAsStringAsync();
                    var count = int.Parse(jsonString);
                    var extraTimeout = ((count / 1000) + count % 1000 > 0 ? 1 : 0);
                    client.Timeout = Configuration.Timeout * (extraTimeout == 0 ? 1 : extraTimeout);
                }
            }
            return await Task.FromResult(new Tuple<string, HttpClient>(configuredEndPoint, client));
        }

        private async Task<IEnumerable<T>> ParseResponse<T>(HttpResponseMessage response)
        {
            var jsonString = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(jsonString))
            {
                throw new HttpClientException(response.StatusCode, $"Unexpected Json result. Result was null");
            }

            return JsonConvert.DeserializeObject<IEnumerable<T>>(jsonString);

        }

        private HttpClient CreateHttpClient()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}
