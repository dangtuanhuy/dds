﻿namespace Harvey.MarketingAutomation.Enums
{
    public enum SyncType
    {
        Product = 1,
        Variant = 2,
        Category = 3,
        FieldValue = 4,
        Price = 5,
        Stock = 6,
        Promotion = 7
    }
}
