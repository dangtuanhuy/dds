﻿namespace Harvey.MarketingAutomation.Enums
{
    public enum FeedType
    {
        Product = 1,
        Variant = 2,
        Category = 3,
        Price = 4,
        StockType = 5,
        TransactionType = 6,
        Channel = 7,
        Store = 8,
        ChannelStoreAssignment = 9,
        Location = 10,
        BarCode = 11,
        PaymentMethod = 12
    }
}
