﻿using Harvey.EventBus;
using System;

namespace Harvey.MarketingAutomation.Events
{
    public class ConnectorIntergrationEvent : EventBase
    {
        public Guid ConnectorId { get; set; }
        public string Name { get; set; }

        public ConnectorIntergrationEvent()
        {
        }

        public ConnectorIntergrationEvent(string aggregateId) : base(aggregateId)
        {
        }
    }
}
