﻿using Harvey.Job;
using Harvey.MarketingAutomation.Connectors;
using Harvey.MarketingAutomation.Enums;
using System;

namespace Harvey.MarketingAutomation
{
    public class ConnectorBuilder
    {
        private readonly IJobManager _jobManager;
        private readonly ConnectorInfo _connector;
        private readonly IServiceProvider _serviceProvider;
        public ConnectorBuilder(IServiceProvider serviceProvider, IJobManager jobManager, ConnectorInfo connector)
        {
            _serviceProvider = serviceProvider;
            _connector = connector;
            _jobManager = jobManager;
        }

        public ConnectorBuilder AddProductSyncService(Action<SyncServiceBuilder> registration)
        {
            registration(new SyncServiceBuilder(SyncType.Product, _serviceProvider, _connector));
            return this;
        }

        public ConnectorBuilder AddProductFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration, bool rebuildOnExecution = false)
            where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.Product, _serviceProvider, _connector);
            registration(buider);
            if (rebuildOnExecution)
            {
                buider.Feed.Build = registration;
            }
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddVariantSyncService(Action<SyncServiceBuilder> registration)
        {
            registration(new SyncServiceBuilder(SyncType.Variant, _serviceProvider, _connector));
            return this;
        }

        public ConnectorBuilder AddVariantFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration, bool rebuildOnExecution = false, FeedType type = FeedType.Variant)
          where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(type, _serviceProvider, _connector);
            registration(buider);
            if (rebuildOnExecution)
            {
                buider.Feed.Build = registration;
            }
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddCategorySyncService(Action<SyncServiceBuilder> registration)
        {
            registration(new SyncServiceBuilder(SyncType.Category, _serviceProvider, _connector));
            return this;
        }

        public ConnectorBuilder AddCategoryFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration, bool rebuildOnExecution = false)
          where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.Category, _serviceProvider, _connector);
            registration(buider);
            if (rebuildOnExecution)
            {
                buider.Feed.Build = registration;
            }
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddPriceFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration)
          where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.Price, _serviceProvider, _connector);
            registration(buider);
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddBarCodeFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration)
          where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.BarCode, _serviceProvider, _connector);
            registration(buider);
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddPaymentMethodFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration)
          where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.PaymentMethod, _serviceProvider, _connector);
            registration(buider);
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddFieldValueSyncService(Action<SyncServiceBuilder> registration)
        {
            registration(new SyncServiceBuilder(SyncType.FieldValue, _serviceProvider, _connector));
            return this;
        }

        public ConnectorBuilder AddPriceSyncService(Action<SyncServiceBuilder> registration)
        {
            registration(new SyncServiceBuilder(SyncType.Price, _serviceProvider, _connector));
            return this;
        }

        public ConnectorBuilder AddStockTypeFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration)
          where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.StockType, _serviceProvider, _connector);
            registration(buider);
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddTransactionTypeFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration)
          where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.TransactionType, _serviceProvider, _connector);
            registration(buider);
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddChannelFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration, bool rebuildOnExecution = false)
            where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.Channel, _serviceProvider, _connector);
            registration(buider);
            if (rebuildOnExecution)
            {
                buider.Feed.Build = registration;
            }
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddStoreFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration, bool rebuildOnExecution = false)
            where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.Store, _serviceProvider, _connector);
            registration(buider);
            if (rebuildOnExecution)
            {
                buider.Feed.Build = registration;
            }
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddChannelStoreAssignmentFeedService<TSource, TTarget>(Action<FeedServiceBuilder<TSource, TTarget>> registration, bool rebuildOnExecution = false)
            where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(FeedType.ChannelStoreAssignment, _serviceProvider, _connector);
            registration(buider);
            if (rebuildOnExecution)
            {
                buider.Feed.Build = registration;
            }
            _jobManager.RegisterRecurringJob<FeedWorker>(buider.Feed.CorrelationId, buider.Feed.Name, buider.Feed.Execute, buider.Feed.DueTime, buider.Feed.Interval);
            return this;
        }

        public ConnectorBuilder AddChannelStockSyncService(Action<SyncServiceBuilder> registration)
        {
            registration(new SyncServiceBuilder(SyncType.Stock, _serviceProvider, _connector));
            return this;
        }

        public ConnectorInfo GetConnector()
        {
            return _connector;
        }

        public ConnectorBuilder DeleteFeedService<TSource, TTarget>(FeedType feedType)
            where TTarget : FeedItemBase
        {
            var buider = new FeedServiceBuilder<TSource, TTarget>(feedType, _serviceProvider, _connector);

            _jobManager.RemoveRecurringJob(buider.Feed.Name);
            return this;
        }
    }
}
