﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Harvey.Exception.Extensions;
using Microsoft.Extensions.Logging;

namespace Harvey.MarketingAutomation
{
    public class Feed<TSource, TTarget> : FeedBase
        where TTarget : FeedItemBase
    {
        private ILogger<Feed<TSource, TTarget>> _logger;
        protected IFeedFetcher<TSource> _fetcher;
        protected IFeedFilter<TSource> _filter;
        protected IFeedConverter<TSource, TTarget> _converter;
        protected IFeedSerializer<TTarget> _serializer;
        private bool _isRunning = false;
        private static readonly object _syncLock = new object();
        private FeedServiceBuilder<TSource, TTarget> _builder;
        public Action<FeedServiceBuilder<TSource, TTarget>> Build;
        private int _numberOfWaitingTimeToResetFeed = 10;
        public Feed(Guid correlationId, string name, FeedServiceBuilder<TSource, TTarget> builder = null) : base(correlationId, name)
        {
            _builder = builder;
        }

        public void SetFetcher(IFeedFetcher<TSource> fetcher)
        {
            _fetcher = fetcher;
        }

        public void SetFilter(IFeedFilter<TSource> filter)
        {
            _filter = filter;
        }

        public void SetConverter(IFeedConverter<TSource, TTarget> converter)
        {
            _converter = converter;
        }

        public void SetSerializer(IFeedSerializer<TTarget> serializer)
        {
            _serializer = serializer;
        }

        public void SetLogger(ILogger<Feed<TSource, TTarget>> logger)
        {
            _logger = logger;
        }

        public void SetScheduler(TimeSpan dueTime, TimeSpan interval)
        {
            DueTime = dueTime;
            Interval = interval;
        }


        public override void Execute()
        {
            var transaction = Guid.NewGuid();
            _logger.LogInformation($"[{DateTime.UtcNow}] Starting feed [{transaction}]: {this.GetType().FullName}_{CorrelationId} - Active Runner: {_isRunning}");

            if (_isRunning)
            {
                _numberOfWaitingTimeToResetFeed--;
                if (_numberOfWaitingTimeToResetFeed == 0)
                {
                    _isRunning = false;
                    _logger.LogInformation($"[{DateTime.UtcNow}] Reset & End feed [{transaction}]: {this.GetType().FullName}_{CorrelationId}");

                }
                else
                {
                    _logger.LogInformation($"[{DateTime.UtcNow}] End feed [{transaction}]: {this.GetType().FullName}_{CorrelationId} - Feed is running - Count down to reset: {_numberOfWaitingTimeToResetFeed}");
                }
                return;
            }
            Task.Factory.StartNew(async () =>
           {
               _isRunning = true;
               _numberOfWaitingTimeToResetFeed = 10;
               if (Build != null && _builder != null)
               {
                   Build(_builder);
               }
               IEnumerable<TSource> source = null;
               try
               {
                   _logger.LogInformation($"[{DateTime.UtcNow}] [{transaction}] Fetching...: {this.GetType().FullName}_{CorrelationId}");
                   var result = await _fetcher.FetchAsync();
                   if (!result.Any())
                   {
                       _logger.LogInformation($"[{DateTime.UtcNow}] End feed [{transaction}]: {this.GetType().FullName}_{CorrelationId} - Fetch return no results.");
                       return;
                   }
                   _logger.LogInformation($"[{DateTime.UtcNow}] [{transaction}] Filtering...: {this.GetType().FullName}_{CorrelationId} {Environment.NewLine} Data: {result.Count()}");
                   source = _filter.Filter(CorrelationId, result);
                   if (!source.Any())
                   {
                       _logger.LogInformation($"[{DateTime.UtcNow}] End feed [{transaction}]: {this.GetType().FullName}_{CorrelationId} - Filter return no results.");
                       return;
                   }
                   _logger.LogInformation($"[{DateTime.UtcNow}] [{transaction}] Start to converting...: {this.GetType().FullName}_{CorrelationId} {Environment.NewLine} Data: {result.Count()}");
                   if (_converter.CanConvert(source.GetType()))
                   {
                       _logger.LogInformation($"[{DateTime.UtcNow}] [{transaction}] Converting...: {this.GetType().FullName}_{CorrelationId}");
                       var data = _converter.Convert(source).ToList();
                       foreach (var item in data)
                       {
                           item.CorrelationId = CorrelationId;
                       }
                       _logger.LogInformation($"[{DateTime.UtcNow}] [{transaction}] Serializing...: {this.GetType().FullName}_{CorrelationId} {Environment.NewLine} Data: {result.Count()}");
                       await _serializer.SerializeAsync(data);
                       _logger.LogInformation($"[{DateTime.UtcNow}] End feed [{transaction}]: {this.GetType().FullName}_{CorrelationId} - Successful.");
                   }
                   else
                   {
                       _logger.LogInformation($"[{DateTime.UtcNow}] End feed [{transaction}]: {this.GetType().FullName}_{CorrelationId} - Converter is not supported.");
                   }
               }
               catch (System.Exception ex)
               {
                   _logger.LogInformation($"[{DateTime.UtcNow}] End feed [{transaction}]: {this.GetType().FullName}_{CorrelationId} - Exception {ex.GetTraceLog()}.");
                   _logger.LogError(ex.GetTraceLog());
               }
               finally
               {
                   _isRunning = false;
                   _numberOfWaitingTimeToResetFeed = 10;
                   _logger.LogInformation($"[{DateTime.UtcNow}] End feed [{transaction}]: {this.GetType().FullName}_{CorrelationId} - Active Runner: {_isRunning}");
               }

           }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default)
           .Wait();
        }
    }
}
