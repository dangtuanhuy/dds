﻿using System;
using System.Collections.Generic;

namespace Harvey.MarketingAutomation
{
    public interface IFeedFilter<TModel>
    {
        IEnumerable<TModel> Filter(Guid CorrelationId, IEnumerable<TModel> source);
    }
}
