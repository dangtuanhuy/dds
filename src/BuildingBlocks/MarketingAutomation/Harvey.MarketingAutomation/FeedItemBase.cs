﻿using System;
using Harvey.Domain;

namespace Harvey.MarketingAutomation
{
    public class FeedItemBase : EntityBase
    {
        public Guid CorrelationId { get; set; }
    }
}
