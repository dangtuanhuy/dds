﻿namespace Harvey.MarketingAutomation.Connectors
{
    public interface IConectorInstaller
    {
        void Install(ApplicationBuilder appBuilder);
    }
}
