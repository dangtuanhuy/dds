﻿namespace Harvey.MarketingAutomation.Connectors
{
    public interface IConnectorSetup
    {
        void Execute();
    }
}
