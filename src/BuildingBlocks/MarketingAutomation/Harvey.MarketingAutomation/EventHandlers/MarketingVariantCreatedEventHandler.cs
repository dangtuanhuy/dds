﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Variants;
using Harvey.MarketingAutomation.Connectors;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.MarketingAutomation.EventHandlers
{
    public class MarketingVariantCreatedEventHandler : EventHandlerBase<VariantCreatedEvent>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEventBus _eventBus;
        public MarketingVariantCreatedEventHandler(ConnectorInfoCollection connectorInfos,
            IEventBus eventBus,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<VariantCreatedEvent>> logger) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _eventBus = eventBus;
        }
        protected override async Task ExecuteAsync(VariantCreatedEvent @event)
        {
            foreach (var item in _connectorInfos)
            {
                await _eventBus.PublishAsync(new MarketingAutomationEvent<VariantCreatedEvent>(@event)
                {
                    CorrelationId = item.CorrelationId
                });
            }
        }
    }
}
