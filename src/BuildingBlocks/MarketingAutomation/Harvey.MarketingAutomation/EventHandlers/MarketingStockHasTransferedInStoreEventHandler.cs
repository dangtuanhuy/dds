﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.StockLevels;
using Harvey.MarketingAutomation.Connectors;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.MarketingAutomation.EventHandlers
{
    public class MarketingStockHasTransferedInStoreEventHandler : EventHandlerBase<StockHasTransferedInStoreEvent>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEventBus _eventBus;
        public MarketingStockHasTransferedInStoreEventHandler(
            IEventBus eventBus,
            ConnectorInfoCollection connectorInfos,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<StockHasTransferedInStoreEvent>> logger) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _eventBus = eventBus;
        }

        protected async override Task ExecuteAsync(StockHasTransferedInStoreEvent @event)
        {
            foreach (var item in _connectorInfos)
            {
                await _eventBus.PublishAsync(new MarketingAutomationEvent<StockHasTransferedInStoreEvent>(@event)
                {
                    CorrelationId = item.CorrelationId
                });
            }
        }
    }
}