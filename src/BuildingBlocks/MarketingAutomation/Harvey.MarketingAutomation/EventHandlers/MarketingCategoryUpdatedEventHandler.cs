﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Categories;
using Harvey.MarketingAutomation.Connectors;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.MarketingAutomation.EventHandlers
{
    public class MarketingCategoryUpdatedEventHandler : EventHandlerBase<CategoryUpdatedEvent>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEventBus _eventBus;
        public MarketingCategoryUpdatedEventHandler(
            ConnectorInfoCollection connectorInfos,
            IEventBus eventBus,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<CategoryUpdatedEvent>> logger) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _eventBus = eventBus;
        }
        protected override async Task ExecuteAsync(CategoryUpdatedEvent @event)
        {
            foreach (var item in _connectorInfos)
            {
                await _eventBus.PublishAsync(new MarketingAutomationEvent<CategoryUpdatedEvent>(@event)
                {
                    CorrelationId = item.CorrelationId
                });
            }
        }
    }
}
