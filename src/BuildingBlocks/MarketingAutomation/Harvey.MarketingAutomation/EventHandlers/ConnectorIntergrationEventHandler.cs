﻿using System.Threading.Tasks;
using Harvey.Domain;
using Harvey.EventBus;
using Harvey.MarketingAutomation.Events;
using Microsoft.Extensions.Logging;

namespace Harvey.MarketingAutomation.EventHandlers
{
    public class ConnectorIntergrationEventHandler : EventHandlerBase<ConnectorIntergrationEvent>
    {
        private readonly ApplicationBuilder _applicationBuilder;
        public ConnectorIntergrationEventHandler(
     ApplicationBuilder applicationBuilder,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<ConnectorIntergrationEvent>> logger
            ) : base(repository, logger)
        {
            _applicationBuilder = applicationBuilder;
        }

        protected async override Task ExecuteAsync(ConnectorIntergrationEvent @event)
        {
            await Task.Yield();
            _applicationBuilder.AddConnector(@event.ConnectorId, @event.Name);
        }
    }
}
