﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.Products;
using Harvey.MarketingAutomation.Connectors;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Harvey.MarketingAutomation.EventHandlers
{
    public sealed class MarketingProductDeletedEventHandler : EventHandlerBase<ProductDeletedEvent>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEventBus _eventBus;
        public MarketingProductDeletedEventHandler(
            ConnectorInfoCollection connectorInfos,
            IEventBus eventBus,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<ProductDeletedEvent>> logger) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _eventBus = eventBus;
        }

        protected override async Task ExecuteAsync(ProductDeletedEvent @event)
        {
            foreach (var item in _connectorInfos)
            {
                await _eventBus.PublishAsync(new MarketingAutomationEvent<ProductDeletedEvent>(@event)
                {
                    CorrelationId = item.CorrelationId
                });
            }

        }
    }
}
