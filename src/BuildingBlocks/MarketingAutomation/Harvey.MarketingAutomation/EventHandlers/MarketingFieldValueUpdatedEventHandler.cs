﻿using Harvey.Domain;
using Harvey.EventBus;
using Harvey.EventBus.Abstractions;
using Harvey.EventBus.Events.FieldValues;
using Harvey.MarketingAutomation.Connectors;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Harvey.MarketingAutomation.EventHandlers
{
    public class MarketingFieldValueUpdatedEventHandler : EventHandlerBase<FieldValueUpdatedEvent>
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IEventBus _eventBus;
        public MarketingFieldValueUpdatedEventHandler(
            ConnectorInfoCollection connectorInfos,
            IEventBus eventBus,
            IRepository<IdentifiedEvent> repository,
            ILogger<EventHandlerBase<FieldValueUpdatedEvent>> logger) : base(repository, logger)
        {
            _connectorInfos = connectorInfos;
            _eventBus = eventBus;
        }
        protected override async Task ExecuteAsync(FieldValueUpdatedEvent @event)
        {
            foreach (var item in _connectorInfos)
            {
                await _eventBus.PublishAsync(new MarketingAutomationEvent<FieldValueUpdatedEvent>(@event)
                {
                    CorrelationId = item.CorrelationId
                });
            }
        }
    }
}
