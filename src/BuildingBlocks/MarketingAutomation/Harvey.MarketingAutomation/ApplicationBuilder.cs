﻿using System;
using System.Linq;
using Harvey.EventBus.Abstractions;
using Harvey.Job;
using Harvey.MarketingAutomation.Connectors;
using Harvey.MarketingAutomation.Events;

namespace Harvey.MarketingAutomation
{
    public class ApplicationBuilder
    {
        private readonly ConnectorInfoCollection _connectorInfos;
        private readonly IServiceProvider _serviceProvider;
        private readonly IJobManager _jobManager;
        private readonly IEventBus _eventBus;
        public ApplicationBuilder(
            IEventBus eventBus,
            IServiceProvider serviceProvider,
            IJobManager jobManager,
            ConnectorInfoCollection connectorInfos)
        {
            _eventBus = eventBus;
            _serviceProvider = serviceProvider;
            _connectorInfos = connectorInfos;
            _jobManager = jobManager;
        }

        public ApplicationBuilder AddConnector(Guid id, string name, IEventBus eventBus = null, Action<ConnectorBuilder> registration = null)
        {
            var connector = _connectorInfos.FirstOrDefault(x => x.Name == name);
            if (connector == null)
            {
                connector = new ConnectorInfo(id, name);
                _connectorInfos.Add(connector);
            }
            if (registration != null)
            {
                var connectorBuilder = new ConnectorBuilder(_serviceProvider, _jobManager, connector);
                registration(connectorBuilder);
            }
            return this;
        }

        public ConnectorInfoCollection GetConnectors()
        {
            return _connectorInfos;
        }

        public void Merge()
        {
            foreach (var item in _connectorInfos)
            {
                _eventBus.PublishAsync(new ConnectorIntergrationEvent()
                {
                    ConnectorId = item.CorrelationId,
                    Name = item.Name
                });
            }
        }
    }
}
