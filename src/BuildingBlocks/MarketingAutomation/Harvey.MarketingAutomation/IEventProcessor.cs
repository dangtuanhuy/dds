﻿using Harvey.EventBus;

namespace Harvey.MarketingAutomation
{
    public interface IEventProcessor
    {
        bool CanProcess(EventBase @event);
        void Process();
    }
}
