﻿using Harvey.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Harvey.NextSequence
{
    public class NextSequenceService<TContext> : INextSequenceService<TContext>
        where TContext : DbContext
    {
        readonly TContext _dbContext;
        readonly object _object = new object();

        public NextSequenceService(TContext dbContext)
        {
            _dbContext = dbContext;
        }

        public string Get(string key, int numberOfChars)
        {
            Monitor.Enter(_object);
            try
            {
                var result = string.Empty;
                if (!string.IsNullOrEmpty(key))
                {
                    var item = _dbContext.Set<Domain.NextSequence>().FirstOrDefault(s => s.Key == key);
                    if (item == null)
                    {
                        var entity = new Domain.NextSequence
                        {
                            Id = Guid.NewGuid(),
                            Key = key,
                            Value = GenerateNextSequenceItem(0).ToString().PadLeft(numberOfChars, '0')
                        };
                        _dbContext.Set<Domain.NextSequence>().Add(entity);
                        result = entity.Value;
                    }
                    else
                    {
                        _dbContext.Entry(item).State = EntityState.Modified;
                        item.Value = GenerateNextSequenceItem(Int32.Parse(item.Value)).ToString().PadLeft(numberOfChars, '0');
                        result = item.Value;
                    }
                    _dbContext.SaveChanges();
                }
                return result;
            }
            finally
            {
                Monitor.Exit(_object);
            }
        }

        public Domain.NextSequence GetNextEntity(string key, int numberOfChars)
        {
            Monitor.Enter(_object);
            try
            {
                Domain.NextSequence result = null;
                if (!string.IsNullOrEmpty(key))
                {
                    var item = _dbContext.Set<Domain.NextSequence>().FirstOrDefault(s => s.Key == key);
                    if (item == null)
                    {
                        var entity = new Domain.NextSequence
                        {
                            Id = Guid.NewGuid(),
                            Key = key,
                            Value = GenerateNextSequenceItem(0).ToString().PadLeft(numberOfChars, '0')
                        };
                        _dbContext.Set<Domain.NextSequence>().Add(entity);
                        result = entity;
                    }
                    else
                    {
                        _dbContext.Entry(item).State = EntityState.Modified;
                        item.Value = GenerateNextSequenceItem(Int32.Parse(item.Value)).ToString().PadLeft(numberOfChars, '0');
                        result = item;
                    }
                    _dbContext.SaveChanges();
                }
                return result;
            }
            finally
            {
                Monitor.Exit(_object);
            }
        }

        public void UpdateRange(Dictionary<string, int> values, int numberOfChars)
        {
            var keys = values.Select(x => x.Key);
            var entities = _dbContext.Set<Domain.NextSequence>().Where(x => keys.Contains(x.Key)).ToList();
            foreach (var item in entities)
            {
                item.Value = values[item.Key].ToString().PadLeft(numberOfChars, '0'); ;
            }

            _dbContext.SaveChanges();
        }

        private int GenerateNextSequenceItem(int value)
        {
            return ++value;
        }

        public List<Domain.NextSequence> GetAllCurrentNextSequence()
        {
            Monitor.Enter(_object);
            try
            {
                DateTime dateTimeNow = DateTime.UtcNow;
                var currentTime = dateTimeNow.ToString("MMyy", System.Globalization.CultureInfo.InvariantCulture);
                return _dbContext.Set<Domain.NextSequence>().Where(x => x.Key.Contains(currentTime) || x.Key.Equals("variants")).AsNoTracking().ToList();
            }
            finally
            {
                Monitor.Exit(_object);
            }
        }

        public Domain.NextSequence AddNextSequence(string key, int numberOfChars)
        {
            Monitor.Enter(_object);
            try
            {
                var entity = new Domain.NextSequence
                {
                    Id = Guid.NewGuid(),
                    Key = key,
                    Value = GenerateNextSequenceItem(0).ToString().PadLeft(numberOfChars, '0')
                };
                _dbContext.Set<Domain.NextSequence>().Add(entity);
                _dbContext.SaveChanges();
                return entity;
            }
            finally
            {
                Monitor.Exit(_object);
            }
        }

        public string GetExtentionSaleTransactionFile(string key)
        {
            Monitor.Enter(_object);
            try
            {
                var entity = _dbContext.Set<Domain.NextSequence>().FirstOrDefault(x => x.Key == key);
                if (entity == null)
                {
                    entity = new Domain.NextSequence
                    {
                        Key = key,
                        Value = 0.ToString().PadLeft(3, '0')
                    };
                    _dbContext.Set<Domain.NextSequence>().Add(entity);
                }
                else
                {
                    var updateValue = int.Parse(entity.Value) + 1;
                    if (updateValue > 999)
                    {
                        updateValue = 0;
                    }
                    entity.Value = updateValue.ToString().PadLeft(3, '0');
                    _dbContext.Set<Domain.NextSequence>().Update(entity);
                }

                _dbContext.SaveChanges();
                return entity.Value;
            }
            finally
            {
                Monitor.Exit(_object);
            }
        }
    }
}
