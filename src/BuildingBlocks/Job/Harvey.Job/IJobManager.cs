﻿using System;

namespace Harvey.Job
{
    public interface IJobManager
    {
        void RegisterRecurringJob<T>(Guid correlationId, string name, Action execution, TimeSpan dueTime, TimeSpan interval)
            where T : IWorker;

        void RegisterRecurringJobDaily<T>(Guid correlationId, string name, Action execution, int hour, int minute)
            where T : IWorker;

        void RegisterRecurringJobMonthly<T>(Guid correlationId, string name, Action execution, int day, int hour, int minute)
            where T : IWorker;

        void RemoveRecurringJob(string name);
    }
}
