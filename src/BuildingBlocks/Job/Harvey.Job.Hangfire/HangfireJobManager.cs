﻿using System;
using System.Threading;
using Hangfire;

namespace Harvey.Job.Hangfire
{
    public class HangfireJobManager : IJobManager
    {
        public void RegisterRecurringJob<T>(Guid correlationId, string name, Action execution, TimeSpan dueTime, TimeSpan interval)
            where T : IWorker
        {
            RecurringJob.RemoveIfExists(name);
            Thread.Sleep(dueTime.Seconds);
            RecurringJob.AddOrUpdate<T>(name, (worker) => worker.Execute(correlationId, name), Cron.MinuteInterval(interval.Minutes));
            RecurringJob.Trigger(name);
        }

        public void RegisterRecurringJobDaily<T>(Guid correlationId, string name, Action execution, int hour, int minute)
            where T : IWorker
        {
            RecurringJob.RemoveIfExists(name);
            RecurringJob.AddOrUpdate<T>(name, (worker) => worker.Execute(correlationId, name), Cron.Daily(hour, minute));
        }

        public void RegisterRecurringJobMonthly<T>(Guid correlationId, string name, Action execution, int day, int hour, int minute)
              where T : IWorker
        {
            RecurringJob.RemoveIfExists(name);
            RecurringJob.AddOrUpdate<T>(name, (worker) => worker.Execute(correlationId, name), Cron.Monthly(day, hour, minute));
        }

        public void RemoveRecurringJob(string name)
        {
            RecurringJob.RemoveIfExists(name);
        }
    }
}
