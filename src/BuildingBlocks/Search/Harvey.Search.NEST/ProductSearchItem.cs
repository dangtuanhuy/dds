﻿using Harvey.Search.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Harvey.Search.NEST
{
    class ProductSearchItem : SearchItem, ISearchItem
    {
        public ProductSearchItem()
        {
        }
        public ProductSearchItem(string id) : base(id)
        {
        }
        public string Name { get; set; }
        public string ChineseSearchName { get; set; }
        public string Description { get; set; }
        public string ChineseSearchDescription { get; set; }
        public Guid CategoryId { get; set; }
        public string IndexingValue { get; set; }
        public string ChineseSearchIndexingValue { get; set; }
        public string CategoryName { get; set; }
        public string Code { get; set; }
        public bool IsDelete { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
