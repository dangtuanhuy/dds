﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Harvey.Exception;
using Harvey.Persitance.EF;
using Harvey.Search.Abstractions;
using Nest;
using Newtonsoft.Json;

namespace Harvey.Search.NEST
{
    public class SearchService : ISearchService
    {
        private readonly SearchSettings _searchSettings;
        public SearchService(SearchSettings searchSettings)
        {
            _searchSettings = searchSettings;
        }

        public async Task AddAsync<TSearchItem>(IndexedItem<TSearchItem> searchItem)
            where TSearchItem : SearchItem
        {
            var result = await WithElasticClient(x => x.CreateAsync(searchItem.Item, idx => idx.Index(searchItem.IndexName)));

            if (result.Result == Result.Created)
            {
                return;
            }
            throw new IndexingException($"Cannot add document {JsonConvert.SerializeObject(searchItem)} - Result {result.Result}") { Item = searchItem };

        }

        public async Task UpdateAsync<TSearchItem>(IndexedItem<TSearchItem> searchItem)
            where TSearchItem : SearchItem
        {
            var result = await WithElasticClient(x => x
                                .UpdateAsync(
                                    DocumentPath<TSearchItem>.Id(searchItem.Item.Id),
                                    u => u.Index(searchItem.IndexName)
                                        .DocAsUpsert()
                                        .Doc(searchItem.Item)));
            if (result.Result == Result.Created || result.Result == Result.Updated)
            {
                return;
            }
            throw new IndexingException($"Cannot update document {JsonConvert.SerializeObject(searchItem)} - Result {result.Result}") { Item = searchItem };
        }

        public async Task DeleteAsync<TSearchItem>(IndexedItem<TSearchItem> searchItem)
            where TSearchItem : SearchItem
        {
            var result = await WithElasticClient(x => x.DeleteAsync<TSearchItem>(searchItem.Item, idx => idx.Index(searchItem.IndexName)));
            if (result.Result == Result.Deleted)
            {
                return;
            }
            throw new IndexingException($"Cannot update document {JsonConvert.SerializeObject(searchItem)} - Result {result.Result}") { Item = searchItem };
        }

        public async Task AddAsync<TSearchItem>(List<IndexedItem<TSearchItem>> searchItem) where TSearchItem : SearchItem
        {
            foreach (var item in searchItem)
            {
                await AddAsync(searchItem);
            }
            return;
        }

        public async Task UpdateAsync<TSearchItem>(List<IndexedItem<TSearchItem>> searchItem) where TSearchItem : SearchItem
        {
            foreach (var item in searchItem)
            {
                await UpdateAsync(searchItem);
            }
            return;
        }

        public async Task DeleteAsync<TSearchItem>(List<IndexedItem<TSearchItem>> searchItem) where TSearchItem : SearchItem
        {
            foreach (var item in searchItem)
            {
                await DeleteAsync(searchItem);
            }
            return;
        }

        public async Task<ISearchResults<TSearchItem, TSearchResult>> SearchAsync<TSearchItem, TSearchResult>(ISearchQuery<TSearchItem> query, string sortColumn = "", string sortDirection = "")
            where TSearchItem : SearchItem
            where TSearchResult : SearchResult<TSearchItem>, new()
        {

            Func<SearchDescriptor<TSearchItem>, SearchDescriptor<TSearchItem>> search = (predicate) =>
            {
                if (query.Page == 0 && query.NumberItemsPerPage == 0)
                {
                }
                else
                {

                    predicate = predicate
                                .Sort(s => s
                                    .Descending(query.SortBy.FirstOrDefault())
                                )
                                .Size(query.NumberItemsPerPage)
                                .Skip(query.NumberItemsPerPage * (query.Page - 1));
                }

                predicate = predicate.Index(query.IndexName)
                            .Query(q => q
                                .Bool(a => a
                                .MustNot(GetFilters(query))
                                .Should(GetMatches(query))))
                                ;

                return predicate;
            };

            var result = await WithElasticClient(
                          x => x.SearchAsync<TSearchItem>(s => search(s)));


            var totalItems = await WithElasticClient(x => x.CountAsync<TSearchItem>(c => c.Index(query.IndexName)
                                .Query(q => q
                                    .Bool(a => a
                                    .MustNot(GetFilters(query))
                                    .Should(GetMatches(query))))));

            return new SearchResults<TSearchItem, TSearchResult>()
            {
                Results = result.Documents.Select(x =>
                {
                    var item = new TSearchResult();
                    item.SetItem(x);
                    return item;
                }),
                TotalItems = totalItems.Count
            };
        }

        public async Task<ISearchResults<TSearchItem, TSearchResult>> SearchOverLimitAsync<TSearchItem, TSearchResult>(ISearchQuery<TSearchItem> query, string sortColumn = "", string sortDirection = "" )
            where TSearchItem : SearchItem
            where TSearchResult : SearchResult<TSearchItem>, new()
        {
            var allNumberItems = query.NumberItemsPerPage * (query.Page - 1);
            const int maxNumberItemsElasticsearchCanGet = 10000;

            Func<SearchDescriptor<TSearchItem>, SearchDescriptor<TSearchItem>> search = (predicate) =>
            {
                if (query.Page == 0 && query.NumberItemsPerPage == 0)
                {
                }
                else
                {
                    //if (!string.IsNullOrEmpty(sortColumn))
                    //{
                    //    if (string.IsNullOrEmpty(sortDirection))
                    //    {
                    //        predicate.Sort(s => s.Ascending(x => RefectionExtensions.GetPropertyValue(x, sortColumn)));
                    //    }
                    //    else
                    //        predicate.Sort(s => s.Descending(x => RefectionExtensions.GetPropertyValue(x, sortColumn)));
                    //}
                    predicate = predicate
                                .Sort(s => s
                                    .Descending(query.SortBy.FirstOrDefault())
                                )
                                .Scroll("60s")
                                .Size(maxNumberItemsElasticsearchCanGet)
                                .Take(maxNumberItemsElasticsearchCanGet);
                }

                predicate = predicate.Index(query.IndexName)
                            .Query(q => q
                                .Bool(a => a
                                .MustNot(GetFilters(query))
                                .Should(GetMatches(query))))
                                ;

                return predicate;
            };

            var result = await WithElasticClient(
                          x => x.SearchAsync<TSearchItem>(s => search(s)));

            var fromElement = 0;
            var scrollId = result.ScrollId;
            var client = new ElasticClient(new Uri(_searchSettings.Url));
            var lastResults = new List<TSearchItem>();

            while (true)
            {
                var scrollResults = client.Scroll<TSearchItem>("12s", scrollId);
                if (scrollResults.IsValid)
                {
                    scrollId = scrollResults.ScrollId;
                }

                fromElement += maxNumberItemsElasticsearchCanGet;
                if (allNumberItems < fromElement)
                {
                    break;
                }
                if (allNumberItems - maxNumberItemsElasticsearchCanGet < fromElement)
                {
                    lastResults.AddRange(scrollResults.Documents);
                    var numberSkipItems = ((allNumberItems - (allNumberItems / maxNumberItemsElasticsearchCanGet) * maxNumberItemsElasticsearchCanGet)
                                            / query.NumberItemsPerPage) * query.NumberItemsPerPage;

                    //if (!string.IsNullOrEmpty(sortColumn))
                    //{
                    //    if (string.IsNullOrEmpty(sortDirection))
                    //    {
                    //        lastResults.OrderBy(x => RefectionExtensions.GetPropertyValue(x, sortColumn));
                    //    }
                    //    else
                    //        lastResults.OrderByDescending(x => RefectionExtensions.GetPropertyValue(x, sortColumn));
                    //}
                    lastResults = lastResults.Skip(numberSkipItems).Take(query.NumberItemsPerPage).ToList();

                   
                }
            }

            var totalItems = await WithElasticClient(x => x.CountAsync<TSearchItem>(c => c.Index(query.IndexName)
                                .Query(q => q
                                    .Bool(a => a
                                    .MustNot(GetFilters(query))
                                    .Should(GetMatches(query))))));

            return new SearchResults<TSearchItem, TSearchResult>()
            {
                Results = lastResults.Select(x =>
                {
                    var item = new TSearchResult();
                    item.SetItem(x);
                    return item;
                }),
                TotalItems = totalItems.Count
            };
        }


        public async Task<ISearchResults<TSearchItem, TSearchResult>> FuzzySearchAsync<TSearchItem, TSearchResult>(ISearchQuery<TSearchItem> query)
            where TSearchItem : SearchItem
            where TSearchResult : SearchResult<TSearchItem>, new()
        {
            var fuzzyField = query.Matches.FirstOrDefault();
            //TODO need to support multiple field
            var result = await WithElasticClient(
                x => x.SearchAsync<TSearchItem>(s => s
                      .Size(query.NumberItemsPerPage)
                      .Skip(query.NumberItemsPerPage * (query.Page - 1))
                      .Index(query.IndexName)
                      .Query(q => q
                              .Fuzzy(c => c
                                .Name($"fuzzyField.ToString()_{query}")
                                .Field(fuzzyField)
                                .Fuzziness(Fuzziness.Auto)
                                .Value(query.QueryText)
                                .MaxExpansions(100)
                                .PrefixLength(0)
                                .Transpositions()
                                ))));

            var totalItems = await WithElasticClient(x => x.CountAsync<TSearchItem>(c => c.Index(query.IndexName)));

            return new SearchResults<TSearchItem, TSearchResult>()
            {
                Results = result.Documents.Select(x =>
                {
                    var item = new TSearchResult();
                    item.SetItem(x);
                    return item;
                }),
                TotalItems = totalItems.Count
            };
        }

        private List<Func<QueryContainerDescriptor<TSearchItem>, QueryContainer>> GetMatches<TSearchItem>(ISearchQuery<TSearchItem> query)
            where TSearchItem : SearchItem
        {
            var result = new List<Func<QueryContainerDescriptor<TSearchItem>, QueryContainer>>();
            query.Matches.ForEach(match =>
            {
                result.Add((mu) =>
                {
                    mu.MatchPhrase(m => m
                        .Field(match)
                        .Query(query.QueryText)
                        //.Analyzer("ngram_analyzer")
                        );
                    return mu;
                });
            });

            return result;
        }
        private List<Func<QueryContainerDescriptor<TSearchItem>, QueryContainer>> GetFilters<TSearchItem>(ISearchQuery<TSearchItem> query)
           where TSearchItem : SearchItem
        {
            var result = new List<Func<QueryContainerDescriptor<TSearchItem>, QueryContainer>>();
            query.Filters.ForEach(item =>
            {
                result.Add((mu) =>
                {
                    mu.Term(item, true);
                    return mu;
                });
            });
            return result;
        }

        private List<Func<QueryContainerDescriptor<TSearchItem>, QueryContainer>> GetFyzzyMatches<TSearchItem>(ISearchQuery<TSearchItem> query)
                 where TSearchItem : SearchItem
        {
            var result = new List<Func<QueryContainerDescriptor<TSearchItem>, QueryContainer>>();
            query.Matches.ForEach(match =>
            {
                result.Add((mu) =>
                {
                    mu.Match(m => m.Field(match).Fuzziness(Fuzziness.Auto).Query(query.QueryText));
                    return mu;
                });
            });

            return result;
        }


        public void WithConnectionStrings(Action<ConnectionSettings> action)
        {
            var connectionSettings = new ConnectionSettings(new Uri(_searchSettings.Url));
            action(connectionSettings);
        }

        public Task<T> WithElasticClient<T>(Func<ElasticClient, Task<T>> func)
        {
            var client = new ElasticClient(new Uri(_searchSettings.Url));
            return func(client);
        }

        public async Task DeleteByQueryAsync<T>(string indexName) where T : SearchItem
        {
            var client = new ElasticClient(new Uri(_searchSettings.Url));
            await client.DeleteByQueryAsync<T>(idx => idx.Index(indexName).MatchAll());
            return;
        }

        public async Task<bool> InsertDocumentsAsync<T>(List<IndexedItem<T>> items) where T : SearchItem
        {
            if (!items.Any())
            {
                return true;
            }
            var client = new ElasticClient(new Uri(_searchSettings.Url));

            var indexName = items.FirstOrDefault().IndexName;
            var indexExists = await client.IndexExistsAsync(indexName);
            if (indexExists.Exists)
            {
                var deleteIndex = await client.DeleteIndexAsync(indexName);
                if (deleteIndex.IsValid)
                {
                    return await BulkAsyncDocument(indexName, client, items);
                }
            }

            return await BulkAsyncDocument(indexName, client, items);
        }

        public async Task<bool> InsertDocumentsWithoutDeleteIndexAsync<T>(List<IndexedItem<T>> items) where T : SearchItem
        {
            if (!items.Any())
            {
                return true;
            }
            var client = new ElasticClient(new Uri(_searchSettings.Url));
            var indexName = items.FirstOrDefault().IndexName;

            return await BulkAsyncDocument(indexName, client, items);
        }

        private async Task<bool> BulkAsyncDocument<T>(string indexName, ElasticClient client, List<IndexedItem<T>> items) where T : SearchItem
        {
            const string ProductIndexName = "product_index";
            var indexExists = await client.IndexExistsAsync(indexName);

            if (!indexExists.Exists)
            {
                if (indexName == ProductIndexName)
                {
                    await CreateProductIndex(indexName, client, items);
                }
                else
                {
                    await CreateStockIndex(indexName, client, items);
                }
            }
            else
            {
                var descriptor = new BulkDescriptor();
                foreach (var item in items)
                {
                    descriptor.Index<T>(op => op.Index(indexName).Document(item.Item));
                }
                var res = await client.BulkAsync(descriptor);
                if (res.Errors)
                {
                    return false;
                }
            }
            return true;
        }

        private async Task<bool> CreateProductIndex<T>(string indexName, ElasticClient client, List<IndexedItem<T>> items) where T : SearchItem
        {
            var createIndexResponse = client.CreateIndex(indexName, c => c
                    .Settings(s => s
                        .Analysis(a => a
                            .CharFilters(cf => cf
                                .Mapping("stop_words", mca => mca
                                    .Mappings(new[]
                                        {
                                            "@ => ",
                                            "& => ",
                                            "# => ",
                                            "$ => ",
                                            "* => ",
                                            "( => ",
                                            ") => ",
                                            "! => ",
                                            "_ => ",
                                            ": => ",
                                            "; => ",
                                            "| => ",
                                            "[ => ",
                                            "] => ",
                                            "' => ",
                                            "- => ",
                                        }
                                    )
                                )
                            )
                            .TokenFilters(tz => tz
                                .NGram("ngram_filter", td => td
                                    .MinGram(3)
                                    .MaxGram(20)
                                )
                            )
                            .Analyzers(an => an
                                .Custom("ngram_analyzer", cc => cc
                                    .CharFilters("html_strip", "stop_words")
                                    .Tokenizer("standard")
                                    .Filters("lowercase", "ngram_filter")
                                )
                                .Custom("smartcn_analyzer", cc => cc
                                    .CharFilters("html_strip", "stop_words")
                                    .Tokenizer("smartcn_tokenizer")
                                )
                              )
                            )
                        )
                      .Mappings(m => m
                        .Map<ProductSearchItem>(mm => mm
                            .Properties(p => p
                                .Text(t => t
                                    .Name("name")
                                    .Analyzer("ngram_analyzer")
                                )
                                .Text(t => t
                                    .Name("description")
                                    .Analyzer("ngram_analyzer")
                                )
                                .Text(t => t
                                    .Name("indexingValue")
                                    .Analyzer("ngram_analyzer")
                                )
                                .Text(t => t
                                    .Name("chineseSearchName")
                                    .Analyzer("smartcn_analyzer")
                                )
                                .Text(t => t
                                    .Name("chineseSearchDescription")
                                    .Analyzer("smartcn_analyzer")
                                )
                                .Text(t => t
                                    .Name("chineseSearchIndexingValue")
                                    .Analyzer("smartcn_analyzer")
                                )
                            )
                        )
                    ));

            if (createIndexResponse.IsValid)
            {
                var descriptor = new BulkDescriptor();
                foreach (var item in items)
                {
                    descriptor.Index<T>(op => op.Index(indexName).Document(item.Item));
                }
                var res = await client.BulkAsync(descriptor);
                if (res.Errors)
                {
                    return false;
                }
            }
            return true;
        }

        private async Task<bool> CreateStockIndex<T>(string indexName, ElasticClient client, List<IndexedItem<T>> items) where T : SearchItem
        {
            var createIndexResponse = client.CreateIndex(indexName, c => c
                    .Settings(s => s
                        .Analysis(a => a
                            .TokenFilters(tz => tz
                                .NGram("ngram_filter", td => td
                                    .MinGram(3)
                                    .MaxGram(20)
                                )
                            )
                            .Analyzers(an => an
                                .Custom("ngram_analyzer", cc => cc
                                    .Tokenizer("standard")
                                    .Filters("lowercase", "ngram_filter")
                                )
                              )
                            )
                        )
                    );

            if (createIndexResponse.IsValid)
            {
                var descriptor = new BulkDescriptor();
                foreach (var item in items)
                {
                    descriptor.Index<T>(op => op.Index(indexName).Document(item.Item));
                }
                var res = await client.BulkAsync(descriptor);
                if (res.Errors)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
