﻿using System;

namespace Harvey.Search.Abstractions
{
    public interface ISearchItem
    {
        string Id { get; set; }
    }
}
