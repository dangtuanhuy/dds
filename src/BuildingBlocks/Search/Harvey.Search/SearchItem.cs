﻿using System;
using Harvey.Search.Abstractions;

namespace Harvey.Search
{
    public abstract class SearchItem : ISearchItem
    {
        public string Id { get; set; }

        public SearchItem()
        {

        }

        public SearchItem(string id) : this()
        {
            Id = id;
        }
    }
}
