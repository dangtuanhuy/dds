﻿using System.Linq.Expressions;
using System.Reflection;

namespace Harvey.Persitance.EF
{
    public static class ExpressionExtensions
    {
        public static Expression Replace(this Expression expression, Expression searchEx, Expression replaceEx)
        {
            return new ReplaceVisitor(searchEx, replaceEx).Visit(expression);
        }
    }

    public static class RefectionExtensions
    {
        public static object GetPropertyValue(object obj, string property)
        {
            if (!string.IsNullOrEmpty(property))
            {
                PropertyInfo propertyInfo = obj.GetType().GetProperty(property);
                if (propertyInfo != null)
                    return propertyInfo.GetValue(obj, null);
                return null;
            }
            return null;
        }
    }

    internal class ReplaceVisitor : ExpressionVisitor
    {
        private readonly Expression from, to;
        public ReplaceVisitor(Expression from, Expression to)
        {
            this.from = from;
            this.to = to;
        }
        public override Expression Visit(Expression node)
        {
            return node == from ? to : base.Visit(node);
        }
    }

}
